<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<c:set var="hccsmap" value="${bookingComponent.hccsMap}" scope="session"/>
<jsp:useBean id="now" class="java.util.Date" /><fmt:formatDate var ="currentyear" type="date" value="${now}" pattern="yyyy"/>
<%@include file="/common/intellitrackerSnippet.jspf"%>
<html>
<head>
<%@ page contentType="text/html; charset=UTF-8" %>
<version-tag:version/>
<c:set var="paymentFlow" value="${bookingComponent.bookingData.bookingFlowDetails.paymentFlow}"/>

	<c:if test="${paymentFlow != 'REFUND'}"> 
	<title>ATCOMRES - Payment Page</title>
  	</c:if>  
  
  <c:if test="${paymentFlow == 'REFUND'}"> 
	<title>ATCOMRES - Refund Page</title>
	 </c:if>
	 
<META NAME="description" content="Book great value flights with Thomson. Book your cheap flights, hotel accommodation, car hire, airport parking, transfers &amp; travel insurance through us." xmlns:myXsltExtension="urn:XsltExtension">
<META NAME="keywords" content="Thomson Airways, Thomson flights, thomsonfly.com, thomsonfly, cheap flights, cheapflights, cheap, flights, flight, ticket, tickets, air fare, airfares, fly, low cost fare, low cost, air travel, airline, budget airlines, travel, holidays, hotels, tickets, bookings, book, booking, plane, airport, discount, air, europe, international, scheduled, charter, destinations, longhaul, carhire, car hire, car rental, insurance, thomson holidays, Thompson, tompsonfly, flythomson, tomson, thomsonflights, thompsonfly, thomsonflights, britannia airways, birmingham, bournemouth, coventry, cardiff, doncaster, edinburgh, east midlands, gatwick, glasgow, jersey, leeds bradford, liverpool, luton, manchester, newcastle, stansted, london, spain, majorca, alicante, barcelona, faro, grenoble, ibiza, jersey, lanzarote, malaga, minorca, menorca, naples, paris, paphos, palma, prague, pisa, marrakech, salzburg, tenerife, turkey, cancun, mexico, cuba, egypt, greece, cyprus, dominican republic, croatia, italy, morocco, canary islands, portugal" xmlns:myXsltExtension="urn:XsltExtension">


<link rel="icon" href="/cms-cps/atcomres/images/favicon.ico" type="image/x-icon" xmlns:myXsltExtension="urn:XsltExtension">
<link rel="shortcut icon" href="/cms-cps/atcomres/images/favicon.ico" type="image/x-icon" xmlns:myXsltExtension="urn:XsltExtension">

<script src="/cms-cps/atcomres/js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
<script type="text/javascript" src="/cms-cps/atcomres/js/CommonValidation.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
<script type="text/javascript" src="/cms-cps/atcomres/js/basic.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
<!--Modified for ensighten to remove existing tag-->
<c:if test="${paymentFlow != 'REFUND'}">
<script type="text/javascript" src="/cms-cps/atcomres/js/atcomres.js"></script>
<script type="text/javascript" src="/cms-cps/atcomres/js/popups.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
<script type="text/javascript" src="/cms-cps/atcomres/js/payment.js"></script>
<script src="/cms-cps/common/js/utils.js" type="text/javascript"></script>
<script src="/cms-cps/atcomres/js/events.js" type="text/javascript"></script>
<script type="text/javascript" src="/cms-cps/atcomres/js/functions_depositSection.js"></script>
</c:if>

<link href="/cms-cps/atcomres/css/cps.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/cms-cps/atcomres/css/headfoot.css" rel="stylesheet" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="/cms-cps/atcomres/css/flexi-box/core.css" title="Thomson" media="screen" />
<!--flexibox-->
<!--[if lt IE 7]>
            <link rel="stylesheet" type="text/css" href="/cms-cps/atcomres/css/flexi-box/core-ie6.css" title="Thomson" media="screen" />
            <link href="/cms-cps/atcomres/css/headfoot-ie6.css" rel="stylesheet" type="text/css" media="screen" />
      <![endif]-->
<!--end flexibox-->
<!--[if gte IE 8]>
<style>
   .visaContainer{ padding-top: 135px;}
   .mastercardContainer{ padding-top: 138px;}
</style>
<![endif]-->

<!--[if lte IE 6]>
       <%--  This script is added to remove image caching in ie 6 which was a main cause for me
       losing sleep for days :-) This solves the flickering images problem in IE6.--%>
       <script type="text/javascript">
         try
         {
	        document.execCommand("BackgroundImageCache", false, true);
         }
         catch(err) { }
       </script>
<![endif]-->

<!-- Modified for ensighten to remove the existing tag which includes intellitracker.js -->
<c:if test="${not empty paymentFlow}">
<script type="text/javascript" language="javascript">
	//js variable to be used in the intellitracker pqry string.
    var previousPageVar ="${bookingComponent.nonPaymentData['prevPage' ]}";
</script>

  </c:if>
<%--  <%@include file="/atcomres/tag.jsp"%> --%>
</head>
<body>
<c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
<c:set var="visiturl" value="yes" scope="session" />

   <div class="pageContainer">
   <%-- <jsp:include page="/atcomres/cookielegislation.jsp"></jsp:include> --%>
     
     <!--end of HeaderSection-->
  <c:if test="${paymentFlow != 'REFUND'}"> 
	<div id="headerSection">
		<jsp:include page="header.jsp" />
	 </div>
	<jsp:include page="mainPaymentPage.jsp" />
	<jsp:include page="footer.jsp" />
  </c:if>
  <c:if test="${paymentFlow == 'REFUND'}">
	<div id="headerSection" class="rf">
		<jsp:include page="refund/refundheader.jsp" />
	 </div>
	<jsp:include page="refund/refundPaymentPage.jsp" />
	<div class="rf">
		<div class="content-section">
			<jsp:include page="refund/refundfooter.jsp" />
		</div>
	</div>
  </c:if>
   
     <!--end of ContentSection-->
     
     <!--end of FooterSection-->
    
   </div>
   <!--end of PageContainer-->

  <c:if test="${empty paymentFlow}">
   <script>
     if(PaymentInfo.chargeamt !=0) { displayFieldsWithValuesForCard(); }
   </script>
</c:if>
</body>
</html>
