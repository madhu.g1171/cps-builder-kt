<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="nonPaymentData" value="${bookingComponent.nonPaymentData}" scope="request" />	
<div class="head">
   <div class="contentBox">
<a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')" class="logo" title="Thomson Logo"><span class="hide">Thomson</span></a>
        <ul class="rfheader">
            <li class="bld">
		     <span class="detail">Agent code:
		     </span>
		        <span class="id"><c:out value="${nonPaymentData.agent_code}"/>
		        </span>
		     </li>
            <li class="bld">
			    <span class="detail">User Id:
				</span>
				    <span class="id"><c:out value="${nonPaymentData.user_id}"/>
					</span>
			</li>
            <li class="bld">
			    <span class="detail">Reservation Id:
				</span>
				    <span class="id"><c:out value="${nonPaymentData.reservation_id}"/>
			    </span>
			</li>
        </ul>
   </div> 
</div>
