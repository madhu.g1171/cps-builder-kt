<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="nonPaymentData" value="${bookingComponent.nonPaymentData}" scope="request" />
<c:set var="deposit_low_available" value="${nonPaymentData.deposit_low_available}"/>
<c:set var="part_payment_available" value="${nonPaymentData.part_payment_available}"/>

<c:set var="deposit_high_available" value="${nonPaymentData.deposit_high_available}"/>
<div class="lt-col">
            <div class="refund-summary">
                <h3 class="sectionhead"> Cost to customer</h3>
                <ul>
                    <li>
                         <span class="detail">Holiday Cost:</span>
                             <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> 
							      <c:out value="${nonPaymentData.holiday_cost}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/>
							</span>
                    </li>
                    <li>
                        <span class="detail">TUI CC Charges:</span>
                         <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> 
						 <c:out value="${nonPaymentData.creditcard_charges}" escapeXml="false"/>  
						 <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/>
					    </span>
                    </li> 
                    <li class="btmborder">
                         <span class="detail bld">Total:</span>
                         <span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.transaction_total}" escapeXml="false"/>
						</span>
                    </li>
                <li>Amount Paid</li>
                <c:if test="${fn:containsIgnoreCase(nonPaymentData.deposit_low_available, 'true')}">            
			        <li>
                        <span class="detail">Low Deposit:</span>
                        <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.deposit_low_amount}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/>
					    </span>
                    </li>
                </c:if>

			 
				<c:if test="${fn:containsIgnoreCase(nonPaymentData.deposit_high_available, 'true')}"> 			 
				<li>
                    <span class="detail">Deposit:</span>
                     <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.deposit_high_amount}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/>
		            </span>
                </li>
			    </c:if>
                <c:if test="${fn:containsIgnoreCase(nonPaymentData.part_payment_available, 'true')}">			 
                 <li>
                     <span class="detail">Other Payments:</span>
                     <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.othert_payment_amount}" escapeXml="false"/> 
				     </span>
                </li>
			    </c:if>	
                <li>
                    <span class="detail">Total Paid:</span>
                    <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.amount_paid}" escapeXml="false"/>
					</span>
                </li>
                <li class="btmborder">
                  <span class="detail bld">Outstanding Balance:</span>
                  <span class="price bld highlight"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.balance_amount}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
                </li>
                <li>
                  <span class="detail bld">*Amount Refundable:</span>
                  <span class="price bld "><c:out value="${bookingComponent.totalAmount.symbol}"/> 
				  <fmt:formatNumber value="${bookingComponent.totalAmount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/>
				  
				  </span>
                </li>
                <li>
                  <span class="spacer">*Excludes TUI CC Charges</span>
                </li>
              </ul>
            </div>
          </div>