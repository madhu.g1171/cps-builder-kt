<%@ page import="com.tui.uk.config.ConfReader;"%>
<!--<h2>Refund</h2>-->
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<%
	String giftCardBinRange =(String)ConfReader.getConfEntry("TUIGiftCard.BINRange","");
	 pageContext.setAttribute("giftCardBinRange",giftCardBinRange);	
			%>
<c:if test='${not empty error}'>
                    <div class="errorContainer">
                       <c:out value="${bookingComponent.errorMessage}" escapeXml="false"/>
                    </div>
		        </c:if>
            <div class="transaction-list">
                <table class="transaction-table">
										<thead>
											<tr>
												<th class="payee">Payee</th>
												<th class="paydate">Payment Date</th>
												<th class="paydate">Payment Method</th>
												<th class="datacash-ref">Payment System reference</th>
												<th class="cardno">Payment Number</th>
												<th class="expiry">Expiry Date</th>
												<th class="amt">Amount</th>
												<th class="status"></th>
											</tr>
										</thead>
										<tbody>
                    <c:forEach  var="refundDetail" varStatus="loop"
	                   items="${bookingComponent.refundDetails}" >
                      <c:set var="maskedCardNumber" value="${refundDetail.maskedCardNumber}"/>
                       <c:set var="splitmaskedCardNumber" value="${fn:substring(maskedCardNumber, 0, 6)}" />


											<tr>
												<td class="payee">
												<input type="radio" name="payee" value="${refundDetail.maskedCardNumber}" checked>
												<label><span>${refundDetail.payeeName}</span></label>
												</td>
												<td class="paydate"><fmt:formatDate value="${refundDetail.transactionDate}" pattern="dd-MM-yyyy" /></td>
												<td class="payType">
												<c:choose>
													<c:when test="${splitmaskedCardNumber ne giftCardBinRange}">
													 ${refundDetail.paymentType}</c:when><c:otherwise>TUI Gift Card
														</c:otherwise>
												</c:choose></td>
												<td class="datacash-ref">${refundDetail.datacashReference}</td>
												<td class="cardno">${refundDetail.maskedCardNumber}</td>
												<td class="expiry" expdt="<fmt:formatDate value="${refundDetail.cardExpiry}" pattern="dd-MM-yyyy" />"><fmt:formatDate value="${refundDetail.cardExpiry}" pattern="MM/yyyy" /></td>
												<td class="amt"><fmt:formatNumber value="${refundDetail.transactionAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/></td>
												<td class="status"><c:if test="${refundDetail.refundEnabled}"></c:if>
												<c:if test="${!refundDetail.refundEnabled}">Expired</c:if>
												</td>
											</tr>
					</c:forEach>

					<div class="carddata">
                       <input type="hidden"  class="cardtype" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>
					   <input type="hidden"  class="nameoncard" id="payment_0_nameOnCard" value="" name="payment_0_nameOnCard"/>
					   <input type="hidden"  class="cardNumber" id="payment_0_cardNumber" value="" name="payment_0_cardNumber"/>
					   <input type="hidden"  class="datacashrefid" id="payment_0_datacashreference" value="" name="payment_0_datacashreference"/>

					   <input type="hidden"  class="paymentDate" id="payment_0_paymentDate" value="" name="payment_0_paymentDate"/>
					   <input type="hidden"  class="expiryDate" id="payment_0_expiryDate" value="" name="payment_0_expiryDate"/>
					</div>
				
				</table>
            </div>
           <!--<p class="bld bluehiglight">Select payment transaction to refund against</p>-->
            <!-- <div class="summarypanel">
             <ul class="paysummarylist">
                  <li>
                    <span class="detail bld">Refund Amount:</span>
                    <span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/> <span class="amt"><c:out value="${nonPaymentData.holiday_cost}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/></span></span>
                  </li>
              </ul>
              <h2 class="summaryhead">Summary</h2>
              <ul class="paysummarylist refundsummary">
                  <li>
                    <span class="detail">Maximum Amount Refundable:</span>
                    <span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/> <span class="baseamt"><fmt:formatNumber value="${bookingComponent.payableAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/>

					</span></span>
                  </li>
                  <li>
                    <span class="detail">Amount be Refunded:</span>
                    <span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/><span class="amt rfundamt"></span></span>
                  </li>
                  <li>
                    <span class="detail">Outstanding Refundable Amount:</span>
                    <span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/><span class="dueamt"></span></span>
                  </li>
              </ul>
            </div> -->