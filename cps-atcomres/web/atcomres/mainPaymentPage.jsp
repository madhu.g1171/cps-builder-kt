 <%@include file="/common/commonTagLibs.jspf"%>
 <%
 String isMasterCardGiftApplied = com.tui.uk.config.ConfReader.getConfEntry("isMasterCardGiftApplied" , "");
 pageContext.setAttribute("isMasterCardGiftApplied", isMasterCardGiftApplied, PageContext.REQUEST_SCOPE);
 %>
 <script type="text/javascript">
   var  newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";
   var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
   var token = "<c:out value='${param.token}' />" ;
   var streetaddress1="${bookingComponent.nonPaymentData['street_address1']}";
   var streetaddress2="${bookingComponent.nonPaymentData['street_address2']}";
   var streetaddress3="${bookingComponent.nonPaymentData['street_address3']}";
   var cardBillingPostcode="${bookingComponent.nonPaymentData['cardBillingPostcode']}";
  	var selectedcountryforleadpassenger="${bookingComponent.nonPaymentData['selectedCountryCode']}";

   var PaymentInfo = new Object();

PaymentInfo.totalAmount =${bookingComponent.totalAmount.amount};
PaymentInfo.payableAmount = ${bookingComponent.payableAmount.amount};
PaymentInfo.calculatedPayableAmount = ${bookingInfo.calculatedPayableAmount.amount};
PaymentInfo.calculatedTotalAmount = ${bookingInfo.calculatedTotalAmount.amount};
PaymentInfo.currency = "${bookingComponent.totalAmount.symbol}";

<fmt:formatNumber var="ccAmount" value="${bookingInfo.calculatedCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>

PaymentInfo.chargeamt =${ccAmount};
PaymentInfo.selectedCardType = null;

   var cardChargeMap = new Map();
   var pleaseSelect = new Array();

cardChargeMap.put('none', pleaseSelect);
  <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">

  <c:choose>
	<c:when
		test="${cardCharges.key=='MASTERCARD_GIFT'}">
		<c:if test="${isMasterCardGiftApplied=='true'}">

		 var cardChargeInfo = new Array();
cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');

		 </c:if>
	</c:when>

	<c:otherwise>
  var cardChargeInfo = new Array();
		     cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');

	</c:otherwise>
</c:choose>

     </c:forEach>


var cardDetails = new Map;
 <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">

 <c:choose>
	<c:when
		test="${cardDetails.paymentCode=='MASTERCARD_GIFT'}">
		<c:if test="${isMasterCardGiftApplied=='true'}">

			    var cardCharge_Max_Min_Issue = new Array();
        	         	   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
						    cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
							 cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');

			  cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);

		 </c:if>
	</c:when>

	<c:otherwise>
		  var cardCharge_Max_Min_Issue = new Array();
	   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
	    cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
		 cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');

cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);

	</c:otherwise>
</c:choose>

         </c:forEach>

		
        //  The following map stores the pay button labels in case of 3D cards.
        var threeDCards = new Map();
        <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
            threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
        </c:forEach>

  </script>

 <div id="contentSection">

      
          <div class="titleSection" id="pageTitle">
            <h1>Integrated Payment (CPS)</h1>
          </div>
          <!--left panel-->
          <div id="contentCol1">
        <jsp:include page="pricing.jsp"></jsp:include> 
          </div>
          <!--end of left panel-->
       <div id="contentCol2">

         <form  id="SkySales" name ="SkySales" method="post" action="/cps/processPayment?b=15000&token=<c:out value='${param.token}' />&tomcat=<c:out value='${param.tomcat}' />" novalidate>


           <%-- This section was added to solve the problem caused at the sinnerschrader end --%>

           <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="${param.tomcat}" />
           <input type="hidden" name="token" id="token" value="${param.token}" />
			  
        <!-- Payment -->
        <c:choose>
            <c:when	test="${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}">
				<div class="section">
					<%@include file="depositSection.jspf"%>
					<jsp:include page="atcomresHccCardDetails.jsp" />
				</div>  

			   <div class="pageControls">
				  <c:set var="backurl" value="${bookingComponent.prePaymentUrl}"/>
				  <a class="backPage" href="<c:out value='${backurl}'/>" title="Back to passenger details">Back to passenger details</a>  
			   </div>
		    </c:when>
	        <c:otherwise>
			   <jsp:include page="paymentDetails.jsp"></jsp:include>
			   <jsp:include page="passengerDetails.jsp"></jsp:include> 

			   <c:choose>
				  <c:when test="${bookingComponent.accommodationSummary != null &&
									   bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
					 <jsp:include page="tandc.jsp"></jsp:include>
				  </c:when>
				  <c:otherwise>
					 <jsp:include page="fareRules.jsp"></jsp:include>
				  </c:otherwise>
			   </c:choose>

			   <div class="pageControls">
				  <c:set var="backurl" value="${bookingComponent.prePaymentUrl}"/>
				  <a class="backPage" href="<c:out value='${backurl}'/>" title="Back to passenger details">Back to passenger details</a>
				  <c:if test="${bookingInfo.newHoliday}">
					<div class="buttonGroup"><span class="forwardButtonOuter"><span class="forwardButtonInner">
					 <input type="submit" id="bookingcontinue" value="Pay" class="primary" title="pay" onclick="return validateRequiredField() && preventDoubleClick() && formSubmit()"/>
					</span></span>
					</div>
				  </c:if>
			   </div>
		    </c:otherwise>
        </c:choose>
         </form>
       </div>
       <!--end of contentCol2-->
     </div>
