<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="tomcatInstance" value="${param.tomcat}" />
<c:set var="token" value="${param.token}" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<%
    String tuiHeaderSwitch = com.tui.uk.config.ConfReader.getConfEntry("atcomres.switch" , "");
    pageContext.setAttribute("tuiHeaderSwitch2", tuiHeaderSwitch, PageContext.REQUEST_SCOPE);

%>

<c:choose>

   <c:when test="${tuiHeaderSwitch2 eq 'ON'}">
      <c:set value="TUI" var="textValue"/>
   </c:when>
   <c:otherwise>
      <c:set value="Thomson" var="textValue"/>
   </c:otherwise>
</c:choose>


<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">

<%
    String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.GBP.cardChargeDetails" , "");
    pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
    String isMasterCardGiftApplied = com.tui.uk.config.ConfReader.getConfEntry("isMasterCardGiftApplied" , "");
    pageContext.setAttribute("isMasterCardGiftApplied", isMasterCardGiftApplied, PageContext.REQUEST_SCOPE);

%>
</c:if>
<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
        <%
        String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.EUR.cardChargeDetails" , "");
        pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
        %>
</c:if>
<c:set var="cardChargeDetails" value="${creditCardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<fmt:formatNumber value="${cardChargeArray[0]}" type="number" var="cardCharge"/>

<%
   String[] defaultValue=null;
    String debitCardChargeDetails[] = com.tui.uk.config.ConfReader.getStringValues("AtcomRes.GBP.debitCardChargeDetails" ,defaultValue,",");

    %>
	
	 <script type="text/javascript" src="/cms-cps/fcx/js/vs.js"></script>
	 
	 <%
String applyCreditCardSurchargeGBP=com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.GBP.applyCreditCardSurcharge", null);
pageContext.setAttribute("applyCreditCardSurchargeGBP", applyCreditCardSurchargeGBP, PageContext.REQUEST_SCOPE);
String applyCreditCardSurchargeEUR=com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.EUR.applyCreditCardSurcharge", null);
pageContext.setAttribute("applyCreditCardSurchargeEUR", applyCreditCardSurchargeEUR, PageContext.REQUEST_SCOPE);
%>


<script>
applyCreditCardSurchargeGBP ="<%=applyCreditCardSurchargeGBP%>";
applyCreditCardSurchargeEUR ="<%=applyCreditCardSurchargeEUR%>";
</script>

<script type="text/javascript">
   var creditCardChargeDetails = '<c:out value="${creditCardChargeDetails}" />';
  var debitCardChargeDetails = '<c:out value="${debitCardChargeDetails}" />';
</script>


<h2 id="paymentDetails"><span>Payment details</span></h2>
<div class="mandatoryKey">Please complete all fields marked with <span class="mandatory">*</span></div>
<iframe name="HCCRedirectToCPS" id="hcctoCps" src="${hccsmap.hpsUrl}?HPS_SessionID=${hccsmap.hpsSession}" style="width: 100%;height: 744px; border: none;">
</iframe>
		
		