<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}"
    scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}" />

<!--fare rules-->
<div class="detailsPanel" id="fareDetailsPanel">
    <div class="top">
        <div class="right"></div>
    </div>
    <div class="body">
        <div class="right">
            <div class="content">
                <!--  <div class="section">
                     <h2 id="fareRules"><span>Fare rules</span></h2>
                     <div class="sectionContent">

                         <p>Any changes must be made via the Call Centre up to 3 hours before departure. This is subject to availability.</p>   <br/>
                         <p>The purchased ticket is non-refundable. If you would like to amend your ticket, you will be charged for this. This charge will include a fee per change, per person. Depending on your amendment type, together with the fee(s) you may also pay the difference in airfare and you may lose a portion of your original airfare depending on the proximity of the departure date on the day you request your change.</p><br/>
                         <p>Please see our <a href="javascript:openPopupCustomWindow(
                         'http://www.thomson.co.uk/editorial/legal/flight-conditions-of-carriage.html','795','595','yes','yes')" title="View conditions of carriage" rel="external">Conditions of Carriage</a> for further details.</p><br/>

                     </div>
                   </div> -->
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="right"></div>
    </div>
</div>
<!--fare rules-->

<%-- <jsp:include page="safetyInfo.jsp"></jsp:include> --%>
<!--Terms-->
<div class="detailsPanel" id="fareDetailsPanel">
    <div class="top">
        <div class="right"></div>
    </div>
    <div class="body">
        <div class="right">
            <div class="content">
                <div class="section">
                    <h2 id="fareRules">
                        <span>Terms & Conditions</span>
                    </h2>
                    <div class="sectionContent">

                        <p>
                            Please note that all members of your party require valid
                            passports and any applicable visas before travelling. Visit the
                            Foreign Office website at <a href="http://www.fco.gov.uk/travel"
                                target="_blank">http://www.fco.gov.uk/travel</a> to see visa and
                            travel advice.
                        </p>
                        <br />
                        <p>
                            To view the notice summarising the liability rules applied by
                            Community air carriers as required by Community legislation and
                            the Montreal Convention, please view the <a
                                href="javascript:void(0);"
                                onclick="Popup('http://www.thomson.co.uk/editorial/legal/montreal-convention.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">Air
                                Passenger Notice.</a>
                        </p>
                        <c:if
                            test="${(bookingComponent.nonPaymentData['atol_date']) != null}">
                            <c:set var="atoldate"
                                value="${(bookingComponent.nonPaymentData['atol_date'])}" />
                            <c:if
                                test="${!(fn:contains(bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode'],'TOM'))|| (( bookingComponent.nonPaymentData['atol_protected'] != null ) &&( bookingComponent.nonPaymentData['atol_protected'] ))}">
                                <fmt:setTimeZone value="Europe/London" scope="session" />
                                <c:set var="nowOct" value="${atoldate} " />
                                <fmt:parseDate var="nowOct" value="${nowOct}" type="DATE"
                                    pattern="dd MMM yyyy" />
                                <jsp:useBean id="now" class="java.util.Date" />
                                <fmt:formatDate value="${now}" var="now" pattern="dd MMM yyyy" />
                                <fmt:parseDate var="now" value="${now}" type="DATE"
                                    pattern="dd MMM yyyy" />
                                <c:if test="${ now ge nowOct }">
                                    <br />
                                    <p>This booking is authorised under the ATOL licence of TUI
                                        UK Limited, ATOL 2524, and is protected under the ATOL scheme,
                                        as set out in the ATOL certificate to be supplied.</p>
                                </c:if>
                            </c:if>
                        </c:if>
                        <br />
                        <div class="controls tc" >
                            <p>
                                <input class="checkbox" type="checkbox" name="termAndCondition" 
                                    id="termAndCondition" required="true"
                                    requiredError=">Terms and conditions is required." onChange="removeErr()"/>
                                    

                                    <input
                                    id="tourOperatorTermsAccepted" type="hidden"
                                    name="tourOperatorTermsAccepted"> &nbsp; I confirm that
                                I have read and accept the <a
                                    href="javascript:openPopupCustomWindow(
                         'http://flights.thomson.co.uk/en/popup_booking_conditions.html','570','550','yes','yes')"
                                    title="View booking conditions" rel="external">Booking
                                    Conditions</a>, <a
                                    href="javascript:openPopupCustomWindow(
                        'http://www.thomson.co.uk/editorial/legal/flight-conditions-of-carriage.html','795','595','yes','yes')"
                                    title="View conditions of carriage" rel="external">Conditions
                                    of Carriage</a> , <a
                                    href="javascript:openPopupCustomWindow(
                         'http://www.thomson.co.uk/editorial/legal/privacy-policy.html','795','595','yes','yes')"
                                    title="View privacy policy" rel="external">Privacy Policy</a>
                                and<br />&nbsp; Baggage check-in safety information. All transactions are conducted over our secure server.<br />
                                <br />
                            </p>
							<div id="messg"></div>
						
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="right"></div>
    </div>
</div>


<!--Terms-->

