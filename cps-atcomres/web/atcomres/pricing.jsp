<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="nonPaymentData" value="${bookingComponent.nonPaymentData}" scope="request" />
<c:set var="deposit_low_available" value="${nonPaymentData.deposit_low_available}"/>
<c:set var="deposit_high_available" value="${nonPaymentData.deposit_high_available}"/>

<style>
	.pl10px
	{
	padding:0 10px;
	}
	.clear
	{
	clear:both;
	}
</style>
<div id="summaryPanelContainer">
  <div class="detailsPanel" id="summaryDetailsPanel">
    <div class="top">
      <div class="right"></div>
    </div>
    <div class="body">
      <div class="right">
        <div class="content">
          <div class="summarySection">

		  
			<h2 >Cost to Customer </h2>
            <div class="priceDetails">
              <ul>
                <li class="pricesInDetail"><span class="costingDetails">
		                  <c:out value="Agent Code:" escapeXml="false"/>
		             </span>
					  <span class="price"><c:out value="${nonPaymentData.agent_code}" escapeXml="false"/>  </span>
				 </li>
				 <li class="pricesInDetail"><span class="costingDetails">
		                  <c:out value="User Id:" escapeXml="false"/>
		             </span>
					  <span class="price"><c:out value="${nonPaymentData.user_id}" escapeXml="false"/>  </span>
				 </li>
				 <li class="pricesInDetail"><span class="costingDetails">
		                  <c:out value="Reservation Id:" escapeXml="false"/>
		             </span>
					  <span class="price"><c:out value="${nonPaymentData.reservation_id}" escapeXml="false"/>  </span>
				 </li>
              
 	            <li class="pricesInDetail"><span class="costingDetails">
		                  <c:out value="Total" escapeXml="false"/>
		             </span>
					   	   <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.transaction_total}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
			    </li>
									 
				<li class="pricesInDetail"><span class="costingDetails">
		                  <c:out value="Amount Paid" escapeXml="false"/>
		             </span>
					  <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.amount_paid}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
				 </li>
			              	   
				   <c:set var="counter" value="${bookingComponent.nonPaymentData['tax_count']}"/>
				   			 
							
				<li class="pricesInDetail"><span class="costingDetails">
		                 <c:out value="Balance" escapeXml="false"/>
		            </span>
					   <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.balance_amount}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
				</li>
			
			<c:if test="${fn:containsIgnoreCase(deposit_low_available, 'true')}">								
                <li class="taxesCharges"><span class="costingDetails"><a id="taxTerms" class="sticky stickyOwner" href="#" >   <c:out value="Next Payment" escapeXml="false"/></a></span> <span class="price"> <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
							minFractionDigits="2" pattern="#,##,###.##"/></span></li>
							
			    <li class="pricesInDetail"><span class="costingDetails">
					                  <c:out value="Next Payment Due" escapeXml="false"/>
		               	</span>
                           <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.deposit_low_amount}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
									 </li>
									  <li class="pricesInDetail"><span class="costingDetails">
					                  <c:out value="Next Payment Due Date" escapeXml="false"/>
					 </span>
								   <span class="price"><c:out value="${nonPaymentData.deposit_low_due_date}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
									 </li>
				
			</c:if>	
			<c:if test="${fn:containsIgnoreCase(deposit_high_available, 'true')}">			 
                  <li class="taxesCharges"><span class="costingDetails"><a id="taxTerms" class="sticky stickyOwner" href="#" >   <c:out value="Following Payment " escapeXml="false"/></a></span> <span class="price"> <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
							minFractionDigits="2" pattern="#,##,###.##"/></span></li>
							
					<li class="pricesInDetail"><span class="costingDetails">
					                  <c:out value="Following Payment Due" escapeXml="false"/>
					 </span>
					   	    
								   <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.deposit_high_amount}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
									 </li>
									 
									  <li class="pricesInDetail"><span class="costingDetails">
					                  <c:out value="Following Payment Due Date" escapeXml="false"/>
					 </span>
								   <span class="price"><c:out value="${nonPaymentData.deposit_high_due_date}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
									 </li>
			</c:if>				 
								 
				
              </ul>
            </div>
          
	          </div>
        </div>
      </div>
    </div>
    <div class="bottom">
      <div class="right"></div>
    </div>
  </div>
</div>