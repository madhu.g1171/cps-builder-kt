<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />

<div class="foot line">
   <div class="unit size3of4">
    
</div>
   <div class="unit size1of4 lastUnit">
     <!--  <div class="hereToHelp">
         <ul>
            <li><a onclick="window.open('http://www.thomson.co.uk/shopfinder/shop-finder.html','name','height=745,width=820,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no'); return false;" rel="nofollow" target="_blank" title="Shop finder" href="http://www.thomson.co.uk/shopfinder/shop-finder.html">Shop finder</a></li>
            <li><a rel="nofollow" title="Ask us a question" href="http://www.thomson.co.uk/editorial/faqs/flights/flight-faqs.html">Ask us a question</a></li>
            <li><a rel="nofollow" href="http://www.thomson.co.uk/editorial/legal/contact-us-flights.html">Contact us</a></li>
         </ul>
         <address>0871 231 4787</address>
         <p>Calls cost 10p per minute plus network extras</p>
      </div> -->
      <ul class="horizontal partner">
         <li><a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=www.thomson.co.uk&amp;lang=en" title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication" class="verisign" target="_blank"><span class="hide">Verisign Logo</span></a></li> 
   <!--  <li><a href="http://www.abta.com/find-a-holiday/member-search/5736" title="Verify ABTA membership - ABTA Number V5126" class="abta" target="_blank"><span class="hide">The ABTA logo</span>ABTA</a></li>
   --> </ul>
   </div>
</div>
<!-- Modified to remove ensighten from footer -->
