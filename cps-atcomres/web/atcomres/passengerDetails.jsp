<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="tomcatInstance" value="${param.tomcat}" />
<c:set var="token" value="${param.token}" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
<%
    String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.GBP.cardChargeDetails" , "");
    pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
%>
</c:if>
<c:set var="cardChargeDetails" value="${creditCardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<fmt:formatNumber value="${cardChargeArray[0]}" type="number" var="cardCharge" maxFractionDigits="1" minFractionDigits="0"/>

<%
    String debitCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("debitCardChargeDetails" , "");
    pageContext.setAttribute("debitCardChargeDetails", debitCardChargeDetails, PageContext.REQUEST_SCOPE);
%>

<div class="detailsPanel" id="fareDetailsPanel">
             <div class="top">
               <div class="right"></div>
             </div>
             <div class="body">
               <div class="right">
                 <div class="content">
                   <div class="section">
                        <h2 id="leadDetails"><span>Cardholder address details</span></h2>
                  <div class="mandatoryKey">Please complete all fields marked with <span class="mandatory">*</span></div>

                     <div class="sectionContent">

				To help ensure your card details remains secure, Please confirm the address of the cardholder.
								   <br/>If this is same as the lead passenger address, you just need to check the box.
                      <ul class="controlGroups">



                      <li class="controlGroup">
                        <div>
                          <input type="checkbox"  style="margin-left:180px;margin-top:15px;"
                            name="autofill" id="autofill" onclick="autoCompleteAddress()"> <span style="">same address as lead passenger</span>

                        </div>
                      </li>

                      <li class="controlGroup">
                        <p class="fieldLabel"><label>Street address</label></p>
                        <div class="controls">

                          <input type="text"
                             name="payment_0_street_address1" id="payment_0_street_address1"
                             maxlength="25" class="inputWhole longField"
							  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>"
						        requiredError="Street address is required." required="true"
								validationTypeError="Street address line 1 is invalid."
								validationType="Address">
							 <span class="mandatory">*</span>

                        </div>
                      </li>

                      <li class="controlGroup">

                        <div class="controls" style="margin-left:180px;">

                          <input type="text"
                             name="payment_0_street_address2" id="payment_0_street_address2"
                             maxlength="25" class="inputWhole longField"
							  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>"
								validationType="Address" validationTypeError="Street address line 2 is invalid."
							  >
                        </div>
                      </li>
                      <li class="controlGroup">
                        <p class="fieldLabel"><label>Town/city</label></p>
                        <div class="controls">
                        <input type="text"
                             name="payment_0_street_address3" id="payment_0_street_address3"
                              maxlength="25" class="inputWhole longField"
							  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>"
						validationType="City"
							requirederror="Town/city is required."
							required="true"
                            validationTypeError="Town/city is invalid.">
                          <span class="mandatory">*</span>
                        </div>
                      </li>
                      
                      <div id="pcode">
					    <li class="controlGroup">
                        <p class="fieldLabel"><label>Postal code</label></p>
                        <div class="controls">
                          <input name="payment_0_postCode" id="payment_0_postCode" type="text" maxlength="8"
						     value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>"
                           class="inputWhole"
						   requirederror="Postal code is required." 
						   required="true" validationType="Postcode" 
						   validationTypeError="Postal code is invalid.">
                           <span class="mandatory">*</span>
                        </div>
                          </li>
                          </div>

						 <li class="controlGroup">
                        <p class="fieldLabel"><label>Country</label></p>
                        <div class="controls">
						 <c:choose>
              <c:when test="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode'] == null }">
                      <c:set var="selectedCountryCode" value="GB"/>
     	 		  </c:when>
               <c:otherwise>
			      <c:set var="selectedCountryCode" value="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode']}"/>
			   </c:otherwise>
           </c:choose>
		   <c:set var="totalcountrycount" value="${bookingComponent.nonPaymentData['country_count']}"/>
<c:set var="selectedCountryName" value="${bookingInfo.paymentDataMap['payment_0_selectedCountry']}" scope="page"/>
           <select name="payment_0_selectedCountryCode" id="payment_0_selectedCountryCode" required="true" requiredError="Country is required." class="inputWhole longField" onchange="resetPostCode();document.getElementById('payment_0_selectedCountry').value=this.options[selectedIndex].value;" 
           value="<c:out value="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode']}"/>">
		   
		      <option id="pleaseSelect" value='GB' <c:if test='${selectedCountryName=="GB"}'> selected='selected'</c:if>>United Kingdom</option>
		      <option id="pleaseSelect" value='IE' <c:if test='${selectedCountryName=="IE"}'> selected='selected'</c:if>>Ireland</option>
		   </select>
		   <input type="hidden" id="payment_0_selectedCountry" name="payment_0_selectedCountry" value="${selectedCountryCode}" />
	       <span class="mandatory">*</span>
        </div>


                          </li>

                    </ul>


                     </div>
                   </div>
                 </div>
               </div>
             </div>
             <div class="bottom">
               <div class="right"></div>
             </div>
           </div>


<script type="text/javascript">
   var creditCardChargeDetails = '<c:out value="${creditCardChargeDetails}" />';
  var debitCardChargeDetails = '<c:out value="${debitCardChargeDetails}" />';
</script>

<%-- *******Essential fields ********************  --%>
<input type="hidden" name="payment_0_chargeAmount" id="payment_0_chargeIdAmount"  value="0" />

<input type="hidden" name="payment_totamtpaid"  id="payment_totamtpaid"
   value="<fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
   minFractionDigits="2" pattern="#####.##"/>" />
<input type="hidden" id="bookingCalAmt" name="bookingCalAmt" value="${bookingInfo.calculatedTotalAmount.amount}"/>

<%-- should carry payment method - Dcard etc  --%>
<input type="hidden" id="payment_0_paymentmethod" name="payment_0_paymentmethod"/>

<%-- no of transaction is 1  --%>
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>

<%-- should carry cardtype like AMERICAN_EXPRESS etc  --%>
<input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>

<%-- *******End Essential fields ********************  --%>
