package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentFlow;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for .NET applications.
 *
 * @author sesi rekha.r
 *
 */
public class AtcomResPostPaymentProcessor extends DotNetClientPostPaymentProcessor
{
// CHECKSTYLE:OFF
   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public AtcomResPostPaymentProcessor(PaymentData paymentData,
      Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   @Override
   public void process() throws PaymentValidationException, PostPaymentProcessorException
	{
		LogWriter.logInfoMessage("process method of : " + this.getClass());
		boolean hccSwitch = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
		if (!hccSwitch) {
			PaymentFlow paymentFlow = null;
			if (bookingComponent.getBookingData() != null) {
				paymentFlow = bookingComponent.getBookingData().getBookingFlowDetails().getPaymentFlow();
			}
			if (!(paymentFlow == PaymentFlow.REFUND)) {
				Money transactionAmount = getTransactionAmount();
				bookingInfo.setCalculatedPayableAmount(transactionAmount);
				String paymentNotes = requestParameterMap.get("payment_notes");
				bookingComponent.getNonPaymentData().put("payment_notes", paymentNotes);
				updateCardHolderAddress();
				super.process();
			} else {
				Money transactionRefundAmount = getTransactionRefundAmount();
				bookingInfo.setCalculatedPayableAmount(transactionRefundAmount);
				bookingComponent.setPaymentGatewayVirtualTerminalId(null);

				final Payment payment = new Payment();
				if (StringUtils.isNotBlank(
						requestParameterMap.get(PaymentConstants.PAYMENT + 0 + PaymentConstants.TRANSACTION_AMOUNT))) {
					payment.addPaymentTransaction(
							PaymentTransactionFactory.getPaymentTransactions(0, requestParameterMap, bookingInfo));
				}
				paymentData.setPayment(payment);
			}

		} else {
			PaymentFlow paymentFlow = null;
			if (bookingComponent.getBookingData() != null) {
				paymentFlow = bookingComponent.getBookingData().getBookingFlowDetails().getPaymentFlow();
			}
			if (!(paymentFlow == PaymentFlow.REFUND)) {
				Money transactionAmount = getHccTransactionAmount();
				bookingInfo.setCalculatedPayableAmount(transactionAmount);
				super.process();
			}
		}

	}
	/**
	 * This method is responsible for processing the data entered in payment page and verify if it is MasterCard_Gift that includes No surcharge
	 *
	 * @throws PostPaymentProcessorException if validation fails.
	 */
	public void verifyMasterCard_Gift() throws PostPaymentProcessorException{

		String isMasterCardGiftApplied = ConfReader.getConfEntry("isMasterCardGiftApplied",null);
		String value = requestParameterMap.get("payment_0_paymenttypecode");
		if(Boolean.valueOf(isMasterCardGiftApplied)){

			String giftCardNumber = ConfReader.getConfEntry("MasterCardTUIGiftCard.BINRange", null);
			String enteredCardNumber=requestParameterMap.get("payment_0_cardNumber");
			if (enteredCardNumber.startsWith(giftCardNumber))
			{
				if(!(requestParameterMap.get("cardType").equalsIgnoreCase("MASTERCARD_GIFT")))
				{
					throw new PostPaymentProcessorException
					("datacash.cardtype.mismatch");
				}
			}

		}

	}
   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
	{
		boolean hccSwitch = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
		if (!hccSwitch) {
			String paymenttypecode = requestParameterMap.get("payment_0_paymenttypecode");
			if (StringUtils.isBlank(requestParameterMap.get("payment_0_paymenttypecode"))) {
				throw new PostPaymentProcessorException("datacash.select.cardtype");
			}

			AtcomValidationErrors errors = new AtcomValidationErrors();
			PaymentFlow paymentFlow = null;
			if (bookingComponent.getBookingData() != null) {
				paymentFlow = bookingComponent.getBookingData().getBookingFlowDetails().getPaymentFlow();
			}
			if (!(paymentFlow == PaymentFlow.REFUND)) {
				String conf = "TUIGiftCard.BINRange";
				String giftCardNumber = ConfReader.getConfEntry(conf, null);
				String enteredCardNumber = requestParameterMap.get("payment_0_cardNumber");
				if (enteredCardNumber.startsWith(giftCardNumber)) {
					if (!(requestParameterMap.get("cardType").equalsIgnoreCase("MAESTRO"))) {
						throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
					}
				}
				if (requestParameterMap.get("cardType").equalsIgnoreCase("MAESTRO")) {
					if (!(enteredCardNumber.startsWith(giftCardNumber))) {
						throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
					}
				}
				verifyMasterCard_Gift();

				if ((requestParameterMap.get("payment_0_cardNumber") != null)
						&& (requestParameterMap.get("payment_0_paymenttypecode") != null)) {
					String thCCBinRange = "ThomsonCreditcard.BINRange";
					String thCCNumberSelected = null;
					String thCCConfiguration = ConfReader.getConfEntry(thCCBinRange, null);
					String cardNumber = requestParameterMap.get("payment_0_cardNumber");
					String[] thCCNumberList = thCCConfiguration.split(",");
					for (String thCCNumber : thCCNumberList) {
						if (cardNumber.startsWith(thCCNumber)) {
							if (requestParameterMap.get("payment_0_paymenttypecode").contains("TUI_MASTERCARD")) {
								thCCNumberSelected = thCCNumber;
								break;
							} else {
								throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
							}

						}
					}
					if (thCCNumberSelected != null) {
						if (!(requestParameterMap.get("payment_0_paymenttypecode").contains("TUI_MASTERCARD"))) {
							throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
						}
					} else {
						if ((requestParameterMap.get("payment_0_paymenttypecode").contains("TUI_MASTERCARD"))) {
							throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
						}
					}
				}

				String vtid = bookingInfo.getDatacashVTid();
				String password = ConfReader.getConfEntry(vtid, null);

				if (StringUtils.isBlank(password)) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.vtid");
				}
				String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE).trim();
				String total_trans = requestParameterMap.get("total_transamt");
				float totalTransAmt = Float.parseFloat(total_trans);
				if (totalTransAmt < 0) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.negativeAmt");
				}
				if (depositType.equalsIgnoreCase("other")) {
					if (requestParameterMap.get("othrAmt") != "") {
						BigDecimal otherAmount = new BigDecimal(requestParameterMap.get("othrAmt").trim());
						if (otherAmount.compareTo(new BigDecimal(0)) == 1) {

							BigDecimal partPaymentMinAmount = new BigDecimal(
									bookingComponent.getNonPaymentData().get("part_payment_min_amount").trim());
							BigDecimal totalAmount = new BigDecimal(
									bookingComponent.getTotalAmount().getAmount().toString());

							/*
							 * if (otherAmount.compareTo(partPaymentMinAmount) == -1) { throw new
							 * PostPaymentProcessorException(
							 * "nonpaymentdatavalidation.AtcomRes.otherAmount"); }
							 */
							if (otherAmount.compareTo(totalAmount) == 1) {
								throw new PostPaymentProcessorException(
										"nonpaymentdatavalidation.AtcomRes.payAllAmount");
							}
						} else {
							throw new PostPaymentProcessorException(
									"nonpaymentdatavalidation.AtcomRes.invalidOtherAmount");
						}
					} else {
						throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.invalidOtherAmount");
					}
				}
				if (!(errors.patternCheck("streetAddress1", requestParameterMap.get("payment_0_street_address1")))) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.streetAddress1");
				}
				if (requestParameterMap.get("payment_0_street_address2") != "") {
					if (!(errors.patternCheck("streetAddress2",
							requestParameterMap.get("payment_0_street_address2")))) {
						throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.streetAddress2");
					}
				}
				if (!(errors.patternCheck("town", requestParameterMap.get("payment_0_street_address3")))) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.town");
				}
				if ((requestParameterMap.get("payment_0_selectedCountryCode") == "GB")) {
					if (!(errors.patternCheck("postcode", requestParameterMap.get("payment_0_postCode")))) {
						throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.postCode");
					}
				}

				if (requestParameterMap.get("payment_0_selectedCountryCode") == "") {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.country");
				}
				if (requestParameterMap.get("payment_notes") != "") {
					String str = requestParameterMap.get("payment_notes");
					if (str.length() > 100) {
						throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.paymentNotes");
					}
					if (!(str.matches("[\u0020-\u007A]+"))) {
						throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.paymentNotes");
					}
				}
				super.preProcess();
			} else {
				String refundType = requestParameterMap.get("refund").trim();
				String cardNo = requestParameterMap.get("payee");
				String paymentType = requestParameterMap.get("payment_0_paymenttypecode");
				String payeeName = requestParameterMap.get("payment_0_nameOnCard");
				String cardNumber = requestParameterMap.get("payment_0_cardNumber");
				String datacashrefId = requestParameterMap.get("payment_0_datacashreference");
				String total_trans = requestParameterMap.get("payment_0_transamt");
				float totalTransAmt = Float.parseFloat(total_trans);
				String stillDueAmount = requestParameterMap.get("payment_0_stillDueAmount");
				bookingComponent.getNonPaymentData().put("payment_0_stillDueAmount", stillDueAmount);
				if (totalTransAmt < 0) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.negativeAmt");
				}
				if (!(errors.patternCheck("paymentType", requestParameterMap.get("payment_0_paymenttypecode")))) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.paymentType");

				}

				if (!(errors.patternCheck("payeeName", requestParameterMap.get("payment_0_nameOnCard")))) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.payeeName");

				}

				if (!StringUtils.equalsIgnoreCase("PayPal", paymenttypecode)) {

					if (!(errors.patternCheck("maskedCardNumber", requestParameterMap.get("payment_0_cardNumber")))) {
						throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.cardNumber");

					}

				}

				if (!(errors.patternCheck("datacashrefId", requestParameterMap.get("payment_0_datacashreference")))) {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.datacashrefId");

				}

				if (StringUtils.isNotBlank(refundType) && StringUtils.isNotBlank(cardNo)) {

					if (refundType.equalsIgnoreCase("partrefundamount")) {
						String partRefundAmtId = requestParameterMap.get("refundPartAmount");
						if (StringUtils.isNotBlank(partRefundAmtId)) {
							BigDecimal otherAmount = new BigDecimal(partRefundAmtId.trim());
							if (otherAmount.compareTo(new BigDecimal(0)) == 1) {

								BigDecimal totalAmount = new BigDecimal(
										bookingComponent.getPayableAmount().getAmount().toString());

								if (otherAmount.compareTo(totalAmount) == 1) {
									throw new PostPaymentProcessorException(
											"nonpaymentdatavalidation.AtcomRes.refundAmount");
								}
							} else {
								throw new PostPaymentProcessorException(
										"nonpaymentdatavalidation.AtcomRes.invalidOtherAmount");
							}
						}
						/*
						 * else { throw new PostPaymentProcessorException(
						 * "nonpaymentdatavalidation.AtcomRes.invalidOtherAmount"); }
						 */
					}

				} else if (StringUtils.isBlank(cardNo) && StringUtils.equalsIgnoreCase("PayPal", paymenttypecode)) {
				} else {
					throw new PostPaymentProcessorException("nonpaymentdatavalidation.AtcomRes.invalidRefundAmount");
				}
			}

		} else {
			super.preProcess();
		}

	}

   /**
    * This method is responsible for calculating transaction amount based on deposit selected.
    *
    * @return transactionAmount the transaction amount.
    */
   private Money getTransactionAmount()
   {
      Money transactionAmount = null;
      // Get the deposit type, based on radio button selection
      if (requestParameterMap.get(BookingConstants.DEPOSIT_TYPE) != null)
      {
         transactionAmount = getPaymentAmount();
      }
      else
      {
         transactionAmount = bookingInfo.getCalculatedTotalAmount();
      }
      return transactionAmount;
   }
   
   /**
    * This method is responsible for calculating transaction amount based on deposit selected.
    *
    * @return transactionAmount the transaction amount.
    */
   private Money getHccTransactionAmount()
   {
      Money transactionAmount = null;
      // Get the deposit type, based on radio button selection
      
      Currency currency = bookingComponent.getTotalAmount().getCurrency();
      transactionAmount = new Money(new BigDecimal(bookingComponent.getHccCapturedData().get("payment_0_transamt")), currency);


      return transactionAmount;
   }

   private Money getPaymentAmount()
   {
      Money transactionAmount = null;
      String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE).trim();

      // Update Non Payment data mapo with selected deposit type
      bookingComponent.getNonPaymentData().put(BookingConstants.SELECTED_DEPOSIT_TYPE, depositType);

      Currency currency = bookingComponent.getTotalAmount().getCurrency();
      if (depositType.equalsIgnoreCase("lowDeposit"))
      {
         LogWriter.logInfoMessage("Low Deposit Selected");
         transactionAmount =
                  new Money(
                     new BigDecimal(bookingComponent.getNonPaymentData().get("deposit_low_amount")),
                     currency);
      }
      else if (depositType.equalsIgnoreCase("highBalance"))
      {
         LogWriter.logInfoMessage("High Deposit Selected");
         transactionAmount =
                  new Money(new BigDecimal(bookingComponent.getNonPaymentData()
                     .get("deposit_high_amount")), currency);
      }
      else if (depositType.equalsIgnoreCase("fullcost"))
      {
         LogWriter.logInfoMessage("Full Balance Selected");
         transactionAmount = bookingComponent.getTotalAmount();
      }
      else if (depositType.equalsIgnoreCase("other"))
      {
         LogWriter.logInfoMessage("Other Amount Selected");
         transactionAmount =
                  new Money(new BigDecimal(requestParameterMap.get("othrAmt").trim()), currency);
      }
      LogWriter.logInfoMessage("transactionAmount : " + transactionAmount);
      return transactionAmount;
   }

   /**
    * This method is to update the cardholder address
    *
    */
   private void updateCardHolderAddress()
   {
      String street_address1 = requestParameterMap.get("payment_0_street_address1");
      String street_address2 = requestParameterMap.get("payment_0_street_address2");
      String street_address3 = requestParameterMap.get("payment_0_street_address3");
      String cardBillingPostcode = requestParameterMap.get("payment_0_postCode");
      String selectedCountryCode = requestParameterMap.get("payment_0_selectedCountryCode");

      bookingComponent.getNonPaymentData().put("street_address1", street_address1);
      bookingComponent.getNonPaymentData().put("street_address2", street_address2);
      bookingComponent.getNonPaymentData().put("street_address3", street_address3);
      bookingComponent.getNonPaymentData().put("cardBillingPostcode", cardBillingPostcode);
      bookingComponent.getNonPaymentData().put("selectedCountryCode", selectedCountryCode);
   }

   private Money getTransactionRefundAmount()
   {
      Money transactionRefundAmount = null;
      // Get the deposit type, based on radio button selection
      if (requestParameterMap.get("refund") != null)
      {
         transactionRefundAmount = getPaymentRefundAmount();
      }
      else
      {
         transactionRefundAmount = bookingInfo.getCalculatedTotalAmount();
      }
      return transactionRefundAmount;
   }

   private Money getPaymentRefundAmount()
   {
      Money transactionRefundAmount = null;
      String refundType = requestParameterMap.get("refund").trim();
      Currency currency = bookingComponent.getTotalAmount().getCurrency();
      if (refundType.equalsIgnoreCase("refundamount"))
      {
         LogWriter.logInfoMessage("Full Refund Amount Selected");
         transactionRefundAmount =
        		 new Money(new BigDecimal(bookingComponent.getPayableAmount().getAmount().toString()),
        	               currency);
      }
      else if (refundType.equalsIgnoreCase("partrefundamount"))
      {
         LogWriter.logInfoMessage("Part Refund Amount Selected");
         transactionRefundAmount =
                  new Money(new BigDecimal(requestParameterMap.get("payment_0_transamt")),
                     currency);
      }
      LogWriter.logInfoMessage("transactionRefundAmount : " + transactionRefundAmount);
      return new Money(new BigDecimal("-" + transactionRefundAmount.getAmount()), currency);
   }

}
