/* ##################################### **
THIS FILE INCLUDES ALL JAVASCRIPT FUNCTIONS THAT ARE REQUIRED BY EVERY PAGE 
Date is 17/08/2006
**  ##################################### */

var IE = document.all?true:false;
//if (!IE) document.captureEvents(Event.MOUSEMOVE)
//var navLayers = new Object();//an Object of Nav-Layer elements;
var ie      =  !!document.all && !window.opera;
var ie4     =  !!(document.all &&  !document.documentElement && !window.opera);
var ie5     =  !!(document.all &&  document.documentElement && !window.opera);
var dom     =  !!document.getElementById;
var mac     =  !!(navigator.userAgent.indexOf("Mac")!=-1);
var safari  =  !!(navigator.userAgent.toLowerCase().indexOf("safari")!=-1);
var opera	=  window.opera;


var new_airportlist = new Array();
var currentSeason;

//initialize the variables for the page
function initPage(){

}


/* functions for the layer top navigation */
/* ---------------------------------------------------------------------------------------- *\
	Function name:    showGlobalNav()
	Function purpose: Takes an element id for the global nav and hides all other nav menus
	                  except the input element off the page edge. Displays the input menu
					  and highlights the button text colour
\* ---------------------------------------------------------------------------------------- */

function showGlobalNav(elem) {
	if(document.getElementById(elem+'_sub').style.display!='block') {
		// hide all other global nav lists
		elemArray = document.getElementById('glnav_menu').getElementsByTagName('li');
		for(i=0; i < elemArray.length; i++) {
			if(document.getElementById(elemArray[i].id+'_sub') && (document.getElementById(elemArray[i].id+'_sub') != elem+'_sub')) {
				document.getElementById(elemArray[i].id+'_sub').style.top = '-10000px';
			}
		}
					
		// show this nav list
		document.getElementById(elem+'_sub').style.top = '22px';
		
		//S2I FIX::: Hide selector elements if InternetExplorer
		if(IE){
			//hide language selection
			document.all.langselect_container.style.visibility = 'hidden';
			//hide compact search
			if(document.all.hometable){
				document.all.hometable.style.visibility = 'hidden';
			}
		}
		
		// set main button colour
		mainlinksElem = document.getElementById(elem).getElementsByTagName('div');
		for(i=0; i < mainlinksElem.length; i++) {
			if(mainlinksElem[i].className == 'glnav_mainlink') {
				mainlinksElem[i].className = 'glnav_mainlink_hover';
			}
		}
	}
}

/* ---------------------------------------------------------------------------------------- *\
	Function name:    hideGlobalNav()
	Function purpose: Hides the input element id's associated menu off the screen edge and
	                  turns off button highlighting for the element
\* ---------------------------------------------------------------------------------------- */

function hideGlobalNav(elem) {
	// hide popup off screen edge
	document.getElementById(elem+'_sub').style.top = '-10000px';
			
	// set main button colour
	mainlinksElem = document.getElementById(elem).getElementsByTagName('div');
	for(i=0; i < mainlinksElem.length; i++) {
		if(mainlinksElem[i].className == 'glnav_mainlink_hover') {
			mainlinksElem[i].className = 'glnav_mainlink';
		}
	}
	
	//S2I FIX::: showe hidden selector elements if InternetExplorer
	if(IE){
		//hide language selection
		if(document.all.langselect_container.style.visibility == 'hidden'){
			document.all.langselect_container.style.visibility = 'visible';
		}
		//hide compact search
		if(document.all.hometable && document.all.hometable.style.visibility == 'hidden'){
			document.all.hometable.style.visibility = 'visible';
		}
	}
}


function winPopLink(elem) {
	window.open(elem);
	
}
/* end functions for the layer top navigation */


/* ######################### SKYLIGHTS FUNCTIONS #################################### */
function setServertime(_servertime){
	servertime = _servertime;
}			
				
function setDepartureDate(selectedTime){
	var thisDate = selectedTime;
	var thisYear = thisDate.substr(0,4);
	var thisMonth = thisDate.substr(5,2);
	var thisDay = thisDate.substr(6,8);
	var thisHour = thisDate.substr(11,2);
	var thisMinute = thisDate.substr(14,2);
	var thisDateObject = new Date(thisYear,thisMonth,thisDay,thisHour,thisMinute,0);
	//sets the departure date selectors to the right date
	//document.SBform.sector_1_d.value = thisDay;
	//document.SBform.sector_1_m.value = thisMonth;
}

function setReturnDate(selectedTime){
//sets the departure date selectors to the right date
	var thisDate = selectedTime;
	var thisYear = thisDate.substr(0,4);
	var thisMonth = thisDate.substr(5,2) - 1;
	var thisDay = thisDate.substr(8,2);
	var thisHour = thisDate.substr(11,2);
	var thisMinute = thisDate.substr(14,2);
	var thisDateObject = new Date(thisYear,thisMonth,thisDay,thisHour,thisMinute,0);
	//document.SBform.sector_2_d.value = thisDay;
	//document.all.SBform.sector_2_m.value = thisMonth;
	//alert("setting to "+selectedTime);
}

function setTripType(selectedType){
//sets the trip type to the right date
	if(document.all){
		if(document.all.selectRoundTrip){
			if(selectedType=="singleTrip"){
				document.all.selectRoundTrip.checked = false;
			}
			else{
				document.all.selectRoundTrip.checked = true;
			}
			/* now set the style of the weekday selector for IE */
			document.all.SBform.sector_2_weekday.style.background = 'transparent';
			document.all.SBform.sector_1_weekday.style.background = 'transparent';
		}		
	}
		
	else{
		if(document.getElementById('selectRoundTrip')){
			if(selectedType=="singleTrip"){
				document.getElementById('selectRoundTrip').checked = false;
			}
			else{// not selected yet, set round trip as selected
				document.getElementById('selectRoundTrip').checked = true;
			}
		}
	}
}

function enableSector2(){
	//enables the second date selector on the compact seach form
	document.SBform.sector_2_d.disabled = false;
	document.SBform.sector_2_m.disabled = false;
	document.SBform.sector_2_d.selectedIndex = document.SBform.sector_1_d.selectedIndex+1;
	document.SBform.sector_2_m.selectedIndex = document.SBform.sector_1_m.selectedIndex+1;
	
	//show the correct weekday again
	if(document.all){
		document.all.SBform.sector_2_weekday.style.visibility = 'visible';
		/* currentDate gets defined in SB_Search_Compact.wld */
		//document.SBform.sector_2_d.value = "---";
		//document.SBform.sector_2_m.value = "---";
		//setReturnDate(currentDate);
	}
	
	else{
		document.getElementById('sector_2_weekday').style.visibility = 'visible';
	}
}

//disables the second date selector on the compact search form
function disableSector2(){
	document.SBform.sector_2_d.disabled = true;
	document.SBform.sector_2_m.disabled = true;
	document.SBform.sector_2_d.selectedIndex = 0;
	document.SBform.sector_2_m.selectedIndex = 0;
	
	if(document.all){
		//document.all.SBform.weekDayDisplayField2.style.visibility = 'hidden';
		document.all.SBform.sector_2_weekday.style.visibility = 'hidden';
		//document.all.calendar2_icon.onClick="";
	}
	
	else{
		//document.getElementById('weekDayDisplayField2').style.visibility = 'hidden';
		document.getElementById('sector_2_weekday').style.visibility = 'hidden';
	}
	//document.SBform.sector_2_weekday.value = "test";	
}

function startup()
{
// nothing to do
}

//sets the display state of a container to "block"
function displayContainer(containerId){
	//IE
	if(document.all){
		document.all[containerId].style.display = "block";
	}
	//DOM
	else{
		document.getElementById(containerId).style.display = "block";
	}
}    

//sets the display state of a container to "none"
function hideContainer(containerId){
	//alert("hiding "+containerId);
	//IE
	if(document.all){
		document.all[containerId].style.display = "none";
	}
	//DOM
	else{
		document.getElementById(containerId).style.display = "none";
	}
}

function toggleContainerVisibility(showId, hideId){
    //will toggle two containers on/off
	displayContainer(showId);
	//document.getElementById(showId).style.display = "block";
    hideContainer(hideId);
	//document.getElementById(hideId).style.display = "none";
}

//changes the selected value in the language selector
function setActiveLanguage(langCode){
	for( var i = 0; i < document.langSelectForm.langSelector.length; i++){
		if(document.langSelectForm.langSelector[i].id.indexOf(langCode) > 0){
			document.langSelectForm.langSelector.selectedIndex = i;
		}
	}
}


//C3 funcitons
function BlissSwitch(formname, skylightsCallcenterUrl, blissCallcenterUrl, errorWrongPNR, errorNothingSelected){
	var changeMode = "";
	var changeOption = "";
	
	//alert(document[formname].pnrNumber.value.length);
	
	if(document[formname].pnrNumber.value.length==6){
		changeMode = "skylights";
	}
	
	//find out if PNR is a BLISS PNR
	else if(document[formname].pnrNumber.value.length>6){
		changeMode = "bliss";
	}
	
	//PNR is wrong!
	else{
		alert(errorWrongPNR);
		return;
	}
	
	//find out selected option
	for(var i=0; i<document[formname].changeOption.length; i++){
		if(document[formname].changeOption[i].checked){
			changeOption = 	document[formname].changeOption[i].value;
		}
	}
	
	if(changeOption.length<1){
		alert(errorNothingSelected);
		return;
	}

	//open jaw change process
	if(changeMode == "bliss"){
		//alert("this change is handled by BLISS! ");
						
		switch(changeOption){		
			/*case "changeSeats":
			case "changeMeals":
				alert("Bliss deeplink!");
			break;
			*/
			default:
				//redirect to error message call center
				//alert("Bliss eror!");
				document.location.href=blissCallcenterUrl;
			break;
		
		}
	}
	
	//skylights change process
	else{
		switch(changeOption){
			case "changePassenger":
			case "changeInsurance":
			case "changeMeals":
				//alert("Skylights Callcenter Message");
				document.location.href=skylightsCallcenterUrl;
			break;
			
			default:
				//alert("redirecting to Skylights change process!");
				document[formname].submit();
			break;
		}
		//alert("this change is handled by Skylights! ");
	}
}

function reorderSeasonList(){
	for(var k in airports){
		if(k<3){
			//alert(airports[k]);
		}
	}
	//alert("reorderng seasons alphabetically!"+airports);
	for(var i = 0; i<seasons.length; i++){
		//alert(seasons[i].routes.length);
		//i = Numer of seasons
//		alert(seasons[i].routes["ALC"].length);
		for(var j in seasons[i].routes){
			//alert(seasons[i][j]);
			//alert(j);
			
			
			
		}
	}
}


function makeCookie(cookieName,cookieValue,nTime, timeIndex) {
	//alert("setting cookie to "+cookieValue);
	var currentDomain= window.location.hostname;
	var today = new Date();
 	var expire = new Date();
	if(timeIndex == null)timeIndex = "days";
 	if (nTime==null || nTime==0) nTime=1;

	switch(timeIndex){
		case "seconds":
			expire.setTime(today.getTime() + 1000*nTime);
		break;
		case "minutes":
			expire.setTime(today.getTime() + 60000*nTime);
		break;
		case "hours":
			expire.setTime(today.getTime() + 3600000*nTime);
		break;

		case "days":
		default:
		 	expire.setTime(today.getTime() + 3600000*24*nTime);
		break;
	}
	//alert("cookie expires in "+nTime+" "+timeIndex);
	document.cookie = cookieName+"="+escape(cookieValue)+ ";expires="+expire.toGMTString()+";path=/;"+"domain="+currentDomain+";";
}


function getCookie(cookieName) {
//alert("getting cookie!");
 var cookieStartTime, cookieEndTime;
 if (document.cookie) {
  cookieStartTime = document.cookie.indexOf(cookieName+"=");
  if (cookieStartTime < 0) return null;
  cookieStartTime = document.cookie.indexOf("=", cookieStartTime) + 1;
  cookieEndTime = document.cookie.indexOf(";", cookieStartTime);
  if (cookieEndTime < 0) cookieEndTime = document.cookie.length;
  return unescape(document.cookie.substring(cookieStartTime, cookieEndTime));
 }
 else return null;
}

function prefillLimitedAirportlistForm(){
	if(document.cookie){
		//a value has been previuosly detected
		airport_cookie = getCookie("airportSelectCookie");
		
		if(airport_cookie){
			cookie_values = airport_cookie.split(";");
			var selected_origin_airport = cookie_values[0];
			var selected_destination_airport = cookie_values[1];
			var selected_departingDate = cookie_values[3];
			var selected_returningDate = cookie_values[4];
			var tripType = cookie_values[2];
			var selectedAdults = cookie_values[5];
			var selectedChildren = cookie_values[6];
			var selectedInfants = cookie_values[7];
			
			switch(tripType){
				case "oneWay":
					document.SBform.travel_type[1].checked = true;
					document.SBform.travel_type[0].checked = false;		
					disableSector2();		
				break;
				case "roundTrip":
					document.SBform.travel_type[1].checked = false;
					document.SBform.travel_type[0].checked = true;
					enableSector2();
				default:
				break;
			}
			
			//select previously selected departing day
				for(var i=0; i < document.SBform.sector_1_d.length; i++){
					if(document.SBform.sector_1_d[i].value == selected_departingDate.substring(0,2)){
						document.SBform.sector_1_d[i].selected = true;
					}
				}
				
			//select previously selected departing month
				for(var i=0; i < document.SBform.sector_1_m.length; i++){
					if(document.SBform.sector_1_m[i].value == selected_departingDate.substring(2,8)){
						document.SBform.sector_1_m[i].selected = true;
					}
				}
			
		
			
			//if one-way flight: set the other date selectors to "---"
			if(tripType=="oneWay"){
				document.SBform.sector_2_m.selectedIndex = 0;
				document.SBform.sector_2_d.selectedIndex = 0;		
			}
			
			
			else if(tripType=="roundTrip"){
				//select previously selected returning day
				for(var i=0; i < document.SBform.sector_2_d.length; i++){
					if(document.SBform.sector_2_d[i].value == selected_returningDate.substring(0,2)){
						document.SBform.sector_2_d[i].selected = true;
					}
				}
				
				//select previously selected returning month
				for(var i=0; i < document.SBform.sector_2_m.length; i++){
					if(document.SBform.sector_2_m[i].value == selected_returningDate.substring(2,8)){
						document.SBform.sector_2_m[i].selected = true;
					}
				}
			}
			
			//assign previously selected passenger numbers
			//1. Adults
			for(var i=0; i < document.SBform.ADULT.length; i++){
				if(document.SBform.ADULT[i].value == selectedAdults){
					document.SBform.ADULT[i].selected = true;
				}
			}
	
			//2. Children
			for(var i=0; i < document.SBform.CHILD.length; i++){
				if(document.SBform.CHILD[i].value == selectedChildren){
					document.SBform.CHILD[i].selected = true;
				}
			}
			
			//3. Infants
			for(var i=0; i < document.SBform.INFANT.length; i++){
				if(document.SBform.INFANT[i].value == selectedInfants){
					document.SBform.INFANT[i].selected = true;
				}
			}			
			
			//rebuild the limited airport lists
			currentSeason = getSeason();
			//fillOrigList(currentSeason, selected_origin_airport);	
			fillLimitedDestinationList(document.SBform.sector1_o, document.SBform.sector1_d);	
			//fillDestList(currentSeason, selected_origin_airport);
			
			for(var i = 0; i < document.SBform.sector1_d.length; i++){
				if(document.SBform.sector1_d.options[i].value.substring(0, 3) == selected_destination_airport.substring(0, 3)){
					document.SBform.sector1_d.selectedIndex = i;
				}
			}
						
			//preselect the previously selected airport
				
			//fillDestination(document.SBform.sector1_o.options[document.SBform.sector1_o.selectedIndex].value);	
		}
	}
}

function prefillSearchForm(){
	if(document.cookie){
		airport_cookie = getCookie("airportSelectCookie");
		if(airport_cookie){
			cookie_values = airport_cookie.split(";");
			var selected_origin_airport = cookie_values[0];
			var selected_destination_airport = cookie_values[1];
			var selected_departingDate = cookie_values[3];
			var selected_returningDate = cookie_values[4];
			var tripType = cookie_values[2];
			var selectedAdults = cookie_values[5];
			var selectedChildren = cookie_values[6];
			var selectedInfants = cookie_values[7];
			
			switch(tripType){
				case "oneWay":
					document.SBform.travel_type[1].checked = true;
					document.SBform.travel_type[0].checked = false;		
					disableSector2();		
				break;
				case "roundTrip":
					document.SBform.travel_type[1].checked = false;
					document.SBform.travel_type[0].checked = true;
					enableSector2();
				default:
				break;
			}
			
			//select previously selected departing day
				for(var i=0; i < document.SBform.sector_1_d.length; i++){
					if(document.SBform.sector_1_d[i].value == selected_departingDate.substring(0,2)){
						document.SBform.sector_1_d[i].selected = true;
					}
				}
				
			//select previously selected departing month
				for(var i=0; i < document.SBform.sector_1_m.length; i++){
					if(document.SBform.sector_1_m[i].value == selected_departingDate.substring(2,8)){
						document.SBform.sector_1_m[i].selected = true;
					}
				}
			
		
			
			//if one-way flight: set the other date selectors to "---"
			if(tripType=="oneWay"){
				document.SBform.sector_2_m.selectedIndex = 0;
				document.SBform.sector_2_d.selectedIndex = 0;		
			}
			
			
			else if(tripType=="roundTrip"){
				//select previously selected returning day
				for(var i=0; i < document.SBform.sector_2_d.length; i++){
					if(document.SBform.sector_2_d[i].value == selected_returningDate.substring(0,2)){
						document.SBform.sector_2_d[i].selected = true;
					}
				}
				
				//select previously selected returning month
				for(var i=0; i < document.SBform.sector_2_m.length; i++){
					if(document.SBform.sector_2_m[i].value == selected_returningDate.substring(2,8)){
						document.SBform.sector_2_m[i].selected = true;
					}
				}
			}
			
			//assign previously selected passenger numbers
			//1. Adults
			for(var i=0; i < document.SBform.ADULT.length; i++){
				if(document.SBform.ADULT[i].value == selectedAdults){
					document.SBform.ADULT[i].selected = true;
				}
			}
	
			//2. Children
			for(var i=0; i < document.SBform.CHILD.length; i++){
				if(document.SBform.CHILD[i].value == selectedChildren){
					document.SBform.CHILD[i].selected = true;
				}
			}
			
			//3. Infants
			for(var i=0; i < document.SBform.INFANT.length; i++){
				if(document.SBform.INFANT[i].value == selectedInfants){
					document.SBform.INFANT[i].selected = true;
				}
			}			
			
			//rebuild the airport lists
			currentSeason = getSeason();
			fillOrigList(currentSeason, selected_origin_airport);
			fillDestList(currentSeason, selected_origin_airport);
			
			for(var i = 0; i < document.SBform.sector1_d.length; i++){
				if(document.SBform.sector1_d.options[i].value.substring(0, 3) == selected_destination_airport.substring(0, 3)){
					document.SBform.sector1_d.selectedIndex = i;
				}
			}
						
			//preselect the previously selected airport
				
			//fillDestination(document.SBform.sector1_o.options[document.SBform.sector1_o.selectedIndex].value);	
		}
	}
}

function prefillSearchForm2(travelType){
	//this is called if the form is prepopulated... the return date will be set to the depareture date
	//alert("skylights prefill!");
	if(travelType == 1){
		//single
		document.SBform.sector_2_m.disabled = true;
		document.SBform.sector_2_d.disabled = true;
		document.SBform.sector_2_m.selectedIndex = 0;
		document.SBform.sector_2_d.selectedIndex = 0;		
	}
	/*else{
		//return
		document.SBform.sector_2_m.disabled = false;
		document.SBform.sector_2_d.disabled = false;
		document.SBform.sector_2_m.selectedIndex = document.SBform.sector_1_m.selectedIndex+1;
		document.SBform.sector_2_d.selectedIndex = document.SBform.sector_1_d.selectedIndex+1;
	}*/
}

function initSearchForm(){
	currentSeason = getSeason();
	// 2nd set one trip type as selected
	setTripType('roundTrip');
	reconcile_dates();
	var thisCarrier = "";
	
	prefillSearchForm();
	
	if(document.all){
		document.getElementById('loadMessage').style.display = 'none';
	}
	document.getElementById('hometable').style.visibility = 'visible';
	
}

function filterC3Dates(formobject){
//filter all foreign carriers
//alert("filtering");

}

function initC3(){
}

function addAirports(targetElement, airportList){
	var optioncount = 1;
	var airportOption = "";

	for(var i = 0; i < airportList.length; i++){
		 airportOption = new Option(airportList[i].city + " (" + airportList[i].code + ")");
		 targetElement.options[optioncount]		= airportOption;
		 optioncount++;		 		 
	}
}

function filterC3List(selector){
	//1. get the season
	var inList = false;
	var optioncount = 1;
	var c3airports = new Array();
	var addAirport = false;
	var current_origin = selector.options[selector.selectedIndex];
	if(current_origin){}
	
	
	//gehe durch alle Airports und stelle jeweils einen dar, sofern der Airport Code in der Liste der aktuellen Season vorkommt
	//var mySeason = getSeason();
	for(var k = 0; k< new_airportlist.length; k++){
		var current_code = new_airportlist[k].code;
		for(var j = 0; j<seasons.length; j++){
			for(var airportCode in seasons[j].routes){
				if(airportCode == current_code){
					//look if it is already here
					var alreadyInList = false;
					for(var n=0; n<c3airports.length; n++){
						if(c3airports[n]== current_code){
							alreadyInList = true;
						}
						else{
							c3airports.push(current_code);
							var city = airports[airportCode];
							var newOption = new Option(city+ " (" + airportCode + ")");
							selector.options[optioncount] = newOption;
							selector.options[optioncount].value	= airportCode;
							optioncount++;
						}
					}					
				}
			}
		}
	}
}

//submits a form via https
function submitSecure(form){
	var formAction = form.action;
	if(document.all){
		var hostname = window.location.hostname;
		if(formAction.indexOf("http")== -1){
			//must add a protocol and a hostname
			formAction = "https://"+hostname+formAction;
			
		}
	}
	else{
		if(formAction.indexOf("https")== -1){
			formAction = formAction.replace(/http/, "https");
		}
	}
	form.action = formAction;	
	form.submit();
}

//INTELLITRACKER ENCODE FUNCTION
function completeEncodeURIComponent(uri) {
	uri = encodeURIComponent(uri);
	var newUri = uri.replace(/\./g, "%2E");
	return newUri;
}

//this function replaces the Newsletter Link on every Page
 function replaceNlLink(nlLink, languageVar){
     if(!languageVar){
         languageVar = 'ENG';
     }
  
     var nlUrls = new Object();
     var currentLinkContent = "";
     var newLinkContent = "";
     var currentOnClickBlock = "";
 
    nlUrls.en = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=en";
    nlUrls.cz = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=cz";
    nlUrls.de = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=de";
    nlUrls.fr = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=fr";
    nlUrls.it = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=it";
    nlUrls.nl = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=nl";
    nlUrls.at = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=de";
    nlUrls.es = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=es";
     
     if(document.all){
         var linkItem = document.all[nlLink];
     }
     else{
         var linkItem = document.getElementById(nlLink);
     }
	
     for(var i in nlUrls){  
         if(i == languageVar){
            nlPopupLink = nlUrls[i];
         }
     }
    
     currentLinkContent = linkItem.innerHTML;
	 try{
	 var pattern = /(.*onclick=\"window.open\(\')[^\']*(\'.*)/;
	  var matchresult = pattern.exec(currentLinkContent);
	  newLinkContent = matchresult[1] + nlPopupLink + matchresult[2];
	  linkItem.innerHTML = newLinkContent;
	 }
	 catch(e){ }  
 }
 
 
 function openNewsletter(culture){
 	var popupType = 'save';
	// alert(culture);
	var nlUrls = new Object();
	nlUrls.en = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=en";
	nlUrls.cz = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=cz";
	nlUrls.de = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=de";
	nlUrls.fr = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=fr";
	nlUrls.it = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=it";
	nlUrls.nl = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=nl";
	nlUrls.at = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=de";
	nlUrls.es = "http://access2.creatormail.co.uk/tui/TUI.Forms/ThomsonFly/Default.aspx?culture=es";
	
	 if(document.all){
         var linkItem = document.all['nl_link'];
     }
     else{
         var linkItem = document.getElementById('nl_link');
     }
	
     for(var i in nlUrls){  
         if(i == culture){
            nlPopupLink = nlUrls[i];
         }
     }
	
	
	 popupWidth = "650"
	 popupHeight = "540"
	 if(screen.width){
	 	var winl = (screen.width-popupWidth)/2;
		var wint = ((screen.height-popupHeight)/2)-28;
	 }
	 else{
	 	winl = 0;wint =0;
	 }
	 if (winl < 0) winl = 0;
	 if (wint < 0) wint = 0;
	 var settings = 'height=' + popupHeight + ',';
	 settings += 'width=' + popupWidth + ',';
	 settings += 'top=' + wint + ',';
	 settings += 'left=' + winl + ',';
	 settings += 'resizable=no,scrollbars=yes';
	
	if (document.referrer.indexOf("google") == -1) {
	
		if (document.location.href.indexOf("FID=NL") != -1){
			win = window.open(nlPopupLink,"flyout",settings);
		}
		else if (document.location.href.indexOf("thomsonfly") == -1){
			return;
		}
		else{
			if (popupType == 'save'){
				//if (document.cookie.indexOf('save=true')==-1 && document.referrer.indexOf('google')==-1){
				if (document.cookie.indexOf('save=true')==-1){
					setCookie(popupType);
					//alert("SAVE "+document.cookie);
					win = window.open(nlPopupLink,"flyout",settings);
					//    win.window.focus();
				}
			} 
		}
	}
	else {
		return;
	}
}
 
 function openNewsletterPopup(newin,popupType) {
	 popupWidth = "450"
	 popupHeight = "500"
	 if(screen.width){
	  var winl = (screen.width-popupWidth)/2;
	  var wint = ((screen.height-popupHeight)/2)-28;
	 }
	 else{winl = 0;wint =0;
	 }
	 if (winl < 0) winl = 0;
	 if (wint < 0) wint = 0;
	 var settings = 'height=' + popupHeight + ',';
	 settings += 'width=' + popupWidth + ',';
	 settings += 'top=' + wint + ',';
	 settings += 'left=' + winl + ',';
	 settings += 'resizable=no,scrollbars=yes';
	if (document.location.href.indexOf("FID=NL") != -1){
	win = window.open(newin,"flyout",settings);
	}
	else if (document.location.href.indexOf("thomsonfly") == -1)
	{return;
	}
	else
	{
	if (popupType == 'save') {
	    //if (document.cookie.indexOf('save=true')==-1 && document.referrer.indexOf('google')==-1){
	    if (document.cookie.indexOf('save=true')==-1 && document.location.href.indexOf('referrer=google')==-1){
	        setCookie(popupType);
	    //alert("SAVE "+document.cookie);
	        win = window.open(newin,"flyout",settings);
	//    win.window.focus();
	  }
	 } 
	}
}

function myPrint(){
 if (navigator.userAgent.indexOf("Mac") == -1){
 window.print();
 }
 else{
 alert("Please use the functionality of your Browser.");
 }
}

function erase (element, defaultValue)
{
  if (element.value == defaultValue) element.value = '';
}

function set (element, defaultValue)
{
  if (element.value == '') element.value = defaultValue;
}
