
$j(document).ready(function(){
  // stylesheets to be loaded only when javascript is enabled
  loadStylesheet("/cms-cps/atcomres/css/style_js.css", "screen, print");

  toggleOverlay();
  toggleFields("select#payment_type_0", ".maestroGroup", "Switch");
  targetBlank();
});

function toggleFields(owner, toggleField, testStr){

  $j(owner).change(function(){
  toggleGroup = $j(toggleField);
  var chosenoption= $j(owner+' option:selected');


    for (var i=0; i<toggleGroup.length; i++)
    {
      if(chosenoption.val() == "Switch" || chosenoption.val() == "Solo"){
        $j(toggleGroup[i]).show();
      }else{
        $j(toggleGroup[i]).hide();
      }
    }
    return false;
  });

}

function loadStylesheet( filename, media ){
  // ensure that the browser has all the DOM methods/properties we need
  if( !(document.getElementById) ||
    !(document.childNodes) ||
    !(document.createElement) ||
    !(document.getElementsByTagName) ){
    return;
  }

  // make a new link node to the stylesheet
  var link = document.createElement( "link" );
  link.href = filename;
  link.rel = "stylesheet";
  link.type = "text/css";
  link.media = media;


  // insert the stylesheet link into the document head
  document.getElementsByTagName('head')[0].appendChild(link);
}



function toggleOverlay(){
	  var overlayZIndex = 99;
	  var zIndex = 100;
	  var prevOverlay;
	  var stickyOpened = false;
	  jQuery("a.stickyOwner").click(function(e){
	    var overlay = "#" + this.id + "Overlay";
		if (!stickyOpened)
		{
			prevOverlay = overlay;
		}
		if (prevOverlay != overlay)
		{
			jQuery(prevOverlay).hide();
			stickyOpened = false;
		}
		var pos = jQuery("#"+this.id).offset();
		jQuery(overlay).show();
		prevOverlay = overlay;
		stickyOpened = true;
		jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
		zIndex++;

		if (jQuery(overlay).parent(".overlay") != null){
		  jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
		  overlayZIndex++;
		}
	    return false;
	  });

	  jQuery("a.close").click(function(){
	    var overlay = this.id.replace("Close","Overlay");
	    jQuery("#" + overlay).hide();
	    return false;
	  });
}


//This method allows generic detection of all pop-up links
function targetBlank() {
  if (!document.getElementsByTagName) return;

  var anchors = $j("a.popUplink");
  for (var i=0; i<anchors.length; i++)
  {
    var anchor = anchors[i];
    if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external")
    {

      var theScript = 'JavaScript:void(openWindow("' + anchor.getAttribute('href') + '"));';

      anchor.setAttribute("href", theScript);

      // We don't want the target attribute as this will cause problems with the script.
      anchor.removeAttribute("target");
    }
  }
}

  $j(".genericOverlay").hide();


function openWindow(href) {
    var win = window.open(href,"","width=700,height=600,scrollbars=yes,resizable=yes");
    if (win && win.focus) win.focus();
}

function openPopupCustomWindow(href,height,width,scroll,resizing) {
    var win = window.open(href,"","width="+width+",height="+height+",scrollbars="+scroll+",resizable="+resizing);
    if (win && win.focus) win.focus();
}
