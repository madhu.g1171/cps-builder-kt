(function(win, jQuery) {
	rf = window.rf || {};
	rf = {
		init: function() {
			jQuery(document).ready(function(){
				var $refund = jQuery('.refundamt li');
				rf.refundPart();
				rf.transUpdate();
				rf.rfValidate();
				rf.updateFullRf($refund[0]);
			});
		},
		refundPart: function() {
			var $refund = jQuery('.refundamt li'),
				$paypart = jQuery('.refundamt .paypart');

			$refund.live('click',function() {
				jQuery(this).find('input[type="radio"]').attr('checked','checked');
				var chkd = jQuery('.refundamt .partpayment input[type="radio"]').is(":checked");
				if(chkd){
					$paypart.removeClass('hide');
					rf.updateOthrRf($paypart);
				}else{
					$paypart.addClass('hide');
					rf.transUpdate();
					rf.updateFullRf(this);
				}
			});
		},
		transUpdate: function(rfAmt) {
			var $refund = jQuery('.refundamt li');
				chkdamt = parseFloat(rfAmt) || jQuery.trim($refund.find('input:checked').closest('li').find('.amt').text().replace(/,/g, '')),
				$tranLst = jQuery('.transaction-table tbody tr'),
				highLtrow = [];
			$tranLst.each(function(){
				var transamt = jQuery.trim(jQuery(this).find('.amt').text().replace(/,/g, '')),
					status = jQuery.trim(jQuery(this).find('.status').text());

				if(parseFloat(transamt) < parseFloat(chkdamt)){
					jQuery(this).addClass('disable');
					jQuery(this).find('input[type="radio"]').attr('disabled','true');
				}else {
					jQuery(this).removeClass('disable');
					jQuery(this).find('input[type="radio"]').removeAttr('disabled');
					highLtrow.push(jQuery(this));
					if(highLtrow.length>0){
						highLtrow[0].find('input[type="radio"]').attr('checked', true);
						rf.updateCardData(highLtrow[0]);
					}

				}

				if(status=='Expired'){
					jQuery(this).addClass('disable');
					jQuery(this).find('input[type="radio"]').attr('disabled','true');
				}
			});

			jQuery('.transaction-table tbody tr').click(function (){
				if(!jQuery(this).hasClass('disable')){
					jQuery(this).find('input[type="radio"]').removeAttr('disabled');
					jQuery(this).find('input[type="radio"]').attr('checked', true);
					rf.updateCardData(this);
				}
			});
		},
		updateOthrRf: function($paypart) {
			var partAmt = $paypart.find('input'),
				$amt = jQuery('.paysummarylist .amt');
				numRe = /^-?\d+\.?\d*$/,
				fullamt = parseFloat(jQuery.trim(jQuery('.refundamt').find('.amt').text().replace(/,/g, '')));


			/*
			partAmt.live('change',function(){ console.log(this.id);
				var baseAmt  = parseFloat(jQuery('.refundsummary .baseamt').text().replace(/,/g, '')),
					rfAmt = parseFloat(jQuery('.refundsummary .rfundamt').text().replace(/,/g, '')),
					dueAmt = jQuery('.refundsummary .dueamt'),
					$payrfamt = jQuery('.payrfamt');
				if(numRe.test(partAmt.val()) && partAmt.val() < fullamt){
					$amt.text(partAmt.val());
					rf.transUpdate(partAmt.val());
					dueAmt.text(parseFloat(baseAmt-rfAmt).toFixed(2));
					jQuery("#stilDue_Amt").val(parseFloat(baseAmt-rfAmt).toFixed(2));
					$payrfamt.val(rfAmt);

					document.getElementById("refundPartAmount").value = rfAmt;
				}

			}); */

			bindEvent(document.getElementById('partRefundAmtId'), 'change', function () {
			  var baseAmt  = parseFloat(jQuery('.refundsummary .baseamt').text().replace(/,/g, '')),
					rfAmt = parseFloat(jQuery('.refundsummary .rfundamt').text().replace(/,/g, '')),
					dueAmt = jQuery('.refundsummary .dueamt'),
					$payrfamt = jQuery('.payrfamt');
				if(numRe.test(partAmt.val()) && partAmt.val() <= fullamt){
					$amt.text(partAmt.val());
					rf.transUpdate(partAmt.val());
					dueAmt.text(parseFloat(baseAmt-rfAmt).toFixed(2));
					jQuery("#stilDue_Amt").val(parseFloat(baseAmt-rfAmt).toFixed(2));
					$payrfamt.val(rfAmt);

					document.getElementById("refundPartAmount").value = rfAmt;
				}
			});



		},
		updateFullRf: function(that) {
			var fullamt = jQuery.trim(jQuery(that).find('.amt').text().replace(/,/g, '')),
				$amt = jQuery('.paysummarylist .amt'),
				numRe = /^-?\d+\.?\d*$/,
				baseAmt  = jQuery('.refundsummary .baseamt'),
				rfAmt = jQuery('.refundsummary .rfundamt'),
				dueAmt = jQuery('.refundsummary .dueamt'),
				$payrfamt = jQuery('.payrfamt');

			jQuery('.rf .partamt').val('');
			if(numRe.test(fullamt)){
				$amt.text(fullamt);
				$payrfamt.val(fullamt);
				dueAmt.text(parseFloat(baseAmt.text().replace(/,/g, ''))-parseFloat(rfAmt.text().replace(/,/g, '')));
				jQuery("#stilDue_Amt").val(parseFloat(baseAmt.text().replace(/,/g, ''))-parseFloat(rfAmt.text().replace(/,/g, '')));
				document.getElementById("payment_0_transamt").value = fullamt;
				document.getElementById("refundPartAmount").value = "" ;

			}
		},
		tc:false,
		partamt:"test",
		updateCardData:function(thisrow){
			var $selctd = jQuery(thisrow),
				nameoncard = jQuery.trim($selctd.find('.payee').text()),
				cardno = jQuery.trim($selctd.find('.cardno').text()),
				cardType = jQuery.trim($selctd.find('.payType').text()),
				datacashRef = jQuery.trim($selctd.find('.datacash-ref').text()),
				paymentDate = jQuery.trim($selctd.find('.paydate').text()),
				expiryDate = jQuery.trim($selctd.find('.expiry').attr('expdt')),
				$cardData = jQuery('.carddata');

			$cardData.find('.nameoncard').val(nameoncard);
			$cardData.find('.cardNumber').val(cardno);
			$cardData.find('.cardtype').val(cardType);
			$cardData.find('.datacashrefid').val(datacashRef);
			$cardData.find('.paymentDate').val(paymentDate);
			$cardData.find('.expiryDate').val(expiryDate);
		},
		rfValidate: function() {
			jQuery('.rf #refundpayment').click(function(e) {
				e.preventDefault();
				jQuery('.rf .partamt, .rf .tandc').blur();
				// if form fields are valid then submit
				if(rf.tc){
					if(rf.partamt=='test'){
						jQuery('.rf form#refundpayform').submit();
					}else if(rf.partamt){
						jQuery('.rf form#refundpayform').submit();
					}
				}
			});

			jQuery('.rf .partamt, .rf .tandc').blur(function() {
				var numRe = /^-?\d+\.?\d*$/,
					fullamt = parseFloat(jQuery.trim(jQuery('.refundamt').find('.amt').text().replace(/,/g, '')));

				if(jQuery(this).hasClass('partamt') && !jQuery(this).closest('.paypart').hasClass('hide')){
					if(!numRe.test(jQuery(this).val())){
						rf.partamt = false;
						jQuery(this).val(0);
						jQuery(this).trigger('change');
						alert('Please enter valid amount');
					}else{
						if(jQuery(this).val() > fullamt ){
							jQuery(this).val(0);
							jQuery(this).trigger('change');
							rf.partamt = false;
							alert('Please enter amount less than full refund amount');
						}else{
							if(jQuery(this).val()<=0){
								alert('Please enter amount should be more than zero and less than full refund amount');
								jQuery(this).focus();
								rf.partamt = false;
							}else{
								rf.partamt = true;

								document.getElementById("payment_0_transamt").value = jQuery(this).val();
							}
						}
					}
				}

				if(jQuery(this).hasClass('tandc')){
					if(!jQuery('.rf .tandc').is(":checked")){
						rf.tc = false;
						alert('Please accept terms and condition');
					}else{
						rf.tc = true;
					}
				}

				var cardType = jQuery('.cardtype').val(),
					nameOfCard = jQuery('.nameoncard').val(),
					cardNumber = jQuery('.cardNumber').val(),
					pamentDate = new Date(jQuery('.paymentDate').val().replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") ),
					datacashrefid = jQuery('.datacashrefid').val(),
					expiryDate = new Date(jQuery('.expiryDate').val().replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
					todayDate = new Date(),
					nameRegex= /^[a-zA-Z ]*$/,
					charRe=/^[a-zA-Z-_ ]+$/,
					alphaNumeric = /^[a-zA-Z0-9]+$/;

				if(!nameRegex.test(nameOfCard)){
				    rf.tc = false;
				    alert("Payee Name is not valid");
				}
				if(!numRe.test(datacashrefid)){
				    rf.tc = false;
				    alert("Datacash Id is not valid");
				}
				if(!charRe.test(cardType)){
				    rf.tc = false;
				    alert("Card Type is not valid");
				}
				if(pamentDate > todayDate){
				    rf.tc = false;
				    alert("Payment Date should not be the Future Date");
				}
				if(expiryDate.getFullYear() < todayDate.getFullYear())
				{
			    rf.tc = false;
			    alert("Expiry Date is not valid");
				}
				if(expiryDate.getFullYear() == todayDate.getFullYear())
				if(expiryDate.getMonth() < todayDate.getMonth())
				{
			    rf.tc = false;
			    alert("Expiry Date is not valid");
				}


				if(!(numRe.test(cardNumber) || !charRe.test(cardNumber))){
					   rf.tc = false;
					   alert("Card Number is not valid");
					}
			});
		}
	};

	rf.init();

})(window, jQuery);

function bindEvent(el, eventName, eventHandler) {
  if (el.addEventListener){
    el.addEventListener(eventName, eventHandler, false);
  } else if (el.attachEvent){
    el.attachEvent('on'+eventName, eventHandler);
  }
}
