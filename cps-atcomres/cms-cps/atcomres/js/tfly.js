var submitterClicked = false;
var request;

$j("document").ready(function(e){
	summaryPanelOverlay();
	});
function summaryPanelOverlay()
{
 $j("#taxTerms").click(function(e)
 {
	var pos = $j(".taxesCharges").position();
	var height = $j(".taxesCharges").height();
	var top = parseInt((1*pos.top),10);
	$j("#taxTermsOverlay").css("top",top+height);
	return false;
 });}

function validateRequiredField()
{

   return true
      && passengerNamesInputValidation()
      && personalDetailsInputValidation()
      && paymentDetails_Validate()
      && contactAgreement_Validate()
	   && issuefieldvalidate();
}

function preventDoubleClick()
{
   if (!submitterClicked)
   {
      submitterClicked = true;
      return true;
   }

   return false;
}

function passengerNamesInputValidation(ValidateEventArgs)
{
   if (!validate('passenger'))
   {
      return false;
   }
   return true;
}

function personalDetailsInputValidation(ValidateEventArgs)
{
   if (!validate('personaldetails'))
   {
      return false;
   }
   return true;
}

function paymentDetails_Validate(ValidateEventArgs)
{
   if (!validate('payment'))
   {
      return false;
   }
  var expiryMonth = parseInt(document.getElementById('payment_expiryDateMonth').value,10);
   var expiryYear = parseInt(document.getElementById('payment_expiryDateYear').value,10);

   var ns4 = (document.layers);
   var ns6 = (!document.all && document.getElementById);

   var today = new Date();
   currentMonth = parseInt(today.getMonth() + 1);
   var calib=(ns4 || ns6)?1900:0;
   currentYear = today.getYear() + calib;
   currentYear = currentYear + '';
   currentYear = currentYear.substring(2);
   currentYear = parseInt(currentYear);
   if (((expiryMonth < currentMonth) && (expiryYear == currentYear)))
   {
      alert("The expiry date specified has already passed.");
	  document.getElementById("payment_expiryDateMonth").focus();
      return false;
   }
   else if (expiryYear < currentYear)
   {
      alert("The expiry date specified has already passed.");
	  document.getElementById("payment_expiryDateYear").focus();
      return false;
   }

   return true;
}

function contactAgreement_Validate(ValidateEventArgs)
{
   if (!document['SkySales']['termAndCondition'].checked)
   {
      alert("\nSorry, but to proceed with your booking you\nmust click checkbox to accept our \nTerms and conditions.\n");
	  document.getElementById("termAndCondition").focus();
      return false;
   }
   return true;
}
function askForPayment()
{
   return confirm("Thank you.\n\nWe are now ready to complete your reservation.\n\nPlease do not click the Pay, Refresh, Back or Stop buttons until the next page is displayed.\n\nIt may take up to 45 seconds to validate your payment information.  Thank you for your patience.\n\nAll booking details must be correct before proceeding. Please click the 'OK' button to begin authorisation.\n\nYour booking will then be final and not reversible.");
}
 var issueflag=false;
function formSubmit()
{
	document.getElementById("tourOperatorTermsAccepted").value = document.getElementById("termAndCondition").value;
  // document.getElementById('dayTimePhone').value = document.getElementById('personaldetails_phoneNumber').value ;

}

function updateEssentialFields()
{
   var totalAmountPaid = parseFloat(document.getElementById("payment_0_transamt").value+parseFloat(document.getElementById("payment_0_chargeIdAmount").value)).toFixed(2);
   document.getElementById("payment_0_transamt").value = document.getElementById("payment_totamtpaid").value;
   document.getElementById('payment_0_paymentmethod').value = "Dcard";
   document.getElementById('payment_0_totalTrans').value = 1;
}

function filterPhoneField(inputField)
{
   inputField.value = inputField.value.replace(/[^0-9]/g, "");
}



/*DP function feesTaxes_breakDowns(taxDetails)
{
	var msg = '';
	msg = "Here is a breakdown of the taxes and fees\nthat apply to your purchase." + '\n\n\n';
	alert(msg + taxDetails);
}*/

/* updating card charges*/

function updateCardChargeForTfly()
{

	changePayButton($('payment_type_0').value);
 if(PaymentInfo.totalAmount>0)
  {
	  handleDotNetCardSelection(0);
  }
  displayFieldsWithValuesForCard();
}
/*updating total cost */
function displayFieldsWithValuesForCard()
{

		   if(PaymentInfo.selectedCardType!="" && PaymentInfo.selectedCardType!=null  && calCardCharge>0)
		   {

			  document.getElementById("creditcardcontent").style.display="block" ;
			  if(PaymentInfo.chargeamt == 0 || calCardCharge != 0)
			  {
				   $('creditCardSurcharge').innerHTML=cardChargeMap.get(PaymentInfo.selectedCardType).split(",")[4]+" card surcharge";
				   document.getElementById("surcharge").innerHTML= PaymentInfo.currency;

				   $('surcharge').innerHTML+=calCardCharge;
			   }
			   else
			   {

			  	   $('surcharge').innerHTML=PaymentInfo.currency;
			  	   $('surcharge').innerHTML+=PaymentInfo.chargeamt.toFixed(2);
			   }

		   }

		   else
			{
			   	  document.getElementById("creditcardcontent").style.display="none" ;
			    $('creditCardSurcharge').innerHTML="";
				$('surcharge').innerHTML="";
			}


$('totalAmountDue').innerHTML="<b>"+addCommas(PaymentInfo.calculatedTotalAmount)+"</b>";
		  $('calculatedTotalAmount').innerHTML="<b>"+addCommas(PaymentInfo.calculatedTotalAmount)+"</b>";
		  document.getElementById("payment_0_paymenttypecode").value= PaymentInfo.selectedCardType;
		  document.getElementById("total_transamt").value=PaymentInfo.calculatedPayableAmount;


}
 function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
		return x1 + x2;
}
 function setIssueField()
 {
   			 if(PaymentInfo.selectedCardType!='none' && PaymentInfo.selectedCardType != null && PaymentInfo.selectedType != "")
	 {
			  $('payment_0_securityCode').setAttribute('maxlength',getCardDetails(PaymentInfo.selectedCardType)[1]);
			   $('payment_0_securityCode').setAttribute('minlength',getCardDetails(PaymentInfo.selectedCardType)[0]);


			  if(getCardDetails(PaymentInfo.selectedCardType)[2] == 'true')
			  {

					 document.getElementById("issuerequiredlabel").style.display="block";
					 document.getElementById("issuerequiredfield").style.display="block";

					 issueflag=true;
			 }
			 else
			 {
				  issueflag=false;
					document.getElementById("issuerequiredlabel").style.display="none";
					document.getElementById("issuerequiredfield").style.display="none";
			}
	 }
	 else
    {
       issueflag=false;
       document.getElementById("issuerequiredlabel").style.display="none";
	   document.getElementById("issuerequiredfield").style.display="none";
    }

 }

  function issuefieldvalidate()
 {

 	if(issueflag == true)
	{

		          var val = /^[0-9 ]*$/;
	     if(document.getElementById("payment_0_issueNumber").value.length > 0)
	     {

		      if(!val.test(document.getElementById("payment_0_issueNumber").value))
			 {
				alert("Valid Issue Number Required")
					document.getElementById("payment_0_issueNumber").focus();
				return false;
			 }
		    return true;
		}

	}
	return true;
  }

  function setnewsLetter()
 {
			// document.getElementById("newsLetter1").value	 = document.getElementById("newsLetter").checked ;
		 //document.getElementsByName("ecommunication_checked").value	 = document.getElementById("newsLetter").checked ;


 }

  /**This function changes the button based on whether the selected card belongs to the
   * Verified by Visa or Mastercard securecode*/
  function changePayButton(cardType)
  {
		var payButtonDescription = threeDCards.get(cardType);
		var element = document.getElementsByClassName('primary')[0];
		if (payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup")
		{
			element.value = "Proceed to payment";
		}
		else
		{
			element.value = "Pay" ;
		}

  }

function autoCompleteAddress()
{
	if(document.getElementById("autofill").checked==true)
	{
	document.getElementById("payment_0_street_address1").value=streetaddress1;
    document.getElementById("payment_0_street_address2").value=streetaddress2;
	document.getElementById("payment_0_street_address3").value=streetaddress3;
	document.getElementById("payment_0_postCode").value=cardBillingPostcode;
	document.getElementById("payment_0_selectedCountryCode").value=selectedcountryforleadpassenger;
	document.getElementById("payment_0_selectedCountry").value=selectedcountryforleadpassenger;
	}

}