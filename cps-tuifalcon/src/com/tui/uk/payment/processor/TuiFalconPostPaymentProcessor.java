/*
 * Copyright (C)2013 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Map;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for Mobile
 * Flight Only application.
 *
 * @author shwetha.rb
 *
 */
public class TuiFalconPostPaymentProcessor extends ThomsonPostPaymentProcessor
{
   // CHECKSTYLE:OFF
   /** The terms and condition checkbox. */
   private static final String TERMS_AND_CONDITION = "agreeTermAndCondition";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public TuiFalconPostPaymentProcessor(PaymentData paymentData, Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {

       TuiFalconValidationErrors errors = new TuiFalconValidationErrors();

      if (requestParameterMap.get(TERMS_AND_CONDITION) == null)
      {
         String errorMessage =
            PropertyResource
               .getProperty("booking.termsandconditions.notchecked", MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PostPaymentProcessorException("booking.termsandconditions.notchecked");
      }

      if (!(errors.patternCheck("firstName", requestParameterMap.get("firstName"))))
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.firstName");
      }

      if (!(errors.patternCheck("surName", requestParameterMap.get("surName"))))
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.surName");
      }

      if (!(errors.patternCheck("streetAddress1",
         requestParameterMap.get("payment_0_street_address1"))))
      {
         throw new PostPaymentProcessorException(
            "nonpaymentdatavalidation.tuifalcon.streetAddress1");
      }

      if (requestParameterMap.get("payment_0_street_address2") != "")
      {
         if (!(errors.patternCheck("streetAddress2",
            requestParameterMap.get("payment_0_street_address2"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.tuifalcon.streetAddress2");
         }
      }

      if (requestParameterMap.get("payment_0_selectedCountryCode") == "")
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.country");
      }

      if (requestParameterMap.get("payment_0_selectedCountry").equals("IE"))
      {
         if (!(errors.patternCheck("town_republic",
            requestParameterMap.get("payment_0_street_address3"))))
         {
            throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.town");
         }
      }
      else
      {
         if (!(errors.patternCheck("town_ireland",
            requestParameterMap.get("payment_0_street_address3"))))
         {
            throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.town");
         }
      }

      if (requestParameterMap.get("payment_0_selectedCountry").equals("IE"))
      {
         if (requestParameterMap.get("payment_0_postCode") != null)
         {
            if (!requestParameterMap.get("payment_0_postCode").trim().equals(""))
            {
               if (!(errors.patternCheck("postcode_republic",
                  requestParameterMap.get("payment_0_postCode"))))
               {
                  throw new PostPaymentProcessorException(
                     "nonpaymentdatavalidation.tuifalcon.postCode");
               }
            }
         }
      }
      else if (requestParameterMap.get("payment_0_selectedCountry").equals("GB"))
      {
         if (!(errors.patternCheck("postcode_ireland",
            requestParameterMap.get("payment_0_postCode"))))
         {
            throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.postCode");
         }
      }
      //following code commented by sreenivasulu k
     /* if (requestParameterMap.get("payment_0_selectedCountry").equals("IE"))
      {
         if (!(errors.patternCheck("county_republic",
            requestParameterMap.get("payment_0_street_address4"))))
         {
            throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.county");
         }
      }
      else if (requestParameterMap.get("payment_0_selectedCountry").equals("GB"))
      {
         if (!(errors.patternCheck("county_ireland",
            requestParameterMap.get("payment_0_street_address4"))))
         {
            throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.county");
         }
      } */

      if (requestParameterMap.get("title") == "")
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.tuifalcon.title");
      }

      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {

      super.process();
   }

}
