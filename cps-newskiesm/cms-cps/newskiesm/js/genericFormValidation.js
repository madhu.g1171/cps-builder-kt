(function($){

	webForm = {

		validationEventType: null,
		allvalid:null,
		messageMap: null,

		/**
		 * Object: validatorTypes
		 * Defines the rules for all validator input types.
		 */
        validatorTypes: {

			alpha: {
                test: function(inputElement){
                    var filter = /^[a-zA-Z -]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            numeric: {
                test: function(inputElement){
                    var filter = /^[0-9]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			alphanumeric: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 \\\/.,]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            
            alphanumericspec: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 \\\/.,#]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			address: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 ,\.\x27]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            phonenumber: {
                test: function(inputElement){
                    var filter = /(^([+]?[0-9-\(\)]*)([-\(\)0-9]*)$)/;
                    if (inputElement.val() != '' && !filter.test(StringUtils.removeAllSpaces(inputElement.val()))) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			postcode: {
                test: function(inputElement){
                	if((document.getElementById("country").value) == 'United Kingdom')
				{
					var filter = /^([A-PR-UWYZ][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;				                       
				}
                else{
					var filter = /^[-a-zA-Z0-9 \\\/.,]*$/;
                   }				   				   				   
                    if (inputElement.val() != '' && !filter.test(inputElement.val().toUpperCase())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
			
			cardpostcode: {
                test: function(inputElement){							   
                	 if((document.getElementById("cardcountry").value) == 'United Kingdom')
				   {
				      var filter = /^([A-PR-UWYZ][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;		                       
				  }
                  else{
				    var filter = /^[-a-zA-Z0-9 \\\/.,]*$/;
                   }
				   
                    if (inputElement.val() != '' && !filter.test(inputElement.val().toUpperCase())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			cardnumber: {
                test: function(inputElement){
				    var filter = /^[0-9]*$/;
				    var cardType =($("#cardType").val()).split("|")[0];
                    if(cardType=="AMERICAN_EXPRESS")
                    {
				        filter = /^[0-9]{15}$/;
                    }
					else if(cardType != '')
					{
					   filter = /^[0-9]{16,20}$/;
					}
                    if (inputElement.val() != '' && !filter.test(StringUtils.removeAllSpaces(inputElement.val()))) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			securitycode: {
                test: function(inputElement){
				    var filter = /^[0-9]*$/;
				    var cardType =($("#cardType").val()).split("|")[0];
                    if(cardType=="AMERICAN_EXPRESS")
                    {
				        filter = /^[0-9]{4}$/;
                    }
					else if(cardType != '')
					{
					   filter = /^[0-9]{3}$/;
					}
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			expirydate: {
                test: function(inputElement){
				    var Calendar=new Date();
                    var todaysmonth =parseInt(Calendar.getMonth()+1,10);
                    var todaysyear = parseInt(Calendar.getFullYear(),10);
					var cardmonth = $("#expiryDateMM").val();
                    var cardyear = $("#expiryDateYY").val();
					if(cardmonth != "")
					   cardmonth = parseInt(cardmonth,10);
					if(cardyear != "")
                       cardyear = parseInt("20" + cardyear, 10);
                    if (cardmonth != "" && cardyear != "" && ((cardyear == todaysyear) || (cardyear<todaysyear)))
                    {
                       if ((cardmonth<todaysmonth) || (cardyear<todaysyear))
                       {
					      return false;
					   }
					}
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			email: {
                test: function(inputElement){
                    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			nonblank: {
                test: function(inputElement){
                    if (inputElement.val() == '') {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            }

        },

		/**
		 * Object: validators
		 * Defines the rules for all validator.
		 */
        validators: {
        	gfv_required : {		
                test: function(inputElement){			
                    if (inputElement.attr("type")=="text")
                    {
					   var str = inputElement.val();
					   str = str.replace(/^ /i,"")
					   inputElement.val(str);
                    }
                    if (this._isEmpty(inputElement)) {                    	
					   return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            
            gfv_required_postcode : {	
                test: function(inputElement){
                    if (inputElement.attr("type")=="text")
                    {
		       var str = inputElement.val();
		       str = str.replace(/^ /i,"")
		       inputElement.val(str);
                    }
                    if(document.getElementById("country").value == 'United Kingdom'){
                    if (this._isEmpty(inputElement)) {
					   return false;
                    }
                    }        
                    return true;

                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            gfv_required_cardpostcode : {
                test: function(inputElement){
                    if (inputElement.attr("type")=="text")
                    {
		       var str = inputElement.val();
		       str = str.replace(/^ /i,"")
		       inputElement.val(str);
                    }
					
                    if(document.getElementById("cardcountry").value == 'United Kingdom'){
                        if (this._isEmpty(inputElement)) {
    					   return false;
                        }
                        }
                    return true;

                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

         gfv_match : {
                       test: function(inputElement){
          var prevVal = "";
          var matched = true;
          $("input[gfv_match='match']").each(function(index){

            //if ($(this).val().trim()==""){return false;}
           if (prevVal != "" && prevVal != $(this).val()){matched = false;}
           prevVal = $(this).val();

          });
          return matched;


                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            }
        },

        options: {
        	evaluateOnSubmit: true,
			evaluateFieldsOnBlur: true,
            showDefaultError: true,
            validateHidden: false,
			evaluateOnClick: true
        },

        _displayError: function(inputElement, type){
			var webForm = this;
			var errorType = inputElement.attr("id") + webForm.validationEventType + "Error"+ type;
			var errorMessage = (webForm.messageMap == null) ? null : webForm.messageMap[errorType];
            this._trigger("on" + webForm.validationEventType +"ErrorDisplay", null, [inputElement, errorType, errorMessage]);
			if(webForm.validationEventType == "Submit")
			this._setBrowserScrollingOffsets(0,0);
        },

        _init: function(){
			var webForm = this;		
			var ver = window.navigator.appVersion;
            ver = ver.toLowerCase();

        if ( ver.indexOf("android") >= 0 ){            

            var idMaxLengthMap = {};

            //loop through all input-text and textarea element
            $.each($(':text, textarea, :password'), function () {
                var id = $(this).attr('id'),
                    maxlength = $(this).attr('maxlength');

                //element should have id and maxlength attribute
                if ((typeof id !== 'undefined') && (typeof maxlength !== 'undefined')) {
                    idMaxLengthMap[id] = maxlength;

                    //remove maxlength attribute from element
                    $(this).removeAttr('maxlength');

                    //replace maxlength attribute with onkeypress event
                    $(this).attr('onkeypress','if(this.value.length >= maxlength ) return false;');
                }
            });

            //bind onchange & onkeyup events
            //This events prevents user from pasting text with length more then maxlength
            $(':text, textarea, :password').live('change keyup', function () {
                var id = $(this).attr('id'),
                    maxlength = '';
                if (typeof id !== 'undefined' && idMaxLengthMap.hasOwnProperty(id)) {
                    maxlength = idMaxLengthMap[id];
                    if ($(this).val().length > maxlength) {

                        //remove extra text which is more then maxlength
                        $(this).val($(this).val().slice(0, maxlength));
                    }
                }
            });
        }
			
			
			if (webForm.options.evaluateOnSubmit){
            	$(this.element).bind('submit', function(event){
					//event.preventDefault();
					webForm.validationEventType = "Submit";
					var answer = webForm._onValidation(this);
					var inputElements = webForm._getInputFields();
					var allFields = this.elements;
					
					console.log(allFields);
					var chkerror=[];
					var invalidInputs=[];
					/* $.each(inputElements, function(index, inputElement){
						chkerror.push($(inputElement).closest('.row').hasClass('error') ? false : true);
						invalidInputs.push($(inputElement).closest('.row').hasClass('error') ? inputElement:'');
					}); */
					$.each(allFields, function(index, inputElement){
							chkerror.push($(inputElement).closest('.row').hasClass('error') ? false : true);
						invalidInputs.push($(inputElement).closest('.row').hasClass('error') ? inputElement:'');
				});
					
					var allValid = $.inArray(false, chkerror) > -1 ? false : true;
					if(answer && allValid){
						if (hccIframe== 'true')
						   {
							event.preventDefault();
							var formFields = $("form[name='paymentdetails']").serializeArray();
							var serializedData = {};
							formFields.map(function(cur){
								serializedData[cur.name]=cur.value;
								$("input[name='"+cur.name+"']").attr('disabled', 'disabled');
							})
							document.getElementById("passengerTitle0").disabled=true;
							 document.getElementById("country").disabled=true;
							 $.ajax({
								  type: "POST",
								  url: "/cps/acssPDVServlet?token=" + token + tomcatInstance,
								  data: serializedData,
								  success: function(res){
									console.log(res);
									if(res.indexOf('setuperror') !== -1){
										console.log('setuperror');
									}
									if(res.indexOf('validationerror') !== -1){
										console.log('validation error');
									}
									if(res.indexOf('success') == -1){
										$("#iframeHcc").removeClass("hide");
										$("#hccBtn").removeClass("show");
										$("#hccBtn").addClass("hide");
										var hccUrl = res.slice(res.indexOf('https'))
										$("#iframeHcc iframe").attr('src', hccUrl);
									}
								  },
								  error: function(ex){
									  console.log('error');
									  $("#iframeHcc").toggleClass("hide");
									$("#hccBtn").toggleClass("hide");
								  }
								});
						   }				
					}else{
						// filter empty elements of an array
						invalidInputs = invalidInputs.filter(function(e){return e});
						console.log(invalidInputs);
						var pix = 20; //mobile
						var ua=navigator.userAgent; 
						if(ua.indexOf("Mobile") == -1){
							var pix = 0; //desktop
						}
						if (invalidInputs[0].id == "tourOperatorTermsAccepted"){
						//Hard Coded the class as hidden input field offset on the page is zero
							var erinputtop = $(".radio-container.black.smaller-text").offset().top - pix;
						}else{
							var erinputtop = jQuery(invalidInputs[0]).offset().top - pix;
						}
						
						
						webForm._setBrowserScrollingOffsets(0,erinputtop);
						event.preventDefault();						
					}
					
				})
			}
			
            if (webForm.options.evaluateFieldsOnBlur) {
				
                var inputElements = webForm._getInputFields();
                $.each(inputElements, function(index, inputElement){
					
                    $(inputElement).blur(function(event){
					
						event.preventDefault();
						webForm.validationEventType = "Blur";
						if(this.getAttribute('type')!=="checkbox")
						{
						   webForm._trigger("beforeBlur", null, [this.getAttribute('id')]);
                           var answer = webForm._validateInputFieldsType($(this), this.getAttribute('chk'));
						  
						   
						   if(answer)
						      webForm._trigger("on" + webForm.validationEventType +"SucessfulDisplay", null, [this.getAttribute('id'), this]);
						}
                    })
                });
				var selectElements = webForm._getSelectFields();
                $.each(selectElements, function(index, selectElement){
                    $(selectElement).blur(function(event){						
						event.preventDefault();
						webForm.validationEventType = "Blur";
						if($(selectElement).val() != '')
						{
						   webForm._trigger("beforeBlur", null, [this.getAttribute('id')]);
                          var answer = webForm._validateInputFieldsType($(this), this.getAttribute('chk'));
						  if(answer)
						      webForm._trigger("on" + webForm.validationEventType +"SucessfulDisplay", null, [this.getAttribute('id'), this]);
						}
                    })
                });
            }
			 if (webForm.options.evaluateOnClick){
			 $("#promocode").click(function(event){
			 var promoCheck = PromotionalCodeHandler.handle();
			 if(!promoCheck)
			 {
			   $("#promoCodeSection").css('padding-top','6px');
			   webForm._trigger("promoErrorDisplay", null,[PromotionalCodeHandler.errorfieldId, PromotionalCodeHandler.errorMessage]);
			   webForm._setBrowserScrollingOffsets(0,0);
			   event.preventDefault();
	         }
			 else
			 {
			   webForm._trigger("promoSuccessDisplay", null,[PromotionalCodeHandler.errorMessage]);
			 }
             })
            }
        },

		/**
		 * Function: _getInputFields
		 * Get all input fields in a given form.
		 */
		 _gethiddencheckbox:function(){			
			return $(this.element).find("input[chkit='yes']:hidden");
		 },
		_getInputFields: function(){
			var selector = (!this.options.validateHidden) ? 'input[type!=submit]:visible' : ':input[type!=submit]',
				hiddenchkboxes = this._gethiddencheckbox();			
            return $.merge($(this.element).find(selector), hiddenchkboxes);
		},

		/**
		 * Function: _getSelectFields
		 * Get all select fields in a given form.
		 */
		_getSelectFields: function(){
			var selector = (!this.options.validateHidden) ? 'select:visible' : ':select';
            return $(this.element).find(selector);
		},

		/**
		 * Function: _onValidation
		 * Validation form onSubmit using rules specified.
		 *
		 * @param {DOM object} formElement: Given form element in which we want to validate.
		 */
        _onValidation: function(formElement){

  			var webForm = this;
            var passed = new Array();

            var inputElements = webForm._getInputFields();
            inputElements.each(function(i, inputElement){
            	webForm._trigger("beforeSubmit", null, [this.getAttribute('id')]);
                passed.push(webForm._validateInputFields($(inputElement)));
            })
			var selectElements = webForm._getSelectFields();
            selectElements.each(function(i, selectElement){
            	webForm._trigger("beforeSubmit", null, [this.getAttribute('id')]);
                passed.push(webForm._validateInputFields($(selectElement)));
            })
            if ($.inArray(false, passed) != -1) {
                return false;
            }

			return true;
        },

		/**
		 * Function: _validateInputFields
		 * Validation form input fields using rules specified.
		 *
		 * @param {DOM object} inputElement: Given input element, for validating
		 */
        _validateInputFields: function(inputElement,index){
			var webForm = this;
            var passed = new Array();
            for (property in webForm.validators) {
                var value = inputElement.attr(property);
                if (value === property.replace("gfv_","") || value === true) {
                    if (!webForm.validators[property].test.apply(this, [inputElement])) {
						
                        webForm.validators[property].errorDisplay.apply(this, [inputElement, property.replace("gfv_","")]);
                        passed.push(false);
                    }
                }
            }

            if ($.inArray(false, passed) != -1) {
                return false;
            }

			return true;
        },

        /**
		 * Function: _validateInputFieldsType
		 * Validation form input fields using type specified.
		 *
		 * @param {DOM object} inputElement: Given input element, for validating
		 */
        _validateInputFieldsType: function(inputElement, type){
            for (property in webForm.validatorTypes) {
                if (property === type) {
               	 	if (!this.validatorTypes[type].test.apply(this, [inputElement])) {
           				this.validatorTypes[type].errorDisplay.apply(this, [inputElement, type]);
               			return false;
            		}
					return true;
                }
            }
		},

        /**
         * Method: _isEmpty
         * check if a input field is empty.
         */
        _isEmpty: function(inputElement){			
            var type = inputElement.attr('type');			
            var answer  = (type == 'checkbox') ? (inputElement.val() == 'false') : (inputElement.val() == '');return answer;
        },

        /**
         * Method: addValidatorType
         * adds rules to validatortype.
         */
        addValidatorType: function(type, rules, validatorRulesType){
            var validatorTypes = (validatorRulesType === webForm.TYPE_VALIDATOR) ? this.validatorTypes : this.validators
            validatorType = validatorTypes[type];
            validatorType = (validatorType) ? $.extend(true, validatorType, rules) : rules;
            this.validatorTypes[type] = validatorType;
        },

        /**
         * Method: addValidator
         * adds rules to validator.
         */
        addValidator: function(type, rules){
            var validator = this.validators[type];
            validator = (validator) ? $.extend(true, validator, rules) : rules;
            this.validators[type] = validator;
        },

		addErrorMessage: function(messageMap){
			var webForm = this;
			webForm.messageMap = messageMap;
		},

		_setBrowserScrollingOffsets: function(x, y)
		{
		   if( document.documentElement && document.documentElement.scrollTop ){
              // Explorer 6 Strict
              document.documentElement.scrollLeft = x;
              document.documentElement.scrollTop = y;
           }
           else if( document.body ){
              document.body.scrollLeft = x;
              document.body.scrollTop = y;
           }
		},

		_luhnCheck: function()
		{
           var cardnumber = $("#cardNumber").val();
           var oddoreven = cardnumber.length & 1;
           var sum = 0;
           var addition = "";
           for (var count = 0; count < cardnumber.length; count++)
           {
              var digit = parseInt(cardnumber.substr(count,1));
              if (!((count & 1) ^ oddoreven))
              {
                 digit *= 2;
                 if (digit > 9)
                 {
                    digit -= 9;
                    addition = addition + ' ' + digit;
                 }
                 else
                 {
                    addition = addition + ' ' + digit;
                 }
                 sum += digit;
              }
              else
              {
                 sum += digit;
                 addition = addition + ' ' + digit;
              }
           }
           if (sum % 10 != 0) {return false}
              return true
		},

		_clearCardDetails: function()
		{
			$("#cardType").val("");
			$("#cardNumber").val("");
			$("#cardName").val("");
			$("#expiryDateMM").val("");
			$("#expiryDateYY").val("");
			$("#securityCode").val("");
			$("#issueNumber").val("");
		}
    }

    webForm.TYPE_VALIDATOR = "TYPE_VALIDATOR";
    webForm.VALIDATOR = "VALIDATOR";

    $.widget("ui.webForm", webForm);



})(jQuery);