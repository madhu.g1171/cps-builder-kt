<div class="section-heading">
								<h2>Card holder address</h2>
							</div>
							<div class="row c">
								<p>
									To help ensure your card details remain secure, please confirm the
									address of the card holder.
								</p>
							</div>
							<div class="row c">
								<div class="radio-container black" onclick="AddressPopulationHandler.handle()">
									<input style="" type="checkbox" name="autoCheckCardAddress" id="useAddress" value="false">
									<span class="radio active"></span>
									Same address as lead passenger
								</div>
							</div>
							
<input type='hidden'  id='panelType' value='CNP'/>
<input type='hidden' value='' name='payment_0_paymenttypecode' id='payment_0_paymenttypecode'/>
<input type='hidden' id='total_amount_0' name='payment_0_amountPaid' value='0.0'/>
<input name='payment_0_cardCharge' type='hidden' id='cardChargeAmount_0'/>
<input name='payment_0_cardLevyPercentage' type='hidden' id='cardLevyCharge_0' value='0.0'/>
<%-- *******Essential fields ********************  --%>
<input type="hidden" name="total_transamt"  id="total_transamt"  value='NA' />
<%-- *******End Essential fields ********************  --%>
							<div class="row c">
								<label class="two-lines mob-one-line" for="house-number-ch">House <span>name/number</span></label>
								<input type="text" id="cardHouseName" autocomplete="off" chk="alphanumericspec" name="payment_0_street_address1" maxlength="20" gfv_required="required" value="${bookingInfo.paymentDataMap['payment_0_street_address1']}">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label for="adress-ch">Address</label>
								<input type="text" chk="alpha" autocomplete="off" id="cardAddress1" name="payment_0_street_address2" gfv_required="required" maxlength="25" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row no-label c">
								<input type="text" id="cardAddress2" autocomplete="off" chk="alpha" name="payment_0_cardHolderAddress2" maxlength="25" 
				value="<c:out value="${bookingInfo.paymentDataMap['payment_0_cardHolderAddress2']}"/>">
							</div>
							<div class="row c">
								<label for="town-city">Town/city</label>
								<input type="text" chk="alpha" autocomplete="off" id="cardTownCity" maxlength="25" name="payment_0_street_address3" gfv_required="required" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label class="two-lines" for="county">County <span class="hint">(optional)</span></label>
								<input type="text" id="cardCounty" autocomplete="off" chk="alpha" maxlength="16" name="payment_0_street_address4" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label class="b">Country</label>
								<div class="select fixed-width">
									<span class="text">Please select</span>
									<span class="arrow"><span></span></span>
									<c:set var="selectedCountryName" value="${bookingInfo.paymentDataMap['payment_0_selectedCountry']}" scope="page"/>
	  <select chk="nonblank" name="payment_0_selectedCountry" id="cardcountry"  requiredError="Your country"  
          value="<c:out value="${bookingInfo.paymentDataMap['payment_0_selectedCountry']}"/>" gfv_required="required" alt="CardHolder Country"
          onchange="cardpostcodeValidation.cardpostcodeChange()">
	  <option id="pleaseSelect" value='' selected="selected">Please select</option>
         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
            <c:set var="countryCode" value="${CountriesList.key}" />
            <c:set var="countryName" value="${CountriesList.value}" />
             <c:if test='${countryCode=="GB"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
            <c:set var="countryCode" value="${CountriesList.key}" />
            <c:set var="countryName" value="${CountriesList.value}" />
             <c:if test='${countryCode =="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
         <option class="select-dash" disabled="disabled">---------------------------</option>
         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
            <c:set var="countryCode" value="${CountriesList.key}" />
            <c:set var="countryName" value="${CountriesList.value}" />
             <c:if test='${countryCode!="GB" && countryCode !="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
         </select>
								</div>
							</div>
							<div class="row c">
								<label for="postcode-ch">Postcode</label>
								<input type="text" chk="cardpostcode" autocomplete="off" id="cardPostCode" maxlength="8" name="payment_0_postCode" gfv_required_cardpostcode="required_cardpostcode" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>