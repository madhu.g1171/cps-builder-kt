<div class="section-heading first">
								<h2>Passenger details</h2>
							</div>
	<c:set var="adultCount" value="${bookingComponent.nonPaymentData['passenger_ADT_count']}" scope="page"/>
    <c:set var="childrenCount" value="${bookingComponent.nonPaymentData['passenger_CHD_count']}" scope="page"/>
    <c:set var="infantCount" value="${bookingComponent.nonPaymentData['passenger_INF_count']}" scope="page" />
    <c:set var="totalnumberOfPassengers" value="${bookingComponent.nonPaymentData['passenger_total_count']}" scope="page" />
	<c:set var="passengerCount" value="0" scope="request"/>
	<script type="text/javascript">
Personaldetails.setNoOfPassengers('<c:out value="${totalnumberOfPassengers}"/>');
</script>
							<div class="row c">
							<c:set var="passengerCount" value="0" scope="request"/>
	                   <c:set var="titlekey" value="passenger_${passengerCount}_title"/>
								<label class="two-lines b">Lead <span>passenger</span></label>
								<div class="select">
									<span class="text">Title</span>
									<span class="arrow"><span></span></span>
									<c:set var="passengerCount" value="0" scope="request"/>
	     <c:set var="titlekey" value="passenger_${passengerCount}_title"/>
	     <select chk="nonblank" id="passengerTitle0" name="passenger_0_title" gfv_required="required" alt="Lead Passenger Title" class="initials">
	        <option value="" class="lead_title">Title</option>
	        <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
   			<option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
			<option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
			<option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
			<option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
			<option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
			<option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
	     </select>
								</div>
								
								
							</div>
							<div class="row c">
							 <c:set var="foreNameKey" value="passenger_${passengerCount}_foreName"/>
								<label for="first-name">First name</label>
								<input id="foreName_0" type="text" autocomplete="off" class="textfield" maxlength="15" gfv_required="required" chk="alpha" name="<c:out value='${foreNameKey}'/>" value="<c:out value='${bookingComponent.nonPaymentData[foreNameKey]}'/>" >
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							
							<div class="row c">
							<c:set var="lastNameKey" value="passenger_${passengerCount}_lastName"/>
								<label for="surname">Surname</label>
								<input type="text" id="surName_0" autocomplete="off" class="textfield" maxlength="15" gfv_required="required" chk="alpha" name="<c:out value='${lastNameKey}'/>" value="<c:out value='${bookingComponent.nonPaymentData[lastNameKey]}'/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>								
							</div>
							<c:set var="passengerCount" value="${passengerCount+1}"/>