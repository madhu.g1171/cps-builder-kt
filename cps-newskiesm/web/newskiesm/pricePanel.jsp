<div class="accordion sand">
	<div class="item open">
		<div class="trigger no-icon">
			<p>Running total</p>
		</div>
		<div class="content" style="height: 0px;"></div>
	</div>
</div>

<fmt:formatNumber var="balanceDue"
	value="${bookingInfo.calculatedTotalAmount.amount}" type="number"
	maxFractionDigits="2" minFractionDigits="2" groupingUsed="false" />
<fmt:formatNumber var="amountPaid"
	value="${bookingComponent.nonPaymentData['amount_paid']}" type="number"
	maxFractionDigits="2" minFractionDigits="2" groupingUsed="false" />
	<fmt:formatNumber var="totalAmount"
	value="${bookingComponent.nonPaymentData['transaction_total']}" type="number"
	maxFractionDigits="2" minFractionDigits="2" groupingUsed="false" pattern="######.##" />

<c:set var="priceComponentLength" value="${fn:length(bookingComponent.priceComponents)}" />

<div class="total-added">
	<ul>
		<li><p class="title">Added</p></li>
		<c:if test="${priceComponentLength > 0}">
			<li class="items">
			<c:forEach var="priceComponent" items="${bookingComponent.priceComponents}">
			<ul>
			
						<c:choose>
							<%--START OF FLIGHT OUT DETAILS --%>
							<c:when	test="${fn:contains(priceComponent.itemDescription, 'FLIGHT OUT')}">
							<li>
								<p class="title icon">
									<i class="caret flyout brown"></i>
									<span class="grey">
										<c:out value="${priceComponent.itemDescription}" /> - 
										<c:if test="${not empty bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode']}">
											<c:out value="${bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode']}" />
										</c:if>
									</span>
										
								</p>
								
								<p class="from-to">
									<c:if test="${not empty bookingComponent.nonPaymentData['outBoundFlight_departureAirportName'] 	&& not empty bookingComponent.nonPaymentData['outBoundFlight_arrivalAirportName']}">
									<c:out value="${fn:toUpperCase(bookingComponent.nonPaymentData['outBoundFlight_departureAirportName'])}" />
									</c:if>
								</p>
								
								<p class="when">
									<c:if test="${not empty bookingComponent.nonPaymentData['outBoundFlight_departureDate']}">
										<i class="caret date brown"></i>
										<c:out value="${bookingComponent.nonPaymentData['outBoundFlight_departureDate']}" />
									</c:if>
									<c:if test="${not empty bookingComponent.nonPaymentData['outBoundFlight_departureTime'] && not empty bookingComponent.nonPaymentData['outBoundFlight_arrivalTime']}">
										<c:set var="Fo_lengthDepTime" value="${fn:length(bookingComponent.nonPaymentData['outBoundFlight_departureTime'])}"/>
										<c:set var="Fo_meridianDepTime" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_departureTime'],Fo_lengthDepTime-2,Fo_lengthDepTime)}"/>
										<c:set var="Fo_dep_Time" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_departureTime'],0,Fo_lengthDepTime-3)}"/>
										<c:set var="Fo_lengthArrTime" value="${fn:length(bookingComponent.nonPaymentData['outBoundFlight_arrivalTime'])}"/>
										<c:set var="Fo_meridianArrTime" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_arrivalTime'],Fo_lengthArrTime-2,Fo_lengthArrTime)}"/>
										<c:set var="Fo_arr_Time" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_arrivalTime'],0,Fo_lengthArrTime-3)}"/>
										<i class="caret time brown"></i>
										<c:out value="${Fo_dep_Time}"/>
									</c:if>
								</p>
								
							</li>
							</c:when>								
							<%--END OF FLIGHT OUT DETAILS --%>

							<%--START OF FLIGHT HOME DETAILS --%>
							<c:when test="${fn:contains(priceComponent.itemDescription,'FLIGHT HOME')}">
							<p class="title"></p>							
							<li>
								<p class="title icon">
									<i class="caret flyin brown"></i>
									<span class="grey">
										<c:out value="${priceComponent.itemDescription}" /> - 
										<c:if test="${not empty bookingComponent.nonPaymentData['inBoundFlight_operatingAirlineCode']}">
											<c:out value="${bookingComponent.nonPaymentData['inBoundFlight_operatingAirlineCode']}" />
										</c:if>
									</span>										
								</p>
								<c:if test="${not empty bookingComponent.nonPaymentData['inBoundFlight_departureAirportName'] && not empty bookingComponent.nonPaymentData['inBoundFlight_arrivalAirportName']}">
								<p class="from-to">
									<c:out value="${fn:toUpperCase(bookingComponent.nonPaymentData['inBoundFlight_departureAirportName'])}" />
								</p>
								</c:if>
								
								<p class="when">
									<c:if test="${not empty bookingComponent.nonPaymentData['inBoundFlight_departureDate']}">
										<i class="caret date brown"></i>
										<c:out value="${bookingComponent.nonPaymentData['inBoundFlight_departureDate']}" />
									</c:if>
									
									<c:if test="${not empty bookingComponent.nonPaymentData['inBoundFlight_departureTime'] && not empty bookingComponent.nonPaymentData['inBoundFlight_arrivalTime']}">
										<c:set var="Fh_lengthDepTime" value="${fn:length(bookingComponent.nonPaymentData['inBoundFlight_departureTime'])}"/>
										<c:set var="Fh_meridianDepTime" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_departureTime'],Fh_lengthDepTime-2,Fh_lengthDepTime)}"/>
										<c:set var="Fh_dep_Time" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_departureTime'],0,Fh_lengthDepTime-3)}"/>
										<c:set var="Fh_lengthArrTime" value="${fn:length(bookingComponent.nonPaymentData['inBoundFlight_arrivalTime'])}"/>
										<c:set var="Fh_meridianArrTime" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_arrivalTime'],Fh_lengthArrTime-2,Fh_lengthArrTime)}"/>
										<c:set var="Fh_arr_Time" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_arrivalTime'],0,Fh_lengthArrTime-3)}"/>
										<i class="caret time brown"></i>
										<c:out value="${Fh_dep_Time}"/>
									</c:if>
								</p>
								
								
							</li>
							</c:when>
							<%--END OF FLIGHT HOME DETAILS --%>					
						</c:choose>
					
						<c:if test="${priceComponent.itemDescription!='FLIGHT OUT'&& priceComponent.itemDescription!='FLIGHT HOME'&& priceComponent.itemDescription !=null}">
							<ul class="c">
								<li class="two-cols c">
									<p>
										<c:out value="${priceComponent.itemDescription}" escapeXml="false" /> x<c:if test="${priceComponent.quantity>0}"><c:out value="${fn:trim(priceComponent.quantity)}" escapeXml="false"/>
										</c:if>	
										<span>
										<c:if test="${priceComponent.amount.amount>=0 && priceComponent.itemDescription !=null}">
											<c:out value="${currencySymbol}" escapeXml="false" />
											<fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##" />
										</c:if>	
										</span>
									</p>
								</li>
							</ul>
						</c:if>
			</ul>
			</c:forEach>
			</li>
		</c:if>
		<li class="total-price two-cols">
			<p class="title">
				<i><c:out value="${currencySymbol}" escapeXml="false"/></i>Total added <span><c:out value="${currencySymbol}" escapeXml="false"/><span><c:out value="${balanceDue}"/></span></span>
			</p>
		</li>
		<li class="credit-charges">
			<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
			<c:if test="${applyCreditCardSurchargeGBP eq 'true'}">
			<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html" class="small-text">
				<i class="caret link"></i>Our Credit Card Charges
			</a> 
			</c:if>
			</c:if>
			<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
			<c:if test="${applyCreditCardSurchargeEUR eq 'true'}">
			<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html" class="small-text">
				<i class="caret link"></i>Our Credit Card Charges
			</a> 
			</c:if>
			</c:if>
			<p>All extras are non-refundable. Check the running total above for confirmation of your selections and prices.</p>
		</li>
	</ul>
</div>
