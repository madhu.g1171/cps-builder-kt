<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <title>Payments</title>
	  <link href="/cms-cps/newskiesm/images/favicon.ico" rel="shortcut icon">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
      <version-tag:version/>
      <%@include file="javascript.jspf" %>
      <%@include file="configSettings.jspf" %>
      <%@include file="css.jspf" %>
      <%@include file="tag.jsp"%>
      <%@include file="/common/commonTagLibs.jspf"%>
      <%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         String axaInsurance = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".axaInsuranceEnabled","false");
         pageContext.setAttribute("axaInsurance", axaInsurance, PageContext.SESSION_SCOPE);
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
      %>
       <c:set var="hccsmap" value="${bookingComponent.hccsMap}" />
<%
	String applyCreditCardSurchargeGBP = com.tui.uk.config.ConfReader.getConfEntry("Newskiesm.GBP.applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurchargeGBP", applyCreditCardSurchargeGBP, PageContext.REQUEST_SCOPE);
	String applyCreditCardSurchargeEUR = com.tui.uk.config.ConfReader.getConfEntry("Newskiesm.EUR.applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurchargeEUR", applyCreditCardSurchargeEUR, PageContext.REQUEST_SCOPE);
		String hccIframeEnable = com.tui.uk.config.ConfReader.getConfEntry("NEWSKIESM.hcc.switch" ,"false");
	pageContext.setAttribute("hccIframeEnable", hccIframeEnable, PageContext.REQUEST_SCOPE);
%>
<script>
var hccIframe = "${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}"
applyCreditCardSurchargeGBP = "<%= applyCreditCardSurchargeGBP %>";
applyCreditCardSurchargeEUR = "<%= applyCreditCardSurchargeEUR %>";
</script>
      <script type="text/javascript" src="/cms-cps/newskiesm/js/iscroll-lite.js"">
	</script>
   </head>
   <body>
   <script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.page.BkgRef = "${bookingComponent.nonPaymentData['booking_Reference_Number']}";
				 tui.analytics.page.InboundDays = "${bookingComponent.nonPaymentData['InboundDays']}";
				 tui.analytics.page.CheckIn = "${bookingComponent.nonPaymentData['CheckIn']}";
				 tui.analytics.page.SrcBkgRef = "${bookingComponent.nonPaymentData['SrcBkgRef']}";
				 tui.analytics.page.SrcBkgSys = "${bookingComponent.nonPaymentData['SrcBkgSys']}";
</script>
    <script type="text/javascript" src="/cms-cps/newskiesm/js/genericFormValidation.js"></script>
    		<script type='text/javascript'>
			$(document).ready(function() {			
				enableSelectBoxes();
				enableCheckBoxes();
			});
			
			function enableSelectBoxes(){
				$('div.select').each(function(){
					var $txt = $(this).children('.text'),
						$select = $(this).children('select');
						
					// onload set the selected value
					if(!$(this).attr('selectedIndex')==0){								
						$txt.text($(this).find('option:selected').text());
					}else{
						$txt.text($(this).find('option:selected').text());								
					}
					$select.change(function() {
						if(!$(this).attr('selectedIndex')==0){								
							$txt.text($(this).find('option:selected').text());
						}else{
							$txt.text($(this).find('option:selected').text());								
						}
					});
					
				});
			}
			
			function enableCheckBoxes(){
				$('.radio-container').each(function(){
					
					// intial checkbox status update on load
					var $chkinput = $(this).children('input[type="checkbox"]'),
						$radio = $(this).children('.radio');
						
					// hide radio inputs
					$chkinput.hide();					
					
					if(!$chkinput.val()){
						$radio.addClass('active');
					}else{
						$radio.removeClass('active');
					}
					
					// checkbox status update on click
					$radio.click(function(){
						var $chkinput = $(this).closest('.radio-container').children('input[type="checkbox"]');
						$radio.toggleClass('active');
						if(!$radio.hasClass('active')){							
							$chkinput.attr('checked', false);
							$chkinput.val(false);
						}else{
							$chkinput.val(true);
							$chkinput.attr('checked', true);
						}
					});
				});
			}
		</script>
		
		
      <!--
      Start of DoubleClick Floodlight Tag: Please do not remove
      Activity name of this tag: Thomson Payment Page
      URL of the webpage where the tag is expected to be placed: http://www.thomson.co.uk
      This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
      Creation Date: 10/05/2010
      -->
      <script type="text/javascript">
         var axel = Math.random() + "";
         var a = axel * 10000000000000;
         document.write('<iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=thoms460;cat=thoms149;ord=1;num=' + a + '?" width="1" height="0" frameborder="0"></iframe>');
      </script>
 
      <noscript>
         <iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=thoms460;cat=thoms149;ord=1;num=1?" width="1" height="0" frameborder="0"></iframe>
      </noscript>
      <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
      <div id="contentCol2">
	  <div id="page" class="c">
		
		<%@include file="header.jspf" %>
		<%@include file="crumby.jspf" %>
		
		 
		<div id="content" class="acss">
			<div class="content-width">
				<div class="right-column no-pad show-mobile booking-details">
					<%@include file="pricePanel.jsp" %>
				</div>
							    
				<div id="customer-form" class="acss">
					<div class="payment">
						
					 <c:choose>
						<c:when	test="${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}">
							<form name="paymentdetails" method="post" action="/cps/acssPDVServlet?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />">
								<%@include file="topError.jspf" %>
								<%@include file="passengerDetails.jsp" %>
								<%@include file="leadPassengerContactDetails.jsp" %>
								<%@include file="importantInformation.jspf" %>
								<div class ="hide" id = "iframeHcc">
								<iframe name="HCCRedirectToCPS" id="hcctoCps" src="${hccsmap.hpsUrl}?HPS_SessionID=${hccsmap.hpsSession}" style="width: 100%;height: 960px; border: none;">
								</iframe>
								</div>
							</form>
						 </c:when>
				        <c:otherwise>
							<form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />">
								<%@include file="topError.jspf" %>
								<%@include file="passengerDetails.jsp" %>
								<%@include file="leadPassengerContactDetails.jsp" %> 
								<%@include file="paymentDetails.jsp" %>
			                     <%@include file="cardHolderAddressDetails.jsp" %>
								<%@include file="importantInformation.jspf" %>
							</form>
						</c:otherwise>
        			</c:choose>
					</div>
				</div>
			</div>
		</div>
		<%@include file="footer.jspf" %>
	  </div>
	 </div>
      <!-- <script type="text/javascript" src="/cms-cps/newskiesm/js/vs.js"></script> -->
      <script type="text/javascript">
       window.onload=
        function()
        {
    	   amountUpdationWithCardCharge.updateCardChargeForNEWSKIES()
        }
      </script>
   </body>
</html>