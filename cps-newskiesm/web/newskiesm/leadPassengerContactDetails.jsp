<p class="title contact-details">Contact details</p>

							<div class="row c">
								<label class="two-lines mob-one-line" for="house-number">House <span>name/number</span></label>
								<input type="text" id="houseName" autocomplete="off" name="houseName" maxlength="20" gfv_required="required" chk="alphanumericspec" value="<c:out value="${bookingComponent.nonPaymentData['houseName']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>								
							</div>
							<div class="row c">
								<label for="adress">Address</label>
								<input type="text" id="addressLine1" autocomplete="off" name="addressLine1" maxlength="25" chk="alpha" gfv_required="required" value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c no-label">
								<input type="text" id="addressLine2" autocomplete="off" name="addressLine2" chk="alpha" maxlength="25" value="<c:out value="${bookingComponent.nonPaymentData['addressLine2']}"/>">
							</div>
							<div class="row c">
								<label for="town-city">Town/city</label>
								<input type="text" id="city" autocomplete="off" name="city" chk="alpha" maxlength="25" gfv_required="required" value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label class="two-lines" for="county">County <span class="hint">(optional)</span></label>
								<input type="text" id="county" autocomplete="off" name="county" chk="alpha" maxlength="16" value="<c:out value="${bookingComponent.nonPaymentData['county']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label class="b">Country</label>
								<div class="select fixed-width">
								
									<span class="text">Please select</span>
									<span class="arrow"><span></span></span>
									<c:set var="selectedCountryName" value="${bookingComponent.nonPaymentData['country']}"/>
									<select chk="nonblank" name="country" id="country" gfv_required="required" requiredError="Your country" value="<c:out value="${bookingComponent.nonPaymentData['country']}"/>" gfv_required="required" alt="Country"
		  onchange="postcodeValidation.postcodeChange()">
			<option id="pleaseSelect" value='' selected="selected">Please select</option>
			
			<c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
			
	            <c:set var="countryCode" value="${CountriesList.key}" />
	            <c:set var="countryName" value="${CountriesList.value}" />
	            <c:if test='${countryCode=="GB"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
			   
            </option>
            </c:if>
         </c:forEach>
		 <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
			
	            <c:set var="countryCode" value="${CountriesList.key}" />
	            <c:set var="countryName" value="${CountriesList.value}" />
	            <c:if test='${countryCode =="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
			   
            </option>
            </c:if>
         </c:forEach>
			<option class="select-dash" disabled="disabled">---------------------------</option>
	        <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
	            <c:set var="countryCode" value="${CountriesList.key}" />
	            <c:set var="countryName" value="${CountriesList.value}" />
	            <c:if test='${countryCode!="GB" && countryCode !="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
        </select>
								</div>
							</div>
							<div class="row c">
								<label for="postcode">Postcode</label>
								<input type="text" id="postCode" autocomplete="off" name="postCode" maxlength="8" chk="postcode"gfv_required_postcode="required_postcode" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label for="phone">Phone number</label>
								<input type="tel" id="dayTimePhone" autocomplete="off" name="dayTimePhone" maxlength="15" gfv_required="required" chk="phonenumber" value="<c:out value="${bookingComponent.nonPaymentData['dayTimePhone']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label class="two-lines" for="mobile">Mobile <span class="hint">(optional)</span></label>
								<input type="tel" chk="phonenumber" autocomplete="off" id="mobilePhone" name="mobilePhone" maxlength="15" value="<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
