<%
				String acssSwitch2 = com.tui.uk.config.ConfReader.getConfEntry("acss.switch" , "");
                pageContext.setAttribute("acssSwitch2", acssSwitch2, PageContext.REQUEST_SCOPE);
%>

<c:set var="TUISwitch3" value="${acssSwitch2}"/>

<c:choose>

   <c:when test="${TUISwitch3 eq 'ON'}">
      <c:set value="www.tui.co.uk" var="tuiFlagLink" />
   </c:when>
   <c:otherwise>
      <c:set value="www.thomson.co.uk" var="tuiFlagLink" />
   </c:otherwise>
</c:choose>





<div class="section-heading">
								<h2>Payment details</h2>
							</div>
							<div class="row c">
							<c:choose>
                            <c:when test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
                            	<c:choose>
									<c:when test="${applyCreditCardSurchargeGBP eq 'true'}">
								<p>
									There are no additional charges when paying by Maestro,
									MasterCard Debit, Visa/ Delta debit cards or TUI Credit Card. A fee of <c:out value="${cardChargePercent}"/>% applies to
									credit card payments when using American Express, MasterCard
									Credit or Visa Credit cards.<br>
								</p>
								</c:when>
                                    <c:otherwise>
                                     <p>
									There are no additional charges when paying by Maestro,
									MasterCard Debit, Visa/ Delta debit cards or TUI Credit Card. A fee of <c:out value="${cardChargePercent}"/>% applies to
									credit card payments when using American Express, MasterCard
									Credit or Visa Credit cards.<br>
								</p>
                                 </c:otherwise>
                                  </c:choose>

								</c:when>
								<c:when test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
								<c:choose>
									<c:when test="${applyCreditCardSurchargeEUR eq 'true'}">
								<p>
									There are no additional charges when paying by Maestro,
									MasterCard Debit, Visa/ Delta debit cards or TUI Credit Card. A fee of <c:out value="${cardChargePercent}"/>% applies to
									credit card payments when using American Express, MasterCard
									Credit or Visa Credit cards.<br>
								</p>
								</c:when>
                                    <c:otherwise>
                                     <p>
									There are no additional charges when paying by Maestro,
									MasterCard Debit, Visa/ Delta debit cards or TUI Credit Card. A fee of <c:out value="${cardChargePercent}"/>% applies to
									credit card payments when using American Express, MasterCard
									Credit or Visa Credit cards.<br>
								</p>
                                 </c:otherwise>
                                  </c:choose>
								</c:when>
								<c:otherwise></c:otherwise>
                            </c:choose>
							</div>
<fmt:formatNumber var="transactionTotal" value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" scope="request" pattern="######.##" />
<fmt:formatNumber var="amountWithCardCharge" value="${transactionTotal+applicableCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>
<fmt:formatNumber var="amountWithDebitCardCharge" value="${transactionTotal+applicableDebitCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>

							<div class="row cb">
								<p id="amountWithoutCardCharge" class="alert generic mid-level row-content">
									<span class="amount"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithDebitCardCharge}"/></span>
									Payable today if paying by Debit Card
								</p>
							</div>

							<div class="row cb">
								<p id="thomsonCardCharge" class="alert generic mid-level row-content" style=" display:none" >
									<span class="amount"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithDebitCardCharge}"/></span>
									         <c:choose>
                                           <c:when test="${TUISwitch3 eq 'ON'}">
									Includes no extra charge for using TUI credit card
									</c:when>
                                    <c:otherwise>
                                     Includes no extra charge for using TUI credit card
                                 </c:otherwise>
                                  </c:choose>
								</p>
							</div>

							<div class="row cb">
								<p id="amountWithCardCharge" class="alert generic mid-level green row-content">
									<span class="amount"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithCardCharge}"/></span>
									Payable today if paying by Credit Card 
									<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
									<c:if test="${applyCreditCardSurchargeGBP eq 'true'}">
									- Includes <c:out value="${cardChargePercent}"/>% Credit Card surcharge</c:if>
									</c:if>
									<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
									<c:if test="${applyCreditCardSurchargeEUR eq 'true'}">
									- Includes <c:out value="${cardChargePercent}"/>% Credit Card surcharge</c:if>
									</c:if>
								</p>
							</div>

							<div class="row c">
								<label class="b">Card type</label>
								<div class="select fixed-width">
									<span class="text">Visa</span>
									<span class="arrow"><span></span></span>
									<select chk="nonblank" id="cardType" name="payment_0_type" gfv_required="required" onchange="CardTypeChangeHandler.handleCardSelection(),amountUpdationWithCardCharge.updatePaymentInfoForNEWSKIES()" alt="Card Type" class="card_type">
         <option id="pleaseSelect" value='' selected="selected">Please select</option>
         <c:choose>
<c:when test="${TUISwitch3 eq 'ON'}">
<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'>
				 <c:choose>
				 <c:when test="${paymentType.paymentCode eq 'TUI_MASTERCARD'}">
<c:out value="TUI CreditCard" escapeXml='false'/> </option>
</c:when>
<c:otherwise>
<c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
</c:otherwise>
</c:choose>
         </c:forEach>
</c:when>
<c:otherwise>
<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
         </c:forEach>
</c:otherwise>
</c:choose>
         <c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
         </c:forEach>
      </select>
								</div>
							</div>

							<div class="cardnumber row c">
								<label for="card-number">Card number</label>
								<input class="cardno" type="tel" chk="cardnumber" id="cardNumber" autocomplete="off" maxlength="20" name="payment_0_cardNumber" gfv_required="required">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c">
								<label for="name-on-card">Name on card</label>
								<input type="text" chk="alpha" id="cardName" autocomplete="off" name="payment_0_nameOnCard" maxlength="25" gfv_required="required">
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<div class="row c expiry-date">
								<label class="b">Expiry date</label>
								<div class="select fixed-width">
									<span class="text">Month</span>
									<span class="arrow"><span></span></span>
									<select id="expiryDateMM" name="payment_0_expiryMonth" class="smallList expiryDateMM" gfv_required="required" alt="Expiry Date" chk="expirydate">
         <option value="">MM</option>
         <option value='01'>01</option>
         <option value='02'>02</option>
         <option value='03'>03</option>
         <option value='04'>04</option>
         <option value='05'>05</option>
         <option value='06'>06</option>
         <option value='07'>07</option>
         <option value='08'>08</option>
         <option value='09'>09</option>
         <option value='10'>10</option>
         <option value='11'>11</option>
         <option value='12'>12</option>
      </select>
								</div>
								<div class="select fixed-width">
									<span class="text">Year</span>
									<span class="arrow"><span></span></span>
									<select id="expiryDateYY" name="payment_0_expiryYear" class="smallList expiryDateYY" gfv_required="required" alt="Expiry Date" chk="expirydate">
         <option value="">YY</option>
         <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
            <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
         </c:forEach>
      </select>
								</div>
							</div>
							<div class="row c security-code">
								<label for="security-code">Security code</label>
								<input type="tel" style="width: 120px;" id="securityCode" name="payment_0_securityCode" autocomplete="off" maxlength="4" gfv_required="required" chk="securitycode">
								<!-- DO NOT DELETE - it is not used at the moment, but might be added later
								<a class="tooltip" href="javascript:void(0)">
									What's this?
								</a>
								-->
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>

							<div class="row c security-code hide" id="issue-number">
								<label for="issue-number">Issue number</label>
								<input type="text" id="issueNumber" autocomplete="off" name="payment_0_issueNumber" maxlength="2" chk="numeric" style="width: 120px;">
								<!-- DO NOT DELETE - it is not used at the moment, but might be added later
								<a class="tooltip" href="javascript:void(0)">
									What's this?
								</a>
								-->
								<i class="caret erase red"></i>
								<i class="caret tick green"></i>
							</div>
							<c:if test="${not empty bookingInfo.threeDEnabledLogos}">
							<div class="row c">
							<c:set var="isMastercardLogoPresent" value="false"/>
		    <c:set var="isVisaLogoPresent" value="false"/>
            <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
              <c:choose>
                 <c:when test="${threeDLogos == 'mastercardgroup'}">
                    <c:set var="isMastercardLogoPresent" value="true"/>
								<div class="security-logo">
									<img src="/cms-cps/newskiesm/images/logos/mastercard.gif">
									<a href="javascript:void(0);" class="threeDSstickyOwner" id="masterCardDetails"><i class="caret link"></i>Learn more</a>
									<%@ include file="mastercardLearnMoreSticky.jspf"%>
								</div>
								</c:when>
						</c:choose>
						</c:forEach>
						<c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
            <c:choose>
               <c:when test="${threeDLogos == 'visagroup'}">
				  <c:set var="isVisaLogoPresent" value="true"/>
								<div class="security-logo" <c:if test="${isMastercardLogoPresent}"> </c:if>>
									<img src="/cms-cps/newskiesm/images/logos/verified-by-visa.gif">
									<a href="javascript:void(0);" id="visaDetails" class="threeDSstickyOwner">
									<i class="caret link"></i>Learn more</a>
									<%@ include file="visaLearnMoreSticky.jspf"%>
								</div>
								</c:when>
							</c:choose>
						</c:forEach>
						<c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
            <c:choose>
               <c:when test="${threeDLogos == 'americanexpressgroup'}">
				  <c:set var="isVisaLogoPresent" value="true"/>
								<div class="security-logo" <c:if test="${isVisaLogoPresent}"> </c:if>>
									<img src="/cms-cps/newskiesm/images/logos/logo-amex.gif">
									<a href="javascript:void(0);" id="amexDetails" class="threeDSstickyOwner">
									<i class="caret link"></i>Learn more</a>
									 <%@ include file="amexLearnMoreSticky.jspf"%>
								</div>
								</c:when>
							</c:choose>
						</c:forEach>

							</div>
					</c:if>