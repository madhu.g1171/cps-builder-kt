<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <title>Payments - Error!!</title>

	  <link rel="stylesheet" type="text/css" href="/cms-cps/newskiesm/css/reset.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/newskiesm/css/global.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/newskiesm/css/flexibox.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/newskiesm/css/payment_details.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/newskiesm/css/footer.css"/>
      <!--[if IE 7]>
      <link rel="stylesheet" type="text/css" href="/cms-cps/newskies/css/styles-ie7.css"/>
      <![endif]-->
      <link rel="shortcut icon" href="/cms-cps/newskiesm/images/favicon.ico"/>
      <%@include file="javascript.jspf" %>
      <%@include file="tag.jsp"%>
   </head>
   <body>
   
						<c:if test="${hccIframeEnable eq 'false'}">
                        <c:choose>
                           <c:when test="${not empty bookingInfo.trackingData.sessionId}">
                              <%@include file="errorTechDifficulties.jspf"%>
                           </c:when>
                           <c:otherwise>
                              <%@include file="errorSessionTimeOut.jspf"%>
                           </c:otherwise>
                        </c:choose>		
							</c:if>
   </body>
</html>