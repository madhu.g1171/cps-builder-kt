<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>



		<%
				String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.mobile.thfo" , "");
				pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);
				String firstchoiceHomepage = ConfReader.getConfEntry("firstchoice.homepage.url" , "");
				pageContext.setAttribute("firstchoiceHomepage", firstchoiceHomepage, PageContext.REQUEST_SCOPE);
				String tuiBrochure = ConfReader.getConfEntry("tui.brochure.url" , "");
				pageContext.setAttribute("tuiBrochure", tuiBrochure, PageContext.REQUEST_SCOPE);
				%>
<div id="footer">
				<div id="call-us" class="hide">
					<div class="content-width">
						<i class="caret call white b"></i><h2 class="d-blue">BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203 636 1931</a></span></h2>
					</div>
				</div>
				<div id="search" class="b">
					<div class="content-width">
						<a href="#page" id="backtotop"><span>To top </span><i class="caret back-to-top white"></i></a>
					</div>
				</div>
				<div id="group" class="b thomson">
					<div class="content-width">
						<div class="copy">
						    <!-- <span id="world-of-tui"><img alt="World of TUI" src="/cms-cps/tuifalconfo/images/logo/wtui.png"></span> -->
							<p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p>
							<p>Registered address: Company Reg. No: 116977, One Spencer Dock, North Wall Quay, Dublin 1, Ireland.</p>
						</div>
						<div class="logos">
										<!--<span id="world-of-tui" title="World of TUI"></span> -->
							
									<a href="http://www.aviationreg.ie/" target="_blank" id="c-ar"
										title="Commission for Aviation Regulation">ROI</a>
								
									<!-- <span id="world-of-tui" title="World of TUI"></span>
									<a
										href="http://abta.com/go-travel/before-you-travel/find-a-member"
										id="logo-abta" title="ABTA - The Travel Association"></a>
									<a
										href="http://www.caa.co.uk/application.aspx?catid=490&pagetype=65&appid=2&mode=detailnosummary&atolnumber=2524"
										id="logo-atol" title="ATOL Protected"></a>
									 -->	
								
						</div>
                           <span class="commission">
                            Licenced by the Commission for Aviation Regulation T.O. 272
                           </span>
					</div>
				</div>
				<div id="terms">
					<div class="content-width">
						<p>
							<%-- <a href="http://www.thomson.co.uk/editorial/legal/about-thomson.html">About <c:out value="${textCapValue}"/></a> --%>
							<a href="https://www.tui.co.uk/destinations/info/my-tui-app">About TUI</a>
							<a href="https://www.tui.co.uk/destinations/info/my-tui-app">MyTUI app</a>
							<a href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies.html">Cookies policy</a>

							<a href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy Policy</a>
							<a href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms &amp; conditions</a>
							<c:choose>
								<c:when test="${applyCreditCardSurcharge eq 'true'}">
							<a href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Credit card fees</a>
								</c:when>
								<c:otherwise>
									<a href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Ways to Pay</a>
								</c:otherwise>
							</c:choose>
							<a href="http://www.thomson.co.uk/">Accessibility</a>
							<a class="desktopLinkHide" href="http://www.thomson.co.uk/">Desktop site</a>
							<a href="http://communicationcentre.thomson.co.uk/">Media Centre</a>

							<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>
							<a href="http://www.thomson.co.uk/affiliates.html">Affiliates</a>
							<a href="https://blog.tui.co.uk/">TUI Blog</a>
							<%-- <a href="http://www.thomson.co.uk/blog/"><c:out value="${textCapValue}"/> Blog</a> --%>

							<a target="_blank" href="http://www.tuigroup.com/en-en"><jsp:useBean id='CurrentDate12' class='java.util.Date'/>
         	<fmt:formatDate var='currentYear' value='${CurrentDate12}' pattern='yyyy'/>
			&copy; <c:out value="${currentYear}" />  TUI Group</a>
						  <%-- <a href="http://www.thomson.co.uk/destinations/info/thomson-credit-card"><c:out value="${textCapValue}"/> Credit Card</a> --%>
						  <a href="https://www.tui.co.uk/destinations/info/tui-credit-card">TUI Credit Card</a>
                          <a target="_blank"  href="${firstchoiceHomepage}">First Choice</a>
						<a target="_blank"  href="${tuiBrochure}">Holiday Brochures
						</a>

						</p>
					</div>
				</div>
			</div>

			<div id="disclaimer">
			<%=staySafeAbroad%>
				<div class="content-width disclaim two-columns">
				<c:if test="${bookingComponent.flightSummary.isThirdPartyFlight == 'true'}">
					<p>Some of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme.  But ATOL protection does not apply to all holiday and travel services listed on this website. This website will provide you with information on the protection that applies in the case of each holiday and travel service offered before you make your booking.  If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information, or for more information about financial protection and the ATOL Certificate go to:<a href="http://www.caa.co.uk/home/" target="_blank">www.caa.co.uk</a></p>
				</c:if>
				</div>
			</div>
