
		var errorMap = {
			    //On submit error messages
			   	firstnameSubmitErrorrequired:"Please enter a First Name",
			    surnameSubmitErrorrequired:"Please enter a Surname",
				houseNameSubmitErrorrequired: "Please enter your House Name/No",
				addressLine1SubmitErrorrequired: "Please enter a Street Address",
				citySubmitErrorrequired: "Please enter a Town/City",
				postCodeSubmitErrorrequired: "Please enter a Post Code",
				dayTimePhoneSubmitErrorrequired: "Please enter a Main Phone No",
				emailAddressSubmitErrorrequired: "Please enter an Email Address",
				confirmEmailAddressSubmitErrorrequired: "Please confirm your Email Address",
				emailAddressSubmitErrormatch: "The email addresses do not match",
				confirmEmailAddressSubmitErrormatch: "The email addresses do not match",
				cardTypeSubmitErrorrequired: "Please select a Card Type",
				expiryDateYYSubmitErrorrequired: "Please enter an Expiry Date",
				expiryDateMMSubmitErrorrequired: "Please enter an Expiry Date",
				cardNumberSubmitErrorrequired: "Please enter your Card Number",
				cardNameSubmitErrorrequired: "Please enter Name as it appears on the Card",
				securityCodeSubmitErrorrequired: "Please enter a Security Code",
				issueNumberSubmitErrorrequired: "Please enter a issue number",
				cardHouseNameSubmitErrorrequired: "Please enter your House Name/No",
				cardAddress1SubmitErrorrequired: "Please enter a Street Address",
				cardTownCitySubmitErrorrequired: "Please enter a Town/City",
				cardPostCodeSubmitErrorrequired_cardpostcode: "Please enter a Post Code",
				countySubmitErrorrequired: "Please enter a County",
				cardcountrySubmitErrorrequired: "Please select a Country",
				tourOperatorTermsAcceptedSubmitErrorrequired: "Please confirm you have read and accept the Terms & Conditions and Privacy Policy ",
				passengerTitle0SubmitErrorrequired: "Please select a Title",
				partSubmitErrorrequired: "Please enter valid amount",
				passengerTitle0BlurErrortitleEmpty: "Please select a Title",

				cardNumberBlurErrorcardNoEmpty: "Card Number field cannot be empty",
				cardNumberBlurErrorcardNoMinLength:"Card Number must be of 15 or 16 digits",
				cardNumberBlurErrorcardNoRegEx: "Card Number cannot be characters",
				cardNumberBlurErrorcardNoZero: "Invalid Card Number",

				securityCodeBlurErrorsecurityCodeEmpty: "Security Code field cannot be empty",
				cardNameBlurErrorcardNameEmpty: "Card Name field cannot be empty",
				firstnameBlurErrorfirstnameEmpty: "First Name field cannot be empty",
				surnameBlurErrorsurnameEmpty: "Last Name field cannot be empty",
				houseNameBlurErrorhouseNameEmpty: "House Name field cannot be empty",
				cityBlurErrorcityEmpty: "City field cannot be empty",
				countyBlurErrorcountyEmpty: "County field cannot be empty",
				postCodeBlurErrorpostcodeEmpty: "PostCode field cannot be empty",
				addressLine1BlurErroraddressLine1Empty: "Street Address field cannot be empty",

				//addressLine2BlurErrorrequired: "Please enter a Street Address2"
				postCodeBlurErrorrequired: "Please enter a valid Post Code",
				dayTimePhoneBlurErrorrequired: "Please enter a Main Phone No",
				emailAddressBlurErrorrequired: "Please enter an Email Address",
				confirmEmailAddressBlurErrorrequired: "Please confirm your Email Address",
				cardTypeBlurErrorrequired: "Please select a Card Type",
				expiryDateYYBlurErrorrequired: "Please enter an Expiry Date",
				expiryDateMMBlurErrorrequired: "Please enter an Expiry Date",
				cardNumberBlurErrorrequired: "Please enter your Card Number",
				cardNameBlurErrorrequired: "Please enter Name as it appears on the Card",
				securityCodeBlurErrorrequired: "Please enter a Security Code",
				cardHouseNameBlurErrorrequired: "Please enter your House Name/No",
				cardAddress1BlurErrorrequired: "Please enter a Street Address",
				cardTownCityBlurErrorrequired: "Please enter a Town/City",
				cardPostCodeBlurErrorrequired: "Please enter a valid Post Code",
				countryBlurErrorrequired: "Please select a Country",
				cardcountryBlurErrorrequired: "Please select a Country",
				tourOperatorTermsAcceptedBlurErrorrequired: "Please confirm you have read and accept the Terms & Conditions and Privacy Policy ",
				passengerTitle0BlurErrorTitleEmpty: "Please select a title",

				//On blur error messages
				firstnameBlurErroralpha:"Please enter a valid First Name",
			    surnameBlurErroralpha:"Please enter a valid Surname",
				houseNameBlurErroralphanumericspec: "Please enter a valid House Name/No",
				addressLine1BlurErrorstreetline1Check: "Please enter a valid Street Address",
				addressLine2BlurErrorstreetline1Check: "Please enter a valid Street Address",
				cityBlurErrortownorcityCheck: "Please enter a valid Town/City",
				countyBlurErrortestCounty: "Please enter a valid County",
				postCodeBlurErrorpostcode: "Please enter a valid Post Code",
				postCodeOptionalBlurErroralphanumeric: "Please enter a valid Post Code",
				dayTimePhoneBlurErrorphonenumber: "Please enter a valid Phone Number",
				mobilePhoneBlurErrorphonenumber: "Please enter a valid Phone Number",
				emailAddressBlurErroremail: "Please enter a valid Email Address",
				confirmEmailAddressBlurErroremail: "Please enter a valid Confirm Email Address",
				cardNumberBlurErrorcardnumber: "Please check your Card Number",
				cardNameBlurErroralpha: "Please enter a valid name on Card",
				expiryDateYYBlurErrorexpirydate: "Expiry Date - Has The Card Expired?",
				expiryDateMMBlurErrorexpirydate: "Expiry Date - Has The Card Expired?",
				securityCodeBlurErrorsecuritycode: "Please enter a valid Security Code",
				issueNumberBlurErrornumeric:"Please enter a valid Issue Number",
				issueNumberBlurErrorissuenumber:"Please enter a valid Issue Number",
				cardHouseNameBlurErroralphanumericspec: "Please enter a valid House Name/No",
				cardAddress1BlurErroralpha: "Please enter a valid Street Address",
				cardAddress2BlurErroralpha: "Please enter a valid Street Address",
				cardTownCityBlurErroralpha: "Please enter a valid Town/City",
				cardPostCodeBlurErrorcardpostcode: "Please enter a valid Post Code",
				cardCountyBlurErroralpha:"Please enter a valid County",

                //top error messages for server side validation errors
				houseNameTopServerError: "House Name/No",
				addressLine1TopServerError: "Street Address",
				cityTopServerError: "Town/City",
				countyTopServerError: "County",
				postCodeTopServerError: "Post Code",
				dayTimePhoneTopServerError: "Telephone Number",
				mobilePhoneTopServerError: "Phone Number",
				emailAddressTopServerError: "Email Address",
				cardHouseNameTopServerError: "CardHolder House Name/No",
				cardAddress1TopServerError: "CardHolder Street Address",
				cardTownCityTopServerError: "CardHolder Town/City",
				cardPostCodeTopServerError: "CardHolder Post Code",
				countryTopServerError: "Country",
				cardcountryTopServerError: "CardHolder Country",
                titleTopServerError:"Passenger Title",
                cardCountyTopServerError:"CardHolder County",

                // Field specific error messages
				houseNameServerError: "Please enter a valid House Name/No",
				addressLine1ServerError: "Please enter a valid Street Address",
				cityServerError: "Please enter a valid Town/City",
				countyServerError: "Please enter a valid County",
				postCodeServerError: "Please enter a valid Post Code",
				dayTimePhoneServerError: "Please enter a valid Phone Number",
				mobilePhoneServerError: "Please enter a valid Phone Number",
				emailAddressServerError: "Please enter a valid Email Address",
				confirmEmailAddressServerError: "Please enter a valid Email Address",
				cardHouseNameServerError: "Please enter a valid House Name/No",
				cardAddress1ServerError: "Please enter a valid Street Address",
				cardTownCityServerError: "Please enter a valid Town/City",
				cardPostCodeServerError: "Please enter a valid Post Code",
				countryServerError: "Please select a Country",
				cardcountryServerError: "Please select a Country",
				//cardCountyServerError:"CardHolder County",
			    titleServerError:"Passenger Title"
			};

		function InitiatizeValidation() {
			var form2 = jQuery('form').webForm({
				onSubmitErrorDisplay: function(event, args){
				   commonSubmitBlurErrorDisplay(args);
				},
				onBlurErrorDisplay: function(event, args){
				   commonSubmitBlurErrorDisplay(args);
     			},
				onBlurSucessfulDisplay: function(event, args){
					var field = jQuery(args[1]),
						fieldRow = field.closest('.row');
					console.log('args[1]:',args);
					if(args[1].value){
						fieldRow.find('.message').remove();
					  	fieldRow.removeClass("error valid").addClass('valid');
					}else{
						fieldRow.find('.message').remove();
						fieldRow.removeClass("error valid");
					}
     			},
				beforeSubmit: function(event, args){
					if(jQuery("."+ args +"Error #"+ args +"Error").length >1)
					{
			           jQuery("#"+ args +"Error").remove();
					   jQuery("#"+args+"TopError").remove();
					}
					if(args == "tourOperatorTermsAccepted" && jQuery("#"+args).val()== 'true')
					{
					   console.log('its terms & condition',args);
					   //jQuery("#"+ args +"Error").remove();
					   //jQuery("#"+args+"TopError").remove();
					   jQuery("#"+ args).closest('.row').removeClass('error');
					   jQuery("#"+ args).closest('.row').find('.message').text('');
					}
				    if(jQuery("."+ args +"Error p.formErrorMessage").length < 1)
				       jQuery("."+ args +"Error").removeClass("formError");

					if(jQuery("ul#topError li").length < 1)
					   jQuery("#errorSummary").addClass("hide");
					jQuery("#cardDetailsError").addClass("hide");
					forceRedraw();
				},
				beforeBlur: function(event, args){
					jQuery("#"+ args +"Error").remove();
					if(jQuery("li ."+ args +"Error p.formErrorMessage").length < 1)
                       jQuery("."+ args +"Error").removeClass("formError");
					jQuery("."+ args +"Error").removeClass("formSucess");
					jQuery("#"+args+"TopError").remove();
					if(jQuery("ul#topError li").length < 1)
					   jQuery("#errorSummary").addClass("hide");
					forceRedraw();
				},
				onSubmitLuhnErrorDisplay: function(event)
				{
				   jQuery("#cardDetailsError").removeClass("hide");
				   forceRedraw();
				},
				promoErrorDisplay: function(event,args)
				{
				   var id=args[0] + "Error";
				   if (jQuery("#promoCodeSectionformErrorMessage"))
				   {
				     jQuery("#promoCodeSection").removeClass("formError");
                     jQuery("#promoCodeSectionformErrorMessage").addClass("hide");
				     jQuery("#promoCodeSectionError").remove();
			         jQuery("#promoCodeSectionTopError").remove();
				   }
				   if (jQuery("#leadPassengerformErrorMessage"))
				   {
                     jQuery("#leadPassenger").removeClass("formError");
                     jQuery("#leadPassengerformErrorMessage").addClass("hide");
				     jQuery("#leadPassengerError").remove();
			         jQuery("#leadPassengerTopError").remove();
				   }
				   var topContainer = jQuery("#topError");
				   jQuery("#" + args[0]).addClass("formError");
				   var topText = 'Promotional Code';
				   var topContainer = jQuery("#topError");
				   jQuery("#errorSummary").removeClass("hide");
				   jQuery("<p class='formErrorMessage' id='"+id+"'>"+args[1]+"</p>").prependTo(jQuery("#" + args[0]));
                   jQuery("<li id='"+args[0]+"TopError'>"+topText+"</li>").appendTo(topContainer);
				   forceRedraw();

				   if (args[0] == 'leadPassenger')
				   {
				     jQuery("#promoCodeSection").addClass("formError");
                     jQuery("<p class='formErrorMessage' id='promoCodeSectionformErrorMessage'>"+'Please enter Lead Passenger Details'+"</p>").prependTo(jQuery("#promoCodeSection"));
				   }
			       jQuery("#commonError").addClass("hide");

				},

				promoSuccessDisplay: function(event, args)
				{
				  if(jQuery("ul#topError li").length > 1)
				  {
					jQuery("#promoCodeSectionError").remove();
				    jQuery("#promoCodeSectionTopError").remove();
				    jQuery("#leadPassengerError").remove();
			        jQuery("#leadPassengerTopError").remove();
				  }
				  else
				  {
					jQuery("#errorSummary").addClass("hide");
				  }
				  forceRedraw();
				  jQuery("#promoCodeText").addClass('hide');
				  jQuery("#promoCodeSection").addClass('hide');
				  jQuery("#promoCodeSuccessSection").removeClass('hide');
				  jQuery("#discountAmt").html(args[0]);
				}
			});
		}

		jQuery(document).ready(function (jQuery) {



			InitiatizeValidation();



			for (var i = 0; i < Personaldetails.noOfPassengers; i++)
			{
				errorMap["surName_"+i+"BlurErroralpha"]= "Please enter a valid Surname";
                errorMap["foreName_"+i+"BlurErroralpha"]= "Please enter a valid First Name";
				errorMap["surName_"+i+"SubmitErrorrequired"]= "Please enter a Surname";
				errorMap["foreName_"+i+"SubmitErrorrequired"]= "Please enter a First Name";
				errorMap["passengerTitle"+i+"SubmitErrorrequired"]="Please select a Title";


            }
			jQuery('form').webForm("addErrorMessage", errorMap);

    });
