

// function to open/close the summary pannel modal in mobile view

  function summaryPanelModal() {
	var scrollPos = 0;
	jQuery(".summary-panel-trigger" ).click(function() {
		scrollPos = jQuery(window).scrollTop();
		modalShow();
		intiateIscroll(jQuery(".scroll-container"));
	});
	jQuery(".modal.flyout .close").click(function() {
		modalHide();
		setTimeout(function() {
			window.scrollTo(0, scrollPos);
		}, 600);

	});

	function modalShow() {
		if(jQuery(".flyout").hasClass("hide-it") ){
			jQuery(".flyout").removeClass( "hide-it" );
		}
		jQuery(".flyout").css("display", "block");
		if(!jQuery(".flyout").hasClass("show-it") ){
			setTimeout(function() {
				jQuery(".flyout").addClass( "show-it" )
			}, 300);
		}
		if(!jQuery( "html" ).hasClass("modal-open show-bg")) {
			jQuery('html').addClass( "modal-open show-bg" );
		}
	}
	function modalHide() {
		if(jQuery(".flyout").hasClass("show-it") ){
				jQuery(".flyout").removeClass( "show-it" );
		}
		setTimeout(function() {
			jQuery(".flyout").css("display", "none");
			if(jQuery( "html" ).hasClass("modal-open show-bg")) {
				jQuery('html').removeClass( "modal-open show-bg" );
			}
		}, 600);
		if(!jQuery(".flyout").hasClass("hide-it") ){
				jQuery(".flyout").addClass( "hide-it" );
		}
	}
	jQuery( window ).resize(function() {
		// console.log(jQuery(window).width());
		if(jQuery(window).width() > 579) {

			if(jQuery(".flyout").hasClass("show-it") ){
				modalHide();
			}
		}
	});

}

// function to open/close modal

	function openModal(trigger) {

		var modal = jQuery("#"+jQuery(trigger).attr("data-modal-id"));

		if(jQuery(modal).hasClass("out")) {
			jQuery(modal).removeClass( "out" );
		}

		jQuery(modal).addClass( "show" );

		if(!jQuery(modal).hasClass("in") ){
			setTimeout(function() {
				jQuery(modal).addClass( "in" )
			}, 300);
		}
	}

	function closeModal(trigger) {
		var modal = jQuery("#"+jQuery(trigger).attr("data-modal-id"));

		if(jQuery(modal).hasClass("in")) {
			jQuery(modal).removeClass( "in" );
		}
		jQuery(modal).addClass( "out" );

		if(jQuery(modal).hasClass("show") ){
			setTimeout(function() {
				jQuery(modal).removeClass( "show" )
			}, 300);
		}
	}

	window.datarules = window.datarules || {};

	function dataProSubmit() {
		var modal = jQuery("#data-protection");

		var checkboxes = jQuery(".radio.check", modal);
		$(checkboxes).each(function(index, checkbox) {
			if(jQuery(checkbox).hasClass("active")) {
				datarules[jQuery(checkbox).attr("name")] =  jQuery(checkbox).attr("id");
			}
			else {
				delete datarules[jQuery(checkbox).attr("name")];
			}
		});
		console.log(datarules);
	}

// function for add/change address

function changeAddr() {
/*
 * jQuery("#changeAddr").click(function() { showEditAddr();
 * InitiatizeValidation(); showDiv();
 * jQuery("#changeAddressCheckBox").val("true");
 *
 * }); jQuery("#unchangeAddr").click(function() { hideEditAddr();
 * InitiatizeValidation(); resetAddressFields();
 * jQuery("#changeAddressCheckBox").val("false"); });
 */
	function parseBoolean(str) {
		return /true/i.test(str);
	}
	jQuery(".click-area").click(function() {
	var hiddenElement = jQuery(".click-area")[0].previousElementSibling;
	hiddenElement.value = ""+ (!parseBoolean(hiddenElement.value));
		if(hiddenElement.value == "false"){
			showEditAddr();
			InitiatizeValidation();
			showDiv();
			jQuery("#changeAddressCheckBox").val("true");
		}else{

			hideEditAddr();
			InitiatizeValidation();
			resetAddressFields();
			jQuery("#changeAddressCheckBox").val("false");

		}

	});

}
function showEditAddr() {

	jQuery("#edit-addr").removeClass("hide");
	jQuery("#edit-addr").addClass("show");
	jQuery("#default-addr").addClass("hide");
	jQuery("#default-addr").removeClass("show");
	addReqTags();
}
function hideEditAddr() {
	removeReqTags();
	jQuery("#edit-addr").removeClass("show");
	jQuery("#edit-addr").addClass("hide");
	jQuery("#default-addr").removeClass("hide");
	jQuery("#default-addr").addClass("show");

}
function addReqTags() {
	jQuery("#passengerTitle0").attr("gfv_required","required");
	jQuery("#country").attr("gfv_required","required");
	jQuery("#firstname").attr("gfv_required","required");
	jQuery("#surname").attr("gfv_required","required");
	jQuery("#houseName").attr("gfv_required","required");
	jQuery("#addressLine1").attr("gfv_required","required");
	jQuery("#city").attr("gfv_required","required");
	jQuery("#postCode").attr("gfv_required","required");
}
function removeReqTags() {
	jQuery("#passengerTitle0").removeAttr("gfv_required");
	jQuery("#country").removeAttr("gfv_required");
	jQuery("#firstname").removeAttr("gfv_required");
	jQuery("#surname").removeAttr("gfv_required");
	jQuery("#houseName").removeAttr("gfv_required");
	jQuery("#addressLine1").removeAttr("gfv_required");
	jQuery("#city").removeAttr("gfv_required");
	jQuery("#postCode").removeAttr("gfv_required");
}


// function for radio buttons card seletion
	 function radioGrpInit() {
	var radioGrp = jQuery(".pay-modes .pay-box");
	var radioGrpDep = jQuery(".deposite-box");

	radioGrpDep.each(function(rindex, radio) {
		jQuery(radio).click(function() {
			toggleRadio(this, radioGrpDep);

		});
	});

	radioGrp.each(function(rindex, radio) {
		jQuery(radio).click(function() {
			toggleRadio(this, radioGrp);

		});
	});
	function toggleRadio(ele, radioGroup) {
		var rDom = jQuery(".radio", ele);
			var rBox = jQuery(".box", ele);
			var rSelectText = jQuery(".rtext", ele);

				uncheckAllRadio(radioGroup);
				checkRadio(rDom,rBox,rSelectText);
				displayCreditCharges(jQuery(ele).attr('id'));

	}
	function checkRadio(element,divDom,innerHt){
      element.addClass('active');
	  divDom.addClass('active');
	  innerHt.text("SELECTED");
    }
	function unCheckRadio(element,divDom,innerHt) {
      element.removeClass('active');
	  divDom.removeClass('active');
	  innerHt.text("SELECT");
    }
	function isCheckedRadio(element) {
		return element.hasClass("active");
    }
	function uncheckAllRadio(radioDivision) {
		jQuery(radioDivision).each(function(radio){
			var rDom = jQuery(".radio", this);
			var rBox = jQuery(".box", this);
			var rSelectText = jQuery(".rtext", this);
			if(isCheckedRadio(rDom)){
				unCheckRadio(rDom,rBox,rSelectText);
			}
		});
    }
}

/**
 * * This method handles 1. Displaying of payment method 2. Displaying the final
 * Amount 3. Card Type drop down toggling based on the condition
 */
function displayCreditCharges(id) {
    var getId = id;
	var getDiv = document.getElementById(id);
	var divClass = document.getElementsByClassName("paycard-details");
	var debitSection = document.getElementById("debitcardType");
	var creditSection = document.getElementById("creditcardType");
	var giftSection = document.getElementById("giftcardType");
	var creditCharges= document.getElementById("creditCardCharges");
	var debitCharges = document.getElementById("debitCardCharges");
	var giftCharges = document.getElementById("giftCardCharges");
	var thomsonCardCharges = document.getElementById("thomsonCardCharges");
	var cardNumber = "";
	try{
		if(document.getElementById("cardNumber"))cardNumber = document.getElementById("cardNumber").value;
		if(document.getElementById("card_number"))cardNumber = document.getElementById("card_number").value;
	}catch(ex){console.log(ex);}
	var thomsonCardsList = null;
	if(ThomsonCreditCard != null && ThomsonCreditCard != ""){
	  thomsonCardsList = ThomsonCreditCard.split(',');
	}

	// Reset to the default value
	// document.getElementById('payment_type_credit').value = "PleaseSelect";
	// document.getElementById('payment_type_debit').value = "PleaseSelect";
	jQuery("#cardType").each(function(){
		jQuery(this).val('PleaseSelect');
	});
	// payment method change
	if(applyCreditCardSurcharge=='true'){
		var creditCardAmt = document.getElementById("creditCardChargeAmt_fullCost").innerHTML;
	}
	//var debitCardAmt = document.getElementById("debitCardChargeAmt_fullCost").innerHTML;
	//var giftCardAmt = document.getElementById("giftCardChargeAmt_fullCost").innerHTML;
	var paypalCharges = document.getElementById("payPalCharges");
	var cardpaymentCharges = document.getElementById("cardPaymentCharges");

	if (getId == 'creditcardType') {
		if(null != thomsonCardsList){
			for(var i=0;i< thomsonCardsList.length; i++){
				 if(cardNumber != null && cardNumber != "" && cardNumber.startsWith(thomsonCardsList[i])){
				   thomsonCardCharges.style.display = "block";
				   document.getElementById("thomsoncard").style.display = "";
				   document.getElementById("thomsoncard").innerHTML = "Includes no extra charge for using TUI Credit Card.";
				   document.getElementById("thomsoncredit-text").style.display = "none";
				   document.getElementById("thomsoncredit-text").innerHTML = "";
				   document.getElementById("cardCharges").style.display = "none";
				   document.getElementById("credit-text").style.display = "none";
				   paypalCharges.style.display = "none";
				   cardpaymentCharges.style.display = "none";
				   debitCharges.style.display = "none";
				   creditCharges.style.display = "none";
				   giftCharges.style.display = "none";
				   break;
				}else{
				   creditCharges.style.display = "block";
				   cardpaymentCharges.style.display = "none";
				   paypalCharges.style.display = "none";
				   debitCharges.style.display = "none";
				   giftCharges.style.display = "none";
				   thomsonCardCharges.style.display = "none";
				   document.getElementById("cardCharges").style.display = "";
				   document.getElementById("credit-text").style.display = "";
				   document.getElementById("thomsoncard").style.display = "none";
				   document.getElementById("thomsoncredit-text").style.display = "none";
				}}}else{
					creditCharges.style.display = "block";
					cardpaymentCharges.style.display = "none";
					paypalCharges.style.display = "none";
				    debitCharges.style.display = "none";
				    giftCharges.style.display = "none";
					thomsonCardCharges.style.display = "none";
	        }
			// creditSection.className += " highlighted-div";
			debitSection.className = "paycard-details";
			giftSection.className = "paycard-details";
			// document.getElementById("creditCardChargeFinalAmt").innerHTML=creditCardAmt;
			jQuery('#cardtype').val('Credit');

		} else if (getId == 'debitcardType') {
			debitCharges.style.display = "block";
			creditCharges.style.display = "none";
			giftCharges.style.display = "none";
			thomsonCardCharges.style.display = "none";
			paypalCharges.style.display = "none";
			cardpaymentCharges.style.display = "none";
			for(var i=0;(null != thomsonCardsList && i< thomsonCardsList.length); i++){
				if(cardNumber != null && cardNumber != "" && cardNumber.startsWith(thomsonCardsList[i])){
				document.getElementById("thomsoncard").style.display = "";
				document.getElementById("thomsoncard").innerHTML = "Includes no extra charge for using TUI Credit Card.";
				document.getElementById("thomsoncredit-text").style.display = "none";
				document.getElementById("thomsoncredit-text").innerHTML = "";
				document.getElementById("cardCharges").style.display = "none";
				document.getElementById("credit-text").style.display = "none";
				break;
			}else{
				creditCharges.style.display = "none";
				debitCharges.style.display = "block";
				giftCharges.style.display = "none";
				paypalCharges.style.display = "none";
				cardpaymentCharges.style.display = "none";
				thomsonCardCharges.style.display = "none";
				document.getElementById("cardCharges").style.display = "";
				document.getElementById("credit-text").style.display = "";
				document.getElementById("thomsoncard").style.display = "none";
				document.getElementById("thomsoncredit-text").style.display = "none";
			}}
			// debitSection.className += " highlighted-div";
			creditSection.className = "paycard-details";
			giftSection.className = "paycard-details";
			// document.getElementById("debitCardFinalAmt").innerHTML=debitCardAmt;
			jQuery('#cardtype').val('Debit');

		} else if (getId == 'giftcardType') {
		    debitCharges.style.display = "none";
			giftCharges.style.display = "block";
			creditCharges.style.display = "none";
			thomsonCardCharges.style.display = "none";
			paypalCharges.style.display = "none";
			cardpaymentCharges.style.display = "none";
	     for(var i=0;(null != thomsonCardsList && i< thomsonCardsList.length); i++){
				if(cardNumber != null && cardNumber != "" && cardNumber.startsWith(thomsonCardsList[i])){
				document.getElementById("thomsoncard").style.display = "";
				document.getElementById("thomsoncard").innerHTML = "Includes no extra charge for using TUI Credit Card.";
				document.getElementById("thomsoncredit-text").style.display = "none";
				document.getElementById("thomsoncredit-text").innerHTML = "";
				document.getElementById("cardCharges").style.display = "none";
				document.getElementById("credit-text").style.display = "none";
				break;
			}else{
				   creditCharges.style.display = "none";
				   debitCharges.style.display = "none";
				   giftCharges.style.display = "block";
				   thomsonCardCharges.style.display = "none";
				   paypalCharges.style.display = "none";
				   cardpaymentCharges.style.display = "none";
				document.getElementById("cardCharges").style.display = "";
				document.getElementById("credit-text").style.display = "";
				document.getElementById("thomsoncard").style.display = "none";
				document.getElementById("thomsoncredit-text").style.display = "none";
			}}
			// giftSection.className += " highlighted-div";
			creditSection.className = "paycard-details";
			debitSection.className = "paycard-details";
			// document.getElementById("giftCardFinalAmt").innerHTML=giftCardAmt;
			jQuery('#cardtype').val('Gift');
		}

	if ((document.getElementById('debitPaymentTypeCode') && document.getElementById('debitPaymentTypeCode').style.display == 'block')
			|| (document.getElementById('creditPaymentTypeCode') && document.getElementById('creditPaymentTypeCode').style.display == 'block')
			|| (document.getElementById('giftPaymentTypeCode') && document.getElementById('giftPaymentTypeCode').style.display == 'block')) {
		if (getId == 'creditcardType') {
			document.getElementById('debitPaymentTypeCode').style.display = 'none';
			document.getElementById('creditPaymentTypeCode').style.display = 'block';
			document.getElementById('giftPaymentTypeCode').style.display = 'none';
		} else if (getId == 'debitcardType') {
			document.getElementById('debitPaymentTypeCode').style.display = 'block';
			document.getElementById('creditPaymentTypeCode').style.display = 'none';
			document.getElementById('giftPaymentTypeCode').style.display = 'none';
		} else {
			document.getElementById('debitPaymentTypeCode').style.display = 'none';
			document.getElementById('creditPaymentTypeCode').style.display = 'none';
			document.getElementById('giftPaymentTypeCode').style.display = 'block';
		}
	}
}

function displayCardChargesPaypal(id) {
	var getId = id;
	var getDiv = document.getElementById(id);
	var divClass = document.getElementsByClassName("paycard-details");
	var creditCharges= document.getElementById("creditCardCharges");
	var debitCharges = document.getElementById("debitCardCharges");
	var giftCharges = document.getElementById("giftCardCharges");
	var cardNumber = "";
	try{
		if(document.getElementById("cardNumber"))cardNumber = document.getElementById("cardNumber").value;
		if(document.getElementById("card_number"))cardNumber = document.getElementById("card_number").value;
	}catch(ex){console.log(ex);}
	if(document.getElementById("payPalChargeAmt_fullCost")){var payPalAmt = document.getElementById("payPalChargeAmt_fullCost").innerHTML;}
        var paypalSection = document.getElementById("paypalType");
	var paypalCharges = document.getElementById("payPalCharges");
	var cardpaymentCharges = document.getElementById("cardPaymentCharges");
	var cardpaymentSection = document.getElementById("cardpaymentType");
//	var cardLicenseTermsSection = document.getElementById("cardLicense");
//	var paypalLicenseTermsSection = document.getElementById("paypalLicense");

	jQuery("#cardType").each(function(){
		jQuery(this).val('PleaseSelect');
	});
	// payment method change
	jQuery("#ddcomponent-hide").addClass("hide");
	if (getId == 'cardpaymentType') {
		jQuery("#ddcomponent-hide").removeClass("hide");
		cardpaymentCharges.style.display = "none";
		$('CheckoutPaymentDetailsForm').action="javascript:makePayment();";
		debitCharges.style.display = "block";
		creditCharges.style.display = "none";
		giftCharges.style.display = "none";
		paypalCharges.style.display = "none";
		paypalSection.className = "paycard-details";
		jQuery('#cardtype').val('CardPayment');
		var cardSectionElement = document.getElementById('card-details-hide');
		var isHidden = cardSectionElement.classList.contains('hide');
	        isHidden ? cardSectionElement.classList.remove('hide') : '';
		var paymentButton =document.getElementById('paypalbutton');
		if(paymentButton){
			if (typeof lockYourPriceSummarySelected !== 'undefined'
			&& lockYourPriceSummarySelected == 'true'
			&& typeof isLockYourPriceMMBFlow !== 'undefined'
			&& !(isLockYourPriceMMBFlow == 'true')){
					paymentButton.value = 'LOCK THIS PRICE';
					paymentButton.className="book-flow button large cta lockbutton";
					paymentButton.type='submit';
			}else{
					paymentButton.value = 'Book this Holiday';
			}
		}
		jQuery('.card-details-section').find('input[type=text],input[type=tel]').val('');
		/*
		 * document.getElementById("expiryDateMM").value = "";
		 * document.getElementById("monthspan").innerHTML =
		 * document.getElementById("expiryDateMM").options[0].text;
		 * document.getElementById("expiryDateYY").value = "";
		 * document.getElementById("yearspan").innerHTML =
		 * document.getElementById("expiryDateYY").options[0].text;
		 * document.getElementById("card-img").className = "";
		 */
		jQuery("#card-details-hide .row").removeClass("valid");
		jQuery("#card-details-hide .row").removeClass("error");
		jQuery("#card-details-hide .message").remove();
			if(document.getElementById('paypalbutton')){
			if(clientApp == 'TUIFC '|| clientApp == 'FIRSTCHOICEMOBILE ') {
				document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_this_holiday_button_fc.png';
			} else if(clientApp == 'TUITHFO '|| clientApp == 'TUIFALCONFO ') {
				document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_this_flight_button.png';
			} else {
					document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_this_holiday_button.png';
			}
		}
		jQuery("#card-details-hide").removeClass("hide");
		jQuery("#paypalTandC").addClass("hide");
		//cardLicenseTermsSection.style.display = "block";
		//paypalLicenseTermsSection.style.display = "none";

	} else if (getId == 'paypalType') {
		var paypalButton =document.getElementById('paypalbutton');
		if(jQuery(".card-details-section  .row.c.marg-bottom-25").length !=0){
			jQuery(".card-details-section  .row.c.marg-bottom-25").removeClass("error");
		}
		if(paypalButton){
			paypalButton.className="";
			paypalButton.type='image';
			if(document.getElementById('paypalbutton')){
				if (clientApp == 'TUIFC '|| clientApp == 'FIRSTCHOICEMOBILE ') {
					document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_with_pplogo_fc.png';
				} if (clientApp == 'TUICS '|| clientApp == 'TUIES ') {
					document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_with_pplogo_cs.png';
				} else {
					document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_with_pplogo.png';
				}
			}
		}

		jQuery("#card-details-hide").addClass("hide");
		jQuery("#paypalTandC").removeClass("hide");
		$('CheckoutPaymentDetailsForm').action = "./paypal?token="+token + tomcatInstance;
		if(jQuery("#card-details-hide").length){
			jQuery("#card-details-hide").addClass("hide");
		}
		cardpaymentCharges.style.display = "none";
		paypalCharges.style.display = "block";
		debitCharges.style.display = "none";
		creditCharges.style.display = "none";
		giftCharges.style.display = "none";
		cardpaymentSection.className = "paycard-details";
		jQuery('#cardtype').val('PayPal');
		//cardLicenseTermsSection.style.display = "none";
		//paypalLicenseTermsSection.style.display = "block";
	}
	else if (getId == 'payPalCredit') {
		var payPalCreditDisplay = document.getElementById("payPalCreditDisplay");
		jQuery("#card-details-hide").addClass("hide");
		jQuery("#paypalTandC").removeClass("hide");

		$('CheckoutPaymentDetailsForm').action = "./paypal?token="+token + tomcatInstance;
		if(jQuery("#card-details-hide").length){
			jQuery("#card-details-hide").addClass("hide");
		}
		cardpaymentCharges.style.display = "none";
		try {
			payPalCreditDisplay.style.display = "block";
			} catch (error) {
				console.log(error);
		}
		
		debitCharges.style.display = "none";
		creditCharges.style.display = "none";
		giftCharges.style.display = "none";
		if(cardpaymentSection){cardpaymentSection.className = "paycard-details";}
		jQuery('#cardtype').val('payPalCredit');
	}
}

function displayDDcharges(id){
	var getmode = id;
	var depositamount = document.getElementById(id).getAttribute('depositamount');
	var paypalSelected=document.getElementById("paypal");
	if(getmode == 'quarter-amt-dd' && depositamount == 0){
	   if(jQuery("#card-details-hide").length){
				jQuery("#card-details-hide").addClass("hide");
				jQuery("#paypalTandC").removeClass("hide");
	   }
		jQuery(".new-card-details-section").addClass("hide");
		jQuery("#ddcomponent-hide").addClass("hide");
		jQuery("#payment_mode_title").addClass("hide");
		$('CheckoutPaymentDetailsForm').action="javascript:makePayment();";
		document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/DD-button.png';
	}
	else if(getmode == 'payPalCredit'){
		jQuery("#card-details-hide").addClass("hide");
		jQuery(".new-card-details-section").addClass("hide");
		jQuery("#ddcomponent-hide").addClass("hide");
		if(jQuery("#payment_mode_title")){
			jQuery("#payment_mode_title").addClass("hide");
		}
		$('CheckoutPaymentDetailsForm').action="javascript:makePayment();";
		jQuery("#card-details-hide").addClass("hide");
		jQuery('#funding_source').val('funding_source');
		$('CheckoutPaymentDetailsForm').action = "./paypal?token="+token + tomcatInstance;
		document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/payPalCredit.png';
	}
	else if(getmode == 'quarter-amt' || getmode == 'full-amt' || getmode == 'half-amt' || (getmode == 'quarter-amt-dd' && depositamount != 0 )){
		jQuery("#card-details-hide").removeClass("hide");
		jQuery("#paypalTandC").addClass("hide");
		jQuery(".new-card-details-section").removeClass("hide");
		jQuery("#ddcomponent-hide").removeClass("hide");
		jQuery("#payment_mode_title").removeClass("hide");
		$('CheckoutPaymentDetailsForm').action="javascript:makePayment();";
		if (clientApp == 'TUIFC ') {
			 document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_this_holiday_button_fc.png';
		} else if(clientApp == 'TUITHFO '|| clientApp == 'TUIFALCONFO ') {
			document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_this_flight_button.png';
		} else {
		document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_this_holiday_button.png';
			 }
		if(paypalSelected != null && paypalSelected.hasClassName('active')) {
			jQuery("#card-details-hide").addClass("hide");
			jQuery("#paypalTandC").removeClass("hide");
			jQuery("#ddcomponent-hide").addClass("hide");
			$('CheckoutPaymentDetailsForm').action = "./paypal?token="+token + tomcatInstance;
			 if (clientApp == 'TUIFC ') {
				 document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_with_pplogo_fc.png';
			} else if(clientApp == 'TUITHFO '|| clientApp == 'TUIFALCONFO ') {
				document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_this_flight_button.png';
			} else {
			document.getElementById('paypalbutton').src = '/cms-cps/tuicommon/images/logo/Book_with_pplogo.png';
		}
	}
	}
}
function displayPayPalCreditCharges(){
	var cardpaymentSection = document.getElementById("cardpaymentType");
	var debitCharges = document.getElementById("debitCardCharges");
	var payPalCreditDisplay = document.getElementById("payPalCreditDisplay");
	debitCharges.style.display = "none";
	payPalCreditDisplay.style.display = "block";
	jQuery('#cardtype').val('PayPal');
}
// function to intiate the Iscroll
function intiateIscroll(element) {
	jQuery(element).each (function(scrollDivIndex, scrollDiv) {
		var ul = jQuery('ul:first-child', scrollDiv);
		var dynamicWidth = JSON.parse(jQuery.attr(scrollDiv, 'data-scroll-dw') || "false");

		if(ul[0] && dynamicWidth) {
			var w = calculateWidth(jQuery("li",ul)) +"px";
			jQuery(ul).css("width", w);
		}

		var scroller = new IScroll(scrollDiv, JSON.parse(jQuery.attr(scrollDiv, 'data-scroll-options') ));
		if(jQuery(scrollDiv).attr('id') == 'breadcrumb') {
			breadcrumbScroll = scroller;
			setTimeout(function() {
				scrollintoView(scrollDiv);
				}, 1000);
		}
	});

	function calculateWidth(xs) {
		var width = 0;
		jQuery(xs).each(function(elIndex, el){
			width+= Math.ceil(jQuery(el)[0].getBoundingClientRect().width);
		});
		return width;
	}

}

function scrollintoView(scrollDiv) {
	if(breadcrumbScroll){
		var curNav = jQuery('li.active', scrollDiv)[0];
		breadcrumbScroll.scrollToElement(curNav, 1000, 20, 0, IScroll.utils.ease.quadratic);
	}
}

function enableSelectBoxes(){
	jQuery('div.select').each(function(){
		var jQuerytxt = jQuery(this).children('.text'),
			jQueryselect = jQuery(this).children('select');

		// onload set the selected value
		if(!jQuery(this).attr('selectedIndex')==0){
			jQuerytxt.text(jQuery(this).find('option:selected').text());
		}else{
			jQuerytxt.text(jQuery(this).find('option:selected').text());
		}
		jQueryselect.change(function() {
			if(!jQuery(this).attr('selectedIndex')==0){
				jQuerytxt.text(jQuery(this).find('option:selected').text());
			}else{
				jQuerytxt.text(jQuery(this).find('option:selected').text());
			}
		});

	});
}

function enableCheckBoxes() {
	jQuery(".radio.check").each(function(cIndex, cbox) {
		jQuery(cbox).click(function() {
			if(jQuery(cbox).hasClass("active")) {
				jQuery(cbox).removeClass("active");
			}
			else {
				jQuery(cbox).addClass("active");
			}
		});
	});
}

// tooltips

function initTooltip() {
	jQuery("a.tooltip").each(function(tindex, trigger) {
		var id="tooltip"+tindex;
		jQuery(trigger).attr("data-tooltip-id", id);
		jQuery(trigger).click(function(event) {
			var triggId = jQuery(trigger).attr("data-tooltip-id");
			var text = jQuery(trigger).attr("data-tooltip-text");
			var tooptip = jQuery("div.tooltip[data-tooltip-id='" + triggId + "']");
			if(tooptip[0] == null) {
				tooptip = createTooltip(triggId, text);
				toggelTootlip(trigger,triggId,tooptip);
			}
			else {
				toggelTootlip(trigger,triggId,tooptip);
			}
			event.stopPropagation();
		});

		jQuery(trigger).hover(function(event) {
			var triggId = jQuery(trigger).attr("data-tooltip-id");
			var text = jQuery(trigger).attr("data-tooltip-text");
			var tooptip = jQuery("div.tooltip[data-tooltip-id='" + triggId + "']");
			if(tooptip[0] == null) {
				tooptip = createTooltip(triggId, text);
				toggelTootlip(trigger,triggId,tooptip);
			}
			else {
				toggelTootlip(trigger,triggId,tooptip);
			}
			event.stopPropagation();
		});
	});

	function createTooltip(triggId, text) {
		var tooltipTmpl = jQuery("#tooltipTmpl").clone();
		jQuery(tooltipTmpl).attr("data-tooltip-id", triggId);
		jQuery(tooltipTmpl).removeAttr("id");
		jQuery("p", tooltipTmpl).html(text);
		jQuery("body").append(jQuery(tooltipTmpl));
		return tooltipTmpl;
	}
	function toggelTootlip(trigger,triggId,tooptip) {
		if((jQuery(tooptip).css("display") == "none") || jQuery(tooptip).css("display") == "") {
			var pos = jQuery(trigger).offset();
			var marLeft = parseInt(jQuery(trigger).css("margin-left").replace("px", ""));
			var marRight = parseInt(jQuery(trigger).css("margin-right").replace("px", ""));
			var marTop = parseInt(jQuery(trigger).css("margin-top").replace("px", ""));
			var marBottom = parseInt(jQuery(trigger).css("margin-bottom").replace("px", ""));
			var top = pos.top+(jQuery(trigger).outerHeight()) +marTop + marBottom+5+"px";
			var left = pos.left+((jQuery(trigger).outerWidth())/2)+ marLeft+marRight - ((jQuery(tooptip).outerWidth())/2) +"px";

			jQuery(".tp").each(function(tindex, tp){
				jQuery(tp).css("display","none");
			});
			jQuery(tooptip).css({"top":top, "left":left, "display":"block"});
		}
		else {
			jQuery(tooptip).css("display", "none");
		}
	}
	jQuery("body").click(function() {
		jQuery("div.tooltip").css("display","none");
	});

	document.body.addEventListener('touchstart',function (){
		jQuery("div.tooltip").css("display","none");
	});

	jQuery( window ).resize(function() {
	  jQuery("div.tooltip").css("display","none");
	});
}

function resizeIframeLayout(domId) {
	var iframe = document.getElementById(domId);
	try {
		jQuery('#card-details-hide').height(iframe.contentWindow.window.document.body.offsetHeight + 25);
	} catch (error) {
		console.log(error);
	};
}