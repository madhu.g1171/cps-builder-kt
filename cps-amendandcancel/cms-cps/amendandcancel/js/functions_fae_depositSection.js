updateChargesForDeposits();
/*
 * Updates the card charge amounts for each of the deposit types.
 */
function updateChargesForDeposits()
{
  var depositTypes = $$('input[name=depositType]');
  var deposCardChargeDivs =  $$('#creditCardChargeAmt');
  var depositAmount = $$('#depositAmt');
  var cardCharge = null;
  var check_length=3;
  for(var i=0; i<check_length; i++)
  {

	  if(depositTypes[i]!= null){
		    depositAmount = (depositTypes[i].value)?depositAmountsMap.get(depositTypes[i].value): depositAmountsMap.get("fullCost");
		    cardCharge = calculateCardCharge(depositAmount);
		    cardCharge = parseFloat(depositAmount) + parseFloat(cardCharge);
		    if(applyCreditCardSurcharge=='true'){
		    document.getElementById("creditCardChargeAmt_"+depositTypes[i].value+"").innerHTML =parseFloat(cardCharge).toFixed(2);
		    	//if(deposCardChargeDivs[i]){
		    		//deposCardChargeDivs[i].innerHTML = parseFloat(cardCharge).toFixed(2);
		    	//}
		    }
	  }
  }
}

/**
 ** This method calculates the card charge for the deposit amount.
**/
function calculateCardCharge(amount)
{
  var applicableCharges= new Array();
  var cardCharge=0.0;

    applicableCharges = configurableCardCharge.split(",");


    if(applicableCharges!=null)
    {
     if(applicableCharges[0] > 0.0)
      {
      cardCharge = parseFloat(amount * (applicableCharges[0]/100));
    }
       if (parseFloat(applicableCharges[1]) > 0.0 && parseFloat(cardCharge) < parseFloat(applicableCharges[1]))
      {
        cardCharge = parseFloat(applicableCharges[1]);
      }
      if (parseFloat(applicableCharges[2]) > 0.0 && parseFloat(cardCharge) > parseFloat(applicableCharges[2]))
      {
         cardCharge = parseFloat(applicableCharges[2]);
      }
    }
    return parseFloat(Math.round(cardCharge * 100) / 100).toFixed(2);
}


/**
 ** Refreshes the PaymentInfo. A central function responsible for
 ** keeping data integrity of different amounts in
 ** in this payment for client side validation.
**/
function updatePaymentInfo(selectedDepositType)
{
PaymentInfo.depositType = trimSpaces((selectedDepositType));
   
  if( typeof lockYourPriceSummarySelected !== 'undefined' && lockYourPriceSummarySelected =='true' && typeof isLockYourPriceMMBFlow !== 'undefined'&& !(isLockYourPriceMMBFlow=='true')){
		PaymentInfo.selectedDepositAmount=lockYourPriceSummaryTotalPrice && lockYourPriceSummaryTotalPrice !== null ? lockYourPriceSummaryTotalPrice : "00.00";
		UpdatePaymentTitleMode(PaymentInfo.depositType);
	}else{
		
		 if(typeof isLockYourPriceMMBFlow !== 'undefined'&& !(isLockYourPriceMMBFlow=='true')){
			if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined){
					PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
			}else{
			PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
			}
		}else{
			PaymentInfo.selectedDepositAmount = PaymentInfo.depositType == "fullCost" ?  PaymentInfo.payableAmount :  depositAmountsMap.get(PaymentInfo.depositType)
		 }
		 switchTab(PaymentInfo.depositType);
		 UpdatePaymentTitleMode(PaymentInfo.depositType);
		 payPalCreditDisplayupdate(selectedDepositType);
	}
   
   calCardCharge  = calculateCardCharges(PaymentInfo.selectedDepositAmount);

   PaymentInfo.totalCardCharge = calCardCharge;
   PaymentInfo.calculatedPayableAmount = roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2); //parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge).toFixed(2);
   PaymentInfo.calculatedTotalAmount = roundOff((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge , 2); //parseFloat((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge).toFixed(2);

   var totalCharge = parseFloat(PaymentInfo.selectedDepositAmount) + parseFloat(calCardCharge);
   document.getElementById("creditCardChargeFinalAmt").innerHTML = totalCharge.toFixed(2);
   if(PaymentInfo.selectedDepositAmount != 0){
      		document.getElementById("debitCardCurrency").innerHTML = "&euro;";
      		document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
//      		document.getElementById("debitCardText").innerHTML	= " by Debit Card";
   	}else{
   		document.getElementById("debitCardCurrency").innerHTML = "";
   		document.getElementById("debitCardFinalAmt").innerHTML = "";
//   		document.getElementById("debitCardText").innerHTML ="by Direct Debit";
   	}
   document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   document.getElementById("cardPaymentFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   if(PaymentInfo.selectedDepositAmount != 0){
      		document.getElementById("paypalCurrency").innerHTML = "&euro;";
      		 document.getElementById("payPalFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
//      		document.getElementById("paypalCardText").innerHTML	= " by PayPal";
   	}else{
   		document.getElementById("paypalCurrency").innerHTML = "";
   		document.getElementById("payPalFinalAmt").innerHTML = "";
//   		document.getElementById("paypalCardText").innerHTML ="by Direct Debit";
		 }
}

function selectedpaymentMode(id) {
	var getmode = id;
	var getfullDeposit = document.getElementById("full-amt");
	var gethalfDeposit = document.getElementById("half-amt");
	var getlowDeposit = document.getElementById("quarter-amt");
	var depositClass = document.getElementsByClassName("payment-details");
	if (getmode == "full-amt") {

		var fullCheck = document.getElementById("full");
		if(fullCheck){
			fullCheck.checked = "checked";
		}
		

	} else if (getmode == "half-amt") {

		var halfCheck = document.getElementById("half");
		if(halfCheck){
			halfCheck.checked = "checked";
		}
		

	} else {
		var lowdepositCheck = document.getElementById("initial");
		if(lowdepositCheck){
			lowdepositCheck.checked = "checked";
		}
		
	}
}


/**
 ** This method sets the default deposit option and payment method on load of the payment page.
**/
function setDefaultDepositOption(){
	updatePaymentInfo('fullCost');    
    findPaymentOption('fullCost');
	/*var issueDom = document.getElementById('issue');
	if(issueDom){
		issueDom.style.display = "none";
	}
	//document.getElementById('issue').style.display = 'none';
	if(isNewHoliday == 'false'){
		document.CheckoutPaymentDetailsForm.reset();
	}
	//displayCreditCharges('debitcardType');
	if (typeof lockYourPriceSummarySelected !== 'undefined'
			&& lockYourPriceSummarySelected == 'true'
			&& typeof isLockYourPriceMMBFlow !== 'undefined'
			&& !(isLockYourPriceMMBFlow == 'true')) {
		updatePaymentInfo('priceLock');
		findPaymentOption('priceLock');
	} else {
		if(fjfoSelected) {
			updatePaymentInfo('fullCost');
			findPaymentOption('fullCost');
		}else if(depositAmountsMap.get('lowDeposit') != null){
			updatePaymentInfo('lowDeposit');
			selectedpaymentMode('quarter-amt');
			findPaymentOption('lowDeposit');
		}else if(depositAmountsMap.get('deposit') != null){
			updatePaymentInfo('deposit');
			selectedpaymentMode('half-amt');
			findPaymentOption('deposit');
		}else{
			updatePaymentInfo('fullCost');
			selectedpaymentMode('full-amt');
			findPaymentOption('fullCost');
		}
	}*/

	// if(lockYourPriceSummarySelected == "true"){
	// 	if(depositAmountsMap.get('lowDeposit') != null){
	// 		updatePaymentInfo('lowDeposit');
	// 		selectedpaymentMode('quarter-amt');
	// 	}else if(depositAmountsMap.get('deposit') != null){
	// 		updatePaymentInfo('deposit');
	// 		selectedpaymentMode('half-amt');
	// 	}else{
	// 		updatePaymentInfo('fullCost');
	// 		selectedpaymentMode('full-amt');
	// 	}
	// }else if(fjfoSelected){
	// 	if(depositAmountsMap.get('lowDeposit') != null){
	// 		updatePaymentInfo('lowDeposit');
	// 		findPaymentOption('lowDeposit');
	// 	}else{
	// 		updatePaymentInfo('fullCost');
	// 		findPaymentOption('fullCost');
	// 	}
	// }else{
	// 	updatePaymentInfo('deposit');
	// 	findPaymentOption('deposit');
	// }
}

/*


  This method switches tab selection
*/
function switchTab(paymentType){
	var radioButtons = document.querySelectorAll(".price-list .radio-button");
	var totalRadioButtons = radioButtons.length;
	for(var radioIndex = 0; radioIndex < totalRadioButtons; radioIndex++){
	    var radio = radioButtons[radioIndex];
	    if(clientApp == 'TUIES '){
	    	radio.className = "icon-v2 text-white radio radio-button";
	    } else {
	    	radio.className = "icon-v2 text-white icon-TUI_radio-circle radio-button";
	    }
		radio.parentElement.className = "deposit-amount fl";
		radio.parentElement.removeAttribute('analytics-text','DepOpt');
			radio.parentElement.removeAttribute('analytics-id','PAYUS');
		if(paymentType === radio.getAttribute("data-payment-type")){
		    radio.className = "icon-v2 brand-blue icon-TUI_radio-on active radio-button";
			radio.parentElement.className = "deposit-amount fl active";
			radio.parentElement.setAttribute('analytics-text','DepOpt');
			radio.parentElement.setAttribute('analytics-id','PAYUS');
			if(paymentType === "lowDeposit"){
			    document.querySelector(".depositbox").className = "box depositbox hide";
				document.querySelector(".lowDepositbox").className = "box lowDepositbox";
			} else if(paymentType === "deposit"){
				document.querySelector(".depositbox").className = "box depositbox";
				if(document.querySelector(".lowDepositbox")) {
					document.querySelector(".lowDepositbox").className = "box lowDepositbox hide";
				}
			}
		}
	}
	if(paymentType === "lowDeposit" || paymentType === "deposit") {
		var domNodes = document.querySelectorAll("."+paymentType + "box");
		if(domNodes && domNodes.length){
		   var depositBox = domNodes[0];
		   var fullCostBox = document.querySelectorAll(".fullCostbox")[0];
		   var height = depositBox.clientHeight - (2 * parseInt(window.getComputedStyle(depositBox, null).getPropertyValue('padding-top').split("px")[0]));
		   fullCostBox.setAttribute("style","height:"+ height +"px");
		}
	}
}

function UpdatePaymentTitleMode(depositType) {
	var paymode_title = document.getElementById('payment_mode_title');
	paymode_title ? updateTitleForAll(depositType)
			: updateTitleForDD(depositType);
}

function updateTitleForAll(depositType) {
	var paymode_title = document.getElementById('payment_mode_title');
	if (depositType == 'lowDepositPlusDD' && PaymentInfo.selectedDepositAmount == 0) {
		paymode_title.innerHTML = "";
		paymode_title.removeClassName('underline');
	}
	else if (depositType == 'lowDepositPlusDD') {
		paymode_title.innerHTML = "PAY A DEPOSIT TO CONTINUE TO SET UP DIRECT DEBIT";
		paymode_title.addClassName('underline');
	} else if (depositType == 'fullCost') {
		paymode_title.innerHTML = "HOW WOULD YOU LIKE TO PAY?"
		paymode_title.addClassName('underline');
	} else {
		paymode_title.innerHTML = "HOW WOULD YOU LIKE TO PAY YOUR DEPOSIT?";
		paymode_title.addClassName('underline');
	}
}

function updateTitleForDD(depositType) {
	// When paymode_title element doesn't exist using new element
	var paymode_title_dd = document.getElementById('payment_mode_title');
	if (depositType == 'lowDepositPlusDD') {
		paymode_title_dd.innerHTML = "PAY A DEPOSIT TO CONTINUE TO SET UP DIRECT DEBIT";
		paymode_title_dd.addClassName('underline');
	} else {
		paymode_title_dd.innerHTML = "";
		paymode_title_dd.removeClassName('underline');
	}
}

function findPaymentOption(despositType) {
	console.log("despositType****    " + despositType);
	var url = "/cps/DepositTypeFinderServlet?token=" + token + tomcatInstance;
	parameterString = "depositType=" + despositType + "&token=" + token
			+ tomcatInstance;
	var request = new Ajax.Request(url, {
		method : "POST",
		parameters : parameterString,
		onSuccess : showCardType
	});

}
/* Following method is introduced to display pay pal credit amount below CTA and hide it when other Deposit type is selected */
function payPalCreditDisplayupdate(id){
	try{
		if (id == 'PayPal_Credit') {
			jQuery(".payPalCreditDisplay").removeClass("hide");
			jQuery(".paypalcreditbutton").addClass("hide");
		} else {
			jQuery(".payPalCreditDisplay").addClass("hide");
			jQuery(".paypalcreditbutton").removeClass("hide");
		}	
	} catch(e){
		console.log(e);
	}
}