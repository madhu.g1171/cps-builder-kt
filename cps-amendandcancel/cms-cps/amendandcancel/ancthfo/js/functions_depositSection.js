updateChargesForDeposits();
/*
 * Updates the card charge amounts for each of the deposit types.
 */
function updateChargesForDeposits()
{
	//var depositTypes = $$('input[name=depositType]');
	//var deposCardChargeDivs =  $$('#creditCardChargeAmt');
	//var depositAmount = $$('#depositAmt');
	var cardCharge = null;
	var amendmentCardCharge = null;
	depositAmount = depositAmountsMap.get("fullCost");
	if(PaymentInfo.amendmentChargeAmount != '')
	{
	   amendmentCardCharge = calculateCardCharge(PaymentInfo.amendmentChargeAmount);
	   amendmentCardCharge = parseFloat(PaymentInfo.amendmentChargeAmount) + parseFloat(amendmentCardCharge);
	   if(applyCreditCardSurcharge=='true'){
	   document.getElementById("amendmentChargeWithCC").innerHTML = parseFloat(amendmentCardCharge).toFixed(2);
	   document.getElementById("creditCardChargeAmt_fullCost").innerHTML = parseFloat(amendmentCardCharge).toFixed(2);
	   }
	   document.getElementById("debitCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
	   document.getElementById("thomsonCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
	   if(document.getElementById("giftCardChargeAmt_fullCost") != null)
	   {
	      document.getElementById("giftCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
	   }
	}
	else
	{
	   cardCharge = calculateCardCharge(depositAmount);
	   cardCharge = parseFloat(depositAmount) + parseFloat(cardCharge);
	   if(applyCreditCardSurcharge=='true'){
	   document.getElementById("creditCardChargeAmt_fullCost").innerHTML = parseFloat(cardCharge).toFixed(2);
	}
}
}

/**
 ** This method calculates the card charge for the deposit amount.
**/
function calculateCardCharge(amount)
{
  var applicableCharges= new Array();
  var cardCharge=0.0;

    applicableCharges = configurableCardCharge.split(",");


    if(applicableCharges!=null)
    {
     if(applicableCharges[0] > 0.0)
      {
      cardCharge = parseFloat(amount * (applicableCharges[0]/100));
    }
       if (parseFloat(applicableCharges[1]) > 0.0 && parseFloat(cardCharge) < parseFloat(applicableCharges[1]))
      {
        cardCharge = parseFloat(applicableCharges[1]);
      }
      if (parseFloat(applicableCharges[2]) > 0.0 && parseFloat(cardCharge) > parseFloat(applicableCharges[2]))
      {
         cardCharge = parseFloat(applicableCharges[2]);
      }
    }
    return parseFloat(Math.round(cardCharge * 100) / 100).toFixed(2);
}


/**
 ** Refreshes the PaymentInfo. A central function responsible for
 ** keeping data integrity of different amounts in
 ** in this payment for client side validation.
**/
function updatePaymentInfo(selectedDepositType)
{
PaymentInfo.depositType = trimSpaces((selectedDepositType));
   if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
   {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
   }
   else
   {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
   }
   PaymentInfo.selectedDepositAmount = PaymentInfo.amendmentChargeAmount;
   calCardCharge  = calculateCardCharges(PaymentInfo.selectedDepositAmount);

   PaymentInfo.totalCardCharge = calCardCharge;
   PaymentInfo.calculatedPayableAmount = roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2); //parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge).toFixed(2);
   PaymentInfo.calculatedTotalAmount = roundOff((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge , 2); //parseFloat((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge).toFixed(2);

   var totalCharge = parseFloat(PaymentInfo.selectedDepositAmount) + parseFloat(calCardCharge);
   if(document.getElementById("creditCardChargeFinalAmt")){ document.getElementById("creditCardChargeFinalAmt").innerHTML = totalCharge.toFixed(2);}
   document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   if(document.getElementById("giftCardFinalAmt") != null)
   {
      document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   }
   document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
}

function selectedpaymentMode(id) {
	var getmode = id;
	var getfullDeposit = document.getElementById("full-amt");
	var gethalfDeposit = document.getElementById("half-amt");
	var getlowDeposit = document.getElementById("quarter-amt");
	var getpartDeposit = document.getElementById("other-amt");
	var depositClass = document.getElementsByClassName("payment-details");

	if(document.getElementById("creditCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "creditCardCharges";
	}
	if(document.getElementById("debitCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "debitCardCharges";
	}
	if(document.getElementById("thomsonCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "thomsonCardCharges";
	}
	if(document.getElementById("giftCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "giftCardCharges";
	}
	if(document.getElementById("paypalCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "paypalCharges";
	}
    if (getmode == "noThanksDiv")
	{
		var noThanksVar = document.getElementById("noThanks");
		noThanksVar.checked = "checked";
		document.getElementById("getOtheramt").value="false";
		document.getElementById("otheramount").innerHTML = "";
	}
    else if (getmode == "full-amt")
	{
		var fullCheck = document.getElementById("full");
		fullCheck.checked = "checked";
		document.getElementById("getOtheramt").value="false";
		document.getElementById("otheramount").innerHTML = "";
	}
	else if (getmode == "half-amt")
	{
		var halfCheck = document.getElementById("half");
		halfCheck.checked = "checked";
		document.getElementById("getOtheramt").value="false";
		document.getElementById("otheramount").innerHTML = "";
	}
	else if (getmode == "other-amt")
	{
		PaymentInfo.selectedDepositAmount = PaymentInfo.amendmentChargeAmount;
		if(document.getElementById("otheramount").innerHTML == "")
        {
			if(isNaN(PaymentInfo.selectedDepositAmount) || (PaymentInfo.selectedDepositAmount == ""))
			{
				document.getElementById("creditCardCharges").style.display = "none";
				document.getElementById("debitCardCharges").style.display = "none";
				document.getElementById("thomsonCardCharges").style.display = "none";
				document.getElementById("giftCardCharges").style.display = "none";
				document.getElementById("paypalCharges").style.display = "none";
			}
			else
			{
				document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("creditCardChargeFinalAmt").innerHTML = parseFloat(parseFloat(PaymentInfo.selectedDepositAmount) + parseFloat(calculateCardCharge(PaymentInfo.selectedDepositAmount))).toFixed(2);
				document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("paypalFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("paypalcreditFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				
			}
		}
		var partCheck = document.getElementById("other");
		partCheck.checked = "checked";
		document.getElementById("getOtheramt").value="true";

	}
	else
	{
		try {
			var lowdepositCheck = document.getElementById("initial");
			lowdepositCheck.checked = "checked";
			document.getElementById("getOtheramt").value="false";
			document.getElementById("otheramount").innerHTML = "";
		}catch(ex){}
	}
}

/**
 * * To send the selected deposit types to the backend
 */
function findPaymentOption(despositType) {

	console.log("despositType****    " + despositType);

	var url = "/cps/DepositTypeFinderServlet?token=" + token + tomcatInstance;
	parameterString = "depositType=" + despositType + "&token=" + token
			+ tomcatInstance;
	var request = new Ajax.Request(url, {
		method : "POST",
		parameters : parameterString,
		onSuccess : showCardType
	});
}

/**
 ** This method sets the default deposit option and payment method on load of the payment page.
**/
function setDefaultDepositOption(){
	document.getElementById('issue').style.display = 'none';
	if(isNewHoliday == 'false'){
		document.CheckoutPaymentDetailsForm.reset();
	}
	if (typeof lockYourPriceSummarySelected !== 'undefined'
		&& lockYourPriceSummarySelected == 'true'
		&& typeof isLockYourPriceMMBFlow !== 'undefined'
		&& !(isLockYourPriceMMBFlow == 'true')) {
	updatePaymentInfo('priceLock');
	findPaymentOption('priceLock');
} else {
	if(depositAmountsMap.get('lowDeposit') != null){
		updatePaymentInfo('lowDeposit');
		selectedpaymentMode('quarter-amt');
		findPaymentOption('lowDeposit');
	}else if(depositAmountsMap.get('deposit') != null){
		updatePaymentInfo('deposit');
		selectedpaymentMode('half-amt');
		findPaymentOption('deposit');
	}else{
		updatePaymentInfo('fullCost');
		selectedpaymentMode('full-amt');
		findPaymentOption('fullCost');
	}
}
	//displayCreditCharges('debitcardType');
	//updatePaymentInfo('fullCost');
	//	selectedpaymentMode('full-amt');

}