<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!-- included css for header bread crumb  -->
<link rel="stylesheet"
	href="/cms-cps/amendandcancel/css/fae_breadcrumb_ancfirstchoice.css" />
<fmt:formatNumber
	value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
	var="totalCostingLine" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="zeroDepositDDExist" value="false" />
<c:forEach var="depositComponent"
	items="${bookingComponent.depositComponents}" varStatus="count">
	<c:set var="zeroDepositDDExist"
		value="${depositComponent.depositAmount.amount gt 0 ? false : true}"
		scope="page" />
</c:forEach>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<fmt:formatNumber
	value="${bookingComponent.lockYourPriceSummary.outstandingAmount.amount}"
	var="outStandingAmount" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />

<fmt:formatDate var="paySummaryCurrentDateFormatted"
	value="${paySummaryCurrentDate}" pattern="EEE dd MMM yyyy" />
<fmt:parseDate value="${paySummaryCurrentDateFormatted}" type="date"
	pattern="EEE dd MMM yyyy" var="paySummaryCurrentDateValue" />
<c:set var="paySummaryLeftToPay"
	value="${bookingComponent.payableAmount.amount-bookingInfo.calculatedDiscount.amount}" />
<c:set var="paySummaryLeftToPaySplit"
	value="${fn:split(paySummaryLeftToPay, '.')}" />
<c:set var="paySummaryLeftToPay_Dec"
	value="${paySummaryLeftToPaySplit[1]}" />
<c:if test="${empty paySummaryLeftToPay_Dec}">
	<c:set var="paySummaryLeftToPay_Dec" value="00" />
</c:if>

<c:set var="outStandingcost" value="${fn:split(outStandingAmount, '.')}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
	<c:set var="optionsUrl"
		value="${bookingComponent.breadCrumbTrail['HOTEL']}" />
	<c:set var="searchUrl"
		value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}" />
	<c:set var="passengersUrl"
		value="${bookingComponent.breadCrumbTrail['PASSENGERS']}" />
</c:if>


<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="discountFlag" value="false" />
<c:set var="priceBreakDown"
	value="${bookingComponent.pricingDetails['priceBreakDown']}" />

<c:if test="${not empty  bookingComponent.depositComponents}">
	<c:set var="depositDetails"
		value="${bookingComponent.depositComponents }"></c:set>
	<c:set var="CONST_LOW_DEPOSIT">lowDeposit</c:set>
	<c:set var="CONST_DEPOSIT">deposit</c:set>
</c:if>

<c:forEach var="priceComponent" items="${priceBreakDown}"
	varStatus="count">
	<c:if
		test="${not empty priceComponent.onlineDiscountData &&  not empty bookingComponent.flightSummary}">
		<c:set var="discountFlag" value="true" />
	</c:if>
</c:forEach>
<c:set var="depositComponentval" value="false" />
<c:forEach var="eachDepositComponentAcc" items="${depositDetails}"
	varStatus="count">
	<c:if test="${not empty eachDepositComponentAcc.depositDataPP}">
		<c:set var="depositComponentval" value="true" />
	</c:if>
</c:forEach>


<!-- Adding to fix ABA-4820 to hide the discount amount in header -->

<c:forEach var="priceComponent" items="${priceBreakDown}"
	varStatus="count">
	<c:if
		test="${priceComponent.itemDescription == 'Online Discount' &&  priceComponent.amount.amount > 0 }">
		<c:set var="discountFlag" value="false" />
	</c:if>
</c:forEach>

<c:set var="perPersonPrice"
	value="${bookingComponent.pricingDetails['PER_PERSON_PRICE']}" />
<c:if test="${not empty perPersonPrice}">
	<c:forEach var="price" items="${perPersonPrice}">

		<fmt:formatNumber value="${price.amount.amount}"
			var="perPersonPriceCostingLine" type="number" maxFractionDigits="2"
			minFractionDigits="2" pattern="#####" />
		<c:set var="perpersonpricecost"
			value="${fn:split(perPersonPriceCostingLine, '.')}" />
	</c:forEach>
</c:if>
<c:set var="outstandingPerPersonPrice"
	value="${bookingComponent.lockYourPriceSummary.outstandingAmountPP.amount}" />
<c:if test="${not empty outstandingPerPersonPrice}">

	<fmt:formatNumber value="${outstandingPerPersonPrice}"
		var="mmbPerPersonPriceCostingLine" type="number" maxFractionDigits="2"
		minFractionDigits="2" pattern="#####" />
	<c:set var="outstandingPerpersonpricecost"
		value="${fn:split(mmbPerPersonPriceCostingLine, '.')}" />

</c:if>
<fmt:parseNumber var="fcpCount" integerOnly="true" type="number"
	value="${bookingComponent.nonPaymentData['freeChildPlaceCount']}" />
<div id="book-flow-header">
	<div class="content-width">
		<div class="logo thomson firstchoiceX falconX">
			<!--  <a href="${bookingComponent.clientURLLinks.homePageURL}"></a>-->
		</div>
		<!--<div class="summary-panel-trigger b abs bg-tui-sand black">
						<i class="caret fly-out"></i>&pound;<c:out value="${totalcost[0]}."/><span class="pennys"><c:out value="${totalcost[1]}"/></span>

					</div>-->

	</div>

</div>

<div id="ProgressBarNavigator__component"
	aria-label="Checkin Progress bar">

	<div>
		<div>
			<div
				class="ProgressbarNavigation__progressBarWrapper ProgressbarNavigation__progressBarWithPrice">
				<div class="ProgressbarNavigation__scrollWrapper">
					<div id="scroll-wrapper" class="scrollers__scroll">
						<div id="scroll-container">
							<ul class="ProgressbarNavigation__steps">
								<c:forEach var="indicator"
									items="${bookingComponent.breadCrumbTrail}" varStatus="counter">

									<c:choose>

										<c:when test="${counter.count eq 5}">

											<li
												class="ProgressbarNavigation__step">
										</c:when>
										<c:when test="${counter.count eq 4}">

											<li id="payment"
												class="ProgressbarNavigation__step 
                    								ProgressbarNavigation__selected 
                    								ProgressbarNavigation__completed
                    						">
										</c:when>
										<c:otherwise>
											<li
												class="ProgressbarNavigation__step ProgressbarNavigation__clickable
                                       ProgressbarNavigation__completed">
										</c:otherwise>

									</c:choose>

									<div class="ProgressbarNavigation__numberWrapper">
										<div class=" ProgressbarNavigation__numberCircled">${counter.count}</div>
										<c:choose>

											<c:when test="${counter.count eq 1}">
												<div class="ProgressbarNavigation__solidLineHalf"></div>

											</c:when>
											<c:when test="${counter.count gt 4}">
												<div class="ProgressbarNavigation__dottedLine"></div>

											</c:when>
											<c:when test="${counter.count eq 4}">

												<div class="ProgressbarNavigation__dottedLine"></div>
												<div class="ProgressbarNavigation__solidLineHalf"></div>

											</c:when>
											<c:otherwise>
												<div class="ProgressbarNavigation__solidLine"></div>

											</c:otherwise>

										</c:choose>
										<c:choose>

											<%-- <c:when test="${!counter.last}">
												<c:set var="indicatorHref" value="${indicator.value}" />

											</c:when> --%>
											<c:when test="${counter.count ne 4 && counter.count ne 5}">
												<c:set var="indicatorHref" value="${indicator.value}" />

											</c:when>
											<c:otherwise>
												<c:set var="indicatorHref" value="#" />

											</c:otherwise>
										</c:choose>


									</div>
									<a
										<c:if test="${counter.count ne 4 && counter.count ne 5}">href="<c:out value='${indicatorHref}'/>"
											</c:if>
										short-label="${indicator.key}" aria-label="${indicator.key}"
										data-order="${counter.count}"> ${indicator.key} </a>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div class="ProgressbarNavigation__pricePanelWrapper">
						<!-- <div>
                                 <span class="UK__priceContainer UK__small "><span class="UK__currency">£</span><span class="UK__main">40</span><span class="UK__fraction">00</span></span>
                                 <div><span class="ProgressbarNavigation__priceText"> Total price for flight extras </span></div>
                              </div> -->
						<div
							class="left-content <c:if test='${ not discountFlag and not depositComponentval}'> hide </c:if>">
							<div class="text">
								<span class="promotional-discount"> <c:if
										test="${not empty priceBreakDown and discountFlag}">
										<c:forEach var="priceComponent" items="${priceBreakDown}"
											varStatus="count">
											<c:choose>
												<c:when
													test="${not empty priceComponent.itemDescription and not empty priceComponent.onlineDiscountData and count.index==0}">
																	Inc &pound;<fmt:formatNumber
														value="${priceComponent.amount.amount}" type="number"
														maxFractionDigits="2" minFractionDigits="2"
														pattern="#####.##" /> discount<br>
												</c:when>
											</c:choose>
										</c:forEach>
									</c:if>
								</span> <span class="promotional-discount"> <c:choose>
										<c:when test="${zeroDepositDDExist}">
											Book now with no deposit
										</c:when>
										<c:otherwise>
											<c:if test="${not empty depositDetails}">
												<c:forEach var="eachDepositComponent"
													items="${depositDetails}" varStatus="deposits">
													<c:choose>
														<c:when
															test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP }">

															<c:out value="${eachDepositComponent.depositDataPP}"
																escapeXml="false" />
														</c:when>
														<c:when
															test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP}">

															<c:out value="${eachDepositComponent.depositDataPP}"
																escapeXml="false" />
														</c:when>
													</c:choose>
												</c:forEach>
											</c:if>
										</c:otherwise>
									</c:choose>
								</span>
							</div>
						</div>

						<div
							class="right-content <c:if test='${ not discountFlag and not depositComponentval}'>no-discount</c:if>">
							<div class="price">
								<span class="UK__priceContainer UK__small "> <span
									class="UK__currency">&pound;</span> <span class="UK__main"><fmt:formatNumber
											value="${paySummaryLeftToPaySplit[0]}" /></span> <span
									class="UK__fraction"><c:out
											value="${paySummaryLeftToPay_Dec}" /></span>
								</span>
								<div>
									<span class="ProgressbarNavigation__priceText"> Total
										price for flight extras </span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
			var scrollContainer = document.querySelector('#scroll-container');
			var progressBarScroller = null;
			var initScroller = function() {
				if (document.querySelector('body').clientWidth < 960) {
					scrollContainer.style.width = '880px';
					progressBarScroller = new IScroll('#scroll-wrapper', {
						scrollX : true,
						scrollY : false,
						mouseWheel : true,
						scrollAxis : {
							offsetX : false,
							offsetY : true
						}
					});
					progressBarScroller.scrollToElement(document
							.querySelector('#payment'));
				}
			};
			var refreshPaymentScrollBar = function() {
				if (document.querySelector('body').clientWidth < 960) {
					if (!progressBarScroller) {
						initScroller();
					} else {
						progressBarScroller.refresh();
						progressBarScroller.scrollToElement(document
								.querySelector('#payment'));
					}
				} else {
					if (progressBarScroller) {
						progressBarScroller.destroy();
						progressBarScroller = null;
					}
				}
			};
			initScroller();
			window.addEventListener('resize', refreshPaymentScrollBar);
</script>

