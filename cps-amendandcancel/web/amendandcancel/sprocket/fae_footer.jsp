<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>



<div id="footer">
	<div id="call-us" class="hide">
		<div class="content-width">
			<i class="caret call white b"></i>
			<h2 class="d-blue">
				BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203
						636 1931</a></span>
			</h2>
		</div>
	</div>
	<div id="search" class="b">
		<div class="content-width">
			<a href="#page" id="backtotop"><span>To top </span><i
				class="caret back-to-top white"></i></a>
		</div>
	</div>
	<div id="group" class="b thomson">
		<div class="content-width">
			<div class="copy">
				<c:if
					test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName != 'ANCFIRSTCHOICE'}">
					<!--   <span id="world-of-tui"><img alt="World of TUI" src="/cms-cps/amendandcancel/images/logo/wtui.png"></span> -->
				</c:if>
				<c:choose>
					<c:when
						test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON' || bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO' }">
							<p class="footer-p">We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p>
							<p class="footer-p">Registered address: Company Reg. No: 116977, One Spencer Dock, North Wall Quay, Dublin 1, Ireland.</p>

					</c:when>
					<c:otherwise>
						<p class="footer-p">Just so you know, Thomson is now called TUI and we're part
							of TUI Group - the world's leading travel company. All of our
							holidays are designed to help you 'Discover your smile'.</p>
					</c:otherwise>
				</c:choose>

			</div>
			<div class="logos">
				<!-- <span id="world-of-tui" title="World of TUI"></span> -->
				<c:choose>
					<c:when
						test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
						<a href="http://www.aviationreg.ie/" target="_blank" id="c-ar"
							title="Commission for Aviation Regulation">RIO</a>
							<p class="commission">
								Licenced by the Commission for Aviation Regulation T.O. 272
				            </p>
					</c:when>
					<c:when
						test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO'}">
						<a href="http://www.aviationreg.ie/" target="_blank" id="c-ar"
							title="Commission for Aviation Regulation">RIO</a>
							<p class="commission">
							Licenced by the Commission for Aviation Regulation T.O. 272
				            </p>
					</c:when>
					<c:otherwise>
						<a
							href="http://abta.com/go-travel/before-you-travel/find-a-member"
							id="logo-abta" title="ABTA - The Travel Association"></a>
						<a
							href="http://www.caa.co.uk/application.aspx?catid=490&pagetype=65&appid=2&mode=detailnosummary&atolnumber=2524"
							id="logo-atol" title="ATOL Protected"></a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<div id="terms">
		<div class="content-width">
			<p>
				<c:choose>
					<c:when
						test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
						<a href="http://www.firstchoice.co.uk/about-us">About First
							Choice</a>
						<a href="http://www.firstchoice.co.uk/myapp/">MyFirstChoice
							app</a>
						<a
							href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies">Cookies
							policy</a>

						<a href="http://www.firstchoice.co.uk/our-policies/privacy-policy">Privacy
							Policy</a>
						<a href="http://www.firstchoice.co.uk/our-policies/terms-of-use">Terms
							&amp; conditions</a>
						<c:choose>
							<c:when test="${applyCreditCardSurcharge eq 'true'}">
								<a
									href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges">Credit
									card fees</a>
							</c:when>
							<c:otherwise>
								<a
									href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges">Ways
									to Pay</a>
							</c:otherwise>
						</c:choose>
						<a class="desktopLinkHide" href="http://www.firstchoice.co.uk/">Desktop
							site</a>
						<a href="http://communicationcentre.firstchoice.co.uk/">Media
							Centre</a>

						<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>
						<a href="http://www.firstchoice.co.uk/affiliates.html">Affiliates</a>
						<a href="http://www.firstchoice.co.uk/blog/">First Choice Blog</a>
					</c:when>
					<c:when
						test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
						<a href="https://www.tuiholidays.ie/f/info/about-us">About TUI</a>
						<a href="http://www.falconholidays.ie/our-policies/accessibility/">Accessibility</a>
						<a
							href="http://www.falconholidays.ie/our-policies/statement-on-cookies/index.html">Statement
							on cookies</a>
						<a href="http://www.falconholidays.ie/our-policies/terms-of-use/">Terms
							&amp; conditions</a>
						<a
							href="http://www.falconholidays.ie/our-policies/privacy-policy/">Privacy
							Policy</a>
						<c:choose>
							<c:when test="${applyCreditCardSurcharge eq 'true'}">
								<a
									href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Credit
									card fees</a>
							</c:when>
							<c:otherwise>
								<a
									href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Ways
									to Pay</a>
							</c:otherwise>
						</c:choose>
						<a class="desktopLinkHide" href="http://www.falconholidays.ie">Desktop
							site</a>
						<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>
						<a href="https://blog.tuiholidays.ie/">TUI Blog</a>
					</c:when>
					<c:when
						test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO'}">
						<a href="https://www.tuiholidays.ie/f/info/about-us">About TUI
						</a>
						<a href="http://www.falconholidays.ie/our-policies/accessibility/">Accessibility</a>
						<a
							href="http://www.falconholidays.ie/our-policies/statement-on-cookies/index.html">Statement
							on cookies</a>
						<a href="http://www.falconholidays.ie/our-policies/terms-of-use/">Terms
							&amp; conditions</a>
						<a
							href="http://www.falconholidays.ie/our-policies/privacy-policy/">Privacy
							Policy</a>
						<c:choose>
							<c:when test="${applyCreditCardSurcharge eq 'true'}">
								<a
									href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Credit
									card fees</a>
							</c:when>
							<c:otherwise>
								<a
									href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Ways
									to Pay</a>
							</c:otherwise>
						</c:choose>
						<a class="desktopLinkHide" href="http://www.falconholidays.ie">Desktop
							site</a>
						<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>
						<a href="https://blog.tuiholidays.ie/">TUI Blog</a>
					</c:when>
					<c:otherwise>
						<a href="https://www.tui.co.uk/destinations/info/my-tui-app">About
							TUI</a>
						<a href="https://www.tui.co.uk/destinations/info/my-tui-app">MyTUI
							app</a>
						<a
							href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies.html">Cookies
							policy</a>

						<a
							href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy
							Policy</a>
						<a
							href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms
							&amp; conditions</a>
						<c:choose>
							<c:when test="${applyCreditCardSurcharge eq 'true'}">
								<a
									href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Credit
									card fees</a>
							</c:when>
							<c:otherwise>
								<a
									href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Ways
									to Pay</a>
							</c:otherwise>
						</c:choose>
						<a href="http://www.thomson.co.uk/">Accessibility</a>
						<a class="desktopLinkHide" href="http://www.thomson.co.uk/">Desktop
							site</a>
						<a href="http://communicationcentre.thomson.co.uk/">Media
							Centre</a>

						<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>
						<a href="http://www.thomson.co.uk/affiliates.html">Affiliates</a>
						<a href="https://blog.tui.co.uk/">TUI Blog</a>
					</c:otherwise>
				</c:choose>
				<a target="_blank" href="http://www.tuigroup.com/en-en"><jsp:useBean
						id='CurrentDate12' class='java.util.Date' /> <fmt:formatDate
						var='currentYear' value='${CurrentDate12}' pattern='yyyy' />
					&copy; <c:out value="${currentYear}" /> TUI Group</a>
			</p>
		</div>
	</div>
</div>
<div id="disclaimer">
	<div class="content-width disclaim ">
		<c:choose>
			<c:when
				test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
				<c:choose>
				<c:when test="${bookingComponent.nonPaymentData['oscarEnabled'] eq 'true' }">
				<p  class="footer-p-disc">
				Adehy Limited is licenced by the Commission for Aviation Regulation under Licence T.O. 272 we have arranged an approved bond, therefore your money is secure with us.
				Adehy Limited is the part of the TUI Group of Companies.
				</p>
				</c:when>
				<c:otherwise>
				<p  class="footer-p-disc">TUI is licenced by the Commission for Aviation Regulation under Licence T.O. 021, we have arranged an approved bond, therefore your money is secure with us. TUI is the part of the TUI Group of Companies.</p>
                 </c:otherwise>
				 </c:choose>
			</c:when>
			<c:when
				test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO'}">
				<c:choose>
				<c:when test="${bookingComponent.nonPaymentData['oscarEnabled'] eq 'true' }">
				<p class="footer-p-disc">
				Adehy Limited is licenced by the Commission for Aviation Regulation under Licence T.O. 272 we have arranged an approved bond, therefore your money is secure with us.
				Adehy Limited is the part of the TUI Group of Companies.
				</p>
				</c:when>
				<c:otherwise>
				<p class="footer-p-disc">TUI is licenced by the Commission for Aviation Regulation under Licence T.O. 021, we have arranged an approved bond, therefore your money is secure with us. TUI is the part of the TUI Group of Companies.</p>
                 </c:otherwise>
				 </c:choose>
			</c:when>
			<c:when
				test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHFO'}">
				<c:if
					test="${bookingComponent.flightSummary.isThirdPartyFlight == 'true'}">
					<p class="footer-p-disc">
						Some of the flights and flight-inclusive holidays on this website
						are financially protected by the ATOL scheme. But ATOL protection
						does not apply to all holiday and travel services listed on this
						website. This website will provide you with information on the
						protection that applies in the case of each holiday and travel
						service offered before you make your booking. If you do not
						receive an ATOL Certificate then the booking will not be ATOL
						protected. If you do receive an ATOL Certificate but all the parts
						of your trip are not listed on it, those parts will not be ATOL
						protected. Please see our booking conditions for information, or
						for more information about financial protection and the ATOL
						Certificate go to:<a href="http://www.caa.co.uk/home/"
							target="_blank">www.caa.co.uk</a>
					</p>
				</c:if>
			</c:when>
			<c:otherwise>
				<p class="footer-p-disc">
					Some of the flights and flight-inclusive holidays on this website
					are financially protected by the ATOL scheme. But ATOL protection
					does not apply to all holiday and travel services listed on this
					website. This website will provide you with information on the
					protection that applies in the case of each holiday and travel
					service offered before you make your booking. If you do not receive
					an ATOL Certificate then the booking will not be ATOL protected. If
					you do receive an ATOL Certificate but all the parts of your trip
					are not listed on it, those parts will not be ATOL protected.
					Please see our booking conditions for information, or for more
					information about financial protection and the ATOL Certificate go
					to:<a href="http://www.caa.co.uk/home/" target="_blank">www.caa.co.uk</a>
				</p>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<c:choose>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
		<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,tuiAnalyticsJson </c:set>
		<c:if
			test="${(analyticsPageID == 'paymentPage') || (analyticsPageID == 'technicaldifficulties')}">
			<script type="text/javascript">
						     var tui = {};
						     tui.analytics = {};
						     tui.analytics.page = {};
						     tui.analytics.page.pageUid = "${analyticsPageID}";
						     tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
						     <c:if test="${analyticsPageID == 'paymentPage'}">
							     tui.analytics.page.componentIds = [];
							     tui.analytics.page.componentIds.push("WF_COM_ACMYBOOKING");
							     tui.analytics.page.componentIds.push("WF_COM_ACTOPLEFTSUM");
							     tui.analytics.page.componentIds.push("WF_COM_ACBTSP");
							     <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
								     <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
								         tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
							         </c:if>
						         </c:forEach>
							 </c:if>
						</script>

			<script>
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});
			</script>
		</c:if>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
		<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,communicateByEmail,chkThirdPartyMarketingAllowed,communicateByPost,communicateByPhone,tuiAnalyticsJson </c:set>
		<c:if
			test="${(analyticsPageID == 'paymentPage') || (analyticsPageID == 'technicaldifficulties')}">
			<script type="text/javascript">
						     var tui = {};
						     tui.analytics = {};
						     tui.analytics.page = {};
						     tui.analytics.page.pageUid = "${analyticsPageID}";
						     tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
						     <c:if test="${analyticsPageID == 'paymentPage'}">
							     tui.analytics.page.componentIds = [];
							     tui.analytics.page.componentIds.push("WF_COM_ACMYBOOKING");
							     tui.analytics.page.componentIds.push("WF_COM_ACTOPLEFTSUM");
							     tui.analytics.page.componentIds.push("WF_COM_ACBTSP");
							     <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
								     <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
								         tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
							         </c:if>
								     <c:if test= "${analyticsDataEntry.key == 'Party'}">
									     tui.analytics.page.Party= "${analyticsDataEntry.value}";
								     </c:if>
						         </c:forEach>
							 </c:if>
			</script>

			<script>
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});
			</script>
		</c:if>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHCRUISE'}">
		<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,chkTuiMarketingAllowed,chkThirdPartyMarketingAllowed,communicateByPost,communicateByPhone,communicateByEmail,tuiAnalyticsJson</c:set>
		<c:if
			test="${(analyticsPageID == 'paymentPage') || (analyticsPageID == 'technicaldifficulties')}">
			<script type="text/javascript">
						     var tui = {};
						     tui.analytics = {};
						     tui.analytics.page = {};
						     tui.analytics.page.pageUid = "${analyticsPageID}";
						     tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
						     <c:if test="${analyticsPageID == 'paymentPage'}">
							     tui.analytics.page.componentIds = [];
							     tui.analytics.page.componentIds.push("WF_COM_ACMYBOOKING");
							     tui.analytics.page.componentIds.push("WF_COM_ACTOPLEFTSUM");
							     tui.analytics.page.componentIds.push("WF_COM_ACBTSP");
							     <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
								     <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
								         tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
							         </c:if>
								     <c:if test= "${analyticsDataEntry.key == 'Party'}">
									     tui.analytics.page.Party= "${analyticsDataEntry.value}";
								     </c:if>
						         </c:forEach>
							 </c:if>
		</script>

			<script>
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});
			</script>
		</c:if>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHFO'}">
		<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,chkTuiMarketingAllowed,chkThirdPartyMarketingAllowed,communicateByPost,communicateByPhone,communicateByEmail,selected_country_code,tuiAnalyticsJson</c:set>
		<c:if
			test="${(analyticsPageID == 'paymentPage') || (analyticsPageID == 'technicaldifficulties')}">
			<script type="text/javascript">
						     var tui = {};
						     tui.analytics = {};
						     tui.analytics.page = {};
						     tui.analytics.page.pageUid = "${analyticsPageID}";
						     tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
						     <c:if test="${analyticsPageID == 'paymentPage'}">
							     tui.analytics.page.componentIds = [];
							     tui.analytics.page.componentIds.push("WF_COM_ACMYBOOKING");
							     tui.analytics.page.componentIds.push("WF_COM_ACTOPLEFTSUM");
							     tui.analytics.page.componentIds.push("WF_COM_ACBTSP");
							     <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
								     <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
								         tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
							         </c:if>
								     <c:if test= "${analyticsDataEntry.key == 'Party'}">
									     tui.analytics.page.Party= "${analyticsDataEntry.value}";
								     </c:if>
						         </c:forEach>
							 </c:if>
			</script>

			<script>
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});
			</script>
		</c:if>
	</c:when>
	<c:otherwise>
		<c:if
			test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHOMSON'}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,tuiAnalyticsJson</c:set>
			<c:if
				test="${(analyticsPageID == 'paymentPage') || (analyticsPageID == 'technicaldifficulties')}">
				<script type="text/javascript">
						        var tui = {};
						        tui.analytics = {};
						        tui.analytics.page = {};
						        tui.analytics.page.pageUid = "${analyticsPageID}";
						        tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
						        <c:if test="${analyticsPageID == 'paymentPage'}">
							        tui.analytics.page.componentIds = [];
							        tui.analytics.page.componentIds.push("WF_COM_ACMYBOOKING");
							        tui.analytics.page.componentIds.push("WF_COM_ACTOPLEFTSUM");
							        tui.analytics.page.componentIds.push("WF_COM_ACBTSP");
							        <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
								        <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
								            tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
							            </c:if>
						            </c:forEach>
							    </c:if>
			 </script>

			 <script>
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});
			</script>
			</c:if>
		</c:if>
	</c:otherwise>
</c:choose>