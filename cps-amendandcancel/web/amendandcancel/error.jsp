<%@ page import="com.tui.uk.config.ConfReader;"%>
<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<c:choose>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHOMSON'}">
			<title>TUI | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
			<title>Falcon | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
			<title>First Choice | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHFO'}">
			<title>Flight Only | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHCRUISE'}">
			<title>Cruise | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKICS'}">
		<title>Crystal Ski Holidays | Get More Winter</title>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKIES'}">
		<title>Crystal Ski Holidays | Get More Winter</title>
	</c:when>
		<c:otherwise>
			<title>TUI | Error</title>
		</c:otherwise>
	</c:choose>
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
    <c:choose>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/base.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/base-new-th.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/bf.css"/>
            <link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/ac.css"/>
            <%
			 	String getSummaryPageURL =(String)ConfReader.getConfEntry("ANCFALCON.summarypage.url","");
                pageContext.setAttribute("getSummaryPageURL",getSummaryPageURL);
			%>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/base.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/base-new-fc.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/bf.css"/>
            <link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/ac.css"/>
            <%
			 	String getSummaryPageURL =(String)ConfReader.getConfEntry("ANCFIRSTCHOICE.summarypage.url","");
			    pageContext.setAttribute("getSummaryPageURL",getSummaryPageURL);
			%>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKICS'}">
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskics/css/base-new-ski.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskics/css/base.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskics/css/bf.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskics/css/ac.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskics/css/footer.css" />
			<link rel="icon" type="image/png"
				href="/cms-cps/amendandcancel/ancskics/images/favicon.png" />
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKIES'}">
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskies/css/base-new-ski.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskies/css/base.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskies/css/bf.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskies/css/ac.css" />
			<link rel="stylesheet"
				href="/cms-cps/amendandcancel/ancskies/css/footer.css" />
			<link rel="icon" type="image/png"
				href="/cms-cps/amendandcancel/ancskies/images/favicon.png" />
		</c:when>
		<c:otherwise>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/base.css" />
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/base-new-th.css" />
	        <link rel="stylesheet" href="/cms-cps/amendandcancel/css/bf.css" />
            <link rel="stylesheet" href="/cms-cps/amendandcancel/css/ac.css"/>
            <%
			   String getSummaryPageURL =(String)ConfReader.getConfEntry("ANCTHOMSON.summarypage.url","");
               pageContext.setAttribute("getSummaryPageURL",getSummaryPageURL);
			%>
		</c:otherwise>
	</c:choose>

	<link rel="stylesheet" href="/cms-cps/amendandcancel/css/fonts.css"/>

	<c:choose>
		 <c:when test="${not empty bookingComponent.prePaymentUrl}">
		 	<c:set var="summaryPageURL" value="${bookingComponent.prePaymentUrl}"/>
	      </c:when>
	     <c:otherwise>
	        <c:set var="summaryPageURL" value="${getSummaryPageURL}"/>
	     </c:otherwise>
	</c:choose>
<%--	<script src="//nexus.ensighten.com/tui/Bootstrap.js"></script> --%>
	<c:set var="analyticsPageID" value="technicaldifficulties"/>
</head>
<body>
	<div class="structure">

		<div id="page">
			<%
			     String analyticsPageID = (String)pageContext.getAttribute("analyticsPageID");
			     pageContext.setAttribute("analyticsPageID", analyticsPageID, PageContext.REQUEST_SCOPE);
			%>
			<jsp:include page="sprocket/header.jsp" />

			<div id="content" class="book-flow">
				<div class="content-width">

					<!-- Sorry - something has gone wrong... -->
					<div class="error-oh bg-light-grey marg-bottom-20">
						<div class="halfs">
							<div class="crop">
								<img src="/cms-cps/amendandcancel/images/658technical.jpg" srcset="
									/cms-cps/amendandcancel/images/658technical.jpg 658w,
									/cms-cps/amendandcancel/images/658technical.jpg 488w,
									/cms-cps/amendandcancel/images/658technical.jpg 232w" sizes="100vw" alt="" class="dis-block full" />
							</div>
						</div>
						<div class="halfs copy">
							<c:choose>
                                <c:when test="${empty errorMessage}">
                                    <h1>Technical difficulties.</h1>
                                </c:when>
                                <c:otherwise>
                                    <h1>Sorry - <c:out value="${errorMessage}"/>.</h1>
                                </c:otherwise>
                            </c:choose>
							<p class="grey-med marg-bottom-10">We're really sorry we're having some technical problems. Try going <a href="<c:out value='${summaryPageURL}'/>">back to your booking summary page</a>.</p>
					        <p class="grey-med marg-bottom-10"></p>

						</div>
					</div>

				</div>
			</div>

			<!--footer -->
			<c:choose>
				<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKIES'}">
					<jsp:include page="sprocket/footer_tuies.jsp" />
				</c:when>
				<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKICS'}">
					<jsp:include page="sprocket/footer_tuics.jsp" />
				</c:when>
				<c:otherwise>
					<jsp:include page="sprocket/footer.jsp" />
				</c:otherwise>
			</c:choose>
			<div class="page-mask"></div>
		</div>

	</div>
	<script src="/cms-cps/amendandcancel/js/iscroll-lite.js" type="text/javascript"></script>
<script>
	  var tui = {};
	  tui.analytics = {};
	  tui.analytics.page = {};
	  tui.analytics.page.pageUid = "technicaldifficultiespage";
	 </script>
</body>
</html>
