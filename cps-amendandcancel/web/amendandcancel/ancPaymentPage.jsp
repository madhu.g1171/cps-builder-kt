<%@ page import="com.tui.uk.config.ConfReader"%>
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}"
	scope="request" />
<c:set var="hccsmap" value="${bookingComponent.hccsMap}"/>
<!DOCTYPE HTML>
<html lang="en-US" class="" id="falcon">
<head>
<meta charset="UTF-8">
<c:choose>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHOMSON'}">
		<title>TUI Holidays | Discover Your Smile</title>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKICS'}">
		<title>Crystal Ski Holidays | Get More Winter</title>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKIES'}">
		<title>Crystal Ski Holidays | Get More Winter</title>
	</c:when>
	
	
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
		<%@ page contentType="text/html; charset=UTF-8"%>
		<title>TUI Holidays | Discover Your Smile</title>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
		<title>First Choice | The Home of All Inclusive Holidays</title>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHFO'}">
		<title>Flights with TUI | Thomson now TUI Airways</title>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHCRUISE'}">
		<title>Cruise | Payment Details</title>
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO'}">
		<%@ page contentType="text/html; charset=UTF-8"%>
		<title>Flights with TUI | Thomson now TUI Airways</title>
	</c:when>
	<c:otherwise>
		<title>TUI | Payment Details</title>
	</c:otherwise>
</c:choose>
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

<c:choose>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKICS'}">
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskics/css/base-new-ski.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskics/css/base.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskics/css/bf.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskics/css/ac.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskics/css/footer.css" />
		<link rel="icon" type="image/png"
			href="/cms-cps/amendandcancel/ancskics/images/favicon.png" />
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCSKIES'}">
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskies/css/base-new-ski.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskies/css/base.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskies/css/bf.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskies/css/ac.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancskies/css/footer.css" />
		<link rel="icon" type="image/png"
			href="/cms-cps/amendandcancel/ancskies/images/favicon.png" />
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfalcon/css/base.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfalcon/css/base-new-th.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfalcon/css/bf.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfalcon/css/ac.css" />
		<link rel="icon" type="image/png"
			href="/cms-cps/amendandcancel/images/favicon.png" />
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfirstchoice/css/base.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfirstchoice/css/base-new-fc.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfirstchoice/css/bf.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfirstchoice/css/ac.css" />
		<link rel="icon" type="image/png"
			href="/cms-cps/amendandcancel/ancfirstchoice/images/favicon.png" />
	</c:when>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO'}">
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfjfo/css/base.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfjfo/css/base-new-th.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfjfo/css/bf.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/ancfjfo/css/ac.css" />
		<link rel="icon" type="image/png"
			href="/cms-cps/amendandcancel/images/favicon.png" />
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/base.css" />
		<link rel="stylesheet"
			href="/cms-cps/amendandcancel/css/base-new-th.css" />
		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/bf.css" />
		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/ac.css" />
		<link rel="icon" type="image/png"
			href="/cms-cps/amendandcancel/images/favicon.png" />
	</c:otherwise>
</c:choose>

<link rel="stylesheet" href="/cms-cps/amendandcancel/css/fonts.css" />

<%@include file="javascript.jspf"%>
<script type="text/javascript">
		var breadcrumbScroll = null;
		jQuery(document).ready(function() {
			enableSelectBoxes();
			enableCheckBoxes();
			summaryPanelModal();
			intiateIscroll(jQuery(".scroll"));
			changeAddr();
			radioGrpInit();
			initTooltip();

		});
		window.addEventListener("resize", function() {
			var scrollDiv = jQuery('#breadcrumb');
			setTimeout(function() {
				scrollintoView(scrollDiv)
			}, 200);
		}, false);
    </script>
<script src="/cms-cps/amendandcancel/js/config.js"
	type="text/javascript"></script>
<script type="text/javascript">
		var ensLinkTrack = function(){};
</script>
</head>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag"%>
<version-tag:version />
</head>
<c:set var='clientapp'
	value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName}' />
<c:set var="analyticsPageID" value="paymentPage" />
<c:choose>
	<c:when test="${clientapp == 'ANCFALCON'}">
		<c:set var="poundOrEuro" value="&euro;" />
	</c:when>
	<c:when test="${clientapp == 'ANCFJFO'}">
		<c:set var="poundOrEuro" value="&euro;" />
	</c:when>
	<c:when test="${clientapp == 'ANCSKIES'}">
		<c:set var="poundOrEuro" value="&euro;" />
	</c:when>
	<c:otherwise>
		<c:set var="poundOrEuro" value="&pound;" />
	</c:otherwise>
</c:choose>
<fmt:formatNumber var="ancPaymentPageFullAmount"
	value="${bookingComponent.payableAmount.amount-bookingInfo.calculatedDiscount.amount}"
	type="number" maxFractionDigits="2" minFractionDigits="2"
	pattern="#####.##" />
<%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         clientApp = (clientApp!=null)?clientApp.trim():clientApp;
         String analyticsPageID = (String)pageContext.getAttribute("analyticsPageID");
         pageContext.setAttribute("analyticsPageID", analyticsPageID, PageContext.REQUEST_SCOPE);
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
         String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".CV2AVS.Enabled", "false");
         pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
         String isExcursion =
            com.tui.uk.config.ConfReader.getConfEntry(clientApp+".summaryPanelWithExcursionTickets", "false");
         pageContext.setAttribute("isExcursion", isExcursion, PageContext.REQUEST_SCOPE);

      %>
<%
	String applyCreditCardSurcharge = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurcharge", applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurcharge = "<%= applyCreditCardSurcharge %>";
</script>
<c:choose>
	<c:when test="${clientapp == 'ANCFALCON'}">
		<%
	  			String getLoginPageURL =(String)ConfReader.getConfEntry("ANCFALCON.homepage.url","");
	  		 	pageContext.setAttribute("getLoginPageURL",getLoginPageURL);
	  		%>
	</c:when>
	<c:when test="${clientapp == 'ANCFJFO'}">
		<%
	  			String getLoginPageURL =(String)ConfReader.getConfEntry("ANCFJFO.homepage.url","");
	  		 	pageContext.setAttribute("getLoginPageURL",getLoginPageURL);
	  		%>
	</c:when>
	<c:when test="${clientapp == 'ANCFIRSTCHOICE'}">
		<%
	  			String getLoginPageURL =(String)ConfReader.getConfEntry("ANCFIRSTCHOICE.homepage.url","");
	  		 	pageContext.setAttribute("getLoginPageURL",getLoginPageURL);
	  		%>
	</c:when>
	<c:otherwise>
		<%
	  			String getLoginPageURL =(String)ConfReader.getConfEntry("ANCTHOMSON.homepage.url","");
	  		 	pageContext.setAttribute("getLoginPageURL",getLoginPageURL);
	  		%>
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when
		test="${not empty bookingInfo.bookingComponent.clientURLLinks.homePageURL}">
		<c:set var='loginPageURL'
			value="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}" />
	</c:when>
	<c:otherwise>
		<c:set var='loginPageURL' value='${getLoginPageURL}' />
	</c:otherwise>
</c:choose>
<script type="text/javascript" language="javascript">
	function newPopup(url) {
		var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
	    if (win && win.focus) win.focus();
	}

	var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	var _idleSecondsCounter = 0;
	var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	var url="${loginPageURL}";
	var sessionTimeOutFlag =  false;

	window.addEventListener('pageshow', function(event) {
		console.log(event.persisted);
		if(event.persisted) {
			location.reload();
		}
		  if(event.currentTarget.performance.navigation.type == 2)
			{
				 location.reload();
				 }
	});

	function noback(){
		window.history.forward();

	}

	window.setInterval(CheckIdleTime, 1000);
	window.alertMsg();
	function alertMsg(){

	var myvar = setTimeout(function() {
	document.getElementById("sessionTime").style.display = "block";
	//document.getElementById("modal").style.visibility = "visible";
	//document.getElementById("sessionTimeTextbox").style.visibility = "visible";

	var count = Math.round((session_timeout - _idleSecondsCounter)/60);
	if(count == 1)
	{
	   document.getElementById("sessiontimeDisplay").innerHTML = count +" min";
	}
	else
	{
	   document.getElementById("sessiontimeDisplay").innerHTML = count +" mins";
	}
	},IDLE_TIMEOUT_millisecond);

	}

	function CheckIdleTime() {
    	_idleSecondsCounter++;

	    if (  _idleSecondsCounter >= session_timeout) {

	    	if (window.sessionTimeOutFlag == false){
	    		document.getElementById("sessionTime").style.display = "none";
			    document.getElementById("sessionTimeOut").style.display = "block";
	    	}else{
	    		document.getElementById("sessionTimeOut").style.display = "none";
	    	}
		   //navigate to technical difficulty page
		}
	}

	function reloadPage()
	{
		window.sessionTimeOutFlag= true;
		document.getElementById("sessionTimeOut").style.display = "none";
		window.noback();
		window.location.replace(window.url);
		return(false);
	}

	function homepage()
	{
		url="${loginPageURL}";
		window.noback();
		window.location.replace(url);
	}

	function activestate()
    {
		document.getElementById("sessionTime").style.display = "none";
        window.ajaxForCounterReset();
        window.alertMsg();
        window._idleSecondsCounter = 0;
    }

	function ajaxForCounterReset() {
		var token = "<c:out value='${param.token}' />";
		var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		var request = new Ajax.Request(url, {
			method : "POST"
		});
		window._idleSecondsCounter = 0;

	}

	function closesessionTime()
    {
        document.getElementById("sessionTime").style.display = "none";
    }

	function makePayment() {
		var paymentForm = $('CheckoutPaymentDetailsForm');
		var element = paymentForm.serialize();
		if(!window.leadTitle){
			window.leadTitle = "";
		}
		elementstring = element
				+ "&token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
		var url = "/cps/processMobilePayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />";
		elementstring = elementstring.replace("&title=&", "&title=" + leadTitle + "&");
		elementstring = elementstring.replace("payment_0_type=&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		//if(isvalid && cardValid){
			//document.getElementById("modal").style.visibility = "visible";
			var request = new Ajax.Request(url, {
				method : "POST",
				parameters : elementstring,
				onSuccess : showThreeDOverlay
			});
		//}
	}

	function showThreeDOverlay(http_request) {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			result = http_request.responseText;
			if (result.indexOf("3dspage") == -1) {
				if (result.indexOf("ERROR_MESSAGE") == -1) {
					document.postPaymentForm.action = result;
					//document.CheckoutPaymentDetailsForm.reset();
					var csrfTokenVal = "<c:out value="${bookingComponent.nonPaymentData['csrfToken']}"/>";
	                if(csrfTokenVal != '')
	                {
					   var csrfParameter = document.createElement("input");
                       csrfParameter.type = "hidden";
                       csrfParameter.name = "CSRFToken";
                       csrfParameter.value = csrfTokenVal;
                       document.postPaymentForm.appendChild(csrfParameter);
					}
					document.postPaymentForm.submit();
				} else {
					var errorMsg = result.split(':');
						jQuery('.alert').html('<i class="caret warning"></i>'+errorMsg[1]);
						document.getElementById("commonError").style.display = "block";
						jQuery('#commonError').removeClass('hide');
						//document.getElementById('commonError').innerHTML = "<p><strong>"
						//		+ errorMsg[1] + "</p></strong>";
						//document.getElementById('commonError').className = "commonErrorSummary info-section clear padding10px mb20px";
						clearCardEntryElements();
				}
			} else {
				window.location.href = 	result;

			}
		} else {
			document.getElementById('errorMsg').innerHTML = "<h2>Payment failed. Please try again.</h2>";
		}
	}
	window.scrollTo(0, 0);
}

/**
 ** This method submits the bank form present in the overlay and fills the overlay in the iframe.
**/
function bankRedirect() {
	document.bankform.target = "ACSframe";
	document.bankform.submit();
}
function clearCardEntryElements(){
	jQuery("#cardType").val("");
	jQuery("#cardTypespan").html("Select type of credit card");
	jQuery("#cardNumber").val("");
	jQuery("#cardNumberDiv").removeClass("valid");
	jQuery("#cardName").val("");
	jQuery("#cardNameDiv").removeClass("valid");
	jQuery("#expiryDateMM").val("");
	jQuery("#monthspan").html("MM");
	jQuery("#expiryDateYY").val("");
	jQuery("#yearspan").html("YY");
	jQuery("#securityCode").val("");
	jQuery("#securityCodeDiv").removeClass("valid");
	jQuery("#creditPaymentTypeCode").hide();
	jQuery("#debitPaymentTypeCode").hide();
	jQuery("#issueNumber").val("");
    jQuery("#issue").hide();
	document.getElementById("card-img").className = "";
	document.getElementById("card-desc").innerHTML = "";


}
	</script>

<body onload="setDefaultDepositOption();">
	<%@include file="/tuicommon/tealium.jspf" %>
	<form id="CheckoutPaymentDetailsForm" name="paymentdetails"
		method="post" action="javascript:makePayment();"
		<c:if test="${(clientapp != 'ANCTHFO') && (ancPaymentPageFullAmount > 0.0)}">onSubmit="javascript:otherAmountUpdation();"</c:if>>
		<%@include file="ancConfigSettings.jspf"%>

		<div class="structure">

			<div id="page" class="payment full-width">


				<jsp:include page="sprocket/header.jsp" />


				<div id="content" class="book-flow">
					<div class="content-width">
						<%@include file="topError.jspf"%>
						<div id="main">
							<p class="grey-light marg-bottom-15 bread-crumb">
								<a class="ensLinkTrack" data-componentId="WF_COM_ACTOPLEFTSUM"
									href="${bookingComponent.prePaymentUrl}">Summary</a> <i
									class="caret link blue"></i> My Booking
							</p>

							<div class="component-wrap">
								<c:if test="${clientapp == 'ANCTHFO'}">
									<c:if test="${not empty bookingComponent.errorMessage}">
										<c:if
											test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
											<div class="alert med marg-bottom-20">
												<c:if
													test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
													<h5 class="tui black size-22 marg-bottom-5">Your
														flight is now cheaper!</h5>
												</c:if>
												<c:if
													test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
													<h5 class="tui black size-22 marg-bottom-5">The price
														of your flight has changed</h5>
												</c:if>

												<p class="grey-med">
													<c:out value="${bookingComponent.errorMessage}"
														escapeXml="false" />
												</p>
											</div>
										</c:if>
									</c:if>
									<c:if test="${not empty bookingComponent.errorMessage}">
										<c:if
											test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
											<div class="alert med marg-bottom-20">
												<p class="grey-med">
													<c:out value="${bookingComponent.errorMessage}"
														escapeXml="false" />
												</p>
											</div>
										</c:if>
									</c:if>
								</c:if>

								<c:if test="${clientapp == 'ANCFJFO'}">
									<c:if test="${not empty bookingComponent.errorMessage}">
										<c:if
											test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
											<div class="alert med marg-bottom-20">
												<c:if
													test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
													<h5 class="tui black size-22 marg-bottom-5">Your
														flight is now cheaper!</h5>
												</c:if>
												<c:if
													test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
													<h5 class="tui black size-22 marg-bottom-5">The price
														of your flight has changed</h5>
												</c:if>

												<p class="grey-med">
													<c:out value="${bookingComponent.errorMessage}"
														escapeXml="false" />
												</p>
											</div>
										</c:if>
									</c:if>
									<c:if test="${not empty bookingComponent.errorMessage}">
										<c:if
											test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
											<div class="alert med marg-bottom-20">
												<p class="grey-med">
													<c:out value="${bookingComponent.errorMessage}"
														escapeXml="false" />
												</p>
											</div>
										</c:if>
									</c:if>
								</c:if>

								<div id="customer-form" class="full-width">

									<!-- Information -->
									<div class="component">
										<div class="section-heading pad-top-0">
											<h2>Information</h2>
										</div>
										<p>
											Please complete this page within 30 minutes. <span
												class="grey-med">After 30 minutes this page will time
												out and you'll have to re-enter all details.</span>
										</p>
									</div>

									<!-- Make A Payment Section -->
									<%@include file="paymentSummarySection.jspf"%>

									<!-- Secure Online payment -->
									<%@include file="depositSection.jspf"%>

									<c:choose>
										<c:when	test="${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}">
											<!-- Card Details -->
											<%@include file="hccCardDetails.jspf"%>
										</c:when>
										<c:otherwise>
											<%@include file="cardDetails.jspf"%>
											<%@include file="cardHolderAddress.jspf"%>
										</c:otherwise>
									</c:choose>

									<!-- Card Details -->
									<%-- <%@include file="cardDetails.jspf"%> --%>

									<!-- Card holder Details -->
									<%-- <%@include file="cardHolderAddress.jspf"%> --%>

									



									<c:if test="${hccsmap.hccSwitch == 'false'}">

										<c:if test="${clientapp == 'ANCFJFO'}">
											<!-- Fare rules -->
											<%@include file="ancfjfo/fareRules.jspf"%>

											<!-- Baggage check-in safety information -->
											<%@include file="ancfjfo/baggageSafetyinformation.jspf"%>
										</c:if>


										<c:if test="${clientapp == 'ANCTHFO'}">
											<!-- Fare rules -->
											<%@include file="ancthfo/fareRules.jspf"%>

											<!-- Baggage check-in safety information -->
											<%@include file="ancthfo/baggageSafetyinformation.jspf"%>
										</c:if>

										<!-- Terms and Conditions -->
										<%@include file="termsAndCondition.jspf"%>

										<!-- CTA -->
									<span id="paypalspanText">Pay with</span>
									<div class="component text-c">
										<c:if test="${bookingInfo.newHoliday == true}">
											<c:choose>
												<c:when
													test="${requestScope.disablePaymentButton == 'true'}">
													<input type="submit" style="cursor: default;"
														disabled="disabled" value="Make Payment"
														class="book-flow button large cta disabled" id="paypalbutton"/>
												</c:when>
												<c:otherwise>
													<input type="submit" value="Make Payment"
														class="book-flow button large cta" id="paypalbutton"/>
												</c:otherwise>
											</c:choose>
										</c:if>
									</div>
									</c:if>
<span id="paypalspanText"></span>
									<%-- <%@include file="termsAndCondition.jspf"%> --%>

									<!-- CTA -->


								</div>
							</div>


							<!-- Sidebar -->
							<%@include file="summaryPanel.jspf"%>

						</div>
					</div>
				</div>

				<!-- Include for Footer > Thomson -->

				<c:choose>
					<c:when test="${clientapp == 'ANCSKIES'}">
						<jsp:include page="sprocket/footer_tuies.jsp" />
					</c:when>
					<c:when test="${clientapp == 'ANCSKICS'}">
						<jsp:include page="sprocket/footer_tuics.jsp" />
					</c:when>
					<c:otherwise>
						<jsp:include page="sprocket/footer.jsp" />
					</c:otherwise>
				</c:choose>

				<div class="page-mask"></div>
			</div>
		</div>

		<div class="book-flow amend-cancel modal in" style="display: none"
			id="sessionTime">
			<div class="window auto-height">
				<div class="title b">
					<span onclick="closesessionTime()" class="close">y</span>
				</div>
				<div class="modal-content">
					<div class="container c session-timeout">
						<div class="timer b">
						<c:choose>
							<c:when test="${clientapp == 'ANCSKICS' || clientapp == 'ANCSKIES' }">
								<svg width="48px" height="48px" class="" viewBox="0 0 1024 1024" style="fill: #252a32; stroke: #252a32;"><path d="M512 1024c-282.453 0-512-229.547-512-512s229.547-512 512-512 512 229.547 512 512-229.547 512-512 512zM512 940.373c236.373 0 428.373-192 428.373-428.373s-192-428.373-428.373-428.373-428.373 192-428.373 428.373 192 428.373 428.373 428.373zM378.027 587.093c-16.213 16.213-16.213 42.667 0 58.88s42.667 16.213 58.88 0l104.107-104.107c7.68-7.68 11.947-18.773 11.947-29.867v-313.173c0-23.040-18.773-41.813-41.813-41.813s-41.813 18.773-41.813 41.813v296.107l-91.307 92.16z"></path></svg>
								<span class="size-15">Time
								out in <span id="sessiontimeDisplay"></span>
							</c:when>
							<c:otherwise>
								<i class="caret blue time fl"></i> <span class="size-15">Time
								out in <span id="sessiontimeDisplay"></span>
							</span>
							</c:otherwise>
						</c:choose>
							
						</div>
						<div class="component">
							<h1 class="tui marg-bottom-15 grey-dark">Are you still
								there?</h1>
							<p class="marg-bottom-20">This page will be timing out for
								security reasons. Let us know you are still here, otherwise
								you'll have to re-enter your details.</p>
							<div class="c">
								<a class="book-flow button halfs fr" href="#"
									onclick="activestate()">I'M Still here</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="book-flow amend-cancel modal in" style="display: none"
			id="sessionTimeOut">
			<c:set var="url" value="${loginPageURL}" />
			<div class="window auto-height">
				<div class="title b">
					<span onclick="reloadPage()" class="close">y</span>
				</div>
				<div class="modal-content">
					<div class="container c session-timeout">
						<div class="component">
							<h1 class="tui marg-bottom-15 grey-dark">Sorry, this page
								has timed out.</h1>
							<p class="marg-bottom-20">
								Unfortunately this page has timed out for security reasons.
								Please <a href="#" onclick="homepage()">log back in to your
									booking.</a>
							</p>
							<div class="c">
								<a class="book-flow button halfs fr" onclick="reloadPage()">ok</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="video-container" class="hide"></div>
		<fmt:formatNumber
			value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
			var="totalCostingLine" type="number" pattern="####"
			maxFractionDigits="2" minFractionDigits="2" />
		<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
		<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
		<!-- Summary Flyout -->
		<div class="modal flyout ">
			<div class="window">

				<div class="summary-panel bg-sand">
					<div class="image-container rel">
						<div class="crop nomobile">
							<c:choose>
								<c:when test="${clientapp == 'ANCTHFO'}">
									<c:choose>
										<c:when
											test="${(not empty bookingComponent.accommodationSummary) && (not empty bookingComponent.accommodationSummary.additionalDetails['ACCOMMODATION_IMAGE_URL'])}">
											<img class="dis-block" alt="" sizes="100vw"
												src="${bookingComponent.accommodationSummary.additionalDetails['ACCOMMODATION_IMAGE_URL']}">
										</c:when>
										<c:otherwise>
											<img class="dis-block" alt="" sizes="100vw"
												src="/cms-cps/amendandcancel/images/Discover_01.jpg">
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:when test="${clientapp == 'ANCTHCRUISE'}">
									<c:choose>
										<c:when
											test="${(not empty bookingComponent.cruiseSummary) && (not empty bookingComponent.cruiseSummary.additionalDetails['ACCOMMODATION_IMAGE_URL'])}">
											<img class="dis-block" alt="" sizes="100vw"
												src="${bookingComponent.cruiseSummary.additionalDetails['ACCOMMODATION_IMAGE_URL']}">
										</c:when>
										<c:otherwise>
											<c:if
												test="${(not empty bookingComponent.accommodationSummary) && (not empty bookingComponent.accommodationSummary.additionalDetails['ACCOMMODATION_IMAGE_URL'])}">
												<img class="dis-block" alt="" sizes="100vw"
													src="${bookingComponent.accommodationSummary.additionalDetails['ACCOMMODATION_IMAGE_URL']}">
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:if
										test="${(not empty bookingComponent.accommodationSummary) && (not empty bookingComponent.accommodationSummary.additionalDetails['ACCOMMODATION_IMAGE_URL'])}">
										<img class="dis-block" alt="" sizes="100vw"
											src="${bookingComponent.accommodationSummary.additionalDetails['ACCOMMODATION_IMAGE_URL']}">
									</c:if>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="total c bg-tui-sand black">
							<h3>${poundOrEuro}</h3>
							<h2>
								<c:out value="${totalcost[0]}." />
								<span><c:out value="${totalcost[1]}" /></span>
							</h2>
							<div>Total price</div>
							<i class="caret close b abs"></i>
						</div>
					</div>
					<div class="scroll-container"
						data-scroll-options='{"scrollX": false, "scrollY": true, "keyBindings": true, "mouseWheel": true}'>
						<div class="summary-breakdown">
							<%@include file="summaryPanel/paymentHistory.jspf"%>
							<%@include file="summaryPanel/priceBreakdown.jspf"%>
						</div>

					</div>
				</div>

			</div>
		</div>
		<div class="tooltip top center tp" id="tooltipTmpl">
			<p></p>
			<span class="arrow"></span>
		</div>
		<!-- <script type="text/javascript" src="/cms-cps/amendandcancel/js/vs.js"></script> -->
		<script src="/cms-cps/amendandcancel/js/iscroll-lite.js"
			type="text/javascript"></script>
	</form>
	<div id="overlay" class="posFix"></div>
	<form novalidate name="postPaymentForm" method="post">
		<input type="hidden" name="emptyForm" class="disNone"></input>
	</form>
</body>
</html>
