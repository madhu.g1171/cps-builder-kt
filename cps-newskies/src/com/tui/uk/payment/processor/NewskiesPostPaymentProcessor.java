package com.tui.uk.payment.processor;

import java.util.List;
import java.util.Map;

import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.config.ConfReader;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for newskies.
 *
 * @author rajarao.r
 *
 */
public class NewskiesPostPaymentProcessor extends DotNetClientPostPaymentProcessor
{
	// CHECKSTYLE:OFF
   /**
    *
    * Constructor with Payament data and Request parameter Map.
    *
    * @param paymentData the Payment Data object.
    * @param requestParameterMap as the request parameter.
    *
    */
   public NewskiesPostPaymentProcessor(PaymentData paymentData,
                                       Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);

   }

   /** The passenger key. */

   private static final String PASSENGER_KEY = "passenger_";

   /**
    * This method is responsible for creating transaction and updating the Payment Data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions .
    * @throws PostPaymentProcessorException if the amount paid and payable amount do not match
    *
    *
    */
   @Override
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {

      super.process();

   }

   /** The passenger key. */

   // private static final String PASSENGER_KEY = "passenger_";

   /**
    * This method is responsible for creating transaction and updating the Payment Data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions .
    * @throws PostPaymentProcessorException if the amount paid and payable amount do not match
    *
    *
    */
   @Override
   public void preProcess() throws PostPaymentProcessorException
   {
	   boolean hccSwitch = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
     if(!hccSwitch){
		if ((requestParameterMap.get("payment_0_cardNumber") != null)
				&& (requestParameterMap.get("payment_0_type") != null)) {

			String thCCBinRange = "ThomsonCreditcard.BINRange";
			String thCCNumberSelected = null;
			String thCCConfiguration = ConfReader.getConfEntry(thCCBinRange, null);
			String cardNumber = requestParameterMap.get("payment_0_cardNumber");
			String[] thCCNumberList = thCCConfiguration.split(",");
			for (String thCCNumber : thCCNumberList) {
				if (cardNumber.startsWith(thCCNumber)) {
					if (requestParameterMap.get("payment_0_type").contains("TUI_MASTERCARD")) {
						thCCNumberSelected = thCCNumber;
						break;
					} else {
						throw new PostPaymentProcessorException(
								"datacash.cardtype.mismatch");
					}

				}
			}
			if (thCCNumberSelected != null) {
				if (!(requestParameterMap
						.get("payment_0_type")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}else
			{
				if ((requestParameterMap
						.get("payment_0_type")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}
		}
     }
      super.preProcess();

   }

   /**
    * Updates the passenger information.
    */
   @Override
   protected void updatePassengerInformation()
   {
      int passengerIndex = 0;
      int childPassengerIndex = 1;
      int infantPassengerIndex = 1;

      int adultCount = 0;

      Map<String, String> nonPaymentData = bookingComponent.getNonPaymentData();
      if (nonPaymentData.containsKey("passenger_ADT_count"))
      {
         adultCount = Integer.valueOf(nonPaymentData.get("passenger_ADT_count"));
      }

      int childCount = 0;
      if (nonPaymentData.containsKey("passenger_CHD_count"))
      {
         childCount = Integer.valueOf(nonPaymentData.get("passenger_CHD_count"));
      }

      int infantCount = 0;
      if (nonPaymentData.containsKey("passenger_INF_count"))
      {
         infantCount = Integer.valueOf(nonPaymentData.get("passenger_INF_count"));
      }

      if (bookingComponent.getPassengerRoomSummary() != null)
      {
         for (List<PassengerSummary> passengerSummaryList : bookingComponent
            .getPassengerRoomSummary().values())
         {
            for (PassengerSummary passengerSummary : passengerSummaryList)
            {
               if (passengerIndex <= adultCount)
               {
                  if (requestParameterMap.containsKey(PASSENGER_KEY + passengerIndex + "_title"))
                  {
                     passengerSummary.setTitle(requestParameterMap.get(PASSENGER_KEY
                        + passengerIndex + "_title"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + passengerIndex + "_foreName"))
                  {
                     passengerSummary.setForeName(requestParameterMap.get(PASSENGER_KEY
                        + passengerIndex + "_foreName"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + passengerIndex + "_lastName"))
                  {
                     passengerSummary.setLastName(requestParameterMap.get(PASSENGER_KEY
                        + passengerIndex + "_lastName"));
                  }
                  if (passengerIndex == 1)
                  {
                     passengerSummary.setLeadPassenger(true);
                  }
                  passengerIndex++;
               }
               else if (childPassengerIndex <= childCount)
               {
                  if (requestParameterMap.containsKey(PASSENGER_KEY + childPassengerIndex
                     + "_childTitle"))
                  {
                     passengerSummary.setTitle(requestParameterMap.get(PASSENGER_KEY
                        + childPassengerIndex + "_childTitle"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + childPassengerIndex
                     + "_childForeName"))
                  {
                     passengerSummary.setForeName(requestParameterMap.get(PASSENGER_KEY
                        + childPassengerIndex + "_childForeName"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + childPassengerIndex
                     + "_childLastName"))
                  {
                     passengerSummary.setLastName(requestParameterMap.get(PASSENGER_KEY
                        + childPassengerIndex + "_childLastName"));
                  }
                  childPassengerIndex++;
               }
               else if (infantPassengerIndex <= infantCount)
               {
                  if (requestParameterMap.containsKey(PASSENGER_KEY + infantPassengerIndex
                     + "_infantTitle"))
                  {
                     passengerSummary.setTitle(requestParameterMap.get(PASSENGER_KEY
                        + infantPassengerIndex + "_infantTitle"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + infantPassengerIndex
                     + "_infantForeName"))
                  {
                     passengerSummary.setForeName(requestParameterMap.get(PASSENGER_KEY
                        + infantPassengerIndex + "_infantForeName"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + infantPassengerIndex
                     + "_infantLastName"))
                  {
                     passengerSummary.setLastName(requestParameterMap.get(PASSENGER_KEY
                        + infantPassengerIndex + "_infantLastName"));
                  }
                  infantPassengerIndex++;
               }
            }
         }
      }
   }

}
