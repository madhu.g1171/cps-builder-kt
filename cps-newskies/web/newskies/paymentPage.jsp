<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <title>Payments</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <version-tag:version/>
      <%@include file="javascript.jspf" %>
      <%@include file="configSettings.jspf" %>
      <%@include file="css.jspf" %>
      <%@include file="tag.jsp"%>
      <%@include file="/common/commonTagLibs.jspf"%>
<c:set var="hccsmap" value="${bookingComponent.hccsMap}" scope="page"/>
      <c:choose>

   <c:when test="${TUISwitch3 eq 'ON'}">
      <c:set value="www.tui.co.uk" var="tuiFlagLink" />
   </c:when>
   <c:otherwise>
      <c:set value="www.thomson.co.uk" var="tuiFlagLink" />
   </c:otherwise>
</c:choose>

      <%

         String clientApp = (String)pageContext.getAttribute("clientapp");
         String axaInsurance = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".axaInsuranceEnabled","false");
         pageContext.setAttribute("axaInsurance", axaInsurance, PageContext.SESSION_SCOPE);
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
      %>
<%
	String applyCreditCardSurchargeGBP = com.tui.uk.config.ConfReader.getConfEntry("Newskies.GBP.applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurchargeGBP", applyCreditCardSurchargeGBP, PageContext.REQUEST_SCOPE);
	String applyCreditCardSurchargeEUR = com.tui.uk.config.ConfReader.getConfEntry("Newskies.EUR.applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurchargeEUR", applyCreditCardSurchargeEUR, PageContext.REQUEST_SCOPE);
	String hccIframeEnable = com.tui.uk.config.ConfReader.getConfEntry("NEWSKIES.hcc.switch" ,"false");
	pageContext.setAttribute("hccIframeEnable", hccIframeEnable, PageContext.REQUEST_SCOPE);
%>
<script>
var hccIframe = "${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}"
applyCreditCardSurchargeGBP = "<%= applyCreditCardSurchargeGBP %>";
applyCreditCardSurchargeEUR = "<%= applyCreditCardSurchargeEUR %>";
</script>
   </head>
   <body>
   <style>
   @font-face {
      font-family: 'TUITypeLightRegular';
      src: url('/cms-cps/newskies/css/webfont/webfont.tuitypelt.eot');
      src: local('TUITypeLightRegular'),
      url('/cms-cps/newskies/css/webfont/webfont.tuitypelt.woff') format('woff'),
      url('/cms-cps/newskies/css/webfont/webfont.tuitypelt.ttf') format('truetype'),
      url('/cms-cps/newskies/css/webfont/webfont.tuitypelt.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
   @font-face {
      font-family: 'TUITypeRegular';
      src: url('/cms-cps/newskies/css/webfont/webfont.tuitype.eot');
      src: local('TUITypeRegular'),
      url('/cms-cps/newskies/css/webfont/webfont.tuitype.woff') format('woff'),
      url('/cms-cps/newskies/css/webfont/webfont.tuitype.ttf') format('truetype'),
      url('/cms-cps/newskies/css/webfont/webfont.tuitype.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
   </style>
  <style type='text/css'>
			div.selectBox
			{
				position: absolute;
				display: inline-block;
				cursor: default;
				text-align: left;
				line-height: 30px;
				clear: both;
				color: #000;
				float: left;
				// left: 84px;
			}
			span.selected
			{
				width:44px;
				text-indent:5px;
				border:1px solid #aaa;
				border-right:none;
				border-top-left-radius:3px;
				border-bottom-left-radius:3px;
				background:#FFF;
				overflow:hidden;
			}
			span.selectArrow
			{
				width:30px;
				border:1px solid #aaa;
				border-top-right-radius:3px;
				border-bottom-right-radius:3px;
				text-align:center;
				// font-size:20px;
				-webkit-user-select: none;
				-khtml-user-select: none;
				-moz-user-select: none;
				-o-user-select: none;
				user-select: none;
				background: #DFE0E2;
				position: absolute !important;
				font-size: 10px;
			}

			span.selectArrow,span.selected
			{
				position:relative;
				float:left;
				height:27px;
				z-index:1;
			}

			div.selectOptions
			{
				position: absolute;
				top: 28px;
				left: 0;
				width: 75px;
				border: 1px solid #ccc;
				border-bottom-right-radius: 5px;
				border-bottom-left-radius: 5px;
				overflow: hidden;
				background: #f6f6f6;
				padding-top: 2px;
				display: none;
				 z-index: 999;
			}

			span.selectOption
			{
				display:block;
				// width:70px;
				line-height:20px;
				padding:5px 10%;
			}

			span.selectOption:hover
			{
				color:#f6f6f6;
				background:#4096ee;
			}
		</style>
		<script type='text/javascript'><!--
			$(document).ready(function() {
			//alert("hi");
				enableSelectBoxes();
			});

			function enableSelectBoxes(){
				$('div.selectBox').each(function(){
					$(this).children('span.selected').html($(this).children('div.selectOptions').children('span.selectOption:first').html());
					$(this).attr('value',$(this).children('div.selectOptions').children('span.selectOption:first').attr('value'));

					$(this).children('span.selected,span.selectArrow').click(function(){
						if($(this).parent().children('div.selectOptions').css('display') == 'none'){
							$(this).parent().children('div.selectOptions').css('display','block');
						}
						else
						{
							$(this).parent().children('div.selectOptions').css('display','none');
						}
					});

					$(this).find('span.selectOption').click(function(){
						$(this).parent().css('display','none');
						$(this).closest('div.selectBox').attr('value',$(this).attr('value'));
						$(this).parent().siblings('span.selected').html($(this).html());
					});
				});
			}//-->
		</script>
      <!--
      Start of DoubleClick Floodlight Tag: Please do not remove
      Activity name of this tag: Thomson Payment Page
      URL of the webpage where the tag is expected to be placed: http://www.thomson.co.uk
      This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
      Creation Date: 10/05/2010
      -->
      <script type="text/javascript">
         var axel = Math.random() + "";
         var a = axel * 10000000000000;
         document.write('<iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=thoms460;cat=thoms149;ord=1;num=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
      </script>
      <noscript>
         <iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=thoms460;cat=thoms149;ord=1;num=1?" width="1" height="1" frameborder="0"></iframe>
      </noscript>
      <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
	  <div class="pageContainer" id="payments">
	     <%@include file="header.jspf" %>
	     <div class="mboxDefault">
         </div>
         <script type="text/javascript">
            mboxCreate("T_PackagePayment_Text");
         </script>
		 <div id="contentSection">
			<%@include file="breadcrumb.jsp" %>
<!--		     <div class="pageHeader">  commented for ACSS Bug Fix#114070-->
   				<h1>Passenger &amp; payment details</h1>
<!--        	  </div> commented for ACSS Bug Fix#114070-->
			<p class="intro"></p>
            <div id="contentCol2">
			 <c:choose>
							<c:when	test="${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}">
<!--			 <form id="paymentdetails" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />" method="post" autocomplete="off">-->
			     <form name="paymentdetails" method="post" action="/cps/acssPDVServlet?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />">
			      <%@include file="topError.jspf" %>
				  <div class="flexiBox greyContainer">
				     <div class="top"><span class="border">&nbsp;</span></div>
					 <div class="middle">
					    <span class="shadow">&nbsp;</span>
					    <div class="body">
							<%@include file="passengerDetails.jsp" %>
							 <%@include file="fullPayment.jspf" %>
							 <c:if test="${bookingInfo.newHoliday}">
								 <%@include file="bookHoliday.jspf" %>
							   </c:if>
							   <div class="hide" id="iframeHcc">
								 <iframe name="HCCRedirectToCPS" id="hcctoCps"  src="${hccsmap.hpsUrl}?HPS_SessionID=${hccsmap.hpsSession}" style="width: 100%;height: 900px; border: none;">
								</iframe>
								</div>
							
					    </div>
					 </div>
				  </div>
			   </form>
			   </c:when>  
			   <c:otherwise>
							  
			   <form id="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />"  autocomplete="off">
			     <!--<form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />"-->
			      <%@include file="topError.jspf" %>
				  <div class="flexiBox greyContainer">
				     <div class="top"><span class="border">&nbsp;</span></div>
					 <div class="middle">
					    <span class="shadow">&nbsp;</span>
					    <div class="body">
						   <%@include file="passengerDetails.jsp" %>
						   <%@include file="fullPayment.jspf" %>
						   <%@include file="paymentDetails.jsp" %>
						   <%@include file="importantInformation.jspf" %>
						   <div class="bottom"><span class="border">&nbsp;</span></div>
						   <c:if test="${bookingInfo.newHoliday}">
							 <%@include file="bookHoliday.jspf" %>
						   </c:if> 
					    </div>
					 </div>
				  </div>
			   </form>
				</c:otherwise>
			</c:choose>
		    </div>
            <div id="contentCol1">
               <%@include file="summaryPanel.jsp" %>
	        </div>
		    <div id="footerSection">
		       <%@include file="footer.jspf" %>
	        </div>
       	 </div>
      </div>
      <!-- <script type="text/javascript" src="/cms-cps/newskies/js/vs.js"></script> -->
      <script type="text/javascript">
       window.onload=
        function()
        {
    	   amountUpdationWithCardCharge.updateCardChargeForNEWSKIES()
        }
      </script>
   </body>
</html>