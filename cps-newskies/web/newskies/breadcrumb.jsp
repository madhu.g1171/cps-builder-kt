<div class="row">
    <div class="meter">

        <ul>
                            <li class="meter-prev">Booking<div class="breadcrumb-separator"></div></li>
                            <li class="meter-prev">Seats<div class="breadcrumb-separator"></div></li>
							<c:choose>
                            <c:when test="${bookingComponent.nonPaymentData['is_thirdparty_flight'] =='true'}">
                            <li class="meter-disabled">Extras<div class="breadcrumb-separator"></div></li>
							</c:when>
							<c:otherwise>
							<li class="meter-prev">Extras<div class="breadcrumb-separator"></div></li>                       
							</c:otherwise>
							</c:choose>
							<li class="meter-current">Payment<div class="breadcrumb-separator"></div></li>
                            <li class="meter-next">Documents<div class="breadcrumb-separator"></div></li>
                            <li class="meter-next">Check-In</li>

        </ul>
    </div>
</div>
