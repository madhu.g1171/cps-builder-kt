<input type='hidden'  id='panelType' value='CNP'/>
<input type='hidden' value='' name='payment_0_paymenttypecode' id='payment_0_paymenttypecode'/>
<input type='hidden' id='total_amount_0' name='payment_0_amountPaid' value='0.0'/>
<input name='payment_0_cardCharge' type='hidden' id='cardChargeAmount_0'/>
<input name='payment_0_cardLevyPercentage' type='hidden' id='cardLevyCharge_0' value='0.0'/>

<h4>Card details</h4>
<div id="cardDetailsError" class="formError hide">
   <p class="formErrorMessage">Unfortunately we could not verify your card details</p>
</div>
<ul class="formFormat cardDetailsForm">
   <li class="cardTypeError cardType">
      <label for="cardType"><div class="address_all_card_type">Card type</div></label>
	  <div class="add_input">
	  <div class="styleSelect" style="margin-left: 9px; width: 195px; ">
	  <select id="cardType" name="payment_0_type" gfv_required="required" onchange="CardTypeChangeHandler.handleCardSelection(),amountUpdationWithCardCharge.updatePaymentInfoForNEWSKIES()" alt="Card Type" class="card_type">
         <option id="pleaseSelect" value='' selected="selected">Please select</option>
         <c:choose>
<c:when test="${TUISwitch3 eq 'ON'}">
<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'>
				 <c:choose>
				 <c:when test="${paymentType.paymentCode eq 'TUI_MASTERCARD'}">
<c:out value="TUI CreditCard" escapeXml='false'/> </option>
</c:when>
<c:otherwise>
<c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
</c:otherwise>
</c:choose>
         </c:forEach>
</c:when>
<c:otherwise>
<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
         </c:forEach>
</c:otherwise>
</c:choose>
         <c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
         </c:forEach>
      </select>
	  </div>
	    </div>
   </li>
   <li class="cardNumberError cardnumber">
      <label for="cardNumber"><div class="address_all_card">Card number</div></label>
	  <div class="add_input"><input type="cardnumber" id="cardNumber" class="textfield"  name='payment_0_cardNumber' gfv_required="required" maxlength="20" autocomplete="off" alt="Card Number"/></div>
   </li>
   <li class="cardNameError cardName">
      <label for="cardName"><div class="address_all_card">Name on card</div></label>
      <div class="address_all_card_name"><input type="alpha" id="cardName" name="payment_0_nameOnCard" class="textfield" gfv_required="required" maxlength='25' autocomplete="off" alt="Name On Card"/></div>
   </li>
   <li class="expiryDateYYError expiryDateMMError expiryDate">
      <div class="address_all_card exp_d">Expiry date</div></label>
	   <div class="add_input_card_exp">
	  <div class="styleSelect" style=" margin-left: 7px; width: 75px; ">
      <select id="expiryDateMM" name="payment_0_expiryMonth" class="smallList expiryDateMM" gfv_required="required" alt="Expiry Date" type="expirydate">
         <option value="">MM</option>
         <option value='01'>01</option>
         <option value='02'>02</option>
         <option value='03'>03</option>
         <option value='04'>04</option>
         <option value='05'>05</option>
         <option value='06'>06</option>
         <option value='07'>07</option>
         <option value='08'>08</option>
         <option value='09'>09</option>
         <option value='10'>10</option>
         <option value='11'>11</option>
         <option value='12'>12</option>
      </select>
	  </div>
	  <div class="styleSelect" style=" margin-left: 7px; width: 75px; ">
      <select id="expiryDateYY" name="payment_0_expiryYear" class="smallList expiryDateYY" gfv_required="required" alt="Expiry Date" type="expirydate">
         <option value="">YY</option>
         <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
            <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
         </c:forEach>
      </select>
	  </div>
	   </div>
   </li>
   <li class="securityCodeError securitycode">
      <label for="securityCode"><div class="address_all_card">Security code</div></label>
      <div class="add_input_sec"><input type="securitycode" id="securityCode" name="payment_0_securityCode" class="textfieldSmall" gfv_required="required" maxLength="4" autocomplete="off" alt="Security Code"/></div>
   </li>
   <li class="issueNumberError hide" id="issueNumber">
      <label for="issueNumber"><div class="address_all_card">Issue number</div></label>
	  <div class="add_input"><input type="numeric" id="issueNumber" name="payment_0_issueNumber" class="textfieldSmall" maxlength="2" autocomplete="off" alt="Issue Number" style="margin-left: 10px;"/></div>
   </li>
   <c:if test="${not empty bookingInfo.threeDEnabledLogos}">
      <li class="indent threeDSLogoContainerHeight cardLogos" style="padding:0 0 0 95px;">
         <c:set var="isMastercardLogoPresent" value="false"/>
		 <c:set var="isVisaLogoPresent" value="false"/>
         <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
            <c:choose>
               <c:when test="${threeDLogos == 'mastercardgroup'}">
                  <c:set var="isMastercardLogoPresent" value="true"/>
                  <div>
                     <a href="javascript:void(0);" id="masterCardDetails" class="logoMasterCard threeDSstickyOwner card_logo"><img title="MasterCard" src="/cms-cps/newskies/images/logos/logo-mastercard.gif"/>Learn more</a>
                     <%@ include file="mastercardLearnMoreSticky.jspf"%>
                  </div>
               </c:when>
            </c:choose>
         </c:forEach>
         <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
            <c:choose>
               <c:when test="${threeDLogos == 'visagroup'}">
				  <c:set var="isVisaLogoPresent" value="true"/>
                  <div <c:if test="${isMastercardLogoPresent}">style="margin-left:97px"</c:if>>
                     <a href="javascript:void(0);" id="visaDetails" class="logoVisa threeDSstickyOwner card_logo"><img title="Visa" src="/cms-cps/newskies/images/logos/logo-visa.gif"/>Learn more</a>
                     <%@ include file="visaLearnMoreSticky.jspf"%>
                  </div>
               </c:when>
            </c:choose>
         </c:forEach>
		 <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
            <c:choose>
               <c:when test="${threeDLogos == 'americanexpressgroup'}">
                  <div <c:if test="${isVisaLogoPresent}">style="margin-left:167px"</c:if>>
                     <a href="javascript:void(0);" id="amexDetails" class="logoAmex threeDSstickyOwner card_logo"><img title="amex" src="/cms-cps/newskies/images/logos/logo-amex.gif"/>Learn more</a>
                     <%@ include file="amexLearnMoreSticky.jspf"%>
                  </div>
               </c:when>
            </c:choose>
         </c:forEach>
      </li>
   </c:if>
</ul>
<fmt:formatNumber var="transactionTotal" value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" scope="request" pattern="######.##" />
<fmt:formatNumber var="amountWithCardCharge" value="${transactionTotal+applicableCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>
<fmt:formatNumber var="amountWithDebitCardCharge" value="${transactionTotal+applicableDebitCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>

<ul class="paymentAmount">
   <li id="amountWithoutCardCharge" class="fontWeightNormal">
      <span class="amount" id="spanAmountWithoutCardCharge"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithDebitCardCharge}"/></span><br/>
      <span class="amount_det">Payable today if paying by Debit Card</span>
   </li>
   <li id="thomsonCardCharge" class="fontWeightNormal" style=" display :none">
      <span class="amount" id="spanthomsonCardCharge"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithDebitCardCharge}"/></span><br/>
      <span class="amount_det">Includes no extra charge for using Thomson credit card</span>
   </li>
   <li id="amountWithCardCharge" class="fontWeightNormal">
      <span class="amount" id="spanAmountWithCardCharge"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithCardCharge}"/></span><br/>
      <span class="amount_det">Payable today if paying by Credit Card
	
	 <c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
	  <c:if test="${applyCreditCardSurchargeGBP eq 'true'}">
	  <p class="surcharge_note">Includes <c:out value="${cardChargePercent}"/>% Credit Card surcharge</p>
	  </c:if>
	  </c:if>
	  <c:if test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
	  <c:if test="${applyCreditCardSurchargeEUR eq 'true'}">
	  <p class="surcharge_note">Includes <c:out value="${cardChargePercent}"/>% Credit Card surcharge</p>
	  </c:if>
	  </c:if>
      </span>
   </li>
</ul>

<%-- *******Essential fields ********************  --%>
<input type="hidden" name="total_transamt"  id="total_transamt"  value='NA' />
<%-- *******End Essential fields ********************  --%>