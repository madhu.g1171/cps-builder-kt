<!--START LEFT PANEL -->
<h2 class="offScreen">Summary</h2>
<div class="flexiBox leftPanel">
   <div class="top">
      <span class="border">&nbsp;</span>
   </div>
   <div class="middle">
      <span class="shadow">&nbsp;</span>
      <div class="body">
         <div class="pricePanel paddingLeftRight5 summaryPanelBorder">
            <%@include file="pricePanel.jsp" %>
	 
</div>
   </div></div>
   <div class="bottom" id="summaryPanelBottomBorder">
      <span class="border">&nbsp;</span>
   </div>
</div>
<%--<p class="ipSmall">Your IP Address:&nbsp;<c:out value='${pageContext.request.remoteHost}'/></p>--%>
<!--END LEFT PANEL -->