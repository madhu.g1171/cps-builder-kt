<h3>Lead Passenger Details</h3>
<fieldset>
  <p></p>
   <ul class="bgrWhite">
      <li class="padding_det">
         <%@include file="leadPassenger.jsp" %>
      </li>
 	  <li class="padding_det">
         <h4>Contact details</h4>
		 <%-- <div class="formError hide" id="addressFinderError">
		    <p class="formErrorMessage">Unfortunately we could not find your postcode, please enter your address manually</p>
		 </div>--%>
		 <ul class="formFormat">
            <%@include file="leadPassengerContactDetails.jsp" %>
	     </ul>
		 
	  </li>
	 </ul>
</fieldset>
<div class="bottomImage"></div>