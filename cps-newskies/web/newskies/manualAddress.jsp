<%-- Manual address START --%>
<li id="manualAddress">
<ul>
<li class="houseNameError houseName">
   <label for="houseName"><div class="address_1">House name/No</div></label>
   <div class="add_input"><input type="alphanumericspec" id="houseName" name="houseName" class="textfield" value="<c:out value="${bookingComponent.nonPaymentData['houseName']}"/>" maxlength="20" gfv_required="required" alt="House Name/No"/></div>
</li> 
<li class="addressLine1Error">
   <label for="addressLine1"><div class="address_all">Address 1</div></label>
   <div class="add_input"><input type="alpha" id="addressLine1" name="addressLine1" class="textfield" value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>" maxlength="25" gfv_required="required" alt="Street Address1"/></div>
</li>
<li class="addressLine2Error">
   <label for="addressLine2"><div class="address_all">Address 2</div></label>
   <div class="add_input"><input type="alpha" id="addressLine2" name="addressLine2" class="textfield" value="<c:out value="${bookingComponent.nonPaymentData['addressLine2']}"/>" maxlength="25" alt="Street Address2"/></div>
</li>
<li class="cityError">
   <label for="city"><div class="address_all">Town/City</div></label>
   <div class="add_input"><input type="alpha" id="city" name="city" class="textfield" value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>" maxlength="25" gfv_required="required" alt="Town/City"/></div>
</li>
<li class="countyError">
   <label for="county"><div class="address_all">County</div></label>
   <div class="add_input"><input type="alpha" id="county" name="county" class="textfieldSmall" value="<c:out value="${bookingComponent.nonPaymentData['county']}"/>" maxlength="16" alt="County"/></div>
   <p class="note">Optional</p>
</li>
<li class="countryError ">
	  <label for="country"><div class="address_all">Country</div></label>
      <div class="add_input">
	  <div style="margin-left: 7px; width: 206px; " class="styleSelect">
		<c:set var="selectedCountryName" value="${bookingComponent.nonPaymentData['country']}"/>
		 <select name="country" id="country" gfv_required="required" requiredError="Your country" value="<c:out value="${bookingComponent.nonPaymentData['country']}"/>" gfv_required="required" alt="Country"
		  onchange="postcodeValidation.postcodeChange()">
			<option id="pleaseSelect" value='' selected="selected">Please select</option>
			
			<c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
			
	            <c:set var="countryCode" value="${CountriesList.key}" />
	            <c:set var="countryName" value="${CountriesList.value}" />
	            <c:if test='${countryCode=="GB"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
			   
            </option>
            </c:if>
         </c:forEach>
		 <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
			
	            <c:set var="countryCode" value="${CountriesList.key}" />
	            <c:set var="countryName" value="${CountriesList.value}" />
	            <c:if test='${countryCode =="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
			   
            </option>
            </c:if>
         </c:forEach>
			<option class="select-dash" disabled="disabled">---------------------------</option>
	        <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
	            <c:set var="countryCode" value="${CountriesList.key}" />
	            <c:set var="countryName" value="${CountriesList.value}" />
	            <c:if test='${countryCode!="GB" && countryCode !="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
        </select>
		</div>
	  </div>
   </li>
<li class="postCodeError" >
   <label for="postCode"><div class="address_all">Postcode</div></label>
   <div  class="add_input"  ><input type="postcode"  id="postCode" name="postCode" class="textfieldSmall" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>" maxlength="8" alt="Post Code" gfv_required_postcode="required_postcode"/>
   
   </div>
</li>
</ul>
</li>
<%-- Manual address END --%>