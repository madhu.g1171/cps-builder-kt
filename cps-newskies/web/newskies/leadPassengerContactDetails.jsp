<%-- Address finder Start --%>
<%-- <li id="addressFinder">
   <label for="leadPostcode">Postcode</label>
   <input type="text" id="leadPostcode" name="leadPostcode" class="textfieldSmall" maxlength="8"/>
   <input type="button" value="Find my address" class="button" onClick="AddressFinderHandler.handle();"/>
   <p class="note"><a href="javascript:void(0);" onClick="AddressFinderHandler.manualAddress();">Enter address manually</a></p>
</li> --%>
<%-- Address finder end --%>
<%--<%@include file="addressFinderResults.jspf" %>--%>
<%@include file="manualAddress.jsp" %>
<li id="phone_Numbers">
<ul>
<li class="dayTimePhoneError">
   <label for="dayTimePhone"><div class="address_all">Phone</div></label>
   <div class="add_input"><input type="phonenumber" id="dayTimePhone" name="dayTimePhone" value="<c:out value="${bookingComponent.nonPaymentData['dayTimePhone']}"/>" class="textfield" gfv_required="required" maxlength="15" alt="Telephone Number"/></div>
</li>
<li class="mobilePhoneError mobile_ph">
   <label for="mobilePhone"><div class="address_all">Mobile</div></label>
   <div class="add_input_mobile"><input type="phonenumber" id="mobilePhone" name="mobilePhone" value="<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>" class="textfield" maxlength="15" alt="Alternative Telephone Number"/></div>
   <p class="note">Optional</p>
</li>
<%--<li class="emailAddressError">
   <label for="emailAddress">Email address</label>
   <input type="email" id="emailAddress" name="emailAddress" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>" class="textfield" gfv_match="match" gfv_required="required" maxlength="100" alt="Email Address"/>
</li>
<li class="confirmEmailAddressError">
   <label for="emailAddress1">Confirm email address</label>
   <input type="email" id="confirmEmailAddress" name="confirmEmailAddress" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>" class="textfield" gfv_match="match" gfv_required="required" maxlength="100" alt="Confirm Email Address"/>
</li>--%>

</ul>
</li>