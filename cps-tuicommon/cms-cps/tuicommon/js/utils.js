/**
 * A UTILITY Library contains following util functionality
 */

/*--------------------------------String Utils ------------------------------------------------------*/
/**
 * Utility functions defined for manipulating String
 *
 * Contains following functions
 * isNotEmpty:
 * isEmpty:
 * trim:
 *
 */
var StringUtils = {

   /** */
   EMPTY : "",

   /**
   * returns true if the value passed is not empty/null/undefined
   *
   */
   isNotEmpty : function(value)
   {
      return(value!=undefined && value!=null && this.trim(value).length>0 )
   },

   /**
   * returns true if the value passed is  empty/null/undefined
   *
   */
   isEmpty : function(value)
   {
      return(value==undefined || value==null || this.trim(value).length==0 )
   },

   /**
   * TODO:
   */
   isBlank : function(str)
   {
      return(str==undefined || str==null || this.trim(str).length==0 )
   },

   /**
   * TODO:
   */
   isNotBlank : function(str)
   {
      return(str!=undefined && str!=null && this.trim(str).length>0 )
   },

   /**
   * Trims spaces from both sides of the string
   *
   */
   trim : function(str)
   {
      return str.replace( /^\s+|\s+$/g, "" );
   },

   /**
   * returns a new String after changing first char of the input string to lower case.
   */
   firstCharToLowerCase: function(value)
   {
      if(this.isNotEmpty(value))
	  {
	     value = this.trim(value);
	     firstChar = value.substring(0,1).toLowerCase();
	     otherChars = value.substring(1);
	     value = firstChar+otherChars;
	  }
	  return value;
   },

   /**
   * TODO: Comment
   */
   substringBefore : function(str, separator)
   {
      if(this.isEmpty(str) || separator == null || separator.length == 0)
      {
         return str;
      }
      var pos = str.indexOf(separator);
      if (pos == -1)
      {
         return str;
      }
      return str.substring(0, pos);
   },

   substringAfter : function(str, separator)
   {
      if(this.isEmpty(str) || separator == null || separator.length == 0)
      {
         return str;
      }
      var pos = str.indexOf(separator);
      if (pos == -1)
      {
         return str;
      }
      return str.substring(pos+1, str.length);
   },

   /**Strips the characters passed through the function. */
   stripChars:function(s, bag)
   {
      var i;
      var returnString = "";
      // Search through string's characters one by one.
      // If character is not in bag, append to returnString.
      for (i = 0; i < s.length; i++)
      {
         // Check that current character isn't whitespace.
         var c = s.charAt(i);
         if (bag.indexOf(c) == -1) returnString += c;
      }
      return returnString;
   },

   /**
   * Trims space and Strips only first char from the string
   * @param value
   * @returns string after removing first char
   */
   stripFirstChar:function(value)
   {
      value = (StringUtils.trimSpaces(value)).substring(1);
	  return value;
   },

   /** Checks whether the two strings (Case Sensitive) are equal and returns a boolean. **/
   equals : function(obj1, obj2)
   {
      return (obj1 == obj2);
   },

   /**  Trim spaces from start and end of string   */
   trimSpaces:function(objval)
   {
      // Remove Space at start
      RESpace = /^\s*/i;
      objval = objval.replace(RESpace, '');
      //Remove Space at end
      RESpace = /\s*$/i;
      objval = objval.replace(RESpace, '');
      return objval;
   },

   /** Checks whether the two strings (Not Case Sensitive) are equal and returns a boolean. **/
   equalsIgnoresCase : function(obj1, obj2)
   {
      return (obj1.toLowerCase() == obj2.toLowerCase())
   },

   /** Remove all spaces in value **/
   removeAllSpaces : function(objval)
   {
      RESpace = /\s/ig;
      objval = objval.replace(RESpace, '');
      return objval
   },

   /** Sets cursor to the text end.*/
   SetCursorToTextEnd : function(textObj)
   {
      if (textObj != null && textObj.value.length > 0)
      {
         if (textObj.createTextRange)
         {
            var FieldRange = textObj.createTextRange();
            FieldRange.moveStart('character', textObj.value.length);
            FieldRange.collapse();
            FieldRange.select();
         }
      }
   },

   /** Clear the text in the field and set Focus to that field and show message */
   clearAndFocus: function(alertmessage,obj)
   {
      alert(alertmessage);
      obj.value='';
      obj.focus();
      return false;
   },

   /** Set Focus to field and show message */
   setFocus: function(alertmessage,obj)
   {
      alert(alertmessage);
      obj.focus();
      return;
   },

   clearField: function(obj)
   {
      if (obj.value != '') {
	     obj.value = '';
	     obj.select()
	     obj.onfocus = null;
	  }
   },

   /** If the element exists set value to it.
   * This function is applicable for <input> tags only.
   * @param id- id of the element
            value - value to be set to element
   */
   setValueForInput: function(id , value)
   {
      if(document.getElementById(id))
      {
         document.getElementById(id).value = value;
      }
   },


   disableField: function(id , value)
   {
      if(document.getElementById(id))
      {
         document.getElementById(id).disabled = value;
      }
   },

   getInputValue: function(id)
   {
      if(document.getElementById(id))
      {
         return(document.getElementById(id).value);
      }
   }
};
/*---------------------------*****END String Utils*****----------------------------------------------*/

/*--------------------------Math Utils--------------------------------------------------------*/
var MathUtils = {
   /**
   This function rounds off a decimal floating point number to the specified number of
   digits after the decimal point.
   */
   roundOff: function(amount,digits)
   {
	   // The precision is explicitly mentioned so as to ensure the multiplication yeilds a predictive result.
	  var scaledAmount = (amount*Math.pow(10,digits)).toPrecision(StringUtils.stripChars((amount).toString(),".").length);
	  var roundedAmount = Math.round(scaledAmount);
	  var unscaledAmount = roundedAmount/100;
	  return parseFloat(unscaledAmount).toFixed(digits);
   },

   numberFormat: function(value, precision)
   {
      if(isNaN(1*value))
      {
         return value;
      }
	  return parseFloat(1*value, 10).toFixed(precision);
   },

   /** This function gives the length of a decimal number without counting the decimal point "."
   Example: lengthOf(12.45)= 4.
   */
   lengthOf: function(value)
   {
      return stripChars((value).toString(),".").length;
   }
};

/*-------------------------******End Math Utils-----------------------------------------*/

/**------------------------ObjectUtils-----------------------------------------------------*/
ObjectUtils =
{
  isEmpty:function(obj)
   {
      for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
    return true;
   },

    isNotEmpty:function(obj)
   {
     return(!this.isEmpty(obj));

   }
};

/*-----------------------------------------------------------------------------------------------------------------*/
/**
 ** Creates a map object for holding the objects
 ** in JS and as well the objects from JSP.
**/
function Map()
{
   var object = new Array();
   this.put = function(key, value)
   {
      object[key] = value;
   };

   this.get = function(key)
   {
      var item = object[key];
      if(item==undefined)
      {
         return null;
      }
      return item;
   };

    this.getAllValuesList = function()
   {
      return object
   };


}