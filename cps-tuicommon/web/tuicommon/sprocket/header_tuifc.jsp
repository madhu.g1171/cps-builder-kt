<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:formatNumber
	value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
	var="totalCostingLine" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="zeroDepositDDExist" value="false" />
	<c:forEach var="depositComponent"
					items="${bookingComponent.depositComponents}"
					varStatus="count">
					 <c:set var="zeroDepositDDExist" value="${depositComponent.depositAmount.amount gt 0 ? false : true}"
			scope="page" /> 
		</c:forEach>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<fmt:formatNumber
	value="${bookingComponent.lockYourPriceSummary.outstandingAmount.amount}"
	var="outStandingAmount" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />
<c:set var="outStandingcost" value="${fn:split(outStandingAmount, '.')}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
	<c:set var="optionsUrl"
		value="${bookingComponent.breadCrumbTrail['HOTEL']}" />
	<c:set var="searchUrl"
		value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}" />
	<c:set var="passengersUrl"
		value="${bookingComponent.breadCrumbTrail['PASSENGERS']}" />
</c:if>


<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="discountFlag" value="false" />
<c:set var="priceBreakDown"
	value="${bookingComponent.pricingDetails['priceBreakDown']}" />

<c:if test="${not empty  bookingComponent.depositComponents}">
	<c:set var="depositDetails"
		value="${bookingComponent.depositComponents }"></c:set>
	<c:set var="CONST_LOW_DEPOSIT">lowDeposit</c:set>
	<c:set var="CONST_DEPOSIT">deposit</c:set>
</c:if>

<c:forEach var="priceComponent" items="${priceBreakDown}"
	varStatus="count">
	<c:if test="${not empty priceComponent.onlineDiscountData &&  not empty bookingComponent.flightSummary}">
		<c:set var="discountFlag" value="true" />
	</c:if>
</c:forEach>
<c:set var="depositComponentval" value="false" />
<c:forEach var="eachDepositComponentAcc" items="${depositDetails}"
	varStatus="count">
	<c:if test="${not empty eachDepositComponentAcc.depositDataPP}">
		<c:set var="depositComponentval" value="true" />
	</c:if>
</c:forEach>


<!-- Adding to fix ABA-4820 to hide the discount amount in header -->

<c:forEach var="priceComponent" items="${priceBreakDown}" varStatus="count">
	<c:if
		test="${priceComponent.itemDescription == 'Online Discount' &&  priceComponent.amount.amount > 0 }">
		<c:set var="discountFlag" value="false" />
	</c:if>
</c:forEach>

<c:set var="perPersonPrice" value="${bookingComponent.pricingDetails['PER_PERSON_PRICE']}"/>
<c:if test="${not empty perPersonPrice}">
	<c:forEach var="price" items="${perPersonPrice}">

		<fmt:formatNumber value="${price.amount.amount}" var="perPersonPriceCostingLine" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####" />
		<c:set var="perpersonpricecost" value="${fn:split(perPersonPriceCostingLine, '.')}" />
	</c:forEach>
</c:if>
<c:set var="outstandingPerPersonPrice" value="${bookingComponent.lockYourPriceSummary.outstandingAmountPP.amount}"/>
<c:if test="${not empty outstandingPerPersonPrice}">

<fmt:formatNumber value="${outstandingPerPersonPrice}" var="mmbPerPersonPriceCostingLine" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####" />
		<c:set var="outstandingPerpersonpricecost" value="${fn:split(mmbPerPersonPriceCostingLine, '.')}" />

</c:if>
<fmt:parseNumber var="fcpCount" integerOnly="true" type="number" value="${bookingComponent.nonPaymentData['freeChildPlaceCount']}" />
<div id="book-flow-header">
	<div class="content-width">
		<div class="logo thomson firstchoiceX falconX">
			<!--  <a href="${bookingComponent.clientURLLinks.homePageURL}"></a>-->
		</div>
		<!--<div class="summary-panel-trigger b abs bg-tui-sand black">
						<i class="caret fly-out"></i>&pound;<c:out value="${totalcost[0]}."/><span class="pennys"><c:out value="${totalcost[1]}"/></span>

					</div>-->

	</div>

</div>
<c:choose>
			<c:when test="${bookingComponent.nonPaymentData['isResponsiveHubAndSpoke'] == 'Y' or bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow == true}">
			<c:set var="numOfSteps" value="${fn:length(bookingComponent.breadCrumbTrail)}" />
			<c:set var="stepWidth" value="${100 / numOfSteps}" />
			<c:set var="stepWidth" value="${fn:substringBefore(stepWidth, '.')}" />

<c:if test="${ bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne true}">

			<div class="span no-bottom-margin header-spacing breadcrumb">
				<div class="content-width">
					<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true"
						data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
						<ul id="indicators-sprite-${numOfSteps}"
							class="breadcrumbs-spacing">
							<c:forEach var="indicator"
								items="${bookingComponent.breadCrumbTrail}" varStatus="counter">
								<c:choose>

									<c:when test="${!counter.last}">
										<c:set var="indicatorHref" value="${indicator.value}" />

									</c:when>
									<c:otherwise>
										<c:set var="indicatorHref" value="#" />

									</c:otherwise>
								</c:choose>

								<li
									class="indicator  uc  sprite-${numOfSteps} ${counter.last ? '' : 'completed'} ${indicator.key=='PAYMENT'? 'active':''}">

									<i class="arrow in ">${counter.count}</i> <c:if
										test="${!counter.last}">
										<a <c:if test="${indicator.key ne 'PAYMENT'}">href="<c:out value='${indicatorHref}'/>"</c:if> class="ensLinkTrack" data-componentId="bookflowStepIndicator_comp">
											<span class="">${indicator.key}</span>
										</a>
									</c:if> <c:if test="${counter.last}">
										<a class="last"><span class="">${indicator.key}</span></a>
									</c:if> <i class="arrow out "></i>
								</li>

							</c:forEach>
						</ul>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
</c:if>


				<div class="component summary total-price">
					<div class="book-navigation content-width">
					<c:choose>
					<c:when test="${bookingComponent.lockYourPriceSummary.selected and bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne true}">

							<div class="price-panel lockThePrice fr">
							<div class="price-block dis-inblock vertal-m">
								<div class="uppercase"> Lock this price for ${bookingComponent.lockYourPriceSummary.cancellationPeriodInHours} hours </div>
								<div class="per-person-price">
									<c:if test="${not empty bookingComponent.lockYourPriceSummary.totalPrice}">

										<div class="per-person-price"}>
										<span>
											<span class="lockPricePart1" >&pound;</span>
											<span class="lockPricePart2">${bookingComponent.lockYourPriceSummary.totalPrice}</span>
										</span>
										<span class="lockPricePP">(&pound;${bookingComponent.lockYourPriceSummary.ppPrice}pp)</span>
										</div>
									</c:if>
								</div>
								<div class="total-price"><span class="lockPriceTotal"> Total holiday price<span> &pound;<fmt:formatNumber  value="${totalcost[0]}" />.<c:out value="${totalcost[1]}"/></span></span></div>
							</div>
							</div>

					</c:when>
					<c:when test="${bookingComponent.lockYourPriceSummary.selected and bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow eq true}">

						<div class="price-panel fr">
							<div class="price-block dis-inblock vertal-m lypmmb">
								<div class="per-person-price">
									<span class="currency">&pound;</span>
									<c:if test="${not empty perPersonPrice}">
										<span>
											<span class="part1"><fmt:formatNumber  value="${outstandingPerpersonpricecost[0]}" /></span>
											<span class="part2">.<c:out value="${outstandingPerpersonpricecost[1]}"/></span>pp
										</span>
									</c:if>
								</div>
								<div class="total-price">Outstanding Amount<span> &pound;<fmt:formatNumber  value="${outStandingcost[0]}" />.<c:out value="${outStandingcost[1]}"/></span></div>
							</div>

							<c:if test="${not empty priceBreakDown}">
								<div class="dis-block discount-block">
									<ul>
										<li class="dis-inblock" >
											 <c:forEach var="priceComponent" items="${priceBreakDown}" varStatus="count">
												<c:choose>
												   <c:when test="${not empty priceComponent.itemDescription and count.index==0}">
															Inc &pound;<fmt:formatNumber  value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##" /> discount
													</c:when>
												</c:choose>
											</c:forEach>
										</li>
										<c:if test="${not empty depositDetails}">
											<li>
											<c:choose>
											    <c:when test="${zeroDepositDDExist}">
													Book now with no deposit
											    </c:when>
												<c:otherwise>
												<c:forEach var="eachDepositComponent" items="${depositDetails}" varStatus="deposits">
													<c:choose>
														<c:when test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
															<c:out value="${eachDepositComponent.depositDataPP}" />
														</c:when>
														<c:otherwise>
															<c:if test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
																 <c:out value="${eachDepositComponent.depositDataPP}"/>
													        </c:if>
														</c:otherwise>
													</c:choose>   
												</c:forEach>
												</c:otherwise>
											</c:choose>
										</li>
										</c:if>
									</ul>
								</div>
							</c:if>

							<c:if test="${fcpCount gt 0}">
								<div class="fcp-alert"><p>${fcpCount} X FREE CHILD PLACE INCLUDED</p></div>
							</c:if>
						</div>

					</c:when>
					<c:otherwise>
						<div class="price-panel fr">
							<div class="price-block dis-inblock vertal-m">
								<div class="per-person-price">
									<span class="currency">&pound;</span>
									<c:if test="${not empty perPersonPrice}">
										<span>
											<span class="part1"><fmt:formatNumber  value="${perpersonpricecost[0]}" /></span>
											<span class="part2">.<c:out value="${perpersonpricecost[1]}"/></span>pp
										</span>
									</c:if>
								</div>
								<div class="total-price">Total price<span> &pound;<fmt:formatNumber  value="${totalcost[0]}" />.<c:out value="${totalcost[1]}"/></span></div>
							</div>

							<c:if test="${not empty priceBreakDown or discountFlag}">
								<div class="dis-block discount-block">
									<ul>
										<li class="dis-inblock" >
											 <c:forEach var="priceComponent" items="${priceBreakDown}" varStatus="count">
												<c:choose>
												   <c:when test="${discountFlag and not empty priceComponent.itemDescription and count.index==0}">
															Inc &pound;<fmt:formatNumber  value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##" /> discount
													</c:when>
												</c:choose>
											</c:forEach>
										</li>
										<c:if test="${not empty depositDetails}">
											<li class="dis-inblock">
											<c:choose>
											    <c:when test="${zeroDepositDDExist}">
													Book now with no deposit
											    </c:when>
												<c:otherwise>
												<c:forEach var="eachDepositComponent" items="${depositDetails}" varStatus="deposits">
													<c:choose>
														<c:when test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
															<c:out value="${eachDepositComponent.depositDataPP}" />
														</c:when>
														<c:otherwise>
															<c:if test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
																 <c:out value="${eachDepositComponent.depositDataPP}"/>
													        </c:if>
														</c:otherwise>
													</c:choose>   
												</c:forEach>
												</c:otherwise>
											</c:choose>
											</li>
										</c:if>
									</ul>
								</div>
							</c:if>

							<c:if test="${fcpCount gt 0}">
								<div class="fcp-alert"><p>${fcpCount} X FREE CHILD PLACE INCLUDED</p></div>
							</c:if>
						</div>
					</c:otherwise>
					</c:choose>
					</div>
				</div>
			</c:when>
			<c:otherwise>
			<div id="book-flow-progress">
				<div class="content-width">
					<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true"
						data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>

						<ul class="c">
							<c:choose>
								<c:when test="${multiCentre eq true}">
									<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span
											class="rel b">1</span> HOLIDAY</a></li>
									<li class="back"><a href="<c:out value='${searchUrl}'/>"><span
											class="rel b">2</span> SUMMARY</a></li>
								</c:when>
								<c:otherwise>
									<li class="back"><a href="javascript:void(0)" /><span
										class="rel b">1</span> HOTEL</a></li>
									<li class="back"><a href="javascript:void(0)"><span
											class="rel b">2</span> FLIGHTS</a></li>
									<li class="back"><a href="javascript:void(0)"><span
											class="rel b">3</span> ROOM & BOARD</a></li>
									<li class="back"><a href="javascript:void(0)"><span
											class="rel b">4</span> EXTRAS</a></li>

								</c:otherwise>
							</c:choose>

							<li class="active"><span class="rel b">5</span> BOOK</li>
						</ul>

					</div>
				</div>
			</div>
			<div class="main-price-summary <c:if test='${totalcost[0] <= 0}'> hide </c:if>">
				<div class="content-width">
					<div class="inner-wrap">
						<div
							class="left-content <c:if test='${ not discountFlag and not depositComponentval}'> hide </c:if>">
							<div class="text">
								<span class="promotional-discount"> 
									<c:if
										test="${not empty priceBreakDown and discountFlag}">
										<c:forEach var="priceComponent" items="${priceBreakDown}"
											varStatus="count">
											<c:choose>
												<c:when
													test="${not empty priceComponent.itemDescription and not empty priceComponent.onlineDiscountData and count.index==0}">
																	Inc &pound;<fmt:formatNumber
														value="${priceComponent.amount.amount}" type="number"
														maxFractionDigits="2" minFractionDigits="2"
														pattern="#####.##" /> discount</br>
												</c:when>
											</c:choose>
										</c:forEach>
									</c:if>
								</span> 
									<span class="promotional-discount"> 
										<c:choose>
											<c:when test="${zeroDepositDDExist}">
												Book now with no deposit
											</c:when>
											<c:otherwise>
												<c:if
												   test="${not empty depositDetails}">
													   <c:forEach var="eachDepositComponent" items="${depositDetails}"
																  varStatus="deposits">
																	<c:choose>
																	   <c:when
																		   test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP }">
	
																		<c:out value="${eachDepositComponent.depositDataPP}"
																		 escapeXml="false" />
																		</c:when>
																		<c:when
																		   test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP}">
	
																		   <c:out value="${eachDepositComponent.depositDataPP}"
																			escapeXml="false" />
																		</c:when>
																	</c:choose>
														</c:forEach>
												</c:if>
											</c:otherwise>
								</c:choose>
								</span>
							</div>
						</div>

						<div
							class="right-content <c:if test='${ not discountFlag and not depositComponentval}'>no-discount</c:if>">
							<div class="price">
								<span class="currency">&pound;</span> <span class="num"><span
									class="part1"><fmt:formatNumber value="${totalcost[0]}" />.</span><span
									class="part2"><c:out value="${totalcost[1]}" /></span></span>
							</div>
							<span class="per-person-price"><c:if
									test="${not empty bookingComponent.pricingDetails && not empty flightSummary}">
									<c:set var="perPersonPrice"
										value="${bookingComponent.pricingDetails['PER_PERSON_PRICE']}" />
									<c:if test="${not empty perPersonPrice}">
										<c:forEach var="price" items="${perPersonPrice}">
											<c:if test="${price.amount.amount > 0}">
																		&pound;<fmt:formatNumber value="${price.amount.amount}"
													type="number" maxFractionDigits="2" minFractionDigits="2"
													pattern="#####.##" /> per person
													</c:if>
										</c:forEach>
									</c:if>


								</c:if></span>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
</c:choose>