<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
<c:set var="optionsUrl" value="${bookingComponent.breadCrumbTrail['FLIGHT OPTIONS']}"/>
<c:set var="searchUrl" value="${bookingComponent.breadCrumbTrail['SEARCH_RESULT']}"/>
<c:set var="passengersUrl" value="${bookingComponent.breadCrumbTrail['BOOK']}"/>
<c:set var="extrasUrl" value="${bookingComponent.breadCrumbTrail['EXTRAS']}"/>
</c:if>


<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="discountFlag" value="false" />
<c:set var="priceBreakDown"
	value="${bookingComponent.pricingDetails['priceBreakDown']}" />
	

<c:choose>
   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
      <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="false" var="tuiLogo" />  
   </c:otherwise>
</c:choose>


<c:forEach var="priceComponent" items="${priceBreakDown}"
	varStatus="count">
	<c:if test="${not empty priceComponent.onlineDiscountData}">
		<c:set var="discountFlag" value="true" />
	</c:if>
</c:forEach>

<c:set var="depositComponentval" value="false" />
<c:forEach var="eachDepositComponentAcc" items="${depositDetails}"
	varStatus="count">
	<c:if test="${not empty eachDepositComponentAcc.depositDataPP}">
		<c:set var="depositComponentval" value="true" />
	</c:if>
</c:forEach>

<c:set var="perPersonPrice" value="${bookingComponent.pricingDetails['PER_PERSON_PRICE']}"/>
<c:if test="${not empty perPersonPrice}">
	<c:forEach var="price" items="${perPersonPrice}">
<fmt:formatNumber value="${price.amount.amount}" var="perPersonPriceCostingLine" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####" />
		<c:set var="perpersonpricecost" value="${fn:split(perPersonPriceCostingLine, '.')}" />
	</c:forEach>
</c:if>
<fmt:parseNumber var="fcpCount" integerOnly="true" type="number" value="${bookingComponent.nonPaymentData['freeChildPlaceCount']}" />

<div id="book-flow-header">
	<div class="content-width">
		<div class="logo<c:if test="${tuiLogo}">TUI</c:if> thomson firstchoiceX falconX">
			<a href="${bookingComponent.clientURLLinks.homePageURL}"></a>
		</div>
		<c:if test="${!tuiLogo}">
		<img alt="World Of TUI" class="nomobile  nominitablet "
			id="header-wtui"
			src="/cms-cps/tuithfo/images/WOP-Horizontal.png">
		</c:if>
		<!--<div class="summary-panel-trigger b abs bg-tui-sand black">
						<i class="caret fly-out"></i>&pound;<c:out value="${totalcost[0]}."/><span class="pennys"><c:out value="${totalcost[1]}"/></span>

					</div>-->

	</div>

</div>


			<div id="book-flow-progress">
				<div class="content-width">
					<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true" data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
		
						<ul class="c">
							<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">1</span> SEATS & LUGGAGE</a></li>
							<li class="back"><a href="<c:out value='${extrasUrl}'/>"><span class="rel b">2</span> EXTRAS</a></li>
							<li class="back"><a href="<c:out value='${passengersUrl}'/>"><span class="rel b">3</span> PASSENGER DETAILS</a></li>
							<li class="active"><span class="rel b">4</span> PAYMENT</a></li>
							<li class="front"><span class="rel b">5</span> CONFIRMATION</li>
						</ul>
					</div>
				</div>
			</div>			<div class="component summary total-price">
					<div class="book-navigation content-width">
						<div class="price-panel fr">
							<div class="price-block dis-inblock vertal-m">
								<div class="per-person-price">
									<span class="currency">&pound;</span>
									<c:if test="${not empty perPersonPrice}">
										<span>
											<span class="part1"><fmt:formatNumber  value="${perpersonpricecost[0]}" /></span>
											<span class="part2">.<c:out value="${perpersonpricecost[1]}"/></span>pp
										</span>
									</c:if>
								</div>
								<div class="total-price">Total price<span> &pound;<fmt:formatNumber  value="${totalcost[0]}" />.<c:out value="${totalcost[1]}"/></span></div>
							</div>

							<!--<c:if test="${not empty priceBreakDown or discountFlag}">
								<div class="dis-block discount-block">
									<ul>
										<li class="dis-inblock" >
											 <c:forEach var="priceComponent" items="${priceBreakDown}" varStatus="count">
												<c:choose>
												   <c:when test="${not empty priceComponent.itemDescription and count.index==0}">
															Includes &pound;<fmt:formatNumber  value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##" /> discount
													</c:when>
												</c:choose>
											</c:forEach>
										</li>
										<c:if test="${not empty depositDetails}">
											<li class="dis-inblock">
												<c:forEach var="eachDepositComponent" items="${depositDetails}" varStatus="deposits">

													<c:choose>
														<c:when test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
															<c:out value="${eachDepositComponent.depositDataPP}" />
														</c:when>

														<c:otherwise>
															<c:if test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
																 <c:out value="${eachDepositComponent.depositDataPP}"/>
															</c:if>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</li>
										</c:if>
									</ul>
								</div>
							</c:if>	-->						
						</div>
					</div>
				</div>