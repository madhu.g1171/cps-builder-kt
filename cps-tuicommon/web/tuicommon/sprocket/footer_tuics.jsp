<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.tuith" , "");
pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);
String firstchoiceHomepage = ConfReader.getConfEntry("firstchoice.homepage.url" , "");
pageContext.setAttribute("firstchoiceHomepage", firstchoiceHomepage, PageContext.REQUEST_SCOPE);
String tuiBrochure = ConfReader.getConfEntry("tui.brochure.url" , "");
pageContext.setAttribute("tuiBrochure", tuiBrochure, PageContext.REQUEST_SCOPE);
%>

<div id="footer">
	 <div class="wrapper ski">
		<div id="group" class="b" aria-label="group">
			<div class="content-width">
			<div class="container">
			<p>&copy; 2021 Crystal. Crystal Ski Holidays is a trading name of TUI UK Limited, a member of TUI Group.</p>
			<p>Registered Office: Wigmore House, Wigmore Lane, Luton, LU2 9TN. Registered in England No: 2830117 ATOL No: 2524. ABTA No: V5126.</p>
			<span class="logos">
			<a rel="noopener" href="//abta.com/go-travel/before-you-travel/find-a-member" id="logo-abta" title="ABTA - The Travel Association" data-di-id="#logo-abta">.</a>
			<a rel="noopener" href="//www.caa.co.uk/application.aspx?catid=490&amp;pagetype=65&amp;appid=2&amp;mode=detailnosummary&amp;atolnumber=2524" id="logo-atol" title="ATOL Protected" data-di-id="#logo-atol">.</a>
			</span>
			</div>
			</div>
		</div>
		<div aria-label="terms" class="terms">
			<div class="content-width contentList">
			<span class="title">More from Crystal</span>
			<p>
				<a rel="noopener" href="https://www.crystalski.co.uk/contact-us/">Contact us</a>
				<a rel="noopener" href="https://www.crystalski.co.uk/about-crystal/">About us</a>
				<a rel="noopener" href="https://www.crystalski.co.uk/our-policies/terms-and-conditions/">Terms and conditions</a>
				<a rel="noopener" href="https://www.crystalski.co.uk/our-policies/privacy-policy/">Privacy & cookies</a>
				<a rel="noopener" href="https://www.crystalski.co.uk/help/">Get help</a>
				<a rel="noopener" href="https://www.crystalski.co.uk/your-account/">Manage booking</a>      
				<a rel="noopener" href="https://www.crystalski.co.uk/the-crystal-snow-promise/">The Crystal snow promise</a>
				<a rel="noopener" href="https://www.crystalski.co.uk/book-with-confidence/">Book with confidence</a>
			</p>
			</div>
		</div>
		<div class="knowBefore" area-label="Travel Aware">
			<div class="content-width">
			<div class="display-flex-mobile">
				<div>
				<span class="travelAwareLogo" area-label="Travel Aware"></span>
				</div>
				<div class="travelAwareSection">
				<span class="title" aria-label="travel aware">Travel aware - staying safe and healthy abroad</span>
				<p>The Foreign, Commonwealth &amp; Development Office and the National Travel Health Network and Centre have up-to-date advice on staying safe and healthy abroad.</p>
          		<p>For the latest security and local laws, plus passport and visa information check <a rel="noopener" href="//www.gov.uk/travelaware" target="_blank" class="stay-safe" data-di-id="di-id-57a22b57-6602ed94">www.gov.uk/travelaware</a> and follow <a rel="noopener" href="https://twitter.com/FCDOtravelGovUK" target="_blank" class="stay-safe" data-di-id="di-id-57a22b57-548d2d44">@FCDOtravelGovUK</a> and <a rel="noopener" href="https://www.facebook.com/FCDOTravel/" target="_blank" class="stay-safe" data-di-id="di-id-f406c103-89709a3c">Facebook.com/FCDOTravel</a></p>
				<p>More information is available by checking <a class="travelAware" href="https://www.tui.co.uk/destinations/info/travel-aware" target="_blank" class="stay-safe" data-di-id="di-id-2c073b9e-1b753891">www.tui.co.uk/destinations/info/travel-aware</a></p>
				<p>Keep informed of current travel health news by visiting <a rel="noopener" href="//www.travelhealthpro.org.uk" target="_blank" class="stay-safe" data-di-id="di-id-f406c103-8f024bcc">www.travelhealthpro.org.uk</a></p>
				<p>The advice can change so check regularly for updates.</p>
				</div>
			</div>
			</div>
		</div>
		<div id="disclaimer">
			<div class="content-width disclaim">
			<p>All the flight-inclusive holidays on this website are financially protected by the ATOL scheme. When you pay you will be supplied with an ATOL Certificate. Please ask for it and check to ensure that everything you booked (flights, hotels and other services) is listed on it. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Some of the flights on this website are also financially protected by the ATOL scheme, but ATOL protection does not apply to all flights. This website will provide you with information on the protection that applies in the case of each flight before you make your booking. If you do not receive an ATOL Certificate then the booking will not be ATOL protected. Please see our booking conditions for information, or for more information about financial protection and the ATOL Certificate go to: <a rel="noopener" href="https://www.caa.co.uk/">www.caa.co.uk</a>. ATOL protection does not apply to the other holiday and travel services listed on this website.</p>
			</div>
		</div>
		</div>		
</div>


