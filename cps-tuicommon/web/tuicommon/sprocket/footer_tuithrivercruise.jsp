<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.tuith" , "");
pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);
String firstchoiceHomepage = ConfReader.getConfEntry("firstchoice.homepage.url" , "");
pageContext.setAttribute("firstchoiceHomepage", firstchoiceHomepage, PageContext.REQUEST_SCOPE);
String tuiBrochure = ConfReader.getConfEntry("tui.brochure.url" , "");
pageContext.setAttribute("tuiBrochure", tuiBrochure, PageContext.REQUEST_SCOPE);
%>

<div id="footer">
				<div id="call-us" class="hide">
					<div class="content-width">
						<i class="caret call white b"></i><h2 class="d-blue">BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203 636 1931</a></span></h2>
					</div>
				</div>
				<div id="search" class="b">
					<div class="content-width">
						<a href="#page" id="backtotop"><span>To top </span><i class="caret back-to-top white"></i></a>
					</div>
				</div>
				<div id="group" class="b thomson">
					<div class="content-width">
						<div class="copy">
							<!-- <span id="world-of-tui"><img alt="World of TUI" src="/cms-cps/tuithcruise/images/logo/wtui.png"></span> -->
							<p>Just so you know, Thomson is now called TUI and we're part of TUI Group - the world's leading travel company. All of our holidays are designed to help you 'Discover your smile'.</p>
						</div>
						<div class="logos">
							<a href="http://abta.com/go-travel/before-you-travel/find-a-member" id="logo-abta" title="ABTA - The Travel Association" target="_blank"></a>
							<a href="http://www.caa.co.uk/application.aspx?catid=490&pagetype=65&appid=2&mode=detailnosummary&atolnumber=2524" id="logo-atol" title="ATOL Protected"></a>
						</div>
					</div>
				</div>
				<div id="seo" class="nominitablet nomobile">
					<div class="accordion" data-accordion>
						<div class="item">
							<div class="content-width">
								<div class="trigger">
								<ul>

									<li  >Holiday Types</li>
									<li  >Popular Destinations</li>
									<li  >Mid/Long haul</li>
									<li  >Short haul</li>
									<li  >Flights To</li>
									<li  >Cruise</li>

								</ul>
								</div>
								<div class="content">
								  <div class="c">
									<div id="holidaytypes" class="c">
										<p>Holiday Types</p>
										<div>
											<a target="_blank" href="http://www.thomson.co.uk/destinations/deals" enslinktrackattached="true">Cheap Holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/luxury" enslinktrackattached="true">Luxury Holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/destinations/deals/summer-2017-deals" enslinktrackattached="true">Sunshine Holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/family" enslinktrackattached="true">Family Holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/destinations/deals/winter-sun-holidays" enslinktrackattached="true">Winter Sun Holidays</a>

											<a target="_blank" href="http://www.thomson.co.uk/holidays/all-inclusive" enslinktrackattached="true">All inclusive holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/" enslinktrackattached="true">Package Holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/beach" enslinktrackattached="true">Beach holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/last-minute" enslinktrackattached="true">Late holiday deals</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/city-breaks" enslinktrackattached="true">City Breaks</a>

											<a target="_blank" href="http://www.thomson.co.uk/destinations/deals/summer-2017-deals" enslinktrackattached="true">Summer holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/last-minute" enslinktrackattached="true">Last minute Holidays</a>

											<a target="_blank" href="http://www.thomson.co.uk/holidays/villas" enslinktrackattached="true">Villa Holidays</a>
											<a target="_blank" href="http://www.thomson.co.uk/destinations/deals/short-breaks" enslinktrackattached="true">Short Breaks</a>
											<a target="_blank" href="http://www.thomson.co.uk/holidays/multi-centre" enslinktrackattached="true">Multi-Centre Holidays</a>
										</div>
									</div>
									<div id="popDestinations" class="c">
										<p>Popular Destinations</p>
										<div>
											<a target="_blank"  class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/portugal/algarve/holidays-algarve.html" enslinktrackattached="true">Algarve holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/costa-blanca/benidorm/holidays-benidorm.html" enslinktrackattached="true">Benidorm holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/canary-islands/holidays-canary-islands.html" enslinktrackattached="true">Canary Islands holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/greece/crete/holidays-crete.html" enslinktrackattached="true">Crete holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/cyprus/holidays-cyprus.html" enslinktrackattached="true">Cyprus holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/gran-canaria/holidays-gran-canaria.html" enslinktrackattached="true">Gran Canaria holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/ibiza/holidays-ibiza.html" enslinktrackattached="true">Ibiza holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/lanzarote/holidays-lanzarote.html" enslinktrackattached="true">Lanzarote holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/american-cities/las-vegas/holidays-las-vegas.html" enslinktrackattached="true">Las Vegas Holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/majorca/holidays-majorca.html" enslinktrackattached="true">Majorca holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/menorca/holidays-menorca.html" enslinktrackattached="true">Menorca holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/caribbean/mexico/holidays-mexico.html" enslinktrackattached="true">Mexico Holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/florida/miami/holidays-miami.html" enslinktrackattached="true">Miami Holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/american-cities/new-york/holidays-new-york.html" enslinktrackattached="true">New York Holidays</a>
										</div>
									</div>
									<div id="longhaul" class="c">
									  <p>Mid/Long haul</p>
									  <div>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/caribbean/aruba-island/holidays-aruba-island.html" enslinktrackattached="true">Aruba holidays</a>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/asia/thailand/thailand/bangkok/holidays-bangkok.html" enslinktrackattached="true">Bangkok Holidays</a>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/caribbean/barbados/holidays-barbados.html" enslinktrackattached="true">Barbados holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/asia/vietnam/holidays-vietnam.html" enslinktrackattached="true">Vietnam Holidays</a>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/africa/cape-verde/holidays-cape-verde.html" enslinktrackattached="true">Cape Verde holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/caribbean/holidays-caribbean.html" enslinktrackattached="true">Caribbean Holidays</a>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/the-americas/costa-rica/holidays-costa-rica.html" enslinktrackattached="true">Costa Rica Holidays</a>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/caribbean/cuba/holidays-cuba.html" enslinktrackattached="true">Cuba Holidays</a>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/middle-east/united-arab-emirates/dubai-and-emirates/holidays-dubai-and-emirates.html" enslinktrackattached="true">Dubai Holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/africa/egypt/holidays-egypt.html" enslinktrackattached="true">Egypt holidays</a>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/florida/holidays-florida.html" enslinktrackattached="true">Florida holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/asia/hong-kong/holidays-hong-kong.html" enslinktrackattached="true">Hong Kong Holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/asia/malaysia/malaysia/kuala-lumpur/holidays-kuala-lumpur.html" enslinktrackattached="true">Kuala Lumpur Holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/caribbean/jamaica/holidays-jamaica.html" enslinktrackattached="true">Jamaica Holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/indian-ocean/mauritius/holidays-mauritius.html" enslinktrackattached="true">Mauritius holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/asia/singapore/holidays-singapore.html" enslinktrackattached="true">Singapore Holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/indian-ocean/sri-lanka/holidays-sri-lanka.html" enslinktrackattached="true">Sri Lanka Holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/asia/thailand/holidays-thailand.html" enslinktrackattached="true">Thailand holidays</a>
											   <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/tenerife/holidays-tenerife.html" enslinktrackattached="true">Tenerife holidays</a>
									   </div>
									</div>
									<div id="shorthaul" class="c">
									  <p>Short Haul</p>
									  <div>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/costa-blanca/benidorm/holidays-benidorm.html" enslinktrackattached="true">Benidorm Holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/portugal/holidays-portugal.html" enslinktrackattached="true">Portugal holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/spain/holidays-spain.html" enslinktrackattached="true">Spain holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/turkey/holidays-turkey.html" enslinktrackattached="true">Turkey Holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/greece/zante/holidays-zante.html" enslinktrackattached="true">Zante holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/africa/morocco/holidays-morocco.html" enslinktrackattached="true">Morocco holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/croatia/holidays-croatia.html" enslinktrackattached="true">Croatia holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/greece/holidays-greece.html" enslinktrackattached="true">Greece holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/malta/holidays-malta.html" enslinktrackattached="true">Malta holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/bulgaria/holidays-bulgaria.html" enslinktrackattached="true">Bulgaria holidays</a>

											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/italy/holidays-italy.html" enslinktrackattached="true">Italy holidays</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/europe/finland/lapland/holidays-lapland.html" enslinktrackattached="true">Lapland holidays</a>

									  </div>
									</div>
									<div id="flightsTo" class="c">
									  <p>Flights To</p>
									  <div>
											 <a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/deals" enslinktrackattached="true">Cheap Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/spain/canary-islands/tenerife-flights" enslinktrackattached="true">Tenerife Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/jamaica/montego-bay-airport" enslinktrackattached="true">Montego Bay Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/mauritius-flights" enslinktrackattached="true">Mauritius Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/thailand-flights" enslinktrackattached="true">Thailand Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/spain/valencia/alicante-airport" enslinktrackattached="true">Alicante Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/spain/canary-islands/lanzarote-flights" enslinktrackattached="true">Lanzarote Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/spain/balearic-islands/ibiza-flights" enslinktrackattached="true">Ibiza Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/united-states-of-america/florida-flights" enslinktrackattached="true">Florida Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/united-states-of-america/florida/orlando-flights" enslinktrackattached="true">Orlando Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/spain-flights" enslinktrackattached="true">Spain Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/cyprus-flights" enslinktrackattached="true">Cyprus Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/malta-flights" enslinktrackattached="true">Malta Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/portugal-flights" enslinktrackattached="true">Portugal Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/turkey-flights" enslinktrackattached="true">Turkey Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/india/goa-flights" enslinktrackattached="true">Goa Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/mexico/mexico-caribbean-coast/cancun-airport" enslinktrackattached="true">Cancun Flights</a>
											<a target="_blank" data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" href="http://www.thomson.co.uk/flight/barbados-flights" enslinktrackattached="true">Barbados Flights</a>
									  </div>
									</div>

									<div id="cruseDestination" class="c">
										<p>Cruise</p>
										<div>
											<a target="_blank" href="http://www.thomson.co.uk/cruise/central-america/puerto-limon-port/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Puerto Limon</a>
											<a target="_blank"  href="http://www.thomson.co.uk/cruise/caribbean-cruises/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Caribbean Cruises</a>
											<a target="_blank"  href="http://www.thomson.co.uk/cruise/fjords-iceland-and-arctic-cruises/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Fjords Cruise</a>
										   <a target="_blank" href="http://www.thomson.co.uk/cruise/western-mediterranean-cruises/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Italian Cruises</a>
										   <a target="_blank" href="http://www.thomson.co.uk/cruise/cruise-and-stay/croatia/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Croatia</a>
										   <a target="_blank" href="http://www.thomson.co.uk/cruise/western-mediterranean-cruises/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Mediterranean Cruises</a>

										   <a target="_blank" href="http://www.thomson.co.uk/cruise/canary-islands-and-atlantic-cruises/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Canary Islands and Atlantic</a>
										   <a target="_blank" href="http://www.thomson.co.uk/cruise/central-america-cruises/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Central America</a>
										    <a target="_blank" href="http://www.thomson.co.uk/cruise/cruise-from-the-uk/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Cruises from the UK</a>

										   <a target="_blank" href="http://www.thomson.co.uk/cruise/eastern-mediterranean/dubrovnik-port/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Dubrovnik</a>
										   <a target="_blank" href="http://www.thomson.co.uk/cruise/western-mediterranean/barcelona-port" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Barcelona</a>

										   <a target="_blank" href="http://www.thomson.co.uk/cruise/northern-europe-and-uk/southampton-port/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Cruises from Southampton</a>

										   <a target="_blank" href="http://www.thomson.co.uk/destinations/info/all-inclusive-cruises" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">All Inclusive</a>
										   <a target="_blank" href="http://www.thomson.co.uk/destinations/info/family-cruises" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Family Cruises</a>
										   <a target="_blank" href="http://www.thomson.co.uk/destinations/info/last-minute-cruises" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Last Minute Cruises</a>
										   <a target="_blank" href="http://www.thomson.co.uk/cruise/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Cruises</a>
										   <a target="_blank" href="http://www.thomson.co.uk/destinations/info/summer-2017-cruises" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Cruise Deals</a>
										   <a target="_blank" href="http://www.thomson.co.uk/cruise/" class="ensLinkTrack" data-componentid="R_WF_COM_200-3">Cruise Holidays</a>

										</div>
									</div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="terms">

					<div class="content-width">
					<p class="title"><a href=" https://www.tui.co.uk/destinations/info/about-us">More from TUI </a></p>
						<p>
							<a target="_blank" href="https://www.tui.co.uk/destinations/info/my-tui-app">About TUI</a>
							<a target="_blank" href="https://www.tui.co.uk/destinations/info/my-tui-app">MyTUI app</a>
							<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies.html">Cookies policy</a>

							<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy Policy</a>
							<a target="_blank"  href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms &amp; conditions</a>
						<c:choose>
								<c:when test="${applyCreditCardSurcharge eq 'true'}">
							<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Credit card fees</a>
</c:when>
<c:otherwise><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Ways to Pay</a>
</c:otherwise>
</c:choose>
							<a target="_blank" href="http://communicationcentre.thomson.co.uk/">Media Centre</a>
							<a target="_blank" href="http://www.tuijobsuk.co.uk/">Travel Jobs</a>
							<a target="_blank" href="http://www.thomson.co.uk/affiliates.html">Affiliates</a>

							<a target="_blank" href=" https://blog.tui.co.uk/">TUI Blog</a>
							<a target="_blank" href="https://play.google.com/store/apps/details?id=com.thomson.mythomson">Google Play Store</a>
							<a target="_blank" href="https://appsto.re/gb/NubyM.i">App Store for Ios</a>


							<a target="_blank"  href="http://www.tuigroup.com/en-en"><jsp:useBean id='CurrentDate12' class='java.util.Date'/>
         	<fmt:formatDate var='currentYear' value='${CurrentDate12}' pattern='yyyy'/>
			&copy; <c:out value="${currentYear}" />  TUI Group</a>
						<a target="_blank"  href="${firstchoiceHomepage}">First Choice</a>
						<a target="_blank"  href="${tuiBrochure}">Holiday Brochures
						</a>
						</p>
					</div>
				</div>
				<%= staySafeAbroad %>
			</div>
					<div id="disclaimer">
				<div class="content-width disclaim two-columns">

					<p>Some of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme.  But ATOL protection does not apply to all holiday and travel services listed on this website. This website will provide you with information on the protection that applies in the case of each holiday and travel service offered before you make your booking.  If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information, or for more information about financial protection and the ATOL Certificate go to:<a href="http://www.caa.co.uk/home/" target="_blank">www.caa.co.uk</a></p>

				</div>
			</div>




