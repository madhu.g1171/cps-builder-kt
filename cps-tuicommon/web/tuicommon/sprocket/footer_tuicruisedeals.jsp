<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.tuicruisedeals" , "");
pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);

%>

<div id="footer">
	<div class="wrapper">
	<div class="container">
			<div class="col-3">
				<h3>Cruise Deals</h3>
				<ul>
					<li><a href="https://www.cruisedeals.co.uk/cruise-destinations">All Cruise Destinations</a></li>
					<li><a href="https://www.cruisedeals.co.uk/deals">2018 &amp; 2019 Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/best-cruises">Best-Selling Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/avonmouth">From Bristol</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/dover">From Dover</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/harwich">From Harwich</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/liverpool">From Liverpool</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/newcastle">From Newcastle</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/scotland">From Scotland</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/southampton">From Southampton</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk/tilbury">From Tilbury</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-under-999pp">Under �999pp</a></li>
					<li><a href="https://www.cruisedeals.co.uk/last-minute-cruises">Last Minute Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/summer-cruises">Summer Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-destinations/world-cruises">World Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/winter-cruises">Winter Cruises</a></li>
				</ul>
			</div>
			<div class="col-3">
				<h3>Cruise Types</h3>
				<ul>
					<li><a href="https://www.cruisedeals.co.uk/all-inclusive-cruises">All Inclusive Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/adult-only-cruises">Adult Only Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/christmas-new-year-cruises">Christmas &amp; New Year Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruises-from-the-uk">Cruises from the UK</a></li>
					<li><a href="https://www.cruisedeals.co.uk/family-cruises">Family Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/luxury-cruises">Luxury Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/mini-cruises">Mini Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/river-cruises">River Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/singles-cruises">Single Cruises</a></li>
				</ul>
			</div>
			<div class="col-3">
				<h3>Cruise Lines</h3>
				<ul>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/marella-cruises">Marella Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/royal-caribbean-international">Royal Caribbean</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/po-cruises">P&O Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/princess-cruises">Princess Cruises</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/cruise-maritime-voyages">Cruise & Maritime Voyages</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/fred-olsen-cruise-lines">Fred. Olsen Cruise Lines</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/avalon-waterways">Avalon Waterways</a></li>
					<li><a href="https://www.cruisedeals.co.uk/cruise-lines/viking-cruises">Viking Cruises</a></li>
				</ul>
			</div>
			<div class="col-3">
				<h3>Useful Links</h3>
				<ul>
					<li class=""><a href="https://www.cruisedeals.co.uk/information/about-us">About Us</a></li>
					<li class=""><a href="https://www.cruisedeals.co.uk/information/campaign-terms-and-conditions">Campaign Terms &amp; Conditions</a></li>
					<li class=""><a href="https://www.cruisedeals.co.uk/information/contact-us">Contact Us</a></li>
					<li class=""><a href="https://www.cruisedeals.co.uk/information/cookies">Cookies</a></li>
					<li class=""><a href="https://www.cruisedeals.co.uk/information/faqs">FAQs</a></li>
					<li class=""><a href="https://www.cruisedeals.co.uk/information/privacy-policy">Privacy Policy</a></li>
					<li class=""><a href="https://www.cruisedeals.co.uk/information/terms-and-conditions">Terms &amp; Conditions</a></li>
				</ul>
			</div>
	</div>
	<div class="office-details"><p>CruiseDeals.co.uk is a trading name of TUI UK Retail Limited. Registered office: Wigmore House, Wigmore Lane, Luton, LU2 9TN. Company Number: 01456086</p></div>
	<div class="atol">
		<h3>� 2017 TUI UK Retail Limited</h3>
		<p>
			Prices are per person based on 2 adults sharing the lowest grade inside room unless otherwise stated. Offers apply to new bookings only and are subject to very limited availability and can be changed/withdrawn at any time without notice. Additional supplement/charges may apply. Itineraries show ports of call only and are subject to change. Calls are free from BT landlines, other operators may charge. <c:if test="${applyCreditCardSurcharge eq 'true'}"> A fee of 2% applies to credit card payments (Visa, MasterCard, Amex). Payments by debit cards (Maestro or Delta) are free of charge.</c:if> Some of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme.  But ATOL protection does not apply to all holiday and travel services listed on this website. This website will provide you with information on the protection that applies in the case of each holiday and travel service offered before you make your booking.  If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information, or for more information about financial protection and the ATOL Certificate go to:<a href="http://www.caa.co.uk/home/" target="_blank">www.caa.co.uk</a>
		</p>
	</div>
	<div class="content-width">
		<div class="know-header-section">
		<h3 class="know-before-heading">Travel Aware &#45; Staying Safe and Healthy Abroad</h3>
		</div>
		<p>
			The Foreign &amp; Commonwealth Office and National Travel Health Network and Centre have up-to-date advice on staying safe and healthy abroad.
		</p>
		<p>For the latest travel advice from the Foreign &amp; Commonwealth Office including security and local laws, plus passport and visa information check                                                                       <a href="https://www.gov.uk/travelaware" target="_blank" class="stay-safe">www.gov.uk/travelaware</a> and follow <a href="https://twitter.com/FCOtravel" target="_blank" class="stay-safe">@FCOtravel</a> and <a href="https://www.facebook.com/fcotravel/" target="_blank" class="stay-safe">Facebook.com/FCOtravel</a></p>
		<p>More information is available by checking <a href="https://www.cruisedeals.co.uk/know-before-you-go.phtml" class="stay-safe"> www.cruisedeals.co.uk/know-before-you-go.phtml</a></p>
		<p> Keep informed of current travel health news by visiting  <a href="https://www.travelhealthpro.org.uk" target="_blank" class="stay-safe">www.travelhealthpro.org.uk</a></p>
		<p> The advice can change so check regularly for updates.</p>
	</div>
</div>


</div>




