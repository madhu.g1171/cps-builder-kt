<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="errorPage" value="true" scope="request" />
<c:set var='clientapp'	value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName}' />
<c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow eq 'true'}">

<c:set var='clientapp'	value='${bookingInfo.bookingComponent.lockYourPriceSummary.clientApplication}' />
</c:if>
<!-- currency -->

<c:choose>
<c:when test="${(clientapp == 'TUIFALCON' || clientapp == 'TUIFALCONFO')}">
<c:set var='currency' scope="request" value='&#128;'/>
</c:when>
<c:otherwise>
<c:set var='currency' scope="request" value='&#163;'/>
</c:otherwise>
</c:choose>
<c:set var="skyVariable" value="${param.clientapp}" scope="request"> </c:set>
<c:if test="${skyVariable=='TUICS' || skyVariable=='TUIES'}">
	<c:set var="clientapp" value="${skyVariable}" scope="page"></c:set>
</c:if>
<%
	String clientApp = (String)pageContext.getAttribute("clientapp");
	clientApp = (clientApp!=null)?clientApp.trim():clientApp;
	String sky=(String)request.getParameter("clientapp");
	clientApp = (clientApp==null && sky!=null &&(sky.equals("TUICS")||sky.equals("TUIES")))?sky.trim():sky;
	//added for payment page consolidation
	String title = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".title", "");
	String headerText = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".headerText", "");		
%>
	  

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Mobile > Book Flow > Payment</title>
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

	<link rel="stylesheet" href="/cms-cps/tuicommon/css/base_${fn:toLowerCase(clientapp)}.css" />

<c:choose>
	<c:when test="${isNewUI == 'true'}">
		<!-- Card Details -->
		<link rel="stylesheet"
			href="/cms-cps/tuicommon/css/bfHz_${fn:toLowerCase(clientapp)}.css" />
	</c:when>
	<c:otherwise>
		<link rel="stylesheet"
			href="/cms-cps/tuicommon/css/bf_${fn:toLowerCase(clientapp)}.css" />
	</c:otherwise>
</c:choose>

</head>
<body>
	<div class="structure">

		<div id="page">

			<jsp:include page="sprocket/header_${fn:toLowerCase(clientapp)}.jsp" />

			<div id="content" class="book-flow">
				<div class="content-width">

					<!-- Sorry - something has gone wrong... -->
					<div class="error-oh bg-light-grey marg-bottom-20">
						<div class="halfs">
							<div class="crop">
								<img src="/cms-cps/tuicommon/images/TOM_AIR_787_13_F0429_2.jpg" srcset="
									/cms-cps/tuicommon/images/TOM_AIR_787_13_F0429_3.jpg 658w,
									/cms-cps/tuicommon/images/TOM_AIR_787_13_F0429_2.jpg 488w,
									/cms-cps/tuicommon/images/TOM_AIR_787_13_F0429_1.jpg 232w" sizes="100vw" alt="" class="dis-block full" />
							</div>
						</div>
						<div class="halfs copy">
							<h1>Sorry - something has gone wrong with our website.</h1>
							<p class="grey-med marg-bottom-10">The feature you were using may not have worked properly. Our engineers have been informed.</p>
					        <p class="grey-med marg-bottom-10">If you notice any odd effects try re-loading the page.</p>

						</div>
					</div>

				</div>
			</div>

			<!--footer -->
			<jsp:include page="sprocket/footer_${fn:toLowerCase(clientapp)}.jsp" />

			<div class="page-mask"></div>
		</div>

	</div>
	<script src="/cms-cps/tuicommon/js/iscroll-lite.js" type="text/javascript"></script>
<script>
	  var tui = {};
	  tui.analytics = {};
	  tui.analytics.page = {};
	  tui.analytics.page.pageUid = "technicaldifficultiespage";
	 </script>
</body>
</html>
