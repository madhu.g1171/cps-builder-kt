<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="errorPage" value="false" scope="request" />
<c:set var="multiCentre" value="${bookingComponent.multiCentre}"/>
<c:set var="alertMessage" value="${bookingComponent.nonPaymentData['alertMessage']}"/>
<c:set var='clientapp'	value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName}' />
<c:set var="hccsmap" value="${bookingComponent.hccsMap}"/>
<c:set var="isNewUI" value="${bookingComponent.nonPaymentData['isNewUI']}"/>
<c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow eq 'true'}">

<c:set var='clientapp'	value='${bookingInfo.bookingComponent.lockYourPriceSummary.clientApplication}' />
</c:if>
<!-- currency -->

<c:choose>
<c:when test="${(clientapp == 'TUIFALCON' || clientapp == 'TUIFALCONFO')}">
<c:set var='currency' scope="request" value='&#128;'/>
</c:when>
<c:otherwise>
<c:set var='currency' scope="request" value='&#163;'/>
</c:otherwise>
</c:choose>




<%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         clientApp = (clientApp!=null)?clientApp.trim():clientApp;
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
         String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".CV2AVS.Enabled", "false");
         pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
         String isExcursion =com.tui.uk.config.ConfReader.getConfEntry(clientApp+".summaryPanelWithExcursionTickets", "false");
         pageContext.setAttribute("isExcursion", isExcursion, PageContext.REQUEST_SCOPE);
		 //added for payment page consolidation
         String title = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".title", "");
         String headerText = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".headerText", "");
         String holidaySummaryAccordian = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".holidaySummaryAccordian", "");
         String passengersAccordian = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".passengersAccordian", "");
         String paymentAccordian = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".paymentAccordian", "");
		
      %>
	  

	  
<!DOCTYPE HTML>
<html lang="en-US" class="" id="falcon">
<head>
	<meta charset="UTF-8">
	<title><%=title%> </title>
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<script>
var hccIframe = "${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}"
	var fjfoSelected = false;
</script>
<c:if test="${hccsmap.hccSwitch == 'true' && hccsmap.paymentFailure eq 'true'}">
	<form id="redirectorForm" action="${bookingComponent.paymentFailureURL}" method="post"></form>
	<script>
		(function(){
			document.getElementById('redirectorForm').submit();
		}());
	</script>
</c:if>
<c:if test="${clientapp eq 'TUIFALCONFO'}">
	<c:set var='fjfoSelected'	value='true' />
	<script>
		fjfoSelected = true;
	</script>
</c:if>
	
	
<c:choose>
	<c:when
		test="${clientapp == 'TUIHMCRUISE'}">
		<link rel="icon" type="image/png" href="/cms-cps/tuicommon/images/favicon_${fn:toLowerCase(clientapp)}.ico" />
	</c:when>
	<c:when
		test="${clientapp == 'FIRSTCHOICEMOBILE'}">
		<link rel="icon" type="image/png" href="/cms-cps/tuicommon/images/favicon_fcmobile.png" />
	</c:when>
	<c:when
		test="${clientapp == 'THOMSONMOBILE'}">
		<link rel="icon" type="image/png" href="/cms-cps/tuicommon/images/favicon_thmobile.png" />
	</c:when>
	<c:otherwise>
		<link rel="icon" type="image/png" href="/cms-cps/tuicommon/images/favicon_${fn:toLowerCase(clientapp)}.png" />
	</c:otherwise>
</c:choose>
	
<c:choose>
	<c:when
		test="${(clientapp == 'TUICRUISEDEALS' || clientapp == 'TUIHMCRUISE')}">
		<link rel="stylesheet" href="/cms-cps/tuicommon/css/footer_${fn:toLowerCase(clientapp)}.css" />
	</c:when>
	
	<c:when
		test="${clientapp != 'TUIFALCON' && isNewUI == 'false'}">
		<link rel="stylesheet" href="/cms-cps/tuicommon/css/ddB_${fn:toLowerCase(clientapp)}.css" />
	</c:when>
	
	<c:when
		test="${clientapp != 'TUIFALCON' && isNewUI == 'true'}">
		<link rel="stylesheet" href="/cms-cps/tuicommon/css/ddBHz_${fn:toLowerCase(clientapp)}.css" />
	</c:when>
	
	<c:otherwise>
		
	</c:otherwise>


</c:choose>

<c:choose>
	<c:when test="${isNewUI == 'true'}">
		<!-- Card Details -->
		<link rel="stylesheet" href="/cms-cps/tuicommon/css/bfHz_${fn:toLowerCase(clientapp)}.css" />
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" href="/cms-cps/tuicommon/css/bf_${fn:toLowerCase(clientapp)}.css" />
	</c:otherwise>
</c:choose>
	<link rel="stylesheet" href="/cms-cps/tuicommon/css/icons_${fn:toLowerCase(clientapp)}.css" />
	<link rel="stylesheet" href="/cms-cps/tuicommon/css/base_${fn:toLowerCase(clientapp)}.css" />
	<link rel="stylesheet" href="/cms-cps/tuicommon/css/mobile_${fn:toLowerCase(clientapp)}.css" />
	<link rel="stylesheet" href="/cms-cps/tuicommon/css/acss_${fn:toLowerCase(clientapp)}.css" />
	<link rel="stylesheet" href="/cms-cps/tuicommon/css/promostrip.css" />
	 
	<!-- <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script> -->
    <%@include file="javascript.jspf"%>
	
	
	<script type="text/javascript">
		var breadcrumbScroll = null;
		
		var lockYourPriceSummarySelected = '${bookingComponent.lockYourPriceSummary.selected}';
		var lockYourPriceSummaryPpPrice = '${bookingComponent.lockYourPriceSummary.ppPrice}';
		var lockYourPriceSummaryTotalPrice = '${bookingComponent.lockYourPriceSummary.totalPrice}';
		var cancellationPeriodInHours = '${bookingComponent.lockYourPriceSummary.cancellationPeriodInHours}';
		var isLockYourPriceMMBFlow = '${bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow}';
		var seoSection ;
		jQuery(document).ready(function() {
			enableSelectBoxes();
			enableCheckBoxes();
			summaryPanelModal();
			intiateIscroll(jQuery(".scroll"));
			changeAddr();
			radioGrpInit();
			initTooltip();

			var allLinksPanels = jQuery('.accordion .content').hide();
			jQuery('.trigger').click(function() {
				seoSection =  jQuery(".accordion > .item").toggleClass("item item open");
				if(seoSection.hasClass('item open')) {
				   allLinksPanels.slideDown("fast");
				}else {
					allLinksPanels.slideUp("fast");
				}
			});
			var accordionPanels = jQuery(".component .accordion-content").hide();
			jQuery(".accordion-trigger").click(function(e) {
				var accordionSection = jQuery(this).parent().find(".accordion-content");
				var iconCollapseExpand = jQuery(this).find(".icon-collapse-expand");
				if(jQuery(this).hasClass("open")){
					accordionSection.slideUp("fast");
					if(jQuery(this)[0].id== "summary-panel"){



						jQuery(this)[0].removeAttribute('analytics-text','HSC');
						jQuery(this)[0].removeAttribute('analytics-id','PAYUS');
					}else if(jQuery(this)[0].id == "passenger-details"){
						jQuery(this)[0].removeAttribute('analytics-text','PDC');
						jQuery(this)[0].removeAttribute('analytics-id','PAYUS');

					}

				} else {
					accordionSection.slideDown("fast");
					if(jQuery(this)[0].id == "summary-panel"){


							jQuery(this)[0].setAttribute('analytics-text','HSC');
						jQuery(this)[0].setAttribute('analytics-id','PAYUS');
					}else if(jQuery(this)[0].id == "passenger-details"){
						jQuery(this)[0].setAttribute('analytics-text','PDC');
						jQuery(this)[0].setAttribute('analytics-id','PAYUS');

					}
				}
				jQuery(this).toggleClass("open");
				jQuery(iconCollapseExpand).toggleClass("icon-down");
				jQuery(iconCollapseExpand).toggleClass("icon-up");

			});
				jQuery(".gift-card-accordion").parent().find(".gift-accordion-text").slideUp("fast");
				jQuery(".gift-card-accordion").click(function(e) {
				var accordionSection = jQuery(this).parent().find(".gift-accordion-text");
				var iconCollapseExpand = jQuery(this).find(".gift-collapse-expand");
				if(jQuery(this).hasClass("open")){
					accordionSection.slideUp("fast");


				} else {
					accordionSection.slideDown("fast");

				}
				jQuery(this).toggleClass("open");
				jQuery(iconCollapseExpand).toggleClass("icon-down");
				jQuery(iconCollapseExpand).toggleClass("icon-up");

		});
		});
		window.addEventListener("resize", function() {
			var scrollDiv = jQuery('#breadcrumb');
			setTimeout(function() {
				scrollintoView(scrollDiv)
			}, 200);
		}, false);

		(function() {
			var userAgent = window.navigator.userAgent;
			var msie = userAgent.indexOf("MSIE ");

			if (msie > 0){
				jQuery("html").addClass("ie");
			}
  })();
    </script>
	<script src="/cms-cps/tuicommon/js/config.js" type="text/javascript"></script>
	<script type="text/javascript">
		var ensLinkTrack = function(){};
</script>
</head>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag"%>
<version-tag:version />
</head>

<%
	String applyCreditCardSurcharge = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurcharge", applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurcharge = "<%= applyCreditCardSurcharge %>";
</script>

	<script type="text/javascript" language="javascript">
	function newPopup(url) {
		var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
	    if (win && win.focus) win.focus();
	}

	var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	var _idleSecondsCounter = 0;
	var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	var url="${bookingInfo.bookingComponent.breadCrumbTrail['SEARCH_RESULT']}";
	var sessionTimeOutFlag =  false;
	var prePaymentUrl = '${bookingComponent.prePaymentUrl}';

	window.addEventListener('pageshow', function(event) {
		console.log(event.persisted);
		if(event.persisted) {
			location.reload();
		}
		  if(event.currentTarget.performance.navigation.type == 2)
			{
				 location.reload();
				 }
	});

	function noback(){
		window.history.forward();
		try{
			$('expiryDateMM').selectedIndex = 0;
			$('expiryDateYY').selectedIndex = 0;
		}catch(ex){console.log(ex);}
		document.getElementById("tourOperatorTermsAccepted").checked=false;
	}


	//New code added for session time out pop-up
	var initialTime = Date.now();
    var currentTime = initialTime;
    var hidden = "hidden";
    //var myVar = setInterval(CheckIdleTime, 1000);
	window.setInterval(CheckIdleTime, 1000);
	window.alertMsg();
	function alertMsg(){

		var myvar = setTimeout(function(){
		document.getElementById("sessionTime").style.display = "block";
		jQuery("div.tooltip").css("display","none");
		jQuery('html').addClass("of-h");
		var count = Math.round((session_timeout - _idleSecondsCounter)/60);
		document.getElementById("sessiontimeDisplay").innerHTML = count +" mins";
		},IDLE_TIMEOUT_millisecond);

	}
	function CheckIdleTime() {
            _idleSecondsCounter++;
            if (  _idleSecondsCounter >= session_timeout) {
                    if (sessionTimeOutFlag == false){
                   	document.getElementById("sessionTime").style.display = "none";
				document.getElementById("sessionTimeOut").style.display = "block";
				jQuery('html').addClass("of-h");
				//clearInterval(myVar);
			}else{
				document.getElementById("sessionTimeout").style.display = "none";
				jQuery('html').removeClass("of-h");
                    }
                }

            }

            adjustTimer = function() {
               currentTime = Date.now();
               //timeDiff.innerHTML = parseInt((currentTime - initialTime)/1000);
               _idleSecondsCounter = parseInt((currentTime - initialTime)/1000);
    	}
	   //navigate to technical difficulty page

            // Standards:
            if (hidden in document)
                document.addEventListener("visibilitychange", onchange);
            else if ((hidden = "mozHidden") in document)
                document.addEventListener("mozvisibilitychange", onchange);
            else if ((hidden = "webkitHidden") in document)
                document.addEventListener("webkitvisibilitychange", onchange);
            else if ((hidden = "msHidden") in document)
                document.addEventListener("msvisibilitychange", onchange);

            // IE 9 and lower:
            else if ("onfocusin" in document)
                document.onfocusin = document.onfocusout = onchange;
            // All others:
            else
                window.onpageshow = window.onpagehide
                = window.onfocus = window.onblur = onchange;

            function onchange (evt) {
                var v = "visible", h = "hidden",
                    evtMap = {
                    focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h
                    };

                evt = evt || window.event;
                if (evt.type in evtMap) {
                    //document.body.className = evtMap[evt.type];
                    if(evtMap[evt.type] == 'visible') {
                        adjustTimer();
		}
	}
                else {
                    document.body.className = this[hidden] ? "hidden" : "visible";
                    if(!this[hidden]) {
                        adjustTimer();
                    }
                }
            }

            // set the initial state (but only if browser supports the Page Visibility API)
            if( document[hidden] !== undefined )
                onchange({type: document[hidden] ? "blur" : "focus"});

	function closesessionTime(){
		document.getElementById("sessionTime").style.display = "none";
		jQuery('html').removeClass("of-h");
	}
	function reloadPage(){
		window.sessionTimeOutFlag= true;
		document.getElementById("sessionTimeout").style.display = "none";
		jQuery('html').removeClass("of-h");

		window.noback();
		window.location.replace(window.url);
		return(false);

	}

	function homepage(){
		url="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}";
		window.noback();
		window.location.replace(url);
	}
	function activestate(){
		document.getElementById("sessionTime").style.display = "none";
		jQuery('html').removeClass("of-h");
		window.ajaxForCounterReset();
		window.alertMsg();
		window._idleSecondsCounter = 0;
	}
	function ajaxForCounterReset() {
		var token = "<c:out value='${param.token}' />";
		var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		var request = new Ajax.Request(url, {
			method : "POST"
		});
		window._idleSecondsCounter = 0;
	}

	function makePayment() {
		var paymentForm = $('CheckoutPaymentDetailsForm');
		var element = paymentForm.serialize();
		if(!window.leadTitle){
			window.leadTitle = "";
		}
		elementstring = element
				+ "&token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
		var url = "/cps/processMobilePayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />";
		elementstring = elementstring.replace("&title=&", "&title=" + leadTitle + "&");
		elementstring = elementstring.replace("payment_0_type=&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		//if(isvalid && cardValid){
			//document.getElementById("modal").style.display = "visible";
			var request = new Ajax.Request(url, {
				method : "POST",
				parameters : elementstring,
				onSuccess : showThreeDOverlay
			});
		//}
	}

	function showThreeDOverlay(http_request) {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			result = http_request.responseText;
			if (result.indexOf("3dspage") == -1) {
				if (result.indexOf("ERROR_MESSAGE") == -1) {
					document.postPaymentForm.action = result;
					//document.CheckoutPaymentDetailsForm.reset();
					var csrfTokenVal = "<c:out value="${bookingComponent.nonPaymentData['csrfToken']}"/>";
                    if(csrfTokenVal != '')
                    {
                     var csrfParameter = document.createElement("input");
                     csrfParameter.type = "hidden";
                     csrfParameter.name = "CSRFToken";
                     csrfParameter.value = csrfTokenVal;
                     document.postPaymentForm.appendChild(csrfParameter);
                    }

                    var loadingScreens = jQuery("#loader-screens")[0];
						var loadingScreensSticky = jQuery("html")[0];
        			loadingScreens.className += " show in";
			loadingScreensSticky.className += " modal-open";

			setTimeout(function(){ document.postPaymentForm.submit(); }, 500);

				} else {
					var errorMsg = result.split(':');
						jQuery('.alert').html('<i class="caret warning"></i>'+errorMsg[1]);
						document.getElementById("commonError").style.display = "block";
						jQuery('#commonError').removeClass('hide');
						jQuery(".ctnbutton").removeClass("disable");
						//document.getElementById('commonError').innerHTML = "<p><strong>"
						//		+ errorMsg[1] + "</p></strong>";
						//document.getElementById('commonError').className = "commonErrorSummary info-section clear padding10px mb20px";
						clearCardEntryElements();
				}
			} else {
				window.location.href = 	result;

			}
		} else {
			document.getElementById('errorMsg').innerHTML = "<h2>Payment failed. Please try again.</h2>";
			jQuery(".ctnbutton").removeClass("disable");
		}
	}
	window.scrollTo(0, 0);
}

/**
 ** This method submits the bank form present in the overlay and fills the overlay in the iframe.
**/
function bankRedirect() {
	document.bankform.target = "ACSframe";
	document.bankform.submit();
}
function clearCardEntryElements(){
	jQuery("#cardType").val("");
	jQuery("#cardTypespan").html("Select type of credit card");
	jQuery("#cardNumber").val("");
	jQuery("#cardNumberDiv").removeClass("valid");
	jQuery("#cardName").val("");
	jQuery("#cardNameDiv").removeClass("valid");
	jQuery("#expiryDateMM").val("");
	jQuery("#monthspan").html("MM");
	jQuery("#expiryDateYY").val("");
	jQuery("#yearspan").html("YY");
	jQuery("#securityCode").val("");
	jQuery("#securityCodeDiv").removeClass("valid");
	jQuery("#creditPaymentTypeCode").hide();
	jQuery("#debitPaymentTypeCode").hide();
	jQuery("#issueNumber").val("");
    jQuery("#issue").hide();
	document.getElementById("card-img").className = "";
	document.getElementById("card-desc").innerHTML = "";


}
	</script>

<body onload="setDefaultDepositOption();">
<%@include file="tealium.jspf"%>

<form id="CheckoutPaymentDetailsForm" name="paymentdetails" method="post" action="javascript:makePayment();">
<%@include file="flightsOnlyConfigSettings.jspf"%>

	<div class="structure">

		<div id="page" class="payment">

			<!-- Include for HEADER > Thomson -->
			<jsp:include page="sprocket/header_${fn:toLowerCase(clientapp)}.jsp" />
			
			<div class="mboxDefault"></div>
			<script type="text/javascript">
				mboxCreate("${clientapp}_PromoStrip");
			</script>
			
			<div id="content" class="book-flow responsive">
				<div class="content-width">
					<div id="main">
	<%@include file="topError.jspf" %>

						<div class="component-wrap">

						<c:if test="${(not empty bookingComponent.nonPaymentData['soldoutalertheader']) && (not empty bookingComponent.nonPaymentData['soldoutalertmessage'])}">
								<div class="price-header-section">

								<div class="price-inc-dec-info">


										<h2 class="icon-v2 icon-TUI_ArrowDown dark-blue standard-layout">
														<span class="tui-font fl marg-left-20"><b><c:out value="${bookingComponent.nonPaymentData['soldoutalertheader']}"/></b></span>
											</h2>

									<p class="grey-med">
									<c:out value="${bookingComponent.nonPaymentData['soldoutalertmessage']}"/>
									</p>
								</div>
                            </div>
							</c:if>

							<c:if test="${not empty alertMessage}">
							<div class="price-header-section">

							<div class='price-inc-dec-info <c:if test="${bookingComponent.nonPaymentData['alertLevel'] == 'decPrice'}">price-dec</c:if>'>
									<c:choose>
										<c:when test="${bookingComponent.nonPaymentData['alertLevel'] == 'decPrice'}">

											<c:choose>
												<c:when test="${clientapp == 'TUICS'|| clientapp == 'TUIES'}">
												<h2>
													<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
														<path fill="#252a32" d="M0 16c0-8.827 7.173-16 16-16s16 7.173 16 16-7.173 16-16 16-16-7.173-16-16zM2.613 16c0 7.387 6 13.387 13.387 13.387s13.387-6 13.387-13.387-6-13.387-13.387-13.387-13.387 6-13.387 13.387zM14.693 21.013v-13.173c0-0.72 0.587-1.307 1.307-1.307s1.307 0.587 1.307 1.307v13.173l5.947-5.947c0.507-0.507 1.333-0.507 1.84 0s0.507 1.333 0 1.84l-8.16 8.187c-0.507 0.507-1.333 0.507-1.84 0l-8.187-8.16c-0.507-0.507-0.507-1.333 0-1.84s1.333-0.507 1.84 0l5.947 5.92z"></path>
													</svg>
													<span><c:out value="${bookingComponent.nonPaymentData['messageHeader']}" escapeXml="false" /></span>
												</h2>
												</c:when>
												<c:otherwise>
													<h2 class="icon-v2 icon-TUI_ArrowDown dark-blue standard-layout">
														<span class="tui-font fl marg-left-20"><c:out value="${bookingComponent.nonPaymentData['messageHeader']}" escapeXml="false" /></span>
													</h2>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
										<c:choose>
												<c:when test="${clientapp == 'TUICS'|| clientapp == 'TUIES'}">
													<h2>
														<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
															<path fill="#252a32" d="M32 16c0 8.827-7.173 16-16 16s-16-7.173-16-16 7.173-16 16-16 16 7.173 16 16zM29.387 16c0-7.387-6-13.387-13.387-13.387s-13.387 6-13.387 13.387 6 13.387 13.387 13.387 13.387-6 13.387-13.387zM17.307 10.987v13.173c0 0.72-0.587 1.307-1.307 1.307s-1.307-0.587-1.307-1.307v-13.173l-5.92 5.947c-0.507 0.507-1.333 0.507-1.84 0s-0.507-1.333 0-1.84l8.16-8.16c0.507-0.507 1.333-0.507 1.84 0l8.16 8.16c0.507 0.507 0.507 1.333 0 1.84s-1.333 0.507-1.84 0l-5.947-5.947z"></path>
														</svg> 
														<span><c:out value="${bookingComponent.nonPaymentData['messageHeader']}" escapeXml="false" /></span>
													</h2>
												</c:when>
												<c:otherwise>
													<h2 class="icon-v2 icon-TUI_ArrowUp dark-blue standard-layout">
														<span class="tui-font fl marg-left-20"><c:out value="${bookingComponent.nonPaymentData['messageHeader']}" escapeXml="false" /></span>
													</h2>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
									<p class="grey-med">
									<c:out value="${bookingComponent.nonPaymentData['alertMessage']}" escapeXml="false" />
									</p>
								</div>
                            </div>
							</c:if>

						<div class="header-section">
						<c:choose>
						<c:when  test="${bookingComponent.lockYourPriceSummary.selected && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
								<div class="lypheading-Wrapper">
								  <c:choose>
                                    <c:when test="${clientapp == 'TUITH'}">
                                        <img class="lockIcon" aria-label="pound" src="/cms-cps/tuicommon/images/LockPricePound.svg" />
                                    </c:when>
                                    <c:when test="${clientapp == 'TUIFC'}">
                                        <img class="lockIcon" aria-label="pound" src="/cms-cps/tuicommon/images/LockPriceFCPound.svg" />
                                    </c:when>
                                    <c:when test="${clientapp == 'TUIFALCON'}">
                                        <img class="lockIcon" aria-label="euro" src="/cms-cps/tuicommon/images/LockPriceEuro.svg" />
                                    </c:when>
                                    <c:otherwise>
                                       
                                    </c:otherwise>
                                </c:choose>
                                  
								
								<h1 class="current-state payment-heading uppercase standard-layout lypHeading">
                              
                                    You're about to lock this price
                                </h1>
								</div>
						 </c:when>
						 <c:otherwise>
								<h1 class="current-state payment-heading uppercase standard-layout marg-top-20"><%=headerText %></h1>
							 </c:otherwise>
						 </c:choose>
						 <c:choose>
						 	<c:when test="${clientapp == 'TUICS'|| clientapp == 'TUIES'}">
						 		<div class="time-info">
									<h2 class="standard-layout">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="29" height="29" viewBox="0 0 29 29">
										<defs>
											<path id="785a3b0r1a" d="M14.5 29C6.492 29 0 22.508 0 14.5S6.492 0 14.5 0 29 6.492 29 14.5 22.508 29 14.5 29zm0-2.367c6.7 0 12.133-5.432 12.133-12.133 0-6.7-5.432-12.133-12.133-12.133C7.8 2.367 2.367 7.8 2.367 14.5c0 6.7 5.432 12.133 12.133 12.133zm-1.48-4.42v-9.562c0-.226.015-.406.043-.538.029-.132.09-.245.183-.34.079-.081.216-.154.409-.22.194-.066.412-.125.656-.176.173-.044.352-.077.538-.1.187-.021.338-.032.453-.032.093 0 .19.009.29.027.1.019.176.054.226.105.072.066.117.144.135.236.018.091.027.24.027.445v10.155c0 .176-.017.31-.049.402-.032.091-.095.17-.188.236-.079.073-.223.13-.43.17-.209.04-.492.06-.85.06-.338 0-.606-.02-.803-.06-.197-.04-.335-.097-.414-.17-.093-.066-.154-.145-.183-.236-.028-.092-.043-.226-.043-.402zm-.296-14.657c0-.222.045-.43.133-.625.089-.195.21-.37.366-.522.156-.153.336-.273.54-.36.206-.087.425-.13.658-.13.208 0 .405.043.59.13.187.087.352.205.496.355.145.15.26.321.345.516.085.195.128.407.128.636 0 .223-.042.433-.123.632-.082.198-.193.368-.335.511-.141.142-.304.257-.49.344-.186.087-.382.13-.59.13-.232 0-.454-.041-.662-.125-.208-.083-.39-.196-.546-.339-.156-.142-.28-.313-.372-.51-.092-.2-.138-.413-.138-.643z"/>
										</defs>
										<g fill="none" fill-rule="evenodd">
											<mask id="iaopnyyg4b" fill="#fff">
												<use xlink:href="#785a3b0r1a"/>
											</mask>
											<g fill="#1C6F71" mask="url(#iaopnyyg4b)">
												<path d="M0 0H29V29H0z"/>
											</g>
										</g>
									</svg>
										<span class=""> PLEASE COMPLETE IN 30 MINS</span>
									</h2>
			
								</div>

								<span class="grey-med sub-time-text standard-layout">After 30 minutes this page will time out and you will have to re-enter all your details.</span>
						 	</c:when>
						 	<c:otherwise>
						 		<div class="time-info">
									<h2 class="icon-v2 icon-info dark-blue standard-layout">
													<span class="tui-font fl marg-left-20"> PLEASE COMPLETE IN 30 MINS</span>
									</h2>
		
								</div>

										<span class="grey-med sub-time-text standard-layout">After 30 minutes this page will time out and you will have to re-enter all your details.</span>
						 	</c:otherwise>
						 </c:choose>
						
							</div>
							<div id="customer-form" class="full-width">
								<form name="paymentdetails" method="post" id="CheckoutPaymentDetailsForm" action="javascript:makePayment();" novalidate>






									<!-- Holiday Details -->
									<c:choose>
										<c:when
											test="${clientapp == 'TUIFALCON'}">
												<%@include file="holidayDetails_tuifalcon.jspf"%>
										</c:when>
										<c:when
											test="${clientapp == 'TUIFALCONFO'}">
												<%@include file="holidayDetails_tuifalconfo.jspf"%>
										</c:when>
										<c:when
											test="${clientapp == 'TUITHFO'}">
												<%@include file="holidayDetails_tuithfo.jspf"%>
										</c:when>
										<c:when
											test="${clientapp == 'TUICS'}">
												<%@include file="holidayDetails_ski.jspf"%>
										</c:when>
										<c:when
											test="${clientapp == 'TUIES'}">
												<%@include file="holidayDetails_ski_es.jspf"%>
										</c:when>
										<c:otherwise>
											<%@include file="holidayDetails.jspf"%>
										</c:otherwise>
									 </c:choose>

									<!-- Passenger Details -->
									<%@include file="passengerDetails.jspf"%>

									<!-- Secure Online payment -->
									<%@include file="depositSection.jspf"%>

									<c:choose>
                                	  <c:when	test="${hccsmap.hccSwitch == 'true' && bookingInfo.newHoliday == true}">
                                	    <!-- Card Details -->
									    <%@include file="hccCardDetails.jspf"%>
	                                  </c:when>
	                                 <c:otherwise>	                                 
	                                   <%@include file="cardDetails.jspf"%>
	                                   <!-- Card holder Details -->
									   <%@include file="cardHolderAddress.jspf"%>

		                                               	<!--Fare and rules  for TUIFALCONFO-->
										<c:if test="${clientapp == 'TUITHFO' || clientApp == 'TUIFALCONFO'}">
												<div class="component">
													<div class="section-heading bf-first">
														<h2 class="icon-v2 icon-TUI_Book dark-blue">
														<span class="tui-font">Fare rules</span>
													</h2></div>
													<div class="term-conditions-details grey-med row tnc">
													<p class="marg-bottom-20">Any changes must be made via the Call Centre up to 3 hours before departure. This is subject to availability.</p>

													<p class="marg-bottom-20">The purchased ticket is non-refundable. If you would like to amend your ticket, you will be charged for this. This charge will include a fee per change, per person. Depending on your amendment type, together with the fee(s) you may also pay the difference in airfare and you may lose a portion of your original airfare depending on the proximity of the departure date on the day you request your change.
													</p>
													<c:if test="${clientapp == 'TUITHFO'}">
													<p class="marg-bottom-20">Please see the <a href="JavaScript:newPopup('https://www.tui.co.uk/destinations/info/flight-conditions-of-carriage');" data-di-id="di-id-51c3bc9a-abf0e41">Conditions of Carriage</a> for further details.</p>
													</c:if>
													<c:if test="${clientapp == 'TUIFALCONFO'}">
													<p class="marg-bottom-20">Please see the <a href="JavaScript:newPopup('https://www.tuiholidays.ie/f/info/flight-conditions-of-carriage');" data-di-id="di-id-51c3bc9a-abf0e41">Conditions of Carriage</a> for further details.</p>
													</c:if>
													</div>
													</div>
											</c:if>
									   <!-- Terms and Conditions -->
									   <%@include file="termsAndCondition.jspf"%>		
	                                 </c:otherwise>
                                  </c:choose>

									<!-- Card holder Details -->
									<%-- <%@include file="cardHolderAddress.jspf"%>


									<!-- Terms and Conditions -->
									<%@include file="termsAndCondition.jspf"%> --%>




									<!-- CTA -->
									<div class="ctaContent marg-bottom-10">
									
										<div class="ctnbutton fr">
											<c:if test="${hccsmap.hccSwitch != 'true'}">
											<c:if test="${bookingInfo.newHoliday == true}">
												<c:choose>
												<c:when test="${bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
														<input type="submit" value="Lock this Price" class="book-flow button large cta lockbutton" id="paypalbutton">
												</c:when>
												<c:when test="${clientapp == 'TUIFC'|| clientapp == 'FIRSTCHOICEMOBILE'}">
														<input type="image" src="/cms-cps/tuicommon/images/logo/Book_this_holiday_button_fc.png" class="" id="paypalbutton" />
													</c:when>
													<c:when test="${clientapp == 'TUICS'|| clientapp == 'TUIES'}">
														<input type="submit" value="BOOK THIS HOLIDAY" class="button book-flow cta" id="paypalbutton" />
													</c:when>
													<c:when test="${clientapp == 'TUITHFO'|| clientapp == 'TUIFALCONFO'}">
														<input type="image" src="/cms-cps/tuicommon/images/logo/Book_this_flight_button.png" class="" id="paypalbutton" />
													</c:when>
													<c:when test="${requestScope.disablePaymentButton == 'true'}">
															<input type="image" src="/cms-cps/tuicommon/images/logo/Book_this_holiday_button.png" style="cursor:default;" disabled="disabled" class="disabled" id="paypalbutton" />
													</c:when>
													<c:otherwise>
																<input type="image" src="/cms-cps/tuicommon/images/logo/Book_this_holiday_button.png" class="" id="paypalbutton" />
													</c:otherwise>
												</c:choose>
											</c:if>
											</c:if>
										</div>
									<!-- ====================================== -->
									
										<c:choose>
											<c:when test="${hccsmap.hccSwitch == 'true'}">
												<!-- Card Details -->
												<div class="paydetails">
											</c:when>
											<c:otherwise>
											<div class="paydetails fl">
											</c:otherwise>
										</c:choose>
											<!--Start - About to pay section-->
											<div class="paypalcreditbutton">
											<div class="disNone" id="creditCardCharges" onclick="displayCreditCharges(this.id)">
												<p class="size-18"><span class="final-amount-text">You're about to pay&nbsp;</span><span class="creditCardChargeFinalAmt currency"><c:choose><c:when test="${clientapp == 'TUIFALCON' || clientapp == 'TUIFALCONFO'}">&euro;</c:when>    <c:otherwise>&pound;</c:otherwise></c:choose></span><span class="creditCardChargeFinalAmt amount" id="creditCardChargeFinalAmt"><c:out value="${fullAmount}" /></span> by Credit Card<c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
												<c:if test="${applyCreditCardSurcharge eq 'true'}">
												<p class="grey-med">Includes <fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% Credit Card Surcharge</p>
												</c:if>
											</div>
											<div id="debitCardCharges"onclick="displayCreditCharges(this.id)">
												<c:choose>
													<c:when
														test="${clientapp == 'TUIFALCON'}">
														<c:set value="&euro;" var="currency" scope="page"/>
														<p class="size-18">
														<span class="final-amount-text">You're about to pay&nbsp;</span>
															<span class="debitCardChargeFinalAmt currency" id="debitCardCurrency">${currency}</span>
															<span class="creditCardChargeFinalAmt amount" id="debitCardFinalAmt"><c:out value="${fullAmount}" /></span>
															<span id="debitCardText" class="hide">&nbsp; by Debit Card</span>
															<c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
													</c:when>
													<c:when
														test="${clientapp == 'TUIFALCONFO'}">
														<c:set value="&euro;" var="currency" scope="page"/>
														<p class="size-18">
                                                		<span class="final-amount-text">You're about to pay&nbsp;</span>
                                                		<span class="debitCardChargeFinalAmt currency" id="debitCardCurrency">${currency}</span>
                                                		<span class="creditCardChargeFinalAmt amount" id="debitCardFinalAmt"><c:out value="${fullAmount}" /></span><span id="debitCardText" class="hide"></span></p>
													</c:when>
													<c:when
														test="${clientapp == 'TUIES'}">
														<c:set value="&euro;" var="currency" scope="page"/>
														<p class="size-18">
														<span class="final-amount-text">You're about to pay&nbsp;</span>
															<span class="debitCardChargeFinalAmt currency" id="debitCardCurrency">${currency}</span>
															<span class="creditCardChargeFinalAmt amount" id="debitCardFinalAmt"><c:out value="${fullAmount}" /></span>
															<span id="debitCardText" class="hide">&nbsp; by Debit Card</span>
															<c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
													</c:when>
													<c:otherwise>
														<c:set value="&pound;" var="currency" scope="page"/>
														<p class="size-18">
														<span class="final-amount-text">You're about to pay&nbsp;</span>
                                                		<span class="debitCardChargeFinalAmt currency" id="debitCardCurrency">${currency}</span>
                                                		<span class="creditCardChargeFinalAmt amount" id="debitCardFinalAmt"><c:out value="${fullAmount}" /></span><span id="debitCardText" class="hide"></span><c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
													</c:otherwise>
												</c:choose> 			
											</div>
											<div class="disNone" id="thomsonCardCharges" style="display:none">
												<p class="size-18"><span>You're about to pay&nbsp;</span><span class="creditCardChargeFinalAmt currency">${currency}</span>
											<span class="creditCardChargeFinalAmt amount" id="thomsonCardFinalAmt"></span><span class="hide">&nbsp; by TUI Credit Card</span><c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
											</div>
											<div class="disNone" id="giftCardCharges" onclick="displayCreditCharges(this.id)">
												<p class="size-18"><span class="final-amount-text">You're about to pay&nbsp;</span><span class="giftCardFinalAmt currency">${currency}</span>
												<span class="creditCardChargeFinalAmt amount" id="giftCardFinalAmt"><c:out value="${fullAmount}" /></span> 
												<span class="hide">by Gift Card</span><c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
											</div>
											<div class="disNone" id="cardPaymentCharges" onclick="displayCardChargesPaypal(this.id)">
												<p class="size-18">
													<span class="final-amount-text">You're about to pay&nbsp;</span>
													<span class="cardPaymentChargeFinalAmt currency">${currency}</span>
													<span class="creditCardChargeFinalAmt amount" id="cardPaymentFinalAmt"></span>
													<span class="hide">&nbsp; by Credit Card</span><c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
													<p>No credit card fees.</p>
											</div>
											<div class="disNone" id="payPalCharges" onclick="displayCardChargesPaypal(this.id)">
												<p class="size-18"><span class="final-amount-text">You're about to pay&nbsp;</span><span class="payPalFinalAmt currency" id="paypalCurrency">${currency}</span>
                                                <span class="creditCardChargeFinalAmt amount" id="payPalFinalAmt"></span><span id="paypalCardText" class=""></span><c:if test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}"><span class="lockPriceText">&nbsp;to Lock Your Price</span></c:if></p>
											</div>
											</div>
											<c:if test="${isNewUI == 'false'}">
											<div class="displayCentre">
													<p class="size-18 payPalCreditDisplay hide">You're about to pay
														by PayPal Credit</p>
											</div>
											</c:if>
											<!--End - About to pay section-->
										</div>
										
									</div>
								</form>
							</div>
						</div>
						<!-- Sidebar -->
					</div>
				</div>
			</div>

			<!-- Include for Footer > TUICOMMON -->
			<jsp:include page="sprocket/footer_${fn:toLowerCase(clientapp)}.jsp" />
			
			<div class="page-mask"></div>
		</div>
	</div>

	<div id="video-container" class="hide"></div>
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
	<!-- Summary Flyout -->
	<div class="modal flyout ">
		<div class="window" >

			<div class="summary-panel bg-sand">
				<div class="image-container rel">
					<div class="total c bg-tui-sand black">
						<h3>&pound;</h3><h2><c:out value="${totalcost[0]}."/><span><c:out value="${totalcost[1]}"/></span></h2>
						<div>
						Total price
						</div>
						<i class="caret close b abs"></i>
					</div>
				</div>
				<div class="scroll-container" data-scroll-options='{"scrollX": false, "scrollY": true, "keyBindings": true, "mouseWheel": true}'>
					<div class="summary-breakdown">

							<c:choose>
								<c:when test="${multiCentre eq true}">

									<%@include file="summaryPanel/mcPricePanel.jspf"%>
									<%@include file="summaryPanel/flightDetails.jspf"%>
									<%@include file="summaryPanel/mcRoomAndBoardDetails.jspf"%>
								</c:when>
								<c:otherwise>
								 <%@include file="summaryPanel/pricePanel.jspf"%>
								 <%@include file="summaryPanel/flightDetails.jspf"%>
								<%@include file="summaryPanel/roomAndBoardDetails.jspf"%>

								</c:otherwise>
							</c:choose>

						<%@include file="summaryPanel/holidayExtras.jspf"%>
						<%@include file="summaryPanel/priceBreakdown.jspf"%>
					</div>

				</div>
			</div>

		</div>
	</div>
	<div class="tooltip top center tp" id="tooltipTmpl">
		<p></p>
		<span class="card-back"></span>
		<span class="arrow"></span>
	</div>
	<!-- <script type="text/javascript" src="/cms-cps/tuicommon/js/vs.js"></script> -->
	<script src="/cms-cps/tuicommon/js/iscroll-lite.js" type="text/javascript"></script>
	</form>
<div id="overlay" class="posFix"></div>
<form novalidate name="postPaymentForm" method="post">
		<input type="hidden" name="emptyForm" class="disNone"></input>
	</form>
<c:choose>
		<c:when test="${(clientapp == 'TUITH' || clientapp == 'TUIFC' || clientapp == 'TUIFALCON')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,tuiAnalyticsJson,alertLevel,alertMessage,csrfToken,isResponsiveHubAndSpoke,messageHeader,bootstrapUrl_old,numberallowedincityfield,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:when test="${(clientapp == 'THOMSONMOBILE' || clientapp == 'FIRSTCHOISEMOBILE')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,tuiAnalyticsJson,csrfToken,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:when test="${(clientapp == 'TUITHFO' || clientapp == 'TUIFALCONFO')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,csrfToken,showATOLForExternalCarrier,paxCount,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:when test="${(clientapp == 'TUITHCRUISE' || clientapp == 'TUITHRIVERCRUISE' || clientapp == 'TUICRUISEDEALS' || clientapp == 'TUIHMCRUISE')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,bootstrapUrl_old,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,TUIHeaderSwitch,bootstrapUrl,chkThirdPartyMarketingAllowed,chkTuiMarketingAllowed,communicateByEmail,communicateByPhone,communicateByPost,communicateBySMS,csrfToken,ensightenUrl,online_discount,tuiAnalyticsJson,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,tuiAnalyticsJson,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:otherwise>
	</c:choose>	
			<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "responsivePaymentDetailsPage";
				 				
				<c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					<c:set var="isAnalyticsParam" value="true"/>
					<c:forEach var="nonWebAnalytics" items="${nonWebAnalyticsData}">
						<c:if test="${nonWebAnalytics eq analyticsDataEntry.key}">
							<c:set var="isAnalyticsParam" value="false"/>
						</c:if>
					</c:forEach>
					<c:if test="${isAnalyticsParam eq 'true'}">
						<c:choose>
							<c:when test="${analyticsDataEntry.key == 'tuiAnalyticsJson' && (clientapp == 'TUITHFO' || clientapp == 'TUIFALCONFO')}"></c:when>
							<c:otherwise>
									tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
							</c:otherwise>
						</c:choose>
				    </c:if>
				</c:forEach>
				
				
				
			</script>

			<script>

			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});

			</script>
</body>
</html>
