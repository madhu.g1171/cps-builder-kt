var PaymentView =
{
   /**
    * Show alert and focus the field.
    */
   showAlert:function(Msg, fieldObj)
   {
      alert(Msg);
	  if (fieldObj != null)
	  {
         fieldObj.focus();
	  }
      return false;
   },

   /**
    * refresh the field
    */
   refreshTheField:function(fieldObj)
   {
      if (fieldObj.type == 'text')
      {
	     fieldObj.value = "";
	  }
	  else if (fieldObj.type == 'checkbox')
	  {
	     fieldObj.checked = false;
	  }
   },

   /**
    * clear the text in a input field.
    *
    * @param fieldObj the field read as DOM
    */
   emptyTheField : function(fieldObj)
   {
      fieldObj.val("");
   },

   /**
    * sets dropDown to default selection(i.e. selected index will be 0)
    *
    * @param dropDownObj the drop down read as DOM
    */
   setDropDownsToDefault : function(dropDownObj)
   {
      dropDownObj.selectedIndex = 0;
   },

   /**
    ** Highlights the container whose id is provided
    ** passed in as a parameter.
    ** @param containerID the id of the container to be highlighted
    ** @param highlightClass the css class which highlights the section
    ** @param isToBeHighlighted switch to highlight/unhighlight a section
   **/
       highlightContainer:function (containerID, highlightClass,isToBeHighlighted)
       {
		   if(isToBeHighlighted)
           {
			  console.log('containerID',containerID);
              jQuery("#"+containerID).addClass(highlightClass);
           }
           else
           {
              jQuery("#"+containerID).removeClass(highlightClass);
           }
       },

	 /**This function shall be responsible for updating the button caption of the Submit button*/
	 changeCaption: function(caption)
	{
		 jQuery("#submit").val(caption);
		 jQuery("#submit").attr("title",caption);
	},

	/**This function hides/unhides a field based on the flag sent*/
	displayContainer:function(containerID,isToBeHidden)
	{
		console.log('containerID',containerID);
		if(isToBeHidden)
		{
		   jQuery("#"+containerID).addClass("hide");
		}
		else
		{
		   jQuery("#"+containerID).removeClass("hide");
		}

	}
};


var cardChargeHandler=
{
		/**
		 ** Calculates the card charges for the amount
		 ** passed in as a parameter.
		 ** @param amount Amount for which card charges to be calculated.
		 ** @return cardCharge Card charge for the amount sent in.
		 **/
		calculateCardCharge:function(amount)
		{
		     if (cardChargeMap.get(PaymentInfo.selectedCardType) == 0)
           {
              return 0.0;
           }
		     var cardChargeData = cardChargeMap.get(PaymentInfo.selectedCardType).split(",");
		     PaymentInfo.chargePercent = cardChargeData[0];
		     PaymentInfo.minCardChargeAmount = cardChargeData[1];
		     PaymentInfo.maxCardChargeAmount = cardChargeData[2];
		     var cardCharge = MathUtils.roundOff(((amount * PaymentInfo.chargePercent)/100),2);

		     if (PaymentInfo.minCardChargeAmount != null)
		     {
		       cardCharge = (cardCharge < 1*PaymentInfo.minCardChargeAmount) ? 1*PaymentInfo.minCardChargeAmount : cardCharge;
		     }

		     if (PaymentInfo.maxCardChargeAmount != null && PaymentInfo.maxCardChargeAmount != "")
		     {
		      cardCharge = (cardCharge > 1*PaymentInfo.maxCardChargeAmount) ? 1*PaymentInfo.maxCardChargeAmount : cardCharge;
		     }
		     return cardCharge;
		},

        getApplicableCardCharge:function(amount,chargeType)
        {
			var cardChargeDetails = "";
			if (chargeType == "Credit")
			{
				cardChargeDetails = creditCardChargeDetails;
			}
			else
			{
				cardChargeDetails = debitCardChargeDetails;
			}

			var cardChargeArr = cardChargeDetails.split(",");
          var cardCharge = MathUtils.roundOff(((amount * cardChargeArr[0])/100),2);

          if (cardChargeArr[1] != null)
          {
             cardCharge = (cardCharge < 1*cardChargeArr[1]) ? 1*cardChargeArr[1] : cardCharge;
          }

          if (cardChargeArr[2] != null && cardChargeArr[2] != "")
          {
           cardCharge = (cardCharge > 1*cardChargeArr[2]) ? 1*cardChargeArr[2] : cardCharge;
          }
         return cardCharge;
       }
};


var CardTypeChangeHandler =
{
/**
 ** Highlights the appropriate amount section and updates essential fields
**/
    handleCardSelection:function ()
    {
       if(newHoliday == 'true')
       {
    	   //var selectedCardValue = jQuery("#cardType").val();
		   var selectedCardValue = document.getElementById("cardType").value;
    	   var selectedCardArray = selectedCardValue.split("|");
    	   PaymentInfo.selectedCardType = (selectedCardArray!=""&&selectedCardArray) ? selectedCardArray[0] : null;
		   console.log('PaymentInfo.selectedCardType',PaymentInfo.selectedCardType);
		   CardTypeChangeHandler.handleIssueNumberSection(PaymentInfo.selectedCardType);
           CardTypeChangeHandler.updateSectionToBeHighlighted();
           CardTypeChangeHandler.updateButtonCaption();

       }
    },

  /**hides/unhides the issue number section.*/
  handleIssueNumberSection:function(cardType)
  {
	  if (cardType=="SWITCH" || cardType=="SOLO")
	  {
		
          PaymentView.displayContainer("issue-number",false)
	  }
	  else
	  {
          PaymentView.displayContainer("issue-number",true);
	  }
  },


  /**
   ** Highlights the appropriate amount section based on the card charge applicability
   **/
  updateSectionToBeHighlighted:function()
  {
	if (PaymentInfo.selectedCardType)
	{
	   if(cardChargeMap.get(PaymentInfo.selectedCardType).split(",")[4] == "Credit")
       {
		
      	PaymentView.highlightContainer("amountWithCardCharge","green highlight",true)
      	PaymentView.highlightContainer("amountWithoutCardCharge","green highlight",false);
       }
      else
       {

      	PaymentView.highlightContainer("amountWithCardCharge","green highlight",false);
      	PaymentView.highlightContainer("amountWithoutCardCharge","green highlight",true);
       }
     }
	 else
	 {
	    PaymentView.highlightContainer("amountWithCardCharge","green highlight",false);
        PaymentView.highlightContainer("amountWithoutCardCharge","green highlight",false);
	 }
  },

	/**Updates the caption of the submit button based on 3DS*/
     updateButtonCaption:function()
	{

      var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
      if(payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup" || payButtonDescription == "americanexpressgroup")
     {
		  PaymentView.changeCaption("Pay Now");
     }
     else
     {
          PaymentView.changeCaption("Pay Now");
	 }

	}
};

var amountUpdationWithCardCharge =
{
		updateCardChargeForNEWSKIES:function()
		{			
			var calCardCharge = cardChargeHandler.getApplicableCardCharge(PaymentInfo.totalAmount,"Credit");
			PaymentInfo.totalCardCharge = calCardCharge;			
	        var amtWithCardCharge = MathUtils.roundOff(1*PaymentInfo.totalAmount + 1*PaymentInfo.totalCardCharge , 2);			
			jQuery("#amountWithCardCharge .amount").html(PaymentInfo.currencySymbol + amtWithCardCharge);


		},

		updatePaymentInfoForNEWSKIES:function()
	    {
			var selectedCard=document.getElementById("cardType").value;
            var selectedCardArray=selectedCard.split("|");
            if(selectedCardArray[0]=="MASTERCARD" || selectedCardArray[0]=="VISA" || selectedCardArray[0]=="AMERICAN_EXPRESS" ||selectedCardArray[0]=="VISA Purchasing" )
            {
            	PaymentInfo.calculatedPayableAmount = MathUtils.roundOff(1*PaymentInfo.totalAmount + 1*PaymentInfo.totalCardCharge , 2);
				PaymentInfo.calculatedTotalAmount = parseFloat((1*PaymentInfo.totalAmount )+( 1*PaymentInfo.totalCardCharge)).toFixed(2);
				document.getElementById("payment_0_paymenttypecode").value= PaymentInfo.selectedCardType;
		        document.getElementById("total_transamt").value=PaymentInfo.calculatedPayableAmount;
            }
            else if(selectedCardArray[0]=="MAESTRO" || selectedCardArray[0]=="DEBIT_MASTERCARD" || selectedCardArray[0]=="SWITCH" || selectedCardArray[0]=="VISA_ELECTRON" || selectedCardArray[0]=="SOLO" || selectedCardArray[0]=="VISA_DELTA")
			{
            	PaymentInfo.calculatedPayableAmount = MathUtils.roundOff(1*PaymentInfo.totalAmount, 2);
                PaymentInfo.calculatedTotalAmount = MathUtils.roundOff(1*PaymentInfo.totalAmount, 2);
				document.getElementById("payment_0_paymenttypecode").value= PaymentInfo.selectedCardType;
		        document.getElementById("total_transamt").value=PaymentInfo.calculatedPayableAmount;
            }
	    }

};

var postcodeValidation =
{
		postcodeChange:function()
		{			
			jQuery("#postCode").blur();
		}

};

var cardpostcodeValidation =
{
		cardpostcodeChange:function()
		{			
			jQuery("#cardPostCode").blur();
		}

};

var Personaldetails =
{
	noOfPassengers : "",
	setNoOfPassengers : function(noOfPassengers)
	{
	this.noOfPassengers = noOfPassengers;
	},

	getNoOfPassengers : function()
	{
      return this.noOfPassengers;
	},

	/**
	 * This function copies the surname of the lead passenger to the
	 * rest of the passengers when the user checks 'same surname as lead passenger' check box.
	 */
	autoCompleteSurname : function(passengerCount)
	{
		if(jQuery("#sameSurname").attr('checked'))
		{
		  for(index=1; index < passengerCount ; index++)
		   {
			 jQuery('#surName_'+index).val(jQuery('#surName_0').val());
			 if(!StringUtils.isBlank(jQuery('#surName_0').val()))
			 {
			    jQuery('#surName_'+index).blur();
			 }
		   }
		}
	},

	/**
	 * This function unchecks the 'same surname as lead passenger' check box when the user
	 * changes the surname of other passengers.
	 */
	unCheck : function()
	{
	  if(jQuery("#sameSurname"))
	  {
	     jQuery("#sameSurname").attr('checked', false);
	  }
	}
};

/**
 ** Responsible for showing/hiding the overlays
 **/
// var stickyOverlay =
// {
   // /**
    // ** Responsible for showing/hiding the 3D overlays
    // **/
   // threeDOverlay: function()
   // {
      // var overlayZIndex = 99;
      // var zIndex = 100;
      // var prevOverlay;
      // var stickyOpened = false;
      // jQuery("a.threeDSstickyOwner").click(function(e){
		// var last = jQuery("a.threeDSstickyOwner").length-1,
			// thisindex = jQuery('a.threeDSstickyOwner').index(this),
			// overlay = "#" + this.id + "Overlay",
			// jQuerygenOverlay = jQuery(overlay + ".genericOverlay"),
			// jQueryarowhead = jQuerygenOverlay.find('.arrow');
		
		 // if (!stickyOpened)
    	 // {
    	    // prevOverlay = overlay;
    	 // }
    	 // if (prevOverlay != overlay)
    	 // {
    	    // jQuery(prevOverlay).hide();
    	    // stickyOpened = false;
    	 // }
    	 // var pos = jQuery("#"+this.id).offset();
		 
    	 // jQuery(overlay).removeClass('hide').show();
    	 // prevOverlay = overlay;
    	 // stickyOpened = true;
    	 
		 // if(thisindex == last){
			// if(window.innerWidth < 386){
				// jQuerygenOverlay.css({'right':0});
				// jQueryarowhead.css({'left':'65%'});
			// }
		 // }else{
			// jQueryarowhead.removeAttr('style');
		 // }
		 // jQuerygenOverlay.css("z-index",zIndex);
    	 // zIndex++;
    	 // if (jQuery(overlay).parent(".overlay") != null){
    	    // jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
    	    // overlayZIndex++;
         // }
         // return false;
      // });
      // jQuery("a.close").click(function(){
         // var overlay = this.id.replace("Close","Overlay");
         // jQuery("#" + overlay).hide();
         // return false;
      // });
   // },

   /**
    ** Responsible for showing/hiding the summary panel overlays
    **/
   // summaryPanelOverlay :function()
   // {
      // var overlayZIndex = 99;
      // var zIndex = 100;
      // var prevOverlay;
      // var stickyOpened = false;
	  // // Don't remove this. Required for benefits overlay content.
	  // jQuery('.summaryPanelMid-s li:last-child').css("padding", "8px 0");
	  // jQuery('.summaryPanelMid-s li:last-child').css("background-image", "none");
      // jQuery("a.stickyOwner").mouseover(function(e){
         // var overlay = "#" + this.id + "Overlay";
    	 // if (!stickyOpened)
    	 // {
    	    // prevOverlay = overlay;
    	 // }
    	 // if (prevOverlay != overlay)
    	 // {
    	    // jQuery(prevOverlay).hide();
    		// stickyOpened = false;
    	 // }
    	 // var pos = jQuery("#"+this.id).offset();
		 // var top = parseInt((1*pos.top-167),10);
		 // var left = 20;
         // var width = 240;
		 // if(StringUtils.equalsIgnoresCase(this.id, 'benefit'))
		 // {
    	    // width = 255;
		 // }
		 // if(StringUtils.equalsIgnoresCase(this.id, 'subPrice'))
		 // {
    	    // width = 190;
		 // }
    	 // jQuery(overlay).show();
		 // jQuery(overlay).css("top",top);
    	 // jQuery(overlay).css("width",width);
		 // jQuery(overlay).css("left",left);
    	 // prevOverlay = overlay;
    	 // stickyOpened = true;
    	 // jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
    	 // zIndex++;
    	 // if (jQuery(overlay).parent(".overlay") != null){
    	    // jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
    	    // overlayZIndex++;
    	 // }
         // return false;
      // });
      // jQuery("a.stickyOwner").mouseout(function(e){
         // var overlay = "#" + this.id + "Overlay";
    	 // jQuery(overlay).hide();
         // return false;
      // });
   // }
// };

// var ImportantInformation=
// {
   // showDataProtectionNotice:function()
   // {
      // if (jQuery("#dataProtectionNotice").hasClass("hide"))
	     // jQuery("#dataProtectionNotice").removeClass("hide");
	  // else
	     // jQuery("#dataProtectionNotice").addClass("hide");
   // },

   // impInfoCheck:function()
   // {
      // if (jQuery("#tourOperatorTermsAccepted").val() === 'false')
	     // jQuery("#tourOperatorTermsAccepted").attr('value',true);
	  // else
	     // jQuery("#tourOperatorTermsAccepted").attr('value',false);
   // },

   // Popup : function(popURL,popW,popH,attr)
   // {
      // if (!popH) { popH = 350 }
      // if (!popW) { popW = 600 }
      // var winLeft = (screen.width-popW)/2;
      // var winTop = (screen.height-popH-30)/2;
      // var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;

      // popupWin=window.open(popURL,"popupWindow",winProp);
      // popupWin.window.focus()
   // },

   // PopupBookingConditions : function(popURL,popW,popH,attr)
   // {
      // if (!popH) { popH = 350 }
      // if (!popW) { popW = 600 }
      // var winLeft = (screen.width-popW)/2;
      // var winTop = (screen.height-popH-30)/2;
      // var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;

      // popupWin=window.open(popURL,"bookingWindow",winProp);
      // popupWin.window.focus()
   // },

   // updateFormElementFromCheckBoxMarketing : function(checkBoxObj, formElement)
   // {
      // if(jQuery("#" + checkBoxObj).attr('checked'))
         // jQuery("#" + formElement).val(false);
      // else
         // jQuery("#" + formElement).val(true);
   // }
// };

/***This function shall populate the cv2avs section with the personal details section.*/
// var AddressPopulationHandler =
// {
   // handle:function()
   // {
      // if(document.getElementById("useAddress").checked)
      // {
		 // jQuery("#cardPostCode").blur();
	     // jQuery("#cardHouseName").val(jQuery("#houseName").val());
		 // jQuery("#cardAddress1").val(jQuery("#addressLine1").val());
		 // jQuery("#cardAddress2").val(jQuery("#addressLine2").val());
		 // jQuery("#cardTownCity").val(jQuery("#city").val());
		 // jQuery("#cardCounty").val(jQuery("#county").val());	 
		 // jQuery("#cardPostCode").val(jQuery("#postCode").val());
		 // jQuery("#cardcountry").val(jQuery("#country").val());
		 // if(jQuery("#country").val()!=''){
			// jQuery("#cardcountry").closest('.select').find('.text').text(jQuery("#country").val());
		 // }else{
			// var defaultval = jQuery("#cardcountry").closest('.select').find('option:selected').text();
			// jQuery("#cardcountry").closest('.select').find('.text').text(defaultval);
		 // }
		 
		 // if(!StringUtils.isBlank(jQuery("#cardHouseName").val()))
		 // {
            // jQuery("#cardHouseName").blur();
		 // }

		 // if(!StringUtils.isBlank(jQuery("#cardAddress1").val()))
		 // {
            // jQuery("#cardAddress1").blur();
		 // }

		 // if(!StringUtils.isBlank(jQuery("#cardAddress2").val()))
		 // {
		    // jQuery("#cardAddress2").blur();
		 // }

		 // if(!StringUtils.isBlank(jQuery("#cardTownCity").val()))
		 // {
		    // jQuery("#cardTownCity").blur();
		 // }

		 // if(!StringUtils.isBlank(jQuery("#cardCounty").val()))
		 // {
		    // jQuery("#cardCounty").blur();
		 // }
		 
		 // if(!StringUtils.isBlank(jQuery("#cardcountry").val()))
		 // {
		    // jQuery("#cardcountry").blur();
		 // }

		 // if(!StringUtils.isBlank(jQuery("#cardPostCode").val()))
		 // {
		    // jQuery("#cardPostCode").blur();
		 // }

      // }
   // }
// };

function openWindow(url)
{
	window.open(url,'blank');
	return false;
}

/** Function to redraw the specified section to avoid IE border breaking issue. */
function forceRedraw(){
   if (jQuery.browser.msie && jQuery("#contentCol2").length){
      var element = jQuery("#contentCol2")[0];
	  var emptyTextNode = document.createTextNode(' ');
	  element.appendChild(emptyTextNode);
	  emptyTextNode.parentNode.removeChild(emptyTextNode)
   }
}
//error message analytics
var errorAnalytics= {
	
	cardNumber:'analytics-text="CaNumError"',
	cardName:'analytics-text="CusNamError"',
	expiryDateMM:'analytics-text="VedError"',
	expiryDateYY:'analytics-text="VedError"',
	securityCode:'analytics-text="SCError"',
	passengerTitle0:'analytics-text="CADTITError"',
	firstname:'analytics-text="CADTWFIFError"',
	surname:'analytics-text="CADOFIFError"',
	houseName:'analytics-text="CADHNNError"',
	addressLine1:'analytics-text="CADA1TWTWError"',
	addressLine2:'analytics-text="CADA2TWTWError"',
	city:'analytics-text="CADTOCIError"',
	county:'analytics-text="CADCOUOTWError"',
	postCode:'analytics-text="ElevError"',
	tourOperatorTermsAccepted:'analytics-text="COICError"'
}

/** Common implementation for client side on blur and submit validation error display */
function commonSubmitBlurErrorDisplay(args){	
   var field = jQuery(args[0]),
   	   fieldRow = field.closest('.row'),
	   analyticsText=errorAnalytics[fieldRow.context.id];
	   
   	   ermsg = args[2];
	   //console.log('field',field[0].id);
	   //console.log('fieldRow',fieldRow);
  if(fieldRow.children('.message')){
	   fieldRow.children('.message').remove();
   }
   fieldRow.removeClass("error valid").addClass('error');
   if(field.attr('id')=='tourOperatorTermsAccepted'){
	    tourAnalyticsText=errorAnalytics[field.attr('id')];
	fieldRow.append('<span class="message" analytics-id="PAYUS" style="padding-left:0"'  +tourAnalyticsText +'>'+ermsg+'</span>');
   }else{
	fieldRow.append('<span class="message" analytics-id="PAYUS"' +analyticsText +' >'+ermsg+'</span>');
   }
}