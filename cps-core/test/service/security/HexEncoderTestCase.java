/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: HexEncoderTestCase..java$
 *
 * $Revision: $
 *
 * $Date: 2009-03-19 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 */
package service.security;

import junit.framework.TestCase;

import com.tui.uk.service.security.HexEncoder;

/**
 * The TestCase for CPSKeyStoreTestCase class.
 *
 */
public class HexEncoderTestCase extends TestCase
{
   /**
    * Tests asymmetric encryption.
    *
    */
   public void testEncode()
   {
      byte[] test = new byte[]{'A', 'B', 'C'};
      String data = HexEncoder.encode(test);
      assertNotNull(data);
   }
}
