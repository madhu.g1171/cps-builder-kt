/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CPSKeyStoreTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2009-03-19 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 */
package service.security;

import junit.framework.TestCase;

import com.tui.uk.config.ConfReader;
import com.tui.uk.service.security.Decrypter;
import com.tui.uk.service.security.DecrypterFactory;

/**
 * The TestCase for CPSKeyStoreTestCase class.
 *
 */
public class CPSKeyStoreTestCase extends TestCase
{
   /**
    * Tests asymmetric encryption.
    *
    */
   public void testAsymmetricEncryption()
   {
      String encryptedCardInfo = "7522406C86C85CCBDBECFE75249371B4CB0DF9D30E0FDC1FC9DB87B58D7D15D"
        + "279023A3C7B1598E00B86B846A83C001474A31025E71558916589B4657E8BDD3CCD519C8BFE28ED36BF63"
        + "524E5D88A0FBAB1F966C94684BCCF5BF7AFC07E00D46690749FA40895E9B57381493A50CB36D9D0D3739BA"
        + "71BD21234128177A20BE91";
      String keystoreType = ConfReader.getConfEntry("keystore.KeystoreType", null);
      String keystoreFileName =
               ConfReader.getConfEntry("keystore.KeystoreFileName", null);
      String keystorePassword =
               ConfReader.getConfEntry("keystore.KeystorePassword", null);

      Decrypter  decrypter  = DecrypterFactory.getDecrypter(keystoreType, keystoreFileName,
               keystorePassword, null, "cluetrader", "1104570287");
      assertNotNull(decrypter.decrypt(encryptedCardInfo).toCharArray());
   }

   /**
    * Tests symmetric encryption.
    *
    */
   public void testSymmetricEncryption()
   {
      String encryptedCardInfo = "07E28017B992EE3657D1816E090C69F1";
      String keystoreType = ConfReader.getConfEntry("keystore.KeystoreType", null);
      String keystoreFileName =
               ConfReader.getConfEntry("keystore.KeystoreFileName", null);
      String keystorePassword =
               ConfReader.getConfEntry("keystore.KeystorePassword", null);

      Decrypter  decrypter  = DecrypterFactory.getDecrypter(keystoreType, keystoreFileName,
               keystorePassword, null, "tracs", "");
      assertNotNull(decrypter.decrypt(encryptedCardInfo).toCharArray());
   }

}
