package com.tui.uk.config;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import junit.framework.JUnit4TestAdapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tui.uk.exception.BrandException;
import com.tui.uk.log.LogWriter;

/** Test cases for BrandConfig class. */
public class BrandConfReaderTestCase
{

   /**
    * Set up.
    * @throws Exception if error
    */
   @Before
   public void setUp() throws Exception
   {
      BrandConfReader.setConfigFileName(System.getProperty("brandconffile"));
   }

   /**
    * Tear down.
    * @throws Exception if error.
    */
   @After
   public void tearDown() throws Exception
   {
      
   }

   /**
    * Test getBrandMappings.
    */
   @Test
   public void testGetBrandMappings()
   {
      try
      {
         assertNotNull("The brandMapping is not null" , BrandConfReader.getBrandMappings()); 
      }
      catch (BrandException cfe)
      {
         assertTrue(cfe.getMessage(), true);
      }

   }

   /**
    * test case for loading the configuration.
    */
   @Test
   public void testLoadConfiguration()
   {
      try
      {
         BrandConfReader.loadConfiguration(); 
      }
      catch (BrandException cfe)
      {
         assertTrue(cfe.getMessage(), true);
      }
   }

   /**
    * Test private constructor.
    * @throws Exception if error
    */
   @Test
   public void testPrivateConstructor() throws Exception
   {
      BrandConfReader brandConfig = null;
      try
      {
         Constructor[] cons = BrandConfReader.class.getDeclaredConstructors();
         cons[0].setAccessible(true);
         brandConfig = (BrandConfReader) cons[0].newInstance();
      }
      catch (InstantiationException e)
      {
         LogWriter.logWarningMessage("InstantiationException " + e);
      }
      catch (IllegalAccessException e)
      {
         LogWriter.logWarningMessage("IllegalAccessException " + e);
      }
      catch (InvocationTargetException ite)
      {
         LogWriter.logWarningMessage("InvocationTargetException " + ite);
      }

      if (brandConfig != null)
      {
         Class klass = brandConfig.getClass();
         Method[] methods = klass.getDeclaredMethods();
         for (int i = 0; i < methods.length; i++)
         {
            String methodName = methods[i].getName();
            methods[i].setAccessible(true);
            if (methodName.equalsIgnoreCase("checkConfiguration"))
            {
               try
               {
                  methods[i].invoke(brandConfig);
               }
               catch (InvocationTargetException ite)
               {
                  LogWriter.logWarningMessage("InvocationTargetException" + ite);
               }
               catch (IllegalAccessException e)
               {
                  LogWriter.logWarningMessage("IllegalAccessException" + e);
               }

            }
         }
      }
   }

   /**
    * Test case for incorrect config file path.
    */
   @Test
   public void testIncorrectConfigFilePath()
   {
      String file = "/abcxzs.zcvbs." + System.currentTimeMillis();   
      try
      {
         BrandConfReader.setConfigFileName(file);
         BrandConfReader.loadConfiguration();
         fail("Should have thrown FilterException as config file[" + file + "] does not exist");
      }
      catch (BrandException e)
      {
         assertTrue(true);
      }
   }
   
   /**
    * Test suit definition.
    * @return the test suit
    */
   public static junit.framework.Test suite()
   {
      return new JUnit4TestAdapter(BrandConfReaderTestCase.class);
   }

}

