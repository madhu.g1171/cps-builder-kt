/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ConfigTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-06-27 $
 *
 * $Author:  $
 *
 * $Log: $
 */
package com.tui.uk.config;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.tui.uk.exception.ConfReaderException;
import com.tui.uk.log.LogWriter;

/**
 * Unit test for the iScape Configuration class.<br/> This test requires
 * test.conf exist in the testResources folder.
 */
public class ConfReaderTestCase extends TestCase
{
   /** The class name. */
   private static String fqcn = ConfReaderTestCase.class.getName();

   /** The Constant LONG_NUMBER_ONE. */
   private static final long LONG_NUMBER_ONE = 33720368L;

   /** The Constant LONG_NUMBER_TWO. */
   private static final long LONG_NUMBER_TWO = 33720368L;

   /** The Constant LONG_NUMBER_THREE. */
   private static final long LONG_NUMBER_THREE = 9223372036854775807L;

   /** The Constant LONG_NUMBER_FOUR. */
   private static final long LONG_NUMBER_FOUR = 92233720775807111L;

   /** The Constant INTEGER_NUMBER. */
   private static final int INTEGER_NUMBER_ONE = 214748388;

   /** The Constant INTEGER_NUMBER_TWO. */
   private static final int INTEGER_NUMBER_TWO = 2147483647;

   /**
    * Reads the test.conf from [dev.home]/global/testResources location
    * Assums that system property dev.home being set before calling this
    * class For iScape build mechanism this has been set in junit task.
    *
    * @throws Exception the exception
    */
   protected void setUp() throws Exception
   {
      super.setUp();
      ConfReader.setAppName("UNITTEST");
      List<String> confFileNames = new ArrayList<String>();
      confFileNames.add(System.getProperty("configfile"));
      confFileNames.add(System.getProperty("datacashfile"));
      ConfReader.loadConfiguration(confFileNames);
   }

   /**
    * test all entry types being accuratly retrieved.
    */
   public void testEntries()
   {
      assertEquals("Hello World", ConfReader.getConfEntry(fqcn + ".STRING", null));
      assertEquals(INTEGER_NUMBER_TWO, ConfReader.getIntEntry(fqcn + ".INT", 0));
      assertEquals(LONG_NUMBER_THREE, ConfReader.getLongEntry(fqcn + ".LONG", 0));
      assertTrue(ConfReader.getBooleanEntry(fqcn + ".BOOLEAN", false));
   }

   /**
    * Test the non existing entries returns the code default values.
    */
   public void testCodeDefaultEntries()
   {
      assertEquals("Hello World", ConfReader.getConfEntry(fqcn + ".XSTRING", "Hello World"));
      assertEquals(INTEGER_NUMBER_ONE,
         ConfReader.getIntEntry(fqcn + ".XINT", INTEGER_NUMBER_ONE));
      assertEquals(LONG_NUMBER_FOUR, ConfReader.getLongEntry(fqcn + ".XLONG", LONG_NUMBER_FOUR));
      assertFalse(ConfReader.getBooleanEntry(fqcn + ".XBOOLEAN", false));
   }

   /**
    * test the overide behavior of entry.
    */
   public void testOverridenEntry()
   {
      assertEquals("Hello iScape", ConfReader.getConfEntry(fqcn + ".OVERRIDE_STRING", null));
   }

   /**
    * test for negative entries for long int & boolean.
    */
   // CHECKSTYLE:OFF
   public void testNegativeEntries()
   {
      assertEquals(-2147483647, ConfReader.getIntEntry(fqcn + ".-INT", 0));
      assertEquals(-9223372036854775807L, ConfReader.getLongEntry(fqcn + ".-LONG", 0));
      assertFalse(ConfReader.getBooleanEntry(fqcn + ".-BOOLEAN", true));
   }

   // CHECKSTYLE:ON

   /**
    * test not a number.
    */
   public void testNaNEntries()
   {
      assertEquals(1, ConfReader.getIntEntry(fqcn + ".NAN_INT", 1));
      assertEquals(LONG_NUMBER_TWO ,
         ConfReader.getLongEntry(fqcn + ".NAN_LONG", LONG_NUMBER_ONE));
   }

   /**
    * tests loading of configuration files.
    *
    * @throws Exception exception.
    */
   @SuppressWarnings("unchecked")
   public void testPrivateConstructor() throws Exception
   {
      ConfReader confReader = null;
      try
      {
         Constructor[] cons = ConfReader.class.getDeclaredConstructors();
         cons[0].setAccessible(true);
         confReader = (ConfReader) cons[0].newInstance();
      }
      catch (InstantiationException e)
      {
         LogWriter.logWarningMessage("InstantiationException " + e);
      }
      catch (IllegalAccessException e)
      {
         LogWriter.logWarningMessage("IllegalAccessException " + e);
      }
      catch (InvocationTargetException ite)
      {
         LogWriter.logWarningMessage("InvocationTargetException " + ite);
      }

      if (confReader != null)
      {
         Class klass = confReader.getClass();
         Method[] methods = klass.getDeclaredMethods();
         for (int i = 0; i < methods.length; i++)
         {
            String methodName = methods[i].getName();
            methods[i].setAccessible(true);
            if (methodName.equalsIgnoreCase("checkConfiguration"))
            {
               try
               {
                  methods[i].invoke(confReader);
               }
               catch (InvocationTargetException ite)
               {
                  LogWriter.logWarningMessage("InvocationTargetException" + ite);
               }
               catch (IllegalAccessException e)
               {
                  LogWriter.logWarningMessage("IllegalAccessException" + e);
               }

            }
         }
      }
   }

   /**
    * test for incorrect configuration file [in this case non existing]
    * which should throw an ConfigException exception.
    */
   public void testIncorrectConfigFilePath()
   {
      String file = "/abcxzs.zcvbs." + System.currentTimeMillis();
      List<String> confFileNames = new ArrayList<String>();
      confFileNames.add("test1.conf");
      try
      {
         ConfReader.loadConfiguration(confFileNames);
         fail("Should have thrown ConfigException as config file[" + file + "] does not exist");
      }
      catch (ConfReaderException e)
      {
         assertTrue(true);
      }
   }

   /**
    * Tear down.
    *
    * @throws Exception the exception
    */
   protected void tearDown() throws Exception
   {
      super.tearDown();
   }
}
