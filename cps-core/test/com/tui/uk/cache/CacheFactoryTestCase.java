/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CacheFactoryTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-09-10 $
 *
 * $Author: Sathish.E $
 *
 * $Log: $
 */
package com.tui.uk.cache;

import junit.framework.TestCase;

/**
 * Test case for CacheFactory class.
 *
 * @author sathish.e
 *
 */
public class CacheFactoryTestCase extends TestCase
{

    /** This is a test variable to hold a sample string object. */
    private static String samplestrng = "test1";

     /**
     * Ensure that all the createCache method is working as well as the
     * other methods in the class.
     */

    public static void testCreateCache()
    {
    try
    {
        Cache  cache = CacheFactory.createCache("test");
        assertNotNull(cache);
        cache.put(samplestrng, "TEST");
        assertEquals("TEST" , cache.get(samplestrng));
        NonDiskCacheImpl  nondiskcacheimp = (NonDiskCacheImpl) cache;
        assertEquals(1, nondiskcacheimp.getElementCount());
        nondiskcacheimp.dump();
        assertEquals(0, nondiskcacheimp.getElementCount());
        cache.remove(samplestrng);
        assertNull(cache.get(samplestrng));
        cache = CacheFactory.createCache("test1", 1);
        assertNotNull(cache);
        cache = CacheFactory.createEternalCache("test2");
        assertNotNull(cache);
    }
    catch (IllegalArgumentException iae)
    {
        assertTrue("Correct exception is thrown", true);
    }
    }
    /**
    * Ensure that CacheManager and its methods are working fine.
    */

    public static void testGetCacheManager()
    {
        CacheManager cachemgr = CacheFactory.getCacheManager();

        cachemgr.removeCache("test");
        assertEquals(2, cachemgr.getAll().size());
        cachemgr.removeAll();
        assertEquals(0, cachemgr.getAll().size());

    }

}
