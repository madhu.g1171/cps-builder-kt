/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: DOBValidatorTestCase.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2009-07-07 08:25:26 $
 * 
 * $Author: roopesh.s@sonata-software.com $
 * 
 * 
 * $Log: $.
 */
package com.tui.uk.validation.validator;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.validation.constants.ValidationConstants;

/**
 * Test class for DOBValidatorTestCase class.
 * 
 * @author roopesh.s
 * 
 */
public class DOBValidatorTestCase extends TestCase
{

   /** The constant DURATION. */
   private static final int DURATION = 7;

   /** The constant SLASH. */
   private static final String SLASH = "/";

   /** The constant DAY. */
   private static final int DAY = -1100;

   /** The constant PASSENGER_COUNT. */
   private static final String PASSENGER_COUNT = "1";

   /** The constant HUNDRED. */
   private static final int HUNDRED = 100;

   /** The constant INFANT_MAX_AGE. */
   private static final String MAX_AGE = "17";

   /** The constant INFANT_MIN_AGE. */
   private static final String MIN_AGE = "2";

   /** The constant AGE. */
   private static final String AGE = "3";

   /**
    * Testing validateDOB method with infant valid data.
    */
   public void testValidateDOBForInfant()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      Calendar cal = Calendar.getInstance();

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.DAY, String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.MONTH, String.valueOf(cal.get(Calendar.MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.YEAR, String.valueOf(cal.get(Calendar.YEAR)));

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.AGECLASSIFICATION, AgeClassification.INFANT.getCode());
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + "_age_for_validation", AGE);
      nonPaymentDataMap.put(ValidationConstants.DURATION, String.valueOf(DURATION));
      cal.add(Calendar.DATE, DURATION);

      cal.get(Calendar.DAY_OF_MONTH);
      cal.get(Calendar.MONTH);
      cal.get(Calendar.YEAR);

      String arrivalDate =
         cal.get(Calendar.DAY_OF_MONTH) + SLASH + cal.get(Calendar.MONTH) + SLASH
            + cal.get(Calendar.YEAR);

      String ageKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
            + ValidationConstants.AGE_FOR_VALIDATION;
      nonPaymentDataMap.put(ageKey, "0");

      // nonPaymentDataMap.put("arrivalDate", "01/07/2009");
      nonPaymentDataMap.put(ValidationConstants.ARRIVALDATE, arrivalDate);

      // setting max and min age
      String maxAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MAX_AGE;
      String minAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MIN_AGE;

      nonPaymentDataMap.put(maxAgeKey, MIN_AGE);
      nonPaymentDataMap.put(minAgeKey, "0");

      DOBValidator.validateDOB(nonPaymentDataMap, ageKey,
         ClientApplication.ThomsonAccommodationOnly);
      assertTrue(true);
   }

   /**
    * Testing validateDOB method with infant invalid data. Infant age is
    * more then infant max age.
    */
   public void testValidateDOBForInfant1()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      Calendar dobCalendar = Calendar.getInstance();
      dobCalendar.add(Calendar.DATE, DAY);

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.DAY, String.valueOf(dobCalendar.get(Calendar.DAY_OF_MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.MONTH, String.valueOf(dobCalendar.get(Calendar.MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.YEAR, String.valueOf(dobCalendar.get(Calendar.YEAR)));

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.AGECLASSIFICATION, AgeClassification.INFANT.getCode());

      nonPaymentDataMap.put(ValidationConstants.DURATION, String.valueOf(DURATION));
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, DURATION);

      cal.get(Calendar.DAY_OF_MONTH);
      cal.get(Calendar.MONTH);
      cal.get(Calendar.YEAR);

      String arrivalDate =
         cal.get(Calendar.DAY_OF_MONTH) + SLASH + cal.get(Calendar.MONTH) + SLASH
            + cal.get(Calendar.YEAR);

      String ageKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
            + ValidationConstants.AGE_FOR_VALIDATION;
      nonPaymentDataMap.put(ageKey, "1");

      // nonPaymentDataMap.put("arrivalDate", "01/07/2009");
      nonPaymentDataMap.put(ValidationConstants.ARRIVALDATE, arrivalDate);

      // setting max and min age
      String maxAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MAX_AGE;
      String minAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MIN_AGE;

      nonPaymentDataMap.put(maxAgeKey, MIN_AGE);
      nonPaymentDataMap.put(minAgeKey, "0");

      assertEquals(DOBValidator.validateDOB(nonPaymentDataMap, ageKey,
         ClientApplication.ThomsonAccommodationOnly), "nonpaymentdatavalidation."
         + ClientApplication.ThomsonAccommodationOnly.getClientApplicationName()
         + ".age.invalidchildcount");
   }

   /**
    * Testing validateDOB method with child valid data.
    */
   public void testValidateDOBForChild()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      Calendar dobCalendar = Calendar.getInstance();

      // making DOB
      dobCalendar.add(Calendar.DATE, DAY);
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.DAY, String.valueOf(dobCalendar.get(Calendar.DAY_OF_MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.MONTH, String.valueOf(dobCalendar.get(Calendar.MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.YEAR, String.valueOf(dobCalendar.get(Calendar.YEAR)));

      // passenger is child
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.AGECLASSIFICATION, AgeClassification.CHILD.getCode());

      // passenger age is 1 year
      String ageKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
            + ValidationConstants.AGE_FOR_VALIDATION;

      nonPaymentDataMap.put(ageKey, AGE);

      // duration 7
      nonPaymentDataMap.put(ValidationConstants.DURATION, String.valueOf(DURATION));

      // arrival date, adding 7 in current date.
      Calendar arrivalDateCalendar = Calendar.getInstance();
      arrivalDateCalendar.add(Calendar.DATE, DURATION);

      arrivalDateCalendar.get(Calendar.DAY_OF_MONTH);
      arrivalDateCalendar.get(Calendar.MONTH);
      arrivalDateCalendar.get(Calendar.YEAR);

      String arrivalDate =
         arrivalDateCalendar.get(Calendar.DAY_OF_MONTH) + SLASH
            + arrivalDateCalendar.get(Calendar.MONTH) + SLASH
            + arrivalDateCalendar.get(Calendar.YEAR);

      // nonPaymentDataMap.put("arrivalDate", "01/07/2009");
      nonPaymentDataMap.put(ValidationConstants.ARRIVALDATE, arrivalDate);

      // setting max and min age
      String maxAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MAX_AGE;
      String minAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MIN_AGE;

      nonPaymentDataMap.put(maxAgeKey, MAX_AGE);
      nonPaymentDataMap.put(minAgeKey, MIN_AGE);

      assertEquals(DOBValidator.validateDOB(nonPaymentDataMap, ageKey,
         ClientApplication.ThomsonAccommodationOnly), ValidationConstants.NOERROR);
   }

   /**
    * Testing validateDOB method. When date Of birth after current date.
    */
   public void testDateOfBirthAfterCurrentDate()
   {
      // hundred
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      Calendar dobCalendar = Calendar.getInstance();

      // making DOB
      dobCalendar.add(Calendar.DATE, HUNDRED);
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.DAY, String.valueOf(dobCalendar.get(Calendar.DAY_OF_MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.MONTH, String.valueOf(dobCalendar.get(Calendar.MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.YEAR, String.valueOf(dobCalendar.get(Calendar.YEAR)));

      // passenger is child
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.AGECLASSIFICATION, AgeClassification.CHILD.getCode());

      // passenger age is 1 year
      String ageKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
            + ValidationConstants.AGE_FOR_VALIDATION;

      nonPaymentDataMap.put(ageKey, AGE);

      // duration 7
      nonPaymentDataMap.put(ValidationConstants.DURATION, String.valueOf(DURATION));

      // arrival date, adding 7 in current date.
      Calendar arrivalDateCalendar = Calendar.getInstance();
      arrivalDateCalendar.add(Calendar.DATE, DURATION);

      arrivalDateCalendar.get(Calendar.DAY_OF_MONTH);
      arrivalDateCalendar.get(Calendar.MONTH);
      arrivalDateCalendar.get(Calendar.YEAR);

      String arrivalDate =
         arrivalDateCalendar.get(Calendar.DAY_OF_MONTH) + SLASH
            + arrivalDateCalendar.get(Calendar.MONTH) + SLASH
            + arrivalDateCalendar.get(Calendar.YEAR);

      nonPaymentDataMap.put(ValidationConstants.ARRIVALDATE, arrivalDate);

      // setting max and min age
      String maxAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MAX_AGE;
      String minAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MIN_AGE;

      nonPaymentDataMap.put(maxAgeKey, MAX_AGE);
      nonPaymentDataMap.put(minAgeKey, MIN_AGE);

      assertEquals(DOBValidator.validateDOB(nonPaymentDataMap, ageKey,
         ClientApplication.ThomsonAccommodationOnly), "nonpaymentdatavalidation."
         + ClientApplication.ThomsonAccommodationOnly.getClientApplicationName()
         + ".age.dobaftertoday");
   }

   /**
    * Testing validateDOB method. When date Of birth after current date.
    */
   public void testMaxAgeNull()
   {
      // hundred
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      Calendar dobCalendar = Calendar.getInstance();

      // making DOB
      dobCalendar.add(Calendar.DATE, DAY);
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.DAY, String.valueOf(dobCalendar.get(Calendar.DAY_OF_MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.MONTH, String.valueOf(dobCalendar.get(Calendar.MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.YEAR, String.valueOf(dobCalendar.get(Calendar.YEAR)));

      // passenger is child
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.AGECLASSIFICATION, AgeClassification.CHILD.getCode());

      // passenger age is 1 year
      String ageKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
            + ValidationConstants.AGE_FOR_VALIDATION;

      nonPaymentDataMap.put(ageKey, AGE);

      // duration 7
      nonPaymentDataMap.put(ValidationConstants.DURATION, String.valueOf(DURATION));

      // arrival date, adding 7 in current date.
      Calendar arrivalDateCalendar = Calendar.getInstance();
      arrivalDateCalendar.add(Calendar.DATE, DURATION);

      arrivalDateCalendar.get(Calendar.DAY_OF_MONTH);
      arrivalDateCalendar.get(Calendar.MONTH);
      arrivalDateCalendar.get(Calendar.YEAR);

      String arrivalDate =
         arrivalDateCalendar.get(Calendar.DAY_OF_MONTH) + SLASH
            + arrivalDateCalendar.get(Calendar.MONTH) + SLASH
            + arrivalDateCalendar.get(Calendar.YEAR);

      nonPaymentDataMap.put(ValidationConstants.ARRIVALDATE, arrivalDate);

      // setting max and min age
      String maxAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MAX_AGE;
      String minAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MIN_AGE;

      nonPaymentDataMap.put(maxAgeKey, null);
      nonPaymentDataMap.put(minAgeKey, null);

      assertEquals(DOBValidator.validateDOB(nonPaymentDataMap, ageKey,
         ClientApplication.ThomsonAccommodationOnly), "nonpaymentdatavalidation."
         + ClientApplication.ThomsonAccommodationOnly.getClientApplicationName()
         + ".age.maxagenotavailable");

   }

}
