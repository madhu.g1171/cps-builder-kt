/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: NonPaymentDataValidatorTestCase.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-07-07 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.validation.validator;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.junit.Test;

import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.PassengersDataPanel;
import com.tui.uk.exception.ValidationException;
import com.tui.uk.validation.constants.ValidationConstants;
import com.tui.uk.validation.exception.NonPaymentValidationException;

/**
 * Test class for NonPaymentDataValidator class.
 *
 * @author roopesh.s
 *
 */
public class NonPaymentDataValidatorTestCase extends TestCase
{

   /** The FORENAME. */
   private static final String FORENAME = "FORENAME";

   /** The constant DURATION. */
   private static final int DURATION = 7;

   /** The constant SLASH. */
   private static final String SLASH = "/";

   /** The constant INFANT_MIN_AGE. */
   private static final String MIN_AGE = "2";

   /** The constant PASSENGER_COUNT. */
   private static final String PASSENGER_COUNT = "1";

   /** The constant COUNT_TWO. */
   private static final String COUNT_TWO = "2";

   /** The constant COUNT_ONE. */
   private static final String COUNT_ONE = "1";

   /** The constant TRUE_VALUE. */
   private static final String TRUE_VALUE = "true";

   /**
    * This test method is testing fore name(non payment data). Passing valid fore name, it should
    * not go to the catch block.
    */
   public void testKeyValueDataField()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.UNDERSCORE + PassengersDataPanel.FORE_NAME, FORENAME);

      Validator validator = new NonPaymentDataValidator(ClientApplication.ThomsonBuildYourOwn);
      try
      {
         validator.validate("personaldetails_" + PASSENGER_COUNT + "_foreName", FORENAME);
         assertTrue(true);
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * This test method is testing fore name(non payment data). Passing invalid fore name, it should
    * go to the catch block.
    *
    * @throws ValidationException the validationException.
    */
   public void testKeyValueDataField1() throws ValidationException
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.UNDERSCORE + PassengersDataPanel.FORE_NAME, "1111");

      Validator validator = new NonPaymentDataValidator(ClientApplication.ThomsonBuildYourOwn);
      try
      {
         validator.validate(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + "_foreName",
            "1111");
         fail();
      }
      catch (ValidationException e)
      {
         try
         {
            throw new NonPaymentValidationException();
         }
         catch (NonPaymentValidationException www)
         {
            assertTrue(true);
         }
         try
         {
            throw new NonPaymentValidationException("message");
         }
         catch (NonPaymentValidationException www)
         {
            assertTrue(true);
         }
         try
         {
            throw new NonPaymentValidationException("code", "message");
         }
         catch (NonPaymentValidationException www)
         {
            assertTrue(true);
         }
         try
         {
            throw new NonPaymentValidationException(new Throwable());
         }
         catch (NonPaymentValidationException www)
         {
            assertTrue(true);
         }
         try
         {
            new NonPaymentValidationException(new Throwable(), "code").getCode();
            throw new NonPaymentValidationException(new Throwable(), "code");
         }
         catch (NonPaymentValidationException www)
         {
            assertTrue(true);
         }
         try
         {
            throw new NonPaymentValidationException("message", new Throwable());
         }
         catch (NonPaymentValidationException www)
         {
            assertTrue(true);
         }

      }
   }

   /**
    * This test method is validating infant age. Passing valid data, it should not go to the catch
    * block.
    */
   public void testNonPaymentDataMapWithValidData()
   {
      String passengerCount = COUNT_ONE;
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      Calendar cal = Calendar.getInstance();

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.DAY, String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.MONTH, String.valueOf(cal.get(Calendar.MONTH)));
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.YEAR, String.valueOf(cal.get(Calendar.YEAR)));

      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.AGECLASSIFICATION, AgeClassification.INFANT.getCode());
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + "_age_for_validation", "3");
      nonPaymentDataMap.put(ValidationConstants.DURATION, String.valueOf(DURATION));
      cal.add(Calendar.DATE, DURATION);

      cal.get(Calendar.DAY_OF_MONTH);
      cal.get(Calendar.MONTH);
      cal.get(Calendar.YEAR);

      String arrivalDate =
         cal.get(Calendar.DAY_OF_MONTH) + SLASH + cal.get(Calendar.MONTH) + SLASH
            + cal.get(Calendar.YEAR);

      String ageKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
            + ValidationConstants.AGE_FOR_VALIDATION;
      nonPaymentDataMap.put(ageKey, "0");

      // nonPaymentDataMap.put("arrivalDate", "01/07/2009");
      nonPaymentDataMap.put(ValidationConstants.ARRIVALDATE, arrivalDate);

      // setting max and min age
      String maxAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MAX_AGE;
      String minAgeKey =
         ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT + ValidationConstants.MIN_AGE;

      nonPaymentDataMap.put(maxAgeKey, MIN_AGE);
      nonPaymentDataMap.put(minAgeKey, "0");
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + passengerCount
         + ValidationConstants.UNDERSCORE + PassengersDataPanel.FORE_NAME, FORENAME);

      Validator validator = new NonPaymentDataValidator(ClientApplication.ThomsonBuildYourOwn);
      try
      {
         // exceptionRequired field is true
         validator.validate(nonPaymentDataMap, true);
         assertTrue(true);
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
      try
      {
         // exceptionRequired field is false.
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);

         if (errorMap.size() == 0)
         {
            assertTrue(true);
         }
         else
         {
            fail();
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * This test method is testing non payment data fields. Passing valid data, map size should equal
    * to zero.
    */
   public void testValidateNonPaymentDataWithErrorMap()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT
         + ValidationConstants.UNDERSCORE + PassengersDataPanel.FORE_NAME, FORENAME);

      Validator validator = new NonPaymentDataValidator(ClientApplication.ThomsonBuildYourOwn);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         assertTrue(true);
         if (errorMap.size() != 0)
         {
            fail();
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * This method is responsible for testing validateAdultAccompanyingInfant method. validating more
    * than one Infant for same accompanying adult. Passing valid data.
    */
   @Test
   public void testValidateAdultAccompanyingInfantWithValidData()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.PASSENGER_INF_COUNT, COUNT_TWO);
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.TRAVEL_WITH, "Adult1");
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + 2 + ValidationConstants.TRAVEL_WITH,
         "Adult2");

      Validator validator = new NonPaymentDataValidator(ClientApplication.ThomsonBuildYourOwn);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (!errorMap.isEmpty())
         {
            fail();
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * This method is responsible for testing validateAdultAccompanyingInfant method. validating more
    * than one Infant for same accompanying adult. Passing invalid data.
    */
   @Test
   public void testValidateAdultAccompanyingInfantWithInValidData()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.PASSENGER_INF_COUNT, COUNT_TWO);
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.TRAVEL_WITH, "Adult1");
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + 2 + ValidationConstants.TRAVEL_WITH,
         "Adult1");

      Validator validator = new NonPaymentDataValidator(ClientApplication.TFLY);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (errorMap.isEmpty())
         {
            assertTrue(true);
         }
      }
      catch (ValidationException e)
      {
         assertTrue(true);
         e.printStackTrace();
      }
   }

   /**
    * This method is responsible for testing validateChildMealsWithValidData method. validating if
    * child age is more then 12, then adult meals should be selected. Passing valid data.
    */
   public void testValidateChildMealsWithValidData()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.PASSENGER_CHD_COUNT, COUNT_ONE);
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + 1
         + ValidationConstants.CHILD_MEALS_SELECTED, "false");
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.CHILD_AGE, "13");

      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.CHILD_FORE_NAME, "john");

      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.CHILD_LAST_NAME, "uk");

      Validator validator = new NonPaymentDataValidator(ClientApplication.TFLY);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (!errorMap.isEmpty())
         {
            fail();
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * This method is responsible for testing validateChildMealsWithValidData method. validating if
    * child age is more then 12, then adult meals should be selected. Passing invalid data.
    */
   public void testValidateSenior()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.PASSENGER_CHD_COUNT, PASSENGER_COUNT);
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.CHILD_MEALS_SELECTED, TRUE_VALUE);
      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.CHILD_AGE, "13");

      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.CHILD_FORE_NAME, "john");

      nonPaymentDataMap.put(ValidationConstants.PASSENGER + PASSENGER_COUNT
         + ValidationConstants.CHILD_LAST_NAME, "uk");

      Validator validator = new NonPaymentDataValidator(ClientApplication.TFLY);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (errorMap.isEmpty())
         {
            fail();
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * Testing age between 65 to 75 and age between 76 to 84.
    * */
   public void testValidateSenior1()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.AGEBETWEEN_65_TO_75, TRUE_VALUE);
      nonPaymentDataMap.put(ValidationConstants.SENIOR_COUNT, COUNT_ONE);
      nonPaymentDataMap.put(ValidationConstants.AGEBETWEEN_76_TO_84, TRUE_VALUE);
      nonPaymentDataMap.put(ValidationConstants.SENIOR2_COUNT, COUNT_ONE);

      Validator validator = new NonPaymentDataValidator(ClientApplication.TFLY);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (errorMap.isEmpty())
         {
            assertTrue(true);
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * Testing age between 65 to 75.
    * */
   public void testInValidateSenior1()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.AGEBETWEEN_65_TO_75, TRUE_VALUE);
      nonPaymentDataMap.put(ValidationConstants.SENIOR_COUNT, COUNT_TWO);

      Validator validator = new NonPaymentDataValidator(ClientApplication.TFLY);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (errorMap.isEmpty())
         {
            assertTrue(true);
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * Testing age between 76 to 84.
    * */
   public void testInValidateSenior2()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.AGEBETWEEN_76_TO_84, TRUE_VALUE);
      nonPaymentDataMap.put(ValidationConstants.SENIOR2_COUNT, COUNT_TWO);

      Validator validator = new NonPaymentDataValidator(ClientApplication.TFLY);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (errorMap.isEmpty())
         {
            assertTrue(true);
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * Testing post code.
    * */
   public void testValidatePostCode()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.POST_CODE, "CV12AQ");
      nonPaymentDataMap.put(ValidationConstants.COUNTRY, "GB");

      Validator validator = new NonPaymentDataValidator(ClientApplication.ThomsonAccommodationOnly);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (errorMap.isEmpty())
         {
            assertTrue(true);
         }
      }
      catch (ValidationException e)
      {
         fail();
         e.printStackTrace();
      }
   }

   /**
    * Testing post code.
    * */
   public void testInValidatePostCode()
   {
      Map<String, String> nonPaymentDataMap = new HashMap<String, String>();
      nonPaymentDataMap.put(ValidationConstants.POST_CODE, "12345");
      nonPaymentDataMap.put(ValidationConstants.COUNTRY, "GB");

      Validator validator = new NonPaymentDataValidator(ClientApplication.ThomsonBuildYourOwn);
      try
      {
         Map<String, String> errorMap = validator.validate(nonPaymentDataMap, false);
         if (errorMap.isEmpty())
         {
            assertTrue(false);
         }
      }
      catch (ValidationException e)
      {
         assertTrue(true);
         e.printStackTrace();
      }
   }
}
