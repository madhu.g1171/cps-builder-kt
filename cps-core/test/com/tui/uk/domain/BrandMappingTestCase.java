package com.tui.uk.domain;

import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/** 
 * Test cases for Brand mapping object.
 * @author kumarhe
 *
 */
public class BrandMappingTestCase
{

  /** The brand mapping object. */
   private BrandMapping bm = null;
   
   
   /**
    * Set up.
    * @throws Exception if error
    */
   @Before
   public void setUp() throws Exception
   {
      bm = new BrandMapping("thomson.co.uk", "thomson");
   }

   /**
    * Tear down.
    * @throws Exception if error.
    */
   @After
   public void tearDown() throws Exception
   {
      bm = null;
   }

   /**
    * test constructor.
    */
   @Test
   public void testBrandMapping()
   {
      assertNotNull(bm);
   }
   /**
    * Test find method.
    */
   @Test
   public void testFind()
   {
      assertEquals(bm.find("thomson.co.uk"), "thomson");
   }
   
   /**
    * Test case suite.
    * @return returns suite.
    */
   public static junit.framework.Test suite()
   {
      return new JUnit4TestAdapter(BrandMappingTestCase.class);
   }

}
