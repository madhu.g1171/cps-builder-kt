/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponentTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-09-10 $
 *
 * $Author: Venkat $
 *
 * $Log: $
 */
package com.tui.uk.domain;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.payment.service.datacash.DataCashCard;

/**
 * This is the test case for Transaction Class.
 */
public class TransactionTestCase extends TestCase
{
    /** The transaction source. */
    private String transactionSource;

    /** The transaction type. */
    private String transactionType;

    /** The character array holding a valid card number. */
    private char[] validPan = "4444333322221111".toCharArray();

    /** The name on card. */
    private String nameOncard = "xxxxxxxx";

    /** The expiry date. */
    private String expiryDate = "12/09";

    /** The character array holding cv2. */
    private char[] cv2 = {'4', '4', '4' };

    /** The post code. */
    private String postCode = "CV24EB";

    /** The card type. */
    private String cardType = "visa";

    /** The client account.*/
    /*private String clientAccount = "99080800";*/

    /**Address.*/
    private Map<String, String> address = new HashMap<String, String>();
    /**Cvv policy.*/
    private Map<String, String> cvvPolicies = new HashMap<String, String>();
    /**Address policy.*/
    private Map<String, String> addressPolicies = new HashMap<String, String>();
    /**Post code policy.*/
    private Map<String, String> postCodePolicies = new HashMap<String, String>();

   /**
    * Tests the Transaction constructor.
    */
   public void testTransactionConstructor()
   {
      Transaction transaction = createTransaction();
      transaction.getTransactionId();
      transaction.getTransactionDate();
      transaction.getTransactionType();
      transaction.getTransactionSource();
      transaction.getCard();
   }

   /**
    * Tests the Transaction constructor.
    */
   public void testConstructor()
   {
      Money money = new Money(BigDecimal.valueOf(Double.parseDouble("100")),
         Currency.getInstance("GBP"));
      Transaction transaction = new Transaction("123456", money);
      assertNotNull(transaction.getTransactionAmount());
   }

   /**
    * Tests do payment.
    */
   /**
   public void testDoPayment()
   {
      Transaction transaction = createTransaction();
      transaction.doPayment(clientAccount, "");
   }*/

   /**
    * Tests refund.
    */
   /*
   public void testRefund()
   {
      Transaction transaction = createTransaction();
      transaction.doRefund(clientAccount, "");
   }*/

   /**
    * Tests refund.
    */
   /*
   public void testFulfilTransaction()
   {
      Transaction transaction = createTransaction();
      transaction.doPayment(clientAccount, "");
      transaction.doFulfilTransaction(clientAccount);
   }*/

   /**
    * Tests purge card details.
    */
   public void testPurgeCardDetails()
   {
      Transaction transaction = createTransaction();
      transaction.purgeCardDetails();
   }

   /**
    * Used to create a Transaction object.
    *
    * @return transaction object.
    */
   private Transaction createTransaction()
   {
      Money amount = new Money(BigDecimal.valueOf(Double.parseDouble("100")),
         Currency.getInstance("GBP"));
      DataCashCard card = new DataCashCard(validPan, nameOncard, expiryDate, cv2, postCode,
         cardType, null, address, cvvPolicies, addressPolicies, postCodePolicies);

      return new Transaction("123456", new Date(), transactionSource,
               transactionType, amount, card);
   }
}
