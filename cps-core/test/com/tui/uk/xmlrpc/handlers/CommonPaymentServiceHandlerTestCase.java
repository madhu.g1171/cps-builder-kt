/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence

any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/
package com.tui.uk.xmlrpc.handlers;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.payment.domain.CnpCard;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;

/**
 * This class is used to test the .NET APIs.
 *
 * @author sindhushree.g
 *
 */
public class CommonPaymentServiceHandlerTestCase extends TestCase
{

   /** The allowed amount. */
   private static final double ALLOWED_AMOUNT_0 = 10.0;

   /** The allowed amount. */
   private static final double ALLOWED_AMOUNT_1 = 10.0;

   /** The Currency string. */
   private static final String CURRENCY = "GBP";

   /** The country locale. */
   private static final String COUNTRY_LOCALE = "GB";

   /** The datacash vTid. */
   private static final String DATACASH_VTID = "99080800";

   /** The token. */
   private String token;

   /** The constant THREE. */
   private static final int THREE = 3;

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();
      CommonPaymentServiceHandler cps = new CommonPaymentServiceHandler();
      Double[] allowedAmounts = {ALLOWED_AMOUNT_0, ALLOWED_AMOUNT_1};
      String[] paymentTypes = {"DCARD"};
      Map<String, String> applicationData = new HashMap<String, String>();
      applicationData.put("client_domain_url", "http://localhost:8080");
      applicationData.put("intellitrackerSnippet", "");
      applicationData.put("prevPage", "prePayment.page");

      token = cps.generateToken2(CURRENCY, COUNTRY_LOCALE, "TFly", DATACASH_VTID,
         allowedAmounts, paymentTypes, applicationData);
   }

   /**
    * Test case to test getPaymentData method.
    */
   public void testGetPaymentData()
   {
      CommonPaymentServiceHandler cps = new CommonPaymentServiceHandler();

      Double[] allowedAmounts = {ALLOWED_AMOUNT_0, ALLOWED_AMOUNT_1};
      String[] paymentTypes = {"DCARD"};
      Map<String, String> applicationData = new HashMap<String, String>();
      applicationData.put("client_domain_url", "http://localhost:8080");
      applicationData.put("intellitrackerSnippet", "");
      applicationData.put("prevPage", "prePayment.page");

      BookingInfo bookingInfo = new BookingInfo(CURRENCY, COUNTRY_LOCALE, "TFly",
         DATACASH_VTID, allowedAmounts, paymentTypes, applicationData);
      PaymentData paymentData = new PaymentData(bookingInfo);
      UUID uuid = PaymentStore.getInstance().newPaymentData(paymentData);

      Payment payment = new Payment();
      payment.addPaymentTransaction(new DataCashPaymentTransaction("VISA", new Money(
         BigDecimal.TEN, Currency.getInstance(CURRENCY)), new Money(BigDecimal.ZERO,
            Currency.getInstance(CURRENCY)), new CnpCard("4444333322221111".toCharArray(),
               "test", "01/20", "444".toCharArray(), "cv24eb", "VISA", THREE, THREE),
               getMerchantReference()));
      paymentData.setPayment(payment);
      assertNotNull(cps.getPaymentData(uuid.toString()).size());
   }

   /**
    * Test case to test setAllowedCards.
    */
   /*public void testSetAllowedCards()
   {
      CommonPaymentServiceHandler cps = new CommonPaymentServiceHandler();

      Map<String, Object[]> allowedCards = new HashMap<String, Object[]>();
      allowedCards.put("VISA", new Double[] {0.0, 0.0, 0.0});

      assertTrue(cps.setAllowedCards(token, allowedCards));
   }*/

   /**
    * Test case to test setGuaranteeCardTypes.
    */
   public void testSetGuaranteeCardTypes()
   {
      CommonPaymentServiceHandler cps = new CommonPaymentServiceHandler();

      Object[] guaranteeCardTypes = new String[] {"MasterCard"};

      assertTrue(cps.setGuaranteeCardTypes(token, guaranteeCardTypes));
   }

   /**
    * Gets a unique merchant reference using <code>UUID</code>.
    *
    * @return The generated unique merchant reference.
    */
   private static String getMerchantReference()
   {
      return String.valueOf(UUID.randomUUID().getMostSignificantBits()).replaceAll("-", "");
   }
}
