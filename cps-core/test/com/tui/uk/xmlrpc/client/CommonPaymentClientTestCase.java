/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CommonPaymentClientTestCase.java,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2008-05-14 12:56:12 $
 *
 * $Author: bibin.j@sonata-software.com $
 *
 * $Log: not supported by cvs2svn $
 */
package com.tui.uk.xmlrpc.client;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import org.apache.commons.httpclient.HttpClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

import com.tui.uk.client.CommonPaymentClient;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.EncryptedCard;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.ChequePaymentTransaction;
import com.tui.uk.payment.domain.CnpCard;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PostPaymentGuaranteeTransaction;
import com.tui.uk.xmlrpc.handlers.CommonPaymentServiceHandler;
import com.tui.uk.xmlrpc.handlers.DataCashServiceHandler;

/**
 * This class is responsible to test the xml rpc calls specified in the
 * CommonPaymentClient class.
 *
 * @author bibin.j@sonata-software.com
 */
public class CommonPaymentClientTestCase extends TestCase
{
   /** The server url. */
   private static final String SERVER_URL = "http://localhost:5012/cps/xmlrpc";

   /** The CommonPaymentClient . */
   private static CommonPaymentClient commonPaymentClient;

   /** The web server. */
   private static WebServer server;

   /** The constant for port. */
   private static final int PORT = 5012;

   /** The token. */
   private static String token;

   /** The authCode. */
   private static String authCode = "123";

   /** The client. */
   private static String client = "99080800";

   /** The dataCashReference. */
   private static String dataCashReference = "4600200057734367";
   // CHECKSTYLE:OFF
   /** The amount. */
   private static final Money AMOUNT =  new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
   // CHECKSTYLE:ON

   /** The card charges array. */
   private static final Object[] CARD_CHARGES = new Double[]{2.5, 0.0, 0.0};

   /** The constant THREE. */
   private static final int THREE = 3;

   static
   {
      commonPaymentClient = new CommonPaymentClient(SERVER_URL);
      try
      {
         server = new WebServer(PORT);
         XmlRpcServer xmlRpcServer = server.getXmlRpcServer();
         PropertyHandlerMapping propertyHandlerMapping = new PropertyHandlerMapping();
         propertyHandlerMapping.addHandler("commonPaymentService",
            CommonPaymentServiceHandler.class);
         propertyHandlerMapping.addHandler("datacash", DataCashServiceHandler.class);
         /*
          * URL url = new CommonPaymentClientTestCase().getClass().
          * getResource("XmlRpcServlet.properties");
          * propertyHandlerMapping.load(new CommonPaymentClientTestCase().
          * getClass().getClassLoader(), url);
          */
         xmlRpcServer.setHandlerMapping(propertyHandlerMapping);
         XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
         serverConfig.setEnabledForExtensions(true);
         serverConfig.setContentLengthOptional(false);
         server.start();
         LogWriter.logInfoMessage("XML RPC Server started");
      }
      catch (XmlRpcException exception)
      {
         LogWriter.logInfoMessage("XML RPC Server: " + exception.toString());
      }
      catch (IOException ioe)
      {
         LogWriter.logInfoMessage("XML RPC Server: " + ioe.toString());
      }
   }

   /**
    * Test case to test the method generateToken.
    */
   public void testgenerateToken()
   {
      try
      {
         String[] cpsInfo = commonPaymentClient.generateToken(getBookingComponent());
         token = cpsInfo[0];
         assertNotNull(token);
         PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                            UUID.fromString(token));
         Payment payment = getPayment();
         paymentData.setPayment(payment);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while generating token");
      }
   }

   /**
    * Test case to test the method setAllowedCards.
    */
   /*public void testsetAllowedCards()
   {
      try
      {
         Map<String, Object[]> allowedCards = new HashMap<String, Object[]>();
         allowedCards.put("VISA", CARD_CHARGES);
         commonPaymentClient.setAllowedCards(token, allowedCards);
         assertTrue(true);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while getting NonPaymentData");
      }
   }*/

   /**
    * Test case to test the method getNonPaymentData.
    */
   public void testgetNonPaymentData()
   {
      try
      {
         Map<String, String> nonPaymentData = commonPaymentClient.getNonPaymentData(token);
         assertNotNull(nonPaymentData);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while getting NonPaymentData");
      }
   }

   /**
    * Test case to test method registerError.
    */
   public void testregisterError()
   {
      try
      {
         commonPaymentClient.registerError(token, "");
      }
      catch (XmlRpcException e)
      {
         fail("Exception while registering error");
      }
   }

   /**
    * Test case to test method getEssentialTransactionData.
    */
   public void testgetEssentialTransactionData()
   {
      try
      {
         List<EssentialTransactionData> essentialTransactions =
            commonPaymentClient.getEssentialTransactionData(token);
         assertNotNull(essentialTransactions);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while getting EssentialTransactionData");
      }
   }

   /**
    * Test case to test method getEncryptedCardData.
    */
   public void testgetEncryptedCardData()
   {
      try
      {
         EncryptedCard card = commonPaymentClient.getEncryptedCardData(token);
         assertNotNull(card);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while Encrypting CardData");
      }
   }

   /**
    * Test case to test method authPayment.
    */
   public void testauthPayment()
   {
      try
      {
         testgenerateToken();
         commonPaymentClient.authPayment(token, authCode, client);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while auth Payment");
      }
   }

   /**
    * Test case to test method doPayment.
    */
   public void testdoPayment()
   {
      try
      {
         testgenerateToken();
         commonPaymentClient.doPayment(token, authCode, client);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while do Payment");
      }
   }

   /**
    * Test case to test method fulfill.
    */
  public void testfulfill()
   {
      try
      {
         testgenerateToken();
         commonPaymentClient.authPayment(token, authCode, client);
         commonPaymentClient.fulfill(token, client);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while fulfill Payment");
      }
   }

   /**
    * Test case to test method authRefund.
    */
   public void testauthRefund()
   {
      try
      {
         testgenerateToken();
         commonPaymentClient.authRefund(token, authCode, client);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while auth Refund");
      }
   }

   /**
    * Test case to test method doRefund.
    */
   public void testdoRefund()
   {
      try
      {
         commonPaymentClient.doRefund(token, authCode, client);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while do Refund");
      }
   }

   /**
    * Test case to test method refund.
    */
   public void testrefund()
   {
      try
      {
         testgenerateToken();
         commonPaymentClient.doPayment(token, authCode, client);
         commonPaymentClient.refund(token, client, dataCashReference);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while reverse Refund");
      }
   }

   /**
    * Test case to test method refund.
    */
   public void testRefund()
   {
      try
      {
         testgenerateToken();
         commonPaymentClient.doPayment(token, authCode, client);
         commonPaymentClient.refund(token, client);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while reverse Refund");
      }
   }

   /**
    * Test case to test method refund.
    */
   public void testNotifyBookingCompletion()
   {
      try
      {
         commonPaymentClient.notifyBookingCompletion(token);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while reverse Refund");
      }
   }

   /**
    * Test case to test method getHttpClient.
    */
   public void testGetHttpClient()
   {
      HttpClient httpClient = commonPaymentClient.getHttpClient();
      assertNotNull(httpClient);
   }

   /**
    * Test case to test method cancel.
    */
   public void testcancel()
   {
      try
      {
         commonPaymentClient.cancel(token, client);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while cancelling Payment");
      }
   }

   /**
    * Test case to test method purgeClientPaymentPageData.
    */
   public void testpurgeClientPaymentPageData()
   {
      try
      {
         String[] cpsInfo = commonPaymentClient.generateToken(getBookingComponent());
         token = cpsInfo[0];
         assertNotNull(token);
         PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                            UUID.fromString(token));
         Payment payment = getPayment();
         paymentData.setPayment(payment);
         commonPaymentClient.purgeClientPaymentPageData(token);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while purging ClientPaymentPageData");
      }

   }

   /**
    * Test case to test method purgeSensitiveData.
    */
   public void testpurgeSensitiveData()
   {
      try
      {
         String[] cpsInfo = commonPaymentClient.generateToken(getBookingComponent());
         token = cpsInfo[0];
         assertNotNull(token);
         PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                            UUID.fromString(token));
         Payment payment = getPayment();
         paymentData.setPayment(payment);
         commonPaymentClient.purgeSensitiveData(token);
      }
      catch (XmlRpcException e)
      {
         fail("Exception while purging ClientPaymentPageData");
      }
      finally
      {
         server.shutdown();
      }
   }

   /**
    * Gets the booking component.
    *
    * @return the booking component
    */
   private static BookingComponent getBookingComponent()
   {
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount =   new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      BookingComponent bookingComponent = new BookingComponent(clientApplicationName,
         clientDomainURL, totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      return bookingComponent;
   }

   /**
    * Gets the booking component.
    *
    * @return the booking component
    */
   private static Payment getPayment()
   {
      Payment payment = new Payment();
      String paymentTypeCode = "Dcard";
      Money transactionAmount = new Money(new BigDecimal("100"), Currency.getInstance("GBP"));
      //Money transactionCharge = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      PaymentTransaction transaction = new ChequePaymentTransaction(paymentTypeCode,
         transactionAmount);
      payment.getPaymentTransactions().add(transaction);

      char[] pan =  {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2',
            '1', '1', '1', '1' };
      char[] cv2 = {'4', '4', '4' };
      String nameOncard = "xxxxxxxx";
      String expiryDate = "01/20";
      String postCode = "aa22bb";
      String cardType = "VISA";
      CnpCard card =
         new CnpCard(pan, nameOncard, expiryDate, cv2, postCode, cardType, THREE, THREE);
      transaction =  new PostPaymentGuaranteeTransaction(card, paymentTypeCode,
            transactionAmount);
      payment.getPaymentTransactions().add(transaction);
      CnpCard dataCashCard =
         new CnpCard(pan, nameOncard, expiryDate, cv2, postCode, cardType, THREE, THREE);
      transaction =  new PostPaymentGuaranteeTransaction(card, paymentTypeCode,
            transactionAmount);
      transaction =  new DataCashPaymentTransaction(paymentTypeCode,
            transactionAmount, AMOUNT, dataCashCard, getMerchantReference());
      payment.getPaymentTransactions().add(transaction);
      return payment;
   }


   /**
    * Gets a unique merchant reference using <code>UUID</code>.
    *
    * @return The generated unique merchant reference.
    */
   private static String getMerchantReference()
   {
      return String.valueOf(UUID.randomUUID().getMostSignificantBits()).replaceAll("-", "");
   }

}
