/*
 * Copyright (C)2006 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: TestDataSetup.java$
 *
 * $Revision: $
 *
 * $Date: May 21, 2009$
 *
 * Author: santosh.ks
 *
 *
 * $Log: $
 */
package com.tui.uk.utility;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import com.tui.uk.client.domain.AbtaDetails;
import com.tui.uk.client.domain.Accommodation;
import com.tui.uk.client.domain.AccommodationSummary;
import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.BookingData;
import com.tui.uk.client.domain.BookingFlow;
import com.tui.uk.client.domain.BookingFlowDetails;
import com.tui.uk.client.domain.CardType;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.CruiseSummary;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.DiscountComponent;
import com.tui.uk.client.domain.FlightDetails;
import com.tui.uk.client.domain.FlightSummary;
import com.tui.uk.client.domain.ImportantInformation;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PackageType;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.client.domain.PaymentFlow;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.client.domain.PriceComponent;
import com.tui.uk.client.domain.RefundDetails;
import com.tui.uk.client.domain.Room;
import com.tui.uk.client.domain.ShopDetails;
import com.tui.uk.client.domain.SourceOfBusiness;
import com.tui.uk.client.domain.TermsAndCondition;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.dispatcher.PaymentServlet;

/**
 * The Utility class to get BookingComponent and BookingInfo objects.
 *
 * @author santosh.ks
 */
public final class TestDataSetup
{
   /** Default Constructor. */
   private TestDataSetup()
   {
   }
   /**  Variableservelt Runner Object.**/
   private static ServletRunner sr = null;

   /** Variable for InvocationContext. */
   private static InvocationContext ic = null;

   /**Variable to store passnger identifier.*/
   private static final int PASSENGERID = 3;

   /** The GBP currency. */
   private static final Currency GBP_CURRENCY = Currency.getInstance("GBP");

   /** the United Kingdom constant. */
   private static final String UNITED_KINGDOM = "United Kingdom";

   /** Setting the value for selected payment method. */
   private static final String SELECTED_PAYMENT_METHOD = "Card";

   /** Setting the value for selected card. */
   private static final String SELECTED_CARD = "VISA";

   /** the GB constant. */
   private static final String GB = "GB";

   /** the ten constant. */
   private static final String HUNDRED = "100";

   /** Setting the value for clientDomainURL. */
   private static String clientDomainURL = "test";

   /** The priceComponent List holing the Price Components. */
   private static List<PriceComponent> priceComponents = null;

   /** The duration. */
   private static final int DURATION = 7;

   /** The accommodation inventory system. */
   private static final String ACCOM_INV = "TRACS";

   /** The paymenttypes. * */
   private static String[] paymentTypes = {"DCard"};

   /** Card charge percentage. */
   private static final double CARD_CHARGE_PERCENT = 2.5;

   /** Minimum Card charge. */
   private static final double CARD_CHARGE_MIN = 0.0;

   /** Maximum card charge. */
   private static final double CARD_CHARGE_MAX = 60.0;

   /** The constant THREE. */
   private static final int THREE = 3;

   /** The constant FOUR. */
   private static final int FOUR = 4;

   /** The constant FIVE. */
   private static final int FIVE = 5;

   /** The constant SEVENTY. */
   private static final int SEVENTY = 3;

   /**Maximum age of a child.*/
   private static final Integer MAX_AGE = 7;

   /**Minimum age for child classification.*/
   private static final Integer MINAGE = 2;

   /** The constant SPAIN. */
   private static final String SPAIN = "Spain";

   /** The constant FLIGHT_NUMBER. */
   private static final String NUMBER = "1234";

   /** The Format of Expiry Month and Year. */
   private static final String EXPIRY_MONTH_YEAR = "MM/yy";

   /** The Format of Expiry Year. */
   private static final String EXPIRY_YEAR = "yy";

   /**
    * Create the BookingInfo.
    *
    * @return BookingInfo object.
    */
   public static BookingInfo getBookingInfo()
   {
      BookingInfo bookingInfo = new BookingInfo(getBookingComponent());
      TransactionTrackingData trackingData = new TransactionTrackingData("", "");
      bookingInfo.setTrackingData(trackingData);
      bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal("500"), GBP_CURRENCY));
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal("500.00"), GBP_CURRENCY));
      bookingInfo.setCalculatedCardCharge(new Money(new BigDecimal("12.50"), GBP_CURRENCY));

      return bookingInfo;
   }

 /**
    * Create the BookingInfo.
    *
    * @param totalAmount the total amount.
    *
    * @return BookingInfo object.
    */
   public static BookingInfo getDotNetBookingInfo(double totalAmount)
   {
      Map<String, String> applicationData = new HashMap<String, String>();
      applicationData.put("client_domain_url", "http://localhost:8090");
      applicationData.put("pre_payment_url", "/prePayment.page");
      applicationData.put("post_payment_url", "/confirmation");
      applicationData.put("failure_payment_url", "/error.page");
      applicationData.put("intellitrackerSnippet", "");
      applicationData.put("prevPage", "prePayment.page");

      applicationData.put("country_count", "2");
      applicationData.put("country_code_0", "ES");
      applicationData.put("country_dsc_0", SPAIN);
      applicationData.put("country_code_1", "GB");
      applicationData.put("country_dsc_1", "United Kingdom");
      applicationData.put("accommodationSelected", "true");
      applicationData.put("tAndCUrl", "tAndCUrl");
      applicationData.put("pricing_caption_count", "1");
      applicationData.put("pricing_caption_0", "Thomson discount");
      applicationData.put("pricing_0_count", "1");
      applicationData.put("pricing_0_amt_0", "100");
      applicationData.put("pricing_0_dsc_0", "dsc");
      applicationData.put("pricing_0_qty_0", "10");
      applicationData.put("inBoundFlight_departureDateTime", "11/10/2011");
      applicationData.put("inBoundFlight_arrivalDateTime", "10/10/2011");
      applicationData.put("outBoundFlight_departureDateTime", "10/10/2011");
      applicationData.put("outBoundFlight_arrivalDateTime", "10/10/2011");
      applicationData.put("accommodation_duration", "2");
      applicationData.put("accommodation_checkInDate", "8 OCT 2012");
      applicationData.put("accommodation_checkOutDate", "10 OCT 2012");
      applicationData.put("cruise_duration", "2");
      applicationData.put("passenger_total_count", "1");
      applicationData.put("passenger_1_paxAgeGroup", "ADT");
      applicationData.put("passenger_1_leadPassenger", "true");
      applicationData.put("package_type", "DP");


      Double[] allowedAmounts = new Double[1];
      allowedAmounts[0] = totalAmount;
      BookingInfo bookingInfo = new BookingInfo("GBP", "GB", "TFly", "99080800", allowedAmounts,
         paymentTypes, applicationData);
      Map<String, Object[]> allowedCardsMap = new HashMap<String, Object[]>();
      Object[] cardCharges = {CARD_CHARGE_PERCENT, CARD_CHARGE_MIN, CARD_CHARGE_MAX, SELECTED_CARD};
      Object[] cardCharges1 = {CARD_CHARGE_PERCENT, CARD_CHARGE_MIN, CARD_CHARGE_MAX};

      allowedCardsMap.put(SELECTED_CARD, cardCharges);
      allowedCardsMap.put("MASTERCARD", cardCharges1);

// add passengersummary

      PassengerSummary passengerSummary =
         new PassengerSummary(AgeClassification.ADULT, false, 2);
      PassengerSummary passengerSummary1 =
         new PassengerSummary(AgeClassification.ADULT, true, 1);

      List<PassengerSummary> passengerSummaryList = new ArrayList<PassengerSummary>();
      passengerSummaryList.add(new PassengerSummary(AgeClassification.ADULT, true, THREE));
      passengerSummaryList.add(new PassengerSummary(AgeClassification.ADULT, true, PASSENGERID));
      passengerSummaryList.add(passengerSummary);
      passengerSummaryList.add(passengerSummary1);
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new HashMap<Integer, List<PassengerSummary>>();

      passengerRoomSummary.put(1, passengerSummaryList);
      bookingInfo.getBookingComponent().setPassengerRoomSummary(passengerRoomSummary);

      // add flightSummary
      Calendar cal = Calendar.getInstance();

      FlightDetails inboundFlightDetails =
         new FlightDetails("xyz", cal.getTime(), "abc", cal.getTime(), "fff", "rrrr", "rrr");

      List<FlightDetails> inboundFlightDetailList = new ArrayList<FlightDetails>();
      inboundFlightDetailList.add(inboundFlightDetails);

      FlightSummary flightSummary =
         new FlightSummary(inboundFlightDetailList, inboundFlightDetailList,
            null, Boolean.valueOf(false));
      bookingInfo.getBookingComponent().setFlightSummary(flightSummary);

      AccommodationSummary accommodationSummary =
         new AccommodationSummary(new Accommodation(SPAIN, "Costa Blanca", "", "Rio Park",
            "Full Board"), DURATION, Boolean.TRUE, ACCOM_INV);

      bookingInfo.getBookingComponent().setAccommodationSummary(accommodationSummary);

      bookingInfo.setAllowedCards(allowedCardsMap);
      TransactionTrackingData trackingData = new TransactionTrackingData("", "");
      bookingInfo.setTrackingData(trackingData);
      return bookingInfo;
   }

   /**
    * Create the AccommodationSummary.
    * @param accomInv The specific accomodation inventory system
    * @return AccommodationSummary object.
    */
   public static AccommodationSummary getAccommodationSummary(String accomInv)
   {
      Accommodation accommodation = new Accommodation(SPAIN, "Costa Blanca", "", "Rio Park",
                                       "Full Board");
      accommodation.setAdditionalValue(Boolean.FALSE);
      accommodation.setAccommodationType("VILLA");
      accommodation.setRating("trating1");
      accommodation.setRatingImage("trating1");
      accommodation.setRoomDescription("DOUBLE ROOM");
      accommodation.setShipId("");
      AccommodationSummary accommodationSummary =
         new AccommodationSummary(accommodation, DURATION, Boolean.TRUE, accomInv);
      List<String> accommodationExtras = new ArrayList<String>();
      accommodationExtras.add("DOUBLE BED");
      accommodationSummary.setPrepay(Boolean.FALSE);
      accommodationSummary.setUniqueId("111");
      accommodationSummary.setStartDate(new Date());
      accommodationSummary.setEndDate(new Date());
      accommodationSummary.setCaption("Accommodation");
      accommodationSummary.setAToZUri("/atoztandc.page");
      accommodationSummary.setAdditionalInfo("ADDITIONAL");
      accommodationSummary.setAccommodationDetailsUri("/detailspopup.page");
      accommodationSummary.setAccommodationExtras(accommodationExtras);
      return accommodationSummary;
   }

   /**
    * Create the CruiseSummary.
    *
    * @return CruiseSummary object.
    */
   public static CruiseSummary getCruiseSummary()
   {
      CruiseSummary cruiseSummary = new CruiseSummary(new Accommodation(UNITED_KINGDOM, "London",
         "Arona Gran Hotel", "Thomson Celebration", "FullBoard"), DURATION);
      cruiseSummary.setStartPort("Sharm El Sheikh");
      cruiseSummary.setEndPort("London Gatwick");
      cruiseSummary.setStartDate(new Date());
      cruiseSummary.setEndDate(new Date());
      cruiseSummary.setAToZUri("/atoztandc.page");
      cruiseSummary.getAccommodation();
      cruiseSummary.getAToZUri();
      cruiseSummary.getDuration();
      cruiseSummary.getEndDate();
      cruiseSummary.getEndPort();
      cruiseSummary.getShip().getAccommodationType();
      cruiseSummary.getShip().getAdditionalValue();
      cruiseSummary.getShip().getBoardBasis();
      cruiseSummary.getShip().getCountry();
      cruiseSummary.getShip().getDestination();
      cruiseSummary.getShip().getHotel();
      cruiseSummary.getShip().getRating();
      cruiseSummary.getShip().getRatingImage();
      cruiseSummary.getShip().getResort();
      cruiseSummary.getShip().getRoomDescription();
      cruiseSummary.getShip().getShipId();
      cruiseSummary.getStartDate();
      cruiseSummary.getStartPort();
      return cruiseSummary;
   }

   /**
    * Create the FlightSummary.
    *
    * @return FlightSummary object.
    */
   public static FlightSummary getFlightSummary()
   {
      List<FlightDetails> outboundFlight = new ArrayList<FlightDetails>();
      List<FlightDetails> inboundFlight = new ArrayList<FlightDetails>();
      List<String> flightExtras = new ArrayList<String>();
      FlightSummary flightSummary = new FlightSummary(outboundFlight, inboundFlight,
         "TRACS", Boolean.TRUE);
      flightExtras.add("MEAL");
      flightSummary.setFlightExtras(flightExtras);
      flightSummary.getFlightExtras();
      flightSummary.getFlightSelected();
      flightSummary.getFlightSupplierSystem();
      flightSummary.getInboundFlight();
      FlightDetails flightdDetails = new FlightDetails("London Gatwick", new Date(),
         "Sharm El Sheikh", new Date(), "Thomson", "LGW", NUMBER);
      flightdDetails.setArrivalAirportCode("LGW");
      flightdDetails.setDepartureAirportCode("CVY");
      flightdDetails.setMarketingAirlineCode("TH");
      flightdDetails.setOperatingAirlineShortName("Thomson");
      flightdDetails.getArrivalAirportCode();
      flightdDetails.getArrivalAirportName();
      flightdDetails.getArrivalDateTime();
      flightdDetails.getCarrier();
      flightdDetails.getDepartureAirportCode();
      flightdDetails.getDepartureAirportName();
      flightdDetails.getDepartureDateTime();
      flightdDetails.getFlightNumber();
      flightdDetails.getMarketingAirlineCode();
      flightdDetails.getOperatingAirlineCode();
      flightdDetails.getOperatingAirlineShortName();
      outboundFlight.add(flightdDetails);
      inboundFlight.add(flightdDetails);
      return flightSummary;
   }

   /**
    * Create the PassengerSummary.
    *
    * @return PassengerSummary object.
    */
   public static PassengerSummary getPassengerSummary()
   {
      PassengerSummary passen = new PassengerSummary(AgeClassification.ADULT, true, 1);
      passen.setAge(DURATION);
      passen.setForeName("ForeName");
      passen.setLabel("ADULT");
      passen.setLastName("LastName");
      passen.setMiddleName("MiddleName");
      passen.setTitle("Mr:");
      return passen;
   }

   /**
    * Create the PassengerSummary for a Child Passenger.
    *
    * @return PassengerSummary object.
    */
   public static PassengerSummary getPassengerSummaryChild()
   {
      PassengerSummary passen = new PassengerSummary(AgeClassification.CHILD, true, 2);
      passen.setAge(DURATION);
      passen.setForeName("ForeName");
      passen.setLabel("CHILD");
      passen.setLastName("LastName");
      passen.setMiddleName("MiddleName");
      passen.setTitle("Mr:");
      passen.setMaxAge(MAX_AGE);
      passen.setMinAge(MINAGE);
      return passen;
   }

   /**
    * Create the PassengerSummary for an Infant.
    *
    * @return PassengerSummary object.
    */
   public static PassengerSummary getPassengerSummaryInfant()
   {
      PassengerSummary passen = new PassengerSummary(AgeClassification.INFANT, true, 2);
      passen.setForeName("ForeName");
      passen.setLabel("CHILD");
      passen.setLastName("LastName");
      passen.setMiddleName("MiddleName");
      passen.setTitle("Mr:");
      passen.setMaxAge(MAX_AGE);
      passen.setMinAge(MINAGE);
      passen.getAgeClassification();
      passen.getAge();
      passen.getForeName();
      passen.getIdentifier();
      passen.getLabel();
      passen.getLastName();
      passen.getLeadPassenger();
      passen.getMaxAge();
      passen.getMiddleName();
      passen.getMinAge();
      passen.getTitle();
      return passen;
   }


   /**
    * Create the Rooms.
    *
    * @return FlightSummary object.
    */
   public static List<Room> getRooms()
   {
      List<Room> rooms = new ArrayList<Room>();
      Room room = new Room();
      room.setRoomDescription("Red Sea");
      room.setRoomId(DURATION);
      room.getRoomDescription();
      room.getRoomId();
      return rooms;
   }

   /**
    * Create the BookingInfo.
    *
    * @param packageType the PackageType object.
    *
    * @return BookingInfo object.
    */
   public static BookingInfo getBookingInfo(PackageType packageType)
   {
      BookingInfo bookingInfo = getBookingInfo();
      bookingInfo.getBookingComponent().setPackageType(packageType);
      return bookingInfo;
   }

   /**
    * Create the ImportantInformation.
    *
    * @return ImportantInformation object.
    */
   public static ImportantInformation getImportantInformation()
   {
      HashMap<String, String> impMap = new HashMap<String, String>();
      List<String> impinfo = new ArrayList<String>();
      impMap.put("CAR_HIRE", "CAR HIRE");
      impinfo.add("CHILD BED SHARING ISNOT PERMITTED");
      ImportantInformation imp = new ImportantInformation(impinfo);
      imp.setDescription(impMap);
      imp.getDescription().get("CAR_HIRE");
      imp.getErrata();
      return imp;
   }

   /**
    * Create the BookingData.
    *
    * @return BookingData object.
    */
   public static BookingData getBookingData()
   {
      BookingData bookingData = new BookingData("216496", "Success", new Date());
      bookingData.setPassengerDetails("passengerDetails");
      bookingData.setLeadPassengerName("leadPAssenger");
      bookingData.setReaccreditationAllowed(Boolean.FALSE);
      bookingData.setFileBriefID(NUMBER);
      bookingData.setFileBriefStatus("Success");
      bookingData.getBookingDate();
      bookingData.getBookingReference();
      bookingData.getBookingStatus();
      bookingData.getFileBriefID();
      bookingData.getFileBriefStatus();
      bookingData.getFileBriefStatus();
      bookingData.getLeadPassengerName();
      bookingData.getPassengerDetails();
      bookingData.getReaccreditationAllowed();
      bookingData.setBookingFlowDetails(new BookingFlowDetails(BookingFlow.OLBP,
         PaymentFlow.PAYMENT));
      bookingData.getBookingFlowDetails();

      return bookingData;
   }

   /**
    * Create the ShopDetails.
    *
    * @return ShopDetails object.
    */
   public static ShopDetails getShopDetails()
   {
      ShopDetails shopDetails = new ShopDetails("lucy", "0015");
      shopDetails.setRetailBrand("BYO");
      shopDetails.getShopId();
      shopDetails.getShopName();
      shopDetails.getRetailBrand();
      return shopDetails;
   }

   /**
    * Create the DepositComponent.
    *
    * @return DepositComponent object.
    */
   public static List<DepositComponent> getDepositComponent()
   {
      List<DepositComponent> depositList = new ArrayList<DepositComponent>();
      DepositComponent depositComponent =
         new DepositComponent("FullBalance", new Money(new BigDecimal("125"), GBP_CURRENCY),
            Calendar.getInstance().getTime(), new Money(new BigDecimal(HUNDRED), GBP_CURRENCY));
      DepositComponent depositComponent1 =
         new DepositComponent("partialDeposit", new Money(new BigDecimal("60"), GBP_CURRENCY),
            Calendar.getInstance().getTime(), new Money(new BigDecimal(HUNDRED), GBP_CURRENCY));
      depositList.add(depositComponent);
      depositList.add(depositComponent1);
      depositComponent.getDepositDueDate();
      depositComponent.getDepositType();
      depositComponent.getOutstandingBalance();
      depositComponent.getDepositAmount();
      depositComponent.setDepositTypeDescription("depositTypeDescription");
      depositComponent.getDepositTypeDescription();
      return depositList;
   }

   /**
    * Create the DiscountComponent.
    *
    * @return DiscountComponent object.
    */
   public static List<DiscountComponent> getDiscountComponent()
   {
      DiscountComponent discountComponent =
         new DiscountComponent(DispatcherConstants.PRICE_BEAT, new Money(new BigDecimal("12.60"),
            GBP_CURRENCY), true, false);
      List<DiscountComponent> discountComponents = new ArrayList<DiscountComponent>();
      discountComponents.add(discountComponent);
      discountComponent.getApplicableForFlight();
      discountComponent.getApplicableForHotel();
      discountComponent.getDiscountType();
      discountComponent.getMaxDiscountAmount();
      return discountComponents;
   }

   /**
    * Create the NonPaymentData.
    *
    * @return Map of nonPaymentData.
    */
   public static Map<String, String> getNonPaymentData()
   {
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      nonPaymentData.put(BookingConstants.DEPOSIT_TYPE, "FullBalance");
      nonPaymentData.put("promotionalCode", "00002708");
      nonPaymentData.put("isPromoCodeApplied", "false");
      nonPaymentData.get(BookingConstants.DEPOSIT_TYPE);
      nonPaymentData.get("promotionalCode");
      nonPaymentData.get("isPromoCodeApplied");
      return nonPaymentData;
   }

   /**
    * Create the BookingComponent.
    *
    * @return BookingComponent object.
    */
   public static BookingComponent getBookingComponent()
   {
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      Money totalAmount = new Money(new BigDecimal(HUNDRED), GBP_CURRENCY);

      List<RefundDetails> refundDetails = new ArrayList<RefundDetails>();
      List<PassengerSummary> passengerSummaryList = new ArrayList<PassengerSummary>();
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new HashMap<Integer, List<PassengerSummary>>();
      Map<String, String> countryMap = new HashMap<String, String>();


      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);


      // add passengersummary

      PassengerSummary passengerSummary =
         new PassengerSummary(AgeClassification.ADULT, false, 2);
      passengerSummary.setAge(2);
      PassengerSummary passengerSummary1 =
         new PassengerSummary(AgeClassification.ADULT, true, 1);

      passengerSummaryList.add(new PassengerSummary(AgeClassification.ADULT, true, THREE));
      passengerSummaryList.add(new PassengerSummary(AgeClassification.ADULT, true, PASSENGERID));
      passengerSummaryList.add(passengerSummary);
      passengerSummaryList.add(passengerSummary1);
      passengerRoomSummary.put(1, passengerSummaryList);
      bookingComponent.setPassengerRoomSummary(passengerRoomSummary);
      passengerRoomSummary.put(1, passengerSummaryList);
      passengerSummaryList.add(getPassengerSummary());
      passengerSummaryList.add(getPassengerSummaryChild());
      passengerSummaryList.add(getPassengerSummaryInfant());

      refundDetails.add(new RefundDetails("432093452312", totalAmount, Calendar.getInstance()
         .getTime()));
      countryMap.put(GB, UNITED_KINGDOM);
      bookingComponent.setCountryMap(countryMap);
      bookingComponent.setRefundDetails(refundDetails);
      bookingComponent.setPayableAmount(new Money(new BigDecimal(HUNDRED), GBP_CURRENCY));

      bookingComponent.setTracsBookMinusId(NUMBER);
      bookingComponent.setAbtaDetails(new AbtaDetails("Q7387", "EXV", "GREENFILED"));
      bookingComponent.setTermsAndCondition(new TermsAndCondition(
         ClientApplication.ThomsonBuildYourOwn, "http://localhosthost",
            "This is test terms and conditions for the HOPLA_THM"));

      getPriceComponents(bookingComponent);
      getPaymentTypes(bookingComponent);
      bookingComponent.setDepositComponents(getDepositComponent());
      bookingComponent.setDiscountComponents(getDiscountComponent());
      bookingComponent.setAccommodationSummary(getAccommodationSummary(ACCOM_INV));
      bookingComponent.setCruiseSummary(getCruiseSummary());
      bookingComponent.setFlightSummary(getFlightSummary());
      bookingComponent.setCompanyCode("FC");
      bookingComponent.setShopDetails(getShopDetails());
      bookingComponent.setSearchType("Lates");
      bookingComponent.setSourceOfBusiness(getSourceOfBusiness());
      bookingComponent.setImportantInformation(getImportantInformation());
      bookingComponent.setRooms(getRooms());
      bookingComponent.setBookingData(getBookingData());
      bookingComponent.setPaymentGatewayVirtualTerminalId("99639700");
      PackageType packageType = PackageType.PreDefinedPackage;
      bookingComponent.setPackageType(packageType);
      bookingComponent.setPaymentFailureURL("http://failure.page");
      bookingComponent.setPaymentSuccessUrl("http://success.page");
      getBookingComponentValues(bookingComponent);

      return bookingComponent;
   }

   /**
    * Get the source of business.
    * @return sob the source of business.
    */
   public static SourceOfBusiness getSourceOfBusiness()
   {
      Map<String, String> sourceofbusiness = new HashMap<String, String>();
      sourceofbusiness.put("Web", "Thomsonwebsite");
      SourceOfBusiness sob = new SourceOfBusiness(sourceofbusiness);
      sob.getSelectedSobCode();
      sob.getSourceOfBusiness();
      return sob;
   }

   /**
    * This method is used to populate the list of price Components with test data.
    *
    * @param bookingComponent bookingComponent object
    */
   public static void getPaymentTypes(BookingComponent bookingComponent)
   {
      Map<String, List<PaymentType>> paymentType = new HashMap<String, List<PaymentType>>();
      String cardChargekey = "Mastercard";
      Map<String, BigDecimal> cardChargesMap = new HashMap<String, BigDecimal>();
      BigDecimal sampleBigDec = new BigDecimal("2.5");
      PaymentType type =
         new PaymentType(SELECTED_CARD, "MasterCard Express", SELECTED_PAYMENT_METHOD);
      CardType cardType = new CardType(THREE, THREE);
      cardType.setIsIssueNumberRequired(Boolean.FALSE);
      cardType.setIsStartDateRequired(Boolean.FALSE);
      type.setCardType(cardType);
      List<PaymentType> payList = new ArrayList<PaymentType>();
      payList.add(type);
      type = new PaymentType(SELECTED_CARD, "VISA Express", SELECTED_PAYMENT_METHOD);
      type.getPaymentDescription();
      type.getPaymentMethod();
      CardType cardType1 = new CardType(THREE);
      cardType.setIsIssueNumberRequired(Boolean.FALSE);
      cardType.setIsStartDateRequired(Boolean.FALSE);
      cardType.getIsIssueNumberRequired();
      cardType.getIsStartDateRequired();
      type.setCardType(cardType1);
      payList.add(type);
      paymentType.put("CNP", payList);
      bookingComponent.setPaymentType(paymentType);
      bookingComponent.setMaxCardChargePercent(new BigDecimal("1000"));
      bookingComponent.setMaxCardCharge(new BigDecimal("1000"));
      cardChargesMap.put(cardChargekey, sampleBigDec);
      cardChargesMap.put("VISA", sampleBigDec);
      cardChargesMap.put("AMERICAN_EXPRESS", sampleBigDec);

      bookingComponent.setCardChargesMap(cardChargesMap);
   }

   /**
    * This method is used to populate the list of price Components with test data.
    *
    * @param bookingComponent bookingComponent object
    */
   public static void getPriceComponents(BookingComponent bookingComponent)
   {
      priceComponents = new ArrayList<PriceComponent>();
      PriceComponent priceComponent =
         new PriceComponent("Thomson discount", new Money(new BigDecimal("-50"), GBP_CURRENCY));
      priceComponent.setDiscount(true);
      PriceComponent priceComponent1 =
         new PriceComponent("Flightmeals", new Money(new BigDecimal("14"), GBP_CURRENCY));
      priceComponent1.setQuantity(2);
      priceComponents.add(new PriceComponent("HotelCost", new Money(new BigDecimal("300"),
         GBP_CURRENCY)));
      priceComponents.add(priceComponent);
      priceComponents.add(priceComponent1);
      bookingComponent.setPriceComponents(priceComponents);
      priceComponents = new ArrayList<PriceComponent>();
      priceComponent.getAmount();
      priceComponent.getDiscount();
      priceComponent.getItemDescription();
      priceComponent.getQuantity();
   }

   /**
    * Create the BookingComponent.
    *
    * @param packageType the PackageType object.
    *
    * @return BookingComponent object.
    */
   public static BookingComponent getBookingComponent(PackageType packageType)
   {
      getBookingComponent().setPackageType(packageType);
      return getBookingComponent();
   }

   /**
    * This method is used to get the webRequest object to execute a testservlet.
    *
    * @return WebRequest WebRequest.
    */
   public static WebRequest getWebRequest()
   {
      sr = new ServletRunner();
      String servletUrl = "http://test.meterware.com/testServlet";
      sr.registerServlet("testServlet", PaymentServlet.class.getName());
      WebRequest request = new PostMethodWebRequest(servletUrl);
      return request;
   }

   /**
    * This method is used to set the Invocation Context for the given webRequest.
    *
    * @param request WebRequest.
    * @return InvocationContext InvocationContext object.
    * @throws IOException IOException.
    */
   public static InvocationContext setInvocationTarget(WebRequest request) throws IOException
   {
      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);
      return ic;
   }

   /**
    * Create the BookingComponent.
    *
    * @return BookingComponent object.
    */
   public static BookingComponent getBookingComponent1()
   {
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      Money totalAmount = new Money(new BigDecimal(HUNDRED), GBP_CURRENCY);
      Map<String, String> sourceofbusiness = new HashMap<String, String>();

      List<RefundDetails> refundDetails = new ArrayList<RefundDetails>();

      Map<String, String> countryMap = new HashMap<String, String>();

      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, "http", totalAmount);
      SourceOfBusiness sob = new SourceOfBusiness(sourceofbusiness, "DEFAULT");

      // adding passengersummary
      bookingComponent.setPassengerRoomSummary(getPassengerRoomSummary());

      // adding duration.
      AccommodationSummary accommodationSummary =
         new AccommodationSummary(new Accommodation(SPAIN, "Costa Blanca", "", "Rio Park",
            "Full Board"), DURATION, Boolean.TRUE, ACCOM_INV);
      bookingComponent.setAccommodationSummary(accommodationSummary);

      // adding flightSummary
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, DURATION);

      // arrival date, adding 7 in current date.
      Calendar arrivalDateCalendar = Calendar.getInstance();
      arrivalDateCalendar.add(Calendar.DATE, DURATION);

      arrivalDateCalendar.get(Calendar.DAY_OF_MONTH);
      arrivalDateCalendar.get(Calendar.MONTH);
      arrivalDateCalendar.get(Calendar.YEAR);
      bookingComponent.setFlightSummary(getFlightSummary());

      sourceofbusiness.put("Web", "Thomsonwebsite");
      refundDetails.add(new RefundDetails("432093452312", totalAmount, Calendar.getInstance()
         .getTime()));
      countryMap.put(GB, UNITED_KINGDOM);
      bookingComponent.setCountryMap(countryMap);
      bookingComponent.setRefundDetails(refundDetails);
      bookingComponent.setPayableAmount(new Money(new BigDecimal(HUNDRED), GBP_CURRENCY));

      bookingComponent.setTracsBookMinusId(NUMBER);
      bookingComponent.setAbtaDetails(new AbtaDetails("Q7387", "EXV", "GREENFILED"));
      bookingComponent.setTermsAndCondition(new TermsAndCondition("Hopla",
         "This is test terms and conditions for the HOPLA_THM"));

      getPriceComponents(bookingComponent);
      getPaymentTypes(bookingComponent);
      bookingComponent.setDepositComponents(getDepositComponent());
      bookingComponent.setDiscountComponents(getDiscountComponent());
      // bookingComponent.setAccommodationSummary(getAccommodationSummary(ACCOM_INV));
      bookingComponent.setCruiseSummary(getCruiseSummary());
      // bookingComponent.setFlightSummary(getFlightSummary());
      bookingComponent.setCompanyCode("FC");
      bookingComponent.setShopDetails(getShopDetails());
      bookingComponent.setSearchType("Lates");
      bookingComponent.setSourceOfBusiness(sob);
      bookingComponent.setPaymentGatewayVirtualTerminalId("99639700");
      bookingComponent.setIntelliTrackSnippet("ExcessSel%3DR1-A2/C0/I0", "prevPage");
      bookingComponent.setCallType("DEFAULT");
      bookingComponent.setDatacashEnabled(false);
      Map<String, List<String>> extraFacilities = new HashMap<String, List<String>>();
      List<String> extraList = new ArrayList<String>();
      extraList.add("child meal");
      extraFacilities.put("extra", extraList);
      bookingComponent.setExtraFacilities(extraFacilities);
      bookingComponent.getClientDomainURL();
      return bookingComponent;
   }

   /**
    * Create the passengerRoomSummary map.
    *
    * @return passengerRoomSummary object.
    */
   private static Map<Integer, List<PassengerSummary>> getPassengerRoomSummary()
   {
      List<PassengerSummary> passengerSummaryList = new ArrayList<PassengerSummary>();
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new HashMap<Integer, List<PassengerSummary>>();

      PassengerSummary passengerSummary = new PassengerSummary(AgeClassification.CHILD, false, 2);
      passengerSummary.setAge(THREE);
      passengerSummary.setMaxAge(SEVENTY);
      passengerSummary.setMinAge(2);

      PassengerSummary passengerSummary1 = new PassengerSummary(AgeClassification.INFANT, true, 1);
      passengerSummary1.setMaxAge(2);

      PassengerSummary passengerSummary2 =
         new PassengerSummary(AgeClassification.SENIOR, true, FIVE);

      PassengerSummary passengerSummary3 =
         new PassengerSummary(AgeClassification.SENIOR2, true, FOUR);

      PassengerSummary ps = new PassengerSummary(AgeClassification.ADULT, true, PASSENGERID);
      ps.setForeName("foreName");
      passengerSummaryList.add(ps);
      passengerSummaryList.add(passengerSummary);
      passengerSummaryList.add(passengerSummary1);
      passengerSummaryList.add(passengerSummary2);
      passengerSummaryList.add(passengerSummary3);
      passengerRoomSummary.put(1, passengerSummaryList);
      return passengerRoomSummary;
   }

   /**
    * Get all booking component values.
    * @param bookingComponent the bookingComponent.
    */
   private static void getBookingComponentValues(BookingComponent bookingComponent)
   {
      bookingComponent.getAbtaDetails();
      bookingComponent.getAccommodationSummary();
      bookingComponent.getBookingData();
      bookingComponent.getCallType();
      bookingComponent.getCardChargesMap();
      bookingComponent.getClientApplication();
      bookingComponent.getClientDomainURL();
      bookingComponent.getCompanyCode();
      bookingComponent.getCountryMap();
      bookingComponent.getCruiseSummary();
      bookingComponent.getDatacashEnabled();
      bookingComponent.getDepositComponents();
      bookingComponent.getDiscountComponents();
      bookingComponent.getErrorMessage();
      bookingComponent.getExtraFacilities();
      bookingComponent.getFlightSummary();
      bookingComponent.getImportantInformation();
      bookingComponent.getIntelliTrackSnippet();
      bookingComponent.getMaxCardCharge();
      bookingComponent.getMaxCardChargePercent();
      bookingComponent.getNonPaymentData();
      bookingComponent.getNoOfTransactions();
      bookingComponent.getPackageType();
      bookingComponent.getPassengerRoomSummary();
      bookingComponent.getPayableAmount();
      bookingComponent.getPaymentFailureURL();
      bookingComponent.getPaymentGatewayVirtualTerminalId();
      bookingComponent.getPaymentSuccessUrl();
      bookingComponent.getPaymentType();
      bookingComponent.getPrePaymentUrl();
      bookingComponent.getPriceComponents();
      bookingComponent.getRefundDetails();
      bookingComponent.getRooms();
      bookingComponent.getSearchType();
      bookingComponent.getShopDetails();
      bookingComponent.getSourceOfBusiness();
      bookingComponent.getTermsAndCondition();
      bookingComponent.getTermsAndCondition().getDomain();
      bookingComponent.getTermsAndCondition().getpayInfoText();
      bookingComponent.getTermsAndCondition().getRelativeTAndCUrl();
      bookingComponent.getTermsAndCondition().getTcClassificationKey();
      bookingComponent.getTotalAmount();
      bookingComponent.getTracsBookMinusId();
   }

   /**
    * Method to get expiry month and year.
    */
   public static String getExpiryMonthAndYear()
   {
	  Date date = new Date();
	  SimpleDateFormat simpledateformat = new SimpleDateFormat(EXPIRY_MONTH_YEAR);
	  return simpledateformat.format(date) ;
   }

   /**
    * Method to get only expiry year.
    */
   public static String getExpiryYear()
   {
	  Date date = new Date();
	  SimpleDateFormat simpledateformat = new SimpleDateFormat(EXPIRY_YEAR);
	  return simpledateformat.format(date) ;
   }
}
