/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CommonPaymentClientTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2010-08-05 $
 *
 * $Author: roopesh.s $
 *
 * $Log: $
 */
package com.tui.uk.client;

import junit.framework.TestCase;

import org.apache.xmlrpc.XmlRpcException;

import com.tui.uk.utility.TestDataSetup;

/**
 *
 * TestCase for CommonPaymentClient Class.
 *
 */
public class CommonPaymentClientTestCase extends TestCase
{
   /** The tomcatInstance. */
   private String tomcatInstance = "tomcat=A12";

   /** The xmlRpcURL. */
   private String xmlRpcURL = "http://10.174.0.209:9070/cps/xmlrpc";

   /** The cpsInfo. */
   private String[] cpsInfo = null;

   /** The CommonPaymentClient Object . */
   private CommonPaymentClient xmlRpcHost = null;

   /**
    * Setting initial values.
    *
    * @throws Exception the exception.
    * */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();

      xmlRpcURL = xmlRpcURL + "?" + tomcatInstance;
      xmlRpcHost = new CommonPaymentClient(xmlRpcURL);
   }

   /**
    * Testing generateToken method.
    * */
   public void testGenerateToken()
   {

      try
      {
         cpsInfo = xmlRpcHost.generateToken(TestDataSetup.getBookingComponent());
      }
      catch (XmlRpcException e)
      {
         assertTrue(true);
      }
   }

   /**
    * Testing generateToken method, fail condition.
    * */
   public void testGenerateToken1()
   {
      try
      {
         String xmlRpcURL1 = "xmlrpc";

         xmlRpcURL1 = xmlRpcURL1 + "?" + tomcatInstance;
         CommonPaymentClient xmlRpcHost1 = new CommonPaymentClient(xmlRpcURL1);
         cpsInfo = xmlRpcHost1.generateToken(TestDataSetup.getBookingComponent());
      }
      catch (Exception e)
      {
         assertTrue(true);
      }
   }

   /**
    * Testing getHttpClient method.
    * */
   public void testGetHttpClient()
   {
      assertNotNull(xmlRpcHost.getHttpClient());
   }

}
