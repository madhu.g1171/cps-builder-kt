/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingConstantsTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2010-08-02 $
 *
 * $Author: roopesh.s $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import junit.framework.TestCase;

/**
 * This Class tests the BookingConstants class.
 */
public class BookingConstantsTestCase extends TestCase
{
   /**
    * To test the constants.
    **/
   public void testBookingConstants()
   {
      assertEquals(BookingConstants.IS_DATACASH_SELECTED, "isDataCash");
      assertEquals(BookingConstants.HTTP_PROTOCOL, "http://");
      assertEquals(BookingConstants.PARTIAL_AMOUNT, "partialPaymentAmount");
      assertEquals(BookingConstants.SELECTED_DEPOSIT_TYPE, "selectedDepositType");
      assertEquals(BookingConstants.DEPOSIT_TYPE, "depositType");
      assertEquals(BookingConstants.LOW_DEPOSIT, "lowDeposit");
      assertEquals(BookingConstants.PARTIAL_DEPOSIT, "partialDeposit");
      assertEquals(BookingConstants.MINIMUM_AMOUNT, "MinimumAmount");
      assertEquals(BookingConstants.DEPOSIT, "deposit");
      assertEquals(BookingConstants.FULL_COST, "fullCost");
      assertEquals(BookingConstants.SELECTED_DCR, "datacashReference");
      assertEquals(BookingConstants.HTTPS_PROTOCOL, "https://");
      assertEquals(BookingConstants.SUMMARYPANEL_DEPOSIT_AMOUNT, "summaryPanelDepositAmount");
      assertEquals(BookingConstants.INVENTORY_BOOKING_REFERENCE, "inventory_booking_ref");
      assertEquals(BookingConstants.ACCOM_DETAILS_URL, "accomDetailsUrl");
      assertEquals(BookingConstants.FLIGHT_DETAILS_URL, "flightDetailsUrl");
      assertEquals(BookingConstants.TRANSACTION_AMOUNT, "transactionAmount");
      assertEquals(BookingConstants.PRICE_COMPONENTS, "priceComponents");
      assertEquals(BookingConstants.PRICE_BREAKDOWN, "priceBreakDown");
      assertEquals(BookingConstants.STREET_ADDRESS1, "street_address1");
      assertEquals(BookingConstants.STREET_ADDRESS2, "street_address2");
      assertEquals(BookingConstants.STREET_ADDRESS3, "street_address3");
      assertEquals(BookingConstants.STREET_ADDRESS4, "street_address4");
      assertEquals(BookingConstants.SELECTED_COUNTRY, "selectedCountry");
      assertEquals(BookingConstants.SELECTED_COUNTRY_CODE, "selectedCountryCode");
      assertEquals(BookingConstants.CARD_BILLING_POSTCODE, "cardBillingPostcode");
      assertEquals(BookingConstants.MASK_CHARACTER, "maskCharacter");
      assertEquals(BookingConstants.CNP, "cnp");
      assertEquals(BookingConstants.ECOMM, "ecomm");
   }

}
