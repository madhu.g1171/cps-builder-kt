/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: IntelliTrackerSnippetTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-07-17 $
 *
 * $Author: thomas.pm $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import junit.framework.TestCase;

/**
 * This Class tests the IntelliTrackerSnippet class.
 */
public class IntelliTrackerSnippetTestCase extends TestCase
{
   /** String pqry. */
   private String validPqry = "ExcessSel%3DR1-A2/C0/I0";

   /** String in valid pqry. */
   private String inValidPqry = "pqry=\"ExcessSel%*=";

   /** String prevPage. */
   private String prevPage = "prevPage";


   /**
    * Test for valid pqry .
    */
   public void testValidPqry()
   {

      try
      {
         new IntelliTrackerSnippet(validPqry, prevPage);
      }
      catch (IllegalArgumentException dse)
      {
         fail("Exception while validating the pqry");
      }
   }

   /**
    * Test for in valid pqry .
    */
   public void testInValidPqry()
   {

      try
      {
         new IntelliTrackerSnippet(inValidPqry, prevPage);
         fail("Exception while validating the pqry");
      }
      catch (IllegalArgumentException dse)
      {
         assertTrue("Testcase was successfull.", true);
      }
   }

   /**
    * Test for valid IntelliTrackerString .
    */
   public void testValidIntelliTrackerString()
   {

      try
      {
         String intel = new IntelliTrackerSnippet(validPqry, prevPage).getIntelliTrackerString();
         assertEquals(intel, validPqry);
      }
      catch (IllegalArgumentException dse)
      {
         fail("Exception while validating the intelli tracker string");
      }
   }

   /**
    * Test for valid previous page .
    */
   public void testValidPreviousPage()
   {

      try
      {
         String page = new IntelliTrackerSnippet(validPqry, prevPage).getPreviousPage();
         assertEquals(page, "prevPage");
      }
      catch (IllegalArgumentException dse)
      {
         fail("Exception while validating the previous page");
      }
   }

}
