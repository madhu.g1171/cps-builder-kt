/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PassengersDataPanelTestCase.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-07-03 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.client.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

/**
 * This class having test for PassengersDataPanelTestCase.
 *
 * @author roopesh.s
 *
 */
public class PassengersDataPanelTestCase extends TestCase
{
   /**
    * Test for valid pattern.
    */
   public void testPattern()
   {
      List<String> validationList = new ArrayList<String>();
      validationList.add(PassengersDataPanel.FORE_NAME.getKey());
      PassengersDataPanel.FORE_NAME.getPattern();
      Map<String, String> patterns = PassengersDataPanel.getAllPatterns(validationList);
      assertEquals("^[-a-zA-Z&@ \"\',\\.\\x27]*$", patterns.get(PassengersDataPanel.FORE_NAME
         .getKey()));
   }
}
