/*
* Copyright (C)2006 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   PaymentFlowTestCase.java$
*
* $Revision:   $
*
* $Date:   Dec 22, 2009$
*
* Author: veena.bn
*
*
* $Log:   $
*/
package com.tui.uk.client.domain;

import com.tui.uk.log.LogWriter;

import junit.framework.TestCase;

/**
 * Testcase for PaymentFlow enumeration.
 * @author veena.bn
 *
 */
public class PaymentFlowTestCase extends TestCase
{
   /**
    * Test for valid paymentFlow.
    */
   public void testValidPaymentFlow()
   {
      LogWriter.logMethodStart("testValidPaymentFlow");
      try
      {
         PaymentFlow.findByPaymentFlowCode("REFUND");
      }
      catch (IllegalArgumentException dse)
      {
         fail("Unexpected exception while validating the paymentFlow" + dse.getMessage());
      }
      LogWriter.logMethodEnd("testValidPaymentFlow");
   }

   /**
    * Test for invalid bookingFlow.
    */
   public void testInvalidPaymentFlow()
   {
      LogWriter.logMethodStart("testInvalidPaymentFlow");
      try
      {
         PaymentFlow.findByPaymentFlowCode("ABC");
      }
      catch (IllegalArgumentException dse)
      {
         assertTrue("Testcase was successful.", true);
      }
      LogWriter.logMethodEnd("testInvalidPaymentFlow");
   }
}
