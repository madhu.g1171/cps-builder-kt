/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: MoneyTestCase.java,v $
 *
 * $Revision: $
 *
 * $Date: 20-7 -17$
 *
 * $Author: Vinodha.s$
 *
 * $Log: $
 *
 *
 */
package com.tui.uk.client.domain;

import java.math.BigDecimal;
import java.util.Currency;
import com.tui.uk.log.LogWriter;
import junit.framework.TestCase;

/**
 * Test case for Money class.
 *
 */
public class MoneyTestCase extends TestCase
{
   /** The Constant GBP. */
   private static final String GBP = "GBP";

   /** The currency GBP. */
   private static final Currency CURRENCY = Currency.getInstance(GBP);

   /** The Constant GBP. */
   private static final String USD = "USD";

   /** The currency in USD. */
   private static final Currency CURRENCYUSD = Currency.getInstance(USD);

   /** The amount. */
   private static final BigDecimal AMOUNT = BigDecimal.valueOf(100.00);

   /**
    * Test for valid currency .
    */
   public void testValidCurrency()
   {
      try
      {
         Object[] params = new Object[]{AMOUNT, CURRENCY};
         LogWriter.logMethodStart("testValidCurrency", params);
         LogWriter.logDebugMessage("testValidCurrencybyAddition");
         Money money = new Money(AMOUNT, CURRENCY);
         Money tobeadded = new Money(AMOUNT, CURRENCY);
         money.add(tobeadded);
         LogWriter.logMethodEnd("testValidCurrency");
      }
      // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         fail("Exception while adding money");
      }
      // CHECKSTYLE:ON
   }

   /**
    * Test for Invalid currency .
    */
   public void testInvalidCurrency()
   {
      try
      {
         Object[] params = new Object[]{AMOUNT, CURRENCY};
         LogWriter.logMethodStart("test Invalid Currency", params);
         Money money = new Money(AMOUNT, CURRENCY);
         Money tobeadded = new Money(AMOUNT, CURRENCYUSD);
         money.add(tobeadded);
         LogWriter.logMethodEnd("testInvalidCurrency");
         LogWriter.logErrorMessage("exception while validating the currency");
         fail("Exception while adding money");
      }
      // CHECKSTYLE:OFF
      catch (RuntimeException pve)
      {
         assertTrue("successfully validated the currency", true);
      }
      // CHECKSTYLE:ON
   }

   /**
    * Test for valid currency by subtraction.
    */
   public void testValidCurrencybySubtraction()
   {
      try
      {
         Object[] params = new Object[]{AMOUNT, CURRENCY};
         LogWriter.logMethodStart("test Invalid Currency", params);
         LogWriter.logDebugMessage("testValidCurrencybySubtraction");
         Money money = new Money(AMOUNT, CURRENCY);
         Money tobesubtracted = new Money(AMOUNT, CURRENCY);
         money.subtract(tobesubtracted);
         LogWriter.logMethodEnd("testInvalidCurrency");
      }
      // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         fail("Exception while validating the currency");
      }
      // CHECKSTYLE:ON
   }

   /**
    * Test for Invalid currency .
    */
   public void testInvalidCurrencybySubtraction()
   {
      try
      {
         Object[] params = new Object[]{AMOUNT, CURRENCY};
         LogWriter.logMethodStart("test Invalid Currency", params);
         Money money = new Money(AMOUNT, CURRENCY);
         Money tobesubtracted = new Money(AMOUNT, CURRENCYUSD);
         money.add(tobesubtracted);
         LogWriter.logMethodEnd("testInvalidCurrency");
         LogWriter.logErrorMessage("Exception while validating the currency");
         fail("Exception while validating the currency");
      }
      // CHECKSTYLE:OFF
      catch (RuntimeException pve)
      {
         assertTrue("successfully validated the currency", true);
      }
      // CHECKSTYLE:ON
   }

}
