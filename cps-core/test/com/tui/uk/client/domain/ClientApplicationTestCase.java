/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ClientApplicationTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-07-17 $
 *
 * $Author: Vinodha.S $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import junit.framework.TestCase;

/**
 * This Class tests the ClientApplication class.
 */
public class ClientApplicationTestCase extends TestCase
{
   /**
    * Test for valid client application name .
    */
   public void testValidClientApplicationName()
   {

      try
      {
         ClientApplication.findByCode("WiSHAO");
      }
      catch (IllegalArgumentException dse)
      {
         fail("Unexpected exception while validating" + dse.getMessage());
      }
   }

   /**
    * Test for invalid client application name .
    */
   public void testInvalidClientApplicationName()
   {
      try
      {
         ClientApplication.findByCode("ABC");
         fail("Exception while validating the code");
      }
      catch (IllegalArgumentException dse)
      {
         assertTrue("Testcase was successfull.", true);
      }
   }

}
