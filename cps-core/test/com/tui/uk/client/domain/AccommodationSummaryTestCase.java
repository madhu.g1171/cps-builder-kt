/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AccommodationSummaryTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2009-09-11 $
 *
 * $Author: thomas.pm $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import junit.framework.TestCase;

import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.utility.TestDataSetup;

/**
 * This Class tests the AccommodationSummary class.
 */
public class AccommodationSummaryTestCase extends TestCase
{

   /** To test the valid accommodation summary.
    *
    * @throws NumberFormatException */
   public void testValidAccom()
   {
      PaymentData paymentData = new PaymentData(
         TestDataSetup.getBookingInfo(PackageType.PreDefinedPackage));
      AccommodationSummary accom = paymentData.getBookingInfo().
                                      getBookingComponent().getAccommodationSummary();
      accom.setHoplaBonded(Boolean.TRUE);
      accom.getUniqueId();
      accom.getStartDate();
      accom.getPrepay();
      accom.getHoplaBonded();
      accom.getEndDate();
      accom.getDuration();
      accom.getCaption();
      accom.getAToZUri();
      accom.getAdditionalInfo();
      accom.getAccommodationSelected();
      accom.getAccommodationInventorySystem();
      accom.getAccommodationExtras();
      accom.getAccommodationDetailsUri();
      accom.getAccommodation().getAccommodationType();
      accom.getAccommodation().getAdditionalValue();
      accom.getAccommodation().getBoardBasis();
      accom.getAccommodation().getCountry();
      accom.getAccommodation().getDestination();
      accom.getAccommodation().getHotel();
      accom.getAccommodation().getRating();
      accom.getAccommodation().getResort();
      accom.getAccommodation().getRoomDescription();
      accom.getAccommodation().getShipId();
      accom.getAccommodation().getRatingImage();
      assertNotNull(accom.getUniqueId(), true);
   }
}
