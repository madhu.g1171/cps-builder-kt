/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponentTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2010-08-05 $
 *
 * $Author: roopesh.s $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

/**
*
* TestCase for BookingComponent Class.
*
*/
public class BookingComponentTestCase extends TestCase
{

   /** the United Kingdom constant. */
   private static final String UNITED_KINGDOM = "United Kingdom";

   /** The duration. */
   private static final int DURATION = 7;

   /**
    * Testing getter and setter.
    * */
   public void test()
   {
      BookingComponent bookingComponent =
         new BookingComponent(ClientApplication.BRAC, "clientDomainURL", new Money(new BigDecimal(
            "123"), Currency.getInstance("GBP")));
      bookingComponent.setPrePaymentUrl("prePaymentUrl");
      assertEquals("clientDomainURLprePaymentUrl", bookingComponent.getPrePaymentUrl());
      bookingComponent.setPaymentSuccessUrl("paymentSuccessUrl");
      assertEquals("clientDomainURLpaymentSuccessUrl", bookingComponent.getPaymentSuccessUrl());

      bookingComponent.setNoRedirect(true);
      assertTrue(bookingComponent.getNoRedirect());

      Map<String, List<PriceComponent>> pricingDetails =
         new HashMap<String, List<PriceComponent>>();
      bookingComponent.setPricingDetails(pricingDetails);
      assertEquals(pricingDetails, bookingComponent.getPricingDetails());

      Map<String, String> breadCrumbTrail = new HashMap<String, String>();
      bookingComponent.setBreadCrumbTrail(breadCrumbTrail);
      assertEquals(breadCrumbTrail, bookingComponent.getBreadCrumbTrail());

      ClientURLComponent clientURLComponent = new ClientURLComponent();
      clientURLComponent.setHomePageURL("homePageURL");
      bookingComponent.setClientURLLinks(clientURLComponent);
      assertEquals("homePageURL", bookingComponent.getClientURLLinks().getHomePageURL());

      CruiseSummary cruiseSummary =
         new CruiseSummary(new Accommodation(UNITED_KINGDOM, "London", "Arona Gran Hotel",
            "Thomson Celebration", "FullBoard"), DURATION, new Accommodation(UNITED_KINGDOM,
            "London", "Arona Gran Hotel", "Thomson Celebration", "FullBoard"));
      assertNotNull(cruiseSummary.getAccommodation());
   }
}
