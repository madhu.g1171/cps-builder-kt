/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PackageTypeTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-07-17 $
 *
 * $Author: Vinodha.S $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import com.tui.uk.log.LogWriter;

import junit.framework.TestCase;

/**
 * This Class tests the PackageType class.
 */
public class PackageTypeTestCase extends TestCase
{
   /**
    * Test for valid package Type.
    */
   public void testValidPackageType()
   {

      try
      {
         PackageType.findByCode("PDP");
      }
      catch (IllegalArgumentException dse)
      {
         LogWriter.logWarningMessage("Got exception.", dse);
         fail("Unexpected exception while validating" + dse.getMessage());
      }
   }

   /**
    * Test for invalid package Type.
    */
   public void testInvalidPackageType()
   {
      try
      {
         PackageType.findByCode("ABC");
         fail("Exception while validating the code");
      }
      catch (IllegalArgumentException dse)
      {
         assertTrue("Testcase was successfull.", true);
      }
   }

}
