/*
* Copyright (C)2006 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   BookingFlowDetailsTestCase.java$
*
* $Revision:   $
*
* $Date:   Dec 22, 2009$
*
* Author: veena.bn
*
*
* $Log:   $
*/
package com.tui.uk.client.domain;

import com.tui.uk.log.LogWriter;

import junit.framework.TestCase;

/**
 * The test case for BookingFlowDetails.
 *
 * @author veena.bn
 */
public class BookingFlowDetailsTestCase extends TestCase
{
   /** The Constant for BookingFlowDetails. */
   private BookingFlowDetails bookingFlowDetails;

   /**
    * Setup method for BookingFlowDetails.
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      bookingFlowDetails = new BookingFlowDetails(BookingFlow.AMEND, PaymentFlow.PAYMENT);
   }

   /**
    * Test for valid BookingFlow.
    */
   public void testValidBookingFlow()
   {
      LogWriter.logMethodStart("testValidBookingFlow");
      assertEquals(BookingFlow.AMEND, bookingFlowDetails.getBookingFlow());
      LogWriter.logMethodEnd("testValidBookingFlow");
   }

   /**
    * Test for valid PaymentFlow.
    */
   public void testValidPaymentFlow()
   {
      LogWriter.logMethodStart("testValidPaymentFlow");
      assertEquals(PaymentFlow.PAYMENT, bookingFlowDetails.getPaymentFlow());
      LogWriter.logMethodEnd("testValidPaymentFlow");
   }

   /**
    * Test for invalid PaymentFlow.
    */
   public void testInvalidPaymentFlow()
   {
      LogWriter.logMethodStart("testInvalidPaymentFlow");
      assertNotSame(PaymentFlow.NO_PAYMENT, bookingFlowDetails.getPaymentFlow());
      LogWriter.logMethodEnd("testInvalidPaymentFlow");
   }

   /**
    * Test for invalid BookingFlow.
    */
   public void testInvalidBookingFlow()
   {
      LogWriter.logMethodStart("testInvalidBookingFlow");
      assertNotSame(BookingFlow.OLBP, bookingFlowDetails.getBookingFlow());
      LogWriter.logMethodEnd("testInvalidBookingFlow");
   }

}
