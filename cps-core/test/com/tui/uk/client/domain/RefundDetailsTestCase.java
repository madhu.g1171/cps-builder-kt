/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: RefundDetailsTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2010-08-02 $
 *
 * $Author: roopesh.s $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;

import junit.framework.TestCase;

/**
 * This Class tests the RefundDetails class.
 */
public class RefundDetailsTestCase extends TestCase
{

   /** The GBP currency. */
   private static final Currency GBP_CURRENCY = Currency.getInstance("GBP");

   /** the ten constant. */
   private static final String HUNDRED = "100";

   /** To test get and setter. */
   public void test()
   {
      Money totalAmount = new Money(new BigDecimal(HUNDRED), GBP_CURRENCY);
      RefundDetails refundDetails =
         new RefundDetails("432093452312", totalAmount, Calendar.getInstance().getTime(),
            "1000011000000005");

      assertEquals(dateWithoutTimestamp(Calendar.getInstance().getTime()),
         dateWithoutTimestamp(refundDetails.getTransactionDate()));
      assertEquals("432093452312", refundDetails.getDatacashReference());
      assertEquals(totalAmount, refundDetails.getTransactionAmount());
      assertEquals("1000011000000005", refundDetails.getMaskedCardNumber());
      refundDetails.setPaymentType("paymentType");
      refundDetails.setPaymentMethod("paymentMethod");
      assertEquals("paymentType", refundDetails.getPaymentType());
      assertEquals("paymentMethod", refundDetails.getPaymentMethod());
   }

   /** This method is to remove Timestamp from the date */
   public String dateWithoutTimestamp(Date date)
   {
      SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
      String dateWithoutTimestamp = formatter.format(date);
      return dateWithoutTimestamp;

   }

}
