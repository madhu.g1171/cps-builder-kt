package com.tui.uk.email;

import static com.tui.uk.client.domain.LeadPassengerDataPanel.ADDRESS_LINE1;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.ADDRESS_LINE2;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.CITY;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.DAY_TIME_PHONE;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.EMAIL_ADDRESS;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.HOUSE_NAME;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.MOBILE_PHONE;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.POST_CODE;
import static com.tui.uk.email.constants.EmailConstants.AMT_REFUND;
import static com.tui.uk.email.constants.EmailConstants.BOOKING_REF_NO;
import static com.tui.uk.email.constants.EmailConstants.BREAK;
import static com.tui.uk.email.constants.EmailConstants.CONTENT;
import static com.tui.uk.email.constants.EmailConstants.COUNTRY;
import static com.tui.uk.email.constants.EmailConstants.DAYTIME_PHONE;
import static com.tui.uk.email.constants.EmailConstants.DEFAULT_HOST;
import static com.tui.uk.email.constants.EmailConstants.DEFAULT_SUBJECT;
import static com.tui.uk.email.constants.EmailConstants.DEPARTURE_DATE;
import static com.tui.uk.email.constants.EmailConstants.EMAIL;
import static com.tui.uk.email.constants.EmailConstants.FROM_ADDRESS;
import static com.tui.uk.email.constants.EmailConstants.HOST;
import static com.tui.uk.email.constants.EmailConstants.HOUSENO;
import static com.tui.uk.email.constants.EmailConstants.LEAD_PASSENGER_DETAILS;
import static com.tui.uk.email.constants.EmailConstants.MOBILEPHONE;
import static com.tui.uk.email.constants.EmailConstants.RECIPIENTS;
import static com.tui.uk.email.constants.EmailConstants.REFUND_CHEQUE_BUSINESSINFO;
import static com.tui.uk.email.constants.EmailConstants.REFUND_CHEQUE_DATE_FORMAT;
import static com.tui.uk.email.constants.EmailConstants.REFUND_CHEQUE_SUBJECT;
import static com.tui.uk.email.constants.EmailConstants.SMTP_CONNECTION_STRING;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import junit.framework.TestCase;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingData;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.FlightDetails;
import com.tui.uk.client.domain.FlightSummary;
import com.tui.uk.client.domain.Money;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.email.service.EmailService;
import com.tui.uk.email.service.impl.EmailServiceImpl;
import com.tui.uk.log.LogWriter;

/**
 * 
 * TestCase for EmailProcessor Class.
 * 
 * @author veena.bn
 * 
 */
public class WSSEmailProcessorTestCase extends TestCase
{
   /** Instance Variable for the Booking Info Class. */
   private BookingInfo bookingInfo = null;

   /**
    * Sets the required data into the bookingInfo which is required for forming the subject and
    * content of the email .
    * 
    */
   protected void setUp()
   {
      ClientApplication clientApplicationName = ClientApplication.WSS;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);
      bookingInfo = new BookingInfo(bookingComponent);
      Money calculatedPayableAmount = new Money(new BigDecimal("100"), Currency.getInstance("GBP"));
      bookingInfo.setCalculatedPayableAmount(calculatedPayableAmount);
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      nonPaymentData.put(HOUSE_NAME.getKey(), "aaa");
      nonPaymentData.put(ADDRESS_LINE1.getKey(), "bbbbb");
      nonPaymentData.put(ADDRESS_LINE2.getKey(), "ccccccc");
      nonPaymentData.put(CITY.getKey(), "luton");
      nonPaymentData.put(POST_CODE.getKey(), "lu2 9tp");
      nonPaymentData.put(DAY_TIME_PHONE.getKey(), "3333333333333");
      nonPaymentData.put(MOBILE_PHONE.getKey(), "9844476758");
      nonPaymentData.put(EMAIL_ADDRESS.getKey(), "veena.bn@dssd.com");
      Date bookingDate = new Date();
      BookingData bookingData = new BookingData("0015/1918", "booked", bookingDate);
      bookingData.setLeadPassengerName("Veena");
      Calendar cal = Calendar.getInstance();
      FlightDetails outboundFlightDetails =
         new FlightDetails("xyz", cal.getTime(), "abc", cal.getTime(), "fff", "rrrr", "rrr");
      List<FlightDetails> outboundFlightDetailList = new ArrayList<FlightDetails>();
      outboundFlightDetailList.add(outboundFlightDetails);
      FlightSummary flightSummary =
         new FlightSummary(outboundFlightDetailList, outboundFlightDetailList, null, Boolean.FALSE);
      bookingComponent.setFlightSummary(flightSummary);
      bookingComponent.setNonPaymentData(nonPaymentData);
      bookingComponent.setBookingData(bookingData);
   }

   /**
    * Tests the valid case of send email.
    * 
    */
   public void testSuccessfullSendEmail()
   {
      EmailService emailService = new EmailServiceImpl();
      Money transactionAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));

      Properties props = new Properties();
      props.put(ConfReader.getConfEntry(SMTP_CONNECTION_STRING, DEFAULT_HOST),
         ConfReader.getConfEntry(HOST, ""));
      Session session = Session.getInstance(props);
      String clientApp =
         bookingInfo.getBookingComponent().getClientApplication().getClientApplicationName();
      try
      {
         // Instantiate the message
         Message message = new MimeMessage(session);
         message.setFrom(new InternetAddress(ConfReader.getConfEntry(
            clientApp + "." + FROM_ADDRESS, "")));
         String[] recipients = ConfReader.getStringValues(clientApp + "." + RECIPIENTS, null, ",");
         int noOfRecipents = recipients.length;
         InternetAddress[] toAddress = new InternetAddress[noOfRecipents];
         for (int i = 0; i < noOfRecipents; i++)
         {
            toAddress[i] = new InternetAddress(recipients[i]);
         }
         message.setRecipients(Message.RecipientType.TO, toAddress);
         message.setSubject(ConfReader.getConfEntry(clientApp + "." + REFUND_CHEQUE_SUBJECT,
            DEFAULT_SUBJECT)
            + " "
            + bookingInfo.getBookingComponent().getBookingData().getBookingReference());
         message.setSentDate(new Date());
         message.setContent(getContent(transactionAmount, bookingInfo.getBookingComponent()),
            CONTENT);
         emailService.sendEmail(message);
      }
      catch (EmailProcessorException epex)
      {
         LogWriter.logErrorMessage(epex.getMessage());
      }
      catch (MessagingException mex)
      {
         LogWriter.logErrorMessage(mex.getMessage());
      }

   }

   /**
    * Tests the failure case of send email.
    * 
    */
   public void testfailureSendEmail()
   {
      try
      {
         throw new EmailProcessorException();
      }
      catch (EmailProcessorException epex)
      {
         assertTrue(true);
      }
      try
      {
         throw new EmailProcessorException("fail message", new Exception());
      }
      catch (EmailProcessorException epex)
      {
         assertTrue(true);
      }
   }

   /**
    * This method will form the content of the email.
    * 
    * @param transactionAmount the transactionAmount.
    * @param bookingComponent the bookingComponent.
    * 
    * @return String the content of email.
    * 
    */
   private String getContent(Money transactionAmount, BookingComponent bookingComponent)
   {
      StringBuilder content = new StringBuilder();
      Map<String, String> nonPaymentData = bookingComponent.getNonPaymentData();
      Date departureDate = null;
      if (bookingComponent.getFlightSummary() != null
         && bookingComponent.getFlightSummary().getOutboundFlight() != null)
      {
         for (FlightDetails flightDetails : bookingComponent.getFlightSummary().getOutboundFlight())
         {
            departureDate = flightDetails.getDepartureDateTime();
         }
      }
      else if (bookingComponent.getAccommodationSummary() != null)
      {
         departureDate = bookingComponent.getAccommodationSummary().getStartDate();
      }
      String date = "";
      if (departureDate != null)
      {
         DateFormat dateFormat = new SimpleDateFormat(REFUND_CHEQUE_DATE_FORMAT);
         date = dateFormat.format(departureDate);
      }
      BigDecimal refundAmount =
         new BigDecimal(Math.abs(transactionAmount.getAmount().doubleValue())).setScale(2,
            BigDecimal.ROUND_HALF_UP);
      content.append("<html><body>");
      content.append(BOOKING_REF_NO);
      content.append(bookingComponent.getBookingData().getBookingReference());
      content.append(BREAK);
      content.append(AMT_REFUND);
      content.append(getCurrencySymbol(transactionAmount));
      content.append(refundAmount);
      content.append(LEAD_PASSENGER_DETAILS);
      content.append(BREAK);
      content.append(bookingComponent.getBookingData().getLeadPassengerName());
      content.append(BREAK);
      content.append(getLeadContactDetails(nonPaymentData));
      content.append("</p>");
      content.append(DEPARTURE_DATE);
      content.append(date).append("</p>");

      String refundChequebusinessInfo = ConfReader.getConfEntry(REFUND_CHEQUE_BUSINESSINFO, "");
      if (StringUtils.isNotBlank(refundChequebusinessInfo))
      {
         content.append(refundChequebusinessInfo);
      }
      content.append("</body></html>");
      return content.toString();
   }

   /**
    * This method will return the postal address,phone number and email address of lead passenger
    * which will be appended to the email content.
    * 
    * @param nonPaymentData the nonPaymentData map.
    * 
    * @return String the lead passenger contact details.
    * 
    */
   private String getLeadContactDetails(Map<String, String> nonPaymentData)
   {
      StringBuilder contactDetails = new StringBuilder();
      getContactData(nonPaymentData, HOUSE_NAME.getKey(), contactDetails);
      getContactData(nonPaymentData, HOUSENO, contactDetails);
      getContactData(nonPaymentData, ADDRESS_LINE1.getKey(), contactDetails);
      getContactData(nonPaymentData, ADDRESS_LINE2.getKey(), contactDetails);
      getContactData(nonPaymentData, CITY.getKey(), contactDetails);
      getContactData(nonPaymentData, POST_CODE.getKey(), contactDetails);
      getContactData(nonPaymentData, COUNTRY, contactDetails);
      getDayTimePhoneNumber(nonPaymentData, DAY_TIME_PHONE.getKey(), contactDetails);
      getMobilePhoneNumber(nonPaymentData, MOBILE_PHONE.getKey(), contactDetails);
      getEmailAddress(nonPaymentData, EMAIL_ADDRESS.getKey(), contactDetails);
      return contactDetails.toString();

   }

   /**
    * This method will take nonPaymentData,key and StringBuilder as the input parameters. First it
    * checks if the value of the key in the non PaymentData Map is null if it is not null it will
    * append it to the StringBuilder to form the contact details.
    * 
    * @param nonPaymentData the nonPaymentData map.
    * @param key the key which should be appended.
    * @param contactDetails the contactDetails StringBuilder.
    * 
    * @return StringBuilder the contact details string which will be formed with lead passenger
    *         address,phone number and email address .
    * 
    */
   private StringBuilder getContactData(Map<String, String> nonPaymentData, String key,
      StringBuilder contactDetails)
   {
      if (StringUtils.isNotBlank(nonPaymentData.get(key)))
      {
         contactDetails.append(nonPaymentData.get(key)).append(BREAK);
      }
      return contactDetails;
   }

   /**
    * This method will take nonPaymentData,key and StringBuilder as the input parameters. First it
    * checks if the value of the key(day time phone) in the non PaymentData Map is null if it is not
    * null it will append it to the StringBuilder to form the contact details.
    * 
    * @param nonPaymentData the nonPaymentData map.
    * @param key the dayTimePhone key which should be appended.
    * @param contactDetails the contactDetails StringBuilder.
    * 
    * @return StringBuilder the contact details string which will be formed with lead passenger
    *         address,phone number and email address .
    * 
    */
   private StringBuilder getDayTimePhoneNumber(Map<String, String> nonPaymentData, String key,
      StringBuilder contactDetails)
   {
      if (StringUtils.isNotBlank(nonPaymentData.get(key)))
      {
         contactDetails.append(DAYTIME_PHONE);
         contactDetails.append(nonPaymentData.get(key)).append(BREAK);
      }
      return contactDetails;
   }

   /**
    * This method will take nonPaymentData,key and StringBuilder as the input parameters. First it
    * checks if the value of the key(mobile phone) in the non PaymentData Map is null if it is not
    * null it will append it to the StringBuilder to form the contact details.
    * 
    * @param nonPaymentData the nonPaymentData map.
    * @param key the mobile phone key which should be appended.
    * @param contactDetails the contactDetails StringBuilder.
    * 
    * @return StringBuilder the contact details string which will be formed with lead passenger
    *         address,phone number and email address .
    * 
    */
   private StringBuilder getMobilePhoneNumber(Map<String, String> nonPaymentData, String key,
      StringBuilder contactDetails)
   {
      if (StringUtils.isNotBlank(nonPaymentData.get(key)))
      {
         contactDetails.append(MOBILEPHONE);
         contactDetails.append(nonPaymentData.get(key)).append(BREAK);
      }
      return contactDetails;
   }

   /**
    * This method will take nonPaymentData,key and StringBuilder as the input parameters. First it
    * checks if the value of the key(email address) in the non PaymentData Map is null if it is not
    * null it will append it to the StringBuilder to form the contact details.
    * 
    * @param nonPaymentData the nonPaymentData map.
    * @param key the email address key which should be appended.
    * @param contactDetails the contactDetails StringBuilder.
    * 
    * @return StringBuilder the contact details string which will be formed with lead passenger
    *         address,phone number and email address .
    * 
    */
   private StringBuilder getEmailAddress(Map<String, String> nonPaymentData, String key,
      StringBuilder contactDetails)
   {
      if (StringUtils.isNotBlank(nonPaymentData.get(key)))
      {
         contactDetails.append(EMAIL);
         contactDetails.append(nonPaymentData.get(key)).append(BREAK);
      }
      return contactDetails;
   }

   /**
    * This method will take the amount of the type Money and returns the corresponding currency
    * symbol.
    * 
    * @param amount the amount.
    * 
    * @return String the currency symbol.
    * 
    */
   private String getCurrencySymbol(Money amount)
   {
      String currency = "";
      if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("GBP"))
      {
         currency = "&pound;";
      }
      else if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("EUR"))
      {
         currency = "&euro;";
      }
      else if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("USD"))
      {
         currency = "&#36;";
      }
      return currency;
   }

}
