package com.tui.uk.email;

import static com.tui.uk.email.constants.EmailConstants.AMT_REFUND;
import static com.tui.uk.email.constants.EmailConstants.BREAK;
import static com.tui.uk.email.constants.EmailConstants.CONTENT;
import static com.tui.uk.email.constants.EmailConstants.CRUISE_HOLIDAY_BOOKING_REF_NO;
import static com.tui.uk.email.constants.EmailConstants.DEFAULT_HOST;
import static com.tui.uk.email.constants.EmailConstants.DEFAULT_SUBJECT;
import static com.tui.uk.email.constants.EmailConstants.FROM_ADDRESS;
import static com.tui.uk.email.constants.EmailConstants.HOLIDAY_START_DATE;
import static com.tui.uk.email.constants.EmailConstants.HOST;
import static com.tui.uk.email.constants.EmailConstants.ITINERARY;
import static com.tui.uk.email.constants.EmailConstants.LEAD_PASSENGER_DETAILS;
import static com.tui.uk.email.constants.EmailConstants.REASON;
import static com.tui.uk.email.constants.EmailConstants.RECIPIENTS;
import static com.tui.uk.email.constants.EmailConstants.REFUND_CHEQUE_BUSINESSINFO;
import static com.tui.uk.email.constants.EmailConstants.REFUND_CHEQUE_SUBJECT;
import static com.tui.uk.email.constants.EmailConstants.SAILING_DATE;
import static com.tui.uk.email.constants.EmailConstants.SHIP;
import static com.tui.uk.email.constants.EmailConstants.SHOREX_BOOKING_REF_NO;
import static com.tui.uk.email.constants.EmailConstants.SMTP_CONNECTION_STRING;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import junit.framework.TestCase;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.Money;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.email.service.EmailService;
import com.tui.uk.email.service.impl.EmailServiceImpl;
import com.tui.uk.log.LogWriter;

/**
 * 
 * TestCase for EmailProcessor Class.
 * 
 * @author veena.bn
 * 
 */
public class ShorexEmailProcessorTestCase extends TestCase
{
   /** Instance Variable for the Booking Info Class. */
   private BookingInfo bookingInfo = null;

   /**
    * Sets the required data into the bookingInfo which is required for forming the subject and
    * content of the email .
    * 
    */
   protected void setUp()
   {
      ClientApplication clientApplicationName = ClientApplication.B2BShoreExcursions;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);

      Map<String, String> nonPaymentData = new HashMap<String, String>();
      nonPaymentData.put("booking_reference", "shorex11234890");
      nonPaymentData.put("shipping_reference", "cruise3345890");
      nonPaymentData.put("sailing_date", "23 Nov 2011");
      nonPaymentData.put("itinerary", "234235");
      nonPaymentData.put("total_refund_amount", "256");
      nonPaymentData.put("reason", "Vacation Cancelled");
      nonPaymentData.put("ship_desc", "The Royal Cruiser");
      nonPaymentData.put("lead_title", "Ms");

      nonPaymentData.put("lead_firstname", "Veena");
      nonPaymentData.put("lead_middle", "");
      nonPaymentData.put("lead_surname", "BN");

      nonPaymentData.put("street_address1", "Walnut");
      nonPaymentData.put("street_address2", "creek");
      nonPaymentData.put("street_address2_1", " california");
      nonPaymentData.put("street_address2_2", "city");
      nonPaymentData.put("street_address3", " ");
      nonPaymentData.put("street_address4", "");
      nonPaymentData.put("cardBillingPostcode", "95643");
      nonPaymentData.put("selectedCountry", "USA");

      bookingComponent.setNonPaymentData(nonPaymentData);
      bookingInfo = new BookingInfo(bookingComponent);
   }

   /**
    * Tests the valid case of send email.
    * 
    */
   public void testSuccessfullSendEmail()
   {
      EmailService emailService = new EmailServiceImpl();
      Money transactionAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));

      Properties props = new Properties();
      props.put(ConfReader.getConfEntry(SMTP_CONNECTION_STRING, DEFAULT_HOST),
         ConfReader.getConfEntry(HOST, ""));
      Session session = Session.getInstance(props);
      String clientApp =
         bookingInfo.getBookingComponent().getClientApplication().getClientApplicationName();
      Message message;
      try
      {
         // Instantiate the message
         message = new MimeMessage(session);
         message.setFrom(new InternetAddress(ConfReader.getConfEntry(
            clientApp + "." + FROM_ADDRESS, "")));
         String[] recipients = ConfReader.getStringValues(clientApp + "." + RECIPIENTS, null, ",");
         int noOfRecipents = recipients.length;
         InternetAddress[] toAddress = new InternetAddress[noOfRecipents];
         for (int i = 0; i < noOfRecipents; i++)
         {
            toAddress[i] = new InternetAddress(recipients[i]);
         }
         message.setRecipients(Message.RecipientType.TO, toAddress);
         message.setSubject(ConfReader.getConfEntry(clientApp + "." + REFUND_CHEQUE_SUBJECT,
            DEFAULT_SUBJECT)
            + " "
            + bookingInfo.getBookingComponent().getNonPaymentData().get("booking_reference"));
         message.setSentDate(new Date());
         message.setContent(getContent(transactionAmount, bookingInfo.getBookingComponent()),
            CONTENT);
         emailService.sendEmail(message);
      }
      catch (EmailProcessorException epex)
      {
         LogWriter.logErrorMessage(epex.getMessage());
      }
      catch (MessagingException mex)
      {
         LogWriter.logErrorMessage(mex.getMessage());
      }
   }

   /**
    * Tests the failure case of send email.
    * 
    */
   public void testfailureSendEmail()
   {
      try
      {
         throw new EmailProcessorException();
      }
      catch (EmailProcessorException epex)
      {
         assertTrue(true);
      }
      try
      {
         throw new EmailProcessorException("fail message", new Exception());
      }
      catch (EmailProcessorException epex)
      {
         assertTrue(true);
      }
   }

   private String getContent(Money transactionAmount, BookingComponent bookingComponent)
   {
      StringBuilder content = new StringBuilder();
      // Map<String, String> nonPaymentData = bookingComponent.getNonPaymentData();
      String bookingReferenceId = bookingComponent.getNonPaymentData().get("booking_reference");
      String cruiseReference = bookingComponent.getNonPaymentData().get("shipping_reference");

      // String holidayStartDate = bookingComponent.getNonPaymentData().get("holiday_startDate");
      String sailingDate = bookingComponent.getNonPaymentData().get("sailing_date");
      String itenary = bookingComponent.getNonPaymentData().get("itinerary");
      String totalRefundAmt = bookingComponent.getNonPaymentData().get("total_refund_amount");
      String reason = bookingComponent.getNonPaymentData().get("reason");
      String shipDesc = bookingComponent.getNonPaymentData().get("ship_desc");

      BigDecimal refundAmount =
         new BigDecimal(Math.abs(Double.valueOf(totalRefundAmt.trim()).doubleValue())).setScale(2,
            BigDecimal.ROUND_HALF_UP);

      content.append("<html><body>");
      content.append(SHOREX_BOOKING_REF_NO);
      content.append(bookingReferenceId);
      content.append(CRUISE_HOLIDAY_BOOKING_REF_NO);
      content.append(cruiseReference);
      content.append(BREAK);
      content.append(LEAD_PASSENGER_DETAILS);
      content.append(BREAK);
      content.append(getLeadName(bookingComponent));
      content.append(BREAK);
      content.append(getLeadAddress(bookingComponent));
      content.append("</p>");
      content.append(HOLIDAY_START_DATE);
      content.append(BREAK);
      content.append(sailingDate);
      content.append(BREAK);
      content.append(SAILING_DATE);
      content.append(sailingDate);
      content.append(BREAK);
      content.append(ITINERARY);
      content.append(itenary);
      content.append(BREAK);
      content.append(SHIP);
      content.append(shipDesc);
      content.append(BREAK);
      content.append(AMT_REFUND);
      content.append(getCurrencySymbol(transactionAmount));
      content.append(refundAmount);
      content.append(BREAK);
      content.append(REASON);
      content.append(reason).append("</p>");

      String refundChequebusinessInfo = ConfReader.getConfEntry(REFUND_CHEQUE_BUSINESSINFO, "");
      if (StringUtils.isNotBlank(refundChequebusinessInfo))
      {
         content.append(refundChequebusinessInfo);
      }
      content.append("</body></html>");
      return content.toString();
   }

   /**
    * This method will return the customer address.
    * 
    * @param bookingComponent BookingComponent
    * 
    * @return String the customer address details.
    * 
    */
   private String getLeadAddress(BookingComponent bookingComponent)
   {
      StringBuilder address = new StringBuilder();
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address1")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address1")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address2")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address2")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address2_1")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address2_1"))
            .append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address2_2")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address2_2"))
            .append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address3")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address3")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address4")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address4")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("cardBillingPostcode")))
      {
         address.append(bookingComponent.getNonPaymentData().get("cardBillingPostcode")).append(
            BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("selectedCountry")))
      {
         address.append(bookingComponent.getNonPaymentData().get("selectedCountry")).append(BREAK);
      }
      return address.toString();
   }

   /**
    * This method will return the lead passenger name.
    * 
    * @param bookingComponent BookingComponent
    * 
    * @return String the lead passenger name details.
    * 
    */
   private String getLeadName(BookingComponent bookingComponent)
   {
      StringBuilder leadName = new StringBuilder();
      leadName.append(bookingComponent.getNonPaymentData().get("lead_title"));
      leadName.append(bookingComponent.getNonPaymentData().get("lead_firstname"));
      leadName.append(bookingComponent.getNonPaymentData().get("lead_middle"));
      leadName.append(bookingComponent.getNonPaymentData().get("lead_surname"));
      return leadName.toString();
   }

   /**
    * This method will take the amount of the type Money and returns the corresponding currency
    * symbol.
    * 
    * @param amount the amount.
    * 
    * @return String the currency symbol.
    * 
    */
   private String getCurrencySymbol(Money amount)
   {
      String currency = "";
      if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("GBP"))
      {
         currency = "&pound;";
      }
      else if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("EUR"))
      {
         currency = "&euro;";
      }
      else if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("USD"))
      {
         currency = "&#36;";
      }
      return currency;
   }

}