/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd,
 * Columbus House,
 * Westwood Way,
 * Westwood Business Park,
 * Coventry,
 * United Kingdom
 * CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence
 * any actual or intended publication of this source code.
 *
 * $RCSfile:   EmailProcessorTestCase.java$
 *
 * $Revision:   $
 *
 * $Date:   Dec 24, 2009$
 *
 * Author: veena.bn
 *
 *
 * $Log:   $
 */
package com.tui.uk.email;

import static com.tui.uk.client.domain.LeadPassengerDataPanel.ADDRESS_LINE1;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.ADDRESS_LINE2;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.CITY;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.DAY_TIME_PHONE;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.EMAIL_ADDRESS;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.HOUSE_NAME;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.MOBILE_PHONE;
import static com.tui.uk.client.domain.LeadPassengerDataPanel.POST_CODE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingData;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.FlightDetails;
import com.tui.uk.client.domain.FlightSummary;
import com.tui.uk.client.domain.Money;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.email.service.EmailService;
import com.tui.uk.email.service.impl.EmailServiceImpl;
import com.tui.uk.log.LogWriter;

/**
 * 
 * TestCase for EmailProcessor Class.
 * 
 * @author veena.bn
 * 
 */
public class EmailProcessorTestCase extends TestCase
{
   /** Instance Variable for the Booking Info Class. */
   private BookingInfo bookingInfo = null;

   /**
    * Sets the required data into the bookingInfo which is required for forming the subject and
    * content of the email .
    * 
    */
   protected void setUp()
   {
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);
      bookingInfo = new BookingInfo(bookingComponent);
      Money calculatedPayableAmount = new Money(new BigDecimal("100"), Currency.getInstance("GBP"));
      bookingInfo.setCalculatedPayableAmount(calculatedPayableAmount);
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      nonPaymentData.put(HOUSE_NAME.getKey(), "aaa");
      nonPaymentData.put(ADDRESS_LINE1.getKey(), "bbbbb");
      nonPaymentData.put(ADDRESS_LINE2.getKey(), "ccccccc");
      nonPaymentData.put(CITY.getKey(), "luton");
      nonPaymentData.put(POST_CODE.getKey(), "lu2 9tp");
      nonPaymentData.put(DAY_TIME_PHONE.getKey(), "3333333333333");
      nonPaymentData.put(MOBILE_PHONE.getKey(), "9844476758");
      nonPaymentData.put(EMAIL_ADDRESS.getKey(), "veena.bn@dssd.com");
      Date bookingDate = new Date();
      BookingData bookingData = new BookingData("0015/1918", "booked", bookingDate);
      bookingData.setLeadPassengerName("Veena");
      Calendar cal = Calendar.getInstance();
      FlightDetails outboundFlightDetails =
         new FlightDetails("xyz", cal.getTime(), "abc", cal.getTime(), "fff", "rrrr", "rrr");
      List<FlightDetails> outboundFlightDetailList = new ArrayList<FlightDetails>();
      outboundFlightDetailList.add(outboundFlightDetails);
      FlightSummary flightSummary =
         new FlightSummary(outboundFlightDetailList, outboundFlightDetailList, null, Boolean.FALSE);
      bookingComponent.setFlightSummary(flightSummary);
      bookingComponent.setNonPaymentData(nonPaymentData);
      bookingComponent.setBookingData(bookingData);
   }

   /**
    * Tests the valid case of send email.
    * 
    */
   public void testSuccessfullSendEmail()
   {
      EmailService emailService = new EmailServiceImpl();
      Money transactionAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      try
      {
         // emailService.sendEmail(transactionAmount, bookingInfo.getBookingComponent());
      }
      // catch (EmailProcessorException epex)
      // {
      // LogWriter.logErrorMessage(epex.getMessage());
      // }
      catch (Exception epex)
      {
         LogWriter.logErrorMessage(epex.getMessage());
      }
   }

   /**
    * Tests the failure case of send email.
    * 
    */
   public void testfailureSendEmail()
   {
      try
      {
         throw new EmailProcessorException();
      }
      catch (EmailProcessorException epex)
      {
         assertTrue(true);
      }
      try
      {
         throw new EmailProcessorException("fail message", new Exception());
      }
      catch (EmailProcessorException epex)
      {
         assertTrue(true);
      }
   }

}
