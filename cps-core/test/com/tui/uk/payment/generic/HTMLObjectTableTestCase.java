/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: CardChargesServletTestCase.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-09-17 08:25:26 $
 *
 * $Author: Venkateswarlu.p@sonata-software.com $
 *
 *
 * $Log: $.
 *
 */
package com.tui.uk.payment.generic;

import junit.framework.TestCase;
import com.tui.uk.utility.TestDataSetup;

/**
 * This is test case for CardChargesServlet class.
 *
 * @author santosh.ks
 *
 */
public class HTMLObjectTableTestCase extends TestCase
{
   /**
    * To Test CardChargesServlet.
    *
    */
   public void testToHTML()
   {
      HTMLObjectTable htmlObjectTable = new HTMLObjectTable();
      String toHTML = htmlObjectTable.toHTML(TestDataSetup.getBookingComponent());
      assertNotNull(toHTML);
   }

}
