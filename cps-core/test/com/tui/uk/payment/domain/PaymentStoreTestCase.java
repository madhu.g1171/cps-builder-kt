/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PaymentStoreTestCase.java,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2008-05-14 12:56:12 $
 *
 * $Author: bibin.j@sonata-software.com $
 *
 * $Log: not supported by cvs2svn $
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.Money;

/**
 * This class is used to test the methods in the class PaymentStore.
 *
 * @author bibin.j@sonata-software.com
 */
public class PaymentStoreTestCase extends TestCase
{
   /** The token. */
   private static UUID uuid;

   /** The constant THREE. */
   private static final int THREE = 3;

   /**
    * Test case to test the method getInstance.
    *
    */
   public void testgetInstance()
   {
      PaymentStore paymentStore = PaymentStore.getInstance();
      assertNotNull(paymentStore);
   }

   /**
    * Test case to test the method newPaymentData.
    *
    */
   public void testnewPaymentData()
   {
      PaymentData paymentData = new PaymentData(getBookingInfo());
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      assertNotNull(uuid);
      paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      Payment payment = getPayment();
      paymentData.setPayment(payment);
   }


   /**
    * Test case to test the method destroySensitiveData.
    *
    */
   public void testDestroySensitiveData()
   {
      PaymentData paymentData = new PaymentData(getBookingInfo());
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);

      paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      Payment payment = getPayment();
      paymentData.setPayment(payment);

      PaymentStore paymentStore = PaymentStore.getInstance();
      assertNotNull(paymentStore);
      paymentStore.destroySensitiveData(uuid);
      assertTrue(true);
   }

   /**
    * Test case to test the method getPaymentData.
    *
    */
   public void testgetPaymentData()
   {
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      assertNotNull(paymentData);
   }

   /**
    * Test case to test the method destroyPayment.
    *
    */
   public void testdestroyPayment()
   {
      PaymentStore.getInstance().destroyPayment(uuid);
   }

   /**
    * Gets the booking component.
    *
    * @return the booking component
    */
   private static BookingComponent getBookingInfo()
   {
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      return bookingComponent;
   }

   /**
    * Gets the booking component.
    *
    * @return the booking component
    */
   private static Payment getPayment()
   {
      Payment payment = new Payment();
      String paymentTypeCode = "Dcard";
      Money transactionAmount = new Money(new BigDecimal("100"), Currency.getInstance("GBP"));
      //Money transactionCharge = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));

      PaymentTransaction transaction =
         new ChequePaymentTransaction(paymentTypeCode, transactionAmount);
      payment.getPaymentTransactions().add(transaction);

      char[] pan =
         {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1', '1', '1', '1' };
      char[] cv2 = {'4', '4', '4' };
      String nameOncard = "xxxxxxxx";
      String expiryDate = "01/09";
      String postCode = "aa22bb";
      String cardType = "VISA";
      CnpCard card = new CnpCard(pan, nameOncard, expiryDate, cv2, postCode, cardType, THREE,
         THREE);
      transaction =
         new PostPaymentGuaranteeTransaction(card, paymentTypeCode, transactionAmount);
      payment.getPaymentTransactions().add(transaction);
      /*transaction = new DataCashPaymentTransaction(paymentTypeCode,
            transactionAmount, transactionCharge, card);
      payment.getPaymentTransactions().add(transaction);*/
      return payment;
   }
}
