/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CpCardTestCase.java$
 *
 * $Revision: $
 *
 * $Date:  $
 *
 * $Author: thomas.pm $
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import junit.framework.TestCase;

import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This Class tests the Card class.
 */
public class CpCardTestCase extends TestCase
{

   /** The character array holding a valid card number. */
   private char[] validPan = "4444333322221111".toCharArray();

   /** The character array holding a blank card number. */
   private char[] blankPan = {};

   /** The Card object. */
   private Card card;

   /**
    * Setting null card details.
    */
   private void setBlankCardNumber()
   {
      card = new CpCard(blankPan);
   }

   /**
    * Setting null card details.
    */
   private void setValidCardNumber()
   {
      card = new CpCard(validPan);
   }

   /**
    * Test for masked card number.
    */
   public void testMaskedCardNumber()
   {
      setValidCardNumber();
      assertEquals(card.getMaskedCardNumber().length(), card.getPan().length);
   }

   /**
    * Test for valid credit card validation.
    */
   public void testValidation()
   {
      try
      {
         setValidCardNumber();
         card.validate();
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated", true);
      }
   }

   /**
    * Test for invalid credit card validation.
    */
   public void testInValidCard()
   {
      try
      {
         setBlankCardNumber();
         card.validate();
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated", true);
      }
   }

   /**
    * Test purge card details.
    */
   public void testPurgeCard()
   {
      setValidCardNumber();
      card.purgeCardDetails();
   }

   /**
    * Test null name on card.
    */
   public void testNullNameOnCard()
   {
      setValidCardNumber();
      assertNull(card.getNameOncard());
   }

   /**
    * Test null expiry date.
    */
   public void testNullExpiryDate()
   {
      setValidCardNumber();
      assertNull(card.getExpiryDate());
   }

   /**
    * Test null card type.
    */
   public void testNullCardType()
   {
      setValidCardNumber();
      assertNull(card.getCardtype());
   }

   /**
    * Test purge card details.
    */
   public void testNullCv2()
   {
      setValidCardNumber();
      assertNull(card.getCv2());
   }
}