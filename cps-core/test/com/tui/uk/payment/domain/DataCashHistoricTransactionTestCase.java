/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd,
 * Columbus House,
 * Westwood Way,
 * Westwood Business Park,
 * Coventry,
 * United Kingdom
 * CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence
 * any actual or intended publication of this source code.
 *
 * $RCSfile:   RefundHistoricTransactionTestCase.java$
 *
 * $Revision:   $
 *
 * $Date:   Oct 5, 2009$
 *
 * Author: veena.bn
 *
 *
 * $Log:   $
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;


/**
 * This class tests the DataCashHistoricTransaction class.
 *
 * @author veena.bn
 *
 */
public class DataCashHistoricTransactionTestCase extends TestCase
{

   /** The Constant CLIENT_VALUE.*/
   private static final String CLIENT_VALUE = "99080800";

   /** Total amount.*/
   private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(10.00);

   /** The payment type code. */
   private static final String PAYMENTTYPECODE_FOR_REFUND = "RR";

   /** The merchant reference number. */
   private static final String MERCHANT_REFERENCE = "BRACApplicationf4435b7e69fa4ce";

   /**The <code>Money</code> object. */
   private Money transactionAmount = new Money(TOTAL_AMOUNT, Currency.getInstance("GBP"));

   /** The Constant DatacashReference. */
   private String datacashReference = "4100200061461780";

   /** The Constant to hold InvalidDatacashReference .*/
   private String invalidDataCashReference = "4100200061461asdf";

   /** The dataCashHistoric object.*/
   private DataCashHistoricTransaction dataCashHistoricTransacion;

   /** String holding expiry date. */
  // private String expiryDate = "08/10";

   /** The character array holing cv2. */

  // private char[] cv2 = {'4', '4', '4' };

   /** Total charge. */
   //private static final BigDecimal TOTAL_CHARGE = BigDecimal.valueOf(0.00);

   /** The <code>Money</code> object. */
  // private Money transactionCharge = new Money(TOTAL_CHARGE, Currency.getInstance("GBP"));

   /** The character array holing cv2. */
  // private char[] validPan =
  //    {'4', '2', '4', '2', '4', '2', '5', '0', '0', '0', '0', '0', '0', '0', '0', '9' };

   /** String to hold cardtype.*/
   //private String cardtype = "visa";

   /**Object  to hold Card.*/
   //private Card card;

   /**
    * set Valid Card Details.
    */
  // private void setValidDetails()
   //{
   //   card = new Card(validPan, "test", expiryDate, cv2, "aaa", cardtype);
  // }

   /**
    * Sets up the initial data.
    */
   protected void setUp()
   {
      dataCashHistoricTransacion = new DataCashHistoricTransaction(
         PAYMENTTYPECODE_FOR_REFUND, new Money(
            BigDecimal.TEN, Currency.getInstance("GBP")),
                  datacashReference, MERCHANT_REFERENCE);
   }

   /**
    *  testRefunvalid.
    */
   public void testRefundValid()
   {
     /** try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
            new DataCashPaymentTransaction("VI", transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.authPayment(CLIENT_VALUE, "");
         dataCashPaymentTransaction.fulfill(CLIENT_VALUE);
         datacashReference = dataCashPaymentTransaction.getDatacashReference();
         RefundHistoricTransaction refundHistoricTransaction =
            new RefundHistoricTransaction("RQ", transactionAmount, datacashReference);
         refundHistoricTransaction.refund(CLIENT_VALUE, datacashReference);

      }
      catch (DataCashServiceException dcse)
      {
         fail("DataCashServiceException occured");
      }*/
   }

   /**
    *  testRefunvalid.
    */
   public void testRefundInValid()
   {
      /**try
      {
         RefundHistoricTransaction refundHistoricTransaction =
            new RefundHistoricTransaction("RR", transactionAmount, datacashReference);
         refundHistoricTransaction.refund(CLIENT_VALUE, datacashReference);

      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull.", true);
      }*/
   }

   /**
    * Tests for a valid DataCashReference.
    */
   public void testDatacashReferenceForNumeric()
   {
      try
      {
         DataCashHistoricTransaction dataCashHistoricTransaction =
            new DataCashHistoricTransaction(PAYMENTTYPECODE_FOR_REFUND, transactionAmount,
                  datacashReference, MERCHANT_REFERENCE);
         dataCashHistoricTransaction.validate();
         assertTrue("successfully validated the Reference value", true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Tests for a valid DataCashReference.
    */
   public void testDatacashReferenceFornonNumeric()
   {
      try
      {
         DataCashHistoricTransaction dataCashHistoricTransaction =
            new DataCashHistoricTransaction(this.invalidDataCashReference,
               "12345678901234", MERCHANT_REFERENCE);
         dataCashHistoricTransaction.getAuthCode();
         dataCashHistoricTransaction.validate();
         fail("Exception while validating voucher code");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("Unexpected exception while validation." + pve.getMessage(), true);
      }
   }

   /**
    * Tests the getTransactionAmount method.
    */
   public void testGetTransactionAmount()
   {
      assertEquals(BigDecimal.TEN,
          dataCashHistoricTransacion.getTransactionAmount().getAmount());
   }

   /**
    * Tests the getTransactionCharge method.
    */
   public void testGetTransactionCharge()
   {
      assertEquals(BigDecimal.ZERO, dataCashHistoricTransacion.getTransactionCharge().getAmount());
   }

   /**
    * Tests the getPaymentTypeCode method.
    */
   public void testGetPaymentTypeCode()
   {
      assertEquals(PAYMENTTYPECODE_FOR_REFUND, dataCashHistoricTransacion.getPaymentTypeCode());
   }

   /**
    * Tests the getEssentialTransaction method.
    */
   public void testEssentialTransaction()
   {
      assertNotNull(dataCashHistoricTransacion.getEssentialTransactionData());
   }

   /**
    * Tests the getDataCashReference method.
    */
   public void testGetDataCashReference()
   {
      assertEquals(datacashReference, dataCashHistoricTransacion.getDatacashReference());
   }

   /**
    * Tests the getClearSensitiveData method.
    */
   public void testClearSensitiveData()
   {
      dataCashHistoricTransacion.clearSensitiveData();
      assertTrue("successfully cleared sensitive data", true);
   }
}