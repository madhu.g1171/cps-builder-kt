/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BackEndDataCashPaymentTransactionTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2009-03-19 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import com.tui.uk.payment.exception.PaymentValidationException;

import junit.framework.TestCase;

/**
 * The TestCase for BackEndDataCashPaymentTransaction class.
 *
 */
public class BackEndDataCashPaymentTransactionTestCase extends TestCase
{
   /** The backEndDataCashHistoric object. */
   private BackEndDataCashPaymentTransaction paymentTransaction;

   /**
    * Sets up the initial data.
    */
   protected void setUp()
   {
      paymentTransaction =
         new BackEndDataCashPaymentTransaction("99080800", null, "34532",
            "BRACApplicationf4435b7e69fa4ce");
   }

   /**
    * Tests whether the fulfil is successful.
    */
   public void testBackEndFulfill()
   {
      BackEndDataCashPaymentTransaction paymentTransaction1 =
         new BackEndDataCashPaymentTransaction("99080800", "7777666655555444", "34532", "");
      try
      {
         String response = paymentTransaction1.backEndFulfilPayment();
         //assertNotNull(response);
      }
      catch (Exception e)
      {
         assertTrue(true);
      }
   }

   /**
    * Tests whether the fulfil is successful.
    */
   public void testValidate()
   {
      BackEndDataCashPaymentTransaction paymentTransaction1 =
         new BackEndDataCashPaymentTransaction("99080800", "4100201000461104", "34532", "");
      try
      {
         paymentTransaction1.validate();
      }
      catch (PaymentValidationException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   /**
    * Tests whether the cancel is successful.
    */
   public void testBackEndCancel()
   {
      BackEndDataCashPaymentTransaction paymentTransaction1 =
         new BackEndDataCashPaymentTransaction("99080800", null, "34532", "");
      String response = paymentTransaction1.backEndCancel();
      assertNotNull(response);
   }

   /**
    * Tests to clear sensitive data.
    */
   public void testClearSensitiveData()
   {
      paymentTransaction.clearSensitiveData();
      assertTrue("successfully cleared sensitive data", true);
   }

   /**
    * Tests the getTransactionAmount method.
    */
   public void testGetTransactionAmount()
   {
      assertNull(paymentTransaction.getTransactionAmount());
   }

   /**
    * Tests the getPaymentTypeCode method.
    */
   public void testGetPaymentTypeCode()
   {
      assertNull(paymentTransaction.getPaymentTypeCode());
   }

   /**
    * Tests the getEssentialTransaction method.
    */
   public void testEssentialTransaction()
   {
      assertNull(paymentTransaction.getEssentialTransactionData());
   }
}
