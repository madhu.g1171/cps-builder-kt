/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: Aug 10, 2009
 *
 * $Author: sindhushree.g
 *
 * $Log: $
*/
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class tests the GiftCardPaymentTransaction class.
 *
 * @author sindhushree.g
 *
 */
public class GiftCardPaymentTransactionTestCase extends TestCase
{

   /** The gift card object. */
   private GiftCardPaymentTransaction giftCardPaymentTransaction;

   /**
    * Sets up the initial data.
    */
   protected void setUp()
   {
      giftCardPaymentTransaction = new GiftCardPaymentTransaction("GiftCard", new Money(
         BigDecimal.TEN, Currency.getInstance("GBP")), new GiftCard("6335860000000018"
            .toCharArray(), "1124".toCharArray(), "GiftCard"), String.valueOf(UUID.randomUUID()
               .getMostSignificantBits()).replaceAll("-", ""));
   }

   /**
    * Test case to test validate method.
    */
   public void testValidate()
   {
      try
      {
         giftCardPaymentTransaction.validate();
         assertTrue(true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected Exception");
      }
   }

   /**
    * Tests the getEssentialTransactionData method.
    */
   public void testEssentialTransactionData()
   {
      assertNotNull(giftCardPaymentTransaction.getEssentialTransactionData());
   }

   /**
    * Tests the clearSensitiveData method.
    */
   public void testClearSensitiveData()
   {
      giftCardPaymentTransaction.clearSensitiveData();
      assertTrue(true);
   }

   /**
    * Tests the getPaymentMethod method.
    */
   public void testGetPaymentMethod()
   {
      assertEquals(PaymentMethod.GIFT_CARD, giftCardPaymentTransaction.getPaymentMethod());
   }

   /**
    * Tests the getPaymentTypeCode method.
    */
   public void testGetPaymentTypeCode()
   {
      assertEquals("GiftCard", giftCardPaymentTransaction.getPaymentTypeCode());
   }

   /**
    * Tests the getTransactionAmount method.
    */
   public void testGetTransactionAmount()
   {
      assertEquals(BigDecimal.TEN, giftCardPaymentTransaction.getTransactionAmount().getAmount());
   }

   /**
    * Tests the getTransactionCharge method.
    */
   public void getTransactionCharge()
   {
      assertEquals(BigDecimal.ZERO, giftCardPaymentTransaction.getTransactionCharge().getAmount());
   }

}
