/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashPaymentTransactionTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 $
 *
 * $Author: Vinodha.S $
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

/*import java.math.BigDecimal;
import java.util.Currency;

import com.tui.uk.booking.domain.Money;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;*/

import java.math.BigDecimal;
import java.util.Currency;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * The TestCase for DataCashPaymentTransaction class.
 * TODO Methods should be uncommented and make use of Datacash stub once ready.
 * Methods are disabled in order Build to work on build machine via cruise control.
 */
public class DataCashPaymentTransactionTestCase extends TestCase
{
   /** The data cash payment transaction object. */
   private DataCashPaymentTransaction dataCashPaymentTransaction;
//CHECKSTYLE:OFF
   /** Total amount. *//*
   private static final BigDecimal TOTAL_AMOUNT =
      BigDecimal.valueOf(100.00);

   *//** Total charge. *//*
   private static final BigDecimal TOTAL_CHARGE = BigDecimal.valueOf(2.00);

   *//** The character array holing cv2. *//*
   private char[] cv2 = {'4', '4', '4'};

   *//** The character array holing cv2. *//*
   private char[] validPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1',
         '1', '1', '1'};

   *//** The character array holing a invalid card number. *//*
   private char[] invalidPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1',
         '1', '8', '8'};

   *//** The payment type code. *//*
   private String paymentTypeCode = "VC";

   *//** String holding expiry date. *//*
   private String expiryDate = "08/10";

   *//** String holding authorization Code. *//*
   private String authCode = "198198";

   *//** The <code>Card</code> object. *//*
   private Card card;

  *//** The <code>Money</code> object. *//*
   private Money transactionAmount = new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));

   *//** The <code>Money</code> object. *//*
   private Money transactionCharge = new Money(TOTAL_CHARGE, Currency.getInstance(GBP));

   *//** The card type. *//*
   private String cardtype = "visa";

   *//** The Constant CLIENT_VALUE. *//*
   private static final String CLIENT_VALUE = "99080800";

   *//** The Constant CLIENT_VALUE. *//*
   private static final String INVALID_CLIENT_VALUE = "123080801";


   *//** Failure Message. *//*
   private static final String FAIL_MSG = "Should have got an exception.";

   *//** The Constant GBP. *//*
   private static final String GBP = "GBP";

   *//**
    * Setting valid card details.
    *//*
   private void setValidDetails()
   {
      card = new Card(validPan, "test", expiryDate, cv2, "aaa", cardtype);
   }*/

   /*/**
    * Setting invalid card details.
    *//*
   private void setInvalidDetails()
   {
      card =
         new Card(invalidPan, "test", expiryDate, cv2, "aaa", cardtype);
   }



   *//**
    * Tests whether the auth payment is valid for card.
    *
    *//*
   public void testAuthPaymentValid()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
            new DataCashPaymentTransaction(paymentTypeCode,
               transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.authPayment(CLIENT_VALUE, authCode);

      }
      catch (DataCashServiceException dse)
      {
         fail("Unexpected exception while authorized payment."
            + dse.getMessage());
      }
   }

   *//**
    * Test whether the auth payment is invalid.
    *//*
   public void testAuthPaymentInvalid()
   {
      try
      {
         setInvalidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
               new DataCashPaymentTransaction(paymentTypeCode, transactionAmount,
                                                                    transactionCharge, card);
         dataCashPaymentTransaction.authPayment(CLIENT_VALUE, authCode);
         fail("Exception while validating the authorized payment");
      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull.", true);
      }
      // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         assertTrue("Testcase was successfull", true);
      }


   }

   *//**
    * Tests whether the auth refund is valid for card.
    *//*
   public void testAuthRefundValid()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
            new DataCashPaymentTransaction(paymentTypeCode, transactionAmount,
                                                                    transactionCharge, card);
         dataCashPaymentTransaction.authRefund(CLIENT_VALUE, authCode);

      }
      catch (DataCashServiceException dse)
      {
         fail("Unexpected exception while authorized refund."
            + dse.getMessage());
      }
    // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         fail("Unexpected exception while authorized refund."
            + dse.getMessage());
      }

   }

   *//**
    * Tests whether the auth refund is invalid.
    *//*
   public void testAuthRefundInvalid()
   {
      try
      {
         setInvalidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
            new DataCashPaymentTransaction(paymentTypeCode, transactionAmount,
                                                                       transactionCharge, card);
         dataCashPaymentTransaction.authRefund(CLIENT_VALUE, authCode);
         fail(FAIL_MSG);
      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull", true);
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         assertTrue("Testcase was successfull", true);
      }

   }

   *//**
    * Tests whether the refund is valid.
    *//*
   public void testdoRefundValid()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
            new DataCashPaymentTransaction(paymentTypeCode, transactionAmount,
                                       transactionCharge, card);
         dataCashPaymentTransaction.doRefund(CLIENT_VALUE, authCode);
      }
      catch (DataCashServiceException dcpt)
      {
         fail("data cash payment transaction exception in refunding."
            + dcpt.getMessage());
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         fail("data cash payment transaction exception in refunding."
            + dse.getMessage());
      }

   }

   *//**
    * Tests whether the refund is invalid.
    *//*
   public void testdoRefundInvalid()
   {
      try
      {
         setInvalidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                      new DataCashPaymentTransaction(paymentTypeCode,
                                  transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.doRefund(CLIENT_VALUE, authCode);
         fail(FAIL_MSG);
      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull", true);
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         assertTrue("Testcase was successfull", true);
      }

   }

   *//**
    * Tests whether the refund is valid.
    *//*
   public void testDoPaymentValid()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                  new DataCashPaymentTransaction(paymentTypeCode,
                              transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.doPayment(CLIENT_VALUE, authCode);

      }
      catch (DataCashServiceException dcpt)
      {
         fail("data cash payment transaction exception in dopayment."
            + dcpt.getMessage());
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dcpt)
      {
         fail("data cash payment transaction exception in dopayment."
            + dcpt.getMessage());
      }

   }

   *//**
    * Tests whether the refund is invalid.
    *//*
   public void testDoPaymentInvalid()
   {
      try
      {
         setInvalidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                  new DataCashPaymentTransaction(paymentTypeCode,
                           transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.doPayment(CLIENT_VALUE, authCode);
         fail(FAIL_MSG);
      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull", true);
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         assertTrue("Testcase was successfull", true);
      }

   }

   *//**
    * Tests whether the fullfilled payment is achieved or not.
    *//*
   public void testFulfillValid()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
               new DataCashPaymentTransaction(paymentTypeCode,
                           transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.authPayment(CLIENT_VALUE, authCode);
         dataCashPaymentTransaction.fulfill(CLIENT_VALUE);
      }
      catch (DataCashServiceException dcse)
      {
         fail("Unexpected data cash service  exception during fulfillment."
            + dcse.getMessage());
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dcse)
      {
         fail("Unexpected data cash service  exception during fulfillment."
            + dcse.getMessage());
      }

   }

   *//**
    * Tests whether the fulfill is invalid.
    *//*

   public void testFulfillInvalid()
   {
      try
      {
         setInvalidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                  new DataCashPaymentTransaction(paymentTypeCode,
                           transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.fulfill(CLIENT_VALUE);
         fail(FAIL_MSG);
      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull", true);
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         assertTrue("Testcase was successfull", true);
      }

   }

   *//**
    * Tests whether the cancel payment is achieved or not.
    *//*

   public void testValidCancel()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                  new DataCashPaymentTransaction(paymentTypeCode,
                           transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.authPayment(CLIENT_VALUE, authCode);
         dataCashPaymentTransaction.cancel(CLIENT_VALUE);

      }
      catch (DataCashServiceException dcse)
      {
         fail("Unexpected data cash service exception during cancel."
            + dcse.getMessage());
      }


   }

   *//**
    * Tests whether the cancel is invalid.
    *//*
   public void testCancelWithInvalidClient()
   {
      try
      {
         setInvalidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                     new DataCashPaymentTransaction(paymentTypeCode,
                              transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.authPayment(CLIENT_VALUE, authCode);
         dataCashPaymentTransaction.cancel(INVALID_CLIENT_VALUE);
         fail(FAIL_MSG);
      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull", true);
      }
   // CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         assertTrue("Testcase was successfull", true);
      }

   }

   *//**
    * Tests whether valid payment reversal is achieved for card.
    *//*
   public void testReverseValid()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                     new DataCashPaymentTransaction(paymentTypeCode,
                              transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.authPayment(CLIENT_VALUE, authCode);
         dataCashPaymentTransaction.fulfill(CLIENT_VALUE);
         dataCashPaymentTransaction.reverse(CLIENT_VALUE,
            transactionAmount);

      }
      catch (DataCashServiceException dcse)
      {
         fail("Unexpected exception while reversing.");
      }
      //CHECKSTYLE:OFF
      catch (RuntimeException dcse)
      {
         fail("Unexpected exception while reversing.");
      }

   }

   *//**
    * Tests whether payment reversal is invalid.
    *//*
   public void testReverseInvalid()
   {
      try
      {
         setInvalidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                     new DataCashPaymentTransaction(paymentTypeCode,
                              transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.reverse(CLIENT_VALUE,
            transactionAmount);
         fail(FAIL_MSG);
      }
      catch (DataCashServiceException dse)
      {
         assertTrue("Testcase was successfull", true);
      }
      //CHECKSTYLE:OFF
      catch (RuntimeException dse)
      {
         assertTrue("Testcase was successfull", true);
      }
     //CHECKSTYLE:ON
   }
*/
   /**
    * Tests whether the cancel payment is achieved or not.
    */

   /*public void testCancelForInvalidDataCashReference()
   {
      try
      {
         setValidDetails();
         DataCashPaymentTransaction dataCashPaymentTransaction =
                  new DataCashPaymentTransaction(paymentTypeCode,
                           transactionAmount, transactionCharge, card);
         dataCashPaymentTransaction.cancel(CLIENT_VALUE);
         assertNull("Datcash reference is null",
            dataCashPaymentTransaction.getDatacashReference());

      }
      catch (DataCashServiceException dcse)
      {
         fail("DataCashServiceException occured");
      }
  }*/
   /**
    * sets up the initial data.
    */
   protected void setUp()
   {
      dataCashPaymentTransaction = new DataCashPaymentTransaction("MASTERCARD",
         new Money(BigDecimal.TEN, Currency.getInstance("GBP")),
            new Money(BigDecimal.ONE, Currency.getInstance("GBP")),
                   getCard(), "ThomsonBYOef6ec158db91403ebb17");
      dataCashPaymentTransaction.setAuthCode("23121232");
   }
   /**
    * gets the card object.
    *
    * @return cnpCard the card object.
    */
   private CnpCard getCard()
   {
      CnpCard card = new CnpCard("1000011000000005".toCharArray(), "test",
         "11/11", "444".toCharArray(), "lu2 9tp", "Mastercard", 0, 3);
      return card;
   }

   /**
    * Blank testcase is written as the required bin files are
    * not picked up properly in the build machine.
    */
   public void testBlank()
   {
       assertTrue("Blank testcase", true);
   }
   /**
    * Tests the getPaymentMethod method.
    */
   public void testGetPaymentMethod()
   {
      assertEquals(PaymentMethod.DATACASH, dataCashPaymentTransaction.getPaymentMethod());
   }

   /**
    * Tests the getPaymentTypeCode method.
    */
   public void testGetPaymentTypeCode()
   {
      assertEquals("MASTERCARD", dataCashPaymentTransaction.getPaymentTypeCode());
   }

   /**
    * Tests the getTransactionAmount method.
    */
   public void testGetTransactionAmount()
   {
      assertEquals(BigDecimal.TEN, dataCashPaymentTransaction.getTransactionAmount().getAmount());
   }

   /**
    * Tests the getTransactionCharge method.
    */
   public void testGetTransactionCharge()
   {
      assertEquals(BigDecimal.ONE, dataCashPaymentTransaction.getTransactionCharge().getAmount());
   }
   /**
    * Tests the getAuthCode method.
    */
   public void testGetAuthCode()
   {
      assertEquals("23121232", dataCashPaymentTransaction.getAuthCode());
   }
   /**
    * Tests the getEssentialTransactionData method.
    */
   public void testEssentialTransactionData()
   {
      assertNotNull(dataCashPaymentTransaction.getEssentialTransactionData());
   }
   /**
    * Tests the clearSensitiveData method.
    */
   public void testClearSensitiveData()
   {
      dataCashPaymentTransaction.clearSensitiveData();
      assertTrue(true);
   }
   /**
    * Tests the payment method.
    */
   public void testPaymentMethod()
   {
      assertEquals(PaymentMethod.DATACASH, dataCashPaymentTransaction.getPaymentMethod());
   }
}
