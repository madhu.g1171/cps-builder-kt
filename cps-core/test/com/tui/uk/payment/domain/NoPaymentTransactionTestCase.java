/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: NoPaymentTransactionTestCase.java$
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: thomas.pm $
 *
 * $Log: $
*/
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class tests the NoPaymentTransaction class.
 *
 * @author thomas.pm
 *
 */
public class NoPaymentTransactionTestCase extends TestCase
{

   /** The gift card object. */
   private NoPaymentTransaction noPaymentTransaction;

   /**
    * Sets up the initial data.
    */
   protected void setUp()
   {
      noPaymentTransaction = new NoPaymentTransaction(PaymentMethod.NO_PAYMENT.getCode(),
         new Money(BigDecimal.ZERO, Currency.getInstance("GBP")));
   }

   /**
    * Test case to test validate method.
    */
   public void testValidate()
   {
      try
      {
         noPaymentTransaction.validate();
         assertTrue(true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected Exception");
      }
   }

   /**
    * Tests the getEssentialTransactionData method.
    */
   public void testEssentialTransactionData()
   {
      assertNotNull(noPaymentTransaction.getEssentialTransactionData());
   }

   /**
    * Tests the clearSensitiveData method.
    */
   public void testClearSensitiveData()
   {
      noPaymentTransaction.clearSensitiveData();
      assertTrue(true);
   }

   /**
    * Tests the getPaymentMethod method.
    */
   public void testGetPaymentMethod()
   {
      assertEquals(PaymentMethod.NO_PAYMENT, noPaymentTransaction.getPaymentMethod());
   }

   /**
    * Tests the getPaymentTypeCode method.
    */
   public void testGetPaymentTypeCode()
   {
      assertEquals("NoPayment", noPaymentTransaction.getPaymentTypeCode());
   }

   /**
    * Tests the getTransactionAmount method.
    */
   public void testGetTransactionAmount()
   {
      assertEquals(BigDecimal.ZERO, noPaymentTransaction.getTransactionAmount().getAmount());
   }

   /**
    * Tests the getTransactionCharge method.
    */
   public void getTransactionCharge()
   {
      assertEquals(BigDecimal.ZERO, noPaymentTransaction.getTransactionCharge().getAmount());
   }
}