/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PINPADPaymentTransactionTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-06-27 $
 *
 * $Author: Vinodha.S$
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class tests the PINPADPaymentTransaction class for valid and
 * invalid authorization codes.
 */
public class PINPADPaymentTransactionTestCase extends TestCase
{

   /** Total amount. */
   private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(100.00);

   /** Card Charges. */
   private static final BigDecimal CARD_CHARGE = BigDecimal.valueOf(0.00);

   /** The transaction amount. */
   private Money transactionAmount = new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));

   /** The card charge. */
   private Money transactionCharge = new Money(CARD_CHARGE, Currency.getInstance(GBP));

   /** The payment type code. */
   private String paymentTypeCode = "VC";

   /** The Constant GBP. */
   private static final String GBP = "GBP";

   /** The constant for merchant copy.  */
   private String merchantCopy = "";

   /** The constant for customer copy.  */
   private String customerCopy = "";

   /** The character array holing cv2. */
   private char[] validPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1',
         '1', '1', '1'};

   /** The character array holing a invalid card number. */
   private char[] invalidPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1',
         '1', '8', '8'};

   /** The <code>Card</code> object. */
   private CpCard card;

   /**
   * Setting valid card details.
   */
   private void setValidDetails()
   {
      card = new CpCard(validPan);
   }

   /**
    * Setting invalid card details.
    */
   private void setInvalidDetails()
   {
      card = new CpCard(invalidPan);
   }

   /**
    * Tests for valid authorization code.
    */
   public void testAuthCode()
   {
      String authCode = "1234567890123456";
      setValidDetails();
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test valid auth code", params);
         PINPADPaymentTransaction pinpadPaymentTransaction =
            new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
                    merchantCopy, customerCopy, transactionCharge, card);
         LogWriter.logDebugMessage("Test Valid auth code");
         pinpadPaymentTransaction.validate();
         LogWriter.logMethodEnd("test Valid auth code");
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Tests for Invalid authCode which is less than max length of the
    * authCode.
    */
   public void testInValidAuthCodeForMinLen()
   {
      setInvalidDetails();
      String authCode = "12";
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test valid auth code", params);
         PINPADPaymentTransaction pinpadPaymentTransaction =
               new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
                     merchantCopy, customerCopy, transactionCharge, card);
         LogWriter.logDebugMessage("test Valid auth code");
         pinpadPaymentTransaction.validate();
         fail("Exception while validating auth code");
         LogWriter.logMethodEnd("Test auth code ");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("Successfully validated the auth code", true);
      }
   }

   /**
    * Tests for Invalid authCode which is more than max length of the
    * authCode.
    */
   public void testInValidAuthCodeForMaxLen()
   {
      setInvalidDetails();
      String authCode = "123456789012345678901234567";
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test valid auth code", params);
         PINPADPaymentTransaction pinpadPaymentTransaction =
             new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
                     merchantCopy, customerCopy, transactionCharge, card);
         LogWriter.logDebugMessage("test Valid auth code");
         pinpadPaymentTransaction.validate();
         fail("exception while validating auth code");
         LogWriter.logMethodEnd("test auth code ");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("Successfully validated the auth code", true);
      }
   }

   /**
    * Tests for blank authCode.
    */
   public void testBlankAuthCode()
   {
      String authCode = "";
      setInvalidDetails();
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test invalid auth code", params);
         PINPADPaymentTransaction pinpadPaymentTransaction =
             new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
                     merchantCopy, customerCopy, transactionCharge, card);
         LogWriter.logDebugMessage("test invalid auth code");
         pinpadPaymentTransaction.validate();
         LogWriter.logErrorMessage("error while validating auth code");
         fail("Exception while validating auth code");
         LogWriter.logMethodEnd("test auth code ");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the auth code", true);
      }
   }

   /**
    * Tests for null authCode.
    */
   public void testNullAuthCode()
   {
      String authCode = null;
      setInvalidDetails();
      try
      {
         PINPADPaymentTransaction pinpadPaymentTransaction =
            new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
                  merchantCopy, customerCopy, transactionCharge, card);
         pinpadPaymentTransaction.validate();
         fail("Exception while validating auth code");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the auth code", true);
      }
   }

   /**
    * Tests the essential transaction data.
    */
   public void testGetEssentialTransactionData()
   {
      String authCode = "1234567890123456";
      setValidDetails();
      try
      {
         LogWriter.logMethodStart("test essential transaction data");
         PINPADPaymentTransaction pinpadPaymentTransaction =
            new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
                    merchantCopy, customerCopy, transactionCharge, card);
         LogWriter.logDebugMessage("Test essential transaction data");
         pinpadPaymentTransaction.validate();
         assertNotNull(pinpadPaymentTransaction.getEssentialTransactionData());
         assertNotNull(pinpadPaymentTransaction.getPaymentTypeCode());
         assertNotNull(pinpadPaymentTransaction.getTransactionAmount());
         assertNotNull(pinpadPaymentTransaction.getTransactionReference());

         LogWriter.logMethodEnd("test essential transaction data");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logErrorMessage("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Tests the clearSensitiveData method.
    */
   public void testClearSensitiveData()
   {
      String authCode = "1234567890123456";
      setValidDetails();
      PINPADPaymentTransaction pinpadPaymentTransaction =
         new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
                 merchantCopy, customerCopy, transactionCharge, card);
      pinpadPaymentTransaction.clearSensitiveData();
      assertTrue(true);
   }
}
