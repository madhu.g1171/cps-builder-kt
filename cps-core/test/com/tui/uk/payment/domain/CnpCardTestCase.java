/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CnpCardTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-06-27 $
 *
 * $Author: Vinodha.S $
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import junit.framework.TestCase;

import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.utility.TestDataSetup;

/**
 * This Class tests the Card class.
 */
public class CnpCardTestCase extends TestCase
{
   /** The constant THREE. */
   private static final int THREE = 3;

   /** The character array holding a valid card number. */
   private char[] validPan = "4444333322221111".toCharArray();

   /** The character array holding a valid card number. 343434200000005  */
   private char[] validPanAE = "343434200000005".toCharArray();

   /** The character array holding valid maestro card number. */
   private char[] validMaestro = "500163001001321".toCharArray();

   /** The character array holding a blank card number. */
   private char[] blankPan = {};

   /** The name on card. */
   private String nameOncard = "xxxxxxxx";

   /** The expiry date. */
   private String expiryDate = "";

   /** The expiry date. */
   private String invalidExpiryMonthMoreThanTwoDigits = "123 /12";

   /** The expiry date. */
   private String singleDigitExpiryDate = "8/9";

   /** The invalid expiry date. */
   private String invalidExpiryDate = "14/07";

   /** The character array holding cv2. */
   private char[] cv2 = {'4', '4', '4' };

   /** The character array holing cv2. */
   private char[] invalidcv2 = {'4', '4' };

   /** The character array holing cv2. */
   private char[] nullcv2 = {};

   /** The card type. */
   private String cardType = "visa";

   /** The card type. */
   private String cardTypeAE = "AMERICAN_EXPRESS";

   /** The card type for maestro card. */
   private String cardTypeMaestro = "SWITCH";

    /** The post code. */
   private String postCode = "CV24EB";

   /** The Card object. */
   private CnpCard card;

   /** The minimum security code length. */
   private int minSecurityCodeLength = THREE;

   /** The maximum security code length. */
   private int maxSecurityCodeLength = THREE;

   @Override
   protected void setUp() throws Exception
   {
	  super.setUp();
	  expiryDate= TestDataSetup.getExpiryMonthAndYear();
   }
   /**
    * Setting null card details.
    */
   private void setBlankCardNumber()
   {
      card = new CnpCard(blankPan, nameOncard, expiryDate, cv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting invalid name on card details.
    */
   private void setInvalidNameOnCard()
   {
      card = new CnpCard(validPan, "*hg^h&jgh", expiryDate, cv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting null name on card details.
    */
   private void setNullNameOnCard()
   {
      card = new CnpCard(validPan, null, expiryDate, cv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting blank name on card details.
    */
   private void setBlankNameOnCard()
   {
      card = new CnpCard(validPan, "", expiryDate, cv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting invalid cv2 on card details.
    */
   private void setInvalidcv2()
   {
      card = new CnpCard(validPan, nameOncard, expiryDate, invalidcv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting null cv2 on card details.
    */
   private void setNullcv2()
   {
      card = new CnpCard(validPan, nameOncard, expiryDate, nullcv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Sets the invalid cardtype.
    */
   private void setBlankCardtype()
   {
      card = new CnpCard(validPan, nameOncard, expiryDate, cv2, postCode, "",
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting invalid cv2 on card details.
    */
   private void setNullCardtype()
   {
      card = new CnpCard(validPan, nameOncard, expiryDate, cv2, postCode, null,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting invalid card type .
    */
   private void setInvalidCardtype()
   {
      card = new CnpCard(validPan, nameOncard, expiryDate, cv2, postCode, "a(*&",
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting valid card type .
    */
   private void setValidCardtype()
   {
      card = new CnpCard(validPanAE, nameOncard, expiryDate, cv2, postCode, cardTypeAE,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting invalid number card type .
    */
   private void setInvalidNumberCardtype()
   {
      card = new CnpCard(validPan, nameOncard, expiryDate, cv2, postCode, "123456",
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting invalid expiry date .
    */
   private void setInvalidExpiryDate()
   {
      card = new CnpCard(validPan, nameOncard, invalidExpiryDate, cv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting invalid expiry month .
    */
   private void setInvalidExpiryMonthMoreThanTwoDigits()
   {
      card =
         new CnpCard(validPan, nameOncard, invalidExpiryMonthMoreThanTwoDigits, cv2, postCode,
            cardType, minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting valid card details .
    */
   private void setValidCardDetails()
   {
      card = new CnpCard(validPan, nameOncard, expiryDate, cv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting expiry date .
    */
   private void setSingleDigitExpiryDate()
   {
      card = new CnpCard(validPan, nameOncard, singleDigitExpiryDate, cv2, postCode, cardType,
         minSecurityCodeLength, maxSecurityCodeLength);
   }

   /**
    * Setting capture method and track2Data.
    */
   private void setOtherData()
   {
      setValidCardDetails();
      card.setCaptureMethod("CNP");
      card.setTrack2Data("sssss");
   }

   /**
    * Test for valid card details .
    */
   public void testValidCardDetails()
   {
      try
      {
         setValidCardDetails();
         ((CnpCard) card).setCv2AVSEnabled(Boolean.FALSE);
         card.validate();
         assertTrue("successfully validated the card details", true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Test for valid card type .
    */
   public void testValidAmericanExpressCardType()
   {
      try
      {
         setValidCardtype();
         ((CnpCard) card).setCv2AVSEnabled(Boolean.FALSE);
         card.validate();
         assertTrue("successfully validated the card details", true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Test for valid maestro card.
    */
   public void testValidMaestroCard()
   {
      try
      {
         card = new CnpCard(validMaestro, nameOncard, expiryDate, cv2, postCode, cardTypeMaestro,
            minSecurityCodeLength, maxSecurityCodeLength);
         ((CnpCard) card).setCv2AVSEnabled(Boolean.FALSE);
         card.validate();
         assertTrue("Successfully validated maestro card details", true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception during validation." + pve.getMessage());
      }
   }

   /**
    * Test for invalid name on the card .
    */
   public void testInvalidNameOnCard()
   {
      try
      {
         setInvalidNameOnCard();
         card.validate();
         LogWriter.logWarningMessage("Fail: should have got exception.");
         fail("Exception is not thrown, when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated the method ", true);
      }
   }

   /**
    * Test for null name on the card .
    */
   public void testNullNameOnCard()
   {
      try
      {
         setNullNameOnCard();
         card.validate();
         LogWriter.logWarningMessage("Fail: Should have got exception.",
              CnpCardTestCase.class.getName());
         fail("Exception is not thrown ,when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), CnpCardTestCase.class.getName(), pve);
         assertTrue("successfully validated the null name on card", true);
      }
   }

   /**
    * Test for blank name on the card .
    */
   public void testBlankNameOnCard()
   {
      try
      {
         setBlankNameOnCard();
         card.validate();
         LogWriter.logWarningMessage("Fail: Should have got exception.");
         fail("Exception is not thrown ,when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated ", true);
      }
   }

   /**
    * Test for invalid cv2 of the card .
    */
   public void testInvalidcv2()
   {
      try
      {
         setInvalidcv2();
         card.validate();
         fail("exception is not thrown when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated ", true);
      }
   }

   /**
    * Test for null cv2 of the card .
    */
   public void testNullcv2()
   {
      try
      {
         setNullcv2();
         card.validate();
         fail("exception is not thrown when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated ", true);
      }
   }

   /**
    * Test for invalid card type .
    */
   public void testInvalidCardtype()
   {
      try
      {
         setInvalidCardtype();
         card.validate();
         LogWriter.logWarningMessage("Fail: should have got exception.");
         fail("Exception is not thrown ,when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("Successfully validated the method", true);
      }
   }

   /**
    * Test for invalid number card type .
    */
   public void testInvalidNumberCardtype()
   {
      try
      {
         setInvalidNumberCardtype();
         card.validate();
         fail("Exception is not thrown, when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated the test case", true);
      }
   }

   /**
    * Test for single digit expiry date .
    */
   public void testSingleDigitExpiryDate()
   {
      try
      {
         setSingleDigitExpiryDate();
         card.validate();
         LogWriter.logWarningMessage("Fail :should have got exception.");
         fail("Exception is not thrown, when it should be thrown.");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated the method", true);
      }
   }

   /**
    * Test for invalid expiry date .
    */
   public void testInvalidExpiryDate()
   {
      try
      {
         setInvalidExpiryDate();
         card.validate();
         LogWriter.logWarningMessage("Fail :should have got exception.");
         fail("Exception is not thrown,when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated the method", true);
      }
   }

   /**
    * Test for invalid expiry date .
    */
   public void testBlankExpiryMonth()
   {
      try
      {
         setInvalidExpiryMonthMoreThanTwoDigits();
         card.validate();
         LogWriter.logWarningMessage("Fail :should have got exception.");
         fail("Exception is not thrown,when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated the method", true);
      }
   }

   /**
    * Test for valid solo card type .
    */
   /*public void testValidSoloCardtype()
   {
      try
      {
         setSoloCardtype();
         card.validate();
         fail("Exception is not thrown,when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the cardtype", true);
      }
   }*/

   /**
    * Test for invalid solo card type .
    */

   /* public void testInvalidSoloCardtype()
   {
     try
     {

        setInvalidSoloCardtype();
        card.validate();
        fail("Exception is not     thrown,when it should be thrown");
     }
     catch (PaymentValidationException pve)
     {
        assertTrue("successfully validated the card type", true);
     }
   }*/

   /**
    * Test for Blank card type .
    */
   public void testBlankCardtype()
   {
      try
      {
         setBlankCardtype();
         card.validate();
         LogWriter.logWarningMessage("Fail, should have got exception.");
         fail("Exception is not thrown when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("Successfully validated", true);
      }
   }

   /**
    * Test for null card type .
    */
   public void testNullCardtype()
   {
      try
      {
         setNullCardtype();
         card.validate();
         LogWriter.logWarningMessage("Fail, should have got exception.");
         fail("Exception is not thrown when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated", true);
      }
   }

   /**
    * Test for blank card number .
    */
   public void testBlankCardNumber()
   {
      try
      {
         setBlankCardNumber();
         card.validate();
         LogWriter.logWarningMessage("Fail, should have got exception.");
         fail("Exception is not thrown when it should be thrown");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated", true);
      }
   }

   /**
    * Test for masked card number.
    */
   public void testMaskedCardNumber()
   {
      setValidCardDetails();
      assertEquals(card.getMaskedCardNumber().length(), card.getPan().length);
   }
   /**
    * Test for dates.
    */
   public void testDates()
   {
      try
      {
         card = new CnpCard(validPan, nameOncard, "05/09", cv2, postCode, cardType,
            minSecurityCodeLength, maxSecurityCodeLength);
         card.validate();
         card = new CnpCard(validPan, nameOncard, "04/56", cv2, postCode, cardType,
            minSecurityCodeLength, maxSecurityCodeLength);
         card.validate();
         card = new CnpCard(validPan, nameOncard, "04/09", cv2, postCode, cardType,
            minSecurityCodeLength, maxSecurityCodeLength);
         card.validate();
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("successfully validated", true);
      }
   }

   /**
    * Test for valid capture method.
    */
   public void testValidCaptureMethod()
   {
      setOtherData();
      assertEquals("CNP", card.getCaptureMethod());
   }

   /**
    * Test for valid track2Data.
    */
   public void testValidTrack2Data()
   {
      setOtherData();
      assertEquals("sssss", card.getTrack2Data());
   }

   /**
    * Test for max securityCodeLength.
    */
   public void testMaxSecurityCodeLength()
   {
      setOtherData();
      assertEquals(maxSecurityCodeLength, card.getMaxSecurityCodeLength());
   }

   /**
    * Test for min SecurityCodeLength.
    */
   public void testMinSecurityCodeLength()
   {
      setOtherData();
      assertEquals(minSecurityCodeLength, card.getMinSecurityCodeLength());
   }

}