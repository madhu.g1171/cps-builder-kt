/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentTransactionFactoryTestCase.java,v $
 *
 * $Revision:  $
 *
 * $Date: 2008-09-11 04:35:06 $
 *
 * $author : Venkat $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.CardType;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 *  This is TestCase for PaymentTransactionFactory Class.
 */
public class PaymentTransactionFactoryTestCase extends TestCase
{
    /** Set the value for Number Of Transactions. */
    private int contributionCount = 0;

    /** Setting the value for index. */
    private static int index = 0;

    /** A map containing values entered by the user. */
    private Map<String, String> requestParameterMap = new HashMap<String, String>();

    /** Instance variable for Booking Component. */
    private BookingInfo bookingInfo = null;

    /** Setting the value for Transaction Amount. */
    private String refundrTransactionAmount = "10";

    /** the client domain url. */
    private String clientDomainURL = "test";

    /** Set the value for Total Amount. */
    private Money totalAmount = new Money(new BigDecimal("1010"), Currency.getInstance("GBP"));

    /** Setting the value for Transaction Amount. */
    private String transactionAmount = "1000";

    /** Setting the value for Transaction Amount. */
    private String  zerotransactionamount = "0";

    /** Setting the value for Transaction Amount. */
    private String cashtransactionAmount = "1010";

    /** Setting the value for Charge. */
    private String charge = "10";

    /** Setting the value for Payment Type Code. */
    private String paymentTypeCode = "VC";

    /** Setting the message for transaction should not be null. */
    private String msg1 = "paymentTransactions should not be null";

    /** Setting the value for AUTH_CODE. */
    private String authcode = "444";

    /** Setting the value for selected payment method. */
    private static final String SELECTED_PAYMENT_METHOD = "Card";

    /** Setting the value for selected card. */
    private static final String SELECTED_CARD = "SOLO";

    /** The constant THREE. */
    private static final int THREE = 3;

    /**
     * Sets request path for the class under test.
     *
     * @throws Exception as the base class does.
     */
    protected void setUp() throws Exception
    {
       LogWriter.logInfoMessage("Test Case Started");
       BookingComponent bookingComponent = new BookingComponent(
          ClientApplication.ThomsonBuildYourOwn, clientDomainURL, totalAmount);
       Map<String, List<PaymentType>> paymentType = new HashMap<String, List<PaymentType>>();
       PaymentType type =
          new PaymentType(SELECTED_CARD, "MasterCard Express", SELECTED_PAYMENT_METHOD);
       CardType cardType = new CardType(THREE, THREE);
       type.setCardType(cardType);
       List<PaymentType> payList = new ArrayList<PaymentType>();
       payList.add(type);
       paymentType.put("CNP", payList);
       bookingComponent.setPaymentType(paymentType);
       bookingInfo = new BookingInfo(bookingComponent);
       bookingInfo.setCalculatedPayableAmount(totalAmount);
       TransactionTrackingData trackingData = new TransactionTrackingData("", "");
       bookingInfo.setTrackingData(trackingData);
    }

   /**
    * Test the Cash Payment Transaction.
    *
    * @throws PaymentValidationException is thrown whenever any payment
    * transaction validation fails in the server side.
    */
   public void testCashPaymentTransaction() throws PaymentValidationException
   {
      putRequestParameters(requestParameterMap,
                PaymentMethod.CASH.getCode(),
                cashtransactionAmount, "",
                "4578", paymentTypeCode, index);
      PaymentTransaction paymentTransaction = PaymentTransactionFactory
                .getPaymentTransactions(contributionCount, requestParameterMap,
                        bookingInfo);
      paymentTransaction.setTrackingData(bookingInfo.getTrackingData().toString());
      paymentTransaction.getPaymentTypeCode();
      paymentTransaction.getTransactionAmount();
      assertNotNull(msg1, paymentTransaction);
    }

   /**
    * Test the Cash Payment Transaction.
    *
    * @throws PaymentValidationException is thrown whenever any payment
    * transaction validation fails in the server side.
    */
   public void testChequePaymentTransaction() throws PaymentValidationException
   {
      putRequestParameters(requestParameterMap,
                PaymentMethod.CHEQUE.getCode(),
                transactionAmount, charge,
                "14567" , paymentTypeCode, index);
      PaymentTransaction paymentTransactions = PaymentTransactionFactory
                .getPaymentTransactions(contributionCount, requestParameterMap,
                        bookingInfo);
      paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
      paymentTransactions.getPaymentMethod();
      paymentTransactions.getTransactionAmount();
      paymentTransactions.getTransactionCharge();
      paymentTransactions.getEssentialTransactionData();
      paymentTransactions.getPaymentTypeCode();
      assertNotNull(msg1, paymentTransactions);
   }

   /**
    * Test the Voucher Payment Transaction.
    *
    * @throws PaymentValidationException is thrown whenever any payment
    * transaction validation fails in the server side.
    */
   public void testVoucherPaymentTransaction() throws PaymentValidationException
   {
      putRequestParameters(requestParameterMap,
                PaymentMethod.VOUCHER.getCode(),
                transactionAmount, charge,
                "12345", paymentTypeCode, index);
      requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.VOUCHER_INDEX, "0");
        requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.VOUCHER_CODE + "[0]", "12345678");
        requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.VOUCHER_AMOUNT + "[" + index + "]" , "50.25");
        PaymentTransaction paymentTransactions = PaymentTransactionFactory
        .getPaymentTransactions(contributionCount, requestParameterMap,
                bookingInfo);
        paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
        assertNotNull(msg1, paymentTransactions);
    }

   /**
    * Test the Data Cash Payment Transaction.
    *
    * @throws PaymentValidationException is thrown whenever any payment
    * transaction validation fails in the server side.
    */
   public void testDataCashPaymentTransaction() throws PaymentValidationException
    {
       putRequestParameters(requestParameterMap,
                PaymentMethod.DATACASH.getCode(),
                transactionAmount, charge,
                authcode, "SOLO", index);
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.CARD_NUMBER, "633499110000000003");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.NAME_ON_CARD, "VENKAT");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.EXPIRY_MONTH, "08");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.EXPIRY_YEAR, "20");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.SECURITY_CODE, authcode);
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.POST_CODE, "ba21db");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.ISSUE_NUMBER, "1");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.START_MONTH, "05");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.START_YEAR, "04");
       PaymentTransaction paymentTransactions = PaymentTransactionFactory
                .getPaymentTransactions(contributionCount, requestParameterMap,
                        bookingInfo);
       paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
       assertNotNull(msg1, paymentTransactions);
    }

   /**
    * Test the Post Payment Transaction.
    *
    * @throws PaymentValidationException is thrown whenever any payment
    * transaction validation fails in the server side.
    */
   public void testPostPaymentTransaction() throws PaymentValidationException
    {
       putRequestParameters(requestParameterMap,
                PaymentMethod.POST_PAYMENT.getCode(),
                transactionAmount, charge,
                authcode, "VISA", index);
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.CARD_NUMBER, "4242425000000009");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.NAME_ON_CARD, "VENKAT");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.EXPIRY_MONTH, "08");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.EXPIRY_YEAR, "2010");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.SECURITY_CODE, authcode);
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.POST_CODE, "ba21db");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.ISSUE_NUMBER, "1");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.START_MONTH, "05");
       requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.START_YEAR, "2004");
       try
       {
          PaymentTransaction paymentTransactions = PaymentTransactionFactory
          .getPaymentTransactions(contributionCount, requestParameterMap,
                bookingInfo);
          paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
          assertNotNull(msg1, paymentTransactions);
       }
       catch (PaymentValidationException e)
       LogWriter.logInfoMessage(e);
       }
    }

   /**
    * Test the Card Payment Transaction.
    *
    * @throws PaymentValidationException is thrown whenever any payment
    * transaction validation fails in the server side.
    */
    public void testCardPaymentTransaction() throws PaymentValidationException
    {
       putRequestParameters(requestParameterMap,
                PaymentMethod.PDQ.getCode(),
                transactionAmount, charge,
                "1234", "VISA", index);
       PaymentTransaction paymentTransactions = PaymentTransactionFactory
                .getPaymentTransactions(contributionCount, requestParameterMap,
                        bookingInfo);
       paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
       assertNotNull(msg1, paymentTransactions);
    }

    /**
     * Test Card Pin Pad Payment Transaction.
     *
     * @throws PaymentValidationException is thrown whenever any payment
     * transaction validation fails in the server side.
     */
    public void testCardPinPadPaymentTransaction() throws PaymentValidationException
    {
       putRequestParameters(requestParameterMap,
                PaymentMethod.PIN_PAD.getCode(),
                transactionAmount, charge,
                "1234123412341234", "VISA", index);
       requestParameterMap.put(PaymentConstants.PAYMENT + index
          + PaymentConstants.CARD_NUMBER, "4242425000000009");
       PaymentTransaction paymentTransactions = PaymentTransactionFactory
                .getPaymentTransactions(contributionCount, requestParameterMap,
                        bookingInfo);
       paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
       assertNotNull(msg1, paymentTransactions);
    }

    /**
     * Test the Default Payment Transaction.
     *
     * @throws PaymentValidationException is thrown whenever any payment
     * transaction validation fails in the server side.
     */
    public void testDefaultPaymentTransaction() throws PaymentValidationException
    {
       putRequestParameters(requestParameterMap,
                PaymentMethod.CASH.getCode(),
                cashtransactionAmount, "",
                authcode, paymentTypeCode, index);
       try
       {
           PaymentTransaction paymentTransactions = PaymentTransactionFactory
                    .getPaymentTransactions(contributionCount, requestParameterMap,
                            bookingInfo);
           paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
           assertNotNull(msg1, paymentTransactions);
       }
       catch (PaymentValidationException e)
       {
       LogWriter.logInfoMessage(e);
       }
    }

    /**
     * Test the Amount is Valid or Not.
     *
     * @throws PaymentValidationException is thrown whenever any payment
     * transaction validation fails in the server side.
     */
    public void testValidateAmount() throws PaymentValidationException
    {
       putRequestParameters(requestParameterMap,
                PaymentMethod.CASH.getCode(),
                transactionAmount, "venkat",
                authcode, paymentTypeCode, index);
       try
       {
           PaymentTransaction paymentTransactions = PaymentTransactionFactory
                    .getPaymentTransactions(contributionCount, requestParameterMap,
                            bookingInfo);
           paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
           assertNotNull(msg1, paymentTransactions);
       }
       catch (PaymentValidationException e)
       {   LogWriter.logInfoMessage(e);
       }
    }

    /**
     * Test the Value from Map is correct or Not.
     *
     * @throws PaymentValidationException is thrown whenever any payment
     * transaction validation fails in the server side.
     */
    public void testGetValueFromMap() throws PaymentValidationException
    {
       putRequestParameters(requestParameterMap,
                PaymentMethod.CASH.getCode(),
                transactionAmount, charge,
                "123456", paymentTypeCode, index);
       requestParameterMap.remove(PaymentConstants.PAYMENT + index
                + PaymentConstants.TRANSACTION_AMOUNT);
       try
       {
           PaymentTransaction paymentTransactions = PaymentTransactionFactory
                    .getPaymentTransactions(contributionCount, requestParameterMap,
                            bookingInfo);
           paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
           assertNotNull(msg1, paymentTransactions);
       }
       catch (PaymentValidationException e)
       {   LogWriter.logInfoMessage(e);
       }
    }

    /**
     * Test the NOPayment Transaction.
     *
     * @throws PaymentValidationException is thrown whenever any payment
     * transaction validation fails in the server side.
     */
     public void testNoPaymentTransaction() throws PaymentValidationException
     {
        putRequestParameters(requestParameterMap,
           PaymentMethod.NO_PAYMENT.getCode(),
           zerotransactionamount, "",
           "12345", paymentTypeCode, index);
        bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal(zerotransactionamount),
         Currency.getInstance("GBP")));
        PaymentTransaction paymentTransactions = PaymentTransactionFactory
                 .getPaymentTransactions(contributionCount, requestParameterMap,
                         bookingInfo);
        paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
        assertNotNull(msg1, paymentTransactions);
     }

     /**
      * Test the RefundHistoricTransaction.
      *
      * @throws PaymentValidationException is thrown whenever any payment
      * transaction validation fails in the server side.
      */
      public void testDataCashHistoricTransaction() throws PaymentValidationException
      {
         putRequestParameters(requestParameterMap,
            PaymentMethod.REFUND_CARD.getCode(),
            refundrTransactionAmount, "",
            "12345", paymentTypeCode, index);
         requestParameterMap.put(PaymentConstants.PAYMENT + index
            + PaymentConstants.DATACASH_REFERENCE, "4100200061461780");
         bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal(refundrTransactionAmount),
          Currency.getInstance("GBP")));
         PaymentTransaction paymentTransactions = PaymentTransactionFactory
                  .getPaymentTransactions(contributionCount, requestParameterMap,
                          bookingInfo);
         paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
         assertNotNull(msg1, paymentTransactions);
      }

    /**
    * Test the Data Cash Payment Transaction.
    *
    * @throws PaymentValidationException is thrown whenever any payment
    * transaction validation fails in the server side.
    */
   public void testGiftCardPaymentTransaction() throws PaymentValidationException
    {
      putRequestParameters(requestParameterMap, PaymentMethod.GIFT_CARD.getCode(),
         transactionAmount, "0", authcode, "GiftCard", index);

      requestParameterMap.put(PaymentConstants.PAYMENT + index
                + PaymentConstants.CARD_NUMBER, "6335860000000018");
      requestParameterMap.put(PaymentConstants.PAYMENT + index
         + PaymentConstants.SECURITY_CODE, "1124");

      PaymentTransaction paymentTransactions = PaymentTransactionFactory
         .getPaymentTransactions(contributionCount, requestParameterMap, bookingInfo);
      paymentTransactions.setTrackingData(bookingInfo.getTrackingData().toString());
      assertNotNull(msg1, paymentTransactions);
    }

    /**
     * Put the Parameters into request Parameter Map.
     *
     * @param requestParameters is map to pass the values.
     * @param paymentmethod is Payment Method.
     * @param transamt is Transaction Amount.
     * @param chargeAmount is Transaction Charge Amount.
     * @param referenceNumber is Reference Number.
     * @param paymenttypecode is Payment Type Code.
     * @param itr is index value.
     */
    private void putRequestParameters(
            Map<String, String> requestParameters, String paymentmethod,
            String transamt, String chargeAmount, String referenceNumber,
            String paymenttypecode, int itr)
    {
        requestParameterMap.put(PaymentConstants.PAYMENT + itr
                + PaymentConstants.PAYMENT_METHOD, paymentmethod);
        requestParameterMap.put(PaymentConstants.PAYMENT + itr
                + PaymentConstants.TRANSACTION_AMOUNT, transamt);
        requestParameterMap.put(PaymentConstants.PAYMENT + itr
                + PaymentConstants.CHARGE, chargeAmount);
        requestParameterMap.put(PaymentConstants.PAYMENT + itr
                + PaymentConstants.AUTH_CODE, referenceNumber);
        requestParameterMap.put(PaymentConstants.PAYMENT + itr
                + PaymentConstants.PAYMENT_TYPE_CODE, paymenttypecode);
    }
}
