/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening;

import junit.framework.TestCase;

import com.tui.uk.payment.service.fraudscreening.exception.FraudScreeningServiceException;

/**
 * This test case tests the FraudScreeningResponse object for different response xml strings.
 *
 * @author sindhushree.g
 *
 */
public class FraudScreeningResponseTestCase extends TestCase
{

   /**
    * This method tests if the recommendation code is populated properly or not.
    */
   public void testFraudScreeningResponse()
   {
      String responseXml = "<transaction-results>"
        + "<transaction-id>1</transaction-id>"
        + "<cross-reference>1cdc0675-e6b8-4ddb-83d4-2d93ffd9a414</cross-reference>"
        + "<rules-tripped></rules-tripped>"
        + "<total-score>0</total-score>"
        + "<recommendation-code>Accept</recommendation-code>"
        + "<remarks></remarks>"
        + "</transaction-results>";
      try
      {
         FraudScreeningResponse fraudScreeningResponse = new FraudScreeningResponse(responseXml);
         assertEquals("Accept", fraudScreeningResponse.getRecommendationCode());
      }
      catch (FraudScreeningServiceException fse)
      {
         fail(fse.getMessage());
      }
   }

   /**
    * This method tests if exception is thrown, when the response xml is invalid.
    */
   public void testInvalidResponse()
   {
      String responseXml = "<transaction-results"
            + "<transaction-id>1</transaction-id>"
            + "<cross-reference>1cdc0675-e6b8-4ddb-83d4-2d93ffd9a414</cross-reference>"
            + "<rules-tripped></rules-tripped>"
            + "<total-score>0</total-score>"
            + "<recommendation-code>Accept</recommendation-code>"
            + "<remarks></remarks>"
            + "</transaction-results>";

      try
      {
         new FraudScreeningResponse(responseXml);
         fail("Expected exception");
      }
      catch (FraudScreeningServiceException fse)
      {
         assertTrue(true);
      }
   }

}
