package com.tui.uk.payment.service.fraudscreening;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.tui.uk.payment.service.fraudscreening.FraudScreeningCardData;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningRequest;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningResponse;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningServiceImpl;
import com.tui.uk.payment.service.fraudscreening.domain.AuthInfo;
import com.tui.uk.payment.service.fraudscreening.domain.ContactInformation;
import com.tui.uk.payment.service.fraudscreening.domain.FraudScreeningCard;
import com.tui.uk.payment.service.fraudscreening.domain.OrderInformation;
import com.tui.uk.payment.service.fraudscreening.domain.PaymentInformation;
import com.tui.uk.payment.service.fraudscreening.exception.FraudScreeningServiceException;

/**
*
* TestCase for FraudScreeningRequest Class.
*
*/
//CHECKSTYLE:OFF
public class FraudScreeningServiceImplTestCase extends TestCase
{

	  /**
        * Testing getter and setter.
        * */
       public void test()
       {

           AuthInfo authInfo = new AuthInfo("004587", "ACCEPTED", "N", "4100201011534793",
                   "ThomsonBYO962027918ea503ad3", "001", "","","","","","");

           FraudScreeningCard fraudScreeningCard = new
            FraudScreeningCard("4242425000000009".toCharArray(),"Test","TODO");

           ContactInformation contactInformation =
               new ContactInformation("18, Twyford Drive", "","Luton","","","","");
           FraudScreeningCardData fraudScreeningCardData = new
            FraudScreeningCardData(authInfo, fraudScreeningCard, "GBP",new BigDecimal(7.50),
                    new BigDecimal(307.50), contactInformation);

            List<FraudScreeningCardData> fraudScreeningCardDatalist =
               new ArrayList<FraudScreeningCardData>();

           fraudScreeningCardDatalist.add(fraudScreeningCardData);

           PaymentInformation paymentInformation =
               new PaymentInformation(fraudScreeningCardDatalist,"00001234","50.00");

           OrderInformation orderInformation = new OrderInformation("3345",
        	         "TODO", "307.50");
            FraudScreeningRequest fraudScreeningRequest = new FraudScreeningRequest("10174036ThomsonBYO10E40C25248861438AB406A14479B363iscapethomsonp07e");
            fraudScreeningRequest.setOrderInformation(orderInformation);
             FraudScreeningServiceImpl fraudScreeningServiceImpl =
            	 new FraudScreeningServiceImpl(fraudScreeningRequest);
            try
            {
            	 FraudScreeningResponse response = fraudScreeningServiceImpl.send();

			}
            catch (FraudScreeningServiceException e)
            {

            }

       }

}
