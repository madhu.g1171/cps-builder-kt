package com.tui.uk.payment.service.fraudscreening.domain;

import junit.framework.TestCase;

/**
*
* TestCase for AuthInfo Class.
*
*/
//CHECKSTYLE:OFF
public class AuthInfoTestCase extends TestCase
{
   /** The Constant for AuthInfo. */
   AuthInfo authInfo;

       /**
        * Setup method for AuthInfo.
        */
       @Override

   protected void setUp()
   {
            authInfo = new AuthInfo("004587", "ACCEPTED", "N", "4100201011534793",
                   "ThomsonBYO962027918ea503ad3", "001", "","","","","", "");


   }

     /**
        * Testing getter and setter.
        * */
       public void testAuthInfo()
       {

              assertEquals("001", authInfo.getPaymentGatewayResponseCode()) ;
       }


}
