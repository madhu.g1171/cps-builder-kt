/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: Jaleel$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

import junit.framework.TestCase;

/**
 * This test case tests the FraudScreeningResponse object for different response xml strings.
 *
 * @author Jaleel
 *
 */
public class CruiseTestCase extends TestCase
{

   /**
    * This method tests if the recommendation code is populated properly or not.
    */
   public void testCruiseRequest()
   {
      String responseXml = "<Cruise><cruiseName>TestCruise</cruiseName>"
                           + "<sellingCode>100</sellingCode>"
                           + "<portOfCallResort>spain</portOfCallResort>"
                           + "<stayDuration>5</stayDuration></Cruise>";

      Cruise cruise = new Cruise("TestCruise", "100", "spain", "5");
      assertEquals(responseXml, cruise.toString());
   }

   /**
    * This method tests if the recommendation code is populated properly or not.
    */
   public void testEmptyRequest()
   {
       String responseXml = "<Cruise><cruiseName></cruiseName>"
           + "<sellingCode></sellingCode><portOfCallResort></portOfCallResort>"
           + "<stayDuration></stayDuration></Cruise>";
       Cruise cruise = new Cruise("", "", "", "");
       assertEquals(responseXml, cruise.toString());
   }

}
