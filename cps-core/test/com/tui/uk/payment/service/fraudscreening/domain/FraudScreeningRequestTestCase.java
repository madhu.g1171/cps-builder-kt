package com.tui.uk.payment.service.fraudscreening.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningCardData;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningRequest;

import junit.framework.TestCase;

/**
*
* TestCase for FraudScreeningRequest Class.
*
*/
//CHECKSTYLE:OFF
public class FraudScreeningRequestTestCase extends TestCase
{

	FraudScreeningRequest fraudScreeningRequest;
	 protected void setUp()
	   {
		 fraudScreeningRequest = new FraudScreeningRequest(
         "10174036ThomsonBYO10E40C25248861438AB406A14479B363iscapethomsonp07e");
           OrderInformation orderInformation = new OrderInformation("3345",
         "TODO", "307.50");
          fraudScreeningRequest.setOrderInformation(orderInformation);

	   }

     /**
        * Testing getter and setter.
        * */
       public void test()
       {

             String responseXml = "<?xml version='1.0' encoding='utf-8'?>"
               + "<request><bookingSessionIdentifier>"
               + "10174036ThomsonBYO10E40C25248861438AB406A14479B363iscapethomsonp07e"
               + "</bookingSessionIdentifier>" + "<orderInformation><bookingReferenceNumber>"
               + "3345</bookingReferenceNumber>" + "<bookingDateTime>TODO</bookingDateTime>"
               + "<bookingTotalPrice>307.50</bookingTotalPrice>"
               + "</orderInformation>"
               +"<pnrData><passengerDetails></passengerDetails></pnrData><pnrDetail></pnrDetail>"
               + "</request>";

           assertEquals(responseXml, fraudScreeningRequest.toString());


           AuthInfo authInfo = new AuthInfo("004587", "ACCEPTED", "N", "4100201011534793",
                   "ThomsonBYO962027918ea503ad3", "001", "","","","","", "");

           FraudScreeningCard fraudScreeningCard = new
            FraudScreeningCard("4242425000000009".toCharArray(),"Test","TODO");

           ContactInformation contactInformation =
               new ContactInformation("18, Twyford Drive", "","Luton","","","","");
           FraudScreeningCardData fraudScreeningCardData = new
            FraudScreeningCardData(authInfo, fraudScreeningCard, "GBP",new BigDecimal(7.50),
                    new BigDecimal(307.50), contactInformation);

            List<FraudScreeningCardData> fraudScreeningCardDatalist =
               new ArrayList<FraudScreeningCardData>();

           fraudScreeningCardDatalist.add(fraudScreeningCardData);

           PaymentInformation paymentInformation =
               new PaymentInformation(fraudScreeningCardDatalist,"00001234","50.00");
           fraudScreeningRequest.setPaymentInformation(paymentInformation);


       }

}
