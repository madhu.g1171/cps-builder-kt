/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: AddressFinderTestCase.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2008-05-14 09:14:04 $
 *
 * $Author: $chandramouli.b
 *
 * $Log: not supported by cvs2svn $ Revision 1.2 2008/05/07 14:20:29 Added Log History
 *
 */
package com.tui.uk.payment.service.capscan.addressfinder;

import junit.framework.TestCase;

import com.tui.uk.payment.exception.AddressFinderServiceException;

/**
 * To Test AddressFinder class.
 */
public class AddressFinderTestCase extends TestCase
{
   /**
    * This class determines whether the process method returns the valid address or not by receiving
    * houseNumber and the pinCode as parameters.
    */
   private AddressFinderService addressFinderService = null;

   /** This variable to hold the Address object. */

   private Address address = null;

   /**
    * Test method for
    * {@link com.tui.uk.payment.service.capscan.addressfinder.AddressFinderService#process(java.lang.String, java.lang.String)}
    * .
    */
   public void testProcess()
   {
      /*
       * try {
       * 
       * addressFinderService = new AddressFinderService(); address =
       * addressFinderService.process("1", "cv14aq"); invalidProcessTest(); branchRangeTest();
       * branchRange1Test(); branchRange2Test(); branchRange3Test(); branchRange4Test();
       * 
       * } catch (AddressFinderServiceException afse) { fail("Unexpected exception: " +
       * afse.getMessage()); }
       */
   }

   /**
    * Test method for AddressFinderConnectionImpl.
    */
   public void testAddressFinderConnection()
   {
      /*
       * try { AddressFinderConnectionImpl addressFinder = new AddressFinderConnectionImpl();
       * addressFinder.search("1", "CV24EB"); } catch (AddressFinderServiceException afse) {
       * fail("Unexpected exception: " + afse.getMessage()); }
       */
   }

   /**
    * To test invalid address.
    * 
    * @throws AddressFinderServiceException throws AddressFinderServiceException.
    */
   private void invalidProcessTest() throws AddressFinderServiceException
   {
      /*
       * address = addressFinderService.process("1", "12345"); assertNotNull(address);
       */
   }

   /**
    * Test method for
    * {@link com.tui.uk.payment.service.capscan.addressfinder.AddressFinderService#process(java.lang.String, java.lang.String)}
    * .
    * 
    * @throws AddressFinderServiceException throws addressfinderexception.
    */
   private void branchRangeTest() throws AddressFinderServiceException
   {
      /*
       * address = addressFinderService.process("67", "cv14aq");
       * assertNotNull("*** INVALID PINCODE ***:", address.getAddress());
       */
   }

   /**
    * Test method for
    * {@link com.tui.uk.payment.service.capscan.addressfinder.AddressFinderService#process(java.lang.String, java.lang.String)}
    * .
    * 
    * @throws AddressFinderServiceException throws addressfinderexception.
    */
   private void branchRange1Test() throws AddressFinderServiceException
   {
      /*
       * address = addressFinderService.process("12", "cv14aq");
       * assertNotNull("*** INVALID PINCODE ***:", address.getAddress());
       */
   }

   /**
    * Test method for
    * {@link com.tui.uk.payment.service.capscan.addressfinder.AddressFinderService#process(java.lang.String, java.lang.String)}
    * .
    * 
    * @throws AddressFinderServiceException throws addressfinderexception.
    */
   private void branchRange2Test() throws AddressFinderServiceException
   {
      /*
       * address = addressFinderService.process("10", "cv48tt");
       * assertNotNull("*** INVALID PINCODE SUBMITTED ***:", address.getAddress());
       */
   }

   /**
    * Test method for
    * {@link com.tui.uk.payment.service.capscan.addressfinder.AddressFinderService#process(java.lang.String, java.lang.String)}
    * .
    * 
    * @throws AddressFinderServiceException throws addressfinderexception.
    */
   private void branchRange3Test() throws AddressFinderServiceException
   {
      /*
       * address = addressFinderService.process("12", "lu29tp");
       * assertNotNull("*** INVALID PINCODE SUBMITTED ***:", address.getAddress());
       */
   }

   /**
    * Test method for
    * {@link com.tui.uk.payment.service.capscan.addressfinder.AddressFinderService#process(java.lang.String, java.lang.String)}
    * .
    * 
    * @throws AddressFinderServiceException throws addressfinderexception.
    */
   private void branchRange4Test() throws AddressFinderServiceException
   {
      /*
       * address = addressFinderService.process("12", "lu29ta");
       * assertNotNull("*** INVALID PINCODE SUBMITTED ***:", address.getAddress());
       */
   }
}
