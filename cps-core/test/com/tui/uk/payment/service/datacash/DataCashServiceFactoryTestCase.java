/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/

package com.tui.uk.payment.service.datacash;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import junit.framework.TestCase;

import org.junit.Test;

import com.tui.uk.log.LogWriter;

/**
 * Test case for DataCashServiceFactory.
 *
 * @author savitha.h
 *
 */
public class DataCashServiceFactoryTestCase extends TestCase
{
   /** Constant for client. */
   private static final String CLIENT = "99080800";

   /**
    * Test case for DataCashService for different client.
    */
   @Test
   public void testGetDataCashServiceForDifferentClient()
   {
      DataCashService dcService1 = DataCashServiceFactory.getDataCashService(CLIENT);
      DataCashService dcService2 = DataCashServiceFactory.getDataCashService("99540800");
      assertNotSame("Data Cash Service are not same", dcService1, dcService2);
   }

   /**
    * Test case for DataCashService for different client.
    */
   @Test
   public void testGetDataCashServiceForsameClient()
   {
      DataCashService dcService1 = DataCashServiceFactory.getDataCashService(CLIENT);
      DataCashService dcService2 = DataCashServiceFactory.getDataCashService(CLIENT);
      assertSame("Data Cash Service are same", dcService1, dcService2);
   }

   /**
    * Tests the private constructor.
    */
   public void testPrivateConstructor()
   {
      try
      {
         Constructor< ? >[] cons = DataCashServiceFactory.class.getDeclaredConstructors();
         cons[0].setAccessible(true);
         cons[0].newInstance();
      }
      catch (InstantiationException e)
      {
         LogWriter.logWarningMessage("InstantiationException : " + e);
      }
      catch (IllegalAccessException e)
      {
         LogWriter.logWarningMessage("IllegalAccessException : " + e);
      }
      catch (InvocationTargetException ite)
      {
         LogWriter.logWarningMessage("InvocationTargetException : " + ite);
      }
   }
}
