/**
 *
 */
package com.tui.uk.payment.service.datacash;

import junit.framework.TestCase;

import org.junit.Test;

import com.tui.uk.log.LogWriter;

/**
 * Test class for Mode.
 *
 * @author savitha.h
 *
 */
public class ModeTestCase extends TestCase
{

   /**
    * Test method for {@link com.tui.uk.payment.service.datacash.Mode#getCode()}.
    */
   @Test
   public void testGetCode()
   {
      assertEquals("Codes are equal", Mode.LIVE.getCode(), "LIVE");
   }

   /**
    * Test method for {@link com.tui.uk.payment.service.datacash.Mode#findByCode(java.lang.String)}.
    */
   @Test
   public void testFindByCode()
   {
      try
      {
         Mode.findByCode("TEST");
      }
      catch (IllegalArgumentException dse)
      {
         LogWriter.logWarningMessage("Got exception.", dse);
         fail("Unexpected exception while validating" + dse.getMessage());
      }
   }

   /**
    * Test for invalid package Type.
    */
   @Test
   public void testFindByCodeForInvalid()
   {
      try
      {
         Mode.findByCode("ABC");
         fail("Exception while validating the code");
      }
      catch (IllegalArgumentException dse)
      {
         assertTrue("Testcase was successfull.", true);
      }
   }

}
