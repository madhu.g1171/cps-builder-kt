/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashBinValidationServiceTestCase.java,v $
 *
 * $Revision: 1.7 $
 *
 * $Date: 2008-05-08 05:35:09 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $ Revision 1.6 2008/05/07 14:13:44
 * thomas.pm Inserted Log History
 *
 * $Author: sindhushree.g $
 *
 */
package com.tui.uk.payment.service.datacash;

import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.AUTH_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REDEEM_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CAPTUREMETHOD_CNP;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CAPTUREMETHOD_SWIPED;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * Test case for DataCashBinValidationService class.
 *
 * @author thomas.pm
 */
public class DataCashCardTestCase extends TestCase
{

   /** The Success message. */
   private static final String SUCCESS_MSG = "Test case is successful.";

   /** The fail message. */
   private static final String FAIL_MSG = "Should have got exception";

   /** Switch Card. */
   private static final String SWITCH = "SWITCH";

   /** The TRACK2DATA value. */
   private static final String TRACK2DATA =
      "7103DF03FCB5B488C22CE7E3B493DE4F20A625007049B832"
         + "7DEC37E41F6727892C8E0EBBFE58CAD5B4E56E850E4A176B2"
         + "4A64A74743177FE61E33EADAE9E64AD6B676E3DD2F2F9FC82ADBB"
         + "6CE6C42E775A732860AD0B65265C0110C6657AC4ADAAA202BC64F51EF645B93C4D3F8DB4697F"
         + "A15A3AAEDDE3FABC3ED52DBED2C92A";

   /** The character array holing a valid card number. */
   private char[] validPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1',
         '1', '1', '1' };

   /** The character array holing a invalid card number. */
   private char[] invalidPan = "1111111111111111".toCharArray();

   /** The character array holing a invalid card number. */
   /*private char[] invalidCardNumber =
      {'<', '4', '4', '<', '3', '3', '3', '<', '2', '2', '2', '2', '1',
         '1', '>', '1' };*/

   /** The character array holing a valid Switch/Solo card number. */
   private char[] validSoloPan = "633499110000000004".toCharArray();

   /** The character array holing a valid Switch/Solo card number. */
   private char[] validSwitchPan =
      {'6', '3', '3', '1', '1', '0', '2', '1', '0', '0', '0', '0', '0',
         '0', '0', '0' };

   /** The character array holing cv2. */
   private char[] cv2 = {'1', '2', '3' };

   /** String holding expiry date. */
   private String expiryDate = "05/12";

   /** String holding expiry date. */
   /*private String startDate = "05/08";*/

   /** The card type. */
   private String cardtype = "visa";

   /** The card type. */
   private String solocardtype = "Solo";

   /** Invalid Card type. */
   private String invalidCardType = "mastercard";

   /** The post code. */
   private String postCode = "CV24EB";

   /** The name on the card. */
   private String nameOnCard = "test";

   /** Declaring card. */
   private DataCashCard datacashCard;

   /**Address.*/
   private Map<String, String> address = new HashMap<String, String>();
   /**Cvv policy.*/
   private Map<String, String> cvvPolicies = new HashMap<String, String>();
   /**Address policy.*/
   private Map<String, String> addressPolicies = new HashMap<String, String>();
   /**Post code policy.*/
   private Map<String, String> postCodePolicies = new HashMap<String, String>();


   /**
    * Set a valid card number.
    */
   private void setValidPan()
   {
      datacashCard = new DataCashCard(validPan, nameOnCard, expiryDate, cv2,
         postCode, cardtype, null, address, cvvPolicies, addressPolicies, postCodePolicies );
   }

   /**
    * Set a valid switch card.
    */
   private void setValidSwitch()
   {
      datacashCard = new DataCashCard(validSwitchPan, nameOnCard, expiryDate,
         cv2, postCode, SWITCH, "1", address, cvvPolicies, addressPolicies, postCodePolicies);
   }

   /**
    * Set an invalid card number.
    */
   private void setInvalidPan()
   {
      datacashCard = new DataCashCard(invalidPan, nameOnCard, expiryDate, cv2,
         postCode, cardtype, null, address, cvvPolicies, addressPolicies, postCodePolicies);
   }

  /**
    * Set invalid switch card number.
    */
   private void setInvalidSoloPan()
   {
      datacashCard = new DataCashCard(invalidPan, nameOnCard, expiryDate, cv2,
         postCode, solocardtype, "123", address, cvvPolicies, addressPolicies, postCodePolicies);
      datacashCard.setStartDate("05/06");
   }

  /**
    * Set a valid card number.
    */
   private void setInvalidCardType()
   {
      datacashCard = new DataCashCard(validPan, nameOnCard, expiryDate, cv2,
         postCode, invalidCardType, null, address, cvvPolicies, addressPolicies, postCodePolicies);
   }

   /**
    * Ensure that no exception is thrown and validate method returns true.
    */
   public void testValidateForValidData()
   {
      setValidPan();
      try
      {
         LogWriter.logInfoMessage("Validating Data Cash card for valid data");
         datacashCard.validate();
         assertTrue("Validation is successful.", true);
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         LogWriter.logErrorMessage(pve.getMessage(), pve);
         fail("Unexpected exception: " + pve.getMessage());
      }
   }

   /**
    * Ensure that no exception is thrown and validate method returns true.
    */
   public void testgetAuthXmlDocument()
   {
      setValidPan();
      assertNotNull(datacashCard.getAuthXmlDocument(AUTH_TRANSACTION, "123"));
   }

   /**
    * Ensure that switch card passes validation.
    */
   public void testValidSwitchCard()
   {
      setValidSwitch();
      try
      {
         LogWriter.logInfoMessage("Validating for Switch card.");
         datacashCard.validate();
         assertTrue("Validation is successful.", true);
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         LogWriter.logErrorMessage(pve.getMessage(), pve);
         fail("Unexpected exception: " + pve.getMessage());
      }
   }

   /**
    * Test case to check if an exception is thrown if an invalid pan is given.
    *
    * Ensure that an exception is thrown by the validate method.
    */
   public void testValidateForInvalidData()
   {
      try
      {
         setInvalidPan();
         datacashCard.validate();
         LogWriter.logWarningMessage("Fail, should have got exception.");
         fail(FAIL_MSG);
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logInfoMessage("Got expected exception : " + pve.getMessage(), pve);
         assertTrue(SUCCESS_MSG, true);
      }
   }


   /**
    * Test case to check if an exception is thrown when invalid card type is given for
    * a particular pan.
    *
    * Ensure that an exception is thrown by the validate method.
    */
   public void testSecureInvalidCardType()
   {
      try
      {
         LogWriter.logMethodStart("testSecureInvalidCardType : ", "Testing validate method for"
            + " invalid data");
         setInvalidCardType();
         datacashCard.validate();
         fail(FAIL_MSG);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(SUCCESS_MSG, true);
      }
   }


   /**
    * Test case to check if an exception is thrown when invalid card type is given for
    * a particular pan.
    *
    * Ensure that an exception is thrown by the validate method.
    */
   public void testInvalidSoloCard()
   {
      try
      {
         setInvalidSoloPan();
         datacashCard.validate();
         fail(FAIL_MSG);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(SUCCESS_MSG, true);
      }
   }

   /**
    * Test case to check if an exception is thrown when invalid card type is given for
    * a particular pan.
    *
    * Ensure that an exception is thrown by the validate method.
    */
/*   public void testEmptyIssueNumber()
   {
      try
      {
         datacashCard = new DataCashCard(validSoloPan, nameOnCard, expiryDate, cv2,
            postCode, solocardtype, null, address, cvvPolicies, addressPolicies, postCodePolicies);
         datacashCard.setStartDate("05/06");
         datacashCard.validate();
         fail(FAIL_MSG);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(SUCCESS_MSG, true);
      }
   }*/

   /**
    * Test case to check if an exception is thrown when invalid card type is given for
    * a particular pan.
    *
    * Ensure that an exception is thrown by the validate method.
    */
/*   public void testInvalidIssueNumber()
   {
      try
      {
         datacashCard = new DataCashCard(validSoloPan, nameOnCard, expiryDate, cv2,
            postCode, solocardtype, "123456", address, cvvPolicies,
            addressPolicies, postCodePolicies);
         datacashCard.setStartDate("05/06");
         datacashCard.validate();
         fail(FAIL_MSG);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(SUCCESS_MSG, true);
      }
   }*/

   /**
    * TestCase to test gift cards.
    */
   public void testGiftCardXmlDocument()
   {
      datacashCard = new DataCashCard("6335860000000018".toCharArray(), "1124".toCharArray());
      assertNotNull(datacashCard.getGiftCardXmlDocument(REDEEM_TRANSACTION));
   }

   /**
    * TestCase to test GT case.
    */
   public void testGtCard()
   {
      datacashCard = new DataCashCard(validPan, nameOnCard, expiryDate, cv2, postCode, cardtype,
         null, CAPTUREMETHOD_SWIPED, "99080800");
      assertNotNull(datacashCard.getAuthXmlDocument(AUTH_TRANSACTION, null));
   }

   /**
    * TestCase to test GT case.
    */
   public void testGtCardCnp()
   {
      datacashCard = new DataCashCard(validPan, nameOnCard, expiryDate, cv2, postCode, cardtype,
         TRACK2DATA, CAPTUREMETHOD_CNP, "99080800");
      assertNotNull(datacashCard.getAuthXmlDocument(AUTH_TRANSACTION, null));
   }

}