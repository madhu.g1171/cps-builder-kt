/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: CardResponseTestCase.java,v $
 *
 * $Revision: 1.7 $
 *
 * $Date: 2008-05-08 05:35:09 $
 *
 * $Author: Vinodha.S $
 *
 * $Log: not supported by cvs2svn $ Revision 1.6 2008/05/07 14:13:44 thomas.pm Inserted Log History
 *
 * $Author: sindhushree.g $
 *
 */
package com.tui.uk.payment.service.datacash;

import junit.framework.TestCase;

import java.math.BigDecimal;
//import java.util.Currency;
//import com.tui.uk.client.domain.Money;
import com.tui.uk.payment.domain.CnpCard;

/**
 * The TestCase for the class CardResponse.
 */
public class CardResponseTestCase extends TestCase
{

   /** The constant for client value. */
 //  private String client = "99080800";

   /** The character array holding a valid card number. */
   private char[] validPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1', '1', '1', '1' };


   /** The name on card. */
   private String nameOncard = "xxxxxxxx";

   /** The expiry date. */
   private String expiryDate = "12/09";

   /** The character array holding cv2. */
   private char[] cv2 = {'4', '4', '4' };

   /** The post code. */
   private String postCode = "CV24EB";

   /** The card type. */
   private String cardType = "visa";

   /** The Card object. */
   @SuppressWarnings("unused")
   private CnpCard card;

   /** The constant THREE. */
   private static final int THREE = 3;


   /** Total amount. */
   @SuppressWarnings("unused")
   private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(1.98);

   /**
    * Setting card details.
    */
   private void setCardDetails()
   {

      card = new CnpCard(validPan, nameOncard, expiryDate, cv2, postCode, cardType, THREE, THREE);
   }

   /**
    * Test card response.
    */
   public void testCardResponse()
   {
    //Money amount  = new Money(TOTAL_AMOUNT, Currency.getInstance("GBP"));
      setCardDetails();
       /**  DataCashService dataCashService = new DataCashService(client);
         CardResponse cardResponse = dataCashService.authPayment(card, amount, "");
         assertTrue(cardResponse.getCardScheme().equalsIgnoreCase("visa"));
         assertTrue("successfull", true);*/


   }

}
