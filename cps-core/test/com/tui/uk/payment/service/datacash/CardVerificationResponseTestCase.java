/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/

package com.tui.uk.payment.service.datacash;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
/*import java.math.BigDecimal;
import java.util.Currency;
*/
import junit.framework.TestCase;

import org.junit.Test;

import com.datacash.client.Agent;
/*import com.datacash.errors.FailureReport;
import com.datacash.logging.Logger;
import com.datacash.util.XMLDocument;
import com.tui.uk.client.domain.Money;
*/import com.tui.uk.log.LogWriter;
/*import com.tui.uk.payment.domain.Card;*/

/**
 * Test case for DataCashServiceFactory.
 *
 * @author santosh.ks
 *
 */
public class CardVerificationResponseTestCase extends TestCase
{
   // UNCOMMENT EVERY COMMENTED LINES BEFORE RUNNING THE TEST CASE.

   /** The Card object. */
   /*private Card card;*/

   /** The character array holding a valid card number. */
   /*private char[] validPan = "4444333322221111".toCharArray();*/

   /** The name on card. */
   /*private String nameOncard = "xxxxxxxx";*/

   /** The expiry date. */
   /*private String expiryDate = "12/09";*/

   /** The character array holding cv2. */
   /*private char[] cv2 = {'4', '4', '4' };*/

   /** The post code. */
   /*private String postCode = "CV24EB";*/

   /** The card type. */
   /*private String cardType = "visa";*/

   /** Total amount. */
   /*private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(100.00);*/

   /** Money object. */
   /*private Money amount = new Money(TOTAL_AMOUNT, Currency.getInstance("GBP"));*/


   /**
    * Setting valid card details .
    */
   /*private void setCardDetails()
   {
      card = new Card(validPan, nameOncard, expiryDate, cv2, postCode, cardType);
   }*/

   /**
    * This is responsible for retrieving Agent object.
    * @param dataCashService the datacash service reference.
    * @param methods the methods[].
    *
    * @return the Agent object.
    */
   public Agent getAgent(DataCashService dataCashService, Method[] methods)
   {
      Agent agent = null;
      for (int i = 0; i < methods.length; i++)
      {
         String methodName = methods[i].getName();
         methods[i].setAccessible(true);
         if (methodName.equalsIgnoreCase("getAgent"))
         {
            try
            {
               agent = (Agent) methods[i].invoke(dataCashService);
            }
            catch (InvocationTargetException ite)
            {
               LogWriter.logWarningMessage("" + ite);
            }
            catch (IllegalAccessException e)
            {
               LogWriter.logWarningMessage("" + e);
            }
         }
      }
      return agent;
   }

   /**
    * Test case for DataCashService for different client.
    */
   @Test
   public void testCardVerificationResponse()
   {
      /*
      XMLDocument xmlDocument = null;
      setCardDetails();
      DataCashService dataCashService = new DataCashService("99080800");

      CardVerificationResponse cardVerficationResponse = null;

      if (dataCashService != null)
      {
         Class< ? > klass = dataCashService.getClass();
         Method[] methods = klass.getDeclaredMethods();
         Agent agent = getAgent(dataCashService, methods);
         if(agent != null)
         {
            for (int i = 0; i < methods.length; i++)
            {
               String methodName = methods[i].getName();
               methods[i].setAccessible(true);
               if (methodName.equalsIgnoreCase("getAuthXmlDocument"))
               {
                  try
                  {
                     String transactionType = "auth";
                     xmlDocument =
                        (XMLDocument) methods[i].invoke(dataCashService,
                           card, amount, null, transactionType);

                     XMLDocument response = agent.request(xmlDocument, new Logger());
                     cardVerficationResponse = new CardVerificationResponse(response);
                     cardVerficationResponse.getAcsUrl();
                     cardVerficationResponse.getPaReq();
                     assertNotNull(cardVerficationResponse);
                  }
                  catch (InvocationTargetException ite)
                  {
                     LogWriter.logWarningMessage("" + ite);
                  }
                  catch (IllegalAccessException e)
                  {
                     LogWriter.logWarningMessage("" + e);
                  }
                  catch (FailureReport e)
                  {
                     LogWriter.logWarningMessage("" + e);
                  }
               }
            }
         }
      }
   */
    }
}
