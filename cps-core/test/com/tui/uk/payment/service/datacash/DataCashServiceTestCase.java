/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: DataCashServiceTestCase.java,v $
 *
 * $Revision: 1.7 $
 *
 * $Date: 2008-05-08 05:33:15 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $ Revision 1.6 2008/05/06 12:02:11 sindhushree.g Changed Copyright
 * (C)2006 TUI UK Ltd to Copyright (C)2008 TUI UK Ltd.
 *
 * Revision 1.5 2008/05/06 11:54:38 sindhushree.g Modified the DataCashCard class to include Card
 * and MoreSecureCard objects.
 *
 * Revision 1.4 2008/05/05 11:15:45 sindhushree.g Modified to resolve checkstyle issues.
 *
 */

package com.tui.uk.payment.service.datacash;

/*
 * import java.math.BigDecimal; import java.util.Currency;
 */

import junit.framework.TestCase;

/*
 * import com.tui.uk.client.domain.Money; import com.tui.uk.log.LogWriter; import
 * com.tui.uk.payment.domain.Card;
 */

/**
 * This class tests the DataCashService class. TODO Methods should be uncommented and make use of
 * Datacash stub once ready. Methods are disabled in order Build to work on build machine via cruise
 * control.
 *
 * @author thomas.pm
 */
public class DataCashServiceTestCase extends TestCase
{


   /** The Constant INVALID_REFERENCE. */
  // private static final String INVALID_REFERENCE = "4000";

   /** The character array holing cv2. */
  // private char[] cv2 = {'4', '4', '4' };

   /** The character array holing cv2. */
  // private char[] validPan =
    //  {'4', '2', '4', '2', '4', '2', '5', '0', '0', '0', '0', '0', '0', '0', '0', '9' };

   /** The character array holing a invalid card number. */
 //  private char[] invalidPan =
    //  {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1', '1', '8', '8' };

   /** String holding expiry date. */
 //  private String expiryDate = "08/10";

   /** String holding authorization Code. */
 //  private String authCode = "1234";

   /** The <code>Card</code> object. */
  // private Card card;

   /** The <code>Money</code> object. */
  // private Money amount;

   /** The cardtype. */
  // private String cardtype = "visa";

   /** Invalid data cash reference. */
  // private String invalidDataCashReference;

   /** Total amount. */
 //  private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(100);

   /** Failure Message. */
  // private static final String FAIL_MSG = "Should have got an exception.";

   /** The Constant GBP. */
  // private static final String GBP = "GBP";

   /** The constant for CLIENT value. */
   //private static final String CLIENT = "99080800";

   /** The TRACK2DATA value. */
   //private static final String TRACK2DATA =
   //   "7103DF03FCB5B488C22CE7E3B493DE4F20A625007049B832"
   //      + "7DEC37E41F6727892C8E0EBBFE58CAD5B4E56E850E4A176B2"
   //      + "4A64A74743177FE61E33EADAE9E64AD6B676E3DD2F2F9FC82ADBB"
   //      + "6CE6C42E775A732860AD0B65265C0110C6657AC4ADAAA202BC64F51EF645B93C4D3F8DB4697F"
   //      + "A15A3AAEDDE3FABC3ED52DBED2C92A";


   /**
    * Setting valid card details.
    */
  // private void setSecureValidDetails()
  // {
      /*
       * amount = new Money(TOTAL_AMOUNT, Currency.getInstance(GBP)); card = new Card(validPan,
       * "test", expiryDate, cv2, "aaa", cardtype); invalidDataCashReference = INVALID_REFERENCE;
       */
  // }

   /**
    * Setting invalid card details.
    */
  // private void setSecureInValidDetails()
  // {
      /*
       * amount = new Money(TOTAL_AMOUNT, Currency.getInstance(GBP)); card = new Card(invalidPan,
       * "test", expiryDate, cv2, "aaa", cardtype); invalidDataCashReference = INVALID_REFERENCE;
       */
  // }

   /**
    * Setting in valid card details for more secure card.
    *
    * @return dataCashReference the data cash reference.
    */
   //private String getSecureDataCashReference()
   //{
   //   String dataCashReference = null;
      /*
       * setSecureValidDetails(); try { DataCashService dataCashService =
       * DataCashServiceFactory.getDataCashService(CLIENT); dataCashService.authPayment(card,
       * amount, authCode); CardResponse cardResponse = dataCashService.doPayment(card, amount,
       * authCode); dataCashReference = cardResponse.getDatacashReference(); } catch (Exception
       * dcse) { fail("Should not have got data cash service exception while getting response." +
       * dcse.getMessage()); }
       */
     // return dataCashReference;
  // }

   /**
    * Setting in valid card details for more secure card.
    *
    * @return dataCashReference the data cash reference.
    */
  // private String setDataCashDetails()
  // {

    //  String dataCashReference = null;
      /*
       * setSecureValidDetails(); try {
       *
       * DataCashService dataCashService = DataCashServiceFactory.getDataCashService(CLIENT);
       * CardResponse cardResponse = dataCashService.authPayment(card, amount, authCode); authCode =
       * cardResponse.getAuthCode(); dataCashReference = cardResponse.getDatacashReference(); }
       * catch (Exception dcse) { fail("Should not have got data cash service exception while
       * getting response." + dcse.getMessage()); }
       */
   //   return dataCashReference;
  // }

   /**
    * Ensure that the payment is authorized for secure card.
    */
   public void testauthPaymentValidSecure()
   {
      /*
       * try { setSecureValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); dataCashService.authPayment(card, amount, authCode); } catch
       * (Exception dse) { fail("Unexpected exception while authorized payment." +
       * dse.getMessage()); }
       */
   }

   /**
    * Ensure that payment is not authorized for secure card.
    */
   public void testauthPaymentInValidSecure()
   {
      /*
       * try { setSecureInValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); dataCashService.authPayment(card, amount, authCode);
       *  } catch (Exception dcse) { // Test case is successful if this exception occurs. assert
       * (true); }
       */
   }

   /**
    * Ensure that the refund is authorized for secure card.
    */
   public void testauthRefundValidSecure()
   {
      /*
       * try { setSecureValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); dataCashService.authRefund(card, amount, authCode);
       *  } catch (Exception dcse) { fail("Unexpected exception in refunding." + dcse.getMessage()); }
       */
   }

   /**
    * Ensure that refund is not authorized for secure card.
    */
   public void testauthRefundInValidSecure()
   {
      /*
       * try { setSecureInValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); CardResponse response = dataCashService.authRefund(card, amount,
       * authCode); LogWriter.logWarningMessage(FAIL_MSG +" : "+ response.getDescription()); } catch
       * (Exception dcse) { // Test case is successful if this exception occurs. fail(FAIL_MSG); }
       */
   }

   /**
    * Ensure that the refund is done for secure card.
    */
   public void testdoRefundValidSecure()
   {
      /*
       * try { setSecureValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); dataCashService.doRefund(card, amount, authCode);
       *  } catch (Exception dcse) { fail("Should have got data cash service exception in
       * refunding." + dcse.getMessage()); }
       */
   }

   /**
    * Ensure that refund is not done for secure card.
    */
   public void testdoRefundInValidSecure()
   {
      /*
       * try { setSecureInValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); Response response = dataCashService.doRefund(card, amount,
       * authCode); LogWriter.logWarningMessage(FAIL_MSG +" : "+ response.getDescription()); } catch
       * (Exception dcse) { fail(FAIL_MSG); // Test case is successful if this exception occurs. }
       */
   }

   /**
    * Ensure that the payment is done for secure card.
    */
   public void testdoPaymentValidSecure()
   {
      /*
       * try { setSecureValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); dataCashService.doPayment(card, amount, authCode); } catch
       * (Exception dcse) { fail("Unexpected exception in payment." + dcse.getMessage()); }
       */
   }

   /**
    * Ensure that payment is not done for secure card.
    */
   public void testdoPaymentInValidSecure()
   {
      /*
       * try { setSecureInValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); Response response = dataCashService.doPayment(card, amount,
       * authCode); LogWriter.logWarningMessage(FAIL_MSG +" : "+ response.getDescription()); } catch
       * (Exception dcse) { // Test case is successful if this exception occurs. fail(FAIL_MSG); }
       */
   }

   /**
    * Ensure whether payment fulfillment is achieved for secure card.
    */
   public void testFulfillValidSecure()
   {
      /*
       * try { setSecureValidDetails(); String dataCashReference = setDataCashDetails();
       * DataCashService dataCashService = new DataCashService(CLIENT);
       * dataCashService.fulfill(dataCashReference, authCode, amount); } catch (Exception dcse) {
       * fail("Unexpected data cash service exception during fulfillment." + dcse.getMessage()); }
       */

   }

   /**
    * Ensure that payment fulfillment not done for secure card.
    */
   public void testFulfillInValidSecure()
   {
      /*
       * try { setSecureInValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); Response response =
       * dataCashService.fulfill(invalidDataCashReference, authCode, amount);
       * LogWriter.logWarningMessage(FAIL_MSG +" : "+ response.getDescription()); } catch (Exception
       * dcse) { // Test case is successful if this exception occurs. fail(FAIL_MSG); }
       */
   }

   /**
    * Ensure whether payment Cancellation is achieved for secure card.
    */
   public void testCancelValidSecure()
   {
      /*
       * try { setSecureValidDetails(); String dataCashReference = getSecureDataCashReference();
       * DataCashService dataCashService = new DataCashService(CLIENT); Response response =
       * dataCashService.cancel(dataCashReference); assertTrue(response.getCode() == 1); } catch
       * (Exception dcse) { fail("Should have got data cash service exception while fulfillment"); }
       */
   }

   /**
    * Ensure that payment cancellation not done for secure card.
    */
   public void testCancelInValidSecure()
   {
      /*
       * try { setSecureInValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); Response response =
       * dataCashService.cancel(invalidDataCashReference); LogWriter.logWarningMessage(FAIL_MSG +" : "+
       * response.getDescription()); } catch (Exception dcse) { // Test case is successful if this
       * exception occurs. fail(FAIL_MSG); }
       */
   }

   /**
    * Ensure whether payment reversal is achieved for secure card.
    */
   public void testRefundValidSecure()
   {
      /*
       * try { setSecureValidDetails(); String dataCashReference = getSecureDataCashReference();
       * DataCashService dataCashService = new DataCashService(CLIENT);
       * dataCashService.refund(dataCashReference, amount); } catch (Exception dcse) {
       * fail("Unexpected exception while reversing." + dcse.getMessage()); }
       */
   }

   /**
    * Ensure that payment reversal not done for secure card.
    */
   public void testRefundInValidSecure()
   {
      /*
       * try { setSecureInValidDetails(); DataCashService dataCashService = new
       * DataCashService(CLIENT); Response response =
       * dataCashService.refund(invalidDataCashReference, amount);
       * LogWriter.logWarningMessage(FAIL_MSG +" : "+ response.getDescription()); } catch (Exception
       * dcse) { // Test case is successful if this exception occurs. fail(FAIL_MSG); }
       */
   }

   /**
    * Ensures for card holder verification.
    */
   public void testCardEnrolmentVerfication()
   {
      /*
       * setSecureInValidDetails(); DataCashService dataCashService = new DataCashService(CLIENT);
       * dataCashService.cardEnrolmentVerfication("merchantUrl", CLIENT, card, amount);
       */
   }

   /**
    * Ensures the payer authentication response.
    */
   public void testAuthorize3DPayment()
   {
      /*
       * setSecureInValidDetails(); DataCashService dataCashService = new DataCashService(CLIENT);
       * dataCashService.authorize3DPayment("paRes");
       */
   }

   /**
    * The method used to test the DataCash Service Constructor.
    */
   public void testDataCashServiceConstructor()
   {
      /*
       * try { new DataCashService("1234"); } catch(Exception e) {
       * LogWriter.logWarningMessage(FAIL_MSG + " : " + e); }
       */
   }

   /**
    * This method is used to test the overloadeConstructor.
    */
   public void testDataCashServiceOverloadedConstructor()
   {
      /*
       * try { new DataCashService("1234", CLIENT); } catch(Exception e) {
       * LogWriter.logWarningMessage(FAIL_MSG + " : " + e); }
       */
   }

   /**
    * Method to populate the tracks data.
    */
   public void testPopulateTrack2Data()
   {
      /*
       * setSecureValidDetails(); DataCashService dataCashService = new DataCashService(CLIENT);
       * card.setCaptureMethod("cnp"); card.setTrack2Data(TRACK2DATA); CardResponse cardResponse =
       * dataCashService.authRefund(card, amount, authCode);
       * LogWriter.logWarningMessage(cardResponse.getDescription());
       */
   }

   /**
    * Method is used to test the authRefund Method.
    */
   public void testAuthXmlDocument()
   {
      /*
       * setSecureValidDetails(); DataCashService dataCashService = new DataCashService(CLIENT,
       * "99080800"); card.setCaptureMethod("cp"); card.setTrack2Data(null); CardResponse
       * cardResponse = dataCashService.authRefund(card, amount, authCode);
       * LogWriter.logWarningMessage(cardResponse.getDescription());
       */
   }
}
