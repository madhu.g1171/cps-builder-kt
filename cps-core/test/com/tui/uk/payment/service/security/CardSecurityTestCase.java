/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CardSecurityTestCase.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2008-05-14 09:14:04 $
 *
 * $Author: thomas.pm $
 *
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2008/05/07 14:20:29  thomas.pm
 * Added Log History
 *
 *
 */
package com.tui.uk.payment.service.security;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import junit.framework.TestCase;

import com.tui.uk.log.LogWriter;

/**
 * Test case for CardSecurity class.
 *
 * @author thomas.pm
 *
 */
public class CardSecurityTestCase extends TestCase
{
   /** The character array holing credit card number. */
    private char[] cardNumber = {'4', '4', '4', '4', '3', '3',
                            '3', '3', '2', '2', '2', '2', '1', '1', '1', '1'};

    /** The encrypted card number. */
    private String encryptedCardNumber;

    /** Constant for public key. */
    private static final int PUBLIC = 1;

    /** CardSecurity object. */
    private static CardSecurity cardSecurity;

    /**
     * Sets request path for the class under test.
     *
     * @throws Exception as the base class does.
     */
    @Override
    protected void setUp() throws Exception
    {
       super.setUp();
    }
   /**
    * Ensure that the encoded message is correct.
    */
    public void testDecrypt()
    {
       encryptedCardNumber = CardSecurity.encrypt(cardNumber);
       char[] decryptedCardNumber = CardSecurity.decrypt(encryptedCardNumber);
       assertTrue(Arrays.equals(cardNumber, decryptedCardNumber));
    }
    /**
     * Ensure that the encrypted card number is not equal to the entered card number.
     */
    public void testEncryptedCardNumber()
    {
       encryptedCardNumber = CardSecurity.encrypt(cardNumber);
       assertFalse((String.valueOf(cardNumber).equals(encryptedCardNumber)));
    }

    /**
     * Tests the private cunstructor.
     */
    public void testPrivateConstructor()
    {
       try
       {
          Constructor< ? >[] cons = CardSecurity.class.getDeclaredConstructors();
          cons[0].setAccessible(true);
          cardSecurity = (CardSecurity) cons[0].newInstance();
       }
       catch (InstantiationException e)
       {
          LogWriter.logWarningMessage("Exception : " + e);
       }
       catch (IllegalAccessException e)
       {
          LogWriter.logWarningMessage("Exception : " + e);
       }
       catch (InvocationTargetException ite)
       {
          LogWriter.logWarningMessage("Exception : " + ite);
       }
    }

    /**
     * Tests the getKey private method.
     */
    public void testGetKey()
    {
       if (cardSecurity != null)
       {
          Class< ? > klass = cardSecurity.getClass();
          Method[] methods = klass.getDeclaredMethods();

          for (int i = 0; i < methods.length; i++)
          {
             String methodName = methods[i].getName();
             methods[i].setAccessible(true);
             if (methodName.equalsIgnoreCase("getKey"))
             {
                try
                {
                   String fileName = "./bin/public1.key";
                   methods[i].invoke(cardSecurity, fileName, PUBLIC);
                }
                catch (InvocationTargetException ite)
                {
                   LogWriter.logWarningMessage("" + ite);
                }
                catch (IllegalAccessException e)
                {
                   LogWriter.logWarningMessage("" + e);
                }
             }
          }
       }
    }
 }
