/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: TomcatInstanceFilterTestCase .java$
 *
 * $Revision: $
 *
 * $Date: 2008-10-08 $
 *
 * $Author: vijayalakshmi.d $
 *
 * $Log: $
 */
package com.tui.uk.payment.filter;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import junit.framework.TestCase;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.utility.TestDataSetup;

/**
 * TestCase for TomcatInstanceFilter Class.
 */

public class TomcatInstanceFilterTestCase extends TestCase
{

   /** Variable for InvocationContext. */
   private InvocationContext ic = null;

   /** Variable for HttpServletRequest. */
   @SuppressWarnings("unused")
   private HttpServletRequest req = null;

   /** Variable for HttpServletResponse. */
   @SuppressWarnings("unused")
   private HttpServletResponse res = null;

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();
      ServletRunner sr = new ServletRunner();
      String filterUrl = "http://test.meterware.com/testFilter";
      sr.registerServlet("testFilter", TomcatInstanceFilter.class.getName());
      WebRequest request = new PostMethodWebRequest(filterUrl);
      request.setParameter("balanceType", "PC");
      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);
      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute(DispatcherConstants.BOOKING_INFO, TestDataSetup.getBookingInfo());
      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * To Test TomcatInstanceFilter Servlet.
    */
   public void testServlet()
   {
      //TomcatInstanceFilter tomcatInstanceFilter = new TomcatInstanceFilter();
      /*
       * try { // tomcatInstanceFilter.doFilter(//request, response, chain)service(); } catch
       * (ServletException e) { // TODO Auto-generated catch block e.printStackTrace(); } catch
       * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
       */
   }

}
