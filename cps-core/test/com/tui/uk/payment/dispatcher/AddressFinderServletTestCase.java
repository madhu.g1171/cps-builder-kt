package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import junit.framework.TestCase;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.tui.uk.payment.service.capscan.addressfinder.AddressConstants;
import com.tui.uk.utility.TestDataSetup;

/**
 * TestCase for the AdressFinderServlet class.
 *
 * @author vijayalakshmi.d
 *
 */
public class AddressFinderServletTestCase extends TestCase
{

   /** Setting the value for InvocationContext. */
   private InvocationContext ic = null;

   /** Setting the value for HttpServletRequest. */
   private HttpServletRequest req = null;

   /** Setting the value HttpServletResponse. */
   private HttpServletResponse res = null;

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();
      WebRequest request = TestDataSetup.getWebRequest();
      request.setParameter("balanceType", "PC");
      request.setParameter(AddressConstants.POST_CODE, "cv14aq");
      request.setParameter(AddressConstants.HOUSE_NAME, "111");
      ic = TestDataSetup.setInvocationTarget(request);
      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute("bookingInfo",  TestDataSetup.getBookingInfo());
      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * Sets request path for the class under test (For the failure case).
    *
    * @throws Exception as the base class does.
    */

   protected void setUpInvalid() throws Exception
   {
      super.setUp();
      WebRequest request = TestDataSetup.getWebRequest();
      request.setParameter("balanceType", "PC");
      request.setParameter(AddressConstants.POST_CODE, "cv14aq");
      // request.setParameter(AddressConstants.HOUSE_NUMBER, "1");
      ic = TestDataSetup.setInvocationTarget(request);
      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute("bookingInfo", TestDataSetup.getBookingInfo());
      req = ic.getRequest();
      res = ic.getResponse();

   }

   /**
    * To test the AddressFinderServlet.
    *
    */
   public void testServlet()
   {

      AddressFinderServlet addressFinderServlet = new AddressFinderServlet();
      try
      {
         addressFinderServlet.doGet(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * To test the AddressFinderServlet for the invalid case.
    *
    * @throws Exception the Exception object.
    *
    */
   public void testServletInvalid() throws Exception
   {
      setUpInvalid();
      AddressFinderServlet addressFinderServlet = new AddressFinderServlet();
      try
      {
         addressFinderServlet.doGet(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }
}