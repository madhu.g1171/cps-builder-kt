/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentServletTestCase  .java$
 *
 * $Revision: $
 *
 * $Date: 2008-10-08 $
 *
 * $Author: vijayalakshmi.d $
 *
 * $Log: $
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;

import com.meterware.httpunit.HttpNotFoundException;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.CardType;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.DiscountComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.utility.TestDataSetup;

/**
 *
 *TestCase for PaymentServlet Class.
 *
 */
public class PaymentServletTestCase extends TestCase
{
   /** Variable for InvocationContext. */
   private InvocationContext ic = null;

   /** Variable for HttpServletRequest. */
   private HttpServletRequest req = null;

   /** Variable for HttpServletResponse. */
   private HttpServletResponse res = null;

   /** variable to hold the UUID.*/
   private static UUID uuid;

   /** The GBP currency. */
   private static final Currency GBP = Currency.getInstance("GBP");

   /** Variable to store passnger identifier. */
   private static final int PASSENGERID = 3;

   /** The constant THREE. */
   private static final int THREE = 3;

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();
      WebRequest request = TestDataSetup.getWebRequest();
      request.setParameter("balanceType", "PC");
      PaymentData paymentData = new PaymentData(getBookingInfo());
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
      ic = TestDataSetup.setInvocationTarget(request);
      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute("bookingInfo", TestDataSetup.getBookingInfo());
      req = ic.getRequest();
      res = ic.getResponse();

   }

   /** To test the PaymentServlet .*/
   public void testServlet()
   {

      PaymentServlet paymentServlet = new PaymentServlet();
      try
      {
         paymentServlet.doGet(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (HttpNotFoundException e)
      {
         e.printStackTrace();
      }
   }

   /** tests the new holiday.*/
   public void testNewHoliday()
   {
      setupPaymentData();
      PaymentServlet paymentServlet = new PaymentServlet();
      try
      {
         paymentServlet.doGet(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (HttpNotFoundException e)
      {
         e.printStackTrace();
      }
   }

   /** To sets the payment data object.*/
   private void setupPaymentData()
   {
      WebRequest request = TestDataSetup.getWebRequest();
      request.setParameter("balanceType", "PC");
      BookingInfo bookingInfo = getBookingInfo();
      bookingInfo.setNewHoliday(false);
      PaymentData paymentData = new PaymentData(bookingInfo);
      paymentData.setFailureCount();
      paymentData.setFailureCount();
      paymentData.setFailureCount();
      paymentData.setFailureCount();
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());

      try
      {
         ic = TestDataSetup.setInvocationTarget(request);
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }

      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute("bookingInfo", bookingInfo);
      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * Create the BookingInfo.
    *
    * @return BookingInfo object.
    */
   public static BookingInfo getBookingInfo()
   {
      ClientApplication clientApplicationName =
         ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount =
         new Money(new BigDecimal("10"), GBP);
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new HashMap<Integer, List<PassengerSummary>>();
      List<PassengerSummary> passengerSummaryList =
         new ArrayList<PassengerSummary>();
      passengerSummaryList.add(new PassengerSummary(
         AgeClassification.ADULT, true, PASSENGERID));
      passengerRoomSummary.put(1, passengerSummaryList);

      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL,
            totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      bookingComponent.setAccommodationSummary(TestDataSetup
         .getAccommodationSummary("TRACS"));
      Map<String, BigDecimal> cardChargesMap =
         new HashMap<String, BigDecimal>();
      BigDecimal percentage = new BigDecimal("2.5");
      cardChargesMap.put("VISA", percentage);
      cardChargesMap.put("AMERICAN_EXPRESS", percentage);

      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      bookingComponent.setCardChargesMap(cardChargesMap);
      DiscountComponent discountComponent =
         new DiscountComponent(DispatcherConstants.PRICE_BEAT, new Money(
            new BigDecimal("12.60"), GBP), true,
            false);
      java.util.List<DiscountComponent> discountComponents =
         new java.util.ArrayList<DiscountComponent>();
      discountComponents.add(discountComponent);
      bookingComponent.setDiscountComponents(discountComponents);

      bookingComponent.setPaymentFailureURL("abc");
      bookingComponent.setPassengerRoomSummary(passengerRoomSummary);
      Map<String, List<PaymentType>> paymentType = new HashMap<String, List<PaymentType>>();
      PaymentType type =
         new PaymentType("VISA", "Visa", "DCard");
      List<PaymentType> payList = new ArrayList<PaymentType>();
      CardType cardType = new CardType(THREE, THREE);
      cardType.setIsIssueNumberRequired(Boolean.FALSE);
      type.setCardType(cardType);
      payList.add(type);
      paymentType.put("CNP", payList);
      bookingComponent.setPaymentType(paymentType);
      BookingInfo bookingInfo = new BookingInfo(bookingComponent);
      bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal("500"), GBP));
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal("512.50"), GBP));
      bookingInfo.setCalculatedCardCharge(new Money(new BigDecimal("12.50"), GBP));
      bookingInfo.setThreeDAuth(false);
      return bookingInfo;
   }

}
