package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;

import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.DiscountComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;

/**
 * The TestCase for ConfirmBookingServlet class.
 *
 * @author sathish.e
 *
 */
public class ConfirmBookingServletTestCase extends TestCase
{
   /** Constant for GBP. */
   private static final String GBP = "GBP";

   /** The GBP currency. */
   //private static final Currency GBP_CURRENCY = Currency.getInstance(GBP);

   /** Variable for InvocationContext. */
   private InvocationContext ic = null;

   /** Variable for HttpServletRequest. */
   private HttpServletRequest req = null;

   /** Variable for HttpServletResponse. */
   private HttpServletResponse res = null;

   /** variable to hold the UUID.*/
   private static UUID uuid;

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   protected void setUp() throws Exception
   {
      super.setUp();
      ServletRunner sr = new ServletRunner();
      String servletUrl = "http://test.meterware.com/testServlet";
      WebRequest request = new PostMethodWebRequest(servletUrl);
      PaymentData paymentData = new PaymentData(getBookingInfo());
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);
      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute("bookingInfo", getBookingInfo());
      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * Sets request path.
    *
    * @throws Exception as the base class does.
    */
   private void setUpInvalid() throws Exception
   {
      super.setUp();
      ServletRunner sr = new ServletRunner();
      String servletUrl = "http://test.meterware.com/testServlet";
      WebRequest request = new PostMethodWebRequest(servletUrl);
      PaymentData paymentData = new PaymentData(getBookingInfo());
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);
      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute("bookingInfo", getBookingInfo());
      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * To Test ConfirmBookingServlet.
    */
   public void testServlet()
   {
      ConfirmBookingServlet confirmBookingServlet = new ConfirmBookingServlet();
      try
      {
         confirmBookingServlet.doPost(req, res);
      }
      catch (ServletException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }

   /**
    * To Test ConfirmBookingServlet.
    * @throws Exception the exception.
    */
   public void testInvalidServlet() throws Exception
   {
      setUpInvalid();
      ConfirmBookingServlet confirmBookingServlet = new ConfirmBookingServlet();
      try
      {
         confirmBookingServlet.doPost(req, res);
      }
      catch (ServletException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }
   /**
    * Creates the BookingComponent.
    *
    * @return bookingComponent object
    */
   private static BookingInfo getBookingInfo()
   {
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance(GBP));
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
           DiscountComponent discountComponent =
         new DiscountComponent(DispatcherConstants.PRICE_BEAT, new Money(new BigDecimal("12.60"),
            Currency.getInstance(GBP)), true, false);
      java.util.List<DiscountComponent> discountComponents =
         new java.util.ArrayList<DiscountComponent>();
      discountComponents.add(discountComponent);
      bookingComponent.setDiscountComponents(discountComponents);
      BookingInfo bookingInfo = new BookingInfo(bookingComponent);
      bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal("100"), Currency
      .getInstance(GBP)));
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal("125.50"), Currency
      .getInstance(GBP)));
      bookingInfo.setCalculatedCardCharge(new Money(new BigDecimal("2.50"), Currency
         .getInstance(GBP)));
      return bookingInfo;
   }

}
