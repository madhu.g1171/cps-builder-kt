/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentServletTestCase  .java$
 *
 * $Revision: $
 *
 * $Date: 2008-10-08 $
 *
 * $Author: vijayalakshmi.d $
 *
 * $Log: $
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import junit.framework.TestCase;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.utility.TestDataSetup;

/**
 *
 *TestCase for RefundOptionServlet Class.
 *
 */
public class RefundOptionServletTestCase extends TestCase
{
   /** Variable for InvocationContext. */
   private InvocationContext ic = null;

   /** Variable for HttpServletRequest. */
   private HttpServletRequest req = null;

   /** Variable for HttpServletResponse. */
   private HttpServletResponse res = null;

   /** variable to hold the UUID.*/
   private static UUID uuid;


   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();
      WebRequest request = TestDataSetup.getWebRequest();
      request.setParameter("balanceType", "PC");
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo());
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
      ic = TestDataSetup.setInvocationTarget(request);
      HttpSession session = ic.getRequest().getSession(true);
      session.setAttribute("bookingInfo", TestDataSetup.getBookingInfo());
      req = ic.getRequest();
      res = ic.getResponse();

   }

   /** To test the PaymentServlet .*/
   public void testServlet()
   {

      RefundOptionServlet refundOptionServlet = new RefundOptionServlet();
      try
      {
         refundOptionServlet.doGet(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

}
