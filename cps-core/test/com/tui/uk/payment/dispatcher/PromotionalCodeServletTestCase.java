/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PromotionalCodeServletTestCase.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-09-16 08:25:26 $
 *
 * $Author: Venkateswarlu.p@sonata-software.com $
 *
 *
 * $Log: $.
 *
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import com.tui.uk.client.domain.PackageType;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.promotionaldiscount.constants.PromotionalDiscountConstants;
import com.tui.uk.utility.TestDataSetup;

/**
 * This is test case for PromotionalCodeServlet class.
 *
 * @author venkateswarlu.p
 *
 */
public class PromotionalCodeServletTestCase extends TestCase
{
   /** Variable for InvocationContext. */
   private InvocationContext ic = null;

   /** Variable for HttpServletRequest. */
   private HttpServletRequest req = null;

   /** Variable for HttpServletResponse. */
   private HttpServletResponse res = null;

   /** the server URL. */
   private static String serverURL = "http://test.meterware.com/testServlet";

   /** the constant to register the test servlet. */
   private static String testServlet = "testServlet";

   /** the balance type constant. */
   private static final String BALANCE_TYPE = "balanceType";

   /** the PC constant. */
   private static final String PC = "PC";

   /** the FullBalance constant. */
   private static final String FULL_BALANCE = "FullBalance";

   /** the zero constant. */
   private static final String ZERO = "00";

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();
      ServletRunner sr = new ServletRunner();
      String servletUrl = serverURL;
      sr.registerServlet(testServlet, PromotionalCodeServlet.class.getName());
      WebRequest request = new PostMethodWebRequest(servletUrl);
      request.setParameter(BALANCE_TYPE, PC);
      request.setParameter(DispatcherConstants.BALANCE_TYPE, FULL_BALANCE);
      request.setParameter(PromotionalDiscountConstants.PROMOTIONALCODE, ZERO);
      request.setHeaderField("Accept-Language", "en-GB");

      PackageType packageType = PackageType.COWSComponentPackage;
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(packageType));
      UUID uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
      request.setHeaderField("Accept-Language", "en-GB");

      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);

      req = ic.getRequest();
      res = ic.getResponse();
   }


   /**
    * To test the PromotionalCodeServlet for a PDP.
    * @throws Exception the Exception object.
    */
   public void testServletAO() throws Exception
   {
      setUpAO();
      PromotionalCodeServlet promotionalCodeServlet = new PromotionalCodeServlet();
      promotionalCodeServlet.doGet(req, res);
   }


   /**
    * Sets request path for the class under test for AO case.
    *
    * @throws Exception as the base class does.
    */

   protected void setUpAO() throws Exception
   {
      super.setUp();
      ServletRunner sr = new ServletRunner();
      String servletUrl = serverURL;
      sr.registerServlet(testServlet, PromotionalCodeServlet.class.getName());
      WebRequest request = new PostMethodWebRequest(servletUrl);
      request.setParameter(BALANCE_TYPE, PC);
      request.setParameter(DispatcherConstants.BALANCE_TYPE, FULL_BALANCE);
      request.setParameter(PromotionalDiscountConstants.PROMOTIONALCODE, "0000");
      request.setHeaderField("Accept-Language", "en-GB");

      PackageType aoPackageType = PackageType.AccommodationOnly;
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(aoPackageType));
      UUID uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());

      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);

      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * To test the PromotionalCodeServlet for a PDP.
    * @throws Exception the Exception object.
    */
   public void testServletFH() throws Exception
   {
      setUpFH();
        PromotionalCodeServlet promotionalCodeServlet = new PromotionalCodeServlet();
        promotionalCodeServlet.doGet(req, res);
   }


   /**
    * Sets request path for the class under test for AO case.
    *
    * @throws Exception as the base class does.
    */

   protected void setUpFH() throws Exception
   {
      super.setUp();
      ServletRunner sr = new ServletRunner();
      String servletUrl = serverURL;
      sr.registerServlet(testServlet, PromotionalCodeServlet.class.getName());
      WebRequest request = new PostMethodWebRequest(servletUrl);
      request.setParameter(BALANCE_TYPE, PC);
      request.setParameter(DispatcherConstants.BALANCE_TYPE, FULL_BALANCE);
      request.setParameter(PromotionalDiscountConstants.PROMOTIONALCODE, ZERO);
      request.setHeaderField("Accept-Language", "en-GB");

      PackageType fhPackageType = PackageType.FlightAndHotel;
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(fhPackageType));
      UUID uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());

      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);

      req = ic.getRequest();
      res = ic.getResponse();
   }

   protected void setUpInvalidPromoCode() throws Exception
   {
      super.setUp();
      ServletRunner sr = new ServletRunner();
      String servletUrl = serverURL;
      sr.registerServlet(testServlet, PromotionalCodeServlet.class.getName());
      WebRequest request = new PostMethodWebRequest(servletUrl);
      request.setParameter(BALANCE_TYPE, PC);
      request.setParameter(DispatcherConstants.BALANCE_TYPE, FULL_BALANCE);
      request.setParameter(PromotionalDiscountConstants.PROMOTIONALCODE, "");
      request.setHeaderField("Accept-Language", "en-GB");

      PackageType fhPackageType = PackageType.FlightAndHotel;
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(fhPackageType));
      UUID uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());

      ServletUnitClient sc = sr.newClient();
      ic = sc.newInvocation(request);

      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * To test the PromotionalCodeServlet for a PDP.
    * @throws Exception the Exception object.
    */
   public void testServletInvalidPromoCode() throws Exception
   {
        setUpInvalidPromoCode();
        PromotionalCodeServlet promotionalCodeServlet = new PromotionalCodeServlet();
        promotionalCodeServlet.doGet(req, res);
   }
}
