package com.tui.uk.payment.dispatcher;

import junit.framework.TestCase;

import com.tui.uk.client.domain.ClientApplication;

/**
 * The TestCase for CommonPaymentServlet class.
 * @author sathish.e
 *
 */

public class CommonPaymentServletTestCase extends TestCase
{
   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   protected void setUp() throws Exception
   {
      super.setUp();
   }

   /**
    * To Test CommonPaymentServlet for Byo client.
    *
    */
   public void testServlet()
   {
      @SuppressWarnings("unused")
      ClientApplication clientApplicationName = null;
      CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
      commonPaymentServlet.getPaymentUrl(ClientApplication.WISHBuildYorOwn);


      commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.WISHBuildYorOwn);

   }

   /**
    *To Test the CommonpaymentServlet AO Client.
    */
   public void testServlet1()
   {
      @SuppressWarnings("unused")
      ClientApplication clientApplicationName = null;
      CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
      commonPaymentServlet.getPaymentUrl(ClientApplication.WISHAccommodationOnly);
      commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.WISHAccommodationOnly);

   }

   /**
    *To Test the CommonpaymentServlet WishByo Client.
    */
   public void testServlet2()
   {
      @SuppressWarnings("unused")
      ClientApplication clientApplicationName = null;
      CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
      commonPaymentServlet.getPaymentUrl(ClientApplication.WISHBuildYorOwn);
      commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.WISHCRUISE);

   }

   /**
    *To Test the CommonpaymentServlet WishAo Client.
    */
   public void testServlet3()
   {
      @SuppressWarnings("unused")
      ClientApplication clientApplicationName = null;
      CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
        commonPaymentServlet.getPaymentUrl(ClientApplication.WISHAccommodationOnly);
      commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.WISHCRUISE);

   }

   /**
    *To Test the CommonpaymentServlet Brac Client.
    */
   public void testServletException()
   {
      try
      {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.BRAC);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.BRAC);
      }
      catch (IllegalArgumentException e)
      {
         assertTrue("Code in Exception block", true);
      }
   }

   /**
    *To Test the CommonpaymentServlet CRUISE Client.
    */
   public void testServlet5()
   {
      @SuppressWarnings("unused")
      ClientApplication clientApplicationName = null;
      CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
      commonPaymentServlet.getPaymentUrl(ClientApplication.WISHCRUISE);
      commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.WISHBuildYorOwn);

   }

   /**
    *To Test the CommonpaymentServlet Falcon Client.
    */
   public void testServletFalcon()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.FALCONBuildYourOwn);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.FALCONBuildYourOwn);

   }

   /**
    *To Test the CommonpaymentServlet  Client for FCAo.
    */
   public void testServletFCAO()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.FCAO);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.FCAO);

   }


   /**
    *To Test the CommonpaymentServlet FCFO Client.
    */
   public void testServletFCFO()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.FCFO);


   }


   /**
    *To Test the CommonpaymentServlet GenericDOTNET  Client.
    */
   public void testServletGenericDotNet()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.GENERICDOTNET);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.GENERICDOTNET);

   }


   /**
    *To Test the CommonpaymentServlet Generic java Client.
    */
   public void testServletGenericJAVA()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.GENERICJAVA);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.GENERICJAVA);

   }


   /**
    *To Test the CommonpaymentServlet Greenfield Client.
    */
   public void testServletGreenfield()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.GREENFIELDBeach);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.GREENFIELDBeach);

   }


   /**
    *To Test the CommonpaymentServlet Greenfield cruise Client.
    */
   public void testServletGreenfieldCruise()
   {
        @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.GREENFIELDCruise);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.GREENFIELDCruise);

   }


   /**
    *To Test the CommonpaymentServlet Greenfield simply Client.
    */
   public void testServletGreenfieldSimply()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.GREENFIELDSimply);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.GREENFIELDSimply);

   }


   /**
    *To Test the CommonpaymentServlet HUGOBuildYourOwn Client.
    */
   public void testServletHUGOBuildYourOwn()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.HUGOBuildYourOwn);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.HUGOBuildYourOwn);

   }


   /**
    *To Test the CommonpaymentServlet KRONOS Client.
    */
   public void testServletKRONOS()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.KRONOS);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.KRONOS);

   }


   /**
    *To Test the CommonpaymentServlet PORTLAND Client.
    */
   public void testServletPORTLAND()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.PORTLAND);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.PORTLAND);

   }


   /**
    *To Test the CommonpaymentServlet TFLY Client.
    */
   public void testServletTFLY()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.TFLY);


   }


   /**
    *To Test the CommonpaymentServlet ThomsonAccommodationOnly Client.
    */
   public void testServletThomsonAccommodationOnly()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.ThomsonAccommodationOnly);
         commonPaymentServlet
         .getPostPaymentRedirectionUrl(ClientApplication.ThomsonAccommodationOnly);

   }

   /**
    *To Test the CommonpaymentServlet ThomsonBuildYourOwn Client.
    */

   public void testServletThomsonBuildYourOwn()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.ThomsonBuildYourOwn);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.ThomsonBuildYourOwn);

   }


   /**
    *To Test the CommonpaymentServlet WISHOLBP Client.
    */

   public void testServletWISHOLBP()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.WISHOLBP);
         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.WISHOLBP);

   }

   /**
    *To Test the CommonpaymentServlet ThomsonCruise Client.
    */

   public void testServletThomsonCruise()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.ThomsonCruise);
   }

   /**
    *To Test the CommonpaymentServlet WSS Client.
    */

   public void testServletWSS()
   {
         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();
         commonPaymentServlet.getPaymentUrl(ClientApplication.WSS);
   }

   /**
    *To Test the CommonpaymentServlet WISHOLBP Client.
    */

   public void testServletIllegal()
   {
      try
      {

         @SuppressWarnings("unused")
         ClientApplication clientApplicationName = null;
         CommonPaymentServlet commonPaymentServlet = new CommonPaymentServlet();

         commonPaymentServlet.getPaymentUrl(ClientApplication.TFLY);

         commonPaymentServlet.getPostPaymentRedirectionUrl(ClientApplication.TFLY);

      }
      catch (IllegalArgumentException e)
      {
         assertTrue("Code in Exception block", true);
      }

   }

}
