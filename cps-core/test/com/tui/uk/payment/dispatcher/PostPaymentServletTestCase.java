/**
 *
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.CardType;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.DiscountComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.utility.TestDataSetup;

/**
 *
 * TestCase for PostPaymentServlet Class.
 *
 */
public class PostPaymentServletTestCase extends TestCase
{

   /** Variable to store passnger identifier. */
   private static final int PASSENGERID = 3;

   /** Variable for InvocationContext. */
   private InvocationContext ic = null;

   /** Variable for HttpServletRequest. */
   private HttpServletRequest req = null;

   /** Variable for HttpServletResponse. */
   private HttpServletResponse res = null;

   /** variable to hold the UUID. */
   private static UUID uuid;

   /** variable to hold the ServletRunner. */
   private ServletRunner sr = null;

   /** variable to hold the request. */
   private WebRequest request = null;

   /** variable to hold Payment Data. */
   private PaymentData paymentData = null;

   /** variable to hold ServletUnitClient. */
   private ServletUnitClient sc = null;

   /** variable to hold transaction amount. */
   private static final String PAYMENT_0_TRANSAMT = "payment_0_transamt";

   /** transaction amount constant. */
   private static final String TRANSAMT = "125";

   /** Variable for GBP currency code. */
   private static final String GBP = "GBP";

   /** The constant THREE. */
   private static final int THREE = 3;

   /**
    * This method instantiates the required objects.
    */
   public void instantiator()
   {
      sr = new ServletRunner();
      String servletUrl = "http://test.meterware.com/testServlet";
      request = new PostMethodWebRequest(servletUrl);
      paymentData = new PaymentData(getBookingInfo());
      sc = sr.newClient();
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
   }

   /**
    * This method instantiates the required objects.
    */
   public void instantiator1()
   {
      sr = new ServletRunner();
      String servletUrl = "http://test.meterware.com/testServlet";
      request = new PostMethodWebRequest(servletUrl);
      paymentData = new PaymentData(getBookingInfo1());
      sc = sr.newClient();
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
   }

   /**
    * This generates the dummy request and response objects.
    *
    * @throws IOException the IOException object
    */
   public void requestgenerator() throws IOException
   {
      ic = sc.newInvocation(request);
      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * This method sets the request parameters specific to the valid test
    * case.
    */
   public void requestParameterSetterValid()
   {
      request.setParameter(PAYMENT_0_TRANSAMT, TRANSAMT);

      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
   }

   /**
    * This method sets the request parameters specific to the invalid1 test
    * case.
    */
   public void requestParameterSetterInValid1()
   {
      request.setParameter(PaymentConstants.TOTAL_TRANS_AMOUNT, "350");
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
   }

   /**
    * This method sets the request parameters specific to the invalid2 test
    * case.
    */
   public void requestParameterSetterInValid2()
   {
      request.setParameter(PaymentConstants.TOTAL_TRANS_AMOUNT, "499.69");
      BookingInfo bookingInfo = TestDataSetup.getBookingInfo();
      bookingInfo.setThreeDAuth(true);
      PaymentData paymentData2 = new PaymentData(bookingInfo);
      uuid = PaymentStore.getInstance().newPaymentData(paymentData2);
      paymentData2.setFailureCount();
      paymentData2.setFailureCount();
      paymentData2.getBookingInfo().getBookingComponent()
         .setPaymentFailureURL("abc");
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
   }

   /**
    * This method sets the request parameters specific to the invalid3 test
    * case.
    */
   public void requestParameterSetterInValid3()
   {
      PaymentData paymentData3 = new PaymentData(TestDataSetup.getBookingInfo());
      paymentData3.getBookingInfo().getBookingComponent()
         .setPaymentGatewayVirtualTerminalId(null);
      uuid = PaymentStore.getInstance().newPaymentData(paymentData3);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
      request.setParameter(PAYMENT_0_TRANSAMT, "####");
      request.setParameter(PaymentConstants.TOTAL_TRANS_AMOUNT, "500.00");
      request.setParameter("payment_0_cardNumber", "1000011000000012");
   }


   /**
    * This method sets the "common to all test cases" request parameters.
    */
   public void requestparameterGeneric()
   {
      request.setParameter("balanceType", "PC");
      request.setParameter("payment_totalTrans", "1");
      request.setParameter(PaymentConstants.TOTAL_TRANS_AMOUNT, "512.5");
      request.setParameter("payment_0_amountPaid", "487.50");
      request.setParameter("payment_0_chargeAmount", "12.19");
      request.setParameter("payment_0_type", "VISA|DCard");
      request.setParameter("payment_0_paymenttypecode", "VISA");
      request.setParameter("payment_0_paymentmethod", "DCard");
      request.setParameter("payment_0_cardNumber", "4242425000000009");
      request.setParameter("payment_0_nameOnCard", "test");
      request.setParameter("payment_0_expiryMonth", "12");
      request.setParameter("payment_0_expiryYear", "15");
      request.setParameter("payment_0_securityCode", "444");
      request.setParameter(BookingConstants.DEPOSIT_TYPE,
         DispatcherConstants.FULL_COST);
      request.setParameter("houseName", "1234");
      request.setParameter("addressLine1", "richmond road");
      request.setParameter("city", "bangalore");
      request.setParameter("county", "india");
      request.setParameter("postCode", "CV12AQ");
      request.setParameter("dayTimePhone", "9886432716");
      request
         .setParameter("emailAddress", "roopesh.s@sonata-software.com");
      request.setHeaderField("Accept-Language", "en-GB");
      request.setParameter("b", "6000");
   }

   /**
    * This method is the setup for testPaymentSuccessURLTestCase.
    *
    * @throws IOException the IOException object.
    */

   public void setupPaymentSuccessURLTestCase() throws IOException
   {
      instantiator();
      requestparameterGeneric();
      requestParameterSetterValid();
      requestgenerator();

   }

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   protected void setUpInValid1() throws Exception
   {
      instantiator1();
      requestparameterGeneric();
      requestParameterSetterInValid1();
      requestgenerator();
   }

   /**
    * This method is the setup method for an invalid valid setting. (the
    * setting induces a PaymentValidationException )
    *
    * @throws Exception the Exception object.
    */
   protected void setUpInValid2() throws Exception
   {
      instantiator();
      requestparameterGeneric();
      requestParameterSetterInValid3();
      requestgenerator();
   }

   /**
    * This method is the setup method for an invalid valid setting. (the
    * setting induces a PaymentValidationException )
    *
    * @throws Exception the Exception object.
    */
   protected void setUpInValid3() throws Exception
   {
      instantiator();
      requestparameterGeneric();
      requestParameterSetterInValid2();
      request.setParameter("payment_0_type", "Mastercard|DCard");
      request.setParameter("payment_0_paymenttypecode", "Mastercard");
      request.setParameter("payment_0_paymentmethod", "DCard");
      request.setParameter("payment_0_cardNumber", "1000011000000005");
      requestgenerator();
   }

   /**
    * This method is to test the servlet with the paymentSuccessURL set.
    *
    * @throws IOException the IOException object
    */
   /*public void testPaymentSuccessURLTestCase() throws IOException
   {
      setupPaymentSuccessURLTestCase();
      PostPaymentServlet postpaymentServlet = new PostPaymentServlet();
      try
      {
         postpaymentServlet.doPost(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (IllegalArgumentException e)
      {
         e.printStackTrace();
      }
   }*/

   /**
    * To test the PaymentServlet for a failure case . This is to enter the
    * "max failure count greater than the conf entry" block in the servlet
    *
    * @throws Exception the Exception object.
    *
    */
   public void testInValidServlet1() throws Exception
   {
      setUpInValid1();
      PostPaymentServlet postpaymentServlet = new PostPaymentServlet();
      try
      {
         postpaymentServlet.doPost(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (IllegalArgumentException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * To test the PaymentServlet for a failure case . This throws a
    * PostPaymentProcessorException
    *
    * @throws Exception the Exception object.
    *
    */
   public void testInValidServlet2() throws Exception
   {
      setUpInValid2();
      PostPaymentServlet postpaymentServlet = new PostPaymentServlet();
      try
      {
         postpaymentServlet.doPost(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (IllegalArgumentException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * To test the PaymentServlet for a failure case .
    *
    * @throws Exception the Exception object.
    *
    */
   public void testInValidServlet3() throws Exception
   {
      setUpInValid3();
      PostPaymentServlet postpaymentServlet = new PostPaymentServlet();
      try
      {
         postpaymentServlet.doPost(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (IllegalArgumentException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Creates the BookingComponent.
    *
    * @return bookingComponent object
    */
   private static BookingInfo getBookingInfo()
   {
      ClientApplication clientApplicationName =
         ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount =
         new Money(new BigDecimal("10"), Currency.getInstance(GBP));
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new HashMap<Integer, List<PassengerSummary>>();
      List<PassengerSummary> passengerSummaryList =
         new ArrayList<PassengerSummary>();
      passengerSummaryList.add(new PassengerSummary(
         AgeClassification.ADULT, true, PASSENGERID));
      passengerRoomSummary.put(1, passengerSummaryList);

      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL,
            totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      bookingComponent.setAccommodationSummary(TestDataSetup
         .getAccommodationSummary("TRACS"));
      Map<String, BigDecimal> cardChargesMap =
         new HashMap<String, BigDecimal>();
      BigDecimal percentage = new BigDecimal("2.5");
      cardChargesMap.put("VISA", percentage);
      cardChargesMap.put("AMERICAN_EXPRESS", percentage);

      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      bookingComponent.setCardChargesMap(cardChargesMap);
      DiscountComponent discountComponent =
         new DiscountComponent(DispatcherConstants.PRICE_BEAT, new Money(
            new BigDecimal("12.60"), Currency.getInstance(GBP)), true,
            false);
      java.util.List<DiscountComponent> discountComponents =
         new java.util.ArrayList<DiscountComponent>();
      discountComponents.add(discountComponent);
      bookingComponent.setDiscountComponents(discountComponents);

      bookingComponent.setPaymentFailureURL("abc");
      bookingComponent.setPassengerRoomSummary(passengerRoomSummary);
      Map<String, List<PaymentType>> paymentType = new HashMap<String, List<PaymentType>>();
      PaymentType type =
         new PaymentType("VISA", "Visa", "DCard");
      List<PaymentType> payList = new ArrayList<PaymentType>();
      CardType cardType = new CardType(THREE, THREE);
      cardType.setIsIssueNumberRequired(Boolean.FALSE);
      type.setCardType(cardType);
      payList.add(type);
      paymentType.put("CNP", payList);
      bookingComponent.setPaymentType(paymentType);
      BookingInfo bookingInfo = new BookingInfo(bookingComponent);
      bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal(
         "500"), Currency.getInstance(GBP)));
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal(
         "512.50"), Currency.getInstance(GBP)));
      bookingInfo.setCalculatedCardCharge(new Money(
         new BigDecimal("12.50"), Currency.getInstance(GBP)));
      bookingInfo.setThreeDAuth(false);
      TransactionTrackingData trackingData = new TransactionTrackingData("", "1");
      bookingInfo.setTrackingData(trackingData);
      return bookingInfo;
   }

   /**
    * Creates the BookingComponent.
    *
    * @return bookingComponent object
    */
   private static BookingInfo getBookingInfo1()
   {
      ClientApplication clientApplicationName =
         ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount =
         new Money(new BigDecimal("10"), Currency.getInstance(GBP));
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new HashMap<Integer, List<PassengerSummary>>();
      List<PassengerSummary> passengerSummaryList =
         new ArrayList<PassengerSummary>();
      passengerSummaryList.add(new PassengerSummary(
         AgeClassification.ADULT, true, PASSENGERID));
      passengerRoomSummary.put(1, passengerSummaryList);

      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL,
            totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      bookingComponent.setAccommodationSummary(TestDataSetup
         .getAccommodationSummary("TRACS"));
      Map<String, BigDecimal> cardChargesMap =
         new HashMap<String, BigDecimal>();
      BigDecimal percentage = new BigDecimal("2.5");
      cardChargesMap.put("VISA", percentage);
      cardChargesMap.put("AMERICAN_EXPRESS", percentage);

      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      bookingComponent.setCardChargesMap(cardChargesMap);
      DiscountComponent discountComponent =
         new DiscountComponent(DispatcherConstants.PRICE_BEAT, new Money(
            new BigDecimal("12.60"), Currency.getInstance(GBP)), true,
            false);
      java.util.List<DiscountComponent> discountComponents =
         new java.util.ArrayList<DiscountComponent>();
      discountComponents.add(discountComponent);
      bookingComponent.setDiscountComponents(discountComponents);

      bookingComponent.setPaymentFailureURL("abc");
      bookingComponent.setPassengerRoomSummary(passengerRoomSummary);
      Map<String, List<PaymentType>> paymentType = new HashMap<String, List<PaymentType>>();
      PaymentType type =
         new PaymentType("VISA", "Visa", "DCard");
      List<PaymentType> payList = new ArrayList<PaymentType>();
      CardType cardType = new CardType(THREE, THREE);
      cardType.setIsIssueNumberRequired(Boolean.FALSE);
      type.setCardType(cardType);
      payList.add(type);
      paymentType.put("CNP", payList);
      bookingComponent.setPaymentType(paymentType);
      BookingInfo bookingInfo = new BookingInfo(bookingComponent);
      bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal(
         "500"), Currency.getInstance(GBP)));
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal(
         "512.50"), Currency.getInstance(GBP)));
      bookingInfo.setCalculatedCardCharge(new Money(
         new BigDecimal("12.50"), Currency.getInstance(GBP)));
      bookingInfo.setThreeDAuth(false);
      TransactionTrackingData trackingData = new TransactionTrackingData("", "2");
      bookingInfo.setTrackingData(trackingData);
      return bookingInfo;
   }

   /**
    * Tests the DispatcherConstants private constructor.
    */
   public void testDispatcherConstantsPrivateConstructor()
   {
      try
      {
         Constructor< ? >[] cons =
            DispatcherConstants.class.getDeclaredConstructors();
         cons[0].setAccessible(true);
         cons[0].newInstance();
      }
      catch (InstantiationException e)
      {
         LogWriter.logWarningMessage("Exception : " + e);
      }
      catch (IllegalAccessException e)
      {
         LogWriter.logWarningMessage("Exception : " + e);
      }
      catch (InvocationTargetException ite)
      {
         LogWriter.logWarningMessage("Exception : " + ite);
      }
   }

   /**
    * This method is to test the private method buildTermUrl. (makes use of
    * the Reflection api)
    */
   public void testBuildTermUrl()
   {
      try
      {
         sr = new ServletRunner();
         String servletUrl = "http://test.meterware.com/testServlet";
         request = new PostMethodWebRequest(servletUrl);
         sc = sr.newClient();

         request.setParameter("b", "6000");
         ic = sc.newInvocation(request);
         req = ic.getRequest();
         res = ic.getResponse();
         Method[] method = PostPaymentServlet.class.getDeclaredMethods();
         for (int i = 0; i < method.length; i++)
         {
            method[i].setAccessible(true);
            if (method[i].getName().equals("buildTermUrl"))
            {
               method[i].invoke(new PostPaymentServlet(), req);
            }
         }
      }
      catch (MalformedURLException mue)
      {
         mue.printStackTrace();
      }
      catch (IllegalArgumentException e)
      {
         e.printStackTrace();
      }
      catch (InvocationTargetException e)
      {
         e.printStackTrace();
      }
      catch (IOException ioe)
      {

         ioe.printStackTrace();
      }
      catch (IllegalAccessException iae)
      {
         iae.printStackTrace();
      }

   }
}
