/*
 * Copyright (C)2008-2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PostAuthenticationServletTestCase.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-07-03 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.InvocationContext;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.CardType;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.DiscountComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.utility.TestDataSetup;

/**
 * 
 * TestCase for PostAuthenticationServletTestCase Class.
 * 
 */
public class PostAuthenticationServletTestCase extends TestCase
{

   /** Variable for InvocationContext. */
   private InvocationContext ic = null;

   /** Variable for HttpServletRequest. */
   private HttpServletRequest req = null;

   /** Variable for HttpServletResponse. */
   private HttpServletResponse res = null;

   /** variable to hold the UUID. */
   private static UUID uuid;

   /** variable to hold the ServletRunner. */
   private ServletRunner sr = null;

   /** variable to hold the request. */
   private WebRequest request = null;

   /** variable to hold ServletUnitClient. */
   private ServletUnitClient sc = null;

   /** variable to hold transaction amount. */
   private static final String PAYMENT_0_TRANSAMT = "payment_0_transamt";

   /** transaction amount constant. */
   private static final String TRANSAMT = "125";

   /** Set the value for Number Of Transactions. */
   private int contributionCount = 0;

   /** Setting the value for index. */
   private static int index = 0;

   /** A map containing values entered by the user. */
   private Map<String, String> requestParameterMap = new HashMap<String, String>();

   /** Setting the value for Transaction Amount. */
   private String transactionAmount = "1000";

   /** Setting the value for Charge. */
   private String charge = "10";

   /** Setting the value for AUTH_CODE. */
   private String authcode = "444";

   /** Variable for GBP currency code. */
   private static final String GBP = "GBP";

   /** The constant THREE. */
   private static final int THREE = 3;

   /**
    * This method is the setup method for a valid setting.
    * 
    * @throws PaymentValidationException the PaymentValidationException.
    * @throws IOException the IOException.
    */
   protected void setUpValid() throws PaymentValidationException, IOException
   {
      instantiator1();
      requestparameterGeneric();
      requestParameterSetterValid();
      requestgenerator();
   }

   /**
    * This method sets the "common to all test cases" request parameters.
    */
   public void requestparameterGeneric()
   {
      request.setParameter("balanceType", "PC");
      request.setParameter("payment_totalTrans", "1");
      request.setParameter(PaymentConstants.TOTAL_TRANS_AMOUNT, "512.50");
      request.setParameter("payment_0_amountPaid", "500.00");
      request.setParameter("payment_0_chargeAmount", "12.50");
      request.setParameter("payment_0_type", "VISA|Dcard");
      request.setParameter("payment_0_paymenttypecode", "VISA - Credit");
      request.setParameter("payment_0_type", "VISA|Dcard");
      request.setParameter("payment_0_cardNumber", "4242425000000009");
      request.setParameter("payment_0_nameOnCard", "test");
      request.setParameter("payment_0_expiryMonth", "01");
      request.setParameter("payment_0_expiryYear", "12");
      request.setParameter("payment_0_securityCode", "444");
      request.setParameter(BookingConstants.DEPOSIT_TYPE, DispatcherConstants.FULL_COST);
      request.setParameter("houseName", "1234");
      request.setParameter("addressLine1", "richmond road");
      request.setParameter("city", "bangalore");
      request.setParameter("county", "india");
      request.setParameter("postCode", "CV12AQ");
      request.setParameter("dayTimePhone", "9886432716");
      request.setParameter("emailAddress", "roopesh.s@sonata-software.com");
      request.setParameter("PaRes", "xyz");
      request.setParameter("MD", "testMD");
      request.setParameter("tourOperatorTermsAccepted", "on");
      request.setParameter("privacyPolicyAccepted", "on");
      request.setHeaderField("Accept-Language", "en-GB");
   }

   /** This method sets the request parameters. */
   public void requestParameterSetterValid()
   {
      request.setParameter(PAYMENT_0_TRANSAMT, TRANSAMT);
      // CHECKSTYLE:OFF
      PaymentData paymentData = new PaymentData(getBookingInfo());
      // CHECKSTYLE:ON
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
   }

   /**
    * This method instantiates the required objects.
    * 
    * @throws PaymentValidationException the payment exception.
    **/
   public void instantiator1() throws PaymentValidationException
   {
      // try
      // {
      sr = new ServletRunner();
      // }
      // catch (IOException e)
      // {
      // e.printStackTrace();
      // }
      // catch (SAXException e)
      // {
      // e.printStackTrace();
      // }
      request = new PostMethodWebRequest("http://localhost/postAuthServlet");
      sc = sr.newClient();
   }

   /**
    * This method instantiates the required objects.
    * 
    * @throws PaymentValidationException the payment exception.
    **/
   public void instantiator2() throws PaymentValidationException
   {
      request = new PostMethodWebRequest("http://localhost/postAuthServlet");
      sc = sr.newClient();
   }

   /**
    * This method set paymentTransaction objects.
    * 
    * @return list the paymentTransaction list.
    * @throws PaymentValidationException the payment exception.
    **/
   public List<PaymentTransaction> paymentTransaction() throws PaymentValidationException
   {
      putRequestParameters(requestParameterMap, PaymentMethod.DATACASH.getCode(),
         transactionAmount, charge, authcode, "SOLO", index);
      requestParameterMap.put(PaymentConstants.PAYMENT + index + PaymentConstants.CARD_NUMBER,
         "633499110000000003");
      requestParameterMap.put(PaymentConstants.PAYMENT + index + PaymentConstants.NAME_ON_CARD,
         "VENKAT");
      requestParameterMap.put(PaymentConstants.PAYMENT + index + PaymentConstants.EXPIRY_MONTH,
         "08");
      requestParameterMap
         .put(PaymentConstants.PAYMENT + index + PaymentConstants.EXPIRY_YEAR, "20");
      requestParameterMap.put(PaymentConstants.PAYMENT + index + PaymentConstants.SECURITY_CODE,
         authcode);
      requestParameterMap.put(PaymentConstants.PAYMENT + index + PaymentConstants.POST_CODE,
         "ba21db");
      requestParameterMap
         .put(PaymentConstants.PAYMENT + index + PaymentConstants.ISSUE_NUMBER, "1");
      requestParameterMap
         .put(PaymentConstants.PAYMENT + index + PaymentConstants.START_MONTH, "05");
      requestParameterMap.put(PaymentConstants.PAYMENT + index + PaymentConstants.START_YEAR, "04");

      PaymentTransaction paymentTransactions =
         PaymentTransactionFactory.getPaymentTransactions(contributionCount, requestParameterMap,
            getBookingInfo());

      List<PaymentTransaction> paymentTransactionsList = new ArrayList<PaymentTransaction>();
      paymentTransactionsList.add(paymentTransactions);

      return paymentTransactionsList;
   }

   /**
    * Put the Parameters into request Parameter Map.
    * 
    * @param requestParameters is map to pass the values.
    * @param paymentmethod is Payment Method.
    * @param transamt is Transaction Amount.
    * @param chargeAmount is Transaction Charge Amount.
    * @param referenceNumber is Reference Number.
    * @param paymenttypecode is Payment Type Code.
    * @param itr is index value.
    */
   private void putRequestParameters(Map<String, String> requestParameters, String paymentmethod,
      String transamt, String chargeAmount, String referenceNumber, String paymenttypecode, int itr)
   {
      requestParameterMap.put(PaymentConstants.PAYMENT + itr + PaymentConstants.PAYMENT_METHOD,
         paymentmethod);
      requestParameterMap.put(PaymentConstants.PAYMENT + itr + PaymentConstants.TRANSACTION_AMOUNT,
         transamt);
      requestParameterMap.put(PaymentConstants.PAYMENT + itr + PaymentConstants.CHARGE,
         chargeAmount);
      requestParameterMap.put(PaymentConstants.PAYMENT + itr + PaymentConstants.AUTH_CODE,
         referenceNumber);
      requestParameterMap.put(PaymentConstants.PAYMENT + itr + PaymentConstants.PAYMENT_TYPE_CODE,
         paymenttypecode);
   }

   /**
    * This generates the dummy request and response objects.
    * 
    * @throws IOException the IOException object
    * */
   public void requestgenerator() throws IOException
   {
      ic = sc.newInvocation(request);
      req = ic.getRequest();
      res = ic.getResponse();
   }

   /**
    * To test the PostAuthenticationServlet .
    * 
    * @throws IOException the IOException.
    * @throws PaymentValidationException the PaymentValidationException.
    */
   public void testServlet() throws PaymentValidationException, IOException
   {
      setUpValid();
      PostPaymentServlet postpaymentServlet = new PostPaymentServlet();
      try
      {
         postpaymentServlet.doPost(req, res);
         instantiator2();
         request.setParameter(DispatcherConstants.TOKEN, uuid.toString());
         request.setParameter("PaRes", "xyz");
         request.setParameter("MD", request.getParameter("MD"));
         requestgenerator();
         PostAuthenticationServlet paymentServlet = new PostAuthenticationServlet();
         paymentServlet.doPost(req, res);
      }
      catch (ServletException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (IllegalArgumentException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Creates the BookingComponent.
    * 
    * @return bookingComponent object
    */
   private static BookingInfo getBookingInfo()
   {
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance(GBP));
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new HashMap<Integer, List<PassengerSummary>>();
      List<PassengerSummary> passengerSummaryList = new ArrayList<PassengerSummary>();
      passengerSummaryList.add(new PassengerSummary(AgeClassification.ADULT, true, THREE));
      passengerRoomSummary.put(1, passengerSummaryList);

      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      bookingComponent.setAccommodationSummary(TestDataSetup.getAccommodationSummary("TRACS"));
      Map<String, BigDecimal> cardChargesMap = new HashMap<String, BigDecimal>();
      BigDecimal percentage = new BigDecimal("2.5");
      cardChargesMap.put("VISA", percentage);
      cardChargesMap.put("AMERICAN_EXPRESS", percentage);

      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      bookingComponent.setCardChargesMap(cardChargesMap);
      DiscountComponent discountComponent =
         new DiscountComponent(DispatcherConstants.PRICE_BEAT, new Money(new BigDecimal("12.60"),
            Currency.getInstance(GBP)), true, false);
      java.util.List<DiscountComponent> discountComponents =
         new java.util.ArrayList<DiscountComponent>();
      discountComponents.add(discountComponent);
      bookingComponent.setDiscountComponents(discountComponents);

      bookingComponent.setPaymentFailureURL("abc");
      bookingComponent.setPassengerRoomSummary(passengerRoomSummary);
      Map<String, List<PaymentType>> paymentType = new HashMap<String, List<PaymentType>>();
      PaymentType type = new PaymentType("VISA", "Visa", "DCard");
      List<PaymentType> payList = new ArrayList<PaymentType>();
      CardType cardType = new CardType(THREE, THREE);
      cardType.setIsIssueNumberRequired(Boolean.FALSE);
      type.setCardType(cardType);
      payList.add(type);
      paymentType.put("CNP", payList);
      bookingComponent.setPaymentType(paymentType);
      BookingInfo bookingInfo = new BookingInfo(bookingComponent);
      bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal("500"), Currency
         .getInstance(GBP)));
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal("512.50"), Currency
         .getInstance(GBP)));
      bookingInfo.setCalculatedCardCharge(new Money(new BigDecimal("12.50"), Currency
         .getInstance(GBP)));
      bookingInfo.setThreeDAuth(false);
      TransactionTrackingData trackingData = new TransactionTrackingData("", "1");
      bookingInfo.setTrackingData(trackingData);
      return bookingInfo;
   }

}
