/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PackageType;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.exception.ValidationException;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.utility.TestDataSetup;
import com.tui.uk.validation.constants.ValidationConstants;

/**
 *
 * TestCase for PostPaymentServlet Class.
 *
 */
public class ThomsonPostPaymentProcessorTestCase extends TestCase
{
   /** variable to hold the UUID. */
   private static UUID uuid;

   /** The request parameter map. */
   private Map<String, String[]> requestParameterMap = new HashMap<String, String[]>();

   /** The full cost constant. */
   private static final String FULL_COST = "FullCost";

   /** The constant DAY. */
   private static final int DAY = -1100;

   /** The constant PASSENGER_COUNT. */
   private static final String PASSENGER_COUNT_1 = "1";

   /** The GBP currency. */
   private static final Currency GBP_CURRENCY = Currency.getInstance("GBP");


   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();

      requestParameterMap.put("balanceType", new String[]{"PC"});
      requestParameterMap.put("payment_totalTrans", new String[]{"1"});
      requestParameterMap.put("payment_1_paymentmethod", new String[]{PaymentMethod
         .POST_PAYMENT.getCode()});
      requestParameterMap.put(PaymentConstants.PAYMENT + PaymentConstants.PINPADTRANSACTIONCOUNT,
         new String[]{"1"});
      requestParameterMap.put(DispatcherConstants.BALANCE_TYPE, new String[]{FULL_COST});
      requestParameterMap.put("payment_0_type", new String[]{"Mastercard|DCard"});
      requestParameterMap.put("payment_0_cardNumber", new String[]{"1000011000000005"});
      requestParameterMap.put("payment_0_nameOnCard", new String[]{"xxxxxxxx"});
      requestParameterMap.put("payment_0_expiryMonth", new String[]{"12"});
      requestParameterMap.put("payment_0_expiryYear", new String[]{"09"});
      requestParameterMap.put("payment_0_postCode", new String[]{"CV24EB"});
      requestParameterMap.put("payment_0_securityCode", new String[]{"444"});
      requestParameterMap.put("tourOperatorTermsAccepted", new String[]{"on"});
      requestParameterMap.put("privacyPolicyAccepted", new String[]{"on"});
      requestParameterMap.put(PaymentConstants.PAYMENT + "0"
         + PaymentConstants.TRANSACTION_AMOUNT , new String[]{"100"});
   }

   /** To test the valid case.
    * @throws ValidationException the validation exception.
    * @throws NumberFormatException */
   public void testValidProcess() throws ValidationException
   {
      PackageType packageType = PackageType.PreDefinedPackage;
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(packageType));
      paymentData.getBookingInfo().getBookingComponent().
         setAccommodationSummary(TestDataSetup.getAccommodationSummary("HOPLA_PEG"));
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(DispatcherConstants.TOKEN, new String[]{uuid.toString()});
      requestParameterMap.put("total_transamt", new String[]{"512.5"});
      requestParameterMap.put("promotionalCode", new String[]{"8405"});
      requestParameterMap.put("isPromoCodeApplied", new String[]{"false"});

      ThomsonPostPaymentProcessor processor = new ThomsonPostPaymentProcessor(
         paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception : " + ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception " + pve.getMessage());
      }
   }

   /** To test the valid case for non payment data validation.
    *
    * @throws ValidationException the validation exception.
    * */
   public void testNonPaymentData() throws ValidationException
   {
      PackageType packageType = PackageType.PreDefinedPackage;
      PaymentData paymentData = new PaymentData(getBookingInfo(packageType));
      paymentData.getBookingInfo().getBookingComponent().setAccommodationSummary(
         TestDataSetup.getAccommodationSummary("HOPLA_PEG"));
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(DispatcherConstants.TOKEN, new String[] {uuid.toString()});
      requestParameterMap.put("total_transamt", new String[] {"512.5"});
      requestParameterMap.put("depositType", new String[] {FULL_COST});
      requestParameterMap.put("promotionalCode", new String[] {"8405"});
      requestParameterMap.put("isPromoCodeApplied", new String[] {"false"});
      setChildDetails();
      setInfantDetails();

      requestParameterMap.put("personaldetails_5_ageBetween65To75", new String[] {"true"});
      requestParameterMap.put("personaldetails_4_ageBetween76To84", new String[] {"true"});

      ThomsonPostPaymentProcessor processor =
         new ThomsonPostPaymentProcessor(paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception : " + ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception " + pve.getMessage());
      }
   }

   /** To test the invalid case.
    * @throws ValidationException the validation exception.
    * @throws NumberFormatException */
  /* public void testInvalidProcess()throws ValidationException
   {
      PackageType packageType = PackageType.AccommodationOnly;
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(packageType));
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(DispatcherConstants.TOKEN, new String[]{uuid.toString()});
      requestParameterMap.put("total_transamt", new String[]{"10"});
      requestParameterMap.put("depositType", new String[]{FULL_COST});

      ThomsonPostPaymentProcessor processor = new ThomsonPostPaymentProcessor(
         paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception : " + ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception " + pve.getMessage());
      }
   }*/

   /** To test the valid case.
    * @throws ValidationException the validation exception.
    * */
   public void testPartialDepositType() throws ValidationException
   {
      Calendar date = Calendar.getInstance();
      //CHECKSTYLE:OFF
      date.set(2008, 10, 22);
      //CHECKSTYLE:ON
      date.getTime();

      DepositComponent depositComponent = new DepositComponent("partialPayment",
         new Money(new BigDecimal("10.25"), Currency.getInstance("GBP")), date.getTime(),
         new Money(new BigDecimal("100"), Currency.getInstance("GBP")));

      List<DepositComponent> depositList = new ArrayList<DepositComponent>();
      depositList.add(depositComponent);

      PackageType packageType = PackageType.AccommodationOnly;
      BookingInfo bookingInfo = TestDataSetup.getBookingInfo(packageType);
      bookingInfo.getBookingComponent().setDepositComponents(depositList);

      PaymentData paymentData = new PaymentData(bookingInfo);
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(DispatcherConstants.TOKEN, new String[]{uuid.toString()});
      requestParameterMap.put("total_transamt", new String[]{"NA"});
      requestParameterMap.put("depositType", new String[]{"partialPayment"});

      ThomsonPostPaymentProcessor processor = new ThomsonPostPaymentProcessor(
         paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception" + ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception" + pve.getMessage());
      }
   }

   /**
    * This method tests if passenger name is populated properly or not.
    */
   public void testPreProcess()
   {
      PackageType packageType = PackageType.AccommodationOnly;
      BookingInfo bookingInfo = TestDataSetup.getBookingInfo(packageType);

      requestParameterMap.put("personaldetails_0_foreName", new String[]{"test"});

      PaymentData paymentData = new PaymentData(bookingInfo);
      ThomsonPostPaymentProcessor processor = new ThomsonPostPaymentProcessor(
                 paymentData, requestParameterMap);
      try
      {
          processor.preProcess();
          assertEquals("test", bookingInfo.getBookingComponent().getPassengerRoomSummary().get(1)
                  .get(0).getForeName());
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         LogWriter.logErrorMessage("Unexpected Exception" + ppe.getMessage());
      }
   }

   /**
    * Setting child details.
    */
   private void setChildDetails()
   {
      Calendar dobCalendar = Calendar.getInstance();

      // making DOB
      dobCalendar.add(Calendar.DATE, DAY);

      requestParameterMap.put(ValidationConstants.PERSONALDETAILS + "2" + ValidationConstants.DAY,
         new String[] {String.valueOf(dobCalendar.get(Calendar.DAY_OF_MONTH))});
      requestParameterMap.put(
         ValidationConstants.PERSONALDETAILS + "2" + ValidationConstants.MONTH,
         new String[] {String.valueOf(dobCalendar.get(Calendar.MONTH))});
      requestParameterMap.put(ValidationConstants.PERSONALDETAILS + "2" + ValidationConstants.YEAR,
         new String[] {String.valueOf(dobCalendar.get(Calendar.YEAR))});
   }

   /**
    * Setting infant details.
    */
   private void setInfantDetails()
   {
      Calendar cal = Calendar.getInstance();

      requestParameterMap
         .put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT_1 + ValidationConstants.DAY,
            new String[] {String.valueOf(cal.get(Calendar.DAY_OF_MONTH))});
      requestParameterMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT_1
         + ValidationConstants.MONTH, new String[] {String.valueOf(cal.get(Calendar.MONTH))});
      requestParameterMap.put(ValidationConstants.PERSONALDETAILS + PASSENGER_COUNT_1
         + ValidationConstants.YEAR, new String[] {String.valueOf(cal.get(Calendar.YEAR))});
   }

   /**
    * Create the BookingInfo.
    *
    * @param packageType the PackageType object.
    *
    * @return BookingInfo object.
    */
   private static BookingInfo getBookingInfo(PackageType packageType)
   {
      BookingInfo bookingInfo = getBookingInfo();
      bookingInfo.getBookingComponent().setPackageType(packageType);
      return bookingInfo;
   }

   /**
    * Create the BookingInfo.
    *
    * @return BookingInfo object.
    */
   private static BookingInfo getBookingInfo()
   {
      BookingInfo bookingInfo = new BookingInfo(TestDataSetup.getBookingComponent1());

      bookingInfo.setCalculatedPayableAmount(new Money(new BigDecimal("500"), GBP_CURRENCY));
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal("500.00"), GBP_CURRENCY));
      bookingInfo.setCalculatedCardCharge(new Money(new BigDecimal("12.50"), GBP_CURRENCY));
      TransactionTrackingData trackingData = new TransactionTrackingData("", "");
      bookingInfo.setTrackingData(trackingData);
      return bookingInfo;
   }
}
