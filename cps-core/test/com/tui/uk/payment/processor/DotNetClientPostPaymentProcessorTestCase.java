/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.tui.uk.domain.BookingInfo;
import com.tui.uk.exception.ValidationException;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.utility.TestDataSetup;

/**
 *
 * TestCase for PostPaymentServlet Class.
 *
 */
public class DotNetClientPostPaymentProcessorTestCase extends TestCase
{

   /** Percent Card charge. */
   private static final double PERCENT_CHARGE = 2.5;

   /** Flat card charge. */
   private static final double FLAT_CHARGE = 10.0;

   /** Transaction amount. */
   private static final double TRANSACTION_AMOUNT = 100.00;

   /** The exception constant. */
   private static final String UNEXPECTED_EXCEPTION = "Unexpected Exception";

   /** The request parameter map. */
   private Map<String, String[]> requestParameterMap =
      new HashMap<String, String[]>();

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();
      requestParameterMap.put("total_transamt", new String[]{"102.50"});
      requestParameterMap.put("payment_0_cardNumber", new String[]{"4444333322221111"});
      requestParameterMap.put("payment_0_securityCode", new String[]{"444"});
      requestParameterMap.put("payment_0_expiryMonth", new String[]{"01"});
      requestParameterMap.put("payment_0_expiryYear", new String[]{"14"});
      requestParameterMap.put("payment_0_nameOnCard", new String[]{"Testname"});
      requestParameterMap.put("payment_0_paymenttypecode", new String[]{"VISA"});

   }

   /**
    * To test the percent charge case.
    * @throws ValidationException the ValidationException
    * @throws ValidationException the validation exception.
    */
   public void testPercentCardCharges() throws ValidationException
   {
      BookingInfo bookingInfo = TestDataSetup.getDotNetBookingInfo(TRANSACTION_AMOUNT);
      requestParameterMap.put("payment_0_paymenttypecode", new String[]{"MASTERCARD"});
      requestParameterMap.put("payment_0_cardNumber", new String[]{"1000011000000005"});
      PaymentData paymentData = new PaymentData(bookingInfo);
      DotNetClientPostPaymentProcessor processor = new DotNetClientPostPaymentProcessor(
         paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         fail(ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         fail(pve.getMessage());
      }
      assertEquals(PERCENT_CHARGE, bookingInfo
         .getCalculatedCardCharge().getAmount().doubleValue());
   }

   /**
    * To test the  flat charge case.
    * @throws ValidationException the ValidationException
    * @throws ValidationException the validation exception.
    */
   public void testFlatTestCardCharges() throws ValidationException
   {
      BookingInfo bookingInfo = TestDataSetup.getDotNetBookingInfo(TRANSACTION_AMOUNT);
      requestParameterMap.put("total_transamt", new String[]{"110.00"});

      PaymentData paymentData = new PaymentData(bookingInfo);
      DotNetClientPostPaymentProcessor processor = new DotNetClientPostPaymentProcessor(
         paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         fail(UNEXPECTED_EXCEPTION);
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         fail(UNEXPECTED_EXCEPTION);
      }
      assertEquals(FLAT_CHARGE, bookingInfo
         .getCalculatedCardCharge().getAmount().doubleValue());
   }

}
