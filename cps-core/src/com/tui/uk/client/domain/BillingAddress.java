package com.tui.uk.client.domain;

import java.io.Serializable;

public class BillingAddress implements Cloneable, Serializable {

	/** The generated serial version id. */
	private static final long serialVersionUID = -6L;
	private String addressOwner;
	private String addressStatus;
	private String city;
	private String countryCode;
	private String name;
	private String postcode;
	private String region;
	private String streetAddessOne;
	private String streetAddessTwo;

	public String getAddressOwner() {
		return addressOwner;
	}

	public void setAddressOwner(String addressOwner) {
		this.addressOwner = addressOwner;
	}

	public String getAddressStatus() {
		return addressStatus;
	}

	public void setAddressStatus(String addressStatus) {
		this.addressStatus = addressStatus;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStreetAddessOne() {
		return streetAddessOne;
	}

	public void setStreetAddessOne(String streetAddessOne) {
		this.streetAddessOne = streetAddessOne;
	}

	public String getStreetAddessTwo() {
		return streetAddessTwo;
	}

	public void setStreetAddessTwo(String streetAddessTwo) {
		this.streetAddessTwo = streetAddessTwo;
	}

}
