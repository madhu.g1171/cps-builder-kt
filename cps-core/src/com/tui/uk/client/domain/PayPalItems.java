/**
 * 
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * @author sreenivasulu.k
 *
 */
public class PayPalItems implements Cloneable, Serializable{	
	
	private static final long serialVersionUID = -12L;	
	/**
	 * name represents itemName
	 */
	private String name;
	/**
	 * quantity represents item quantity
	 */
	private Long quantity;
	
	/**
	 *  represents item amount
	 */
	private String amount;
	
	/**
	 * @return paypal item amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param sets amount 
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 *  defines empty constructor with default values.
	 */
	public  PayPalItems(){}	
	
	/**
	 * defines the parameterised constructor with custom values.
	 * @param name
	 * @param quantity
	 */
	public PayPalItems(String name,Long quantity,String amount){
		this.name=name;
		this.quantity=quantity;
		this.amount=amount;
	}
	
	/**
	 * @return string which defines item name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name  sets the name in the item
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * @return returns item quantity
	 */
	public Long getQuantity() {
		return quantity;
	}
	
	/**
	 * @param quantity sets the quantity in the item.
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}	

}
