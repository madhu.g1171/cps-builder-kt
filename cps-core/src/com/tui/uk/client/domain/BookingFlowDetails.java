/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BookingFlowDetails.java$
 *
 * $Revision: $
 *
 * $Date: Dec 22, 2009$
 *
 * Author: vinothkumar.k
 *
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class is used to hold the details about the booking flow
 * and the payment flow which is used to distinguish th type of flow.
 *
 * @author vinothkumar.k
 *
 */
public class BookingFlowDetails implements Serializable
{
   /**
    * The generated SerialID.
    */
   private static final long serialVersionUID = 6596992056565236656L;

   /** The payment flows like PAYMENT, NO_PAYMENT, REFUND. **/
   private PaymentFlow paymentFlow;

   /** The booking flows like OLBP, AMEND, CANCEL. **/
   private BookingFlow bookingFlow;

   /** The constructor for BookingFlowDetails with booking flow and payment flow.
    *
    * @param bookingFlow the bookingFlow name.
    * @param paymentFlow the paymentFlow name.
    **/
   public BookingFlowDetails(BookingFlow bookingFlow, PaymentFlow paymentFlow)
   {
      this.bookingFlow = bookingFlow;
      this.paymentFlow = paymentFlow;
   }

   /**
    * Gets the paymentFlow.
    *
    * @return paymentFlow the paymentFlow name.
    */
   public PaymentFlow getPaymentFlow()
   {
      return paymentFlow;
   }

   /**
    * Gets the bookingFlow.
    *
    * @return bookingFlow the bookingFlow name.
    */
   public BookingFlow getBookingFlow()
   {
      return bookingFlow;
   }
}
