/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: PassengersDataPanel.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2009-06-22 08:25:26 $
 * 
 * $Author: roopesh.s@sonata-software.com $
 * 
 * 
 * $Log: $.
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents all the possible known field name used in passengers 
 * data panel in cps payment page.
 * This enum is used for validating the non payment data in important 
 * information data panel.
 * Enum constructor contain two parameters key and pattern. 
 * Key represent field name, which is used in passenger section in payment page. 
 * Pattern represent pattern, which is used for validating field value.
 * 
 * @author roopesh.s
 */
public enum PassengersDataPanel implements Serializable
{
   // CHECKSTYLE:OFF
   /** The PERSONAL_DETAILS. */
   PERSONAL_DETAILS("personaldetails", null), 
   
   /** The FORE_NAME. */
   FORE_NAME("foreName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The TITLE. */
   TITLE("title", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The MIDDLE_INITIAL. */
   MIDDLE_INITIAL("middleInitial", null),

   /** The SUR_NAME. */
   SUR_NAME("surName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),
   
   /** The AGE. */
   AGE("age", null),

   /** The CHILD_TITLE. */
   CHILD_TITLE("childTitle", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The CHILD_FORE_NAME. */
   CHILD_FORE_NAME("childForeName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The CHILD_LAST_NAME. */
   CHILD_LAST_NAME("childLastName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The CHILD_AGE. */
   CHILD_AGE("childAge", null),

   /** The INFANT_TITLE. */
   INFANT_TITLE("infantTitle", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The INFANT_FORE_NAME. */
   INFANT_FORE_NAME("infantForeName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The INFANT_LAST_NAME. */
   INFANT_LAST_NAME("infantLastName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The INFANT_AGE. */
   INFANT_AGE("infantAge", null);
   // CHECKSTYLE:ON
   
   /** The key is field name which is used in passenger section in payment page. */
   private String key;
   
   /** The pattern, used for validate field value. */
   private String pattern;
   
   /**
    * Constructor with key.
    * 
    * @param key the field name, which is used in passenger section in payment page.
    * @param pattern the pattern, which is used for validating field value.
    */
   PassengersDataPanel(String key, String pattern)
   {
      this.key = key;
      this.pattern = pattern;
   }
   
   /**
    * It will give key.
    * @return key
    */
   public String getKey()
   {
      return key;
   }
   
   /**
    * It will give pattern.
    * @return pattern
    */
   public String getPattern()
   {
      return pattern;
   }

   /**
    * This method is responsible for populating map with the details of the 
    * specified fields in the validation list.
    * 
    * @param validationList the list of brand specific non payment data to be 
    *       validated. 
    *
    * @return the patternMap, the patternMap with all passengers data panel values.
    */  
   public static Map<String, String> getAllPatterns(List<String> validationList)
   {
      Map<String, String> patternMap = new HashMap<String, String>();
      for (PassengersDataPanel passengersDataPanel : values())
      {
         if (validationList.contains(passengersDataPanel.key))
         {
            patternMap.put(passengersDataPanel.key, passengersDataPanel.pattern);
         }
      }
      return patternMap;
   }
}
