/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: CruiseSummary.java$
 * 
 * $Revision: $
 * 
 * $Date: Jul 16, 2008$
 * 
 * Author: Vijayalakshmi.d
 * 
 * 
 * $Log: $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * This class represents CruiseSummary object of Common Payment Component(CPC). This object includes
 * all related details of the Cruise which are necessary to display in CruisePanel in Payment Page
 * rendered by CPC.
 *
 * @author Vijayalakshmi.d
 *
 * @since 1.0 API
 */
public final class CruiseSummary implements Serializable
{
   /** The generated serial version id. */
   private static final long serialVersionUID = 6009048897295838117L;

   /** this object holds the details of ship. */
   private Accommodation ship;

   /** This object holds the description of the start port. */
   private String startPort;

   /** The start date of cruise. */
   private Date startDate;

   /** This object holds the description of the end port. */
   private String endPort;

   /** The end date of cruise. */
   private Date endDate;

   /** This object holds the details of the accommodation. */
   private Accommodation accommodation;

   /** The variable holds the duration of cruise and stay accommodation. */
   private int duration;

   /** URI for the a-z booking details Uri. */
   private String aToZUri;

   /** The cruise area. */
   private String cruiseArea;

   /** The stayDuration. */
   private int stayDuration;
   
   /** The start time of cruise. */
   private String startTime;

   /** The end time of cruise. */
   private String endTime;
   
   /** The additional details associated with cruise. This map contains information
    *  regarding the cruise image url to be used to display the cruise image
    *  in summary panel of ANC payment page.
    */
   private Map<String, String> additionalDetails;

   /**The place holder for excursion summary introduced as part of Shorex rebranding */
   private ExcursionSummary excursionSummary;
   
   /**
    * Constructor for CruiseSummary.
    *
    * @param ship object holding the details of the cruise.
    * @param duration the duration.
    */
   public CruiseSummary(Accommodation ship, int duration)

   {
      this.ship = ship;
      this.duration = duration;
   }

   /**
    * Constructor for CruiseSummary.
    *
    * @param ship object holding the details of the cruise.
    * @param duration the duration.
    * @param stayacommodation object holding the details of the stay accommodation for cruise and
    *           stay.
    */
   public CruiseSummary(Accommodation ship, int duration, Accommodation stayacommodation)
   {
      this.ship = ship;
      this.duration = duration;
      this.accommodation = stayacommodation;
   }

   /**
    * Constructor for CruiseSummary.
    *
    * @param ship object holding the details of the cruise.
    * @param duration the duration.
    * @param stayacommodation object holding the details of the stay accommodation for cruise and
    *           stay.
    * @param stayDuration is stayDuration for MCC.
    */
   public CruiseSummary(Accommodation ship, int duration, Accommodation stayacommodation,
                        int stayDuration)
   {

      this.ship = ship;
      this.duration = duration;
      this.accommodation = stayacommodation;
      this.stayDuration = stayDuration;

   }

   /**
    * Gets the start port of the cruise.
    * 
    * @return the startPort
    */

   public String getStartPort()
   {
      return startPort;
   }

   /**
    * Gets the start date.
    *
    * @return the start date
    */
   public Date getStartDate()
   {
      return new Date(startDate.getTime());
   }

   /**
    * Gets the end date.
    *
    * @return the end date
    */
   public Date getEndDate()
   {
      return new Date(endDate.getTime());
   }

   /**
    * Gets the end port of the cruise.
    *
    * @return the endPort
    */

   public String getEndPort()
   {
      return endPort;
   }

   /**
    * Gets the duration of cruise and stay accommodation.
    *
    * @return the duration.
    */
   public int getDuration()
   {
      return duration;
   }

   /**
    * gets the ship.
    *
    * @return the ship
    */

   public Accommodation getShip()
   {
      return ship;
   }

   /**
    * Gets the stay accommodation for cruise.
    *
    * @return the accommodation
    */

   public Accommodation getAccommodation()
   {
      return accommodation;
   }

   /**
    * Get the aToZUri for accommodation.
    *
    * @return the aToZUri.
    */
   public String getAToZUri()
   {
      return aToZUri;
   }

   /**
    * Gets the cruise area.
    *
    * @return the cruise area.
    */
   public String getCruiseArea()
   {
      return cruiseArea;
   }

   /**
    * This method will return the stayDuration.
    *
    * @return stayDuration
    */
   public int getStayDuration()
   {
      return stayDuration;
   }

   /**
    * Set the aToZUri for accommodation.
    *
    * @param aToZUri the aToZUri to set.
    */
   public void setAToZUri(String aToZUri)
   {
      this.aToZUri = aToZUri;
   }

   /**
    * Sets the start port.
    *
    * @param startPort the description of the port.
    */

   public void setStartPort(String startPort)
   {
      this.startPort = startPort;
   }

   /**
    * Sets the end port.
    *
    * @param endPort the endPort to set
    */

   public void setEndPort(String endPort)
   {
      this.endPort = endPort;
   }

   /**
    * Sets the end date.
    *
    * @param endDate the end date
    */
   public void setEndDate(Date endDate)
   {
      this.endDate = new Date(endDate.getTime());
   }

   /**
    * Sets the start date.
    *
    * @param startDate the start date
    */
   public void setStartDate(Date startDate)
   {
      this.startDate = new Date(startDate.getTime());
   }

   /**
    * Sets the cruise area.
    *
    * @param cruiseArea the cruise area.
    */
   public void setCruiseArea(String cruiseArea)
   {
      this.cruiseArea = cruiseArea;
   }
   
   /**
    * Gets the start time of cruise.
    *
    * @return the startTime.
    */
   public String getStartTime()
   {
      return startTime;
   }

   /**
    * Sets the start time of cruise.
    *
    * @param startTime the startTime.
    */
   public void setStartTime(String startTime)
   {
      this.startTime = startTime;
   }
   
   /**
    * Gets the end time of cruise.
    *
    * @return the endTime.
    */
   public String getEndTime()
   {
      return endTime;
   }

   /**
    * Sets the end time of cruise.
    *
    * @param endTime the endTime.
    */
   public void setEndTime(String endTime)
   {
      this.endTime = endTime;
   }
   
   /**
    * Gets the map of additional details for Cruise.
    *
    * @return additionalDetails the map of additionalDetails.
    */
   public Map<String, String> getAdditionalDetails()
   {
      return additionalDetails;
   }

   /**
    * Sets the additional details for Cruise.
    *
    * @param additionalDetails the additionalDetails to set
    */
   public void setAdditionalDetails(Map<String, String> additionalDetails)
   {
      this.additionalDetails = additionalDetails;
   }

/**
 * @return the excursionSummary
 */
public ExcursionSummary getExcursionSummary() {
	return excursionSummary;
}

/**
 * @param excursionSummary the excursionSummary to set
 */
public void setExcursionSummary(ExcursionSummary excursionSummary) {
	this.excursionSummary = excursionSummary;
}

}
