/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: SourceOfBusiness.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 07:41:25 $
 *
 * $author: thomas.pm$
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Map;

/**
 * This class is responsible for holding the source of business to be
 * displayed in Payment page rendered by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class SourceOfBusiness implements Serializable
{
   /** The generated serial version id.*/
   private static final long serialVersionUID = 1581404693571445453L;

   /**
    * This HashMap contains the source of business code as key and source
    * of business description as value.
    */
   private Map<String, String> sourceOfBusiness;

   /** The default source of business code. */
   private String selectedSobCode;

   /**
    * Constructor for SourceOfBusiness with source of business map
    * and selected source of business code as parameter.
    *
    * @param sourceOfBusiness the source of business map.
    * @param selectedSobCode the selected sob code.
    */
   public SourceOfBusiness(Map<String, String> sourceOfBusiness,
                           String selectedSobCode)
   {
      this.sourceOfBusiness = sourceOfBusiness;
      this.selectedSobCode = selectedSobCode;
   }

   /**
    * Constructor for SourceOfBusiness with all attributes except
    * selectedSobCode.
    *
    * @param sourceOfBusiness the source of business.
    */
   public SourceOfBusiness(Map<String, String> sourceOfBusiness)
   {
      this.sourceOfBusiness = sourceOfBusiness;
   }

   /**
    * Get the source of business map.
    *
    * @return the sourceOfBusiness.
    */
   public Map<String, String> getSourceOfBusiness()
   {
      return sourceOfBusiness;
   }

   /**
    * Get the selected source of business code.
    *
    * @return the selectedSobCode
    */
   public String getSelectedSobCode()
   {
      return selectedSobCode;
   }
}
