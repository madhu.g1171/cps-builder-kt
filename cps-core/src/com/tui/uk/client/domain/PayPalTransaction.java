package com.tui.uk.client.domain;

import java.io.Serializable;

public class PayPalTransaction implements Serializable{

	/** The generated serial version id. */
	private static final long serialVersionUID = -3473835819078423672L;

	private String transactionDate ;
	private String status;
	private String reason;
	private String merchantReference;
	private String datacashReference;
	private String queryMethod;
	private String paymentStatus;
	private String pendingReason;
	private String amount;
	/**
	 * @return the transactionDate
	 */
	public String getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the merchantReference
	 */
	public String getMerchantReference() {
		return merchantReference;
	}

	/**
	 * @param merchantReference the merchantReference to set
	 */
	public void setMerchantReference(String merchantReference) {
		this.merchantReference = merchantReference;
	}

	/**
	 * @return the datacashReference
	 */
	public String getDatacashReference() {
		return datacashReference;
	}

	/**
	 * @param datacashReference the datacashReference to set
	 */
	public void setDatacashReference(String datacashReference) {
		this.datacashReference = datacashReference;
	}

	/**
	 * @return the queryMethod
	 */
	public String getQueryMethod() {
		return queryMethod;
	}

	/**
	 * @param queryMethod the queryMethod to set
	 */
	public void setQueryMethod(String queryMethod) {
		this.queryMethod = queryMethod;
	}

	/**
	 * @return the paymentStatus
	 */
	public String getPaymentStatus() {
		return paymentStatus;
	}

	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	/**
	 * @return the pendingReason
	 */
	public String getPendingReason() {
		return pendingReason;
	}

	/**
	 * @param pendingReason the pendingReason to set
	 */
	public void setPendingReason(String pendingReason) {
		this.pendingReason = pendingReason;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}


}
