package com.tui.uk.client.domain;

import java.io.Serializable;

public class ShippingAddress implements Cloneable, Serializable {

	private static final long serialVersionUID = -7L;

	private String amount;
	private String city;
	private String countryCode;
	private String name;
	private String postcode;
	private String region;
	private String streetAddessOne;
	private String streetAddessTwo;
	private String telephone_number;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStreetAddessOne() {
		return streetAddessOne;
	}

	public void setStreetAddessOne(String streetAddessOne) {
		this.streetAddessOne = streetAddessOne;
	}

	public String getStreetAddessTwo() {
		return streetAddessTwo;
	}

	public void setStreetAddessTwo(String streetAddessTwo) {
		this.streetAddessTwo = streetAddessTwo;
	}

	public String getTelephone_number() {
		return telephone_number;
	}

	public void setTelephone_number(String telephone_number) {
		this.telephone_number = telephone_number;
	}

}
