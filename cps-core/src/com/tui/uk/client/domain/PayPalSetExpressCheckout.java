package com.tui.uk.client.domain;

import java.io.Serializable;

public class PayPalSetExpressCheckout implements Cloneable, Serializable {

	private static final long serialVersionUID = -11L;

	private String cancelUrl;
	private String returnUrl;
	private String transactionMethod;
	private String billingType;
	private String brandType;
	private String method;

	private String cartBorderColor;
	private String channelType;
	private String HeaderStyle;
	private String invnum;
	private String localeCode;

	private String logoImage;
	private String amount;
	private String shippingAddressRequired;
	private String paymentAction;
	private String paymentType;

	private String billingAddressRequired;
	private String billingAgreementDescription;
	private String billing_agreement_custom;
	private String merchantReference;
	private BillingAddress billingAddress;
	private ShippingAddress shippingAddress;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getCancelUrl() {
		return cancelUrl;
	}

	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getTransactionMethod() {
		return transactionMethod;
	}

	public void setTransactionMethod(String transactionMethod) {
		this.transactionMethod = transactionMethod;
	}

	public String getBillingType() {
		return billingType;
	}

	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}

	public String getBrandType() {
		return brandType;
	}

	public void setBrandType(String brandType) {
		this.brandType = brandType;
	}

	public String getCartBorderColor() {
		return cartBorderColor;
	}

	public void setCartBorderColor(String cartBorderColor) {
		this.cartBorderColor = cartBorderColor;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getHeaderStyle() {
		return HeaderStyle;
	}

	public void setHeaderStyle(String headerStyle) {
		HeaderStyle = headerStyle;
	}

	public String getInvnum() {
		return invnum;
	}

	public void setInvnum(String invnum) {
		this.invnum = invnum;
	}

	public String getLocaleCode() {
		return localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getShippingAddressRequired() {
		return shippingAddressRequired;
	}

	public void setShippingAddressRequired(String shippingAddressRequired) {
		this.shippingAddressRequired = shippingAddressRequired;
	}

	public String getPaymentAction() {
		return paymentAction;
	}

	public void setPaymentAction(String paymentAction) {
		this.paymentAction = paymentAction;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getBillingAddressRequired() {
		return billingAddressRequired;
	}

	public void setBillingAddressRequired(String billingAddressRequired) {
		this.billingAddressRequired = billingAddressRequired;
	}

	public String getBillingAgreementDescription() {
		return billingAgreementDescription;
	}

	public void setBillingAgreementDescription(
			String billingAgreementDescription) {
		this.billingAgreementDescription = billingAgreementDescription;
	}

	public String getBilling_agreement_custom() {
		return billing_agreement_custom;
	}

	public void setBilling_agreement_custom(String billing_agreement_custom) {
		this.billing_agreement_custom = billing_agreement_custom;
	}

	public String getMerchantReference() {
		return merchantReference;
	}

	public void setMerchantReference(String merchantReference) {
		this.merchantReference = merchantReference;
	}

	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

}
