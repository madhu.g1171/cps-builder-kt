package com.tui.uk.client.domain;

/**
 * The class BookingConstants.This contains all the constants used in both CPS and the client
 * application.
 * 
 * @author sindhushree.g
 * 
 * @since 1.0 API
 */
public final class BookingConstants
{
   /**
    * Private constructor to stop instantiation of this class.
    */
   private BookingConstants()
   {
      // Do nothing.
   }

   /** Datacash failure code. */
   public static final int DATACASH_FAILURE_CODE = 7;

   /** The Constant to check if datacash transaction is selected or not. */
   public static final String IS_DATACASH_SELECTED = "isDataCash";

   /** The constant for HTTP_PROTOCOL. */
   public static final String HTTP_PROTOCOL = "http://";

   /** The constant for PARTIAL_PAYMENT_AMOUNT. */
   public static final String PARTIAL_AMOUNT = "partialPaymentAmount";

   /** The constant for SELECTED_DEPOSIT_TYPE. */
   public static final String SELECTED_DEPOSIT_TYPE = "selectedDepositType";

   /** The constant for DEPOSIT_TYPE. */
   public static final String DEPOSIT_TYPE = "depositType";

   /** The constant for DEPOSIT_TYPE. */
   public static final String LOW_DEPOSIT = "lowDeposit";

   /** The constant for PARTIAL_DEPOSIT. */
   public static final String PARTIAL_DEPOSIT = "partialDeposit";

   /** The constant for MINIMUM_AMOUNT. */
   public static final String MINIMUM_AMOUNT = "MinimumAmount";

   /** The constant for DEPOSIT. */
   public static final String DEPOSIT = "deposit";

   /** The constant for FULL_COST. */
   public static final String FULL_COST = "fullCost";
   
   /** The constant for DD_DEPOSIT_TYPE. */
   public static final String DD_DEPOSIT_TYPE = "lowDepositPlusDD";

   /** The constant for SELECTED_DEPOSIT_TYPE. */
   public static final String SELECTED_DCR = "datacashReference";

   /** The https protocol. */
   public static final String HTTPS_PROTOCOL = "https://";

   /** The constant for SUMMARYPANEL_DEPOSIT_AMOUNT. */
   public static final String SUMMARYPANEL_DEPOSIT_AMOUNT = "summaryPanelDepositAmount";

   /** The constant for INVENTORY_BOOKING_REFERENCE. */
   public static final String INVENTORY_BOOKING_REFERENCE = "inventory_booking_ref";

   /** The constant for ACCOM_DETAILS_URL. */
   public static final String ACCOM_DETAILS_URL = "accomDetailsUrl";

   /** The constant for FLIGHT_DETAILS_URL. */
   public static final String FLIGHT_DETAILS_URL = "flightDetailsUrl";

   /** The constant for transactionAmount. */
   public static final String TRANSACTION_AMOUNT = "transactionAmount";

   /** The constant for priceComponents. */
   public static final String PRICE_COMPONENTS = "priceComponents";

   /** The constant for price break down. */
   public static final String PRICE_BREAKDOWN = "priceBreakDown";

   /** The street address one.Will hold house no/name. */
   public static final String STREET_ADDRESS1 = "street_address1";

   /** The street address two.Will hold remaining address details. */
   public static final String STREET_ADDRESS2 = "street_address2";

   /** The street address three.Will hold town/city. */
   public static final String STREET_ADDRESS3 = "street_address3";

   /** The street address four.Will hold county. */
   public static final String STREET_ADDRESS4 = "street_address4";

   /** The constant for country. */
   public static final String SELECTED_COUNTRY = "selectedCountry";

   /** The constant for country code. */
   public static final String SELECTED_COUNTRY_CODE = "selectedCountryCode";

   /** The constant for card billing post code. */
   public static final String CARD_BILLING_POSTCODE = "cardBillingPostcode";

   /** The constant used for pan masking. */
   public static final String MASK_CHARACTER = "maskCharacter";

   /** The constant used for capture method. */
   public static final String CNP = "cnp";

   /** The constant used for capture method. */
   public static final String ECOMM = "ecomm";

   /** The additional address line 1 will hold additional address details. */
   public static final String ADDITIONAL_ADDRESS_LINE1 = "additional_address_line1";

   /** The additional address line 2 will hold additional address details. */
   public static final String ADDITIONAL_ADDRESS_LINE2 = "additional_address_line2";

   /** The constant for sessionId. */
   public static final String JSESSIONID = "jsessionId";

   /** Two year period of the Cookie created. */
   public static final int COOKIE_TWO_YEARS = 63072000;

   /** The constant for cookie name. */
   public static final String COOKIE_NAME = "consentcookie";

   /** The constant for nonjs link. */
   public static final String COOKIE_NONJS_LINK = "nonjslink";

   /** The constant for per person price component. */
   public static final String PER_PERSON_PRICE = "PER_PERSON_PRICE";

   /** The constant for flight extras price component. */
   public static final String FLIGHT_EXTRAS_PRICE_COMPONENT = "FLIGHT_EXTRAS_PRICE_COMPONENT";

   /** The constant for accommodation extras price component. */
   public static final String ACCOM_EXTRAS_PRICE_COMPONENT = "ACCOM_EXTRAS_PRICE_COMPONENT";

   /** The constant for flight extras price component. */
   public static final String HOLIDAY_EXTRAS_PRICE_COMPONENT = "HOLIDAY_EXTRAS_PRICE_COMPONENT";

   /** The constant for payment history. */
   public static final String PAYMENT_HISTORY_PRICE_COMPONENT = "PAYMENT_HISTORY_PRICE_COMPONENT";
   
   /** The constant for due date. */
   public static final String DUE_DATE = "DUE_DATE";
   /** The constant for Payment failure URL. */
   public static final String PAYMENT_FAILURE_URL = "failure_payment_url";

   /** The constant for cruise extras price component. */
   public static final String CRUISE_EXTRAS_PRICE_COMPONENT = "CRUISE_EXTRAS_PRICE_COMPONENT";
   
   /** The constant for Accommodation Image URL. */
   public static final String ACCOMMODATION_IMAGE_URL = "ACCOMMODATION_IMAGE_URL";
   
   /** The constant for Accommodation Extras price component.*/
   public static final String ACCOM_EXTRAS_PRICE_COMPONENT_0="ACCOM_EXTRAS_PRICE_COMPONENT_0";

   /** The constant for Accommodation Extras price component.*/
   public static final String ACCOM_EXTRAS_PRICE_COMPONENT_1="ACCOM_EXTRAS_PRICE_COMPONENT_1";

   /** The constant for Accommodation Extras price component.*/
   public static final String ACCOM_EXTRAS_PRICE_COMPONENT_2="ACCOM_EXTRAS_PRICE_COMPONENT_2";

   
}