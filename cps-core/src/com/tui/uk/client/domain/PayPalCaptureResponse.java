package com.tui.uk.client.domain;

import java.io.Serializable;

public class PayPalCaptureResponse implements Cloneable, Serializable {

	private String acknowledgement;
	private String amount;
	private String build;
	private String currencyExchangeRate;
	private String TransactionCharge;
	private String messageSubmissionId;
	private String orderTime;
	private String paymentStatus;

	private String paymentType;
	private String settledAmount;
	private String taxCharged;
	private String timestamp;
	private String transactionId;
	private String transactionType;
	private String PayPalVersion;
	private String authorizationId;

	private String parentTransactionId;
	private String receiptNumber;
	private String datacashReferance;
	private String status;
	private String reason;
	private ShippingAddress shippingAddress;

	public String getAcknowledgement() {
		return acknowledgement;
	}
	public void setAcknowledgement(String acknowledgement) {
		this.acknowledgement = acknowledgement;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBuild() {
		return build;
	}
	public void setBuild(String build) {
		this.build = build;
	}
	public String getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}
	public void setCurrencyExchangeRate(String currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}
	public String getTransactionCharge() {
		return TransactionCharge;
	}
	public void setTransactionCharge(String transactionCharge) {
		TransactionCharge = transactionCharge;
	}
	public String getMessageSubmissionId() {
		return messageSubmissionId;
	}
	public void setMessageSubmissionId(String messageSubmissionId) {
		this.messageSubmissionId = messageSubmissionId;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getSettledAmount() {
		return settledAmount;
	}
	public void setSettledAmount(String settledAmount) {
		this.settledAmount = settledAmount;
	}
	public String getTaxCharged() {
		return taxCharged;
	}
	public void setTaxCharged(String taxCharged) {
		this.taxCharged = taxCharged;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getPayPalVersion() {
		return PayPalVersion;
	}
	public void setPayPalVersion(String payPalVersion) {
		PayPalVersion = payPalVersion;
	}
	public String getAuthorizationId() {
		return authorizationId;
	}
	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}
	public String getParentTransactionId() {
		return parentTransactionId;
	}
	public void setParentTransactionId(String parentTransactionId) {
		this.parentTransactionId = parentTransactionId;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getDatacashReferance() {
		return datacashReferance;
	}
	public void setDatacashReferance(String datacashReferance) {
		this.datacashReferance = datacashReferance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

}
