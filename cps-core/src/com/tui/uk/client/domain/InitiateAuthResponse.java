package com.tui.uk.client.domain;

import java.util.Objects;

import com.datacash.util.XMLDocument;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;

/**
 * The class is used for mapping Initiate Authentication Response.
 * 
 * @author raghunandan.d
 *
 */
public class InitiateAuthResponse {
	private String redirectHtml;
	private String status;
	private String datacashReference;

	public InitiateAuthResponse(XMLDocument response) {
		if (Objects.nonNull(response)) {
			setDatacashReference(response
					.get(DataCashServiceConstants.EMV_DATACASH_REFERENCE));
			setRedirectHtml(response
					.get(DataCashServiceConstants.EMV_REDIRECT_HTML));
			setStatus(response.get(DataCashServiceConstants.STATUS));
		}
	}

	public String getRedirectHtml() {
		return redirectHtml;
	}

	public void setRedirectHtml(String redirectHtml) {
		this.redirectHtml = redirectHtml;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDatacashReference() {
		return datacashReference;
	}

	public void setDatacashReference(String datacashReference) {
		this.datacashReference = datacashReference;
	}

}
