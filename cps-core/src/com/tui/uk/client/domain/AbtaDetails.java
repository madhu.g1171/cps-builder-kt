/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AbtaDetails.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 07:37:45 $
 *
 * $author: thomas.pm $
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This represents AbtaDetails object of Common Payment Component(CPC).
 * This object includes all Abta related details  which are
 * necessary to query promotional discount from TRACS in Payment Page rendered by CPC.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class AbtaDetails implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = -5175140899413257511L;

   /** The abta number. */
   private String abtaNumber;

   /** The abta user id. */
   private String abtaUserId;

   /** The tracs website id. */
   private String tracsWebSiteId;

   /**
    * Constructor for AbtaDetails.
    *
    * @param abtaNumber the abta number
    * @param abtaUserId the abta user id.
    * @param tracsWebSiteId the tracs website id.
    */
   public AbtaDetails(String abtaNumber, String abtaUserId, String tracsWebSiteId)
   {
      this.abtaNumber = abtaNumber;
      this.abtaUserId = abtaUserId;
      this.tracsWebSiteId = tracsWebSiteId;
   }

   /**
    * Get the abta number.
    *
    * @return the abtaNumber
    */
   public String getAbtaNumber()
   {
      return abtaNumber;
   }

   /**
    * Get the abta user id.
    *
    * @return the abtaUserId
    */
   public String getAbtaUserId()
   {
      return abtaUserId;
   }

   /**
    * Get the tracs website id.
    *
    * @return the tracsWebSiteId
    */
   public String getTracsWebSiteId()
   {
      return tracsWebSiteId;
   }

}
