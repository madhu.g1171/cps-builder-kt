/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: FlightSummary.java$
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $author: thomas.pm$
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.List;

/**
 * This class is responsible for holding all in bound and out bound flight details
 * to be displayed in Payment page rendered by Common Payment Component.
 * It also holds other common properties of in bound and out bound flights.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class FlightSummary implements Serializable
{
   /** The generated serial version id. */
   private static final long serialVersionUID = -367873138400211930L;

   /** This object contains all the details of the out bound flight. */
   private List<FlightDetails> outboundFlight;

   /** This object contains all the details of the in bound flight. */
   private List<FlightDetails> inboundFlight;

   /** The flight supplier system. */
   private String flightSupplierSystem;

   /** Is flight selected. */
   private Boolean flightSelected;

   /** The list of flight extras. */
   private List<String> flightExtras;

   /** The ticket type - Round Trip or One Way. */
   private String ticketType;
   
   /** The Type Of Flight. */
  private Boolean isThirdPartyFlight;
  
  /** Re positional Cruise. */
  private Boolean repositionalCruise;

	/**
	 * This is for FO duration, for all FO packages duration, following
	 * attribute can be used.
	 */
	private int duration;

    

   /**
    * Constructor for FlightSummary.
    *
    * @param outboundFlight details of the out bound flight.
    * @param inboundFlight details of the in bound flight.
    * @param flightSupplierSystem the flight supplier system.
    * @param flightSelected boolean.
    */
   public FlightSummary(List<FlightDetails> outboundFlight, List<FlightDetails> inboundFlight,
                           String flightSupplierSystem, Boolean flightSelected)
   {
      this.outboundFlight = outboundFlight;
      this.inboundFlight = inboundFlight;
      this.flightSupplierSystem = flightSupplierSystem;
      this.flightSelected = flightSelected;
   }

   /**
    * Sets the flight extras.
    *
    * @param flightExtras the flightExtras to set
    */
   public void setFlightExtras(List<String> flightExtras)
   {
      this.flightExtras = flightExtras;
   }

   /**
    * Sets the ticket type.
    *
    * @param ticketType the ticket type.
    */
   public void setTicketType(String ticketType)
   {
      this.ticketType = ticketType;
   }

   /**
    * Get the out bound flight details.
    *
    * @return the outboundFlight.
    */
   public List<FlightDetails> getOutboundFlight()
   {
      return outboundFlight;
   }

   /**
    * Get the in bound flight details.
    *
    * @return the inboundFlight.
    */
   public List<FlightDetails> getInboundFlight()
   {
      return inboundFlight;
   }

   /**
    * Get the flight supplier system.
    *
    * @return the flightSupplierSystem.
    */
   public String getFlightSupplierSystem()
   {
      return flightSupplierSystem;
   }

   /**
    * Get the flightSelected.
    *
    * @return the flightSelected
    */
   public Boolean getFlightSelected()
   {
      return flightSelected;
   }

   /**
    * Gets the flight extras.
    *
    * @return the flightExtras
    */
   public List<String> getFlightExtras()
   {
      return flightExtras;
   }

   /**
    * Gets the ticket type.
    *
    * @return the ticket type.
    */
   public String getTicketType()
   {
      return ticketType;
   }
   
   /**
    * Sets the Third Party Flight.
    *
    * @param isThirdPartyFlight the isThirdPartyFlight to set
    */
   public void setIsThirdPartyFlight(Boolean isThirdPartyFlight)
   {
      this.isThirdPartyFlight = isThirdPartyFlight;
   }
   
   /**
    * Gets the type of flight.
    *
    * @return the type of flight.
    */
   public Boolean getIsThirdPartyFlight()
   {
      return isThirdPartyFlight;
   }

   /**
    * Gets the type of cruise.
    *
    * @return the type of cruise.
    */
   public Boolean getRepositionalCruise()
   {
      return repositionalCruise;
   }

   /**
    * Sets the Re positional Cruise.
    *
    * @param repositionalCruise the repositionalCruise to set
    */
   public void setRepositionalCruise(Boolean repositionalCruise)
   {
      this.repositionalCruise = repositionalCruise;
   }

   /**
    * gets the duration for FO packages.
    *
    */
   public int getDuration() {
 		return duration;
 	}
   
   /**
    * Sets the duration for FO packages.
    *
    * @param duration  to set value in duration field.
    */
 	public void setDuration(int duration) {
 		this.duration = duration;
 	}
   
   
   
}