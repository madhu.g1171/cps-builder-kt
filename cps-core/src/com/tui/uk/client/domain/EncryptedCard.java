/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: EncryptedCard.java,v $
 *
 * $Revision:  $
 *
 * $Date: $
 *
 * $author: thomas.pm $
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class is responsible for holding the encrypted card details
 * entered by the user in the payment page.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class EncryptedCard implements Serializable
{
    /** The generated serial version id. */
    private static final long serialVersionUID = -3473835819078423668L;

    /** The encrypted pan number of card. */
    private String encryptedPan;

    /** The name on card. */
    private String nameOnCard;

    /** The expiry date of card. */
    private String expiryDate;

    /** The post code of card. */
    private String postCode;

    /** The card type. */
    private String cardType;

    /** The start date of card. */
    private String startDate;

    /** The issue number of card. */
    private String issueNumber;

    /**
     * The Constructor to create this object.
     *
     * @param encryptedPan the encrypted pan.
     * @param nameOnCard the name on card.
     * @param expiryDate the expiry date.
     * @param postCode the postCode.
     * @param cardType the cardtype.
     */
    public EncryptedCard(String encryptedPan, String nameOnCard, String expiryDate,
                 String postCode, String cardType)
    {
       this.encryptedPan = encryptedPan;
       this.expiryDate = expiryDate;
       this.nameOnCard = nameOnCard;
       this.postCode = postCode;
       this.cardType = cardType;
    }

    /**
     * Get the encrypted pan.
     *
     * @return the encryptedPan.
     */
    public String getEncryptedPan()
    {
        return encryptedPan;
    }

    /**
     * Get the name on card.
     *
     * @return the nameOnCard.
     */
    public String getNameOnCard()
    {
        return nameOnCard;
    }

    /**
     * Get the expiry date.
     *
     * @return the expiryDate.
     */
    public String getExpiryDate()
    {
        return expiryDate;
    }

    /**
     * Get the post code.
     *
     * @return the postCode.
     */
    public String getPostCode()
    {
        return postCode;
    }
   /**
    * Gets the card type.
    *
    * @return the cardType
    */
   public String getCardType()
   {
      return cardType;
   }

   /**
    * Get the start date.
    *
    * @return the startDate.
    */
   public String getStartDate()
   {
       return startDate;
   }

   /**
    * Get the issue number.
    *
    * @return the issueNumber.
    */
   public String getIssueNumber()
   {
       return issueNumber;
   }

   /**
    * Set the start date.
    *
    * @param startDate the startDate to set.
    */
   public void setStartDate(String startDate)
   {
       this.startDate = startDate;
   }

   /**
    * Set the issue number.
    *
    * @param issueNumber the issueNumber to set.
    */
   public void setIssueNumber(String issueNumber)
   {
       this.issueNumber = issueNumber;
   }

}
