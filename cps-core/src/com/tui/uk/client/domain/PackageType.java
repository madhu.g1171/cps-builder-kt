/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PackageType.java,v$
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: thomas.pm$
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

/**
 * Represents all the possible known package types or components which can be booked.
 * <p>
 * PDP - Represents pre defined packages, package holidays. CP - Represents
 * component packages. These are formed out of components from TRACS
 * inventory system. DP - Represents dynamic packages. These are formed by
 * combining TRACS component with other third party components.
 * AO - Accommodation Only Component. Though this is not a package type, its a
 * component which can be booked. Similarly FO - Flight Only component, FH - Flight and hotel
 * component.
 * </p>
 *
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public enum PackageType
{
   /** Pre Defined Package type. */
   PreDefinedPackage("PDP"),

   /** Component Package. */
   ComponentPackage("CPA"),

   /** COWS Component Package. */
   COWSComponentPackage("CPO"),

   /** Dynamic Package. */
   DynamicPackage("DP"),

   /**
    * Accommodation Only Component.
    * <p>
    * Ideally this should not be here as package type, but it is required
    * when a de-selection feature is put on action.
    * </p>
    */
   AccommodationOnly("AO"),

   /**
    * Flight Only Component.
    * <p>
    * Ideally this should not be here as package type, but it is required
    * when a de-selection feature is put on action.
    * </p>
    */
   FlightOnly("FO"),

   /** Flight and Hotel package. */
   FlightAndHotel("FH");

   /** The unique code for the package type. */
   private final String code;

   /**
    * Construct the enumeration item.
    *
    * @param packageTypeCode the unique code for the package type
    */
   private PackageType(String packageTypeCode)
   {
      code = packageTypeCode;
   }

   /**
    * Retrieve the unique code for the package type.
    *
    * @return the unique code
    */
   public String getCode()
   {
      return code;
   }

   /**
    * Determine the package type for a specified code.
    *
    * @param requiredCode the code of the required package type.
    *
    * @return the package type found
    *
    * @throws IllegalArgumentException if the code does not relate to a
    *            known package type
    */
   public static PackageType findByCode(String requiredCode)
   {
      for (PackageType packageType : values())
      {
         if (packageType.getCode().equals(requiredCode))
         {
            return packageType;
         }
      }
      throw new IllegalArgumentException("Unknown Package Type code:" + requiredCode);
   }
}
