package com.tui.uk.client.domain;

public enum DDVariant {
    V0 ("V0","Default"),
    V1("V1","OldVariant"),
    V2("V2","NewVariant");
    private String ddVariantName;
    private String code;


    public String getCode() {
        return code;
    }


    public String getDdVariantName() {
        return ddVariantName;
    }


    private DDVariant(String code,String ddVariantName) {
        this.code = code;
        this.ddVariantName = ddVariantName;
    }


    public static DDVariant findByCode(String requiredVariantCode)
    {
       for (DDVariant ddVariantName : values())
       {
          if (ddVariantName.getCode().equals(requiredVariantCode))
          {
             return ddVariantName;
          }
       }
       throw new IllegalArgumentException("Unknown DD Variant Name:" + requiredVariantCode);
    }



}
