package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;

import com.tui.uk.client.domain.Money;

public class PaymentScheduleData implements Serializable{

	private Money amount;

	private String paymentScheduleId;

	private String payCount;

	private Date firstInstallmentDate;



	public Date getFirstInstallmentDate() {
		return firstInstallmentDate;
	}

	public void setFirstInstallmentDate(Date firstInstallmentDate) {
		this.firstInstallmentDate = firstInstallmentDate;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}

	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}

	public String getPayCount() {
		return payCount;
	}

	public void setPayCount(String payCount) {
		this.payCount = payCount;
	}






}
