package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class holds the response received after authorizing a paypal transaction
 * @author tanveer.bp
 *
 */
public class PayPalAuthorizeResponse implements Serializable
{
	/** The generated serial version id. */
	private static final long serialVersionUID = -3473835819078423668L;

	private String acknowledgement;
	private String build;
	private String messageSubmissionId;
	private String timestamp;
	private String transactionId;
	private String merchantReferance;
	private String PayPalVersion;
	private String datacashReferance;
	private String status;
	private String reason;
	private ShippingAddress shippingAddress;

	/**
	 * @return the acknowledgement
	 */
	public String getAcknowledgement() {
		return acknowledgement;
	}
	/**
	 * @param acknowledgement the acknowledgement to set
	 */
	public void setAcknowledgement(String acknowledgement) {
		this.acknowledgement = acknowledgement;
	}
	/**
	 * @return the build
	 */
	public String getBuild() {
		return build;
	}
	/**
	 * @param build the build to set
	 */
	public void setBuild(String build) {
		this.build = build;
	}
	/**
	 * @return the messageSubmissionId
	 */
	public String getMessageSubmissionId() {
		return messageSubmissionId;
	}
	/**
	 * @param messageSubmissionId the messageSubmissionId to set
	 */
	public void setMessageSubmissionId(String messageSubmissionId) {
		this.messageSubmissionId = messageSubmissionId;
	}
	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the merchantReferance
	 */
	public String getMerchantReferance() {
		return merchantReferance;
	}
	/**
	 * @param merchantReferance the merchantReferance to set
	 */
	public void setMerchantReferance(String merchantReferance) {
		this.merchantReferance = merchantReferance;
	}
	/**
	 * @return the payPalVersion
	 */
	public String getPayPalVersion() {
		return PayPalVersion;
	}
	/**
	 * @param payPalVersion the payPalVersion to set
	 */
	public void setPayPalVersion(String payPalVersion) {
		PayPalVersion = payPalVersion;
	}
	/**
	 * @return the datacashReferance
	 */
	public String getDatacashReferance() {
		return datacashReferance;
	}
	/**
	 * @param datacashReferance the datacashReferance to set
	 */
	public void setDatacashReferance(String datacashReferance) {
		this.datacashReferance = datacashReferance;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	/**
	 * @return the shippingAddress
	 */
	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}
	/**
	 * @param shippingAddress the shippingAddress to set
	 */
	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
