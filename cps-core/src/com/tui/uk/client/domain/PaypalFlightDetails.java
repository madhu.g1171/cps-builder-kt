package com.tui.uk.client.domain;

import java.io.Serializable;

public class PaypalFlightDetails implements Cloneable, Serializable {

	private String arrivalAirportCode;
	private String departureAirportCode;
	private String carrierCode;
	private String departureTime;
	private String fareBasisCode;
	private String flightNumber;
	private String serviceClass;
	private String stopoverCode;
	private String travelDate;
	private String conjunctionTicket;
	private String couponNumber;
	private String endorsementRestrictions;
	private String exchangeTicket;
	private String fare;
	private String fee;
	private String taxes;


	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getFareBasisCode() {
		return fareBasisCode;
	}
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getServiceClass() {
		return serviceClass;
	}
	public void setServiceClass(String serviceClass) {
		this.serviceClass = serviceClass;
	}
	public String getStopoverCode() {
		return stopoverCode;
	}
	public void setStopoverCode(String stopoverCode) {
		this.stopoverCode = stopoverCode;
	}
	public String getTravelDate() {
		return travelDate;
	}
	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}
	public String getConjunctionTicket() {
		return conjunctionTicket;
	}
	public void setConjunctionTicket(String conjunctionTicket) {
		this.conjunctionTicket = conjunctionTicket;
	}
	public String getCouponNumber() {
		return couponNumber;
	}
	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}
	public String getEndorsementRestrictions() {
		return endorsementRestrictions;
	}
	public void setEndorsementRestrictions(String endorsementRestrictions) {
		this.endorsementRestrictions = endorsementRestrictions;
	}
	public String getExchangeTicket() {
		return exchangeTicket;
	}
	public void setExchangeTicket(String exchangeTicket) {
		this.exchangeTicket = exchangeTicket;
	}
	public String getFare() {
		return fare;
	}
	public void setFare(String fare) {
		this.fare = fare;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getTaxes() {
		return taxes;
	}
	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

}
