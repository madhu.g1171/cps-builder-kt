/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PaymentMethods.java$
 *
 * $Revision: $
 *
 * $Date: Jun 11, 2008$
 *
 * Author: veena.bn
 *
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

/**
 * This defines different PaymentMethod which includes DATACASH, PDQ, PIN_PAD,
 * CASH, CHEQUE, VOUCHER and POST_PAYMENT.
 * DATACASH will include all card transactions to be processed by DataCash.
 * PDQ will include all card transactions taken by PDQ.
 * CASH includes cash transaction.
 * VOUCHER includes different voucher type transactions.
 * CHEQUE includes cheque transaction.
 * POST_PAYMENT includes post payment guarantee card transactions.
 *
 * @author veena.bn
 *
 * @since 1.0 API
 */
public enum PaymentMethod
{

   /** The DATACASH. */
   DATACASH("Dcard"),

   /** The PDQ. */
   PDQ("Card"),

   /** The PIN_PAD. */
   PIN_PAD("CardPinPad"),

   /** The CASH. */
   CASH("Cash"),

   /** The CHEQUE. */
   CHEQUE("Cheque"),

   /** The VOUCHER. */
   VOUCHER("Voucher"),

   /** The POST_PAYMENT. */
   POST_PAYMENT("Postpayment"),

   /**NO_PAYMENT.*/
  NO_PAYMENT("NoPayment"),

   /** Refund Card .*/
   REFUND_CARD("CardRefund"),

   /** Gift Card. */
   GIFT_CARD("GiftCard");

   /** The Payment code. */
   private final String code;

   /**
    * The Constructor.
    *
    * @param code the Payment code
    */
   PaymentMethod(String code)
   {
      this.code = code;
   }

   /**
    * Gets the code.
    *
    * @return the code
    */
   public String getCode()
   {
      return code;
   }

   /**
    * This method will return the payment type based on the code passed. It will throw
    * IllegalArgumentException if the code is not valid.
    *
    * @param code the Payment code
    *
    * @return the payment type
    */
   public static PaymentMethod valueFromCode(String code)
   {
      for (PaymentMethod type : PaymentMethod.values())
      {
         if (type.code.equalsIgnoreCase(code.trim()))
         {
            return type;
         }
      }
      throw new IllegalArgumentException("Unknown Payment method:" + code);
   }
}
