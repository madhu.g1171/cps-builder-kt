/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ImportantInformation.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 07:51:25 $
 *
 * $author: thomas.pm$
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * This class is responsible for holding the important information to be
 * displayed in Payment page rendered by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class ImportantInformation implements Serializable
{
  /**
   * The generated serial version id.
   */
   private static final long serialVersionUID = 6379534849618450393L;

   /** The errata for important information. */
   private List<String> errata;

   /**
    * It holds all the necessary important information if an accommodation is postpay.
    */
   private HashMap<String, String> description = new HashMap<String, String>();

   /**
    * Constructor for ImportantInformation with list of errata.
    *
    * @param errata the errata.
    */
   public ImportantInformation(List<String> errata)
   {
      this.errata = errata;
   }

   /**
    * Get the errata for important information.
    *
    * @return errata the errata
    */
   public List<String> getErrata()
   {
      return errata;
   }

   /**
    * Gets the description.
    *
    * @return the description
    */
   public HashMap<String, String> getDescription()
   {
      return description;
   }

   /**
    * Sets the description.
    *
    * @param description the description
    */
   public void setDescription(HashMap<String, String> description)
   {
      this.description = description;
   }
}
