/*
* Copyright (C)2006 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   BookingFlow.java$
*
* $Revision:   $
*
* $Date:   Dec 9, 2009$
*
* Author: veena.bn
*
*
* $Log:   $
*/
package com.tui.uk.client.domain;

/**
 * Enumeration for bookingFlow. This enumeration is used to distinguish the
 * booking flow name which is used to determine the flow based on the name.
 * @author veena.bn
 *
 */
public enum BookingFlow
{
   /** OLBP Booking flow. */
   OLBP("OLBP"),

   /** Change/Amend Booking flow. */
   AMEND("AMEND"),

   /** Cancel Booking flow. */
   CANCEL("CANCEL");

   /** The booking flow. */
   private String bookingFlow;

   /**
    * Constructor for Booking flow.
    * @param bookingFlow the booking flow.
    **/
   BookingFlow(String bookingFlow)
   {
      this.bookingFlow = bookingFlow;
   }

   /**
    * Retrieve the booking flow.
    * @return bookingFlow the bookingFlow name.
    */
   public String getBookingFlow()
   {
      return bookingFlow;
   }

   /**
    * Determine the booking flow name.
    *
    * @param bookingFlow the booking flow name.
    *
    * @return the bookingFlowName.
    *
    */
   public static BookingFlow findByBookFlowCode(String bookingFlow)
   {
      for (BookingFlow bookFlow : values())
      {
         if (bookFlow.getBookingFlow().equals(bookingFlow))
         {
            return bookFlow;
         }
      }
      return null;
   }
}

