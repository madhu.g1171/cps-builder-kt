/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: FlightDetails.java,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: $
 *
 * $Author: thomas.pm $
 *
 * $Log : $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is responsible for holding all flight details to be displayed in Payment page rendered
 * by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class FlightDetails implements Serializable
{
   /** The generated serial version id. */
   private static final long serialVersionUID = -7836844639905422671L;

   /** The name of the departure airport of flight. */
   private String departureAirportName;

   /** The departure date and time of the flight. */
   private Date departureDateTime;

   /** The name of the arrival airport. of flight. */
   private String arrivalAirportName;

   /** The arrival date and time of the flight. */
   private Date arrivalDateTime;

   /** The flight carrier. */
   private String carrier;

   /** The operating airline code. */
   private String operatingAirlineCode;

   /** The operating airline short name. */
   private String operatingAirlineShortName;

   /** The marketing airline code. */
   private String marketingAirlineCode;

   /** The flight number. */
   private String flightNumber;

   /** The code of the departure airport of flight. */
   private String departureAirportCode;

   /** The code of the arrival airport of flight. */
   private String arrivalAirportCode;

   /** The departure time of the flight. */
   private String departureTime;

   /** The arrival time of the flight. */
   private String arrivalTime;

   /** The flight offset days. */
   private int flightOffsetDays;

   /** The Flight Sector type. */
   private String flightSectorType;

   /**
    * Constructor for Flight.
    *
    * @param departureAirportName the departureAirportName.
    * @param departureDateTime the departureDateTime.
    * @param arrivalAirportName the arrivalAirportName.
    * @param arrivalDateTime the arrivalDateTime.
    * @param carrier the carrier.
    * @param operatingAirlineCode the operating airline code.
    * @param flightNumber the flightNumber.
    */
   public FlightDetails(String departureAirportName, Date departureDateTime,
                        String arrivalAirportName, Date arrivalDateTime, String carrier,
                        String operatingAirlineCode, String flightNumber)
   {
      this.departureAirportName = departureAirportName;
      if (departureDateTime != null)
      {
         this.departureDateTime = new Date(departureDateTime.getTime());
      }
      this.arrivalAirportName = arrivalAirportName;
      if (arrivalDateTime != null)
      {
         this.arrivalDateTime = new Date(arrivalDateTime.getTime());
      }
      this.carrier = carrier;
      this.operatingAirlineCode = operatingAirlineCode;
      this.flightNumber = flightNumber;
   }

   /**
    * Get the departure airport name.
    *
    * @return departureAirportName the departureAirportName.
    */
   public String getDepartureAirportName()
   {
      return departureAirportName;
   }

   /**
    * Get the departure date and time.
    *
    * @return departureDateTime the departureDateTime.
    */
   public Date getDepartureDateTime()
   {
      if (departureDateTime != null)
      {
         return new Date(departureDateTime.getTime());
      }
      else
      {
         return departureDateTime;
      }
   }

   /**
    * Get the arrival airport name.
    *
    * @return arrivalAirportName the arrivalAirportName.
    */
   public String getArrivalAirportName()
   {
      return arrivalAirportName;
   }

   /**
    * Get the arrival date and time.
    *
    * @return arrivalDateTime the arrivalDateTime.
    */
   public Date getArrivalDateTime()
   {
      if (arrivalDateTime != null)
      {
         return new Date(arrivalDateTime.getTime());
      }
      else
      {
         return arrivalDateTime;
      }
   }

   /**
    * Get the flight carrier.
    *
    * @return carrier the carrier.
    */
   public String getCarrier()
   {
      return carrier;
   }

   /**
    * Get the flight operatingAirlineCode.
    *
    * @return operatingAirlineCode the operating airline code.
    */
   public String getOperatingAirlineCode()
   {
      return operatingAirlineCode;
   }

   /**
    * Get the flight number.
    *
    * @return flightNumber the flightNumber.
    */
   public String getFlightNumber()
   {
      return flightNumber;
   }

   /**
    * Get the operating airline short name.
    *
    * @return operatingAirlineShortName the operating airline short name.
    */
   public String getOperatingAirlineShortName()
   {
      return operatingAirlineShortName;
   }

   /**
    * Gets the marketing airline code.
    *
    * @return the marketingAirlineCode.
    */
   public String getMarketingAirlineCode()
   {
      return marketingAirlineCode;
   }

   /**
    * Get the departure airport code.
    *
    * @return departureAirportCode the departureAirportCode.
    */
   public String getDepartureAirportCode()
   {
      return departureAirportCode;
   }

   /**
    * Get the arrival airport code.
    *
    * @return arrivalAirportCode the arrivalAirportCode.
    */
   public String getArrivalAirportCode()
   {
      return arrivalAirportCode;
   }

   /**
    * Set the operating airline short name.
    *
    * @param operatingAirlineShortName the operating airline short name to set.
    */
   public void setOperatingAirlineShortName(String operatingAirlineShortName)
   {
      this.operatingAirlineShortName = operatingAirlineShortName;
   }

   /**
    * Sets the marketing airline code.
    *
    * @param marketingAirlineCode the marketingAirlineCode to set.
    */
   public void setMarketingAirlineCode(String marketingAirlineCode)
   {
      this.marketingAirlineCode = marketingAirlineCode;
   }

   /**
    * Set the departure airport code.
    *
    * @param departureAirportCode the departureAirportCode.
    */
   public void setDepartureAirportCode(String departureAirportCode)
   {
      this.departureAirportCode = departureAirportCode;
   }

   /**
    * Set the arrival airport code.
    *
    * @param arrivalAirportCode the arrivalAirportCode.
    */
   public void setArrivalAirportCode(String arrivalAirportCode)
   {
      this.arrivalAirportCode = arrivalAirportCode;
   }

   /**
    * Gets the Departure Time.
    *
    * @return the departureTime.
    */
   public String getDepartureTime()
   {
      return departureTime;
   }

   /**
    * Set the Departure Time.
    *
    * @param departureTime the departureTime.
    */
   public void setDepartureTime(String departureTime)
   {
      this.departureTime = departureTime;
   }

   /**
    * Gets the Arrival Time.
    *
    * @return the arrivalTime.
    */
   public String getArrivalTime()
   {
      return arrivalTime;
   }

   /**
    * Set the Arrival Time.
    *
    * @param arrivalTime the arrivalTime.
    */
   public void setArrivalTime(String arrivalTime)
   {
      this.arrivalTime = arrivalTime;
   }

   /**
    * Gets the flight offset days.
    *
    * @return the flight offset days
    */
   public int getFlightOffsetDays()
   {
      return flightOffsetDays;
   }

   /**
    * Sets the flight offset days.
    *
    * @param flightOffsetDays the new flight offset days
    */
   public void setFlightOffsetDays(int flightOffsetDays)
   {
      this.flightOffsetDays = flightOffsetDays;
   }

   /**
    * Gets the flight sector type.
    *
    * @return the flight sector type
    */
   public String getFlightSectorType()
   {
      return flightSectorType;
   }

   /**
    * Sets the flight sector type.
    *
    * @param flightSectorType the new flight sector type
    */
    public void setFlightSectorType(String flightSectorType)
    {
       this.flightSectorType = flightSectorType;
    }

}
