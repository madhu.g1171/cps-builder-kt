/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BookingData.java$
 *
 * $Revision: $
 *
 * $Date: Aug 26, 2008$
 *
 * Author: vijayalakshmi.d
 *
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is used to hold all the details of the booking in the CommonpaymentComponent.
 *
 * @author vijayalakshmi.d
 *
 */
public final class BookingData implements Serializable
{
   /**
    * The generated SerialID.
    */
   private static final long serialVersionUID = 6596992056565236615L;

   /** The Constant to hold booking reference . */
   private String bookingReference;

   /** The attribute for  Source Booking System Reference. */
   private String bookingSystemReference;
   
   /** The Constant to hold booking status such as cancelled,sucessfull . */
   private String bookingStatus;

   /** The Constant to hold booking date . */
   private Date bookingdate;

   /** The Constant to hold FileBriefID for the CSSbooking. */
   private String fileBriefID;

   /** The constant to hold FileBriefStatus for the CSSbooking. */
   private String fileBriefStatus;

   /** The Constant to hold whether Reaccreditation is allowed on the booking or not. */
   private Boolean reaccreditationAllowed;

   /** The attribute to hold passenger details like no of passengers etc. */
   private String passengerDetails;

   /** The attribute to hold full name of leadPassenger. */
   private String leadPassengerName;

   /** The attribute for BookingFlowDetails. */
   private BookingFlowDetails bookingFlowDetails;

   /**
    * Constructor for BookingData.
    *
    * @param bookingReference the bookingReference
    * @param bookingStatus the bookingStatus.
    * @param bookingDate the bookingDate.
    *
    */
   public BookingData(String bookingReference, String bookingStatus, Date bookingDate)
   {
      this.bookingReference = bookingReference;
      this.bookingStatus = bookingStatus;
      this.bookingdate = new Date(bookingDate.getTime());
   }

   /**
    * Gets the bookingReference.
    *
    * @return the bookingReference
    */
   public String getBookingReference()
   {
      return bookingReference;
   }

   /**
    * Gets the bookingSystemReference.
    *
    * @return the bookingSystemReference.
    */
   public String getBookingSystemReference() 
   {
     return bookingSystemReference;
    }
   /**
    * Sets the bookingSystemReference.
    *
    * @param bookingSystemReference holds the bookingSystemReference
    */
    public void setBookingSystemReference(String bookingSystemReference) 
    {
      this.bookingSystemReference = bookingSystemReference;
     }

   /**
    * Gets the bookingStatus.
    *
    * @return the bookingStatus
    */
   public String getBookingStatus()
   {
      return bookingStatus;
   }

   /**
    * Gets the bookingDate.
    *
    * @return the shopName
    */
   public Date getBookingDate()
   {
      return new Date(bookingdate.getTime());
   }

   /**
    * Sets the FileBriefID.
    *
    * @param fileBriefID holds the fileBriefID
    */
   public void setFileBriefID(String fileBriefID)
   {
      this.fileBriefID = fileBriefID;
   }

   /**
    * Gets the FileBriefID.
    *
    * @return fileBriefID
    */
   public String getFileBriefID()
   {
      return fileBriefID;
   }

   /**
    * Sets the fileBriefStatus.
    *
    * @param fileBriefStatus holds the fileBriefStatus
    */
   public void setFileBriefStatus(String fileBriefStatus)
   {
      this.fileBriefStatus = fileBriefStatus;
   }

   /**
    * Gets the FileBriefStatus.
    *
    * @return fileBriefStatus
    */
   public String getFileBriefStatus()
   {
      return fileBriefStatus;
   }

   /**
    * Sets the variable reaccreditationAllowed.
    *
    * @param reaccreditationAllowed reaccreditationAllowed
    */
   public void setReaccreditationAllowed(Boolean reaccreditationAllowed)
   {
      this.reaccreditationAllowed = reaccreditationAllowed;
   }

   /**
    * gets the reaccreditationAllowed value.
    *
    * @return reaccreditationAllowed
    */
   public Boolean getReaccreditationAllowed()
   {
      return reaccreditationAllowed;
   }

   /**
    * Sets the variable passengerDetails.
    *
    * @param passengerDetails the passengerDetails
    */
   public void setPassengerDetails(String passengerDetails)
   {
      this.passengerDetails = passengerDetails;
   }

   /**
    * gets the passengerDetails value.
    *
    * @return passengerDetails
    */
   public String getPassengerDetails()
   {
      return passengerDetails;
   }

   /**
    * Sets the variable leadPassengerName.
    *
    * @param leadPassengerName the leadPassengerName
    */
   public void setLeadPassengerName(String leadPassengerName)
   {
      this.leadPassengerName = leadPassengerName;
   }

   /**
    * gets the leadPassengerName value.
    *
    * @return leadPassengerName
    */
   public String getLeadPassengerName()
   {
      return leadPassengerName;
   }

   /**
    * Gets the bookingFlowDetails.
    * @return bookingFlowDetails the bookingFlowDetails object.
    */
   public BookingFlowDetails getBookingFlowDetails()
   {
      return bookingFlowDetails;
   }

   /**
    * Sets the booking flow details.
    * @param bookingFlowDetails the bookingFlowDetails object.
    */
   public void setBookingFlowDetails(BookingFlowDetails bookingFlowDetails)
   {
      this.bookingFlowDetails = bookingFlowDetails;
   }
}