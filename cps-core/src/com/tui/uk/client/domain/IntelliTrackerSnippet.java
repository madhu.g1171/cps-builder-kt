/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: IntelliTrackerSnippet.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-08-28 07:41:25 $
 *
 * $author: vinothkumar.k$
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * This class is responsible for holding the IntelliTracker string to be
 * displayed in Payment page rendered by Common Payment Component.
 *
 * @author vinothkumar.k
 *
 * @since 1.0 API
 *
 */
public final class IntelliTrackerSnippet implements Serializable
{
   /** The generated serial version id.*/
   private static final long serialVersionUID = 1581404693571445453L;

   /** The String that holds pqry. */
   private static final String PQRY = "pqry=\"";

   /** The String that contains closing character for the pqry. */
   private static final String PQRY_CLOSER = "\";";

   /** Pattern for validating valid chars. */
   private static final  Pattern PQRY_PATTERN = Pattern.compile("[A-Za-z0-9-%/_#null]+");

   /** The String that contains invalid char strings. */
   private static final String[] INVALID_CHAR_STRINGS = {"%00" , "0x0f" , "0xff",
      "<script"};

   /** The String that holds intellitracker parameters. */
   private String intelliTrackerString;

   /** The String that holds the previous page name. */
   private String previousPage;

   /**
    * Constructor for IntelliTrackerSnippet with parameters
    * intellitrackerSnippet string and previous page name
    * string.
    *
    * @param intelliTrackerSnippet intelliTracker String variable.
    * @param prevPage previous page name.
    */
    public IntelliTrackerSnippet(
         final String intelliTrackerSnippet, final String prevPage)
    {
        String pqry = getPqry(intelliTrackerSnippet);
        validate(pqry);

        this.intelliTrackerString = pqry;
        this.previousPage = prevPage;
    }

    /**
     * Gets the intelliTrackerSnippet string.
     * @return the intelliTrackerSnippet String.
     */
     public String getIntelliTrackerString()
     {
       //TODO:When all applications start using jar 04.00.01 ,
       //this method needs to return intelliTrackerString instead of pqry
       return getPqry(intelliTrackerString);
     }

   /**
    * Gets the previous page name string.
    * @return the prevPage String.
    */
   public String getPreviousPage()
   {
      return previousPage;
   }

   /**
    *  Extracts the pqry from the given string.
    *
    *  @param intelliTrackerSnippet intelliTracker String variable.
    *  @return the pqry after extracting from intelliTrackerSnippet.
    */
   private String getPqry(String intelliTrackerSnippet)
   {
     if (null != intelliTrackerString && intelliTrackerSnippet.contains(PQRY))
     {
       int start = intelliTrackerSnippet.indexOf(PQRY);
       if (start != -1)
       {
         int end = intelliTrackerSnippet.indexOf(PQRY_CLOSER, start + PQRY.length());
         if (end != -1)
         {
           return intelliTrackerSnippet.substring(start + PQRY.length(), end);
         }
       }
     }
     return intelliTrackerSnippet;
   }

   /**
    * validates pqry to check whether it contains any known bad character.
    *
    * @param pqry pqry contains the information used for site tracking.
    *
    * @throws IllegalArgumentException if pqry does not match with the pattern.
    */
   private void validate(String pqry)
   {
      if (null != pqry)
      {
         Matcher matcher = PQRY_PATTERN.matcher(pqry);
         if (!matcher.matches())
         {
            throw new IllegalArgumentException();
         }
         validateInvalidStrings(pqry);
      }
   }

   /**
    * validates pqry to check whether it contains any known bad strings.
    *
    * @param pqry pqry contains the information used for site tracking.
    *
    * @throws IllegalArgumentException if pqry contain any invalid strings.
    */
   private void validateInvalidStrings(String pqry)
   {
     for (String invalidChar : INVALID_CHAR_STRINGS)
     {
       if (pqry.contains(invalidChar))
       {
         throw new IllegalArgumentException();
       }
     }
   }
}
