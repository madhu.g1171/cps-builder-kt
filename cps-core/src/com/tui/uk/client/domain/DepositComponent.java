/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: DepositComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 08:04:25$
 *
 * $author: thomas.pm$
 *
 * $Log : $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;
import com.tui.uk.client.domain.DDPaymentSheduleComponent;
import java.util.List;


/**
 * This class is responsible for holding all deposit details to be displayed in Payment page
 * rendered by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class DepositComponent implements Serializable
{
	/** The generated serial version id. */
	private static final long serialVersionUID = 3545049721896855055L;

	/** The deposit type. Eg: Low Deposit or Deposit. */
	private String depositType;


	/** The deposit amount. */
	private Money depositAmount;

	/**
	 * Sets the depositAmount.
	 *
	 * @param depositAmount the depositAmount.
	 */
	public void setDepositAmount(Money depositAmount)
	{
		this.depositAmount = depositAmount;
	}

	/** The balance amount for the deposit. */
	private Money outstandingBalance;

	/** The due date for the deposit. */
	private Date depositDueDate;

	/** The depositTypeDescription. */
	private String depositTypeDescription;

	/** The default deposit type to be selected in the payment page. */
	private Boolean defaultDepositTypeToBeSelected;

	/** The card charge for the deposit amount if applicable. */
	private Money applicableCardCharge;

	/** The card charge for outstanding balance if applicable. */
	private Money outstandingBalanceCardCharge;

	/** The debit card charge for the deposit amount. */
	private Money applicableDebitCardCharge;

	/** The deposit data for 3PA flights*/
	private String depositDataPP;

	/** The for direct debit deposit*/
	private String dayOfMonth;

	/** The ddpaymentSheduleComponent */
	private List<DDPaymentSheduleComponent> ddPaymentComponents;

	public List<DDPaymentSheduleComponent> getDdPaymentComponents() {
		return ddPaymentComponents;
	}

	public void setDdPaymentComponents(
			List<DDPaymentSheduleComponent> ddPaymentComponents) {
		this.ddPaymentComponents = ddPaymentComponents;
	}

	public String getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(String dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	/**
	 * Constructor for DepositComponent.
	 *
	 * @param depositType the deposit type.
	 * @param depositAmount the deposit amount.
	 * @param depositDueDate the deposit due date.
	 * @param outstandingBalance the outstanding balance.
	 */
	public DepositComponent(String depositType, Money depositAmount, Date depositDueDate,
			Money outstandingBalance)
	{
		this.depositType = depositType;
		this.depositAmount = depositAmount;
		this.depositDueDate = (Date) depositDueDate.clone();
		this.outstandingBalance = outstandingBalance;
	}

	/**
	 * Get the deposit type.
	 *
	 * @return depositType the deposit type.
	 */
	public String getDepositType()
	{
		return depositType;
	}

	/**
	 * Get the deposit amount.
	 *
	 * @return depositAmount the deposit amount.
	 */
	public Money getDepositAmount()
	{
		return depositAmount;
	}

	/**
	 * Get the deposit due date.
	 *
	 * @return depositDueDate the deposit due date.
	 */
	public Date getDepositDueDate()
	{
		return new Date(depositDueDate.getTime());
	}

	/**
	 * Get the outstandingBalance.
	 *
	 * @return outstandingBalance the outstanding balance.
	 */
	public Money getOutstandingBalance()
	{
		return outstandingBalance;
	}

	/**
	 * Get the depositTypeDescription.
	 *
	 * @return depositTypeDescription the depositTypeDescription.
	 */
	public String getDepositTypeDescription()
	{
		return depositTypeDescription;
	}

	/**
	 * Gets which deposit type should be selected by default on load of the page.
	 *
	 * @return defaultDepositTypeToBeSelected the default deposit type to be selected.
	 */
	public Boolean getDefaultDepositTypeToBeSelected()
	{
		return defaultDepositTypeToBeSelected;
	}

	/**
	 * Gets the card charge for the deposit amount if applicable.
	 *
	 * @return applicableCardCharge the applicable card charge.
	 */
	public Money getApplicableCardCharge()
	{
		return applicableCardCharge;
	}

	/**
	 * Gets the card charge for balance deposit amount if applicable.
	 *
	 * @return outstanding balance card charge.
	 */
	public Money getOutstandingBalanceCardCharge()
	{
		return outstandingBalanceCardCharge;
	}

	/**
	 * Gets the applicable debit card charge.
	 *
	 * @return the debit card charge.
	 */
	public Money getApplicableDebitCardCharge()
	{
		return applicableDebitCardCharge;
	}
	/**
	 * get the deposit data
	 * @return String
	 */
	public String getDepositDataPP() {
		return depositDataPP;
	}

	/**
	 * Sets the depositTypeDescription.
	 *
	 * @param depositTypeDescription the depositTypeDescription.
	 */
	public void setDepositTypeDescription(String depositTypeDescription)
	{
		this.depositTypeDescription = depositTypeDescription;
	}

	/**
	 * Sets the default depositType to be selected.
	 *
	 * @param defaultDepositTypeToBeSelected the defaultDepositTypeToBeSelected.
	 */
	public void setDefaultDepositTypeToBeSelected(Boolean defaultDepositTypeToBeSelected)
	{
		this.defaultDepositTypeToBeSelected = defaultDepositTypeToBeSelected;
	}

	/**
	 * Sets the card charge applicable for the deposit amount.
	 *
	 * @param applicableCardCharge the applicableCardCharge.
	 */
	public void setApplicableCardCharge(Money applicableCardCharge)
	{
		this.applicableCardCharge = applicableCardCharge;
	}

	/**
	 * Sets the card charge applicable for balance deposit amount.
	 *
	 * @param outstandingBalanceCardCharge the outstandingBalanceCardCharge.
	 */
	public void setOutstandingBalanceCardCharge(Money outstandingBalanceCardCharge)
	{
		this.outstandingBalanceCardCharge = outstandingBalanceCardCharge;
	}

	/**
	 * Sets the applicable debit card charge.
	 *
	 * @param applicableDebitCardCharge the applicable debit card charge.
	 */
	public void setApplicableDebitCardCharge(Money applicableDebitCardCharge)
	{
		this.applicableDebitCardCharge = applicableDebitCardCharge;
	}

	/**
	 * Sets the deposit data for 3PA flights
	 *
	 * */
	public void setDepositDataPP(String depositDataPP) {
		this.depositDataPP = depositDataPP;
	}
}
