/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PassengerSummary.java,v $
 *
 * $Revision: $
 *
 * $Date: Jun 11, 2008$
 *
 * $author : thomas.m $
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class is responsible for holding all passenger details to
 * be displayed in Payment page
 * rendered by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */
public final class PassengerSummary implements Serializable
{

   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 2180020655880994900L;

   /** The label of passenger for ex., for adult passenger label
    *  should be ADULT. */
   private String label;


   /** The age of the passenger. Used only for children. */
   private Integer age;

   /** Holds the passenger age indicator. */
   private AgeClassification ageClassification;

   /** This will be true if there is a lead passenger. */
   private Boolean leadPassenger;

   /** The identifier of the passenger. */
   private Integer identifier;

   /** The title of the passenger. */
   private String title;

   /** The foreName of the passenger. */
   private String foreName;

   /** The middleName of the passenger. */
   private String middleName;

   /** The lastName of the passenger. */
   private String lastName;

   /** The max age of the passenger in order to validate DOB calculation. */
   private Integer maxAge;

   /** The min age of the passenger in order to validate DOB calculation. */
   private Integer minAge;

   /** This flag gives us the info whether insurance has been selected
    *  for the corresponding passenger.
    */
   private Boolean isInsuranceSelected = null;

   /**
    * Constructor for creating this object.
    *
    * @param ageClassification the age classification
    * @param leadPassenger the leadPassenger
    * @param identifier the identifier
    *
    */
   public PassengerSummary(AgeClassification ageClassification,
                           Boolean leadPassenger, Integer identifier)
   {
      this.ageClassification = ageClassification;
      this.leadPassenger = leadPassenger;
      this.identifier = identifier;
   }

   /**Returns the insurance selection status of the passenger.
    * @return isInsuranceSelected the insurance status flag.
    */
   public Boolean getIsInsuranceSelected()
   {
      return isInsuranceSelected;
   }

   /**Sets the insurance selection status of the passenger.
    * @param isInsuranceSelectedFlag the insurance status flag.
    */
   public void setInsuranceSelected(Boolean isInsuranceSelectedFlag)
   {
     this.isInsuranceSelected = isInsuranceSelectedFlag;
   }

   /**
    * Gets the identifier.
    *
    * @return the identifier
    */
   public Integer getIdentifier()
   {
      return identifier;
   }

   /**
    * Gets the age classification.
    *
    * @return the ageClassification
    */
   public AgeClassification getAgeClassification()
   {
      return ageClassification;
   }

   /**
    * Get the lead passenger.
    *
    * @return true if the passenger is a lead passenger, otherwise false.
    */
   public Boolean getLeadPassenger()
   {
      return leadPassenger;
   }

   /**
    * Get the label.
    *
    * @return the label
    */
   public String getLabel()
   {
      return label;
   }

   /**
    * Get the age of child.
    *
    * @return the age
    */
   public Integer getAge()
   {
      return age;
   }

   /**
    * Get the title.
    *
    * @return the title
    */
   public String getTitle()
   {
     return title;
   }

   /**
    * Get the foreName.
    *
    * @return the foreName
    */
   public String getForeName()
   {
     return foreName;
   }

   /**
    * Get the middleName.
    *
    * @return the middleName
    */
   public String getMiddleName()
   {
     return middleName;
   }

   /**
    * Get the lastName.
    *
    * @return the lastName
    */
   public String getLastName()
   {
     return lastName;
   }

   /**
    * Get the maxAge. The max age of the passenger in order to validate
    * DOB calculation.
    * Ideally this is set in case of infant and child.
    *
    * @return the maxAge.
    */
   public Integer getMaxAge()
   {
      return maxAge;
   }

   /**
    * Get the minAge. The min age of the passenger in order to validate
    * DOB calculation.
    * Ideally this is set in case of child.
    *
    * @return the minAge.
    */
   public Integer getMinAge()
   {
      return minAge;
   }

   /**
    * Sets the label.
    *
    * @param label the label to set
    */
   public void setLabel(String label)
   {
      this.label = label;
   }

   /**
    * Sets the age.
    *
    * @param age the age to set
    */
   public void setAge(Integer age)
   {
      this.age = age;
   }

   /**
    * Sets the title.
    *
    * @param title the title to set
    */
   public void setTitle(String title)
   {
     this.title = title;
   }

   /**
    * Sets the foreName.
    *
    * @param foreName the foreName to set
    */
   public void setForeName(String foreName)
   {
     this.foreName = foreName;
   }

   /**
    * Sets the middleName.
    *
    * @param middleName the middleName to set
    */
   public void setMiddleName(String middleName)
   {
     this.middleName = middleName;
   }

   /**
    * Sets the lastName.
    *
    * @param lastName the lastName to set
    */
   public void setLastName(String lastName)
   {
     this.lastName = lastName;
   }

   /**
    * Sets the maxAge. The max age of the passenger in order to validate
    * DOB calculation.
    * Ideally this is set in case of infant and child.
    *
    * @param maxAge the maxAge to set
    */
   public void setMaxAge(Integer maxAge)
   {
      this.maxAge = maxAge;
   }

   /**
    * Sets the minAge. The min age of the passenger in order to validate
    * DOB calculation.
    * Ideally this is set in case of child.
    *
    * @param minAge the minAge to set
    */
   public void setMinAge(Integer minAge)
   {
      this.minAge = minAge;
   }

   /**
    * Sets the Lead passenger boolean.
    *
    * @param leadPassenger the leadPassenger to set
    */
    public void setLeadPassenger(Boolean leadPassenger)
    {
       this.leadPassenger = leadPassenger;
    }

}
