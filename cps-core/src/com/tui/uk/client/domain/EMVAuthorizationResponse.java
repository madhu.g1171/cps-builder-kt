/**
 *
 */
package com.tui.uk.client.domain;

import org.apache.commons.lang.StringUtils;

import com.datacash.util.XMLDocument;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;

/**
 * The class is used for mapping EMV 3DS Authorization Response.
 * 
 * @author sreenivasulu.k
 *
 */
public class EMVAuthorizationResponse {

	private String cv2avsStatus=StringUtils.EMPTY;

	private String authcode=StringUtils.EMPTY;

	private String cardScheme=StringUtils.EMPTY;

	private String country=StringUtils.EMPTY;

	private String dsReference=StringUtils.EMPTY;

	private String merchantreference=StringUtils.EMPTY;

	private String mode=StringUtils.EMPTY;

	private String reason=StringUtils.EMPTY;

	private String status=StringUtils.EMPTY;

	private String time=StringUtils.EMPTY;

	private String issuer=StringUtils.EMPTY;

	public EMVAuthorizationResponse(XMLDocument response) {

		this.cv2avsStatus = response
				.get("Response.CardTxn.Cv2Avs.cv2avs_status");
		this.authcode = response.get("Response.CardTxn.authcode");
		this.cardScheme = response.get("Response.card_scheme");
		this.country = response.get("Response.country");
		this.dsReference = response.get("Response.datacash_reference");
		this.merchantreference = response.get("Response.merchantreference");
		this.mode = response.get("Response.mode");
		this.reason = response.get("Response.reason");
		this.status = response.get("Response.status");
		this.time = response.get("Response.time");
		this.issuer = response.get(DataCashServiceConstants.ISSUER_RS);

	}

	public String getCv2avsStatus() {
		return cv2avsStatus;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setCv2avsStatus(String cv2avsStatus) {
		this.cv2avsStatus = cv2avsStatus;
	}

	public String getAuthcode() {
		return authcode;
	}

	public void setAuthcode(String authcode) {
		this.authcode = authcode;
	}

	public String getCardScheme() {
		return cardScheme;
	}

	public void setCardScheme(String cardScheme) {
		this.cardScheme = cardScheme;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDsReference() {
		return dsReference;
	}

	public void setDsReference(String dsReference) {
		this.dsReference = dsReference;
	}

	public String getMerchantreference() {
		return merchantreference;
	}

	public void setMerchantreference(String merchantreference) {
		this.merchantreference = merchantreference;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
