/*
* Copyright (C)2008 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile: AgeClassification.java,v $
*
* $Revision:  $
*
* $Date:  $
*
* $Author:  thomas.pm$
*
* $Log:  $
*
*
*/
package com.tui.uk.client.domain;

/**
 * Classifiers for the age of persons traveling or using accommodation.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */
public enum AgeClassification
{
   /** An infant (usually someone under 2 years of age). */
   INFANT("INF"),
   /** A child (usually someone over 2, upper limit is dependent on inventory system). */
   CHILD("CHD"),
   /** Any individual older than a child, but not yet a senior citizen. */
   ADULT("ADT"),
   /** A senior citizen (usually 65 t0 75). */
   SENIOR("SNR"),
   /** A senior citizen (usually 76 to 84). */
   SENIOR2("SNR2");

   /** Holds the Code for this passenger type. */
   private final String code;

   /**
    * Initializes this enumeration with the passenger type code.
    *
    * @param code the passenger type code
    */
   private AgeClassification(String code)
   {
      this.code = code;
   }

   /**
    * Get the passenger type code.
    *
    * @return the code.
    */
   public String getCode()
   {
      return this.code;
   }

   /**
    * This method determines the age classification for the code passed.
    *
    * @param requiredCode the code of the required age classification.
    *
    * @return the age classification found.
    *
    * @throws IllegalArgumentException if the code does not relate to a known
    *         age classification.
    */
   public static AgeClassification findByCode(String requiredCode)
   {
      for (AgeClassification ageClassification : values())
      {
         if (ageClassification.getCode().equals(requiredCode))
         {
            return ageClassification;
         }
      }
      throw new IllegalArgumentException("Unknown age classification code:" + requiredCode);
   }
}
