/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 08:08:25$
 *
 * $author: thomas.pm$
 *
 * $Log : $
 */
package com.tui.uk.client.domain;

import static com.tui.uk.client.domain.BookingConstants.HTTPS_PROTOCOL;
import static com.tui.uk.client.domain.BookingConstants.HTTP_PROTOCOL;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * This class is responsible for holding all Booking related information required by common payment
 * component so as to render payment page. This is simplified version of client application's
 * booking details, having only required information for payment page display.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */

public final class BookingComponent implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = -3215491945522541807L;

   /**
    * List of countries to be displayed in the select box. Country code is the key and country name
    * is the value.
    */
   private Map<String, String> countryMap;
   
   /**
	 * for HCCS impl new attribute added
	 *
	 */

	private String merchantReference;

	public String getMerchantReference() {
		return merchantReference;
	}

	public void setMerchantReference(String merchantReference) {
		this.merchantReference = merchantReference;
	}

	/**
	 * HCCS response attribute value
	 *
	 */
	
	private Map<String, String> hccsMap;

	public Map<String, String> getHccsMap() {
		return hccsMap;
	}

	public void setHccsMap(Map<String, String> hccsMap) {
		this.hccsMap = hccsMap;
	}

	private Map<String, String> hccCapturedData;

	public Map<String, String> getHccCapturedData() {
		return hccCapturedData;
	}

	public void setHccCapturedData(Map<String, String> hccCapturedData) {
		this.hccCapturedData = hccCapturedData;
	}

   /**
    * A <code>Map</code> containing room number as key and list of passengers as value.
    */
   private Map<Integer, List<PassengerSummary>> passengerRoomSummary;

   private LockYourPriceSummary lockYourPriceSummary;

   /**
    * A <code>Map</code> containing the non payment related data such as the passenger first name,
    * surname, address, etc. entered by the user in the Payment Page rendered by CPC. This Map
    * contains the field names in the Payment Page as the key and the corresponding data entered by
    * the user as the value.
    */
   private Map<String, String> nonPaymentData = new HashMap<String, String>();

   /** The list of price components (costing lines). */
   private List<PriceComponent> priceComponents;

   /**
    * A <code>Map</code> containing the pricing related details like price components, price break
    * down values etc. The price related details are put into the map as key-value pairs and the
    * values should be of type PriceComponent.
    *
    * For PriceComponents the constant that should be sent to CPS should be
    * <code>BookingConstants.PRICE_COMPONENTS</code>, for price break down the constant that should
    * be sent to CPS should be <code>BookingConstants.PRICE_BREAKDOWN</code>.
    */
   private Map<String, List<PriceComponent>> pricingDetails;

   /** The list Refunddetails (transactiondetails). */
   private List<RefundDetails> refundDetails;

   /**
    * A <code>Map</code> containing the different payment types. This map contains the PaymentMode
    * as the key and the PaymentType as the value. PaymentMode can be either CP(Customer
    * Present),CNP(Customer Not Present), CPNA(Chip And Pin Not Available) or PPG(Post Payment
    * Guarantee). CP entry will have paymentTypes like cash, cheque, voucher, PDQ Cards or PinPad.
    * CNP entry will have paymentTypes like DataCash cards. CPNA will have only CNP paymentTypes
    * plus cash, cheque and voucher. PPG paymentTypes will have only paymentTypes which can be
    * accepted via HOPLA Hotel.
    *
    */
    
   private Map<String, List<PaymentType>> paymentType;

   /** The list of applicable discounts. */
   private List<DiscountComponent> discountComponents;

   /** The list of deposit components available. */
   private List<DepositComponent> depositComponents;

   /** The summary of flight details. */
   private FlightSummary flightSummary;

   /** The summary of cruise details. */
   private CruiseSummary cruiseSummary;

   /** The summary of cruise details. */
   private boolean  multiCentre;

   /** The summary of accommodation details. */
   private AccommodationSummary accommodationSummary;
   
   private boolean setUpFails            = false;


	public boolean isSetUpFails() {
		return setUpFails;
	}

	public void setSetUpFails(boolean setUpFails) {
		this.setUpFails = setUpFails;
	}

   /** Details of important information section. */
   private ImportantInformation importantInformation;

   /** The source of business. */
   private SourceOfBusiness sourceOfBusiness;

   /** The client application name. */
   private ClientApplication clientApplication;

   /**
    * The url of the client application, used for redirections and other links.
    */
   private String clientDomainURL;

   /** The total amount of booking. */
   private Money totalAmount;

   /** This is used for promotional code validation. */
   private String tracsBookMinusId;

   /** The package type. */
   private PackageType packageType;

   /** Any error message. Eg: error messages related to price change. */
   private String errorMessage;

   /** The call type. */
   private String callType;

   /** The company Code. */
   private String companyCode;

   /** The payable amount. */
   private Money payableAmount;

   /** The paid amount till date. */
   private Money totalPaidTillDate;

   /** The max card charge percent. */
   private BigDecimal maxCardChargePercent;

   /** The max card charge. */
   private BigDecimal maxCardCharge;

   /** The no of transactions. */
   private Integer noOfTransactions;

   /** The search type like free kids, late search, etc. */
   private String searchType;

   /** The abta details. */
   private AbtaDetails abtaDetails;

   /** Intellitracker snippet. */
   private IntelliTrackerSnippet intelliTrackSnippet;

   /** Map containing card type code along with its corresponding charges. */
   private Map<String, BigDecimal> cardChargesMap;

   /** Object to hold ShopDetails . **/
   private ShopDetails shopDetails;

   /** Object to hold bookingData. **/
   private BookingData bookingData;

   /** Object to hold TermsAndCondition. **/
   private TermsAndCondition termsAndCondition;

   /** Object to hold data-cash enabled status. */
   private boolean datacashEnabled;

   /**
    * The map of extra facilities. Key will be Extra facility/Option description. Values will be
    * list of options/facilities. Any package level options can be put into this map. Eg., Transport
    * Options, Extras, etc.,
    */
   private Map<String, List<String>> extraFacilities;

   /** The payment failure URL. */
   private String paymentFailureURL;

   /** The data-cash client account(virtual terminal id). */
   private String paymentGatewayVirtualTerminalId;

   /** The list of rooms. */
   private List<Room> rooms;

   /** The pre payment page URL. */
   private String prePaymentUrl;

   /** The success page URL. */
   private String paymentSuccessUrl;

   /**
    * To specify if redirect is required or not for TRACS application. By default it is set to
    * false.
    */
   private Boolean noRedirect = Boolean.FALSE;

   /**
    * The map for holding bread crumbs.
    */
   private Map<String, String> breadCrumbTrail;

   /**
    * This object holds all the URLs set by the client application and all the URLs that needs to be
    * set for future requirements should be set into this object.
    */
   private ClientURLComponent clientURLLinks;

   /** The booking session identifier. */
   private String bookingSessionIdentifier;

   /** The contact info. */
   private ContactInfo contactInfo;

   /** The discount. */
   private Discount discount;

   /**
    * The operating brand for BRAC/WSS. i.e, the brand for which the amendment is being done.
    */
   private ClientApplication operatingBrand;

   /** used to indicate price has been changed. **/
   private Boolean priceStatus = false;

   /** Used to hold the shopId and the vision reference number. **/
   private String retailBookingReference;

   /** The session time out. */
   private int sessionTimeOut;

   /** The session time alert. */
   private int sessionTimeAlert;

   /**
    * The amount to be paid today. If current date is within the due date, then only the amendment
    * fee needs to be paid today. If current date is after the due date, then the amendment changes
    * need to be paid along with the amendment fee.
    */
   private Money amendmentCharge;

   /** The amendment changes pending which includes all the amendment changes for extras excluding
    *  the amendment fee.
    */
   private Money amendmentChangesPending;

   /** The total amendment fee. */
   private Money totalAmendmentFee;

   /** The Credit Card Fee which is the sum of all credit card charges that were applied as part
    *  of previous payments.
    */
   private Money creditCardFee;

   /** The shellSummary. */
   private ShellSummary shellSummary;

   private PayPalComponent paypalComponent;


   public PayPalComponent getPaypalComponent() {
	return paypalComponent;
   }

   public void setPaypalComponent(PayPalComponent paypalComponent) {
	this.paypalComponent = paypalComponent;
   }

   private DDVariant ddVariant;
   
   public DDVariant getDdVariant() {
    return ddVariant;
}

public void setDdVariant(DDVariant ddVariant) {
    this.ddVariant = ddVariant;
}

   private boolean payPalCreditApplicable;
   private boolean directDebitAvailable;


public boolean isPayPalCreditApplicable() {
	return payPalCreditApplicable;
}

public void setPayPalCreditApplicable(boolean payPalCreditApplicable) {
	this.payPalCreditApplicable = payPalCreditApplicable;
}

public boolean isDirectDebitAvailable() {
	return directDebitAvailable;
}

public void setDirectDebitAvailable(boolean directDebitAvailable) {
	this.directDebitAvailable = directDebitAvailable;
}


	private RetriveThreedAuthResponse retriveThreedAuthRes;

	public RetriveThreedAuthResponse getRetriveThreedAuthRes() {
		return retriveThreedAuthRes;
	}

	public void setRetriveThreedAuthRes(
			RetriveThreedAuthResponse retriveThreedAuthRes) {
		this.retriveThreedAuthRes = retriveThreedAuthRes;
	}
	
	/**
    * Constructor for BookingComponent.
    *
    * @param clientApplication the client application name.
    * @param clientDomainURL the client domain url.
    * @param totalAmount the total amount.
    */
   public BookingComponent(ClientApplication clientApplication, String clientDomainURL,
                           Money totalAmount)
   {
      this.clientApplication = clientApplication;
      this.clientDomainURL = clientDomainURL;
      this.totalAmount = totalAmount;
      // set the discount amount to zero, by default.
      // this.calculatedDiscount = new Money(BigDecimal.ZERO,
      // totalAmount.getCurrency());
   }

   /**
    * Get the passenger room summary.
    *
    * @return the passengerRoomSummary
    */
   public Map<Integer, List<PassengerSummary>> getPassengerRoomSummary()
   {
      return passengerRoomSummary;
   }

   /**
    * Get the list of price components (costing lines).
    *
    * @return the priceComponents
    */
   public List<PriceComponent> getPriceComponents()
   {
      return priceComponents;
   }

   /**
    * Get the flight summary.
    *
    * @return the flightSummary
    */
   public FlightSummary getFlightSummary()
   {
      return flightSummary;
   }

   /**
    * Get the cruisesummary.
    *
    * @return the cruiseSummary
    */
   public CruiseSummary getCruiseSummary()
   {
      return cruiseSummary;
   }

   /**
    * Get the accommodation summary.
    *
    * @return the accommodationSummary
    */
   public AccommodationSummary getAccommodationSummary()
   {
      return accommodationSummary;
   }

   /**
    * Get the country map.
    *
    * @return the countryMap
    */
   public Map<String, String> getCountryMap()
   {
      return countryMap;
   }

   /**
    * Get the important information.
    *
    * @return the importantInformation
    */
   public ImportantInformation getImportantInformation()
   {
      return importantInformation;
   }

   /**
    * Get the source of business.
    *
    * @return the sob
    */
   public SourceOfBusiness getSourceOfBusiness()
   {
      return sourceOfBusiness;
   }

   /**
    * Get the client application name.
    *
    * @return the clientApplication
    */
   public ClientApplication getClientApplication()
   {
      return clientApplication;
   }

   /**
    * Get the client domain URL.
    *
    * @return the clientDomainURL
    */
   public String getClientDomainURL()
   {
      // TODO: This should be removed, once all the client applications send
      // http or https as
      // part of this url.
      if (StringUtils.contains(clientDomainURL, "http"))
      {
         return clientDomainURL;
      }
      return (HTTP_PROTOCOL + clientDomainURL);
   }

   /**
    * Get the total amount.
    *
    * @return the totalAmount
    */
   public Money getTotalAmount()
   {
      return totalAmount;
   }

   /**
    * Get the tracs BookMinusId.
    *
    * @return the tracsBookMinusId
    */
   public String getTracsBookMinusId()
   {
      return tracsBookMinusId;
   }

   /**
    * Get the package type.
    *
    * @return the packageType
    */
   public PackageType getPackageType()
   {
      return packageType;
   }

   /**
    * Get the discount components.
    *
    * @return the discountComponents
    */
   public List<DiscountComponent> getDiscountComponents()
   {
      return discountComponents;
   }

   /**
    * Get the Refunddetails.
    *
    * @return the Refunddetails.
    */
   public List<RefundDetails> getRefundDetails()
   {
      return refundDetails;
   }

   /**
    * Get the deposit components.
    *
    * @return the depositComponents
    */
   public List<DepositComponent> getDepositComponents()
   {
      return depositComponents;
   }

   /**
    * Gets the error message.
    *
    * @return the errorMessage
    */
   public String getErrorMessage()
   {
      return errorMessage;
   }

   /**
    * Get the callType.
    *
    * @return the callType
    */
   public String getCallType()
   {
      return callType;
   }

   /**
    * Get the non payment data.
    *
    * @return nonPaymentData.
    */
   public Map<String, String> getNonPaymentData()
   {
      return nonPaymentData;
   }

   /**
    * Get the payable amount.
    *
    * @return the payableAmount.
    */
   public Money getPayableAmount()
   {
      return payableAmount;
   }

   /**
    * Gets the payment type.
    *
    * @return the paymentType
    */
   public Map<String, List<PaymentType>> getPaymentType()
   {
      return paymentType;
   }

   /**
    * Gets the no of transactions.
    *
    * @return the noOfTransactions
    */
   public Integer getNoOfTransactions()
   {
      return noOfTransactions;
   }

   /**
    * Get the searchType like free kids, late search, etc.
    *
    * @return the searchType the searchType.
    */
   public String getSearchType()
   {
      return searchType;
   }

   /**
    * Gets the max card charge percent.
    *
    * @return the maxCardChargePercent
    *
    * @deprecated as MaxCardChargePercent is not required.
    */
   public BigDecimal getMaxCardChargePercent()
   {
      return maxCardChargePercent;
   }

   /**
    * Gets the max card charge.
    *
    * @return the maxCardCharge
    *
    * @deprecated as this has been made configurable now.
    */
   public BigDecimal getMaxCardCharge()
   {
      return maxCardCharge;
   }

   /**
    * Get the abta details.
    *
    * @return the abtaDetails
    */
   public AbtaDetails getAbtaDetails()
   {
      return abtaDetails;
   }

   /**
    * Get the shop details.
    *
    * @return shopDetails
    */
   public ShopDetails getShopDetails()
   {
      return shopDetails;
   }

   /**
    * Get the Booking details.
    *
    * @return bookDetails
    */
   public BookingData getBookingData()
   {
      return bookingData;
   }

   /**
    * Gets the intellitracker string value.
    *
    * @return the intellitracker string.
    */
   public IntelliTrackerSnippet getIntelliTrackSnippet()
   {
      return intelliTrackSnippet;
   }

   /**
    * Gets the card charges map.
    *
    * @return the cardChargesMap.
    *
    * @deprecated not required as this has been made configurable.
    */
   public Map<String, BigDecimal> getCardChargesMap()
   {
      return cardChargesMap;
   }

   /**
    * Gets the termsAndCondition.
    *
    * @return the termsAndCondition.
    */
   public TermsAndCondition getTermsAndCondition()
   {
      return termsAndCondition;
   }

   /**
    * Gets the map of extra Facilities.
    *
    * @return the extraFacilities
    */
   public Map<String, List<String>> getExtraFacilities()
   {
      return extraFacilities;
   }

   /**
    * Gets the data-cash client account(virtual terminal id).
    *
    * @return the paymentGatewayVirtualTerminalId.
    */
   public String getPaymentGatewayVirtualTerminalId()
   {
      return paymentGatewayVirtualTerminalId;
   }

   /**
    * Gets the list of rooms.
    *
    * @return the rooms
    */
   public List<Room> getRooms()
   {
      return rooms;
   }

   /**
    * To get the priceStatus boolean value.
    *
    * @return priceStatus.
    */
   public Boolean getPriceStatus()
   {
      return priceStatus;
   }

   /**
    * To get the shopId and vision reference number.
    *
    * @return shop/vision reference number.
    */
   public String getRetailBookingReference()
   {
      return retailBookingReference;
   }

   /**
    * Set the passengerRoomSummary.
    *
    * @param passengerRoomSummary the passengerRoomSummary to set
    */
   public void setPassengerRoomSummary(Map<Integer, List<PassengerSummary>> passengerRoomSummary)
   {
      this.passengerRoomSummary = passengerRoomSummary;
   }

   /**
    * Gets the session time out time.
    *
    * @return the sessionTimeOut
    */
   public int getSessionTimeOut()
   {
      return sessionTimeOut;
   }

   /**
    * Sets the session time out.
    *
    * @param sessionTimeOut the sessionTimeOut to set
    */
   public void setSessionTimeOut(int sessionTimeOut)
   {
      this.sessionTimeOut = sessionTimeOut;
   }

   /**
    * Gets the session time out alert time.
    *
    * @return the sessionTimeAlert
    */
   public int getSessionTimeAlert()
   {
      return sessionTimeAlert;
   }

   /**
    * Sets the session time alert.
    *
    * @param sessionTimeAlert the sessionTimeAlert to set
    */
   public void setSessionTimeAlert(int sessionTimeAlert)
   {
      this.sessionTimeAlert = sessionTimeAlert;
   }

   /**
    * Set the booking details.
    *
    * @param bookingData the bookingData to set
    */
   public void setBookingData(BookingData bookingData)
   {
      this.bookingData = bookingData;
   }

   /**
    * Set the shop details.
    *
    * @param shopDetails the shopDetails to set
    */
   public void setShopDetails(ShopDetails shopDetails)
   {
      this.shopDetails = shopDetails;
   }

   /**
    * Sets the error message.
    *
    * @param errorMessage the errorMessage to set
    */
   public void setErrorMessage(String errorMessage)
   {
      this.errorMessage = errorMessage;
   }

   /**
    * Set the price components.
    *
    * @param priceComponents the priceComponents to set
    */
   public void setPriceComponents(List<PriceComponent> priceComponents)
   {
      this.priceComponents = priceComponents;
   }

   /**
    * Set the refundDetails.
    *
    * @param refundDetails the refundDetails to set
    */
   public void setRefundDetails(List<RefundDetails> refundDetails)
   {
      this.refundDetails = refundDetails;
   }

   /**
    * Set the discount components.
    *
    * @param discountComponents the discountComponents to set
    */
   public void setDiscountComponents(List<DiscountComponent> discountComponents)
   {
      this.discountComponents = discountComponents;
   }

   /**
    * Set the deposit components.
    *
    * @param depositComponents the depositComponents to set
    */
   public void setDepositComponents(List<DepositComponent> depositComponents)
   {
      this.depositComponents = depositComponents;
   }

   /**
    * Set the country map.
    *
    * @param countryMap the countryMap to set
    */
   public void setCountryMap(Map<String, String> countryMap)
   {
      this.countryMap = countryMap;
   }

   /**
    * Set the callType.
    *
    * @param callType the callType to set
    */
   public void setCallType(String callType)
   {
      this.callType = callType;
   }

   /**
    * Set the flightSummary.
    *
    * @param flightSummary the flightSummary to set
    */
   public void setFlightSummary(FlightSummary flightSummary)
   {
      this.flightSummary = flightSummary;
   }

   /**
    * Set the cruiseSummary.
    *
    * @param cruiseSummary the cruiseSummary to set
    */
   public void setCruiseSummary(CruiseSummary cruiseSummary)
   {
      this.cruiseSummary = cruiseSummary;
   }

   /**
    * Set the accommodationSummary.
    *
    * @param accommodationSummary the accommodationSummary to set
    */
   public void setAccommodationSummary(AccommodationSummary accommodationSummary)
   {
      this.accommodationSummary = accommodationSummary;
   }

   /**
    * Set the non payment data.
    *
    * @param nonPaymentData the non payment data.
    */
   public void setNonPaymentData(Map<String, String> nonPaymentData)
   {
      this.nonPaymentData = nonPaymentData;
   }

   /**
    * Set the source of business.
    *
    * @param sourceOfBusiness the sourceOfBusiness to set
    */
   public void setSourceOfBusiness(SourceOfBusiness sourceOfBusiness)
   {
      this.sourceOfBusiness = sourceOfBusiness;
   }

   /**
    * Set the important information.
    *
    * @param importantInformation the importantInformation to set
    */
   public void setImportantInformation(ImportantInformation importantInformation)
   {
      this.importantInformation = importantInformation;
   }

   /**
    * Set the transaction amount.
    *
    * @param payableAmount the payableAmount to set
    */
   public void setPayableAmount(Money payableAmount)
   {
      this.payableAmount = payableAmount;
   }

   /**
    * Sets the no of transactions.
    *
    * @param noOfTransactions the noOfTransactions to set
    */
   public void setNoOfTransactions(Integer noOfTransactions)
   {
      this.noOfTransactions = noOfTransactions;
   }

   /**
    * Sets the payment type.
    *
    * @param paymentType the paymentType to set
    */
   public void setPaymentType(Map<String, List<PaymentType>> paymentType)
   {
      this.paymentType = paymentType;
   }

   /**
    * Sets the max card charge.
    *
    * @param maxCardCharge the maxCardCharge to set
    *
    * @deprecated not required as this data is made configurable.
    */
   public void setMaxCardCharge(BigDecimal maxCardCharge)
   {
      this.maxCardCharge = maxCardCharge;
   }

   /**
    * Sets the max card charge percent.
    *
    * @param maxCardChargePercent the maxCardChargePercent to set
    *
    * @deprecated as MaxCardChargePercent is not required.
    */
   public void setMaxCardChargePercent(BigDecimal maxCardChargePercent)
   {
      this.maxCardChargePercent = maxCardChargePercent;
   }

   /**
    * Sets the searchType like free kids, late search, etc.
    *
    * @param searchType the searchType.
    */
   public void setSearchType(String searchType)
   {
      this.searchType = searchType;
   }

   /**
    * Set the tracs BookMinusId.
    *
    * @param tracsBookMinusId the tracsBookMinusId to set
    */
   public void setTracsBookMinusId(String tracsBookMinusId)
   {
      this.tracsBookMinusId = tracsBookMinusId;
   }

   /**
    * Set the package type.
    *
    * @param packageType the packageType to set
    */
   public void setPackageType(PackageType packageType)
   {
      this.packageType = packageType;
   }

   /**
    * Set the abta details.
    *
    * @param abtaDetails the abtaDetails to set
    */
   public void setAbtaDetails(AbtaDetails abtaDetails)
   {
      this.abtaDetails = abtaDetails;
   }

   /**
    * Set the Intellitracker details.
    *
    * @param intelliTrackString the Intellitracker to set
    * @param prevPage the previous page name.
    */
   public void setIntelliTrackSnippet(String intelliTrackString, String prevPage)
   {
      intelliTrackSnippet = new IntelliTrackerSnippet(intelliTrackString, prevPage);
   }

   /**
    * Sets the card charges map.
    *
    * @param cardChargesMap to set.
    *
    * @deprecated not required to be sent from client application as this has been made configurable
    */
   public void setCardChargesMap(Map<String, BigDecimal> cardChargesMap)
   {
      this.cardChargesMap = cardChargesMap;
   }

   /**
    * Sets the termsAndCondition.
    *
    * @param termsAndCondition the termsAndCondition to set.
    */
   public void setTermsAndCondition(TermsAndCondition termsAndCondition)
   {
      this.termsAndCondition = termsAndCondition;
   }

   /**
    * Sets the extra Facilities.
    *
    * @param extraFacilities the extraFacilities to set
    */
   public void setExtraFacilities(Map<String, List<String>> extraFacilities)
   {
      this.extraFacilities = extraFacilities;
   }

   /**
    * gets the paymentFailureURL.
    *
    * @return the paymentFailureURL
    */
   public String getPaymentFailureURL()
   {
      // TODO: Remove this once, all the brands send payment failure URL as
      // absolute URL.
      if (StringUtils.isBlank(paymentFailureURL) || paymentFailureURL.contains(HTTP_PROTOCOL)
         || paymentFailureURL.contains(HTTPS_PROTOCOL))
      {
         return paymentFailureURL;
      }
      else
      {
         return clientDomainURL + paymentFailureURL;
      }
   }

   /**
    * sets the paymentFailureURL.
    *
    * @param paymentFailureURL the paymentFailureURL to set
    */
   public void setPaymentFailureURL(String paymentFailureURL)
   {
      this.paymentFailureURL = paymentFailureURL;
   }

   /**
    * Gets the DatacashEnabled status.
    *
    * @return datacashEnabled status.
    */
   public boolean getDatacashEnabled()
   {
      return datacashEnabled;
   }

   /**
    * Sets the DatacashEnabled status.
    *
    * @param datacashEnabled the datacashEnabled status.
    */
   public void setDatacashEnabled(boolean datacashEnabled)
   {
      this.datacashEnabled = datacashEnabled;
   }

   /**
    * gets the companyCode.
    *
    * @return the companyCode.
    */
   public String getCompanyCode()
   {
      return companyCode;
   }

   /**
    * Sets the companyCode .
    *
    * @param companyCode the companyCode to set.
    */
   public void setCompanyCode(String companyCode)
   {
      this.companyCode = companyCode;
   }

   /**
    * Sets the data-cash client account(virtual terminal id).
    *
    * @param paymentGatewayVirtualTerminalId the data-cash client account(virtual terminal id).
    */
   public void setPaymentGatewayVirtualTerminalId(String paymentGatewayVirtualTerminalId)
   {
      this.paymentGatewayVirtualTerminalId = paymentGatewayVirtualTerminalId;
   }

   /**
    * Sets the list of rooms .
    *
    * @param rooms the rooms to set.
    */
   public void setRooms(List<Room> rooms)
   {
      this.rooms = rooms;
   }

   /**
    * Gets the pre payment URL.
    *
    * @return the prePaymentUrl.
    */
   public String getPrePaymentUrl()
   {
      // Since Quintessence has not agreed to send absolute url, following
      // logic is added.
      // It is not good to have these kind of logic in CPS.
      // TODO: Once SYS agrees to send absolute url, this logic needs to be
      // removed.
      if (StringUtils.isBlank(prePaymentUrl) || prePaymentUrl.contains(HTTP_PROTOCOL)
         || prePaymentUrl.contains(HTTPS_PROTOCOL))
      {
         return prePaymentUrl;
      }
      else
      {
         return clientDomainURL + prePaymentUrl;
      }
   }

   /**
    * Sets the pre payment URL.
    *
    * @param prePaymentUrl the prePaymentUrl to set.
    */
   public void setPrePaymentUrl(String prePaymentUrl)
   {
      this.prePaymentUrl = prePaymentUrl;
   }

   /**
    * Gets the payment success URL.
    *
    * @return the paymentSuccessUrl.
    */
   public String getPaymentSuccessUrl()
   {
      // Since Quintessence has not agreed to send absolute url, following
      // logic is added.
      // It is not good to have these kind of logic in CPS.
      // TODO: Once SYS agrees to send absolute url, this logic needs to be
      // removed.
      if (StringUtils.isBlank(paymentSuccessUrl) || paymentSuccessUrl.contains(HTTP_PROTOCOL)
         || paymentSuccessUrl.contains(HTTPS_PROTOCOL))
      {
         return paymentSuccessUrl;
      }
      else
      {
         return clientDomainURL + paymentSuccessUrl;
      }
   }

   /**
    * Sets the payment success URL.
    *
    * @param paymentSuccessUrl the paymentSuccessUrl to set
    */
   public void setPaymentSuccessUrl(String paymentSuccessUrl)
   {
      this.paymentSuccessUrl = paymentSuccessUrl;
   }

   /**
    * Gets the noRedirect attribute.
    *
    * @return the noRedirect.
    */
   public Boolean getNoRedirect()
   {
      return noRedirect;
   }

   /**
    * Sets the noRedirect attribute.
    *
    * @param noRedirect the noRedirect to set.
    */
   public void setNoRedirect(Boolean noRedirect)
   {
      this.noRedirect = noRedirect;
   }

   /**
    * Gets the pricing details map.
    *
    * @return pricingDetails the pricing details map.
    */
   public Map<String, List<PriceComponent>> getPricingDetails()
   {
      return pricingDetails;
   }

   /**
    * Sets the pricing details map.
    *
    * @param pricingDetails the pricing details map.
    */
   public void setPricingDetails(Map<String, List<PriceComponent>> pricingDetails)
   {
      this.pricingDetails = pricingDetails;
   }

   /**
    * Gets the bread crumb trail.
    *
    * @return the breadCrumbTrail
    */
   public Map<String, String> getBreadCrumbTrail()
   {
      return breadCrumbTrail;
   }

   /**
    * Sets the bread crumb trail.
    *
    * @param breadCrumbTrail the breadCrumbTrail to set
    */
   public void setBreadCrumbTrail(Map<String, String> breadCrumbTrail)
   {
      this.breadCrumbTrail = breadCrumbTrail;
   }

   /**
    * Gets the client url links sent by client application.
    *
    * @return the clientURLLinks
    */
   public ClientURLComponent getClientURLLinks()
   {
      return clientURLLinks;
   }

   /**
    * Sets the client url links sent by client application.
    *
    * @param clientURLLinks the clientURLLinks to set
    */
   public void setClientURLLinks(ClientURLComponent clientURLLinks)
   {
      this.clientURLLinks = clientURLLinks;
   }

   /**
    * Sets the booking session identifier.
    *
    * @param bookingSessionIdentifier the bookingSessionIdentifier to set.
    */
   public void setBookingSessionIdentifier(String bookingSessionIdentifier)
   {
      this.bookingSessionIdentifier = bookingSessionIdentifier;
   }

   /**
    * Sets the contact info.
    *
    * @param contactInfo the contactInfo to set.
    */
   public void setContactInfo(ContactInfo contactInfo)
   {
      this.contactInfo = contactInfo;
   }

   /**
    * Gets the discount.
    *
    * @param discount the discount to set.
    */
   public void setDiscount(Discount discount)
   {
      this.discount = discount;
   }

   /**
    * Sets the operating brand. i.e, brand for which amendment is done in BRA/WSS.
    *
    * @param operatingBrand the sub brand to set.
    */
   public void setOperatingBrand(ClientApplication operatingBrand)
   {
      this.operatingBrand = operatingBrand;
   }

   /**
    * to set priceStatus boolean value.
    *
    * @param priceStatus boolean value.
    */
   public void setPriceStatus(Boolean priceStatus)
   {
      this.priceStatus = priceStatus;
   }

   /**
    * To set the shopId with vision reference number.
    *
    * @param retailBookingReference a string for shopid/vision reference number.
    *
    */
   public void setRetailBookingReference(String retailBookingReference)
   {
      this.retailBookingReference = retailBookingReference;
   }

   /**
    * Gets the booking session identifier.
    *
    * @return the bookingSessionIdentifier.
    */

   public String getBookingSessionIdentifier()
   {
      // Until all applications send this data, we have to have the following
      // if condition.
      if (bookingSessionIdentifier == null)
      {
         return StringUtils.EMPTY;
      }
      return bookingSessionIdentifier;
   }

   /**
    * Gets the contact info.
    *
    * @return the contactInfo.
    */
   public ContactInfo getContactInfo()
   {
      return contactInfo;
   }

   /**
    * Gets he discount.
    *
    * @return the discount.
    */
   public Discount getDiscount()
   {
      return discount;
   }

   /**
    * Gets the operating brand. i.e., the brand for which amendment is being done in BRAC/WSS.
    *
    * @return the sub brand.
    */
   public ClientApplication getOperatingBrand()
   {
      return operatingBrand;
   }

   /**
    * Gets the total Paid Till Date info.
    *
    * @return the totalPaidTillDate.
    */
   public Money getTotalPaidTillDate()
   {
      return totalPaidTillDate;
   }

   /**
    * Set the setTotalPaidTillDate.
    *
    * @param totalPaidTillDate totalPaidTillDate.
    */
   public void setTotalPaidTillDate(Money totalPaidTillDate)
   {
      this.totalPaidTillDate = totalPaidTillDate;
   }

   /**
    * Gets the amendment charge to be paid today info.
    *
    * @return the amendmentCharge.
    */
   public Money getAmendmentCharge()
   {
      return amendmentCharge;
   }

   /**
    * Set the amendment charge to be paid today.
    *
    * @param amendmentCharge the amendment charge to be set.
    */
   public void setAmendmentCharge(Money amendmentCharge)
   {
      this.amendmentCharge = amendmentCharge;
   }

   /**
    * Gets the amendment changes pending info.
    *
    * @return the amendmentChangesPending.
    */
   public Money getAmendmentChangesPending()
   {
      return amendmentChangesPending;
   }

   /**
    * Set the amendment changes pending info.
    *
    * @param amendmentChangesPending the amendment changes pending amount to be set.
    */
   public void setAmendmentChangesPending(Money amendmentChangesPending)
   {
      this.amendmentChangesPending = amendmentChangesPending;
   }

   /**
    * Gets the total amendment fee info.
    *
    * @return the totalAmendmentFee.
    */
   public Money getTotalAmendmentFee()
   {
      return totalAmendmentFee;
   }

   /**
    * Set the total amendment fee info.
    *
    * @param totalAmendmentFee the total amendment fee amount to be set.
    */
   public void setTotalAmendmentFee(Money totalAmendmentFee)
   {
      this.totalAmendmentFee = totalAmendmentFee;
   }

   /**
    * Gets the credit card fee info.
    *
    * @return the creditCardFee.
    */
   public Money getCreditCardFee()
   {
      return creditCardFee;
   }

   /**
    * Set the credit card fee info.
    *
    * @param creditCardFee the credit card fee to be set.
    */
   public void setCreditCardFee(Money creditCardFee)
   {
      this.creditCardFee = creditCardFee;
   }
   /**
    * Gets the getShellSummary.
    *
    * @return the getShellSummary.
    */
public ShellSummary getShellSummary()
{
  return shellSummary;
}
/**
 * Set the shellSummary.
 *
 * @param shellSummary  to be set.
 */
public void setShellSummary(ShellSummary shellSummary)
{
   this.shellSummary = shellSummary;
}

public boolean isMultiCentre() {
	return multiCentre;
}

public void setMultiCentre(boolean multiCentre) {
	this.multiCentre = multiCentre;
}

   /**
    * @return the lockYourPriceSummary
    */
   public LockYourPriceSummary getLockYourPriceSummary()
   {
      return lockYourPriceSummary;
   }

   /**
    * @param lockYourPriceSummary the lockYourPriceSummary to set
    */
   public void setLockYourPriceSummary(LockYourPriceSummary lockYourPriceSummary)
   {
      this.lockYourPriceSummary = lockYourPriceSummary;
   }

}
