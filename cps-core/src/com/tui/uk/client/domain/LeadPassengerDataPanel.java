/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: LeadPassengerDataPanel.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-06-22 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents all the possible known field name used in lead passenger data panel
 * in cps payment page.
 * This enum is used for validating the non payment data in lead passenger
 * data panel.
 * Enum constructor contain two parameters key and pattern.
 * Key represent field name, which is used in lead passenger section in payment page.
 * Pattern represent pattern, which is used for validating field value.
 *
 * @author roopesh.s
 */
public enum LeadPassengerDataPanel implements Serializable
{
   // CHECKSTYLE:OFF
   /** The HOUSE_NAME. */
   HOUSE_NAME("houseName","([a-zA-Z0-9]+[-,:;&()\"\'.!\\/\\s*]*[a-zA-Z0-9 \\s*]*)+"),

   /** The ADDRESS_LINE1. */
   ADDRESS_LINE1("addressLine1","([a-zA-Z0-9]+[-,:;&()\"\'.!\\/\\s*]*[a-zA-Z0-9 \\s*]*)+"),

   /** The ADDRESS_LINE2. */
   ADDRESS_LINE2("addressLine2","([a-zA-Z0-9]+[-,:;&()\"\'.!\\/\\s*]*[a-zA-Z0-9 \\s*]*)+"),

   /** The CITY. */
   CITY("city","[a-zA-Z\\'\\., \\-\\s*]+"),

   /** The COUNTY. */
   COUNTY("county","[a-zA-Z\\'\\., \\-\\s*]+"),

   /** The POST_CODE. */
   POST_CODE("postCode","^([A-Pa-pR-UWYZr-uwyz0-9][A-Ha-hK-Yk-y0-9][AEHMNPRTUVXYaehmnprtuvxy0-9]?[ABEHMNPRVWXYabehmnprvwxy0-9]?[ \\s]{0,1}[0-9][ABD-HJLN-UW-Zabd-hjln-uw-z]{2}|GIR 0AA)$"),

   /** The DAY_TIME_PHONE. */
   DAY_TIME_PHONE("dayTimePhone","^[\\(\\+?\\d][\\(\\)\\d\\-?]+[\\d\\)]$"),

   /** The MOBILE_PHONE. */
   MOBILE_PHONE("mobilePhone","^[\\(\\+?\\d][\\(\\)\\d\\-?]+[\\d\\)]$"),

   /** The EMAIL_ADDRESS. */
   EMAIL_ADDRESS("emailAddress","(.+@.+\\.[a-z]{2,3}+)"),

   /** The EMAIL_ADDRESS1. */
   EMAIL_ADDRESS1("emailAddress1","(.+@.+\\.[a-z]{2,3}+)");
   // CHECKSTYLE:ON

   /** The key is field name which is used in lead passenger section in payment page. */
   private String key;

   /** The pattern, used for validate field value. */
   private String pattern;

   /**
    * Constructor with key.
    *
    * @param key the field name, which is used in lead passenger section in payment page.
    * @param pattern the pattern, which is used for validating field value.
    */
   private LeadPassengerDataPanel(String key, String pattern)
   {
      this.key = key;
      this.pattern = pattern;
   }

   /**
    * It will give key.
    *
    * @return key
    */
   public String getKey()
   {
      return key;
   }

   /**
    * This method is responsible for populating map with the details of the
    * specified fields in the validation list.
    *
    * @param validationList the list of brand specific non payment data to be
    *       validated.
    *
    * @return the patternMap, the patternMap with all lead passenger data panel values.
    */
   public static Map<String, String> getAllPatterns(List<String> validationList)
   {
      Map<String, String> patternMap = new HashMap<String, String>();
      for (LeadPassengerDataPanel leadPassengerDataPanel : values())
      {
         if (validationList.contains(leadPassengerDataPanel.key))
         {
            patternMap.put(leadPassengerDataPanel.key, leadPassengerDataPanel.pattern);
         }
      }
      return patternMap;
   }

}
