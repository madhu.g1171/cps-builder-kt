/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentMode.java$
 *
 * $Revision: $
 *
 * $Date: Jun 11, 2008$
 *
 * Author: veena.bn
 *
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

/**
 * This represents different payment modes available in the payment page rendered by
 * Common Payment Component.Currently there are four Payment modes like Customer Not Present Mode,
 * Customer Present Mode, Chip and Pin Not Available Mode and Post Payment Guarantee Mode.
 *
 * @author veena.bn
 *
 * @since 1.0 API
 */
public enum PaymentMode
{
   /** The customer present mode. */
   CUSTOMERPRESENT("CP"),

   /** The customer not present mode. */
   CUSTOMERNOTPRESENT("CNP"),

   /** The chip and pin not available mode. */
   CHIPANDPINNOTAVAIL("CPNA"),

   /** The post payment guarantee mode. */
   POSTPAYMENTGUARANTEE("PPG");

   /** The payment mode code. */
   private final String code;

   /**
    * The Constructor for this enum.
    *
    * @param code the Payment Code
    */
   PaymentMode(String code)
   {
      this.code = code;
   }

   /**
    * Gets the payment mode code.
    *
    * @return the Payment Code
    */
   public String getCode()
   {
      return code;
   }
   /**
    * This method will return the payment mode based on the code passed.It will throw
    * IllegalArgumentException if the code is not valid.
    *
    * @param code the Payment code
    *
    * @return the payment mode.
    */
   public static PaymentMode findByCode(String code)
   {
      for (PaymentMode paymentMode : PaymentMode.values())
      {
         if (paymentMode.code.equalsIgnoreCase(code.trim()))
         {
            return paymentMode;
         }
      }
      throw new IllegalArgumentException("Unknown Payment mode:" + code);
   }
}
