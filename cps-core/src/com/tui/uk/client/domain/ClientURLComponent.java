/*
* Copyright (C)2006 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   ClientURLComponent.java$
*
* $Revision:   $
*
* $Date:   Feb 3, 2010$
*
* Author: veena.bn
*
*
* $Log:   $
*/
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class holds all the URLs that are set from client application.
 *
 * @author veena.bn
 *
 */
public class ClientURLComponent implements Serializable
{
   /** The generated serial version id. */
   private static final long serialVersionUID = -3273417321712200198L;

   /** The URL that is set from client application. */
   private String logOutURL;

   /** The home page URL that is set from client application. */
   private String homePageURL;

   /** The search results URL that is set from client application. */
   private String searchResultsURL;

   /** The accommodation details URL that is set from client application. */
   private String accomDetailsURL;

   /** The flight details URL that is set from client application. */
   private String flightDetailsURL;

   /** The travel options URL that is set from client application. */
   private String holidayOptionsURL;

   /**
    * Gets the logout url set by the client application.
    * @return the logOutURL url used for logging out in CPS.
    */
   public String getLogOutURL()
   {
      return logOutURL;
   }

   /**
    * Gets the home page url set by the client application.
    * @return the home page url used in CPS.
    */
   public String getHomePageURL()
   {
      return homePageURL;
   }

   /**
    * Gets the searchResults url set by the client application.
    * @return the searchResults url used in CPS.
    */
   public String getSearchResultsURL()
   {
      return searchResultsURL;
   }

   /**
    * Gets the accomDetails url set by the client application.
    * @return the accomDetails url used in CPS.
    */
   public String getAccomDetailsURL()
   {
      return accomDetailsURL;
   }

   /**
    * Gets the flightDetails url set by the client application.
    * @return the flightDetails url used in CPS.
    */
   public String getFlightDetailsURL()
   {
      return flightDetailsURL;
   }

   /**
    * Gets the holidayOptions url set by the client application.
    * @return the holidayOptions url used in CPS.
    */
   public String getHolidayOptionsURL()
   {
      return holidayOptionsURL;
   }

   /**
    * Sets the logout url set by the client application.
    * @param logOutURL the logOutURL to set
    */
   public void setLogOutURL(String logOutURL)
   {
      this.logOutURL = logOutURL;
   }

   /**
    * Sets the homePage url set by the client application.
    * @param homePageURL the homePageURL to set
    */
   public void setHomePageURL(String homePageURL)
   {
      this.homePageURL = homePageURL;
   }

   /**
    * Sets the searchResults url set by the client application.
    * @param searchResultsURL the searchResults to set
    */
   public void setSearchResultsURL(String searchResultsURL)
   {
      this.searchResultsURL = searchResultsURL;
   }

   /**
    * Sets the accomDetails url set by the client application.
    * @param accomDetailsURL the accomDetailsURL to set
    */
   public void setAccomDetailsURL(String accomDetailsURL)
   {
      this.accomDetailsURL = accomDetailsURL;
   }

   /**
    * Sets the flightDetails url set by the client application.
    * @param flightDetailsURL the flightDetailsURL to set
    */
   public void setFlightDetailsURL(String flightDetailsURL)
   {
      this.flightDetailsURL = flightDetailsURL;
   }

   /**
    * Sets the holidayOptions url set by the client application.
    * @param holidayOptionsURL the holidayOptionsURL to set
    */
   public void setHolidayOptionsURL(String holidayOptionsURL)
   {
      this.holidayOptionsURL = holidayOptionsURL;
   }
}
