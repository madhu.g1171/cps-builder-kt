package com.tui.uk.client.domain;

import java.io.Serializable;

public class PayPalComponent implements Cloneable, Serializable {


	private static final long serialVersionUID = 1L;
	/* vTid for PayPal requests */
	private String vTid;
	/* enablePaypal attribute decides whether PayPal component should be displayed on payment page or not */
	private boolean enablePaypal;
	/* paypalOptionActive attribute decides whether PayPal component should be visible actively or inactively */
	private boolean paypalOptionActive;
	/* payPalSetExpressCheckoutData contains request information about set_express_checkout service  */
	private PayPalSetExpressCheckoutData payPalSetExpressCheckoutData;
	/* payPalSetExpressCheckoutResponse contains set_express_checkout service response information. */
	private PayPalSetExpressCheckoutResponse payPalSetExpressCheckoutResponse;

	public String getvTid() {
		return vTid;
	}

	public void setvTid(String vTid) {
		this.vTid = vTid;
	}

	public Boolean getEnablePaypal() {
		return enablePaypal;
	}

	public void setEnablePaypal(Boolean enablePaypal) {
		this.enablePaypal = enablePaypal;
	}

	public boolean isPaypalOptionActive() {
		return paypalOptionActive;
	}

	public void setPaypalOptionActive(boolean paypalStatusActive) {
		this.paypalOptionActive = paypalStatusActive;
	}

	public PayPalSetExpressCheckoutData getPayPalExpressCheckout() {
		return payPalSetExpressCheckoutData;
	}

	public void setPayPalExpressCheckout(
			PayPalSetExpressCheckoutData payPalSetExpressCheckoutData) {
		this.payPalSetExpressCheckoutData = payPalSetExpressCheckoutData;
	}

	public PayPalSetExpressCheckoutResponse getPayPalExpressCheckoutResponse() {
		return payPalSetExpressCheckoutResponse;
	}

	public void setPayPalExpressCheckoutResponse(
			PayPalSetExpressCheckoutResponse payPalSetExpressCheckoutResponse) {
		this.payPalSetExpressCheckoutResponse = payPalSetExpressCheckoutResponse;
	}

}
