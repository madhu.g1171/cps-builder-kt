/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: ShopDetails.java$
 *
 * $Revision: $
 *
 * $Date: Aug 26, 2008$
 *
 * Author: vijayalakshmi.d
 *
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class holds all the related information of the shop such as shop name ,shop id.
 *
 * @author vijayalakshmi.d
 *
 */
public final class ShopDetails implements Serializable
{
   /** The generated serial versionId. */
   private static final long serialVersionUID = 0L;

   /** The Constant to hold shop name. */
   private String shopName;

   /** The Constant to hold shop id. */
   private String shopId;

   /** The Constant to hold retail brand. */
   private String retailBrand;

   /**
    * Constructor for shop details.
    *
    * @param shopName the shop name
    * @param shopId the shop id.
    */
   public ShopDetails(String shopName, String shopId)
   {
      this.shopName = shopName;
      this.shopId = shopId;
   }

   /**
    * Gets the shop name.
    *
    * @return the shopName
    */
   public String getShopName()
   {
      return shopName;
   }

   /**
    * Gets the shop id.
    *
    * @return the shopId
    */
   public String getShopId()
   {
      return shopId;
   }

   /**
    * Gets the retail brand.
    *
    * @return the retailBrand
    */
    public String getRetailBrand()
    {
      return retailBrand;
    }

    /**
     * Sets the retail brand .
     *
     * @param retailBrand the retailBrand to set.
     */
    public void setRetailBrand(String retailBrand)
    {
        this.retailBrand = retailBrand;
    }
}