/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PDQPaymentTransaction.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : veena.bn $
 *
 * $Log : $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Map;
import java.util.List;
import java.util.Date;

/**
 * This class gives the essential payment transaction data entered by the user
 * in the Common Payment Component, which are required by the client
 * application. Payment Type code, transaction amount, transaction reference,
 * transaction charges in each transaction are considered as essential for
 * client application to record for future reference.
 *
 * @author veena.bn
 *
 * @since 1.0 API
 */

public final class EssentialTransactionData implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4293919676289513953L;

	/** The payment type code. */
	private String paymentTypeCode;

	/** The transaction amount. */
	private Money transactionAmount;

	/** The transaction charges. */
	private Money transactionCharge;

	/** The transaction reference. */
	private String transactionReference;

	/** The <code>PaymentMethod</code>. */
	private PaymentMethod paymentMethod;

	/** The line items of the transactions. */
	private Map<String, Money> transactionLineItems;

	/** The pinpad merchant copy. */
	private String merchantCopy;

	/** The pinpad customer copy. */
	private String customerCopy;

	/** The masked card number. */
	private String maskedCardNumber;

	/** The name on card. */
	private String nameOnCard;

	/** The expire date. */
	private String expiryDate;

	/** The auth code sent by the datacash. */
	private String authCode;

	/** The 3D secure auth flag. */
	private boolean threeDSecureAuthFlag;

	/** The response code sent by the datacash. */
	private String paymentGatewayResponse;

	/** The flag is defined for direct debit usage. */
	private boolean ddSelectedByUser = false;

	/** The flag is defined for direct debit usage. */
	private List<PaymentScheduleData> paymentScheduleData;

	/** The flag is defined for paypal services. */
	private PayPalSetExpressCheckoutResponse paypalExpressCheckoutResponse;
	
	private PaymentStatus paymentStatus;
	
	private String refundReferenceId;
	
	private Date refundProcessedDate;

	public Date getRefundProcessedDate() {
		return refundProcessedDate;
	}

	public void setRefundProcessedDate(Date refundProcessedDate) {
		this.refundProcessedDate = refundProcessedDate;
	}

	public PayPalSetExpressCheckoutResponse getPaypalExpressCheckoutResponse() {
		return paypalExpressCheckoutResponse;
	}

	public void setPaypalExpressCheckoutResponse(
			PayPalSetExpressCheckoutResponse paypalExpressCheckoutResponse) {
		this.paypalExpressCheckoutResponse = paypalExpressCheckoutResponse;
	}

	public List<PaymentScheduleData> getPaymentScheduleData() {
		return paymentScheduleData;
	}

	public void setPaymentScheduleData(
			List<PaymentScheduleData> paymentScheduleData) {
		this.paymentScheduleData = paymentScheduleData;
	}

	/**
	 * The Constructor.
	 *
	 * @param transactionAmount
	 *            the transaction amount
	 * @param paymentTypeCode
	 *            the payment type code
	 * @param transactionCharge
	 *            the transaction charge
	 * @param paymentMethod
	 *            the PaymentMethod
	 */
	public EssentialTransactionData(String paymentTypeCode,
			Money transactionAmount, Money transactionCharge,
			PaymentMethod paymentMethod) {
		this.paymentTypeCode = paymentTypeCode;
		this.transactionAmount = transactionAmount;
		this.transactionCharge = transactionCharge;
		this.paymentMethod = paymentMethod;
	}

	/**
	 * Gets the payment type code.
	 *
	 * @return the paymentTypeCode
	 */
	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}

	/**
	 * Gets the transaction amount.
	 *
	 * @return the transaction amount
	 */
	public Money getTransactionAmount() {
		return transactionAmount;
	}

	/**
	 * Gets the transaction charge.
	 *
	 * @return the transactionCharge
	 */
	public Money getTransactionCharge() {
		return transactionCharge;
	}

	/**
	 * Gets the Payment Method.
	 *
	 * @return the paymentMethod.
	 */
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * Gets the transaction reference.
	 *
	 * @return the transactionReference
	 */
	public String getTransactionReference() {
		return transactionReference;
	}

	/**
	 * Gets the list of transaction line items.
	 *
	 * @return The list of transaction line items.
	 */
	public Map<String, Money> getTransactionLineItems() {
		return transactionLineItems;
	}

	/**
	 * Sets the transaction reference.
	 *
	 * @param transactionReference
	 *            the transactionReference to set
	 */
	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	/**
	 * Sets the line items of the transaction.
	 *
	 * @param transactionLineItems
	 *            The line items of the transaction.
	 */
	public void setTransactionLineItems(Map<String, Money> transactionLineItems) {
		this.transactionLineItems = transactionLineItems;
	}

	/**
	 * Gets the merchant copy associated with this Transaction.
	 *
	 * @return the merchantCopy
	 */
	public String getMerchantCopy() {
		return merchantCopy;
	}

	/**
	 * Gets the customer copy associated with this Transaction.
	 *
	 * @return the customerCopy
	 */
	public String getCustomerCopy() {
		return customerCopy;
	}

	/**
	 * Gets the masked card number.
	 *
	 * @return the masked card number.
	 */
	public String getMaskedCardNumber() {
		return maskedCardNumber;
	}

	/**
	 * Gets the name on card.
	 *
	 * @return the name on card.
	 */
	public String getNameOnCard() {
		return nameOnCard;
	}

	/**
	 * Gets the expiry date.
	 *
	 * @return the expiry date.
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * Gets the auth code.
	 *
	 * @return the auth code.
	 */
	public String getAuthCode() {
		return authCode;
	}

	/**
	 * Sets the merchant copy associated with this Transaction.
	 *
	 * @param merchantCopy
	 *            the merchant copy
	 */
	public void setMerchantCopy(String merchantCopy) {
		this.merchantCopy = merchantCopy;
	}

	/**
	 * Sets the customer copy associated with this Transaction.
	 *
	 * @param customerCopy
	 *            the customer copy
	 */
	public void setCustomerCopy(String customerCopy) {
		this.customerCopy = customerCopy;
	}

	/**
	 * Sets the masked card number.
	 *
	 * @param maskedCardNumber
	 *            the masked card number.
	 */
	public void setMaskedCardNumber(String maskedCardNumber) {
		this.maskedCardNumber = maskedCardNumber;
	}

	/**
	 * Sets the name on card.
	 *
	 * @param nameOnCard
	 *            the name on card.
	 */
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	/**
	 * Sets the expiry date.
	 *
	 * @param expiryDate
	 *            the expiry date.
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * Sets the auth code.
	 *
	 * @param authCode
	 *            the auth code.
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	/**
	 * Get the 3D secure auth flag.
	 *
	 * @return the threeDSecureAuthFlag
	 */
	public boolean getThreeDSecureAuthFlag() {
		return threeDSecureAuthFlag;
	}

	/**
	 * Set the 3D secure auth flag.
	 *
	 * @param threeDSecureAuthFlag
	 *            the threeDSecureAuthFlag to set
	 */
	public void setThreeDSecureAuthFlag(boolean threeDSecureAuthFlag) {
		this.threeDSecureAuthFlag = threeDSecureAuthFlag;
	}

	/**
	 * Set the return code from datacash.
	 *
	 * @param responseCode
	 *            the response code from datacash
	 */
	public void setPaymentGatewayResponse(final String responseCode) {
		this.paymentGatewayResponse = responseCode;
	}

	/**
	 * Returns the PaymentGateway Response code. Returns "001" for successfull
	 * payment gateway transaction
	 *
	 * @return the paymentGatewayResponsecode
	 */
	public String getPaymentGatewayResponse() {
		return paymentGatewayResponse;
	}

	public boolean getDdSelectedByUser() {
		return ddSelectedByUser;
	}

	public void setDdSelectedByUser(boolean ddSelectedByUser) {
		this.ddSelectedByUser = ddSelectedByUser;
	}
	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRefundReferenceId() {
		return refundReferenceId;
	}

	public void setRefundReferenceId(String refundReferenceId) {
		this.refundReferenceId = refundReferenceId;
	}
}
