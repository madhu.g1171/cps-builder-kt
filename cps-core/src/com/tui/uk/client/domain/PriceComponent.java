/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PriceComponent.java,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2008-06-02 07:46:25 $
 *
 * $author: thomas.pm $
 *
 * $Log : $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * This class is responsible for holding all price components to be displayed in the Price Panel of
 * the Payment page rendered by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */
public final class PriceComponent implements Serializable
{

	/** The generated serial version id. */
	private static final long serialVersionUID = -905008411877767632L;

	/** The description of the price item. */
	private String itemDescription;

	/**
	 * The list of priceComponents.This will hold any number of parent child relationship of price
	 * components.
	 */
	private List<PriceComponent> priceComponents;

	/** The quantity of the component. */
	private Integer quantity;

	/** The total amount of the item. */
	private Money amount;

	/** The date of excursion. */
	private Date excursionDate;

	/** The description of the item. */
	private String itemInfo;

	/** The online description data of the item. */
	private String onlineDiscountData;

	/** The online text data of the item. */
	private String onlineTextData;

	/** selection on payment history*/
	private boolean isPaidWithDD = false;

	/** selection on payment history*/
	private boolean isPaidWithPayPal = false;
	/** selection on Crystal SKI ancillaries offer type*/
	private String offerType;
	
	/** returns Crystal SKI ancillaries offer type*/
	public String getOfferType()
   {
      return offerType;
   }
	/** sets Crystal SKI ancillaries offer type*/
   public void setOfferType(String offerType)
   {
      this.offerType = offerType;
   }

   /**
	 * Get the excursionDate of the price item.
	 *
	 * @return excursionDate the excursionDate.
	 */
	public Date getExcursionDate()
	{
		return excursionDate;
	}

	/**
	 * Sets the excursionDate.
	 *
	 * @param excursionDate the discount to set
	 */
	public void setExcursionDate(Date excursionDate)
	{
		this.excursionDate = excursionDate;
	}

	/**
	 * This field indicates if the price component is a discount or not. If this is set to true,
	 * there should be negative sign shown before displaying the amount.
	 */
	private Boolean discount = Boolean.FALSE;

	/**
	 * This field indicates the additional price information like if the price component is free or
	 * included.
	 */
	private String additionalPriceInfo;

	/**
	 * Constructor for PriceComponent with itemDescription and amount as parameters.
	 *
	 * @param amount the amount.
	 * @param itemDescription the itemDescription.
	 */
	public PriceComponent(String itemDescription, Money amount)
	{
		this.itemDescription = itemDescription;
		this.amount = amount;
	}

	/**
	 * Get the description of the price item.
	 *
	 * @return itemDescription the itemDescription.
	 */
	public String getItemDescription()
	{
		return itemDescription;
	}

	/**
	 * Get the quantity of the component.
	 *
	 * @return quantity the Quantity.
	 */
	public Integer getQuantity()
	{
		return quantity;
	}

	/**
	 * Gets the amount of the component.
	 *
	 * @return amount the Amount.
	 */
	public Money getAmount()
	{
		return amount;
	}

	/**
	 * Get the discount of price component.
	 *
	 * @return discount the discount.
	 */
	public Boolean getDiscount()
	{
		return discount;
	}

	/**
	 * Gets the list of price components.
	 *
	 * @return priceComponents the list of priceComponents.
	 */
	public List<PriceComponent> getPriceComponents()
	{
		return priceComponents;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * Sets the discount.
	 *
	 * @param discount the discount to set
	 */
	public void setDiscount(Boolean discount)
	{
		this.discount = discount;
	}

	/**
	 * Sets the list of priceComponents.
	 *
	 * @param priceComponents the list of priceComponents to set
	 */
	public void setPriceComponents(List<PriceComponent> priceComponents)
	{
		this.priceComponents = priceComponents;
	}

	/**
	 * Gets the additional price information.
	 *
	 * @return additionalPriceInfo.
	 */
	public String getAdditionalPriceInfo()
	{
		return additionalPriceInfo;
	}

	/**
	 * Sets the additional price information.
	 *
	 * @param additionalPriceInfo the additional price information(free/included)
	 *
	 */
	public void setAdditionalPriceInfo(String additionalPriceInfo)
	{
		this.additionalPriceInfo = additionalPriceInfo;
	}

	/**
	 * Gets the item information.
	 *
	 * @return itemInfo.
	 */
	public String getItemInfo()
	{
		return itemInfo;
	}

	/**
	 * Sets the item information.
	 *
	 * @param itemInfo the price information
	 *
	 */
	public void setItemInfo(String itemInfo)
	{
		this.itemInfo = itemInfo;
	}

	/**
	 * Gets the onlineDiscountData.
	 *
	 * @return onlineDiscountData.
	 */
	public String getOnlineDiscountData()
	{
		return onlineDiscountData;
	}

	/**
	 * Sets the onlineDiscountData.
	 *
	 * @param onlineDiscountData the price information
	 *
	 */
	public void setOnlineDiscountData(String onlineDiscountData)
	{
		this.onlineDiscountData = onlineDiscountData;
	}

	/**
	 * Gets the onlineTextData.
	 *
	 * @return onlineTextData.
	 */
	public String getOnlineTextData()
	{
		return onlineTextData;
	}

	/**
	 * Sets the onlineTextData.
	 *
	 * @param onlineTextData the price information
	 *
	 */
	public void setOnlineTextData(String onlineTextData)
	{
		this.onlineTextData = onlineTextData;
	}
	/** isPaidWithDD*/
	public boolean isPaidWithDD() {
		return isPaidWithDD;
	}
	/** PaidWithDD selection*/
	public void setPaidWithDD(boolean isPaidWithDD) {
		this.isPaidWithDD = isPaidWithDD;
	}
	/** PaidWithPayPal*/
	public boolean isPaidWithPayPal() {
		return isPaidWithPayPal;
	}
	/** PaidWithPayPal selection*/
	public void setPaidWithPayPal(boolean isPaidWithPayPal) {
		this.isPaidWithPayPal = isPaidWithPayPal;
	}
}