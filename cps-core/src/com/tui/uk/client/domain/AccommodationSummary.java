/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AccommodationSummary.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 07:37:45 $
 *
 * $author: thomas.pm $
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * This represents AccommodationSummary object of Common Payment
 * Component(CPC). This object includes all accommodation related details
 * which are necessary to display accommodation panel in Payment Page rendered by CPC.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */

public final class AccommodationSummary implements Serializable
{

	/** Genetered serial version iD. */
	private static final long serialVersionUID = -7761766606086273328L;

	/** This object holds the details of the accommodation. */
	private Accommodation accommodation;

   /** The duration of accommodation. */
   private int duration;

   /** The accommodation inventory system. */
   private String accommodationInventorySystem;

   /** Is accommodation selected. */
   private Boolean accommodationSelected;

   /** Holds the value whether an accommodation is pre pay or not. */
   private Boolean prepay;

   /** Holds the value whether hopla accommodation is bonded or not. */
   private String hoplaBonded;

   /** The additionalInfo of accommodation. */
   private String additionalInfo;

   /** The additional details associated with the accommodation.The key will
    *  be the additional detail and the value will be the URL or the content associated with it.
    *  The additional benefits associated with the accommodation can also go as part of this.
    *  For ex:Key will be Family Resort Benefits and value will be the content.  */
   private Map<String, String> additionalDetails;

   /** The displayed caption. Used for FLY DRIVE accommodation types. */
   private String caption;

   /** URI for the accommodation details link. */
   private String accommodationDetailsUri;

   /** URI for the a-z booking details link. */
   private String aToZUri;

   /** The start date of accommodation. */
   private Date startDate;

   /** The end date of accommodation. */
   private Date endDate;

   /** The unique id associated with each accommodation. */
   private String uniqueId;

   /** The list of accommodation extras. */
   private List<String> accommodationExtras;

   /** The number of rooms pertaining to the accommodation. */
   private int numberOfRooms;

   /**
    * Constructor for AccommodationSummary.
    *
    * @param accommodationInventorySystem the accommodation inventory
    *           system.
    * @param accommodation object holding the details of the accommodation.
    * @param accommodationSelected boolean.
    * @param duration the duration.
    */
   public AccommodationSummary(Accommodation accommodation, int duration,
                               Boolean accommodationSelected,
                               String accommodationInventorySystem)
   {
      this.accommodation = accommodation;
      this.duration = duration;
      this.accommodationSelected = accommodationSelected;
      this.accommodationInventorySystem = accommodationInventorySystem;
   }

   /**
    * Get the accommodationSelected.
    *
    * @return the accommodationSelected
    */
   public Boolean getAccommodationSelected()
   {
      return accommodationSelected;
   }

   /**
    * Get the accommodation details.
    *
    * @return the accommodation.
    */
   public Accommodation getAccommodation()
   {
      return accommodation;
   }

   /**
    * Get the number of nights of accommodation.
    *
    * @return the duration.
    */
   public int getDuration()
   {
      return duration;
   }

   /**
    * Get the accommodation inventory system.
    *
    * @return the accommodationInventorySystem.
    */
   public String getAccommodationInventorySystem()
   {
      return accommodationInventorySystem;
   }

   /**
    * Get the caption of accommodation.
    *
    * @return the caption.
    */
   public String getCaption()
   {
      return caption;
   }

   /**
    * Get the accommodationDetailsUri for accommodation.
    *
    * @return the accommodationDetailsUri.
    */
   public String getAccommodationDetailsUri()
   {
      return accommodationDetailsUri;
   }

   /**
    * Get the aToZUri for accommodation.
    *
    * @return the aToZUri.
    */
   public String getAToZUri()
   {
      return aToZUri;
   }

   /**
    * Gets the is prepay.
    *
    * @return the is prepay
    */
   public Boolean getPrepay()
   {
      return prepay;
   }

   /**
    * Gets the hoplaBonded.
    *
    * @return the hoplaBonded
    */
   public String getHoplaBonded()
   {
      return hoplaBonded;
   }

   /**
    * Gets the additional info.
    *
    * @return the additional info
    */
   public String getAdditionalInfo()
   {
      return additionalInfo;
   }

   /**
    * Gets the start date.
    *
    * @return the start date
    */
   public Date getStartDate()
   {
      if (startDate != null)
      {
         return new Date(startDate.getTime());
      }
      else
      {
         return null;
      }
   }

   /**
    * Gets the end date.
    *
    * @return the end date
    */
   public Date getEndDate()
   {
      if (endDate != null)
      {
         return new Date(endDate.getTime());
      }
      else
      {
         return null;
      }
   }

   /**
    * Gets the uniqueId.
    *
    * @return the uniqueId.
    */
   public String getUniqueId()
   {
      return uniqueId;
   }

   /**
    * Gets the accommodation extras.
    *
    * @return the accommodationExtras
    */
   public List<String> getAccommodationExtras()
   {
      return accommodationExtras;
   }

   /**
    * Gets the number of rooms for the accommodation.
    *
    * @return the numberOfRooms number of rooms in the accommodation.
    */
   public int getNumberOfRooms()
   {
      return numberOfRooms;
   }

   /**
    * Gets the list of additional details.
    *
    * @return additionalDetails the list of additionalDetails.
    */
   public Map<String, String> getAdditionalDetails()
   {
      return additionalDetails;
   }

   /**
    * Set the accommodationDetailsUri for accommodation.
    *
    * @param accommodationDetailsUri the accommodationDetailsUri to set.
    */
   public void setAccommodationDetailsUri(String accommodationDetailsUri)
   {
      this.accommodationDetailsUri = accommodationDetailsUri;
   }

   /**
    * Set the aToZUri for accommodation.
    *
    * @param aToZUri the aToZUri to set.
    */
   public void setAToZUri(String aToZUri)
   {
      this.aToZUri = aToZUri;
   }

   /**
    * Sets the is prepay.
    *
    * @param prepay the is prepay
    */
   public void setPrepay(Boolean prepay)
   {
      this.prepay = prepay;
   }

   /**
    * Sets whether hopla is Bonded or not.
    * Because of Xml-RPC restrictions, non primitive objects other than String
    * set as NULL from clients can not be retrieved as NULL.
    * So to support backward compatibility we need to have a workaround
    * solution like below to convert Boolean to String while setting.
    *
    * @param hoplaBonded the hoplaBonded
    */
   public void setHoplaBonded(Boolean hoplaBonded)
   {
      this.hoplaBonded = String.valueOf(hoplaBonded);
   }

   /**
    * Sets the additional info.
    *
    * @param additionalInfo the additional info
    */
   public void setAdditionalInfo(String additionalInfo)
   {
      this.additionalInfo = additionalInfo;
   }

   /**
    * Sets the caption.
    *
    * @param caption the caption
    */
   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   /**
    * Sets the end date.
    *
    * @param endDate the end date
    */
   public void setEndDate(Date endDate)
   {
      this.endDate = new Date(endDate.getTime());
   }

   /**
    * Sets the start date.
    *
    * @param startDate the start date
    */
   public void setStartDate(Date startDate)
   {
      this.startDate = new Date(startDate.getTime());
   }

   /**
    * Sets the uniqueId.
    *
    * @param uniqueId the uniqueId.
    */
   public void setUniqueId(String uniqueId)
   {
      this.uniqueId = uniqueId;
   }

   /**
    * Sets the accommodation extras.
    *
    * @param accommodationExtras the accommodationExtras to set
    */
   public void setAccommodationExtras(List<String> accommodationExtras)
   {
      this.accommodationExtras = accommodationExtras;
   }

   /**
    * Sets the number of rooms for the accommodation.
    *
    * @param numberOfRooms the numberOfRooms to set
    */
   public void setNumberOfRooms(int numberOfRooms)
   {
      this.numberOfRooms = numberOfRooms;
   }

   /**
    * Sets the list of additional details.
    *
    * @param additionalDetails the additionalDetails to set
    */
   public void setAdditionalDetails(Map<String, String> additionalDetails)
   {
      this.additionalDetails = additionalDetails;
   }

}
