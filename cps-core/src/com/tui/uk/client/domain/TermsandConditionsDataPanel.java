/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: TermsandConditionsDataPanel.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2009-06-22 08:25:26 $
 * 
 * $Author: roopesh.s@sonata-software.com $
 * 
 * 
 * $Log: $.
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents all the possible known field name used in terms and conditions 
 * data panel in cps payment page.
 * This enum is used for validating the non payment data in important 
 * information data panel.
 * Enum constructor contain two parameters key and pattern. 
 * Key represent field name, which is used in terms and conditions section in payment page. 
 * Pattern represent pattern, which is used for validating field value.
 * 
 * @author roopesh.s
 */
public enum TermsandConditionsDataPanel implements Serializable
{
   /** The TOUR_OPERATOR_TERMS_ACCEPTED. */
   TOUR_OPERATOR_TERMS_ACCEPTED("tourOperatorTermsAccepted", "on");

   /** The key is field name which is used in terms and conditions section in payment page. */
   private String key;
   
   /** The pattern, used for validate field value. */
   private String pattern;
   
   /**
    * Constructor with key.
    * 
    * @param key the field name, which is used in terms and conditions section in payment page.
    * @param pattern the pattern, which is used for validating field value.
    */
   TermsandConditionsDataPanel(String key, String pattern)
   {
      this.key = key;
      this.pattern = pattern;
   }
   
   /**
    * It will give key.
    * @return key
    */
   public String getKey()
   {
      return key;
   }
   
   /**
    * This method is responsible for populating map with the details of the 
    * specified fields in the validation list.
    * 
    * @param validationList the list of brand specific non payment data to be 
    *       validated. 
    *
    * @return the patternMap, the patternMap with all terms and conditions data panel values.
    */   
   public static Map<String, String> getAllPatterns(List<String> validationList)
   {
      Map<String, String> patternMap = new HashMap<String, String>();
      for (TermsandConditionsDataPanel termsandConditionsDataPanel : values())
      {
         if (validationList.contains(termsandConditionsDataPanel.key))
         {
            patternMap.put(termsandConditionsDataPanel.key, termsandConditionsDataPanel.pattern);
         }
      }
      return patternMap;
   }
}
