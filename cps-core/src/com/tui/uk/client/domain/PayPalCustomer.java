/**
 * 
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class holds the data required for canceling a PayPal Transaction
 * @author tanveer.bp
 *
 */
public class PayPalCustomer implements Serializable{

	private String transactionMethod ;
	private String datacashReference ;

	/** The generated serial version id. */
	private static final long serialVersionUID = -3215491945522541807L;
	
	public PayPalCustomer(String transactionMethod, String datacashReference){

		this.transactionMethod = transactionMethod;
		this.datacashReference = datacashReference;
	}

	/**
	 * @return the transactionMethod
	 */
	public String getTransactionMethod() {
		return transactionMethod;
	}

	/**
	 * @return the datacashReference
	 */
	public String getDatacashReference() {
		return datacashReference;
	}
	
}
