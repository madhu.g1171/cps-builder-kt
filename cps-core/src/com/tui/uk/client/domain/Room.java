package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This represents Room object of Common Payment Service(CPS). It
 * includes necessary Room details which are essential to display passenger
 * details section in Payment Page rendered by CPS.
 *
 * @author roopesh.s
 *
 * @since 01.03.08 API
 *
 */

public final class Room implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = 2142044867374994417L;

   /** The room id or room number. */
   private int roomId;

   /** The room description. */
   private String roomDescription;

   /**
    * Gets the room id or number.
    *
    * @return the roomId
    */
   public int getRoomId()
   {
      return roomId;
   }

   /**
    * Gets the room description.
    *
    * @return the roomDescription
    */
   public String getRoomDescription()
   {
      return roomDescription;
   }

   /**
    * Sets roomeId field.
    *
    * @param roomId the roomId to set.
    */
   public void setRoomId(int roomId)
   {
      this.roomId = roomId;
   }

   /**
    * Sets roomDescription field.
    *
    * @param roomDescription the roomDescription to set.
    */
   public void setRoomDescription(String roomDescription)
   {
      this.roomDescription = roomDescription;
   }

}
