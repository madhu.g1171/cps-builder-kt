package com.tui.uk.client.domain;

import java.io.Serializable;

public class PayPalSetExpressCheckoutResponse implements Cloneable,
		Serializable {
	private static final long serialVersionUID = -10L;
	private String acknowledgement;
	private String build;
	private String correlationId;
	private String timestamp;
	private String token;
	private String PayPalVersion;
	private String datacashReference;
	private String status;
	private String reason;
	private String merchantReference;

	public String getAcknowledgement() {
		return acknowledgement;
	}

	public void setAcknowledgement(String acknowledgement) {
		this.acknowledgement = acknowledgement;
	}

	public String getBuild() {
		return build;
	}

	public void setBuild(String build) {
		this.build = build;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPayPalVersion() {
		return PayPalVersion;
	}

	public void setPayPalVersion(String payPalVersion) {
		PayPalVersion = payPalVersion;
	}

	public String getDatacashReference() {
		return datacashReference;
	}

	public void setDatacashReference(String datacashReference) {
		this.datacashReference = datacashReference;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMerchantReference() {
		return merchantReference;
	}

	public void setMerchantReference(String merchantReference) {
		this.merchantReference = merchantReference;
	}

}
