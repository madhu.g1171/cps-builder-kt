package com.tui.uk.client.domain;

import java.io.Serializable;


public class PayPalCaptureRequest implements Cloneable, Serializable {

	private static final long serialVersionUID = 317685589755433369L;

	private String transactionMethod;
	private String datacashReferance;
	private String isCompleted;
	private String invnum;
	private String messageSubmissionId;
	private String noteText;
	private String softDescriptor;
	private String amount;
	private AirlineItenaryData AirlineItenaryData;
	private String CountryCode;
	private String customerEmailId;
	private String firstname;
	private String lastname;
	private String middlename;
	private String payerId;
	private String payerStatus;
	private String paymentStatus;
	private String paymentType;
	private String bookingReferenceNumber;

	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransactionMethod() {
		return transactionMethod;
	}
	public void setTransactionMethod(String transactionMethod) {
		this.transactionMethod = transactionMethod;
	}
	public String getDatacashReferance() {
		return datacashReferance;
	}
	public void setDatacashReferance(String datacashReferance) {
		this.datacashReferance = datacashReferance;
	}
	public String getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}
	public String getInvnum() {
		return invnum;
	}
	public void setInvnum(String invnum) {
		this.invnum = invnum;
	}
	public String getMessageSubmissionId() {
		return messageSubmissionId;
	}
	public void setMessageSubmissionId(String messageSubmissionId) {
		this.messageSubmissionId = messageSubmissionId;
	}
	public String getNoteText() {
		return noteText;
	}
	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}
	public String getSoftDescriptor() {
		return softDescriptor;
	}
	public void setSoftDescriptor(String softDescriptor) {
		this.softDescriptor = softDescriptor;
	}
	public AirlineItenaryData getAirlineItenaryData() {
		return AirlineItenaryData;
	}
	public void setAirlineItenaryData(AirlineItenaryData airlineItenaryData) {
		AirlineItenaryData = airlineItenaryData;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return CountryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	/**
	 * @return the customerEmailId
	 */
	public String getCustomerEmailId() {
		return customerEmailId;
	}
	/**
	 * @param customerEmailId the customerEmailId to set
	 */
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the middlename
	 */
	public String getMiddlename() {
		return middlename;
	}
	/**
	 * @param middlename the middlename to set
	 */
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	/**
	 * @return the payerId
	 */
	public String getPayerId() {
		return payerId;
	}
	/**
	 * @param payerId the payerId to set
	 */
	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	/**
	 * @return the payerStatus
	 */
	public String getPayerStatus() {
		return payerStatus;
	}
	/**
	 * @param payerStatus the payerStatus to set
	 */
	public void setPayerStatus(String payerStatus) {
		this.payerStatus = payerStatus;
	}
	/**
	 * @return the paymentStatus
	 */
	public String getPaymentStatus() {
		return paymentStatus;
	}
	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the bookingReferenceNumber
	 */
	public String getBookingReferenceNumber() {
		return bookingReferenceNumber;
	}
	/**
	 * @param bookingReferenceNumber the bookingReferenceNumber to set
	 */
	public void setBookingReferenceNumber(String bookingReferenceNumber) {
		this.bookingReferenceNumber = bookingReferenceNumber;
	}




}
