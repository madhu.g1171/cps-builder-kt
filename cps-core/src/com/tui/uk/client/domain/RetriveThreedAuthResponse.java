/**
 * 
 */
package com.tui.uk.client.domain;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;

import com.datacash.util.XMLDocument;

/**
 * The class is used for mapping Retrieve Authentication Status.
 * 
 * @author sreenivasulu.k
 *
 */
public class RetriveThreedAuthResponse {

	private String acsEci = StringUtils.EMPTY;
	private String acsTransactionId = StringUtils.EMPTY;
	private String authenticationStatus = StringUtils.EMPTY;
	private String authentication_token = StringUtils.EMPTY;
	private String authenticationVersion = StringUtils.EMPTY;
	private String dsTransactionId = StringUtils.EMPTY;
	private String redirectHtml = StringUtils.EMPTY;
	private String result = StringUtils.EMPTY;
	private String transactionId = StringUtils.EMPTY;
	private String transactionStatus = StringUtils.EMPTY;
	private String dsReference = StringUtils.EMPTY;
	private String merchantreference = StringUtils.EMPTY;
	private String status = StringUtils.EMPTY;
	private String reason = StringUtils.EMPTY;
	private String mode = StringUtils.EMPTY;
	private String time = StringUtils.EMPTY;

	public RetriveThreedAuthResponse(XMLDocument payeeAuthResponse) {

		if (Objects.nonNull(payeeAuthResponse)) {
			this.acsEci = payeeAuthResponse.get("Response.Emv3dsTxn.acs_eci");
			this.acsTransactionId = payeeAuthResponse
					.get("Response.Emv3dsTxn.acs_transaction_id");
			this.authenticationStatus = payeeAuthResponse
					.get("Response.Emv3dsTxn.authentication_status");
			this.authentication_token = payeeAuthResponse
					.get("Response.Emv3dsTxn.authentication_token");
			this.authenticationVersion = payeeAuthResponse
					.get("Response.Emv3dsTxn.authentication_version");
			this.dsTransactionId = payeeAuthResponse
					.get("Response.Emv3dsTxn.ds_transaction_id");
			this.redirectHtml = payeeAuthResponse
					.get("Response.Emv3dsTxn.redirect_html");
			this.result = payeeAuthResponse.get("Response.Emv3dsTxn.result");
			this.transactionId = payeeAuthResponse
					.get("Response.Emv3dsTxn.transaction_id");
			this.transactionStatus = payeeAuthResponse
					.get("Response.Emv3dsTxn.transaction_status");
			this.transactionStatus = payeeAuthResponse
					.get("Response.Emv3dsTxn.acs_eci");

			this.dsReference = payeeAuthResponse
					.get("Response.datacash_reference");
			this.merchantreference = payeeAuthResponse
					.get("Response.merchantreference");
			this.status = payeeAuthResponse.get("Response.status");
			this.reason = payeeAuthResponse.get("Response.reason");
			this.mode = payeeAuthResponse.get("Response.mode");
			this.time = payeeAuthResponse.get("Response.time");
		}

	}

	public String getAcsEci() {
		return acsEci;
	}

	public void setAcsEci(String acsEci) {
		this.acsEci = acsEci;
	}

	public String getAcsTransactionId() {
		return acsTransactionId;
	}

	public void setAcsTransactionId(String acsTransactionId) {
		this.acsTransactionId = acsTransactionId;
	}

	public String getAuthenticationStatus() {
		return authenticationStatus;
	}

	public void setAuthenticationStatus(String authenticationStatus) {
		this.authenticationStatus = authenticationStatus;
	}

	public String getAuthentication_token() {
		return authentication_token;
	}

	public void setAuthentication_token(String authentication_token) {
		this.authentication_token = authentication_token;
	}

	public String getAuthenticationVersion() {
		return authenticationVersion;
	}

	public void setAuthenticationVersion(String authenticationVersion) {
		this.authenticationVersion = authenticationVersion;
	}

	public String getDsTransactionId() {
		return dsTransactionId;
	}

	public void setDsTransactionId(String dsTransactionId) {
		this.dsTransactionId = dsTransactionId;
	}

	public String getRedirectHtml() {
		return redirectHtml;
	}

	public void setRedirectHtml(String redirectHtml) {
		this.redirectHtml = redirectHtml;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getDsReference() {
		return dsReference;
	}

	public void setDsReference(String dsReference) {
		this.dsReference = dsReference;
	}

	public String getMerchantreference() {
		return merchantreference;
	}

	public void setMerchantreference(String merchantreference) {
		this.merchantreference = merchantreference;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
