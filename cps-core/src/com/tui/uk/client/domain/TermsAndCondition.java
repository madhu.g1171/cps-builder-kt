/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: TermsAndCondition.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-10-28 08:08:25$
 *
 * $author: thomas.pm$
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class is responsible for holding the terms and condition to be
 * displayed in Payment page rendered by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */
public final class TermsAndCondition implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = 2811543368044374030L;

   /** The terms and condition classification key. */
   private String tcClassificationKey;

   /** The terms and condition pay information text. */
   private String payInfoText;

   /** Relative url containing path for terms and condition url. */
   private String relativeTAndCUrl;

   /** The domain name hosting relativeTAndCUrl. */
   private ClientApplication tAndCDomain;

   /**
    * Constructor for TermsAndCondition.
    *
    * @param tcClassificationKey the tcClassificationKey.
    * @param payInfoText the payInfoText.
    *
    * @deprecated This constructor should not be used.
    */
   public TermsAndCondition(String tcClassificationKey, String payInfoText)
   {
      this.tcClassificationKey = tcClassificationKey;
      this.payInfoText = payInfoText;
   }

   /**
    * Constructor for TermsAndCondition with t&c related entries.
    *
    * @param tAndCDomain the t and c domain.
    * @param relativeTAndCUrl the relativeTAndCUrl.
    * @param payInfoText the the information to be displayed with t&c url.
    */
   public TermsAndCondition(ClientApplication tAndCDomain, String relativeTAndCUrl,
           String payInfoText)
   {
      this.tAndCDomain = tAndCDomain;
      this.relativeTAndCUrl = relativeTAndCUrl;
      this.payInfoText = payInfoText;
   }

   /**
    * Get the terms and condition classification key.
    *
    * @return the tcClassificationKey
    */
   public String getTcClassificationKey()
   {
      return tcClassificationKey;
   }

   /**
    * Get the pay information text.
    * @return the payInfoText
    */
   public String getpayInfoText()
   {
      return payInfoText;
   }

   /**
    * Get relative t&c url.
    *
    * @return the relativeTAndCUrl
    */
   public String getRelativeTAndCUrl()
   {
     return relativeTAndCUrl;
   }

   /**
    * Get t&c domain.
    *
    * @return the tAndCDomain
    */
   public ClientApplication getDomain()
   {
     return tAndCDomain;
   }

}
