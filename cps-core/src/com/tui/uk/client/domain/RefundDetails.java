/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: RefundDetails.java$
 *
 * $Revision: $
 *
 * $Date: Oct 30, 2008$
 *
 * Author: vijayalakshmi.d
 *
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is responsible for holding all the Refund details to be displayed in Payment page
 * rendered by Common Payment Component at the time of refund.
 *
 */
public final class RefundDetails implements Serializable
{
   /** The generated serial version id. */
   private static final long serialVersionUID = 2851945452281264156L;

   /** The transaction amount. */
   private Money transactionAmount;

   /** The transaction date. */
   private Date transactionDate;

   /** The data-cash reference number for the transaction. */
   private String datacashReference;

   /** The masked card number used at the time of transaction. */
   private String maskedCardNumber;

   /** The paymentType for the transaction. */
   private String paymentType;

   /** The paymentMethod for the transaction. */
   private String paymentMethod;

   /** The RefundFlag for the transaction. */
   private boolean refundEnabled;

   /** The payeeName. */
   private String payeeName;

   /** The cardExpiry date. */
   private Date cardExpiry;

   /** The integratedRefund for the transaction. */
   private boolean integratedRefund;

   /** The status. */
   private String status;

   private String originalPaymentMode;
   
   private String refundReferenceId;

   public String getRefundReferenceId() {
	return refundReferenceId;
}

public void setRefundReferenceId(String refundReferenceId) {
	this.refundReferenceId = refundReferenceId;
}

   public String getOriginalPaymentMode() {
	return originalPaymentMode;
}

public void setOriginalPaymentMode(String originalPaymentMode) {
	this.originalPaymentMode = originalPaymentMode;
}

/**
    * Constructor for RefundDetails.
    *
    * @param datacashReference the datacashReference
    * @param transactionAmount the transactionAmount
    * @param transactionDate the transactionDate.
    */
   public RefundDetails(String datacashReference, Money transactionAmount, Date transactionDate)
   {
      this.datacashReference = datacashReference;
      this.transactionAmount = transactionAmount;
      this.transactionDate = new Date(transactionDate.getTime());
   }

   /**
    * Constructor for RefundDetails.
    *
    * @param datacashReference the datacashReference
    * @param transactionAmount the transactionAmount
    * @param transactionDate the transactionDate.
    * @param maskedCardNumber the maskedCardNumber.
    */
   public RefundDetails(String datacashReference, Money transactionAmount, Date transactionDate,
                        String maskedCardNumber)
   {
      this.datacashReference = datacashReference;
      this.transactionAmount = transactionAmount;
      this.transactionDate = new Date(transactionDate.getTime());
      this.maskedCardNumber = maskedCardNumber;
   }

   /**
    * Get the datacashReference.
    *
    * @return datacashReference the datacashReference.
    */
   public String getDatacashReference()
   {
      return datacashReference;
   }

   /**
    * Get the transactionAmount.
    *
    * @return transactionAmount the transactionAmount.
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Get the deposit due date.
    *
    * @return depositDueDate the deposit due date.
    */
   public Date getTransactionDate()
   {
      return new Date(transactionDate.getTime());
   }

   /**
    * Gets the masked card number.
    *
    * @return maskedCardNumber the masked card number used at the time of payment.
    */
   public String getMaskedCardNumber()
   {
      return maskedCardNumber;
   }

   /**
    * Gets the payment type.
    *
    * @return the paymentType
    */
   public String getPaymentType()
   {
      return paymentType;
   }

   /**
    * Sets the paymentType.
    *
    * @param paymentType the paymentType to set
    */
   public void setPaymentType(String paymentType)
   {
      this.paymentType = paymentType;
   }

   /**
    * Gets the paymentMethod.
    *
    * @return the paymentMethod.
    */
   public String getPaymentMethod()
   {
      return paymentMethod;
   }

   /**
    * Sets the paymentMethod.
    *
    * @param paymentMethod the paymentMethod to set
    */
   public void setPaymentMethod(String paymentMethod)
   {
      this.paymentMethod = paymentMethod;
   }

   /**
    * Gets the RefundEnabled Flag.
    *
    * @return the refundEnabled
    */
   public boolean isRefundEnabled()
   {
      return refundEnabled;
   }

   /**
    * Sets the refundEnabled Flag.
    *
    * @param refundEnabled the refundEnabled to set
    */
   public void setRefundEnabled(boolean refundEnabled)
   {
      this.refundEnabled = refundEnabled;
   }

   /**
    * Gets the payeeName.
    *
    * @return the payeeName.
    */
   public String getPayeeName()
   {
      return payeeName;
   }

   /**
    * Sets the payeeName.
    *
    * @param payeeName the payeeName to set
    */
   public void setPayeeName(String payeeName)
   {
      this.payeeName = payeeName;
   }

   /**
    * Gets the cardExpiry.
    *
    * @return the cardExpiry.
    */
   public Date getCardExpiry()
   {
      return cardExpiry;
   }

   /**
    * Sets the cardExpiry.
    *
    * @param cardExpiry the cardExpiry to set
    */
   public void setCardExpiry(Date cardExpiry)
   {
      this.cardExpiry = cardExpiry;
   }

   /**
    * Gets the integratedRefund.
    *
    * @return the integratedRefund.
    */
   public boolean isIntegratedRefund()
   {
      return integratedRefund;
   }

   /**
    * Sets the integratedRefund.
    *
    * @param integratedRefund the integratedRefund to set
    */
   public void setIntegratedRefund(boolean integratedRefund)
   {
      this.integratedRefund = integratedRefund;
   }

   /**
    * Gets the status.
    *
    * @return the status.
    */
   public String getStatus()
   {
      return status;
   }

   /**
    * Sets the status.
    *
    * @param status the status to set
    */
   public void setStatus(String status)
   {
      this.status = status;
   }

}
