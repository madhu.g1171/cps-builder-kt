package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;

public class PayDayRange implements Serializable {

	

	private Integer startDate;
	private Integer endDate;
	
	
	public PayDayRange(){}
	
	public PayDayRange(Integer startDate,Integer endDate){
		this.startDate=startDate;
		this.endDate=endDate;
		
	}
	
	
	
	public Integer getStartDate() {
		return startDate;
	}
	public void setStartDate(Integer startDate) {
		this.startDate = startDate;
	}
	public Integer getEndDate() {
		return endDate;
	}
	public void setEndDate(Integer endDate) {
		this.endDate = endDate;
	}

	

	





}
