/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Accommodation.java,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2008-06-02 07:38:45 $
 *
 * $author: thomas.pm $
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.util.List;
import java.io.Serializable;

/**
 * This represents Accommodation object of Common Payment Component(CPC).
 * It includes necessary accommodation details which are essential to display
 * accommodation panel in Payment Page rendered by CPC.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */
public final class Accommodation implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = -3273417987712200198L;

   /** The country of accommodation. */
   private String country;

   /** The destination of accommodation. */
   private String destination;

   /** The resort of accommodation. */
   private String resort;

   /** The hotel of accommodation. */
   private String hotel;

   /** The basis of board for accommodation. */
   private String boardBasis;

   /** The rating for accommodation. */
   private String rating;

   /** The type of accommodation. */
   private String accommodationType;

   /** The roomDescription of accommodation. */
   private String roomDescription;

   /** The ratingImage for the accommodation rating. */
   private String ratingImage;

   /** The shipId of accommodation. */
   private String shipId;

   /** The additionalInfo for accommodation. */
   private Boolean additionalValue;

   /** The ratingDescription for the accommodation rating. */
   private String ratingDescription;

   /** The selling code. */
   private String sellingCode;

   /** The list of cabin. */
   private List<Cabin> deckInformation;
   
   /**
    * Constructor for Accommodation.
    *
    * @param boardBasis the boardBasis.
    * @param destination the destination.
    * @param country the country.
    * @param resort the resort.
    * @param hotel the hotel.
    */
   public Accommodation(String country, String destination, String resort,
                        String hotel, String boardBasis)
   {
      this.country = country;
      this.destination = destination;
      this.resort = resort;
      this.hotel = hotel;
      this.boardBasis = boardBasis;
   }

   /**
    * Get the country of accommodation.
    *
    * @return the country
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Get the destination.
    *
    * @return the destination.
    */
   public String getDestination()
   {
      return destination;
   }

   /**
    * Get the resort for accommodation.
    *
    * @return the resort
    */
   public String getResort()
   {
      return resort;
   }

   /**
    * Get the hotel for accommodation.
    *
    * @return the hotel
    */
   public String getHotel()
   {
      return hotel;
   }

   /**
    * Get the basis of board for accommodation.
    *
    * @return the boardBasis
    */
   public String getBoardBasis()
   {
      return boardBasis;
   }

   /**
    * Get the rating for accommodation.
    *
    * @return the rating
    */
   public String getRating()
   {
      return rating;
   }

   /**
    * Gets the accommodation type.
    *
    * @return the accommodation type
    */
   public String getAccommodationType()
   {
      return accommodationType;
   }

   /**
    * Gets the room description.
    *
    * @return the room description
    */
   public String getRoomDescription()
   {
      return roomDescription;
   }

   /**
    * Gets the rating image for the accommodation rating.
    *
    * @return the ratingImage
    */
   public String getRatingImage()
   {
      return ratingImage;
   }

   /**
    * Gets the rating description for the accommodation rating.
    *
    * @return the ratingDescription
    */
   public String getRatingDescription()
   {
      return ratingDescription;
   }

   /**
    * Gets the ship id.
    *
    * @return the ship description
    */
   public String getShipId()
   {
      return shipId;
   }

   /**
    * Gets the additional value for the accommodation rating.
    *
    * @return the additionalValue
    */
   public Boolean getAdditionalValue()
   {
      return additionalValue;
   }

   /**
    * Gets the selling code.
    *
    * @return the selling code.
    */
   public String getSellingCode()
   {
      return sellingCode;
   }

   /**
    * Sets the accommodation type.
    *
    * @param accommodationType the accommodation type
    */
   public void setAccommodationType(String accommodationType)
   {
      this.accommodationType = accommodationType;
   }

   /**
    * Sets the room description.
    *
    * @param roomDescription the room description
    */
   public void setRoomDescription(String roomDescription)
   {
      this.roomDescription = roomDescription;
   }

   /**
    * Sets the rating for accommodation..
    *
    * @param rating the rating
    */
   public void setRating(String rating)
   {
      this.rating = rating;
   }

   /**
    * Sets the rating image for the accommodation rating.
    *
    * @param ratingImage the ratingImage to set
    */
   public void setRatingImage(String ratingImage)
   {
      this.ratingImage = ratingImage;
   }

   /**
    * Sets the rating description for the accommodation rating.
    *
    * @param ratingDescription the ratingDescription to set
    */
   public void setRatingDescription(String ratingDescription)
   {
      this.ratingDescription = ratingDescription;
   }

   /**
    * Sets the ship id.
    *
    * @param shipId the ship id
    */
   public void setShipId(String shipId)
   {
      this.shipId = shipId;
   }

   /**
    * Sets the additional value for the accommodation rating.
    *
    * @param additionalValue the additionalValue to set
    */
   public void setAdditionalValue(Boolean additionalValue)
   {
      this.additionalValue = additionalValue;
   }

   /**
    * Set the selling code.
    *
    * @param sellingCode the selling code.
    */
   public void setSellingCode(String sellingCode)
   {
      this.sellingCode = sellingCode;
   }
   
   
   /**
    * Get DeckInformation.
    * 
    * @return deckInformation.
    */
   public List<Cabin> getDeckInformation()
   {
      return deckInformation;
   }

   
   /**
    * set deckInformation.
    * 
    * @param deckInformation the deck info.
    */
   public void setDeckInformation(List<Cabin> deckInformation)
   {
      this.deckInformation = deckInformation;
   }

}
