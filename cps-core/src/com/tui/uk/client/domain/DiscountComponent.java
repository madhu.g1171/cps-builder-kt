/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DiscountComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 07:59:25$
 *
 * $author: thomas.pm $
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class is responsible for holding all discount details like discount type,
 * maximum discount amount, whether it's applicable for Hotel or flight etc.,
 * to be displayed in Payment page rendered by Common Payment Component.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public final class DiscountComponent implements Serializable
{
   /** The generated serial version id. */
   private static final long serialVersionUID = -4442077740550093734L;

   /** The discount type/name. */
   private String discountType;

   /** This attribute holds the maximum discount applicable for the discount type. */
   private Money maxDiscountAmount;

   /** Is the discount applicable for Hotel. */
   private Boolean applicableForHotel;

   /** Is the discount applicable for Flight. */
   private Boolean applicableForFlight;

   /**
    * Constructor for DiscountComponent.
    *
    * @param discountType the discount type.
    * @param maxDiscountAmount  the maximum discount amount .
    * @param applicableForHotel indicates if the discount is applicable for hotel or not.
    * @param applicableForFlight indicates if the discount is applicable for flight or not.
    */
   public DiscountComponent(String discountType, Money maxDiscountAmount,
                            Boolean applicableForHotel, Boolean applicableForFlight)
   {
      this.discountType = discountType;
      this.maxDiscountAmount  = maxDiscountAmount;
      this.applicableForFlight = applicableForFlight;
      this.applicableForHotel = applicableForHotel;
   }

   /**
    * Get the discount type.
    *
    * @return the discount type
    */
   public String getDiscountType()
   {
      return discountType;
   }

   /**
    * Get the maximum discount amount .
    *
    * @return the maxDiscountAmount
    */
   public Money getMaxDiscountAmount()
   {
      return maxDiscountAmount;
   }

   /**
    * Get the applicableForHotel.
    *
    * @return the applicableForHotel
    */
   public Boolean getApplicableForHotel()
   {
      return applicableForHotel;
   }

   /**
    * Get the applicableForFlight.
    *
    * @return the applicableForFlight
    */
   public Boolean getApplicableForFlight()
   {
      return applicableForFlight;
   }

}
