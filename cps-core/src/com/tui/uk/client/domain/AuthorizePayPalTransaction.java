package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class to hold data for authorizing paypal transaction
 * @author tanveer.bp
 *
 */
public class AuthorizePayPalTransaction implements Serializable{

	private String transactionMethod ;
	private String datacashReferance ;
	private String Amount ;
	private String itemAmount;
	private String messageSubmissionId;
	private String shippingTotalCost;
	private String totalTax;
	private ShippingAddress shippingAddress;
	private String CurrencyCode;
	private String CountryCode;
	private String customerEmailId;
	private String firstname;
	private String lastname;
	private String middlename;
	private String payerId;
	private String payerStatus;
	private String paymentStatus;
	private String paymentType;

	/** The generated serial version id. */
	private static final long serialVersionUID = -3215491945522541807L;

	public AuthorizePayPalTransaction(String transactionMethod , String datacashReferance){
		this.transactionMethod = transactionMethod;
		this.datacashReferance = datacashReferance;
	}
	/**
	 * @return the transactionMethod
	 */
	public String getTransactionMethod() {
		return transactionMethod;
	}
	/**
	 * @param transactionMethod the transactionMethod to set
	 */
	public void setTransactionMethod(String transactionMethod) {
		this.transactionMethod = transactionMethod;
	}
	/**
	 * @return the datacashReferance
	 */
	public String getDatacashReferance() {
		return datacashReferance;
	}
	/**
	 * @param datacashReferance the transactionMethod to set
	 */
	public void setDatacashReferance(String datacashReferance) {
		this.datacashReferance=datacashReferance;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return Amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		Amount = amount;
	}
	/**
	 * @return the itemAmount
	 */
	public String getItemAmount() {
		return itemAmount;
	}
	/**
	 * @param itemAmount the itemAmount to set
	 */
	public void setItemAmount(String itemAmount) {
		this.itemAmount = itemAmount;
	}
	/**
	 * @return the messageSubmissionId
	 */
	public String getMessageSubmissionId() {
		return messageSubmissionId;
	}
	/**
	 * @param messageSubmissionId the messageSubmissionId to set
	 */
	public void setMessageSubmissionId(String messageSubmissionId) {
		this.messageSubmissionId = messageSubmissionId;
	}
	/**
	 * @return the shippingTotalCost
	 */
	public String getShippingTotalCost() {
		return shippingTotalCost;
	}
	/**
	 * @param shippingTotalCost the shippingTotalCost to set
	 */
	public void setShippingTotalCost(String shippingTotalCost) {
		this.shippingTotalCost = shippingTotalCost;
	}
	/**
	 * @return the totalTax
	 */
	public String getTotalTax() {
		return totalTax;
	}
	/**
	 * @param totalTax the totalTax to set
	 */
	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}
	/**
	 * @return the shippingAddress
	 */
	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}
	/**
	 * @param shippingAddress the shippingAddress to set
	 */
	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return CurrencyCode;
	}
	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}
	/**
	 * @return the customerEmailId
	 */
	public String getCustomerEmailId() {
		return customerEmailId;
	}
	/**
	 * @param customerEmailId the customerEmailId to set
	 */
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the middlename
	 */
	public String getMiddlename() {
		return middlename;
	}
	/**
	 * @param middlename the middlename to set
	 */
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	/**
	 * @return the payerId
	 */
	public String getPayerId() {
		return payerId;
	}
	/**
	 * @param payerId the payerId to set
	 */
	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	/**
	 * @return the payerStatus
	 */
	public String getPayerStatus() {
		return payerStatus;
	}
	/**
	 * @param payerStatus the payerStatus to set
	 */
	public void setPayerStatus(String payerStatus) {
		this.payerStatus = payerStatus;
	}
	/**
	 * @return the paymentStatus
	 */
	public String getPaymentStatus() {
		return paymentStatus;
	}
	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return CountryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

}
