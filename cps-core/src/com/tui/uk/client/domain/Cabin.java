/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Accommodation.java,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2008-06-02 07:38:45 $
 *
 * $author: saleem shaik $
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
/**
 * This represents Cabin object of Common Payment Component(CPC).
 * It includes necessary Cabin details which are essential to display
 * Cabin panel in Payment Page rendered by CPC.
 *
 * @author Saleem Shaik
 *
 * @since 1.0 API
 */
public final class Cabin implements Serializable
{
/** The cabin id or deck number. */
private int id;

/** The cabin description. */
private String cabinDescription;

/** The deck info like deck 3 4 deck 4 5. */
private String deckInfo;

/**
 * Constructor for Cabin.
 *
 * @param id the id.
 * @param cabinDescription the cabinDescription.
 * @param deckInfo the deckInfo.
 */
public Cabin(int id, String cabinDescription, String deckInfo)
{
   this.id = id;
   this.cabinDescription = cabinDescription;
   this.deckInfo = deckInfo;
}
/**
* Gets the room id or number.
*
* @return the Id
*/
public int getId()
{
return id;
}
/**
* Gets the cabinDescription.
 *
* @return the cabinDescription
*/
public String getCabinDescription()
{
return cabinDescription;
}
/**
* Sets id field.
*
* @param id to set.
*/
public void setId(int id)
{
this.id = id;
}
/**
* Sets cabinDescription field.
*
* @param cabinDescription the cabinDescription to set.
*/
 public void setCabinDescription(String cabinDescription)
{
this.cabinDescription = cabinDescription;
}
/**
* Gets the deck description.
*
* @return deckInfo
*/
public String getDeckInfo() 
{
return deckInfo;
}
/**
* Sets deckInfo field.
 *
* @param deckInfo the deckInfo to set.
*/
public void setDeckInfo(String deckInfo) 
{
this.deckInfo = deckInfo;
}
}
