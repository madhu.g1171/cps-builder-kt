/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class holds contact information including email address, postal address and phone numbers.
 *
 * @author sindhushree.g
 *
 */
public class ContactInfo implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = 154597291481038544L;

   /** The house number or house name. */
   private String houseDetails;

   /** The street address. */
   private String streetAddress1;

   /** The street address 2. */
   private String streetAddress2;

   /** The city. */
   private String city;

   /** The county. */
   private String county;

   /** The post code. */
   private String postCode;

   /** The country. */
   private String country;

   /** The countryName. */
   private String countryName;

   public String getCountryName() {
	return countryName;
}

public void setCountryName(String countryName) {
	this.countryName = countryName;
}

/** The email address. */
   private String emailAddress;

   /** The phone number. */
   private String phoneNumber;

   /** The alternate phone number. */
   private String altPhoneNumber;

   /**
    * Gets the houseDetails.
    *
    * @return the houseDetails
    */
   public String getHouseDetails()
   {
      return houseDetails;
   }

   /**
    * Sets the house number or house name.
    *
    * @param houseDetails the house number or house name to set.
    */
   public void setHouseDetails(String houseDetails)
   {
      this.houseDetails = houseDetails;
   }

   /**
    * Sets the street address1.
    *
    * @param streetAddress1 the streetAddress1 to set.
    */
   public void setStreetAddress1(String streetAddress1)
   {
      this.streetAddress1 = streetAddress1;
   }

   /**
    * Sets the street address2.
    *
    * @param streetAddress2 the streetAddress2 to set.
    */
   public void setStreetAddress2(String streetAddress2)
   {
      this.streetAddress2 = streetAddress2;
   }

   /**
    * Sets the city.
    *
    * @param city the city to set.
    */
   public void setCity(String city)
   {
      this.city = city;
   }

   /**
    * Sets the county.
    *
    * @param county the county to set.
    */
   public void setCounty(String county)
   {
      this.county = county;
   }

   /**
    * Sets the post code.
    *
    * @param postCode the postCode to set.
    */
   public void setPostCode(String postCode)
   {
      this.postCode = postCode;
   }

   /**
    * Sets the country.
    *
    * @param country the country to set.
    */
   public void setCountry(String country)
   {
      this.country = country;
   }

   /**
    * Sets the email address.
    *
    * @param emailAddress the emailAddress to set.
    */
   public void setEmailAddress(String emailAddress)
   {
      this.emailAddress = emailAddress;
   }

   /**
    * Sets the phone number.
    *
    * @param phoneNumber the phoneNumber to set.
    */
   public void setPhoneNumber(String phoneNumber)
   {
      this.phoneNumber = phoneNumber;
   }

   /**
    * Sets the alternate phone number.
    *
    * @param altPhoneNumber the altPhoneNumber to set.
    */
   public void setAltPhoneNumber(String altPhoneNumber)
   {
      this.altPhoneNumber = altPhoneNumber;
   }

   /**
    * Gets the street address1.
    *
    * @return the streetAddress1.
    */
   public String getStreetAddress1()
   {
      return streetAddress1;
   }

   /**
    * Gets the street address2.
    *
    * @return the streetAddress2.
    */
   public String getStreetAddress2()
   {
      return streetAddress2;
   }

   /**
    * Gets the city.
    *
    * @return the city
    */
   public String getCity()
   {
      return city;
   }

   /**
    * Gets the county.
    *
    * @return the county
    */
   public String getCounty()
   {
      return county;
   }

   /**
    * Gets the post code.
    *
    * @return the postCode.
    */
   public String getPostCode()
   {
      return postCode;
   }

   /**
    * Gets the country.
    *
    * @return the country.
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Gets the email address.
    *
    * @return the emailAddress.
    */
   public String getEmailAddress()
   {
      return emailAddress;
   }

   /**
    * Gets the phone number.
    *
    * @return the phoneNumber.
    */
   public String getPhoneNumber()
   {
      return phoneNumber;
   }

   /**
    * Gets the alternate phone number.
    *
    * @return the altPhoneNumber
    */
   public String getAltPhoneNumber()
   {
      return altPhoneNumber;
   }

}
