package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.tui.uk.domain.ShorexSummaryOrder;

public class ExcursionSummary implements Serializable
{
   /** The generated serial version id. */
   private static final long serialVersionUID = 6009048897295838227L;

   private String itineraryName;
   private String cruiseName;
   private Date cruiseDepartureDate;
   private int cruiseDuration;
   /** The total amount of the excursion. */
   private Money excursionTotalCost;
   /** The list individual excursions. */
   private List<ShorexExcursions> shorexExcursions;
   
   /** represents merchantID. */
   private String merchantID;
   
   /** The list for individual reservations. */
   private List<DiningReservation> diningReservations;
   
   private List allShorexSummaryElements;
   
   public List getAllShorexSummaryElements()
   {
      return allShorexSummaryElements;
   }
   public void setAllShorexSummaryElements(List allShorexSummaryElements)
   {
      this.allShorexSummaryElements = allShorexSummaryElements;
   }
   /** returns the list for dining reservations. */
   public List<DiningReservation> getDiningReservations() {
      return diningReservations;
   }
   /** sets the list of dining reservations. */
   public void setDiningReservations(List<DiningReservation> diningReservations) {
      this.diningReservations = diningReservations;
   }

   /**
    * @return the itineraryName
    */
   public String getItineraryName() {
      return itineraryName;
   }
   /**
    * @param itineraryName the itineraryName to set
    */
   public void setItineraryName(String itineraryName) {
      this.itineraryName = itineraryName;
   }
   /**
    * @return the cruiseName
    */
   public String getCruiseName() {
      return cruiseName;
   }
   /**
    * @param cruiseName the cruiseName to set
    */
   public void setCruiseName(String cruiseName) {
      this.cruiseName = cruiseName;
   }
   
   /**
    * @return the shorexExcursions
    */
   public List<ShorexExcursions> getShorexExcursions() {
      return shorexExcursions;
   }
   /**
    * @param shorexExcursions the shorexExcursions to set
    */
   public void setShorexExcursions(List<ShorexExcursions> shorexExcursions) {
      this.shorexExcursions = shorexExcursions;
   }
   /**
    * @return the excursionTotalCost
    */
   public Money getExcursionTotalCost() {
      return excursionTotalCost;
   }
   /**
    * @param excursionTotalCost the excursionTotalCost to set
    */
   public void setExcursionTotalCost(Money excursionTotalCost) {
      this.excursionTotalCost = excursionTotalCost;
   }
   /**
    * @return the cruiseDepartureDate
    */
   public Date getCruiseDepartureDate() {
      
      return cruiseDepartureDate;
         
   }
   /**
    * @return the cruiseDuration
    */
   public int getCruiseDuration() {
      return cruiseDuration;
   }
   /**
    * @param cruiseDepartureDate the cruiseDepartureDate to set
    */
   public void setCruiseDepartureDate(Date cruiseDepartureDate) {
      this.cruiseDepartureDate = cruiseDepartureDate;
   }
   /**
    * @param cruiseDuration the cruiseDuration to set
    */
   public void setCruiseDuration(int cruiseDuration) {
      this.cruiseDuration = cruiseDuration;
   }
   
   /** returns merchantID. */
   public String getMerchantID() {
      return merchantID;
   }

   /** sets merchantID. */
   public void setMerchantID(String merchantID) {
      this.merchantID = merchantID;
   }
   
   
   
   public void sortAllSummmaryElements(){
      ArrayList elements=new ArrayList();
      elements.addAll(this.getShorexExcursions());
      elements.addAll(this.getDiningReservations());
      Collections.sort(elements,new ShorexSummaryOrder());
      this.setAllShorexSummaryElements(elements);
      
      
   }

}
