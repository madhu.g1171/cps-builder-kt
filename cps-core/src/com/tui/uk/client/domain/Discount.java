/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class holds discount information for the booking.
 *
 * @author sindhushree.g
 *
 */
public class Discount implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = -728089483854923925L;

   /** The discount id. */
   private String discountId;

   /** The discount amount. */
   private String discountAmount;

   /**
    * Sets the discount id.
    *
    * @param discountId the discount id.
    */
   public void setDiscountId(String discountId)
   {
      this.discountId = discountId;
   }

   /**
    * Sets the discount amount.
    *
    * @param discountAmount the discount amount.
    */
   public void setDiscountAmount(String discountAmount)
   {
      this.discountAmount = discountAmount;
   }

   /**
    * Gets the discount id.
    *
    * @return the discount id.
    */
   public String getDiscountId()
   {
      return discountId;
   }

   /**
    * Gets the discount amount.
    *
    * @return discount amount.
    */
   public String getDiscountAmount()
   {
      return discountAmount;
   }

}
