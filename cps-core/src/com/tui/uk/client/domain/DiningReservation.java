/**
 * 
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;
import java.time.LocalTime; 
/**
 * @author sreenivasulu.k
 *
 */

public class DiningReservation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 *  restarant name
	 */
	private String restarantName;
	

	/**
	 * represents no. of adults
	 */
	private int adultsCount;
	

	/**
	 * represents no of childrens.
	 */
	private int childrenCount;


	/**
	 * represents no of passengers(adults + children).
	 */
	private int passengersCount;
	

	/**
	 *    represents payable amount for adults.
	 */
	private Money payableAmountForAdults;

	/**
	 * represents payable amount for children.
	 */
	private Money payableAmountForChildren;
	
	/**
	 * represents total amount for both adults and children.
	 */
	private Money totalPayableAmountForPassengers;

	/**
	 *  represents the date .
	 */
	private Date date;
	
	
	  /**
    *  days
    */
   private Integer days;
   
   /**
    *  This field is for displaying purpose.
    */
   private String formatedDateValue;
   /**
    *  returns formatedDate value in the form of E dd MMM yyyy.
    */
   public String getFormatedDateValue()
   {
      return formatedDateValue;
   }
   /**
    *  setting the formated Date value.
    */
   public void setFormatedDateValue(String formatedDateValue)
   {
      this.formatedDateValue = formatedDateValue;
   }
   /**
    *  returns days
    */
   public Integer getDays()
   {
      return days;
   }
   /**
    *  set days
    */
   public void setDays(Integer days)
   {
      this.days = days;
   }
   
	/**
	 *  represents the local time.
	 */
	private LocalTime localTime;
	
	/**
	 *  returns the dining time.
	 */
	public LocalTime getLocalTime() {
		return localTime;
	}

	/**
	 *  sets the dining time.
	 */
	public void setLocalTime(LocalTime localTime) {
		this.localTime = localTime;
	}


	/**
	 *  parameterized constructor for reservation.
	 */
	public DiningReservation(String restarantName, int childrenCount,
			int adultsCount, int passengersCount, Money payableAmountForAdults,
			Money payableAmountForChildren,
			Money totalPayableAmountForPassengers) {
		
		this.restarantName=restarantName;
		this.childrenCount=childrenCount;
		this.adultsCount=adultsCount;
		this.passengersCount=passengersCount;
		this.payableAmountForAdults=payableAmountForAdults;
		this.payableAmountForChildren=payableAmountForChildren;
		this.totalPayableAmountForPassengers=totalPayableAmountForPassengers;

	}

	 
	/**
	 * default constructor for reservation.
	 */
	
	
	public DiningReservation() {	


	}

	
	/**
	 *  returns payable amount for children.
	 */
    public Money getPayableAmountForChildren() {
		return payableAmountForChildren;
	}


	/**
	 *  sets the payable amount for children.
	 */
	public void setPayableAmountForChildren(Money payableAmountForChildren) {
		this.payableAmountForChildren = payableAmountForChildren;
	}
	
	/**
	 * returns total payable amount for passengers.
	 */
	public Money getTotalPayableAmountForPassengers() {
		return totalPayableAmountForPassengers;
	}



	/**
	 * sets the total payable amount for passengers.
	 */
	public void setTotalPayableAmountForPassengers(
			Money totalPayableAmountForPassengers) {
		this.totalPayableAmountForPassengers = totalPayableAmountForPassengers;
	}



	/**
	 * returns restarant name.
	 */
	public String getRestarantName() {
		return restarantName;
	}


	/**
	 * sets restarant name.
	 */
	public void setRestarantName(String restarantName) {
		this.restarantName = restarantName;
	}
	   
	/**
	 * returns the payable amount for adults.
	 */

	public Money getPayableAmountForAdults() {
		return payableAmountForAdults;
	}



	/**
	 * sets the payable amount for adults.
	 */
	public void setPayableAmountForAdults(Money payableAmountForAdults) {
		this.payableAmountForAdults = payableAmountForAdults;
	}
	
	/**
	 *  returns total passengers(adults + children).
	 */
	public int getPassengersCount() {
		return passengersCount;
	}


	/**
	 *   sets total passengers.
	 */
	public void setPassengersCount(int passengersCount) {
		this.passengersCount = passengersCount;
	}
	/**
	 *  returns children count.
	 */
	public int getChildrenCount() {
		return childrenCount;
	}


	/**
	 *  sets children count.
	 */
	public void setChildrenCount(int childrenCount) {
		this.childrenCount = childrenCount;
	}
	
	/**
	 * returns adults count.
	 */
	public int getAdultsCount() {
		return adultsCount;
	}




	/**
	 * sets adults count.
	 */
	public void setAdultsCount(int adultsCount) {
		this.adultsCount = adultsCount;
	}

	
	/**
	 * returns date.
	 */
	public Date getDate() {
		return date;
	}




	/**
	 * sets date .
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
