package com.tui.uk.client.domain;

import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PayDayRange;

import java.io.Serializable;
import java.util.Date;

public class DDPaymentSheduleComponent implements Serializable {

	private String paymentScheduleId;

	private Money amount;

	private Money lastAmount;

	private String payCount;

	private String firstInstallmentMonth;

	private String ddSetUpURL;



	public String getDdSetUpURL() {
        return ddSetUpURL;
    }

    public void setDdSetUpURL(String ddSetUpURL) {
        this.ddSetUpURL = ddSetUpURL;
    }

    public String getFirstInstallmentMonth() {
		return firstInstallmentMonth;
	}

	public void setFirstInstallmentMonth(String firstInstallmentMonth) {
		this.firstInstallmentMonth = firstInstallmentMonth;
	}

	private Boolean defaultSchedule;

	private PayDayRange payDayRange;


	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}

	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public Money getLastAmount() {
		return lastAmount;
	}

	public void setLastAmount(Money lastAmount) {
		this.lastAmount = lastAmount;
	}

	public String getPayCount() {
		return payCount;
	}

	public void setPayCount(String payCount) {
		this.payCount = payCount;
	}



	public Boolean getDefaultSchedule() {
		return defaultSchedule;
	}

	public void setDefaultSchedule(Boolean defaultSchedule) {
		this.defaultSchedule = defaultSchedule;
	}

	public PayDayRange getPayDayRange() {
		return payDayRange;
	}

	public void setPayDayRange(PayDayRange payDayRange) {
		this.payDayRange = payDayRange;
	}




}
