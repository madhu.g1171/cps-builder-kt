/**
 *
 */
package com.tui.uk.client;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcCommonsTransport;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransport;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggingTransport extends XmlRpcCommonsTransport {

	public Logger logger = Logger.getLogger(LoggingTransport.class.getName());

    public LoggingTransport(CustomXmlRpcCommonsTransportFactory pFactory) {
      super(pFactory);
    }

    /**
     * Logs the request content in addition to the actual work.
     */
    @Override
    protected void writeRequest(final ReqWriter pWriter) throws XmlRpcException {
      super.writeRequest(pWriter);

        CustomLoggingUtils.logRequest(logger, method.getRequestEntity());

    }

    /**
     * Logs the response from the server, and returns the contents of
     * the response as a ByteArrayInputStream.
     */
    @Override
    protected InputStream getInputStream() throws XmlRpcException {
      InputStream istream = super.getInputStream();

        return new ByteArrayInputStream(
          CustomLoggingUtils.logResponse(logger, istream).getBytes());

    }
  }
