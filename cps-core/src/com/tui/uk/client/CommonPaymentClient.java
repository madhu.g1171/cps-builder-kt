/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CommonPaymentClient.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-07-11 07:59:25$
 *
 * $author: bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransport;

import com.tui.uk.client.domain.AuthorizePayPalTransaction;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.EncryptedCard;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.PayPalAuthorizeResponse;
import com.tui.uk.client.domain.PayPalCaptureRequest;
import com.tui.uk.client.domain.PayPalCaptureResponse;
import com.tui.uk.client.domain.PayPalCustomer;
import com.tui.uk.client.domain.PayPalCustomerVerificationDetails;
import com.tui.uk.client.domain.PayPalFundReleaseResponse;
import com.tui.uk.client.domain.PayPalQueryTransactionResponse;
import com.tui.uk.log.LogWriter;

/**
 * This is the class which allows client of Common Payment Component(CPC) to interact with CPC
 * via XML-RPC. This gives following interface methods which can be used by client of CPC.
 * 1)Generating Token
 * 2)Getting NonPaymentData
 * 3)Getting EssentialTransactionData
 * 4)Registering Error
 * 5)Authorizing Payment
 * 6)Canceling payment
 * 7)Fulfilling payment
 * 8)Reversal of payment
 * 9)Refund of the payment
 * 10)Purging ClientPaymentPageData
 * 11)Getting EncryptedCardData.
 *
 * @author bibin.j@sonata-software.com
 */
public final class CommonPaymentClient
{

	/** The xml rpc client. */
	private XmlRpcClient xmlRpcClient = null;

	/** The http client. */
	private HttpClient httpClient = null;

	/** The constant GENERATE_TOKEN. */
	private static final String GENERATE_TOKEN = "commonPaymentService.generateToken";

	/** The constant TOMCAT. */
	private static final String TOMCAT = "tomcat";

	/** The constant ESSENTIAL_TRANSACTION_DATA. */
	private static final String ESSENTIAL_TRANSACTION_DATA =
			"commonPaymentService.getEssentialTransactionData";

	/** The constant for calling setAllowedCards. */
	private static final String ALLOWED_CARDS = "commonPaymentService.setAllowedCards";

	/** The constant NON_PAYMENT_DATA. */
	private static final String NON_PAYMENT_DATA = "commonPaymentService.getNonPaymentData";

	/** The constant ENCRYPTED_CARD_DATA. */
	private static final String ENCRYPTED_CARD_DATA = "commonPaymentService.getEncryptedCardData";

	/** The constant AUTH_PAYMENT. */
	private static final String AUTH_PAYMENT = "datacash.authPayment";

	/** The constant AUTH_SINGLE_PAYMENT. */
	private static final String AUTH_SINGLE_PAYMENT = "datacash.authSinglePayment";

	/** The constant CANCEL. */
	private static final String CANCEL = "datacash.cancel";

	/** The constant REGISTER_ERROR. */
	private static final String REGISTER_ERROR = "commonPaymentService.registerError";

	/** The constant PURGE_CLIENT_DATA. */
	private static final String PURGE_CLIENT_DATA =
			"commonPaymentService.purgeClientPaymentPageData";

	/** The constant PURGE_SENSITIVE_DATA. */
	private static final String PURGE_SENSITIVE_DATA = "commonPaymentService.purgeSensitiveData";

	/** The constant FULFILL. */
	private static final String FULFILL = "datacash.fulfill";

	/** The constant FULFIL. */
	private static final String FULFIL = "datacash.fulfil";

	/** The constant AUTH_REFUND. */
	private static final String AUTH_REFUND = "datacash.authRefund";

	/** The constant REFUND. */
	private static final String REFUND = "datacash.refund";

	/** The constant DO_PAYMENT. */
	private static final String DO_PAYMENT = "datacash.doPayment";

	/** The constant DO_REFUND. */
	private static final String DO_REFUND = "datacash.doRefund";

	/** The constant PRE_REGISTERED_REFUND. */
	private static final String PRE_REGISTERED_REFUND = "datacash.preRegisteredRefund";

	/** The constant NOTIFY_BOOKING_COMPLETION. */
	private static final String NOTIFY_BOOKING_COMPLETION =
			"commonPaymentService.notifyBookingCompletion";

	/** The constant PURGE_PAYMENT_TOKEN. */
	private static final String PURGE_PAYMENT_TOKEN = "commonPaymentService.purgePaymentToken";

	/** The constant PURGE_PAYMENT_TOKEN. */
	private static final String DD_SETUP_MANDATE = "datacash.ddSetUpMandate";
	private static final String validate_Account_Details =  "datacash.validateAccountDetails" ;
	private static final String DD_UPDATE_BOOKING_DETAILS = "datacash.updateBookingDetailsForDD";
	private static final String verify_Account_Details = "datacash.verifyAccountDetails";

    private static final String DD_REVOKE = "datacash.revokeDDSetup";
	/**
	 * This constructs XML RPC client for XML RPC Service provided by Common payment component with
	 * the given payment server URL.
	 *
	 * @param xmlRpcUrl the xmlRpcUrl url.
	 *
	 * @since 1.0 API
	 */
	public CommonPaymentClient(String xmlRpcUrl)
	{
		final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		config.setEnabledForExtensions(true);
		try
		{
			config.setServerURL(new URL(xmlRpcUrl));
		}
		catch (MalformedURLException e)
		{
			String errorMessage = e.getMessage();
			throw new RuntimeException(errorMessage, e);
		}
		xmlRpcClient = new XmlRpcClient();
		xmlRpcClient.setConfig(config);
		httpClient = new HttpClient();
		final XmlRpcCommonsTransportFactory factory = new XmlRpcCommonsTransportFactory(xmlRpcClient);
		factory.setHttpClient(httpClient);
		xmlRpcClient.setTransportFactory(factory);
	}

	/**
	 * Gets the associated HttpClient.
	 *
	 * @return httpClient.
	 */
	public HttpClient getHttpClient()
	{
		return httpClient;
	}

	/**
	 * This method stores BookingComponent against a unique token in PaymentStore of CPC
	 * and returns the token as a string.This token is used to identify a single application
	 * request within the CPS. It does not identify the payment. After payment has been taken,
	 * the token is of no value and can be discarded. It is not necessary for the client
	 * application to store the token in session data.
	 *
	 * @param bookingComponent the booking component.
	 * @return String token
	 * @throws XmlRpcException Exception.
	 *
	 * @since 1.0 API
	 */
	public String[] generateToken(BookingComponent bookingComponent) throws XmlRpcException
	{
		Object[] params = new Object[] {bookingComponent};
		String token =  (String) xmlRpcClient.execute(GENERATE_TOKEN, params);
		String cookie = "tomcat=" + getTomcatInstance();
		return new String []{token, cookie};
	}

	/**
	 * This method is called after generateToken.The method to get the tomcat instance to which
	 * the client should redirect to.When the token and tomcat instance is obtained, the client
	 * application should redirect to the CPS.
	 *
	 * @return the tomcatInstance.
	 *
	 * @since 1.0 API
	 */
	private String getTomcatInstance()
	{
		HttpState httpState = httpClient.getState();
		Cookie[] cookies = httpState.getCookies();
		String tomcatInstance = null;
		for (Cookie cookie : cookies)
		{
			if (cookie.getName().equals(TOMCAT))
			{
				tomcatInstance = cookie.getValue();
				break;
			}
		}
		return tomcatInstance;
	}

	/**
	 * Sets the allowed cards to the object associated with the token.
	 *
	 * @param token the token.
	 * @param allowedCards the allowed cards.
	 *
	 * @throws XmlRpcException if there is a problem while setting the card details.
	 *
	 * @deprecated not required to be sent from client as this is made configurable.
	 */
	public void setAllowedCards(String token, Map<String, Object[]> allowedCards)
			throws XmlRpcException
			{
		Object[] params = new Object[] {token, allowedCards};
		xmlRpcClient.execute(ALLOWED_CARDS, params);
			}

	/**
	 * This is used by the client application to get the payment data from the CPS.
	 * This method returns a list of payment methods, such as cash, cheque, CNP, etc.
	 * For each payment method the following data is returned:
	 *  The method of payment: cash, cheque, CNP, etc.
	 *  The amount paid on this payment method.
	 *  Any credit card charges, accepted by the customer, and included in the payment.
	 *  The DataCash transaction id for the payment.
	 * The sum of the amounts paid on each payment method should add up to the amount
	 * the host application said was payable in generateToken.
	 *
	 * @param token the token
	 * @return the essential transaction data
	 * @throws XmlRpcException the xmlrpc exception
	 *
	 * @since 1.0 API
	 */
	public List<EssentialTransactionData> getEssentialTransactionData(String token)
			throws XmlRpcException
			{
		List<EssentialTransactionData> essentialTransactions =
				new ArrayList<EssentialTransactionData>();
		Object[] params = new Object[] {token};
		Object[] transactions = (Object[]) xmlRpcClient.execute(ESSENTIAL_TRANSACTION_DATA, params);
		int length = transactions.length;
		for (int i = 0; i < length; i++)
		{
			EssentialTransactionData essentialTransactionData =
					(EssentialTransactionData) transactions[i];
			essentialTransactions.add(essentialTransactionData);
		}
		return essentialTransactions;
			}


	/**
	 * This is used by the client application to get non-payment data from the CPS.
	 * The payment page served by the CPS will contain a mixture of payment and non-payment data.
	 * Payment data is handled by the CPS and returned by the getEssentialTransactionData method.
	 * Any input fields on the payment page that are not recognised as payment data is returned
	 * by this method.
	 * Examples of non-payment data are:
	 * Lead passenger name and address.
	 * Acceptance of terms and conditions.
	 * This method takes the CPS token as an input parameter. It returns the non-payment data as a
	 * simple String, String map. The map keys are the input field names on the payment page.
	 * The map values are the values entered by the user. The map will include any hidden input
	 * fields.
	 *
	 * @param token the token
	 * @return the non payment data
	 * @throws XmlRpcException Exception.
	 *
	 * @since 1.0 API
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getNonPaymentData(String token) throws XmlRpcException
	{
		Object[] params = new Object[] {token};
		return (Map<String, String>) xmlRpcClient.execute(NON_PAYMENT_DATA, params);
	}

	/**
	 * This method returns card details for post-payment guarentee payments (HOPLA).
	 * The card details are encrypted using HOPLA's public key so they are not visible to
	 * applications while being transmitted to HOPLA. Only the details of one card will be returned.
	 * This method cannot be used to return normal CNP card details.
	 *
	 * @param token the token
	 * @return the encrypted card data.
	 * @throws XmlRpcException Exception.
	 *
	 * @since 1.0 API
	 */
	public EncryptedCard getEncryptedCardData(String token)throws XmlRpcException
	{
		Object[] params = new Object[] {token};
		return (EncryptedCard) xmlRpcClient.execute(ENCRYPTED_CARD_DATA, params);
	}

	/**
	 * This method authorises the payment using DataCash. This method should be called
	 * after the client application has validated any non-payment data, but before it
	 * has done any non-reversable back-end processing, such as taking a holiday out of inventory.
	 *
	 * @param token the token
	 * @param client the client
	 * @param authCode the auth code This is optional.
	 *
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void authPayment(String token, String authCode, String client) throws XmlRpcException
	{
		Object[] params = new Object[] {token, authCode, client};
		xmlRpcClient.execute(AUTH_PAYMENT, params);
	}

	/**
	 * This method authorises the payment using DataCash. This method should be called
	 * after the client application has validated any non-payment data, but before it
	 * has done any non-reversable back-end processing, such as taking a holiday out of inventory.
	 *
	 * @param token the token
	 * @param client the client
	 * @param authCode the auth code This is optional.
	 *
	 * @return the datacash reference.
	 *
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public String authSinglePayment(String token, String authCode, String client)
			throws XmlRpcException
			{
		Object[] params = new Object[] {token, authCode, client};
		return (String) xmlRpcClient.execute(AUTH_SINGLE_PAYMENT, params);
			}

	/**
	 * This cancels a previously authorised DataCash payment. The client identifies the DataCash
	 * account number. This method will only be called if the client application cannot
	 * complete the booking or similar. In this case, cancel will be called instead of fulfill.
	 *
	 * @param token the token
	 * @param client the client
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @deprecated As the new cancel method is added which takes the additional parameter
	 * bookingDetails
	 * map that contains "inv_booking_ref" as the key and booking reference number as value which is
	 * required for logging purpose.
	 *
	 * @since 1.0 API
	 */
	public void cancel(String token, String client) throws XmlRpcException
	{
		Object[] params = new Object[] {token, client};
		xmlRpcClient.execute(CANCEL, params);
	}

	/**
	 * This allows a client application to register an error before returning to the CPS.
	 * The use case is as follows:
	 *  The CPS accepts a payment on behalf of a client application, and redirects the browser
	 *  back to the client application.
	 *  The client application gets payment and non-payment data.
	 *  The client application does some additional validation and finds an error.
	 *  The client application registers the error with the CPS, using this method.
	 *  The client application redirects the browser back to the CPS.
	 *  The CPS redisplays the payment page, including the registered error.
	 * It is possible to register more than one error by calling this method multiple times.
	 *
	 * @param token the token
	 * @param errorMsg the error message
	 *
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void registerError(String token, String errorMsg) throws XmlRpcException
	{
		Object[] params = new Object[] {token, errorMsg};
		xmlRpcClient.execute(REGISTER_ERROR, params);
	}

	/**
	 * This clears all payment data(booking information and payment transaction information)
	 * associated with the token. The payment card pan and cvv are overwritten before being
	 * dereferenced.After this method is called, the token is no longer valid. Any call to a
	 * token-related method, with this token, will fail.This method returns either
	 * TRUE or exception. In case of exception client should catch the exception and should
	 * proceed with its application flow.
	 *
	 * @param token the token
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void purgeClientPaymentPageData(String token) throws XmlRpcException
	{
		Object[] params = new Object[] {token};
		xmlRpcClient.execute(PURGE_CLIENT_DATA, params);
	}

	/**
	 * This clears all payment data associated with the token. The payment card pan and cvv
	 * are overwritten before being dereferenced. This will be the last CPS method called by
	 * the client application, for a given payment.This method returns either TRUE or exception.
	 * In case of exception client should catch the exception and should proceed with its
	 * application flow.
	 *
	 * @param token the token
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void purgeSensitiveData(String token) throws XmlRpcException
	{
		Object[] params = new Object[] {token};
		xmlRpcClient.execute(PURGE_SENSITIVE_DATA, params);
	}

	/**
	 * This fulfills an authorised DataCash payment. The token identifies the payment and the
	 * client identifies the DataCash account number. No error is expected from this method.
	 * Since the payment has been authorised, the fulfilment should complete without error.
	 *
	 * @param token the token
	 * @param client the client
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @deprecated As the new fulfil method is added which takes the additional parameter
	 * bookingDetails
	 * map that contains the "inv_booking_ref" as key and booking reference number as value which is
	 * required for logging purpose.
	 *
	 *
	 * @since 1.0 API
	 */
	public void fulfill(String token, String client) throws XmlRpcException
	{
		Object[] params = new Object[] {token, client};
		xmlRpcClient.execute(FULFILL, params);
	}

	/**
	 * This authorises a refund using DataCash. This method should be called after the client
	 * application has validated any non-payment data, but before it has done any non-reversable
	 * back-end processing, such as taking a holiday out of inventory. After completing the
	 * back-end processing, the client application should call fulfill or cancel.
	 *
	 * @param token the token
	 * @param client the client
	 * @param authCode the authorization code
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void authRefund(String token, String authCode, String client) throws XmlRpcException
	{
		Object[] params = new Object[] {token, authCode, client};
		xmlRpcClient.execute(AUTH_REFUND, params);
	}

	/**
	 * This method takes the token, authcode and client as the input parameters and do the payment.
	 *
	 * @param token the token
	 * @param client the client
	 * @param authCode the authorization code
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @deprecated As the new doPayment method is added which takes the additional parameter
	 * bookingDetails
	 * map that contains "inv_booking_ref" as key and booking reference number as value which is
	 * required for logging purpose.
	 *
	 * @since 1.0 API
	 */
	public void doPayment(String token, String authCode, String client) throws XmlRpcException
	{
		Object[] params = new Object[] {token, authCode, client};
		xmlRpcClient.execute(DO_PAYMENT, params);
	}

	/**
	 * This method takes the token, authcode and client as the input parameters and do the refund.
	 *
	 * @param token the token
	 * @param client the client
	 * @param authCode the authorization code
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @deprecated As the new doRefund method is added which takes the additional parameter
	 * bookingDetails
	 * map that contains "inv_booking_ref" as the key and booking reference number as value which is
	 * required for logging purpose.
	 *
	 * @since 1.0 API
	 */
	public void doRefund(String token, String authCode, String client) throws XmlRpcException
	{
		Object[] params = new Object[] {token, authCode, client};
		xmlRpcClient.execute(DO_REFUND, params);
	}

	/**
	 * This method takes the token,client and datacashreference  as the input parameters and
	 * do the refund.
	 *
	 * @param token the token
	 * @param client the datacash client account
	 * @param dataCashReference the dataCashReference
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @deprecated As the new refund method is added which takes the additional parameter
	 * bookingDetails
	 * map that contains "inv_booking_ref" as key and booking reference number as the value which is
	 * required for logging purpose.
	 *
	 * @since 1.0 API
	 */
	public void refund(String token, String client, String dataCashReference)
			throws XmlRpcException
			{
		Object[] params = new Object[] {token, client, dataCashReference};
		xmlRpcClient.execute(REFUND, params);
			}

	/**
	 * This method takes the token,client as the input parameters and do the refund
	 * with the datacashreference.
	 *
	 * @param token the token
	 * @param client the datacash client account
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @deprecated As the new refund method is added which takes the additional parameter
	 * bookingDetails
	 * map that contains "inv_booking_ref" as key and booking reference number as value which is
	 * required for logging purpose.
	 *
	 * @since 1.0 API
	 */
	public void refund(String token, String client)throws XmlRpcException
	{
		Object[] params = new Object[] {token, client};
		xmlRpcClient.execute(REFUND, params);
	}

	/**
	 * Notifies to CPS that booking has been completed.
	 *
	 * @param token the token.
	 * @throws XmlRpcException the xml rpc exception.
	 *
	 * @since 1.0 API
	 */
	public void notifyBookingCompletion(String token) throws XmlRpcException
	{
		Object[] params = new Object[] {token};
		xmlRpcClient.execute(NOTIFY_BOOKING_COMPLETION, params);
	}

	/**
	 * This fulfills an authorized DataCash payment. The token identifies the payment and the
	 * client identifies the DataCash account number. No error is expected from this method.
	 * Since the payment has been authorized, the fulfillment should complete without error.
	 *
	 * @param token the token
	 * @param paymentGatewayVirtualTerminalId the data-cash client account
	 * @param bookingDetails the bookingDetails map which has the "inventory_booking_ref" as the key
	 * and value will be in String format which is exactly the Inventory Reference number.
	 *
	 * @throws XmlRpcException the xml rpc exception
	 *
	 */
	public void fulfil(String token, String paymentGatewayVirtualTerminalId,
			Map<String, String> bookingDetails) throws XmlRpcException
			{
		Object[] params = new Object[] {token, paymentGatewayVirtualTerminalId, bookingDetails};
		xmlRpcClient.execute(FULFIL, params);
			}

	/**
	 * This method takes the token, authcode and client as the input parameters and do the refund.
	 *
	 * @param token the token
	 * @param paymentGatewayVirtualTerminalId the data-cash client account
	 * @param authCode the authorization code
	 * @param bookingDetails the bookingDetails map which has the "inventory_booking_ref" as the key
	 * and value will be in String format which is exactly the Inventory Reference number.
	 *
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void doRefund(String token, String authCode, String paymentGatewayVirtualTerminalId,
			Map<String, String> bookingDetails) throws XmlRpcException
			{
		Object[] params = new Object[] {token, authCode,
				paymentGatewayVirtualTerminalId, bookingDetails};
		xmlRpcClient.execute(DO_REFUND, params);
			}

	/**
	 * This method takes the token,client and datacashreference  as the input parameters and
	 * do the refund.
	 *
	 * @param token the token
	 * @param paymentGatewayVirtualTerminalId the datacash client account
	 * @param dataCashReference the dataCashReference
	 * @param bookingDetails the bookingDetails map which has the "inventory_booking_ref" as the key
	 * and value will be in String format which is exactly the Inventory Reference number.
	 *
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void refund(String token, String paymentGatewayVirtualTerminalId,
			String dataCashReference, Map<String, String> bookingDetails) throws XmlRpcException
			{
		Object[] params = new Object[] {token, paymentGatewayVirtualTerminalId,
				dataCashReference, bookingDetails};
		xmlRpcClient.execute(REFUND, params);
			}

	/**
	 * This method takes the token,client as the input parameters and do the refund
	 * with the datacashreference.
	 *
	 * @param token the token
	 * @param paymentGatewayVirtualTerminalId the datacash client account
	 * @param bookingDetails the bookingDetails map which has the "inventory_booking_ref" as the key
	 * and value will be in String format which is exactly the Inventory Reference number.
	 *
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void refund(String token, String paymentGatewayVirtualTerminalId,
			Map<String, String> bookingDetails)throws XmlRpcException
			{
		Object[] params = new Object[] {token, paymentGatewayVirtualTerminalId, bookingDetails};
		xmlRpcClient.execute(REFUND, params);
			}

	/**
	 * This method takes the token, authcode and client as the input parameters and do the payment.
	 *
	 * @param token the token
	 * @param paymentGatewayVirtualTerminalId the datacash client account
	 * @param authCode the authorization code
	 * @param bookingDetails the bookingDetails map which has the "inventory_booking_ref" as the key
	 * and value will be in String format which is exactly the Inventory Reference number.
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void doPayment(String token, String authCode, String paymentGatewayVirtualTerminalId,
			Map<String, String> bookingDetails) throws XmlRpcException
			{
		Object[] params = new Object[] {token, authCode,
				paymentGatewayVirtualTerminalId, bookingDetails};
		xmlRpcClient.execute(DO_PAYMENT, params);
			}

	/**
	 * This cancels a previously authorised DataCash payment. The client identifies the DataCash
	 * account number. This method will only be called if the client application cannot
	 * complete the booking or similar. In this case, cancel will be called instead of fulfill.
	 *
	 * @param token the token
	 * @param paymentGatewayVirtualTerminalId the datacash client account
	 * @param bookingDetails the bookingDetails map which has the "inventory_booking_ref" as the key
	 * and value will be in String format which is exactly the Inventory Reference number.
	 * @throws XmlRpcException the xml rpc exception
	 *
	 * @since 1.0 API
	 */
	public void cancel(String token, String paymentGatewayVirtualTerminalId,
			Map<String, String> bookingDetails) throws XmlRpcException
			{
		Object[] params = new Object[] {token, paymentGatewayVirtualTerminalId, bookingDetails};
		xmlRpcClient.execute(CANCEL, params);
			}

	/**
	 * This method takes the token,client,dataCashReference and bookingDetails as the input
	 * and does the preRegistered refund.
	 *
	 * @param token the token
	 * @param paymentGatewayVirtualTerminalId the dataCash client account.
	 * @param dataCashReference the dataCashReference number.
	 * @param bookingDetails the bookingDetails map which has the "inventory_booking_ref" as the key
	 * and value will be in String format which is exactly the Inventory Reference number.
	 *
	 * @throws XmlRpcException the xml rpc exception
	 */
	public void preRegisteredRefund(String token, String paymentGatewayVirtualTerminalId,
			String dataCashReference, Map<String, String> bookingDetails) throws XmlRpcException
			{
		Object[] params = new Object[] {token, paymentGatewayVirtualTerminalId,
				dataCashReference, bookingDetails};
		xmlRpcClient.execute(PRE_REGISTERED_REFUND, params);
			}

	/**
	 * This method is to indicate that the price has been  changed due to browser back and forward
	 * button . client applications will call this method to inform cps that the price has been
	 * changed.
	 * @param token the token
	 * @param priceStatus indicates whether price changed or not
	 * @throws XmlRpcException the xml rpc exception
	 *
	 */
	public void  priceChanged(String token, Boolean priceStatus)throws XmlRpcException
	{
		Object[] params = new Object[] {token, priceStatus };
		xmlRpcClient.execute("commonPaymentService.priceChanged", params);
	}

	/**
	 * This clears all payment data associated with the token and removes the token itself.
	 * This method returns either TRUE or exception.
	 *
	 * @param token the token
	 * @throws XmlRpcException the xml rpc exception
	 *
	 */
	public void  purgePaymentToken(String token)throws XmlRpcException
	{
		Object[] params = new Object[] {token};
		xmlRpcClient.execute(PURGE_PAYMENT_TOKEN, params);
	}


	public Map<String,String> verifyAccountDetails(String countryCode, Map verifyAccountDetails)throws XmlRpcException{

		Object[] params = new Object[] {countryCode,verifyAccountDetails};

		Object response=xmlRpcClient.execute(verify_Account_Details, params);

		Map<String,String> result=(Map<String,String>)response;

		return result;
	}

	public Map<String,String> ddSetUpMandate(String token, String currency, String countryLocale,String countryCode, String clientApplication,
			String datacashVTid,Map directDebitDetails)throws XmlRpcException{

		Object[] params = new Object[] {token,currency,countryLocale,countryCode,clientApplication,datacashVTid,directDebitDetails};

		Object response=xmlRpcClient.execute(DD_SETUP_MANDATE, params);

		Map<String,String> result=(Map<String,String>)response;

		return result;
	}

	public Map<String, String> validateAccountDetails(String countryCode, Map<String, String> validateDetails)throws XmlRpcException{

		Object[] params = new Object[] {countryCode,validateDetails};

		Object response=xmlRpcClient.execute(validate_Account_Details, params);

		Map<String,String> result=(Map<String,String>)response;

		return result;
	}


	public PayPalCustomerVerificationDetails verifyPaypalCustomerDetails(String token)throws XmlRpcException{

		Object[] params = new Object[] {token};

		return (PayPalCustomerVerificationDetails)xmlRpcClient.execute("datacash.verifyPaypalCustomerDetails", params);

	}
	/**
	 * This is a wrapper service to authorize a paypal transaction
	 * @param token
	 * @param authorizePayPalTransaction
	 * @return
	 * @throws XmlRpcException
	 */
	public PayPalAuthorizeResponse doAuthorizePayPalTransaction(String token , AuthorizePayPalTransaction authorizePayPalTransaction)throws XmlRpcException{

		Object[] params = new Object[] {token , authorizePayPalTransaction};

		return (PayPalAuthorizeResponse) xmlRpcClient.execute("datacash.doAuthorizePayPalTransaction", params);

	}
	/**
	 * This is a wrapper service to reverse a order or authorization of PayPal transaction
	 * @param token
	 * @param authorizePayPalTransaction
	 * @return
	 * @throws XmlRpcException
	 */
	public PayPalFundReleaseResponse cancelPayPalTransaction(String token , PayPalCustomer paypalCustomer)throws XmlRpcException{

		Object[] params = new Object[] {token , paypalCustomer};

		return (PayPalFundReleaseResponse) xmlRpcClient.execute("datacash.cancelPayPalTransaction", params);

	}


	/**
	 * This is a wrapper service to reverse a order or authorization of PayPal transaction
	 * @param token
	 * @param PayPalCaptureResponse
	 * @return
	 * @throws XmlRpcException
	 */
	public PayPalCaptureResponse doCapturePaypalTransaction(String token,PayPalCaptureRequest paypalCaptureRequest)throws XmlRpcException{

		Object[] params = new Object[] {token , paypalCaptureRequest};

		return (PayPalCaptureResponse) xmlRpcClient.execute("datacash.doCapturePaypalTransaction", params);

	}
	/**
	 * This is a wrapper service to query PayPal transaction
	 * @param token
	 * @param PayPalCaptureResponse
	 * @return
	 * @throws XmlRpcException
	 */
	public PayPalQueryTransactionResponse queryPayPalTransaction(String token,PayPalCustomer paypalCustomer)throws XmlRpcException{

		Object[] params = new Object[] {token , paypalCustomer};

		return (PayPalQueryTransactionResponse) xmlRpcClient.execute("datacash.queryPayPalTransaction", params);

	}

	/**
	 * This is a wrapper service to query updateBookingDetailsForDD
	 * @param token
	 * @param bookingReference
	 * @return
	 * @throws XmlRpcException
	 */
	public void updateBookingDetailsForDD(String token, String bookingReference)throws XmlRpcException{

		Object[] params = new Object[] {token , bookingReference};

		xmlRpcClient.execute(DD_UPDATE_BOOKING_DETAILS, params);
	}

	public Map<String, String> revokeDDSetup(String datacashVTid, Map<String,String>revokeDDSetupDetails) throws XmlRpcException{

        Object[] params = new Object[] {datacashVTid,revokeDDSetupDetails};

        Object response=xmlRpcClient.execute(DD_REVOKE, params);

        Map<String,String> result=(Map<String,String>)response;

        return result;
    }

}
