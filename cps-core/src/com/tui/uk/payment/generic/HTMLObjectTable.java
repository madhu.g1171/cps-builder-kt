/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/
package com.tui.uk.payment.generic;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tui.uk.client.domain.Money;

// TODO: Javadoc should be yupdated for this class.
// This class is used to display the details in generic payment page.
//CHECKSTYLE:OFF
/**
 * @author Kevin Holloway
 *
 */
public class HTMLObjectTable
{

   public HTMLObjectTable()
   {
   }

   public void buildTable(String name, Map<String, Object> map,
      Object value)
   {
      Class<?> klass = value.getClass();
      if (klass.isPrimitive())
      {
         map.put(name, value);
      }
      else if (klass.isEnum())
      {
         map.put(name, value);
      }
      else
      {
         if (String.class.isInstance(value))
         {
            map.put(name, value);
         }
         else if (Integer.class.isInstance(value))
         {
            map.put(name, value);
         }
         else if (BigDecimal.class.isInstance(value))
         {
            map.put(name, value);
         }
         else if (BigInteger.class.isInstance(value))
         {
            map.put(name, value);
         }
         else if (Money.class.isInstance(value))
         {
            Money money = (Money) value;
            map.put(name, money.getCurrency() + " " + money.getAmount());
         }
         else if (value instanceof Map)
         {
            Map<String, Object> map2 = new LinkedHashMap<String, Object>();
            Map<?, ?> mapValues = (Map<?, ?>) value;
            for (Map.Entry<?, ?> mapEntry : mapValues.entrySet())
            {
               buildTable(mapEntry.getKey().toString(), map2, mapEntry.getValue());
            }
            map.put(name, map2);
         }
         else if (List.class.isInstance(value))
         {
            Map<String, Object> map2 = new LinkedHashMap<String, Object>();
            List<?> listValues = (List<?>) value;
            for (int i = 0; i < listValues.size(); i++)
            {
               buildTable(Integer.toString(i), map2, listValues.get(i));
            }
            map.put(name, map2);
         }
         else
         {
            try
            {
               Map<String, Object> map2 =
                  new LinkedHashMap<String, Object>();
               Field[] fields = klass.getDeclaredFields();
               for (Field field : fields)
               {
                  int modifier = field.getModifiers();
                  if ((modifier & (Modifier.FINAL | Modifier.STRICT | Modifier.TRANSIENT)) == 0)
                  {
                     field.setAccessible(true);
                     Object object = field.get(value);
                     if (object != null)
                     {
                        buildTable(field.getName(), map2, object);
                     }
                  }
               }
               map.put(name, map2);
            }
            catch (SecurityException ex)
            {
               throw new RuntimeException(ex);
            }
            catch (IllegalArgumentException ex)
            {
               throw new RuntimeException(ex);
            }
            catch (IllegalAccessException ex)
            {
               throw new RuntimeException(ex);
            }
         }
      }
   }

   private StringBuilder html = new StringBuilder();

   @SuppressWarnings("unchecked")
   public void toHTML(Map<String, Object> map, boolean outer)
   {
      int mapSize = map.size();
      html.append("<table");
      html.append((outer ? " class='outer'" : ""));
      html.append(" cellpadding=\"0\" cellspacing=\"0\">\n");
      for (Map.Entry<String, Object> entry : map.entrySet())
      {
         Object value = entry.getValue();
         html.append("<tr>\n<th");
         html.append((mapSize == 1 ? " class='vlast'" : ""));
         html.append(">\n<span>");
         html.append(entry.getKey());
         html.append("</span></th>\n");
         if (value instanceof Map)
         {
            html.append("<td");
            html.append((mapSize == 1 ? " class='vlast'" : ""));
            html.append(">\n<span>");
            toHTML((Map<String, Object>) value, false);
            html.append("</span></td>\n");
         }
         else
         {
            html.append("<td");
            html.append((mapSize == 1 ? " class='vlast'" : ""));
            html.append(">\n<span>");
            html.append(value.toString());
            html.append("</span></td>\n");
         }
         html.append("</tr>\n");
         mapSize--;
      }
      html.append("</table>\n");
   }

   @SuppressWarnings("unchecked")
   public String toHTML(Object value)
   {
      html = new StringBuilder();
      Map<String, Object> map = new LinkedHashMap<String, Object>();
      buildTable("", map, value);
      if (map.size() == 1)
      {
         map = (Map<String, Object>) map.values().iterator().next();
      }
      toHTML(map, true);
      return html.toString();
   }

   // public static void main (String[] args) {
   // BookingComponent bc = new
   // ExampleBookingComponent().getBookingComponent();
   // HTMLObjectTable xbc = new HTMLObjectTable();
   // String x = xbc.toHTML(bc);
   // System.out.println(x);
   //
   // }
}
//CHECKSTYLE:ON