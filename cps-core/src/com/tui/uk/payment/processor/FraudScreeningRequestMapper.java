/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.AccommodationSummary;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.client.domain.CruiseSummary;
import com.tui.uk.client.domain.FlightDetails;
import com.tui.uk.client.domain.FlightSummary;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningRequest;
import com.tui.uk.payment.service.fraudscreening.domain.Accommodation;
import com.tui.uk.payment.service.fraudscreening.domain.ContactInformation;
import com.tui.uk.payment.service.fraudscreening.domain.Cruise;
import com.tui.uk.payment.service.fraudscreening.domain.Flight;
import com.tui.uk.payment.service.fraudscreening.domain.RequestSource;
import com.tui.uk.payment.service.fraudscreening.domain.Passenger;

/**
 * This class is responsible for creating the <code>FraudScreeningRequest</code> object from the
 * <code>BookingComponent</code> object.
 *
 * @author sindhushree.g
 *
 */
public class FraudScreeningRequestMapper
{

   /** The booking component object. */
   private BookingInfo bookingInfo;

   /** The StringBuilder stringBuilder. */
   private  StringBuilder stringBuilder = new StringBuilder();

   /** The Comma seperator stringBuilder. */
   private static final String COMMA_SEPERATOR = ",";

   /** The Thomson  Name. */
   private static final String THOMSON = "THOMSON";

   /** The First choice Name. */
   private static final String FIRSTCHOICE = "FIRSTCHOICE";


   /** The default flight details. */
    private static final FlightDetails DEFAULT_FLIGHT_DETAILS = new FlightDetails(null, null,
            null, null, null, null, null);
   /**
    * The constructor which sets the booking component.
    *
    * @param bookingInfo the booking info object.
    */
   public FraudScreeningRequestMapper(BookingInfo bookingInfo)
   {
      this.bookingInfo = bookingInfo;
   }

   /**
    * This method is responsible for creating the fraud screening request object.
    *
    * @param bookingSessionIdentifier the booking session identifier.
    *
    * @return fraud screening request object.
    */
   public FraudScreeningRequest createFraudScreeningRequest(String bookingSessionIdentifier)
   {
      FraudScreeningRequest fraudScreeningRequest = new FraudScreeningRequest(
            bookingSessionIdentifier);

      TransactionTrackingData trackingData = bookingInfo.getTrackingData();
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();

       fraudScreeningRequest.setRequestSource(createRequestSource(trackingData,
               bookingComponent));

      String packageType = "";
      if (bookingComponent.getPackageType() != null)
      {
         packageType = bookingComponent.getPackageType().getCode();
      }

      fraudScreeningRequest.setAccommodation(createAccommodation(bookingComponent
            .getAccommodationSummary(), packageType));

      fraudScreeningRequest.setFlight(createFlight(bookingComponent.getFlightSummary()));

      fraudScreeningRequest.setCruise(createCruise(bookingComponent.getCruiseSummary()));

      fraudScreeningRequest.setPassengerDetails(createPassengerDetails(bookingComponent
            .getPassengerRoomSummary(), bookingComponent.getContactInfo()));

       return fraudScreeningRequest;
   }

   /**
    * Creates the accommodation object.
    *
    * @param accommodationSummary the accommodation summary.
    * @param packageType the package type.
    *
    * @return the Accommodation object.
    */
   private Accommodation createAccommodation(AccommodationSummary accommodationSummary,
           String packageType)
   {
      Accommodation accommodation = null;
      if (accommodationSummary != null)
      {
         com.tui.uk.client.domain.Accommodation accommodationInfo = accommodationSummary
            .getAccommodation();

         //TODO : Set proper format for check in and check out date.
         String checkInDate = "";
         String checkOutDate = "";

         // TODO : Remove null checks if all the applications are sending the below info.
         if (accommodationSummary.getStartDate() != null)
         {
            checkInDate = accommodationSummary.getStartDate().toString();
         }
         if (accommodationSummary.getEndDate() != null)
         {
            checkOutDate = accommodationSummary.getEndDate().toString();
         }

         accommodation = new Accommodation(accommodationInfo.getHotel(), accommodationInfo
                  .getSellingCode(), accommodationInfo.getResort(), accommodationInfo
                  .getDestination(), accommodationInfo.getCountry(), checkInDate, checkOutDate,
                  String.valueOf(accommodationSummary.getDuration()), accommodationSummary
                  .getAccommodationInventorySystem(), packageType);

         stringBuilder.append("Hotel Name: ").append(accommodationInfo.getHotel())
         .append(COMMA_SEPERATOR);
         stringBuilder.append("Selling Code:  ").append(accommodationInfo.getSellingCode())
                  .append(COMMA_SEPERATOR);
         stringBuilder.append("Resort: ")
         .append(accommodationInfo.getResort()).append(COMMA_SEPERATOR);
         stringBuilder.append("Destination: ").append(accommodationInfo.getDestination())
                  .append(COMMA_SEPERATOR);
         stringBuilder.append("Country: ").append(accommodationInfo.getCountry())
         .append(COMMA_SEPERATOR);
         stringBuilder.append("Duration: ")
         .append(String.valueOf(accommodationSummary.getDuration())).append(COMMA_SEPERATOR);
         stringBuilder.append("Checkin Date: ").append(checkInDate)
                  .append(COMMA_SEPERATOR);
         stringBuilder.append("Checkout Date: ").append(checkOutDate)
                  .append(COMMA_SEPERATOR);
         stringBuilder.append("Accomodation inventory System: ")
                  .append(accommodationSummary.getAccommodationInventorySystem())
                  .append(COMMA_SEPERATOR);
         stringBuilder.append("Package Type: ").append(packageType).append(COMMA_SEPERATOR);
      }
      return accommodation;
   }

   /**
    * Creates the flight object.
    *
    * @param flightSummary the flight summary object.
    *
    * @return the Flight object.
    */
   private Flight createFlight(FlightSummary flightSummary)
   {
      Flight flight = null;
      if (flightSummary != null)
      {
         List<FlightDetails> outBoundFlight = flightSummary.getOutboundFlight();
         List<FlightDetails> inBoundFlight = flightSummary.getInboundFlight();

         FlightDetails outBoundDepartureFlight;
         FlightDetails outBoundArrivalFlight;
         FlightDetails inBoundDepartureFlight;
         FlightDetails inBoundArrivalFlight;

         if (outBoundFlight != null && outBoundFlight.size() > 0)
         {
            outBoundDepartureFlight = outBoundFlight.get(0);
            outBoundArrivalFlight = outBoundFlight.get(outBoundFlight.size() - 1);
         }
       else
         {
            outBoundDepartureFlight = DEFAULT_FLIGHT_DETAILS;
            outBoundArrivalFlight = DEFAULT_FLIGHT_DETAILS;
         }



         if (inBoundFlight != null && inBoundFlight.size() > 0)
         {
            inBoundDepartureFlight = inBoundFlight.get(0);
            inBoundArrivalFlight = inBoundFlight.get(inBoundFlight.size() - 1);
         }
         else
         {
            inBoundDepartureFlight = DEFAULT_FLIGHT_DETAILS;
            inBoundArrivalFlight = DEFAULT_FLIGHT_DETAILS;
         }

         String outBoundDepartureDate = "";
         String outBoundArrivalDate = "";
         String inBoundDepartureDate = "";
         String inBoundArrivalDate = "";

         if (outBoundDepartureFlight != null)
         {
            if (outBoundDepartureFlight.getDepartureDateTime() != null)
            {
               outBoundDepartureDate = outBoundDepartureFlight.getDepartureDateTime().toString();
            }
         }
         if (outBoundArrivalFlight != null)
         {
            if (outBoundArrivalFlight.getArrivalDateTime() != null)
            {
              outBoundArrivalDate = outBoundArrivalFlight.getArrivalDateTime().toString();
            }
         }
         if (inBoundDepartureFlight != null)
         {
            if (inBoundDepartureFlight.getDepartureDateTime() != null)
            {
               inBoundDepartureDate = inBoundDepartureFlight.getDepartureDateTime().toString();
            }
         }
         if (inBoundArrivalFlight != null)
         {
            if (inBoundArrivalFlight.getArrivalDateTime() != null)
            {
               inBoundArrivalDate = inBoundArrivalFlight.getArrivalDateTime().toString();
            }
         }

         // TODO : The date time format should be set properly.
         flight = new Flight(outBoundDepartureFlight.getOperatingAirlineCode(),
                  outBoundDepartureFlight.getFlightNumber(), outBoundDepartureDate,
                  outBoundDepartureFlight.getDepartureAirportName(), outBoundDepartureFlight
                  .getDepartureAirportCode(), outBoundArrivalDate, outBoundArrivalFlight
                  .getArrivalAirportName(), outBoundArrivalFlight.getArrivalAirportCode(),
                  inBoundDepartureFlight.getFlightNumber(), inBoundDepartureDate,
                  inBoundDepartureFlight.getDepartureAirportName(), inBoundDepartureFlight
                  .getDepartureAirportCode(), inBoundArrivalDate, inBoundArrivalFlight
                  .getArrivalAirportName(), inBoundArrivalFlight.getArrivalAirportCode(),
                  flightSummary.getTicketType());

         stringBuilder.append("Airline Name: ")
         .append(outBoundDepartureFlight .getOperatingAirlineCode())
             .append(COMMA_SEPERATOR);
         stringBuilder.append("Outbound Flight number: ")
         .append(outBoundDepartureFlight.getFlightNumber())
             .append(COMMA_SEPERATOR);
         stringBuilder.append("Out bound departure date time: ").append(outBoundDepartureDate)
             .append(COMMA_SEPERATOR);
         stringBuilder.append("Outbound departure airport name: ")
             .append(outBoundDepartureFlight.getDepartureAirportName()).append(COMMA_SEPERATOR);
         stringBuilder.append("Outbound departure airport code: ")
              .append(outBoundDepartureFlight.getDepartureAirportCode()).append(COMMA_SEPERATOR);
         stringBuilder.append("Outbound arrival flight date time: ")
              .append(outBoundArrivalDate).append(COMMA_SEPERATOR);
         stringBuilder.append("Outbound arrival flight airport name: ")
              .append(outBoundArrivalFlight.getArrivalAirportName()).append(COMMA_SEPERATOR);
         stringBuilder.append("Outbound arrival flight airport code: ")
              .append(outBoundArrivalFlight.getArrivalAirportCode()).append(COMMA_SEPERATOR);
         stringBuilder.append("Inbound departure flight number: ")
              .append(inBoundDepartureFlight.getFlightNumber()).append(COMMA_SEPERATOR);
         stringBuilder.append("Inbound departure flight date time: ")
              .append(inBoundDepartureDate).append(COMMA_SEPERATOR);
         stringBuilder.append("Inbound departure airport name: ")
              .append(inBoundDepartureFlight.getDepartureAirportName()).append(COMMA_SEPERATOR);
         stringBuilder.append("Inbound departure  airport code: ")
             .append(inBoundDepartureFlight.getDepartureAirportCode()).append(COMMA_SEPERATOR);
         stringBuilder.append("Inbound arrival date time: ")
              .append(inBoundArrivalDate).append(COMMA_SEPERATOR);
         stringBuilder.append("Inbound arrival flight airport name: ")
              .append(inBoundArrivalFlight.getArrivalAirportName()).append(COMMA_SEPERATOR);
         stringBuilder.append("Inbound arrival flight airport code: ")
              .append(inBoundArrivalFlight.getArrivalAirportCode()).append(COMMA_SEPERATOR);
         stringBuilder.append("Ticket Type: ")
         .append(flightSummary.getTicketType()).append(COMMA_SEPERATOR);
      }
      return flight;
   }

   /**
    * Creates the cruise object.
    *
    * @param cruiseSummary the cruise summary object.
    *
    * @return the cruise object.
    */
   private Cruise createCruise(CruiseSummary cruiseSummary)
   {
      Cruise cruise = null;
      if (cruiseSummary != null)
      {
         cruise = new Cruise(cruiseSummary.getShip().getHotel(), cruiseSummary.getShip()
              .getSellingCode(), cruiseSummary.getStartPort(), String.valueOf(cruiseSummary
                      .getDuration()));

         stringBuilder.append("Ship Name: ").append(cruiseSummary.getShip().getHotel())
             .append(COMMA_SEPERATOR);
         stringBuilder.append("Ship Selling Code: ")
         .append(cruiseSummary.getShip().getSellingCode()).append(COMMA_SEPERATOR);
         stringBuilder.append("Start Port: ")
         .append(cruiseSummary.getStartPort()).append(COMMA_SEPERATOR);
         stringBuilder.append("Duration: ").append(String.valueOf(cruiseSummary.getDuration()))
             .append(COMMA_SEPERATOR);
      }
      return cruise;
   }

   /**
    * Creates the passenger details.
    *
    * @param passengerRoomSummary the passenger room summary object.
    * @param contactInfo the contact information.
    *
    * @return the list of passengers.
    */
   private List<Passenger> createPassengerDetails(Map<Integer, List<PassengerSummary>>
      passengerRoomSummary, ContactInfo contactInfo)
   {
       Boolean lead;
      List<Passenger> passengerList = new ArrayList<Passenger>();
      if (passengerRoomSummary != null)
      {
         for (List<PassengerSummary> passengerSummaryList : passengerRoomSummary.values())
         {
            for (PassengerSummary passengerSummary : passengerSummaryList)
            {
                lead = false;
                if (passengerSummary.getLeadPassenger() != null)
                {
                    lead = passengerSummary.getLeadPassenger();

                }
               Passenger passenger = new Passenger(passengerSummary.getTitle(), passengerSummary
                        .getForeName(), passengerSummary.getLastName(), passengerSummary
                        .getAgeClassification().getCode(),
                        lead.toString());

               stringBuilder.append("Passenger name: ").append(passengerSummary.getTitle())
                  .append(COMMA_SEPERATOR);
               stringBuilder.append("Passenger forename: ").append(passengerSummary.getForeName())
                     .append(COMMA_SEPERATOR);
               stringBuilder.append("Passenger lastname: ").append(passengerSummary.getLastName())
                  .append(COMMA_SEPERATOR);
               stringBuilder.append("Passenger age code: ")
                  .append(passengerSummary.getAgeClassification().getCode())
                  .append(COMMA_SEPERATOR);
               stringBuilder.append("Lead Passenger: ").append(lead.toString())
                  .append(COMMA_SEPERATOR);

               // Add shipping information only for lead passenger.
               if (lead && contactInfo != null)
               {
                  ContactInformation contactInformation = new ContactInformation(contactInfo
                        .getStreetAddress1(), contactInfo.getStreetAddress2(), contactInfo
                        .getCity(), contactInfo.getCounty(), contactInfo.getPostCode(), contactInfo
                        .getCountry(), contactInfo.getPhoneNumber());
                  contactInformation.setEmailAddress(contactInfo.getEmailAddress());
                  contactInformation.setAlternatePhoneNumber(contactInfo.getAltPhoneNumber());
                  passenger.setShippingInformation(contactInformation);

                  stringBuilder.append("Address 1: ").append(contactInfo.getStreetAddress1())
                     .append(COMMA_SEPERATOR);
                  stringBuilder.append("Address 2: ").append(contactInfo.getStreetAddress2())
                     .append(COMMA_SEPERATOR);
                  stringBuilder.append("City: ")
                  .append(contactInfo.getCity()).append(COMMA_SEPERATOR);
                  stringBuilder.append("County: ")
                  .append(contactInfo.getCounty()).append(COMMA_SEPERATOR);
                  stringBuilder.append("Country: ")
                  .append(contactInfo.getCountry()).append(COMMA_SEPERATOR);
                  stringBuilder.append("Post Code: ").append(contactInfo.getPostCode())
                     .append(COMMA_SEPERATOR);
                  stringBuilder.append("Phone number: ").append(contactInfo.getPhoneNumber())
                     .append(COMMA_SEPERATOR);
                  stringBuilder.append("Alternate Phone number: ")
                  .append(contactInfo.getAltPhoneNumber())
                     .append(COMMA_SEPERATOR);
                  stringBuilder.append("Email: ").append(contactInfo.getEmailAddress())
                     .append(COMMA_SEPERATOR);
               }
               passengerList.add(passenger);
            }
         }
      }
      return passengerList;
   }

   /**
    * Creates the request source.
    *
    * @param trackingData the tracking data.
    * @param bookingComponent the booking component.
    *
    * @return the RequestSource.
    */
   public RequestSource createRequestSource(TransactionTrackingData trackingData,
           BookingComponent bookingComponent)
   {
      String shopId = "";

      if (bookingComponent.getShopDetails() != null)
      {
         shopId = bookingComponent.getShopDetails().getShopId();

         stringBuilder.append("ShopId: ").append(bookingComponent.getShopDetails().getShopId())
             .append(COMMA_SEPERATOR);
      }
      if (bookingComponent.getBookingData() != null)
      {
         if (bookingComponent.getBookingData().getBookingReference() != null)
         {
             stringBuilder.append("Booking Reference Number: ")
             .append(bookingComponent.getBookingData()
                     .getBookingReference());
         }
         else
         {
             stringBuilder.append("Booking Reference Number: ").append("null");
         }
      }

      RequestSource requestSource = new RequestSource(trackingData.getIpAddress(),
            bookingComponent.getBookingSessionIdentifier(), trackingData .getSessionId(),
            trackingData.getToken(), shopId,
            getApplicationName(bookingComponent.getClientApplication()
            .getClientApplicationName()));

      return requestSource;
   }

/**
 * This method returns the brand name.
 * @param appName the appName.
 * @return the appName.
 */
   private String getApplicationName(final String appName)
   {
      if (StringUtils.equalsIgnoreCase(appName, THOMSON))
      {
         return "THOMSONFHPI";
      }
      else if (StringUtils.equalsIgnoreCase(appName, FIRSTCHOICE))
      {
         return "HUGOBYO";
      }
      return appName;
   }

   /**
    *
    * This method is to build a string
    * which contains all client application
    * information.
    *
    * @return the string which
    * contains all client application information
    */
   public String logClientDetails()
   {
       return stringBuilder.toString();
   }

}
