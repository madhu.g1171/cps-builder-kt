/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;

/**
 * This gets the PostPaymentProcessor class, based on the application.
 *
 * @author sindhushree.g
 *
 */
public final class PostPaymentProcessorFactory
{

   /**
    * The default constructor.
    */
   private PostPaymentProcessorFactory()
   {
      // Do nothing.
   }

   /**
    * This method is returns an instance of Post PaymentProcessor based on the client application.
    *
    * @param clientApplication the client application.
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    *
    * @return the instance of PostPaymentProcessor.
    */
   @SuppressWarnings("unchecked")
   public static PostPaymentProcessor getPostPaymentProcessor(ClientApplication clientApplication,
      PaymentData paymentData, Map<String, String> requestParameterMap)
   {
		String className = null;
		String nonHybrisBrands = ConfReader.getConfEntry("nonhybris.brands", null);
		
		if (ConfReader.getBooleanEntry(
				paymentData.getBookingInfo().getBookingComponent().getClientApplication().getClientApplicationName()
						+ ".hcc.switch",
				false)) {

			if (StringUtils.contains(nonHybrisBrands, clientApplication.getClientApplicationName())) {
				className = ConfReader.getConfEntry(clientApplication.getClientApplicationName() + ".postprocessor",
						null);
			} else {
				className = "com.tui.uk.payment.processor.TuiCommonPostPaymentProcessor";
			}

		}else {
			className = ConfReader.getConfEntry(clientApplication.getClientApplicationName()
			         + ".postprocessor", null);			
		}
	   
      if (className == null)
      {
         className = ConfReader.getConfEntry("default.postprocessor", null);
      }

      Class postProcessorClass = null;
      try
      {
         postProcessorClass = Class.forName(className);
      }
      catch (ClassNotFoundException cnfe)
      {
         handleException(cnfe);
      }

      PostPaymentProcessor postPaymentProcessor = null;
      try
      {
         postPaymentProcessor = (PostPaymentProcessor) postProcessorClass.getConstructors()[0]
            .newInstance(paymentData, requestParameterMap);
      }
      catch (IllegalAccessException iae)
      {
         handleException(iae);
      }
      catch (InvocationTargetException ite)
      {
         handleException(ite);
      }
      catch (InstantiationException ise)
      {
         handleException(ise);
      }
      return postPaymentProcessor;
   }

   /**
    * Handles the exception.
    *
    * @param throwable the <code>Throwable</code> object.
    */
   private static void handleException(Throwable throwable)
   {
      LogWriter.logErrorMessage(throwable.getMessage(), throwable);
      throw new RuntimeException(throwable.getMessage());
   }
}
