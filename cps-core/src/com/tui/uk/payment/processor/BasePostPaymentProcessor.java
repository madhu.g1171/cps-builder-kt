/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BasePostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-09-22 08:25:26 $
 *
 * $Author: sindhushree.g@sonata-software.com $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.AMPERSAND;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.QUESTION_MARK;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;

import com.datacash.errors.FailureReport;
import com.tui.uk.client.domain.AccommodationSummary;
import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Discount;
import com.tui.uk.client.domain.EMVAuthorizationResponse;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.client.domain.PassengersDataPanel;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.exception.ValidationException;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;
import com.tui.uk.promotionaldiscount.PromotionalDiscountServiceImpl;
import com.tui.uk.promotionaldiscount.constants.PromotionalDiscountConstants;
import com.tui.uk.promotionaldiscount.domain.AbtaDetails;
import com.tui.uk.promotionaldiscount.domain.PromotionalDiscountRequest;
import com.tui.uk.promotionaldiscount.domain.PromotionalDiscountResponse;
import com.tui.uk.promotionaldiscount.exception.PromotionalDiscountServiceException;
import com.tui.uk.promotionaldiscount.tracs.domain.TracsRequest;
import com.tui.uk.promotionaldiscount.tracs.domain.TracsResponse;
import com.tui.uk.validation.constants.ValidationConstants;
import com.tui.uk.validation.validator.NonPaymentDataValidator;
import com.tui.uk.validation.validator.Validator;

/**
 * This is an abstract for all the applications to perform all the operations required for post
 * payment.
 *
 * @author sindhushree.g
 *
 */
public class BasePostPaymentProcessor implements PostPaymentProcessor
{

   /** The Payment Data associated with this payment. */
   protected PaymentData paymentData;

   /** The request parameter map. */
   protected Map<String, String> requestParameterMap;

   /** The non payment panel data map. */
   protected Map<String, Validator> nonPaymentPanelDataMap;

   /** The BookingInfo object. */
   protected BookingInfo bookingInfo;

   /** The BookingComponent object. */
   protected BookingComponent bookingComponent;

   /** The error message key. */
   protected static final String ERROR_MESSAGE_KEY = "payment.totaltransactionamount.notequal";

   /** The error message key. */
   protected static final String SHOREX_SEND_EMAIL_ERROR_MESSAGE_KEY = "b2bshorex.sendemail.error";

   /** The Payment field Prefix. */
   private static final String PAYMENT_PREFIX = "payment_0";

   /** The selected card name. */
   private static final String SELECTED_CARD = "payment_0_type";

   /** The PAYMENT. */
   private static final String PAYMENT = "payment.";

   /** The DATA_CASH_ERROR. */
   private static final String DATA_CASH_ERROR = ".datacash.error";

   /** The DEFAULT_VALUE. */
   private static final String DEFAULT_VALUE = "default";

   /** The DOT_VALUE. */
   private static final String DOT_VALUE = ".";

   /** The constant THREE_D_SECURE_CONF_VALUE. */
   private static final String THREE_D_SECURE_CONF_VALUE = ".3DSecure";

   /** The constant VALIDATION_LIST. */
   private static final String VALIDATION_LIST = ".validationList";

   /** The constants NO_OF_PIN_PAD_TRANSACTIONS. */
   private static final String NO_OF_PIN_PAD_TRANSACTIONS = "noOfPinPadTransactions";

   /** The constant DATE_FORMATE. */
   private static final String DATE_FORMAT = "dd/MM/yyyy";

   /** The constant for personal details. */
   private static final String PERSONAL_DETAILS = "personaldetails_";

   /** The card type. */
   private static final String PAYMENT_METHOD = "cardtype";

   /** The auth failed code. */
   private static final int AUTH_FAILED_CODE = 7;
   
   
   /** The threeDData map. */
   private Map<String, String> threeDData = new HashMap<String, String>();

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public BasePostPaymentProcessor(PaymentData paymentData,
                                   Map<String, String[]> requestParameterMap)
   {
      this.paymentData = paymentData;
      bookingInfo = paymentData.getBookingInfo();
      if (bookingInfo != null)
      {
         bookingComponent = bookingInfo.getBookingComponent();
      }
      setRequestParameterMap(requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
	{
		//boolean hccSwitch = ConfReader.getBooleanEntry("tuicommon.hcc", false);
		boolean hccSwitch = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
		if (hccSwitch) {
			createHccNonPaymentData();
			updateHccBookingComponent();
			validateHccPaymentMethod();
		} else {
			createNonPaymentData();
			updateBookingComponent();
			validatePaymentMethod();
		}
		validate();
	}

   /**
    * This method is responsible for card holder verification in 3D secure transactions.
    *
    * @param termUrl the termUrl.
    * @param userAgent the userAgent.
    *
    * @return the URL, the URL to which redirection should take place.
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public String postProcess(String termUrl, String userAgent) throws PaymentValidationException,
      PostPaymentProcessorException
   {
      String acsUrl = null;
      String vTid = bookingComponent.getPaymentGatewayVirtualTerminalId();
      //boolean hccSwitch = ConfReader.getBooleanEntry("tuicommon.hcc", false);
		boolean hccSwitch = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);

      if (!bookingInfo.getThreeDAuth() && StringUtils.isNotBlank(vTid))
      {
         String merchantURL = bookingComponent.getClientDomainURL();
         LogWriter.logDebugMessage("Merchant URL  " + merchantURL);
         LogWriter.logDebugMessage("User agent: " + userAgent);
         DataCashPaymentTransaction dataCashPaymentTransaction = null;

         if(hccSwitch) {
        	 dataCashPaymentTransaction =
        	            processHccDataCashPaymentTransactions(vTid, merchantURL, userAgent, bookingComponent
        	               .getClientApplication().getClientApplicationName(), bookingInfo.getTrackingData());

         }else {
        	 dataCashPaymentTransaction =
        	            processDataCashPaymentTransactions(vTid, merchantURL, userAgent, bookingComponent
        	               .getClientApplication().getClientApplicationName(), bookingInfo.getTrackingData());
		}


         if (dataCashPaymentTransaction != null
            && StringUtils.isNotBlank(dataCashPaymentTransaction.getAcsUrl()))
         {
            acsUrl = dataCashPaymentTransaction.getAcsUrl();
            if (StringUtils.isNotBlank(acsUrl))
            {
               String mdUrl = dataCashPaymentTransaction.getMD();
               String paReq = dataCashPaymentTransaction.getPaReq();
               LogWriter.logDebugMessage(DispatcherConstants.PAREQ + paReq);

               String paramSeparator = QUESTION_MARK;

               if (StringUtils.contains(acsUrl, paramSeparator))
               {
                  paramSeparator = AMPERSAND;
               }
               threeDData.put(DispatcherConstants.MD, mdUrl);
               threeDData.put(DispatcherConstants.TERMURL, termUrl);
               threeDData.put(DispatcherConstants.PAREQ, paReq);

               LogWriter.logInfoMessage("Redirect url :" + acsUrl);

            }
         }
      }
      return acsUrl;
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
		try {
			Payment payment = new Payment();
			paymentData.setPayment(payment);
			//boolean hccSwitch = ConfReader.getBooleanEntry("tuicommon.hcc", false);
			boolean hccSwitch = ConfReader.getBooleanEntry(
					bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);

			if (hccSwitch) {
				payment.addPaymentTransaction(
						PaymentTransactionFactory.getHpsPaymentTransactions(0, requestParameterMap, bookingInfo));
			} else {
				payment.addPaymentTransaction(
						PaymentTransactionFactory.getPaymentTransactions(0, requestParameterMap, bookingInfo));

			}

		} catch (PaymentValidationException pve) {
			if (PropertyResource.getProperty(PAYMENT
					+ bookingComponent.getClientApplication().getClientApplicationName() + DOT_VALUE + pve.getMessage(),
					MESSAGES_PROPERTY) != null) {
				throw new PaymentValidationException(
						PAYMENT + bookingComponent.getClientApplication().getClientApplicationName() + DOT_VALUE
								+ pve.getMessage());
			} else {
				throw pve;
			}
		}
	}

   /**
    * This method is responsible to update the calculated payable amount in the booking info with
    * selected deposit amount.
    */
   public void updateSelectedDepositAmount()
   {
      String selectedDeposit = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE);
      if (selectedDeposit != null)
      {
         for (DepositComponent depositComponent : bookingComponent.getDepositComponents())
         {
            // if the selected deposit type is the current DepositComponent's
            // deposit type.
            if (DispatcherConstants.FULL_COST.equalsIgnoreCase(selectedDeposit))
            {
               bookingInfo.setCalculatedPayableAmount(bookingInfo.getCalculatedTotalAmount());
            }
            else if (depositComponent.getDepositType().equalsIgnoreCase(selectedDeposit))
            {
               bookingInfo.setCalculatedPayableAmount(depositComponent.getDepositAmount());
            }
         }
      }
      else
      {
         bookingInfo.setCalculatedPayableAmount(bookingInfo.getCalculatedTotalAmount());
      }
   }

   /**
    * Checks if the amount paid and the payable amounts are equal.
    *
    * @param amount the amount paid.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   protected void validateAmount(String amount) throws PostPaymentProcessorException
   {
      if (StringUtils.isNotBlank(amount))
      {
         if (bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue() != Double
            .parseDouble(amount))
         {
            LogWriter.logErrorMessage(PropertyResource.getProperty(
               "payment.transactionamount.notequal", MESSAGES_PROPERTY));
            bookingInfo.resetCardCharge();
            throw new PostPaymentProcessorException("payment.transactionamount.notequal " + amount
               + " " + bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue());
         }
      }
   }

   /**
    * This method is responsible for creating non payment data.
    */
   protected void createNonPaymentData()
   {
      for (Entry<String, String> entry : requestParameterMap.entrySet())
      {
         String entryKey = entry.getKey();
         if (bookingComponent.getNonPaymentData() == null)
         {
            bookingComponent.setNonPaymentData(new HashMap<String, String>());
         }
         if (!entryKey.startsWith(DispatcherConstants.PAYMENT))
         {
            bookingComponent.getNonPaymentData().put(entryKey, entry.getValue());
         }
      }
   }

   /**
    * This method is responsible for creating non payment data for HCC.
    */
   protected void createHccNonPaymentData()
	{
		Map<String, String> hccRequestParameterMap = new HashMap<String, String>();
		hccRequestParameterMap = bookingComponent.getHccCapturedData();
		for (Entry<String, String> entry : hccRequestParameterMap.entrySet()) {
			String entryKey = entry.getKey();
			if (bookingComponent.getNonPaymentData() == null) {
				bookingComponent.setNonPaymentData(new HashMap<String, String>());
			}
			if (!entryKey.startsWith(DispatcherConstants.PAYMENT)) {
				bookingComponent.getNonPaymentData().put(entryKey, entry.getValue());
			}
		}
	}



   /**
    * Updates the booking component with the data entered in the payment page. Updates passenger
    * information and promotional discount data, if applicable.
    */
   private void updateBookingComponent()
   {
      updatePassengerInformation();

      ContactInfo contactInfo = new ContactInfo();

      if (requestParameterMap.containsKey("mobilePhone"))
      {
         contactInfo.setAltPhoneNumber(requestParameterMap.get("mobilePhone"));
      }
      if (requestParameterMap.containsKey("city"))
      {
         contactInfo.setCity(requestParameterMap.get("city"));
      }
      if (requestParameterMap.containsKey("country"))
      {
         contactInfo.setCountry(requestParameterMap.get("country"));
      }
      if (requestParameterMap.containsKey("county"))
      {
         contactInfo.setCounty(requestParameterMap.get("county"));
      }
      if (requestParameterMap.containsKey("emailAddress"))
      {
         contactInfo.setEmailAddress(requestParameterMap.get("emailAddress"));
      }
      if (requestParameterMap.containsKey("dayTimePhone"))
      {
         contactInfo.setPhoneNumber(requestParameterMap.get("dayTimePhone"));
      }
      if (requestParameterMap.containsKey("postCode"))
      {
         contactInfo.setPostCode(requestParameterMap.get("postCode"));
      }
      if (requestParameterMap.containsKey("houseName"))
      {
         contactInfo.setStreetAddress1(requestParameterMap.get("houseName")
            + requestParameterMap.get("addressLine1"));
      }
      if (requestParameterMap.containsKey("addressLine2"))
      {
         contactInfo.setStreetAddress2(requestParameterMap.get("addressLine2"));
      }

      if (requestParameterMap.containsKey("addressLine1"))
      {
         bookingComponent.setContactInfo(contactInfo);
      }

      Discount discount = new Discount();
      if (bookingComponent.getNonPaymentData().containsKey("promotionalCode"))
      {
         discount.setDiscountId(bookingComponent.getNonPaymentData().get("promotionalCode"));
      }
      if (bookingComponent.getNonPaymentData().containsKey("promoDiscount"))
      {
         discount.setDiscountAmount(bookingComponent.getNonPaymentData().get("promoDiscount"));
      }

      if (bookingComponent.getNonPaymentData().containsKey("promotionalCode"))
      {
         bookingComponent.setDiscount(discount);
      }
   }

   /**
    * Updates the booking component with the data entered in the payment page. Updates passenger
    * information and promotional discount data, if applicable.
    */
   private void updateHccBookingComponent()
   {
      updatePassengerInformation();

      ContactInfo contactInfo = new ContactInfo();

      Map<String, String> hccRequestParameterMap = new HashMap<String, String>();
		hccRequestParameterMap = bookingComponent.getHccCapturedData();

      if (hccRequestParameterMap.containsKey("mobilePhone"))
      {
         contactInfo.setAltPhoneNumber(hccRequestParameterMap.get("mobilePhone"));
      }
      if (hccRequestParameterMap.containsKey("city"))
      {
         contactInfo.setCity(hccRequestParameterMap.get("city"));
      }
      if (hccRequestParameterMap.containsKey("country"))
      {
         contactInfo.setCountry(hccRequestParameterMap.get("country"));
      }
      if (hccRequestParameterMap.containsKey("county"))
      {
         contactInfo.setCounty(hccRequestParameterMap.get("county"));
      }
      if (hccRequestParameterMap.containsKey("emailAddress"))
      {
         contactInfo.setEmailAddress(hccRequestParameterMap.get("emailAddress"));
      }
      if (hccRequestParameterMap.containsKey("dayTimePhone"))
      {
         contactInfo.setPhoneNumber(hccRequestParameterMap.get("dayTimePhone"));
      }
      if (hccRequestParameterMap.containsKey("postCode"))
      {
         contactInfo.setPostCode(hccRequestParameterMap.get("postCode"));
      }
      if (hccRequestParameterMap.containsKey("houseName"))
      {
         contactInfo.setStreetAddress1(hccRequestParameterMap.get("houseName")
            + hccRequestParameterMap.get("addressLine1"));
      }
      if (hccRequestParameterMap.containsKey("addressLine2"))
      {
         contactInfo.setStreetAddress2(hccRequestParameterMap.get("addressLine2"));
      }

      if (hccRequestParameterMap.containsKey("addressLine1"))
      {
         bookingComponent.setContactInfo(contactInfo);
      }

      Discount discount = new Discount();
      if (bookingComponent.getNonPaymentData().containsKey("promotionalCode"))
      {
         discount.setDiscountId(bookingComponent.getNonPaymentData().get("promotionalCode"));
      }
      if (bookingComponent.getNonPaymentData().containsKey("promoDiscount"))
      {
         discount.setDiscountAmount(bookingComponent.getNonPaymentData().get("promoDiscount"));
      }

      if (bookingComponent.getNonPaymentData().containsKey("promotionalCode"))
      {
         bookingComponent.setDiscount(discount);
      }
   }

   /**
    * Updates the passenger information.
    */
   protected void updatePassengerInformation()
   {
      updatePassengerInfo(0);
   }

   /**
    * Updates the passenger information.
    *
    * @param passengerIndex the passenger index.
    */
   protected void updatePassengerInfo(int passengerIndex)
   {

      if (bookingComponent.getPassengerRoomSummary() != null)
      {
         for (List<PassengerSummary> passengerSummaryList : bookingComponent
            .getPassengerRoomSummary().values())
         {
            for (PassengerSummary passengerSummary : passengerSummaryList)
            {
               if (requestParameterMap.containsKey(PERSONAL_DETAILS + passengerIndex + "_title"))
               {
                  passengerSummary.setTitle(requestParameterMap.get(PERSONAL_DETAILS
                     + passengerIndex + "_title"));
               }
               if (requestParameterMap.containsKey(PERSONAL_DETAILS + passengerIndex + "_foreName"))
               {
                  passengerSummary.setForeName(requestParameterMap.get(PERSONAL_DETAILS
                     + passengerIndex + "_foreName"));
               }
               if (requestParameterMap.containsKey(PERSONAL_DETAILS + passengerIndex + "_surName"))
               {
                  passengerSummary.setLastName(requestParameterMap.get(PERSONAL_DETAILS
                     + passengerIndex + "_surName"));
               }
               passengerIndex++;
            }
         }
      }
   }

   /**
    * 1)Form the promotional code object with tracs session id and promotional code field value if
    * the promotional code entered by the user is not validated. 2)Call the promotional code service
    * passing promotional code request. 3)Returns the promotional code response.
    *
    * The PromotionalCodeServiceException is thrown when the promotional code entered is not valid.
    *
    * @param promoCode the promocode.
    * @param balanceType the balanceType like low deposit,deposit and full balance.
    */
   protected void validatePromotionalCode(String promoCode, String balanceType)
   {
      try
      {
         TracsRequest requestMessage = createPromotionalDiscountRequest(promoCode);
         PromotionalDiscountServiceImpl promoCodeService = new PromotionalDiscountServiceImpl();
         TracsResponse responseMessage = promoCodeService.process(requestMessage);
         String promoDiscount = ((PromotionalDiscountResponse) responseMessage).getDiscount();
         if (promoDiscount != null)
         {
            Money discountAmount =
               new Money(new BigDecimal(Double.parseDouble(promoDiscount)), bookingComponent
                  .getTotalAmount().getCurrency());
            String discountType = PromotionalDiscountConstants.PROMOCODE;
            bookingInfo.updateDiscount(discountAmount, discountType, balanceType);
            bookingComponent.getNonPaymentData().put(PromotionalDiscountConstants.IS_PROMO_APPLIED,
               String.valueOf(true));
            bookingComponent.getNonPaymentData().put("promoDiscount", promoDiscount);
            bookingComponent.getNonPaymentData().put(PromotionalDiscountConstants.PROMO_CODE,
               promoCode);
            bookingComponent.getNonPaymentData().put("bookMinusId",
               ((PromotionalDiscountResponse) responseMessage).getTokenId());
            bookingInfo.setCalculatedTotalAmount(bookingComponent.getTotalAmount().subtract(
               bookingInfo.getCalculatedDiscount()));
         }
      }
      catch (PromotionalDiscountServiceException pcse)
      {
         // Even if promotional code discount is not available the user
         // should not be restricted from doing a booking.
         LogWriter.logErrorMessage(pcse.getMessage(), pcse);
      }
   }

   /**
    * This method is responsible for checking whether card charge has been applied and if not will
    * update the card charge.
    *
    * @throws PostPaymentProcessorException the post payment processor exception.
    *
    */
   protected void verifyAndUpdateCardCharge() throws PostPaymentProcessorException
   {
      // Put the paymentTypeCode(e.g. MASTERCARD ) and paymentMethod into request parameter map
      setSelectedPaymentDetails();
      // Transaction amount without card charge.
      Money transactionAmount = getTransactionAmount();
      bookingInfo.setCalculatedPayableAmount(transactionAmount);
      // For non-js payments chargeApplicableForCard will not be same as cardChargePercent
      // which is present in booking info which is set when ajax call is made
      bookingInfo.getUpdatedCardCharge(
         requestParameterMap.get(PAYMENT_PREFIX + PaymentConstants.PAYMENT_TYPE_CODE),
         transactionAmount);
      // Sets transaction amount and card charge into request parameter map
      setTransactonDetails(transactionAmount);
      validateTotalTransactionAmount();
      setTheSelectedDepositType();
   }

   /**
    * Sets paymentTypeCode(e.g. MASTERCARD etc.) and paymentMethod into request parameter map.
    *
    * @return paymentTypeCode
    */
   protected String setSelectedPaymentDetails()
   {
      // Get the selected card type
      String selectedCard = requestParameterMap.get(SELECTED_CARD);
      // From selected card type extract payment type code and payment method
      String paymentTypeCode = StringUtils.split(selectedCard, '|')[0];
      String paymentMethod = StringUtils.split(selectedCard, '|')[1];
      // MASTERCARD , AMERICAN_EXPRESS etc
      requestParameterMap.put(PAYMENT_PREFIX + PaymentConstants.PAYMENT_TYPE_CODE, paymentTypeCode);
      // DCard etc
      requestParameterMap.put(PAYMENT_PREFIX + PaymentConstants.PAYMENT_METHOD, paymentMethod);
      return paymentTypeCode;
   }

   /**
    * Updates request parameter with transaction amount and card charges.
    *
    * @param transactionAmount is payable amount of this booking.
    */
   protected void setTransactonDetails(Money transactionAmount)
   {
      // Set transaction amount determined as above to requestParameterMap
      requestParameterMap.put(PAYMENT_PREFIX + PaymentConstants.TRANSACTION_AMOUNT,
         transactionAmount.getAmount().toString());
      requestParameterMap.put(PAYMENT_PREFIX + PaymentConstants.CHARGE, bookingInfo
         .getCalculatedCardCharge().getAmount().toString());
   }

   /**
    * Reads total transaction amount from requestParameterMap and calls validateAmount() to check
    * whether amount shown on the JSP is equal to amount calculated at server side.
    *
    * @throws PostPaymentProcessorException the post payment processor exception.
    */
   protected void validateTotalTransactionAmount() throws PostPaymentProcessorException
   {
      if (!(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT))
         .equalsIgnoreCase(PaymentConstants.NOT_APPLICABLE))
      {
         validateAmount(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT));
      }
   }

   /**
    * This method is used to check whether an accommodation is hopla THM or PEG.
    *
    * @return boolean, true if hopla otherwise false.
    */
   protected boolean isHopla()
   {
      if (bookingComponent.getAccommodationSummary() != null
         && bookingComponent.getAccommodationSummary().getAccommodationSelected()
         && bookingComponent.getAccommodationSummary().getAccommodationInventorySystem() != null
         && (bookingComponent.getAccommodationSummary().getAccommodationInventorySystem()
            .equalsIgnoreCase("HOPLA_THM") || bookingComponent.getAccommodationSummary()
            .getAccommodationInventorySystem().equalsIgnoreCase("HOPLA_PEG")))
      {
         return true;
      }
      return false;
   }

   /**
    * This method is responsible for non payment data validation.It will iterate on
    * nonPaymentDataPanelMap that contain key as a data panel name and value as a field value.
    *
    * @throws PostPaymentProcessorException if validation fails
    */
   protected void validate() throws PostPaymentProcessorException
   {
      // if you pass true, this method will throw exception if any
      // validation fail.
      // if you pass false, this method will give error map if validation
      // fails.
      try
      {
         if (ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
            .getClientApplicationName() + ".3DSecure", false))
         {
            Validator validator =
               new NonPaymentDataValidator(bookingComponent.getClientApplication());
            validator.validate(getNonPaymentDataMapForValidate(), true);
         }
      }
      catch (ValidationException ve)
      {
         throw new PostPaymentProcessorException(ve.getMessage(), ve);
      }
   }

   /**
    * This method is responsible for getting the total contribution count.
    *
    * @return contributionCount the contribution count
    */
   protected int getContributionCount()
   {
      return Integer.parseInt(requestParameterMap.get(PaymentConstants.PAYMENT
         + PaymentConstants.TRANSACTION_NUMBER));
   }

   /**
    * This method is responsible for getting the pinpad contribution count.
    *
    */
   protected void getPinPadContributionCount()
   {
      final String noOfPinPadTransactions =
         requestParameterMap
            .get(PaymentConstants.PAYMENT + PaymentConstants.PINPADTRANSACTIONCOUNT);
      if (StringUtils.isNotBlank(noOfPinPadTransactions))
      {
         bookingComponent.getNonPaymentData().put(NO_OF_PIN_PAD_TRANSACTIONS,
            noOfPinPadTransactions);
      }
   }

   /**
    * This method is responsible for creating the payments and adding the transactions to payment
    * object. Gets the amount from payment transaction object and validates it against the
    * bookingInfo object's payable amount.
    *
    * @param contributionCount Number of transactions to be processed.
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   protected void createAndProcessPayments(final int contributionCount)
      throws PaymentValidationException, PostPaymentProcessorException
   {
      final Payment payment = new Payment();
      bookingComponent.getNonPaymentData().put(BookingConstants.IS_DATACASH_SELECTED,
         String.valueOf(false));
      paymentData.setPayment(payment);
      for (int i = 0; i < contributionCount; i++)
      {
         if (StringUtils.isNotBlank(requestParameterMap.get(PaymentConstants.PAYMENT + i
            + PaymentConstants.TRANSACTION_AMOUNT)))
         {
            payment.addPaymentTransaction(PaymentTransactionFactory.getPaymentTransactions(i,
               requestParameterMap, bookingInfo));
         }
      }

      BigDecimal totalAmountPaid = BigDecimal.ZERO;
      for (PaymentTransaction paymentTransaction : paymentData.getPayment()
         .getPaymentTransactions())
      {
         totalAmountPaid =
            totalAmountPaid.add(paymentTransaction.getTransactionAmount().getAmount());
         totalAmountPaid =
            totalAmountPaid.add(paymentTransaction.getTransactionCharge().getAmount());
      }
      if (totalAmountPaid.doubleValue() != bookingInfo.getCalculatedPayableAmount().getAmount()
         .doubleValue())
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(ERROR_MESSAGE_KEY,
            MESSAGES_PROPERTY));
         throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
      }
   }

   /**
    * Gets the request parameter map, which is a key-value pair of String, String.
    *
    * @param requestParameterMap the request parameter map.
    */
   private void setRequestParameterMap(Map<String, String[]> requestParameterMap)
   {
      this.requestParameterMap = new HashMap<String, String>();
      for (Entry<String, String[]> paramEntry : requestParameterMap.entrySet())
      {
         this.requestParameterMap.put(paramEntry.getKey(), paramEntry.getValue()[0]);

      }
    }

   /**
    * The method to create PromotionalDiscountRequest.
    *
    * @param promotionalCode the promotionalCode.
    *
    * @return TracsRequest the PromotionalDiscountRequest.
    *
    * @throws PromotionalDiscountServiceException the Exception thrown when connection with TRACS
    *            fails.
    */
   private TracsRequest createPromotionalDiscountRequest(String promotionalCode)
      throws PromotionalDiscountServiceException
   {
      PromotionalDiscountRequest promotionalCodeRequest =
         new PromotionalDiscountRequest(promotionalCode, bookingComponent.getTracsBookMinusId(),
            new AbtaDetails(bookingComponent.getAbtaDetails().getAbtaNumber(), bookingComponent
               .getAbtaDetails().getAbtaUserId(), bookingComponent.getAbtaDetails()
               .getTracsWebSiteId()));
      promotionalCodeRequest.setTracsConnectionUrl(getShadowWebSeverUrl(bookingComponent
         .getPackageType().getCode(), bookingComponent.getClientApplication()
         .getClientApplicationName()));
      return promotionalCodeRequest;
   }

   /**
    * Returns the url that will be used to communicate with TRACS.
    *
    * @param packageType the package type.
    * @param clientAppName the client application name.
    * @return url the Shadow web server url.
    */
   private String getShadowWebSeverUrl(String packageType, String clientAppName)
   {
      String promCodeServiceUrl =
         ConfReader.getConfEntry("promcodeservice." + clientAppName + ".Url", null);
      String webSiteId = ConfReader.getConfEntry("promcodeservice.nonpdp.Url", null);
      if (StringUtils.isNotBlank(packageType)
         && StringUtils.equalsIgnoreCase(packageType, PromotionalDiscountConstants.PDP))
      {
         webSiteId = ConfReader.getConfEntry("promcodeservice.pdp.Url", null);
      }
      promCodeServiceUrl = promCodeServiceUrl + webSiteId;
      if (StringUtils.isBlank(promCodeServiceUrl))
      {
         return ConfReader.getConfEntry("promcodeservice.default.Url", null) + webSiteId;
      }
      return promCodeServiceUrl;
   }

   /**
    * This method is responsible for calculating transaction amount based on deposit selected.
    *
    * @return transactionAmount the transaction amount.
    */
   private Money getTransactionAmount()
   {
      Money transactionAmount = null;
      // Get the deposit type, based on radio button selection
      if (requestParameterMap.get(BookingConstants.DEPOSIT_TYPE) != null)
      {
         transactionAmount = getPaymentAmount();
      }
      /**
       * TODO: Need to modify the condition once all applications start sending hopla bonded
       * attribute
       */
      else if (bookingComponent.getAccommodationSummary() != null
         && bookingComponent.getAccommodationSummary().getHoplaBonded() != null)
      {
         if (Boolean.parseBoolean(bookingComponent.getAccommodationSummary().getHoplaBonded()))
         {
            transactionAmount = bookingInfo.getCalculatedTotalAmount();
         }
         else
         {
            transactionAmount = bookingInfo.getCalculatedPayableAmount();
         }
      }
      else if (isHopla() && bookingComponent.getAccommodationSummary().getPrepay() != null
         && !(bookingComponent.getAccommodationSummary().getPrepay()))
      {
         transactionAmount = bookingInfo.getCalculatedPayableAmount();
      }
      else
      {
         transactionAmount = bookingInfo.getCalculatedTotalAmount();
      }
      return transactionAmount;
   }

   /**
    * This method is used to get the transaction amount based on deposit type.
    *
    * @return transactionAmount the transaction amount.
    */
   private Money getPaymentAmount()
   {
      Money transactionAmount = null;
      String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE).trim();
      String clientApp = null;
      if (bookingComponent.getClientApplication() != null)
      {
         clientApp = bookingComponent.getClientApplication().getClientApplicationName();
      }
      /*
       * TODO Need to remove the condition once all the applications are compatible with one of the
       * versions.
       */
      if (!ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
         .getClientApplicationName() + ".lowDepositFeatureAvailable", false))
      {
         bookingComponent.getNonPaymentData().put(BookingConstants.SELECTED_DEPOSIT_TYPE,
            depositType);
      }
      bookingComponent.getNonPaymentData().put(BookingConstants.DEPOSIT_TYPE, depositType);
      if (StringUtils.equalsIgnoreCase(depositType, DispatcherConstants.FULL_COST))
      {
         transactionAmount = bookingInfo.getCalculatedTotalAmount();
      }
      else
      {
         if ((clientApp != null)
            && (!StringUtils.equalsIgnoreCase(clientApp, "ANCTHFO"))
            && ((StringUtils.equalsIgnoreCase(clientApp, "ANCTHOMSON"))
               || (StringUtils.equalsIgnoreCase(clientApp, "ANCFIRSTCHOICE"))
               || (StringUtils.equalsIgnoreCase(clientApp, "ANCFJFO"))
               || (StringUtils.equalsIgnoreCase(clientApp, "ANCTHCRUISE")) || (StringUtils
                  .equalsIgnoreCase(clientApp, "ANCFALCON"))))
         {
            Money amendmentChargeMoney = bookingComponent.getAmendmentCharge();
            BigDecimal amendmentChargeBD = null;
            if (StringUtils.equalsIgnoreCase(depositType, "onlyAmendCharge"))
            {
               if (amendmentChargeMoney != null)
               {
                  amendmentChargeBD = amendmentChargeMoney.getAmount();
               }
               if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
               {
                  transactionAmount = amendmentChargeMoney;
               }
            }
            else
            {
               List<DepositComponent> depositComponents = bookingComponent.getDepositComponents();
               for (DepositComponent depositComponent : depositComponents)
               {
                  if (StringUtils.equalsIgnoreCase(depositComponent.getDepositType(), depositType))
                  {
                     transactionAmount = depositComponent.getDepositAmount();
                     if (amendmentChargeMoney != null)
                     {
                        amendmentChargeBD = amendmentChargeMoney.getAmount();
                     }
                     if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
                     {
                        if (transactionAmount != null)
                        {
                           transactionAmount = transactionAmount.add(amendmentChargeMoney);
                        }
                     }
                  }
               }
            }
         }
         else
         {
            List<DepositComponent> depositComponents = bookingComponent.getDepositComponents();
            for (DepositComponent depositComponent : depositComponents)
            {
               if (StringUtils.equalsIgnoreCase(depositComponent.getDepositType(), depositType))
               {
                  transactionAmount = depositComponent.getDepositAmount();
               }
            }
         }
      }
	  return transactionAmount;
   }

   /**
    * This method is responsible for performing card holder verification in 3D secure transactions
    * or normal authentication transactions.
    *
    * @param vTid the data-cash account.
    * @param merchantUrl the merchantUrl
    * @param userAgent the userAgent.
    * @param clientAppName the client application name.
    * @param trackingData the transaction tracking data.
    *
    * @return the DataCashPaymentTransaction.
    * @throws PaymentValidationException if validation fails while creating transactions.
    */
   private DataCashPaymentTransaction processDataCashPaymentTransactions(String vTid,
      String merchantUrl, String userAgent, String clientAppName,
      TransactionTrackingData trackingData) throws PaymentValidationException
   {
      DataCashPaymentTransaction dataCashPaymentTransaction = null;
      try
      {
         for (PaymentTransaction paymentTransaction : paymentData.getPayment()
            .getPaymentTransactions(PaymentMethod.DATACASH))
         {
            dataCashPaymentTransaction = (DataCashPaymentTransaction) paymentTransaction;
            // tracking data is being set into payment transaction for logging purpose.
            dataCashPaymentTransaction.setTrackingData(bookingInfo.getTrackingData().toString());
            if (ConfReader.getBooleanEntry(clientAppName + ".3DSecure", false)
               && checkSchemeForThreeDEnable(getThreeDSchemes(clientAppName),
                  dataCashPaymentTransaction.getPaymentTypeCode()))
            {
               LogWriter.logInfoMessage("Sending request to Datacash for card holder verification");
               dataCashPaymentTransaction.cardEnrolmentVerfication(vTid, merchantUrl, userAgent);
               LogWriter.logInfoMessage("Card holder verfication is successful");
            }
            else
            {
               LogWriter.logInfoMessage("Sending request to Datacash for authorization");
               dataCashPaymentTransaction.authPayment(vTid, null);
               LogWriter.logInfoMessage("Datacash authorization is successful");
            }
         }
      }
      catch (DataCashServiceException dcse)
      {
         // paymentData.getPayment().clearSensitiveData();
         // paymentData.getPayment().clearPaymentTransactions();
         LogWriter.logErrorMessage(dcse.getMessage(), dcse);
         String key = PAYMENT + DEFAULT_VALUE + DATA_CASH_ERROR;
         if (StringUtils.isNotBlank(clientAppName))
         {
            key = PAYMENT + clientAppName + DATA_CASH_ERROR;
         }
         throw new PaymentValidationException(dcse.getCode(), key, dcse);
      }
      return dataCashPaymentTransaction;
   }

   /**
    * This method is responsible for performing card holder verification in 3D secure transactions
    * or normal authentication transactions.
    *
    * @param vTid the data-cash account.
    * @param merchantUrl the merchantUrl
    * @param userAgent the userAgent.
    * @param clientAppName the client application name.
    * @param trackingData the transaction tracking data.
    *
    * @return the DataCashPaymentTransaction.
    * @throws PaymentValidationException if validation fails while creating transactions.
    */
   private DataCashPaymentTransaction processHccDataCashPaymentTransactions(String vTid,
      String merchantUrl, String userAgent, String clientAppName,
      TransactionTrackingData trackingData) throws PaymentValidationException
   {
      DataCashPaymentTransaction dataCashPaymentTransaction = null;
      try
      {
         for (PaymentTransaction paymentTransaction : paymentData.getPayment()
            .getPaymentTransactions(PaymentMethod.DATACASH))
         {
            dataCashPaymentTransaction = (DataCashPaymentTransaction) paymentTransaction;
            // tracking data is being set into payment transaction for logging purpose.
            dataCashPaymentTransaction.setTrackingData(bookingInfo.getTrackingData().toString());
            /*
             * here implementation for query transactins will go
             *
             */
            Map<String, String> hcsMap = bookingComponent.getHccsMap();
            String hpsDatacashReference = hcsMap.get("hpsDatacashReference");
            String queryStatus = hcsMap.get("captureStatus");
            //String queryStatus = dataCashPaymentTransaction.hpsQueryTransaction(vTid, bookingComponent.getHccsMap().get("hpsDatacashReference"));

            if(queryStatus.equals("populated")) {
            	if (ConfReader.getBooleanEntry(clientAppName + ".3DSecure", false)
                        && checkSchemeForThreeDEnable(getThreeDSchemes(clientAppName),
                           dataCashPaymentTransaction.getPaymentTypeCode()))
                     {
                        LogWriter.logInfoMessage("Sending request to Datacash for card holder verification");
                        dataCashPaymentTransaction.hccCardEnrolmentVerfication(vTid, merchantUrl, userAgent, hpsDatacashReference);
                        LogWriter.logInfoMessage("Card holder verfication is successful");
                     }
                     else
                     {
                        LogWriter.logInfoMessage("Sending request to Datacash for authorization");
                        //TODO  need to see this
                        dataCashPaymentTransaction.authHccPayment(vTid, null, hpsDatacashReference);
                        LogWriter.logInfoMessage("Datacash authorization is successful");
                     }
            }
         }
      }
      catch (DataCashServiceException dcse)
      {
         // paymentData.getPayment().clearSensitiveData();
         // paymentData.getPayment().clearPaymentTransactions();
         LogWriter.logErrorMessage(dcse.getMessage(), dcse);
         String key = PAYMENT + DEFAULT_VALUE + DATA_CASH_ERROR;
         if (StringUtils.isNotBlank(clientAppName))
         {
            key = PAYMENT + clientAppName + DATA_CASH_ERROR;
         }
         throw new PaymentValidationException(dcse.getCode(), key, dcse);
      }
      return dataCashPaymentTransaction;
   }

   /**
    * This method is responsible for getting list of scheme from conf file.
    *
    * @param clientAppName the client application name.
    * @return list of scheme.
    */
   private List<String> getThreeDSchemes(String clientAppName)
   {
      List<String> schemeList = new ArrayList<String>();
      String[] schemes = ConfReader.getStringValues(clientAppName + ".3DCardScheme", null, ",");
      if (schemes == null)
      {
         schemes = ConfReader.getStringValues("default.3DCardScheme", null, ",");
      }
      for (String scheme : schemes)
      {
         schemeList.add(StringUtils.lowerCase(scheme));
      }
      return schemeList;
   }

   /**
    * This method is responsible for checking, giving scheme is 3D enable.
    *
    * @param threeDCardSchemeList the 3D enable schemes.
    * @param scheme the scheme to check for 3D enable.
    *
    * @return if scheme is 3D enable return true.
    */
   private boolean checkSchemeForThreeDEnable(List<String> threeDCardSchemeList, String scheme)
   {
      boolean isThreeDEnable = false;
      if (StringUtils.isNotBlank(scheme))
      {
         if (threeDCardSchemeList != null
            && threeDCardSchemeList.contains(StringUtils.lowerCase(scheme.replace('_', ' '))))
         {
            isThreeDEnable = true;
         }
      }
      return isThreeDEnable;
   }

   /**
    * This method give non payment data map for non payment validation.
    *
    * @return nonPaymentDataMap the with non payment data.
    */
   protected Map<String, String> getNonPaymentDataMapForValidate()
   {
      Map<String, String> nonPaymentDataMap = bookingComponent.getNonPaymentData();

      List<String> validationList = getValidationList();
      if (validationList != null && isAgeValidationRequired(validationList))
      {
         int seniorPassengerCount = 0;
         int senior2PassengerCount = 0;
         for (Entry<Integer, List<PassengerSummary>> entry : bookingComponent
            .getPassengerRoomSummary().entrySet())
         {
            for (PassengerSummary passengerSummary : entry.getValue())
            {
               switch (passengerSummary.getAgeClassification())
               {
                  case CHILD:
                     setChildInfantData(passengerSummary);
                     break;
                  case INFANT:
                     setChildInfantData(passengerSummary);
                     break;
                  // this check for senior citizen validation, if senior
                  // citizen available, increasing count.
                  // then set to non payment data, this will use in non
                  // payment
                  // data validation.
                  case SENIOR:
                     seniorPassengerCount++;
                     break;
                  case SENIOR2:
                     senior2PassengerCount++;
                  default:
                     // do something
               }
            }
         }
         setArrivalDateDuration();
         nonPaymentDataMap.put(ValidationConstants.SENIOR_COUNT,
            String.valueOf(seniorPassengerCount));
         nonPaymentDataMap.put(ValidationConstants.SENIOR2_COUNT,
            String.valueOf(senior2PassengerCount));
      }
      return nonPaymentDataMap;
   }

   /**
    * If validationList contain age fields then age validation is required.
    *
    * @param validationList the list of brand specific non payment data to be validated.
    * @return true if validation required for age.
    */
   private boolean isAgeValidationRequired(List<String> validationList)
   {
      boolean isAgeValidationRequired = false;
      if (validationList.contains(PassengersDataPanel.CHILD_AGE.getKey())
         || validationList.contains(PassengersDataPanel.INFANT_AGE.getKey())
         || validationList.contains(ValidationConstants.AGEBETWEEN_65_TO_75)
         || validationList.contains(ValidationConstants.AGEBETWEEN_76_TO_84))
      {
         isAgeValidationRequired = true;
      }
      return isAgeValidationRequired;
   }

   /**
    * This method give brand specific non payment data validation list.
    *
    * @return the validationList.
    */
   private List<String> getValidationList()
   {
      List<String> validationList = null;
      if (ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
         .getClientApplicationName() + THREE_D_SECURE_CONF_VALUE, false))
      {
         String[] validationArray =
            ConfReader.getStringValues(bookingComponent.getClientApplication()
               .getClientApplicationName() + VALIDATION_LIST, null, ",");
         if (validationArray != null && validationArray.length > 0)
         {
            validationList = Arrays.asList(validationArray);
         }
      }
      return validationList;
   }

   /**
    * This method is responsible for setting arrival date and duration to nonPaymentDataMap.
    */
   private void setArrivalDateDuration()
   {
      Map<String, String> nonPaymentDataMap = bookingComponent.getNonPaymentData();
      // set duration
      AccommodationSummary accommodationSummary = bookingComponent.getAccommodationSummary();
      nonPaymentDataMap.put(ValidationConstants.DURATION,
         String.valueOf(accommodationSummary.getDuration()));
      // set arrivalDate
      SimpleDateFormat dateFormator = new SimpleDateFormat(DATE_FORMAT);
      if (null != bookingComponent.getFlightSummary())
      {
         nonPaymentDataMap.put(
            ValidationConstants.ARRIVALDATE,
            dateFormator.format(bookingComponent.getFlightSummary().getOutboundFlight().get(0)
               .getArrivalDateTime()));
      }
      else
      {
         nonPaymentDataMap.put(ValidationConstants.ARRIVALDATE,
            dateFormator.format(accommodationSummary.getStartDate()));
      }
   }

   /**
    * This method is responsible for setting child and infant data to nonPaymentDataMap.
    *
    * @param passengerSummary the passengerSummary object.
    */
   private void setChildInfantData(PassengerSummary passengerSummary)
   {
      Map<String, String> nonPaymentDataMap = bookingComponent.getNonPaymentData();
      String ageKey =
         ValidationConstants.PERSONALDETAILS + passengerSummary.getIdentifier()
            + ValidationConstants.AGE_FOR_VALIDATION;
      if (passengerSummary.getAge() != null)
      {
         nonPaymentDataMap.put(ageKey, passengerSummary.getAge().toString());
      }
      else
      {
         if (passengerSummary.getAgeClassification() == AgeClassification.INFANT)
         {
            nonPaymentDataMap.put(ageKey, String.valueOf(1));
         }
      }
      // setting max child and infant age, it will use for DOB validation.
      if (passengerSummary.getMaxAge() != null)
      {
         nonPaymentDataMap.put(
            ValidationConstants.PERSONALDETAILS + passengerSummary.getIdentifier()
               + ValidationConstants.MAX_AGE, String.valueOf(passengerSummary.getMaxAge()));
      }
      if (passengerSummary.getMinAge() != null)
      {
         nonPaymentDataMap.put(
            ValidationConstants.PERSONALDETAILS + passengerSummary.getIdentifier()
               + ValidationConstants.MIN_AGE, String.valueOf(passengerSummary.getMinAge()));
      }
      nonPaymentDataMap
         .put(ValidationConstants.PERSONALDETAILS + passengerSummary.getIdentifier()
            + ValidationConstants.AGECLASSIFICATION, passengerSummary.getAgeClassification()
            .getCode());
   }

   /**
    * Get the threeDData.
    *
    * @return Map of threeDData.
    */
   public Map<String, String> getThreeDData()
   {
      return threeDData;
   }

   /**
    * Set the threeDData.
    *
    * @param threeDData the threeDData
    */
   public void setThreeDData(Map<String, String> threeDData)
   {
      this.threeDData = threeDData;
   }

   /**
    * This method will notify a refund transaction through a java mail when refund cheque payment
    * type is selected.
    *
    * @param transactionAmount the refund cheque transactionAmount.
    *
    * @throws EmailProcessorException the emailProcessorException.
    */
   public void notifyRefundTransaction(Money transactionAmount) throws EmailProcessorException
   {
      // Empty implementation

   }

   /**
    * This method sets the selected deposit type.
    *
    */
   private void setTheSelectedDepositType()
   {
      String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE);
      if (StringUtils.isNotBlank(depositType))
      {
         List<DepositComponent> depositComponents = bookingComponent.getDepositComponents();
         if (depositComponents != null)
         {
            for (DepositComponent depositComponent : depositComponents)
            {
               if (StringUtils.equalsIgnoreCase(depositComponent.getDepositType(),
                  depositType.trim()))
               {
                  depositComponent.setDefaultDepositTypeToBeSelected(Boolean.TRUE);
               }
               else
               {
                  depositComponent.setDefaultDepositTypeToBeSelected(Boolean.FALSE);
               }
            }
         }
      }

   }

   /**
    * This method is responsible for validating the payment method(Credit, Debit or Gift) against
    * the card number type entered by the user.
    *
    * @throws PostPaymentProcessorException the post payment processor exception
    */
   private void validatePaymentMethod() throws PostPaymentProcessorException
   {
      String selectedPaymentMethod = requestParameterMap.get(PAYMENT_METHOD);
      // Ignore the validation when payment method is not available
      if (StringUtils.isEmpty(selectedPaymentMethod))
      {
         return;
      }
      if (StringUtils.isBlank(requestParameterMap.get(SELECTED_CARD)))
      {
         throw new PostPaymentProcessorException("datacash.enter.validcard.number");
      }
      String selectedCardType = StringUtils.split(requestParameterMap.get(SELECTED_CARD), '|')[0];
      String actualPaymentMethod = StringUtils.EMPTY;
      for (final PaymentType paymentType : bookingComponent.getPaymentType().get("CNP"))
      {
         if (StringUtils.equalsIgnoreCase(selectedCardType, paymentType.getPaymentCode()))
         {
            actualPaymentMethod = paymentType.getCardCategory();
            break;
         }
      }

      if (!StringUtils.equalsIgnoreCase(selectedPaymentMethod, "Gift")
         && !StringUtils.equalsIgnoreCase(selectedPaymentMethod, actualPaymentMethod))
      {
         String errorMessage =
            PropertyResource.getProperty("datacash.cardtype.mismatch", MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
      }
   }

   /**
    * This method is responsible for validating the payment method(Credit, Debit or Gift) against
    * the card number type entered by the user.
    *
    * @throws PostPaymentProcessorException the post payment processor exception
    */
   private void validateHccPaymentMethod() throws PostPaymentProcessorException
	{
		Map<String, String> hccRequestParameterMap = new HashMap<String, String>();
		hccRequestParameterMap = bookingComponent.getHccCapturedData();
		String selectedPaymentMethod = hccRequestParameterMap.get(PAYMENT_METHOD);
		// Ignore the validation when payment method is not available
		if (StringUtils.isEmpty(selectedPaymentMethod)) {
			return;
		}
		if (StringUtils.isBlank(hccRequestParameterMap.get(SELECTED_CARD))) {
			throw new PostPaymentProcessorException("datacash.enter.validcard.number");
		}
		//String selectedCardType = StringUtils.split(hccRequestParameterMap.get(SELECTED_CARD), '|')[0];
		String selectedCardType = transform(hccRequestParameterMap.get(SELECTED_CARD),hccRequestParameterMap.get("payment_0_cardNumber"));
		String actualPaymentMethod = StringUtils.EMPTY;
		for (final PaymentType paymentType : bookingComponent.getPaymentType().get("CNP")) {
			if (StringUtils.equalsIgnoreCase(selectedCardType, paymentType.getPaymentCode())) {
				actualPaymentMethod = paymentType.getCardCategory();
				break;
			}
		}

		if (!StringUtils.equalsIgnoreCase(selectedPaymentMethod, "Gift")
				&& !StringUtils.equalsIgnoreCase(selectedPaymentMethod, actualPaymentMethod)) {
			String errorMessage = PropertyResource.getProperty("datacash.cardtype.mismatch", MESSAGES_PROPERTY);
			LogWriter.logErrorMessage(errorMessage);
			throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
		}
	}

/**
    * This method is responsible for updating the DPN data.
    */
   protected void captureDPNValues()
   {
      String communicateByEmail = (requestParameterMap.get("communicateByEmail"));
      String communicateByPhone = requestParameterMap.get("communicateByPhone");
      String communicateByPost = requestParameterMap.get("communicateByPost");
      String chkThirdPartyMarketingAllowed =
         requestParameterMap.get("chkThirdPartyMarketingAllowed");

      if (communicateByEmail == null)
      {
         bookingComponent.getNonPaymentData().put("communicateByEmail", "false");
      }
      else
      {
         bookingComponent.getNonPaymentData().put("communicateByEmail", "true");
      }
      if (communicateByPhone == null)
      {
         bookingComponent.getNonPaymentData().put("communicateByPhone", "false");
      }
      else
      {
         bookingComponent.getNonPaymentData().put("communicateByPhone", "true");
      }
      if (communicateByPost == null)
      {
         bookingComponent.getNonPaymentData().put("communicateByPost", "false");
      }
      else
      {
         bookingComponent.getNonPaymentData().put("communicateByPost", "true");
      }
      if (chkThirdPartyMarketingAllowed == null)
      {
         bookingComponent.getNonPaymentData().put("chkThirdPartyMarketingAllowed", "false");
      }
      else
      {
         bookingComponent.getNonPaymentData().put("chkThirdPartyMarketingAllowed", "true");

      }

   }
	/**
	 * The method to validate the newly introduced gift card(MasterCard)
	 *
	 * @throws PostPaymentProcessorException
	 *
	 */
	public void validateGiftCard_MasterCard() throws PostPaymentProcessorException{

		String isMasterCardGiftAppliedCheck = ConfReader.getConfEntry("isMasterCardGiftApplied", null);

		if(isMasterCardGiftAppliedCheck=="true"){

			String confMasterCard = "MasterCardTUIGiftCard.BINRange";
			String giftCardNumberSelectedMasterCard = null;
			String giftCardNumberConfigurationMasterCard = ConfReader.getConfEntry(confMasterCard, null);
			String[] giftCardNumberListMasterCard = giftCardNumberConfigurationMasterCard.split(",");
			String cardNumber = requestParameterMap.get("payment_0_cardNumber");
			for (String giftCardNumberMasterCard : giftCardNumberListMasterCard)
			{
				if (cardNumber.startsWith(giftCardNumberMasterCard))
				{
					giftCardNumberSelectedMasterCard = giftCardNumberMasterCard;
					break;
				}
			}
			if (giftCardNumberSelectedMasterCard != null)
			{
				if (!(requestParameterMap.get("payment_0_type").contains("MASTERCARD_GIFT|Dcard")))
				{
					throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
				}
			}
		}
	}
   protected void validateGiftCard() throws PostPaymentProcessorException
   {
      String conf = "TUIGiftCard.BINRange";
      String giftCardNumberSelected = null;
      String giftCardNumberConfiguration = ConfReader.getConfEntry(conf, null);
      String cardNumber = requestParameterMap.get("payment_0_cardNumber");
      String[] giftCardNumberList = giftCardNumberConfiguration.split(",");
      if (StringUtils.isBlank(requestParameterMap.get(SELECTED_CARD)))
      {
         throw new PostPaymentProcessorException("datacash.enter.validcard.number");
      }
      for (String giftCardNumber : giftCardNumberList)
      {
         if (cardNumber.startsWith(giftCardNumber))
         {
            giftCardNumberSelected = giftCardNumber;
            break;
         }
      }
      if (giftCardNumberSelected != null)
      {
         if (!(requestParameterMap.get("payment_0_type").contains("MAESTRO")))
         {
            throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
         }
      }
      else
      {
         if (requestParameterMap.get("payment_0_type").contains("MAESTRO"))
         {
            throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
         }
      }
		validateGiftCard_MasterCard();
   }



   /**
    * @param string
    * @return
    */
   private String transform(String cardScheme, String pan)
   {

      switch (cardScheme)
      {
         case "VISA":
            cardScheme = "VISA";
            break;
         case "VISA Debit":
            cardScheme = "VISA_DELTA";
            break;
         case "Debit Mastercard":
            cardScheme = "DEBIT_MASTERCARD";
            break;
         case "Mastercard":
            cardScheme = findMastercardType(pan);
            break;
         case "Maestro":
            cardScheme = "MAESTRO";
            break;
         case "VISA Electron":
            cardScheme = "VISA_ELECTRON";
            break;
         case "American Express":
            cardScheme = "AMERICAN_EXPRESS";
            break;
         case "VISA Purchasing":
             cardScheme = "VISA_PURCHASING";
             break;

      }
      return cardScheme;
   }

   public String findMastercardType(String cardNumber)
   {
      String cardType = "MASTERCARD";
      String thomsonCardRange = ConfReader.getConfEntry("ThomsonCreditcard.BINRange", null);
      String[] thomsonCardNumberList = null;
      if (null != thomsonCardRange && StringUtils.isNotEmpty(thomsonCardRange))
      {
         thomsonCardNumberList = thomsonCardRange.split(",");
      }
      if (null != thomsonCardNumberList && StringUtils.isNotEmpty(thomsonCardRange))
      {
         for (String thomsonCardNumber : thomsonCardNumberList)
         {
            if (thomsonCardNumber.startsWith(cardNumber.substring(0, 6)))
            {
               return "TUI_MASTERCARD";
            }
         }
      }
      String masterCardRange = ConfReader.getConfEntry("MasterCardTUIGiftCard.BINRange", null);
      String[] masterCardRangeList = null;
      if (null != masterCardRange && StringUtils.isNotEmpty(masterCardRange))
      {
         masterCardRangeList = masterCardRange.split(",");
      }
      if (null != masterCardRangeList && StringUtils.isNotEmpty(masterCardRange))
      {
         for (String masterCardNumber : masterCardRangeList)
         {
            if (masterCardNumber.startsWith(cardNumber.substring(0, 6)))
            {
               return "MASTERCARD_GIFT";
            }
         }
      }
      return cardType;

   }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tui.uk.payment.processor.PostPaymentProcessor#emv3DSPostProcess(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public void emv3DSPostProcess(String vTid, String token)
			throws DataCashServiceException

	{
		DataCashPaymentTransaction datacashPaymentTransaction = (DataCashPaymentTransaction) paymentData
				.getPayment().getPaymentTransactions().get(0);
		EMVAuthorizationResponse emvAuthResponse = datacashPaymentTransaction
				.doEmv3DSAuthorisation(vTid, ConfReader.getConfEntry(
						bookingComponent.getPaymentGatewayVirtualTerminalId(),
						null), bookingComponent.getPayableAmount()
						.getCurrency().getCurrencyCode(), 
						emv3dsTotalAmount(bookingComponent),
						bookingComponent.getClientApplication()
								.getCaptureMethod(), bookingComponent
								.getHccsMap().get("hpsDatacashReference"),
						token);

		datacashPaymentTransaction.processPostEMVChallengeFlow(emvAuthResponse,
				vTid);

		FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
				token);
		if (!fraudScreeningProcessor.processFraudScreening()) {
			LogWriter.logInfoMessage("Fraud screening failed for the token="
					+ token);
			throw new DataCashServiceException(AUTH_FAILED_CODE,
					"Not Authorised");
		}
	}

	  private String emv3dsTotalAmount(BookingComponent bookingComponent)
	   {

	      Map<String, String> hccQueryResponseMap = bookingComponent.getHccCapturedData();

	      BigDecimal transAmt =
	         new BigDecimal(hccQueryResponseMap.get("payment_0" + PaymentConstants.TRANSACTION_AMOUNT));
	      BigDecimal transChrge = new BigDecimal("0.0");
	      Currency currency = Currency.getInstance(hccQueryResponseMap.get("CURRENCY"));
	      Money transactionAmount = new Money(transAmt, currency);
	      Money transactionCharge = new Money(transChrge, currency);
	      BigDecimal totalAmount = (transactionAmount.add(transactionCharge)).getAmount().abs();
	      return totalAmount.toString();

	   }

}
