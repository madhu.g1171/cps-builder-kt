/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ThomsonPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for Thomson application.
 *
 * @author sindhushree.g
 *
 */
public class ThomsonPostPaymentProcessor extends BasePostPaymentProcessor
{

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public ThomsonPostPaymentProcessor(PaymentData paymentData,
                                      Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      String promoCode = bookingComponent.getNonPaymentData().get("promotionalCode");
      if (StringUtils.isNotBlank(promoCode)
         && !Boolean.parseBoolean(bookingComponent.getNonPaymentData().get("isPromoCodeApplied")))
      {
         validatePromotionalCode(promoCode,
            requestParameterMap.get(DispatcherConstants.BALANCE_TYPE));
      }

      //boolean isHccTrue = ConfReader.getBooleanEntry("tuicommon.hcc", false);
		boolean isHccTrue = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);

      if(!isHccTrue) {
    	  verifyAndUpdateCardCharge();
      }
      captureDPNValues();
      super.process();

      // For Hopla Post pay accommodations, if datacash payment is also available, we are
      // creating the data cash transaction also.
      if (bookingComponent.getAccommodationSummary() != null)
      {
         /**
          * TODO: Need to modify the condition once all applications start sending hopla bonded
          * attribute
          */
         if (bookingComponent.getAccommodationSummary().getHoplaBonded() != null)
         {
            if (!Boolean.parseBoolean(bookingComponent.getAccommodationSummary().getHoplaBonded()))
            {
               createDataCashTransaction();
            }
         }
         else if (isHopla())
         {
            createDataCashTransaction();
         }
      }
   }

   /**
    * This method is used to create data cash payment transaction for hopla postpay if transaction
    * amount is greater than zero.
    *
    * @throws PaymentValidationException the PaymentValidationException.
    * @throws PostPaymentProcessorException the PostPaymentProcessorException.
    */
   private void createDataCashTransaction() throws PaymentValidationException,
      PostPaymentProcessorException
   {
      if (requestParameterMap.get(
         PaymentConstants.PAYMENT + "0" + PaymentConstants.TRANSACTION_AMOUNT).length() > 0)
      {
         requestParameterMap.put(PaymentConstants.PAYMENT + "0" + PaymentConstants.PAYMENT_METHOD,
            PaymentMethod.DATACASH.getCode());
         PaymentTransaction paymentTransaction =
            PaymentTransactionFactory.getPaymentTransactions(0, requestParameterMap, bookingInfo);
         paymentData.getPayment().addPaymentTransaction(paymentTransaction);
         bookingInfo.setThreeDAuth(Boolean.FALSE);
      }
   }
}
