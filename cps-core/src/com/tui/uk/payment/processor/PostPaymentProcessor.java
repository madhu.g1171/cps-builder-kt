/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import java.io.IOException;
import java.util.Map;

import org.jdom.JDOMException;

import com.datacash.errors.FailureReport;
import com.tui.uk.client.domain.Money;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;


/**
 * This is an interface for all the applications to perform all the operations
 * required for post payment.
 *
 * @author sindhushree.g
 *
 */
public interface PostPaymentProcessor
{

   /**
    * This method is responsible for pre process of the data entered in the payment page.
    * This may include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   void preProcess() throws PostPaymentProcessorException;

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   void process() throws PaymentValidationException, PostPaymentProcessorException;

   /**
    * This method is responsible for card holder verification in 3D secure transactions.
    *
    * @param termUrl the termUrl.
    * @param userAgent the userAgent.
    *
    * @return url the url to which redirection should take place.
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   String postProcess(String termUrl, String userAgent)
    throws PaymentValidationException, PostPaymentProcessorException;
   
   /**
    * Get the threeDData.
    *
    * @return Map of threeDData.
    */
   Map<String, String> getThreeDData();

   /**
    * Set the threeDData.
    *
    * @param threeDData the threeDData
    */
   void setThreeDData(Map<String, String> threeDData);

   /**
    * This method will notify a refund transaction through a java mail when refund
    * cheque payment type is selected.
    *
    * @param transactionAmount the refund cheque transactionAmount.
    *
    * @throws EmailProcessorException the emailProcessorException.
    */
   void notifyRefundTransaction(Money transactionAmount) throws EmailProcessorException;
   
   void emv3DSPostProcess(String vTid, String token) throws DataCashServiceException;
}