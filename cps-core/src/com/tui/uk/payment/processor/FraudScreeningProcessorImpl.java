/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.processor;

import java.math.BigDecimal;
import java.util.*;

import com.tui.uk.payment.service.fraudscreening.domain.*;
import org.apache.commons.lang.StringUtils;

import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.cacheeventlistener.TransactionStatusUtil;
import com.tui.uk.client.domain.AuthorizePayPalTransaction;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PayPalCaptureRequest;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BillingAddress;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.CnpCard;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.FailedCardData;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningCardData;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningRequest;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningResponse;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningServiceFactory;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningServiceImpl;
import com.tui.uk.payment.service.fraudscreening.exception.FraudScreeningServiceException;

/**
 * This class is responsible for calling the service. It creates necessary data required for calling
 * the service and for handling the response from the fraud screening service.
 *
 * @author sindhushree.g
 *
 */
public class FraudScreeningProcessorImpl implements FraudScreeningProcessor
{

	/** The token. */
	private String token;

	/**
	 * Constructor, which takes the token.
	 *
	 * @param token the token.
	 */
	public FraudScreeningProcessorImpl(String token)
	{
		this.token = token;
	}

	/**
	 * This method is responsible for calling fraud screening service and handling the response
	 * appropriately.
	 *
	 * @return false in case of Reject response, true in all other cases.
	 */
	public boolean processFraudScreening()
	{
		boolean fraudScreeningStatus = true;

		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(
				token));
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		String hybrisSessionId=getHybrisSessionId(paymentData);
		String clientApplication = bookingInfo.getBookingComponent().getClientApplication()
				.getClientApplicationName();

		if (getBrandConfEntry("switch", clientApplication, false))
		{
			FraudScreeningRequestMapper fraudScreeningRequestMapper = new FraudScreeningRequestMapper(
					bookingInfo);

			FraudScreeningRequest fraudScreeningRequest = fraudScreeningRequestMapper
					.createFraudScreeningRequest(paymentData.getBookingSessionId());
			fraudScreeningRequest.setPaymentInformation(createPaymentInformation(paymentData));

			FraudScreeningServiceImpl fraudScreeningService = FraudScreeningServiceFactory
					.updateFraudScreeningServiceImpl(paymentData.getBookingSessionId(),
							fraudScreeningRequest);

			String fraudScreeningResponseCode = "Not Sent";

			try
			{
				FraudScreeningResponse response = null;
				BookingData bookingData = bookingInfo.getBookingComponent().getBookingData();
				if (bookingData != null && bookingData.getBookingReference() != null)
				{
					BigDecimal transactionAmount = BigDecimal.ZERO;
					for (PaymentTransaction paymentTransaction : paymentData.getPayment()
							.getPaymentTransactions(PaymentMethod.DATACASH))
					{
						DataCashPaymentTransaction datacashPaymentTransaction =
								(DataCashPaymentTransaction) paymentTransaction;
						transactionAmount = transactionAmount.add(datacashPaymentTransaction
								.getTransactionAmount().getAmount());
					}
					OrderInformation orderInfo = new OrderInformation(bookingData.getBookingReference(),
							new Date().toString(), transactionAmount.toString());
					fraudScreeningRequest.setOrderInformation(orderInfo);
					LogWriter.logInfoMessage("Sending Fulfill Fraud Screening request for "
							+ clientApplication + " : " + token+",JSESSIONID:"+hybrisSessionId);
					response = fraudScreeningService.send(orderInfo);
				}
				else
				{
					LogWriter.logInfoMessage("Sending Auth Fraud Screening request for "
							+ clientApplication + " : " + token+",JSESSIONID:"+hybrisSessionId);
					response = fraudScreeningService.send();
				}
				//Checked null for response.getRecommendationCode()
				if (null != response.getRecommendationCode() && "" != response.getRecommendationCode()
						&& !"NULL".equalsIgnoreCase(response.getRecommendationCode()))
				{
					fraudScreeningResponseCode = response.getRecommendationCode();
				}
				LogWriter.logInfoMessage("Fraud Screening Response:" + fraudScreeningResponseCode
						+ " for " + clientApplication + "," + token+",JSESSIONID:"+hybrisSessionId);
				//Checked null for response.getRecommendationCode()
				if (null != response.getRecommendationCode() && "" != response.getRecommendationCode()
						&& !"NULL".equalsIgnoreCase(response.getRecommendationCode())
						&& response.getRecommendationCode().equalsIgnoreCase(ConfReader.getConfEntry(
								"fraudscreening.reject.code", "DECLINED")) && !getBrandConfEntry(
										"response.ignore", clientApplication, true))
				{
					fraudScreeningStatus = false;
				}
				if ((bookingData != null && bookingData.getBookingReference() != null))
				{
					if (fraudScreeningStatus)
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_SUCCESS"));
					}
					else
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_FAILURE"));
					}
				}
				else
				{
					if (fraudScreeningStatus)
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_PRE_AUTH_SUCCESS"));
					}
					else
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_PRE_AUTH_FAILURE"));
					}
				}
			}
			catch (FraudScreeningServiceException fse)
			{
				LogWriter.logErrorMessage(fse.getMessage());
				LogWriter.logInfoMessage("Fraud Screening Request not sent for:" + clientApplication
						+ "," + token+",JSESSIONID:"+hybrisSessionId);
			}
			fraudScreeningService.logFraudScreeningRequest("auth", fraudScreeningResponseCode);
			LogWriter.logDebugMessage(fraudScreeningRequestMapper.logClientDetails());
			bookingInfo.setFraudScreeningResponse(fraudScreeningResponseCode);
		}
		return fraudScreeningStatus;
	}

	/**
	 * This method is responsible for updating the card details when authorisation fails.
	 */
	public void updateFailureCardData()
	{
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
		List<FailedCardData> failedCardDataList = new ArrayList<FailedCardData>();
		for (PaymentTransaction paymentTransaction : paymentData.getPayment().getPaymentTransactions(
				PaymentMethod.DATACASH))
		{
			DataCashPaymentTransaction dataCashPaymentTransaction =
					(DataCashPaymentTransaction) paymentTransaction;
			FailedCardData failedCardData = new FailedCardData(dataCashPaymentTransaction.getCard(),
					dataCashPaymentTransaction.getTransactionAmount(), dataCashPaymentTransaction
					.getTransactionCharge());
			failedCardData.setAuthorizationCode(dataCashPaymentTransaction.getAuthCode());
			failedCardData.setAuthorizedBy3dSecurity(dataCashPaymentTransaction.getThreeDSStatus());
			failedCardData.setAvsResponse(dataCashPaymentTransaction.getAvsResponse());

			if (failedCardData.getCnpCard().getAddress() != null && paymentData.getBookingInfo()
					.getBookingComponent().getContactInfo() != null)
			{
				failedCardData.getCnpCard().getAddress().setCountryCode(paymentData.getBookingInfo()
						.getBookingComponent().getContactInfo().getCountry());
				failedCardData.setBillingPhoneNumber(paymentData.getBookingInfo().getBookingComponent()
						.getContactInfo().getPhoneNumber());
			}
			failedCardData.setCardScheme(dataCashPaymentTransaction.getCardScheme());
			failedCardData.setCountry(dataCashPaymentTransaction.getCountry());
			failedCardData.setCvvresponse(dataCashPaymentTransaction.getCvvResponse());
			failedCardData.setIssuer(dataCashPaymentTransaction.getIssuer());
			failedCardData.setMerchantReferenceNumber(dataCashPaymentTransaction
					.getMerchantReference());
			failedCardData.setPaymentGatewayreferenceNumber(dataCashPaymentTransaction
					.getDatacashReference());
			failedCardData.setPaymentGatewayResponseCode(dataCashPaymentTransaction
					.getDataCashResponseCode());
			failedCardData.setReferralCode(dataCashPaymentTransaction.getReferralCode());
			failedCardData.setResponseMsg(dataCashPaymentTransaction.getResponseMsg());

			failedCardDataList.add(failedCardData);
		}
		paymentData.setFailedCardDataList(failedCardDataList);
	}

	/**
	 * This method is responsible for updating booking related information to the fraud screening
	 * application once booking is completed.
	 * @param bookingReference the booking reference.
	 * @param totalAmount the total amount.
	 */
	public void updateBookingDetails(String bookingReference, Money totalAmount)
	{
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		String clientApplication = bookingInfo.getBookingComponent().getClientApplication()
				.getClientApplicationName();

		if (getBrandConfEntry("switch", bookingInfo.getBookingComponent().getClientApplication()
				.getClientApplicationName(), false) && ((bookingInfo.getBookingComponent()
						.getBookingData() == null || bookingInfo.getBookingComponent()
						.getBookingData().getBookingReference() == null)
						|| !(StringUtils.equals(bookingInfo.getBookingComponent()
								.getBookingData().getBookingReference(), bookingReference))))
		{
			String amt = null;
			if(totalAmount != null){
				amt = totalAmount.getAmount().toString();
			}
			OrderInformation orderInformation = new OrderInformation(bookingReference, String.valueOf(
					new Date()), amt);
			FraudScreeningServiceImpl fraudScreeningService =
					FraudScreeningServiceFactory.getFraudScreeningServiceImpl(
							paymentData.getBookingSessionId());
			String fraudScreeningResponseCode = "Not Sent";
			try
			{
				LogWriter.logInfoMessage("Sending Fulfill Fraud Screening request for "
						+ clientApplication + " : " + token);
				FraudScreeningResponse response = fraudScreeningService.send(orderInformation);

				if (response != null)
				{
					fraudScreeningResponseCode = response.getRecommendationCode();
					if (!(fraudScreeningResponseCode.equalsIgnoreCase("Not Sent")))
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_SUCCESS"));
					}
				}
			}

			catch (FraudScreeningServiceException fse)
			{
				new TransactionStatusUtil().updateStatus(paymentData, token,
						paymentData.getTransactionStatus(),
						TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_FAILURE"));
				LogWriter.logErrorMessage(fse.getMessage());
			}

			fraudScreeningService.logFraudScreeningRequest("fulfil", fraudScreeningResponseCode);
			if(totalAmount != null)
			{
				fraudScreeningService.purgeSensitiveCardData();
				paymentData.purgeSensitiveFailedCardData();
				FraudScreeningServiceFactory.remove(paymentData.getBookingSessionId());
			}
		}
	}

	/**
	 * Creates the payment information object.
	 * @param paymentData the payment data object.
	 * @return the payment information object.
	 */
	private PaymentInformation createPaymentInformation(PaymentData paymentData)
	{

		BookingInfo bookingInfo = paymentData.getBookingInfo();
		List<FraudScreeningCardData> fraudScreeningCardDataList =
				new ArrayList<FraudScreeningCardData>();
		for (FailedCardData failedCardData : paymentData.getFailedCardDataList())
		{
			String phoneNumber;
			AuthInfo authInfo = new AuthInfo(failedCardData.getAuthorizationCode(), failedCardData
					.getAvsResponse(), failedCardData.isAuthorizedBy3dSecurity(), failedCardData
					.getPaymentGatewayreferenceNumber(), failedCardData.getMerchantReferenceNumber(),
					failedCardData.getPaymentGatewayResponseCode(), failedCardData.getReferralCode(),
					failedCardData.getIssuer(), failedCardData.getCardScheme(), failedCardData.getCountry(),
					failedCardData.getCvvResponse(), failedCardData.getResponseMsg());

			CnpCard cnpCard = failedCardData.getCnpCard();

			FraudScreeningCard fraudScreeningCard = new FraudScreeningCard(cnpCard.getPan(), cnpCard
					.getNameOncard(), cnpCard.getExpiryDate());

			BillingAddress address = cnpCard.getAddress();
			ContactInformation contactInformation = null;

			/*This part of the code has to be
			 * removed once all the client application
			 * will start sending the phoneNumber
			 */
			if (bookingInfo.getBookingComponent()
					.getContactInfo() == null)
			{
				phoneNumber = StringUtils.EMPTY;
			}
			else
			{
				phoneNumber = bookingInfo.getBookingComponent()
						.getContactInfo().getPhoneNumber();

			}
			if (address != null)
			{
				contactInformation = new ContactInformation(address.getStreetAddress1(),
						address.getStreetAddress2(), address.getStreetAddress3(), address.getStreetAddress4(),
						cnpCard.getPostCode(), address.getCountry(), phoneNumber);
			}

			FraudScreeningCardData fraudScreeningCardData = new FraudScreeningCardData(authInfo,
					fraudScreeningCard, failedCardData.getCardAmount().getCurrency().getCurrencyCode(),
					failedCardData.getCardAmount().getAmount(), failedCardData.getCardCharge().getAmount(),
					contactInformation);
			fraudScreeningCardData.setFailedCard(true);
			fraudScreeningCardDataList.add(fraudScreeningCardData);
		}
		for (PaymentTransaction paymentTransaction : paymentData.getPayment().getPaymentTransactions(
				PaymentMethod.DATACASH))
		{
			DataCashPaymentTransaction datacashPaymentTransaction =
					(DataCashPaymentTransaction) paymentTransaction;

			AuthInfo authInfo = new AuthInfo(datacashPaymentTransaction.getAuthCode(),
					datacashPaymentTransaction.getAvsResponse()
					, datacashPaymentTransaction.getThreeDSStatus()
					, datacashPaymentTransaction.getDatacashReference(),
					datacashPaymentTransaction.getMerchantReference(), datacashPaymentTransaction
					.getDataCashResponseCode(), datacashPaymentTransaction.getReferralCode(),
					datacashPaymentTransaction.getIssuer(), datacashPaymentTransaction.getCardScheme(),
					datacashPaymentTransaction.getCountry(), datacashPaymentTransaction.getCvvResponse(),
					datacashPaymentTransaction.getResponseMsg());

			CnpCard cnpCard = datacashPaymentTransaction.getCard();

			/*commented for hccs*/
			FraudScreeningCard fraudScreeningCard = new FraudScreeningCard(cnpCard.getPan(), cnpCard
					.getNameOncard(), cnpCard.getExpiryDate());

			BillingAddress address = cnpCard.getAddress();

			/*BillingAddress is made null for hccs*/
			//BillingAddress address = null;
			String phoneNumber = StringUtils.EMPTY;
			if (bookingInfo.getBookingComponent().getContactInfo() != null)
			{
				phoneNumber = bookingInfo.getBookingComponent().getContactInfo().getPhoneNumber();
			}
			ContactInformation contactInformation = null;
			if (address != null)
			{
				contactInformation = new ContactInformation(address.getStreetAddress1(),
						address.getStreetAddress2(), address.getStreetAddress3(), address
						.getStreetAddress4(), cnpCard.getPostCode(), address.getCountry(),
						phoneNumber);
			}
			FraudScreeningCardData fraudScreeningCardData = new FraudScreeningCardData(authInfo,
					fraudScreeningCard, datacashPaymentTransaction.getTransactionAmount().getCurrency()
					.getCurrencyCode(), datacashPaymentTransaction.getTransactionAmount().getAmount(),
					datacashPaymentTransaction.getTransactionCharge().getAmount(), contactInformation);
			fraudScreeningCardDataList.add(fraudScreeningCardData);
		}

		String promoId = "";
		String promoAmount = "";
		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		if (bookingComponent.getDiscount() != null)
		{
			promoId = bookingComponent.getDiscount().getDiscountId();
			promoAmount = bookingComponent.getDiscount().getDiscountAmount();
		}
		PaymentInformation paymentInformation = new PaymentInformation(fraudScreeningCardDataList,
				promoId, promoAmount);
		return paymentInformation;
	}

	/**
	 * Gets the conf entry for the given key with respect to a brand.
	 *
	 * @param key the key.
	 * @param clientApplication the brand.
	 * @param defaultValue the default value.
	 *
	 * @return the conf entry.
	 */
	private boolean getBrandConfEntry(String key, String clientApplication,
			boolean defaultValue)
	{
		Boolean confEntry = defaultValue;
		String brandConfEntry = ConfReader.getConfEntry("fraudscreening." + clientApplication
				+ "." + key, null);
		if (brandConfEntry == null)
		{
			confEntry = ConfReader.getBooleanEntry("fraudscreening." + key, defaultValue);
		}
		else
		{
			confEntry = Boolean.valueOf(brandConfEntry);
		}
		return confEntry;
	}
	/**
	 * creates the payment information required for paypal transaction
	 * @param authorizePayPalTransaction
	 * @return
	 */
	public PaymentInformationForPayPal createPaymentInformationForPayPalPreAuth(AuthorizePayPalTransaction authorizePayPalTransaction){

		PaymentInformationForPayPal paymentInformationForPayPal = new PaymentInformationForPayPal();
		paymentInformationForPayPal.setCountrycode(authorizePayPalTransaction.getCountryCode());
		paymentInformationForPayPal.setEmail(authorizePayPalTransaction.getCustomerEmailId());
		paymentInformationForPayPal.setFirstname(authorizePayPalTransaction.getFirstname());
		paymentInformationForPayPal.setLastname(authorizePayPalTransaction.getLastname());
		paymentInformationForPayPal.setMiddlename(authorizePayPalTransaction.getMiddlename());
		paymentInformationForPayPal.setPayerid(authorizePayPalTransaction.getPayerId());
		paymentInformationForPayPal.setPayerstatus(authorizePayPalTransaction.getPayerStatus());
		paymentInformationForPayPal.setPaymentstatus(authorizePayPalTransaction.getPaymentStatus());
		paymentInformationForPayPal.setPaymenttype(authorizePayPalTransaction.getPaymentType());

		return paymentInformationForPayPal;
	}
	/**
	 * creates the payment information required for paypal transaction
	 * @param PayPalCaptureRequest
	 * @return
	 */
	public PaymentInformationForPayPal createPaymentInformationForPayPalFulfill(PayPalCaptureRequest payPalCaptureRequest){

		PaymentInformationForPayPal paymentInformationForPayPal = new PaymentInformationForPayPal();
		paymentInformationForPayPal.setCountrycode(payPalCaptureRequest.getCountryCode());
		paymentInformationForPayPal.setEmail(payPalCaptureRequest.getCustomerEmailId());
		paymentInformationForPayPal.setFirstname(payPalCaptureRequest.getFirstname());
		paymentInformationForPayPal.setLastname(payPalCaptureRequest.getLastname());
		paymentInformationForPayPal.setMiddlename(payPalCaptureRequest.getMiddlename());
		paymentInformationForPayPal.setPayerid(payPalCaptureRequest.getPayerId());
		paymentInformationForPayPal.setPayerstatus(payPalCaptureRequest.getPayerStatus());
		paymentInformationForPayPal.setPaymentstatus(payPalCaptureRequest.getPaymentStatus());
		paymentInformationForPayPal.setPaymenttype(payPalCaptureRequest.getPaymentType());

		return paymentInformationForPayPal;
	}
	/**
	 * This method is responsible for calling fraud screening service and handling the response
	 * appropriately for PayPal Transaction , additional method is introduced to Not break existing flow.
	 *
	 * @return false in case of Reject response, true in all other cases.
	 */
	public boolean processFraudScreeningForPayPalTransaction(AuthorizePayPalTransaction authorizePayPalTransaction , PayPalCaptureRequest payPalCaptureRequest)
	{
		boolean fraudScreeningStatus = true;

		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(
				token));
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		String clientApplication = bookingInfo.getBookingComponent().getClientApplication()
				.getClientApplicationName();

		if (getBrandConfEntry("switch", clientApplication, false))
		{
			FraudScreeningRequestMapper fraudScreeningRequestMapper = new FraudScreeningRequestMapper(
					bookingInfo);

			FraudScreeningRequest fraudScreeningRequest = fraudScreeningRequestMapper
					.createFraudScreeningRequest(paymentData.getBookingSessionId());
			if(authorizePayPalTransaction!=null){

				fraudScreeningRequest.setPaymentInformationForPayPal(createPaymentInformationForPayPalPreAuth(authorizePayPalTransaction));
			}else if(payPalCaptureRequest!=null){

				//fraudScreeningRequest.setOrderInformation(payPalCaptureRequest.getOrderInformation());
				fraudScreeningRequest.setPaymentInformationForPayPal(createPaymentInformationForPayPalFulfill(payPalCaptureRequest));
			}
			FraudScreeningServiceImpl fraudScreeningService = FraudScreeningServiceFactory
					.updateFraudScreeningServiceImpl(paymentData.getBookingSessionId(),
							fraudScreeningRequest);

			String fraudScreeningResponseCode = "Not Sent";

			try
			{
				FraudScreeningResponse response = null;
				BookingData bookingData = bookingInfo.getBookingComponent().getBookingData();

				{
					LogWriter.logInfoMessage("Sending Fraud Screening request for "
							+ clientApplication + " : " + token);
					response = fraudScreeningService.sendToAccertify();
				}
				//Checked null for response.getRecommendationCode()
				if (null != response.getRecommendationCode() && "" != response.getRecommendationCode()
						&& !"NULL".equalsIgnoreCase(response.getRecommendationCode()))
				{
					fraudScreeningResponseCode = response.getRecommendationCode();
				}
				LogWriter.logInfoMessage("Fraud Screening Response:" + fraudScreeningResponseCode
						+ " for " + clientApplication + "," + token);
				//Checked null for response.getRecommendationCode()
				if (null != response.getRecommendationCode() && "" != response.getRecommendationCode()
						&& !"NULL".equalsIgnoreCase(response.getRecommendationCode())
						&& response.getRecommendationCode().equalsIgnoreCase(ConfReader.getConfEntry(
								"fraudscreening.reject.code", "DECLINED")) && !getBrandConfEntry(
										"response.ignore", clientApplication, true))
				{
					fraudScreeningStatus = false;
				}

				if ((payPalCaptureRequest != null ))
				{
					if (fraudScreeningStatus)
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_SUCCESS"));
					}
					else
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_FAILURE"));
					}
				}else{
					if (fraudScreeningStatus)
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_PRE_AUTH_SUCCESS"));
					}
					else
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
								paymentData.getTransactionStatus(),
								TransactionStatus.findByCodeTS("FRAUD_SCREENING_PRE_AUTH_FAILURE"));
					}
				}
			}
			catch (FraudScreeningServiceException fse)
			{
				LogWriter.logErrorMessage(fse.getMessage());
				LogWriter.logInfoMessage("Fraud Screening Request not sent for:" + clientApplication
						+ "," + token);
			}
			fraudScreeningService.logFraudScreeningRequest("auth", fraudScreeningResponseCode);
			LogWriter.logDebugMessage(fraudScreeningRequestMapper.logClientDetails());
			bookingInfo.setFraudScreeningResponse(fraudScreeningResponseCode);
		}
		return fraudScreeningStatus;
	}

	public boolean processFraudScreeningForZeroDepositeDD(Map directDebitDetails)
	{
		boolean fraudScreeningStatus = true;

		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(
					token));
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		String clientApplication = bookingInfo.getBookingComponent().getClientApplication()
					.getClientApplicationName();

		if (getBrandConfEntry("switch", clientApplication, false))
		{
			FraudScreeningRequestMapper fraudScreeningRequestMapper = new FraudScreeningRequestMapper(
						bookingInfo);

			FraudScreeningRequest fraudScreeningRequest = fraudScreeningRequestMapper
						.createFraudScreeningRequest(paymentData.getBookingSessionId());
			fraudScreeningRequest.setPaymentInformationForDirectDebit(createDDPaymentInformation(directDebitDetails));

			FraudScreeningServiceImpl fraudScreeningService = FraudScreeningServiceFactory
						.updateFraudScreeningServiceImpl(paymentData.getBookingSessionId(),
									fraudScreeningRequest);

			String fraudScreeningResponseCode = "Not Sent";

			try
			{
				FraudScreeningResponse response = null;
				BookingData bookingData = bookingInfo.getBookingComponent().getBookingData();

				{
					LogWriter.logInfoMessage("Sending Fraud Screening request for "
								+ clientApplication + " : " + token);
					response = fraudScreeningService.send();
				}

				//Checked null for response.getRecommendationCode()
				if (null != response.getRecommendationCode() && "" != response.getRecommendationCode()
							&& !"NULL".equalsIgnoreCase(response.getRecommendationCode()))
				{
					fraudScreeningResponseCode = response.getRecommendationCode();
				}
				LogWriter.logInfoMessage("Fraud Screening Response:" + fraudScreeningResponseCode
							+ " for " + clientApplication + "," + token);
				//Checked null for response.getRecommendationCode()
				if (null != response.getRecommendationCode() && "" != response.getRecommendationCode()
							&& !"NULL".equalsIgnoreCase(response.getRecommendationCode())
							&& response.getRecommendationCode().equalsIgnoreCase(ConfReader.getConfEntry(
							"fraudscreening.reject.code", "DECLINED")) && !getBrandConfEntry(
							"response.ignore", clientApplication, true))
				{
					fraudScreeningStatus = false;
				}
				if ((bookingData != null && bookingData.getBookingReference() != null))
				{
					if (fraudScreeningStatus)
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
									paymentData.getTransactionStatus(),
									TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_SUCCESS"));
					}
					else
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
									paymentData.getTransactionStatus(),
									TransactionStatus.findByCodeTS("FRAUD_SCREENING_FULFILL_FAILURE"));
					}
				}
				else
				{
					if (fraudScreeningStatus)
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
									paymentData.getTransactionStatus(),
									TransactionStatus.findByCodeTS("FRAUD_SCREENING_PRE_AUTH_SUCCESS"));
					}
					else
					{
						new TransactionStatusUtil().updateStatus(paymentData, token,
									paymentData.getTransactionStatus(),
									TransactionStatus.findByCodeTS("FRAUD_SCREENING_PRE_AUTH_FAILURE"));
					}
				}
			}
			catch (FraudScreeningServiceException fse)
			{
				LogWriter.logErrorMessage(fse.getMessage());
				LogWriter.logInfoMessage("Fraud Screening Request not sent for:" + clientApplication
							+ "," + token);
			}
			fraudScreeningService.logFraudScreeningRequest("auth", fraudScreeningResponseCode);
			LogWriter.logDebugMessage(fraudScreeningRequestMapper.logClientDetails());
			bookingInfo.setFraudScreeningResponse(fraudScreeningResponseCode);
		}
		return fraudScreeningStatus;
	}

	/**
	 * Creates the payment information object.
	 * @param directDebitDetails the payment data object.
	 * @return the payment information object.
	 */
	private PaymentInformationForDirectDebit createDDPaymentInformation(Map directDebitDetails)
	{
		PaymentInformationForDirectDebit paymentInformation = new PaymentInformationForDirectDebit();
		paymentInformation.setSortCode(directDebitDetails.get("sortCode").toString());
		paymentInformation.setBankAccount(directDebitDetails.get("accountNumber").toString());
		return paymentInformation;
	}
	/**
	 * @param paymentData
	 * @return Hybris SessionID if exists in nonpaymentData.
	 */
	private String getHybrisSessionId(PaymentData paymentData) {
		if (paymentData != null && paymentData.getBookingInfo() != null) {
			String hybrisSessionId = paymentData.getBookingInfo()
					.getBookingComponent().getNonPaymentData()
					.get("JSESSIONID");
			if (StringUtils.isBlank(hybrisSessionId)) {
				return StringUtils.EMPTY;
			}
			return hybrisSessionId;
		}
		return StringUtils.EMPTY;
	}
}
