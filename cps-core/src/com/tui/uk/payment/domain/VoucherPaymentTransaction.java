/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: VoucherPaymentTransaction.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This Class represents Voucher Payment Transaction.
 */
public final class VoucherPaymentTransaction implements PaymentTransaction
{
   /** The length of the voucher code. */
   private static final int VOUCHER_LENGTH = 8;

   /** The payment type code. */
   private String paymentTypeCode;

   /** The transaction amount entered by the user. */
   private Money transactionAmount;

   /**
    * This <code>Map</code> holds voucher series code as key and voucher amount as value.
    */
   private Map<String, Money> voucherLineItems;

   /**
    * The Constructor.
    *
    * @param paymentTypeCode the payment type.
    * @param voucherLineItems the voucher code and amount map.
    * @param transactionAmount the transaction amount entered by the user.
    */
   public VoucherPaymentTransaction(String paymentTypeCode, Money transactionAmount,
            Map<String, Money> voucherLineItems)
   {
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
      this.voucherLineItems = voucherLineItems;

   }

   /**
    * Gets the payment type code.
    *
    * @return the payment type code
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the ZERO transaction charge for Voucher transaction.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return new Money(BigDecimal.ZERO, transactionAmount.getCurrency());
   }

   /**
    * Gets a <code>Map</code> containing voucher series code as key and voucher amount as value.
    *
    * @return <code>Map</code> containing voucher series code as key and voucher amount as value.
    */
   public Map<String, Money> getVoucherLineItems()
   {
      return voucherLineItems;
   }

   /**
    * Gets the PaymentMethod associated with this Transaction.
    *
    * @return the PaymentMethod
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.VOUCHER;
   }

   /**
    * This method is responsible for the following voucher validation.
    *   a)Validation for voucher length.
    *   b)Validation of valid patterns.
    *   c)Validation of transaction amount entered by user against total transaction amount of
    *     voucherLineItems.
    *
    * @throws PaymentValidationException when validation fails.
    */
   public void validate() throws PaymentValidationException
   {
      BigDecimal lineItemAmount = BigDecimal.ZERO;
      for (Entry<String, Money> entry : voucherLineItems.entrySet())
      {
         int voucherCodeLength = entry.getKey().length();
         if (voucherCodeLength != VOUCHER_LENGTH)
         {
            throw new PaymentValidationException("voucher.codelength.wrong");
         }
         // Voucher code pattern validation.
         validateVoucherCode(entry.getKey());
         lineItemAmount = lineItemAmount.add(entry.getValue().getAmount());
      }
      if (lineItemAmount == transactionAmount.getAmount())
      {
         throw new PaymentValidationException("voucher.transactionAmount.invalid");
      }
   }

   /**
    * Clears sensitive data present in the transaction.
    */
   public void clearSensitiveData()
   {
      // Do nothing, empty implementation.
   }

   /**
    * This method is responsible for validating voucher code.
    *
    * @param voucherCode the voucher code to be validated.
    *
    * @throws PaymentValidationException if voucher code is not numeric or alphabetic.
    */
   private void validateVoucherCode(String voucherCode) throws PaymentValidationException
   {
      Pattern voucherPattern = Pattern.compile("^[0-9a-zA-Z]*$");
      if (!voucherPattern.matcher(voucherCode).matches())
      {
         throw new PaymentValidationException("voucher.code.invalid");
      }
   }

   /**
    * Gets essential transaction data associated with voucher transaction.
    *
    * @return essentialTransactionData the essential transaction data.
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      EssentialTransactionData essentialTransactionData = new EssentialTransactionData(
               paymentTypeCode, transactionAmount, getTransactionCharge(), getPaymentMethod());
      essentialTransactionData.setTransactionLineItems(voucherLineItems);
      return essentialTransactionData;
   }
   /**
    * This method sets the trackingData.
    * Tracking data contains token,sessionId and inventory booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
       //TODO:need to implement this.
   }
}
