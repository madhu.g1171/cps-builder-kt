/*
 * Copyright (C)2006 TUI UK Ltd

 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentConstants.java$
 *
 * $Revision: $
 *
 * $Date: May 23, 2008$
 *
 * Author: veena.bn
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;


/**
 * The interface PaymentConstants. This contains all the constants required for the payment
 * processing.
 *
 * @author thomas.pm
 */
public final class PaymentConstants
{
   /**
    * Private constructor to stop instantiation of this class.
    */
   private PaymentConstants()
   {
      // Do nothing.
   }

   /** The Constant PAYMENT. */
   public static final String PAYMENT = "payment_";

   /** The Constant REFUND. */
   public static final String REFUND = "refund_";

   /** The Constant REFUND_CARD_CHECKED. */
   public static final String REFUND_CHEQUE_CHECKED = "refundbycheque_checked";

   /** The Constant REFUND_BY_CHEQUE_AMOUNT. */
   public static final String REFUND_BY_CHEQUE_AMOUNT = "refundby_cheque_amount";

   /** The Constant CHEQUE. */
   public static final String CHEQUE = "Cheque";

   /** The Constant TOTAL_AMOUNT_PAID. */
   public static final String TOTAL_AMOUNT_PAID = "totamtpaid";

   /** The Constant TOTAL_TRANS_AMOUNT. */
   public static final String TOTAL_TRANS_AMOUNT = "total_transamt";

   /** The Constant TOTAL_AMOUNT_WITH_DISC. */
   public static final String TOTAL_AMOUNT_WITH_DISC = "amtwithdisc";

   /** The Constant PAYMENT_METHOD. */
   public static final String PAYMENT_METHOD = "_paymentmethod";

   /** The Constant PAYMENT_TYPE_CODE. */
   public static final String PAYMENT_TYPE_CODE = "_paymenttypecode";

   /** The Constant TRANSACTION_NUMBER. */
   public static final String TRANSACTION_NUMBER = "totalTrans";

   /** The Constant TRANSACTION_AMOUNT. */
   public static final String TRANSACTION_AMOUNT = "_transamt";

   /** The Constant TRANSACTION_AMOUNT TEXT. */
   public static final String TRANSACTION_AMOUNT_TEXT = "_transamtText";

   /** The Constant AUTH_CODE. */
   public static final String AUTH_CODE = "_referenceNumber";

   /** The Constant DATACASH_REFERENCE. */
   public static final String DATACASH_REFERENCE = "_datacashreference";

   /** The Constant VOUCHER_CODE. */
   public static final String VOUCHER_CODE = "_voucherSeriesCodes";

   /** The Constant CHARGE. */
   public static final String CHARGE = "_chargeAmount";

   /** The constant VOUCHER_INDEX. */
   public static final String VOUCHER_INDEX = "_vocIndex";

   /** The constant VOUCHER_AMOUNT. */
   public static final String VOUCHER_AMOUNT = "_amountsPaidByVouchers";

   /** The Constant CARD_NUMBER. */
   public static final String CARD_NUMBER = "_cardNumber";

   /** The Constant NAME_ON_CARD. */
   public static final String NAME_ON_CARD = "_nameOnCard";

   /** The Constant EXPIRY_MONTH. */
   public static final String EXPIRY_MONTH = "_expiryMonth";

   /** The Constant EXPIRY_YEAR. */
   public static final String EXPIRY_YEAR = "_expiryYear";

   /** The Constant SECURITY_CODE. */
   public static final String SECURITY_CODE = "_securityCode";

   /** The Constant ISSUER_COUNTRY. */
   public static final String ISSUER_COUNTRY = "_issuerCountry";

   /** The Constant POST_CODE. */
   public static final String POST_CODE = "_postCode";

   /** The Constant CARD_TYPE. */
   public static final String CARD_TYPE = "cardType";

   /** The Constant PIN_PAD. */
   public static final String PIN_PAD = "isPinPad";

   /** The Constant SWITCH. */
   public static final String SWITCH = "SWITCH";

   /** The Constant SOLO. */
   public static final String SOLO = "SOLO";

   /** The Constant AMERICAN_EXPRESS. */
   public static final String AMERICAN_EXPRESS = "AMERICAN_EXPRESS";

   /** The Constant ISSUE_NUMBER. */
   public static final String ISSUE_NUMBER = "_issueNumber";

   /** The Constant START_MONTH. */
   public static final String START_MONTH = "_startMonth";

   /** The Constant START_YEAR. */
   public static final String START_YEAR = "_startYear";

   /** The Constant PAYMENTDATA. */
   public static final String PAYMENTDATA = "paymentdata";

   /** Constant for pinpad transaction count. */
   public static final String PINPADTRANSACTIONCOUNT = "pinpadTransactionCount";

   /** Constant for pinpad merchant copy. */
   public static final String PINPAD_MERCHANT_COPY = "merchantCopy";

   /** Constant for pinpad customer copy. */
   public static final String PINPAD_CUSTOMER_COPY = "customerCopy";

   /** The Constant NOT_APPLICABLE. */
   public static final String NOT_APPLICABLE = "NA";

   /** The Constant for AMOUNT PAID. */
   public static final String AMOUNT_PAID = "_amountPaid";

   /** The Constant for MAX_SECURITY_LENGTH. */
   public static final String MAX_SECURITY_LENGTH = "_securityCodeLength";

   /** The Constant for MIN_SECURITY_LENGTH. */
   public static final String MIN_SECURITY_LENGTH = "_minSecurityCodeLength";

   /** The Constant for TYPE. */
   public static final String TYPE = "_type";

   /** The Constant for transaction index. */
   public static final String TRANSACTION_INDEX = "transactionIndex";

   /** Error code if card type is invalid. */
   public static final int ERRORCODE_INVALIDCARDTYPE = 900;

   /** Error code if card number is invalid. */
   public static final int ERRORCODE_INVALIDCARDNO = 901;

   /** Error code if card name is invalid. */
   public static final int ERRORCODE_INVALIDCARDNAME = 902;

   /** Error code if card expire date is invalid. */
   public static final int ERRORCODE_INVALIDEXPIRYDATE = 903;

   /** Error code if security code is invalid. */
   public static final int ERRORCODE_INVALIDSECURITYCODE = 904;

   /** Error code if issue number is invalid. */
   public static final int ERRORCODE_INVALIDISSUENO = 905;

   /** Error code if card start date is invalid. */
   public static final int ERRORCODE_INVALIDSTARTDATE = 906;

   /** Error code if data-cash account number is invalid. */
   public static final int ERRORCODE_INVALIDDATACASHACCNO = 907;

   /** Error code if data-cash reference number is invalid. */
   public static final int ERRORCODE_INVALIDDATACASHREFNO = 908;

   /** Error code if key store parameters are missing. */
   public static final int ERRORCODE_MISSING_KEYSTORE_PARAMETERS = 914;

   /** The constant for invalid post code. */
   public static final int ERRORCODE_INVALIDPOSTCODE = 915;

   /** The constant for invalid address. */
   public static final int ERRORCODE_INVALIDADDRESS = 916;

   /** The constant to get the IE browser name. */
   public static final int MSIE_INDEX = 8;

   /** The constant to get the FIREFOX browser name. */
   public static final int FIREFOX_INDEX = 14;

   /** The constant to get the CHROME browser name. */
   public static final int CHROME_INDEX = 17;

   /** The constant to get the MAX_REFERENCE_LENGTH. */
   public static final int MAX_REFERENCE_LENGTH = 30;

   /** The length of the datacash Reference. */
   public static final int DATACASH_REFERENCE_LENGTH = 16;

   /** The COMMA. */
   public static final String COMMA = ",";

   /** The constant for holding the date format. */
   public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

   /** The constant for holding the response code length. */
   public static final int RESPONSE_CODE_LENGTH = 3;

   /** The error message key. */
   public static final String ERROR_MESSAGE_KEY = "datacash.response.";

   /** The FATAL_ERROR. */
   public static final String FATAL_ERROR = "fatalError";

   /** The NON_FATAL_ERROR. */
   public static final String NON_FATAL_ERROR = "nonFatalError";

   /** The THREED_NON_FATAL_ERROR. */
   public static final String THREED_NON_FATAL_ERROR = "3dNonFatalError";
   
   /** The HCC THREED_NON_FATAL_ERROR. */
   public static final String HCC_NON_FATAL_ERROR = "hccNonFatalError";

   /** The THREED_NON_FATAL_DUP_RESP_ERROR. */
   public static final String THREED_NON_FATAL_DUP_RESP_ERROR = "3dDuplicateTxError";

   /** The constant for HYPHEN. */
   public static final String HYPHEN = "-";

   /** The constant for DCard. */
   public static final String DCARD = "DCard";

   /** The constant for Card. */
   public static final String CARD = "Card";

   /** The constant for CardPinPad. */
   public static final String CARDPINPAD = "CardPinPad";

   /** The constant for noOfRefunds. */
   public static final String NO_OF_REFUNDS = "noOfRefunds";

   /** The street address one.Will hold house no/name. */
   public static final String HOUSENO = "_street_address1";

   /** The street address two.Will hold remaining address details. */
   public static final String STREETADDRESS = "_street_address2";

   /** The street address three.Will hold town/city. */
   public static final String TOWN = "_street_address3";

   /** The street address four.Will hold county. */
   public static final String COUNTY = "_street_address4";

   /** The constant for country. */
   public static final String SELECTED_COUNTRY = "_selectedCountry";

   /** The constant for country code. */
   public static final String SELECTED_COUNTRY_CODE = "_selectedCountryCode";

   /** The constant for card holder address line 2. */
   public static final String CARDHOLDER_ADDRESS2 = "_cardHolderAddress2";

   /** The constant for card holder day time phone no. */
   public static final String CARDHOLDER_DAYTIMEPHONE = "_cardHoldersdayTimePhone";

   /** The constant for card holder mobile phone no. */
   public static final String CARDHOLDER_MOBILEPHONE = "_cardHoldersmobilePhone";

   /** The constant for card holder email address. */
   public static final String CARDHOLDER_EMAILADDRESS = "_cardHoldersemailAddress";

   /** The constant for card holder email address1. */
   public static final String CARDHOLDER_EMAILADDRESS1 = "_cardHoldersemailAddress1";

   /** The constant for cv2 not provided. */
   public static final String NOT_PROVIDED = "notprovided";

   /** The constant for accept. The accept value are always in small case. */
   public static final String ACCEPT = "accept";

   /** The constant for reject. The reject value are always in small case. */
   public static final String REJECT = "reject";

   /** The constant for cv2 not checked. */
   public static final String NOT_CHECKED = "notchecked";

   /** The constant for cv2 matched. */
   public static final String MATCHED = "matched";

   /** The constant for cv2 not matched. */
   public static final String NOT_MATCHED = "notmatched";

   /** The constant for cv2 partial match. */
   public static final String PARTIAL_MATCH = "partialmatch";

   /** The constant for total refunds. */
   public static final String TOTAL_REFUND = "totalRefund";

   /** The constant for cv2 not provided index. */
   public static final int NOT_PROVIDED_INDEX = 0;

   /** The constant for cv2 not checked index. */
   public static final int NOT_CHECKED_INDEX = 1;

   /** The constant for cv2 matched index. */
   public static final int MATCHED_INDEX = 2;

   /** The constant for cv2 not matched index. */
   public static final int NOT_MATCHED_INDEX = 3;

   /** The constant for cv2 partial match index. */
   public static final int PARTIAL_MATCH_INDEX = 4;

   /** The constant for cv2 DEFAULT_POLICIES. */
   static final String[] DEFAULT_POLICIES = {ACCEPT, ACCEPT, ACCEPT, ACCEPT, ACCEPT};

   /** The constant for cv2 CV2_POLICIES. */
   static final String[] CV2_POLICIES = {REJECT, ACCEPT, ACCEPT, REJECT, REJECT};

   /** The additional address line 1 will hold additional address details. */
   public static final String ADDITIONAL_ADDRESS_LINE1 = "_additional_address_line1";

   /** The additional address line 2 will hold additional address details. */
   public static final String ADDITIONAL_ADDRESS_LINE2 = "_additional_address_line2";

   /** The constant to get the MAX_REFERENCE_LENGTH for ACSS. */
   public static final int MAX_REFERENCE_LENGTH_ACSS = 28;

   /** The constant to get the dddeposit. */
   public static final String DDDEPOSIT = "dddeposit";


}