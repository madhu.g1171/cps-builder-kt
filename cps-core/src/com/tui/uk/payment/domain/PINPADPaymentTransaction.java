/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PINPADPaymentTransaction.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This Class represents PIN PAD transaction.
 */
public final class PINPADPaymentTransaction implements PaymentTransaction
{
   /** The payment type code. */
   private String paymentTypeCode;

   /** The transaction reference code. */
   private String transactionReference;

   /** The transaction amount. */
   private Money transactionAmount;

   /** The transaction charge. */
   private Money transactionCharge;

   /** The pin-pad merchant copy. */
   private String merchantCopy;

   /** The pin-pad customer copy. */
   private String customerCopy;

   /** The Card object, which contains card details. */
   private CpCard cpCard;

   /**
    * The Constructor.
    *
    * @param paymentTypeCode the payment type
    * @param transactionReference the transactionReference
    * @param transactionAmount the transaction amount
    * @param merchantCopy the merchant copy
    * @param customerCopy the customer copy
    * @param transactionCharge the transaction charge
    * @param cpCard the cpCard.
    */
   public PINPADPaymentTransaction(String paymentTypeCode, Money transactionAmount,
      String transactionReference, String merchantCopy, String customerCopy,
      Money transactionCharge, CpCard cpCard)
   {
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
      this.transactionReference = transactionReference;
      this.merchantCopy = merchantCopy;
      this.customerCopy = customerCopy;
      this.transactionCharge = transactionCharge;
      this.cpCard = cpCard;
   }

   /**
    * Gets the payment type code.
    *
    * @return the payment type code.
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the transaction charge.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return transactionCharge;
   }

   /**
    * Gets the transaction reference code.
    *
    * @return the transaction reference code
    */
   public String getTransactionReference()
   {
      return transactionReference;
   }

   /**
    * Gets the PaymentMethod associated with this transaction.
    *
    * @return the payment method
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.PIN_PAD;
   }

   /**
    * Gets the merchant copy associated with  this transaction.
    *
    * @return the merchant copy
    */
   public String getMerchantCopy()
   {
      this.merchantCopy = merchantCopy.replace("><", "><br/><");
      return merchantCopy;
   }

   /**
    * Gets the customer copy associated with this transaction.
    *
    * @return the customer copy
    */
   public String getCustomerCopy()
   {
      this.customerCopy = customerCopy.replace("><", "><br/><");
      return customerCopy;
   }

   /**
    * This method is responsible for validating the transactionReference code length.
    *
    * @throws PaymentValidationException when validation fails.
    */
   public void validate() throws PaymentValidationException
   {
      Pattern pattern = Pattern.compile("^[0-9]{16}+$");
      if (StringUtils.isEmpty(transactionReference) || !pattern.matcher(
         transactionReference).matches())
      {
         String errorMessage = PropertyResource.getProperty("payment.transactionreference.invalid",
            MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PaymentValidationException("payment.transactionreference.invalid");
      }
   }

   /**
    * Clears sensitive data present in the transaction.
    */
   public void clearSensitiveData()
   {
      cpCard.purgeCardDetails();
   }

   /**
    * Gets essential transaction data associated with pin pad transaction.
    *
    * @return essentialTransactionData the essential transaction data
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      EssentialTransactionData essentialTransactionData =
         new EssentialTransactionData(paymentTypeCode, transactionAmount,
               getTransactionCharge(), getPaymentMethod());
      essentialTransactionData.setTransactionReference(transactionReference);
      essentialTransactionData.setMerchantCopy(getMerchantCopy());
      essentialTransactionData.setCustomerCopy(getCustomerCopy());
      essentialTransactionData.setMaskedCardNumber(cpCard.getMaskedCardNumber());
      return essentialTransactionData;
   }
   /**
    * This method sets the trackingData.
    * Tracking data contains token,sessionId and inventory booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
       //TODO:need to implement this.
   }
}
