/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Card.java,v $
 *
 * $Revision: 1.4 $
 *
 * $Date: 2008-05-14 07:14:57 $
 *
 * $author : bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;


import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BillingAddress;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class represents the Card domain object. Represents debit/credit card that is used for
 * payment. Field names in this class should match those used within the DataCash developers guide.
 *
 * @author bibin.j@sonata-software.com
 */
public final class CnpCard implements Card
{

   /** Pattern for card types. */
   private static final Pattern CARDTYPE_PATTERN = Pattern.compile("[a-zA-Z \\_?/?\\s*]+");

   /** Pattern for card number. */
   private static final Pattern CARDNUMBER_PATTERN = Pattern.compile("[0-9]{13,19}+");

   /** Pattern for security code. */
   private static final Pattern SECURITYCODE_PATTERN = Pattern.compile("[0-9]+");

   /** Pattern for expiry date. */
   private static final Pattern DATE_PATTERN = Pattern.compile("[0-9]{1,2}+/[0-9]{2,4}+");

   /** Pattern for issue number. */
   private static final Pattern ISSUENUMBER_PATTERN = Pattern.compile("[0-9]*");

 //CHECKSTYLE:OFF
   /** Pattern for UK post code. */
   private static final Pattern POSTCODE_UK_PATTERN =
      Pattern.compile("^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9]?[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$");
 //CHECKSTYLE:ON

   /** Holds the maximum possible value of the expire month. */
   private static final int MAX_MONTH = 12;

   /** Holds the start index for masking the card number. */
   private static final int MASK_START_INDEX = 6;

   /** Holds the end index for masking the card number. */
   private static final int MASK_END_INDEX = 4;

   /** The address object which holds address details .*/
   private BillingAddress address;

   /** The card type. */
   private String cardType;

   /** Card number.A String of 13 to 19 digits. */
   private char[] pan;

   /** The name on card. */
   private String nameOncard;

   /** The expire date. */
   private String expiryDate;

   /** The CVV number of the card. */
   private char[] cv2;

   /** The post code of the card holder. */
   private String postCode;

   /** The start date. */
   private String startDate;

   /** The issue number. */
   private String issueNumber;

   /** The capture method. */
   private String captureMethod;

   /** The track2Data. */
   private String track2Data;

   /** The minimum security code length. */
   private int minSecurityCodeLength;

   /** The maximum security code length. */
   private int maxSecurityCodeLength;

   /** For CV2AVS enabled flag. */
   private Boolean cv2AVSEnabled;

   /**Map containing cvv policy  details.*/
   private Map<String, String> cvvPolicies = new HashMap<String, String>();

   /**Map containing address policy details.*/
   private Map<String, String> addressPolicies = new HashMap<String, String>();

   /**Map containing post code policy details.*/
   private Map<String, String> postCodePolicies = new HashMap<String, String>();
   /**
    * The Constructor.
    *
    * @param pan the pan
    * @param nameOncard the name on card
    * @param expiryDate the expiry date
    * @param cv2 the cv2
    * @param postCode the post code
    * @param cardType the card type
    * @param minSecurityCodeLength the minimum security code length.
    * @param maxSecurityCodeLength the maximum security code length.
    */
   //CHECKSTYLE:OFF
   public CnpCard(char[] pan, String nameOncard, String expiryDate, char[] cv2, String postCode,
            String cardType, int minSecurityCodeLength, int maxSecurityCodeLength)
   {
      char[] panCopy = pan;
      char[] cv2Copy = cv2;
      this.pan = panCopy;
      this.expiryDate = expiryDate;
      this.nameOncard = nameOncard;
      this.cv2 = cv2Copy;
      this.postCode = postCode;
      this.cardType = cardType;
      this.minSecurityCodeLength = minSecurityCodeLength;
      this.maxSecurityCodeLength = maxSecurityCodeLength;
   }
   //CHECKSTYLE:ON

   /**
    * Gets the pan.
    *
    * @return the pan
    */
   public char[] getPan()
   {
      char[] panCopy = pan;
      return panCopy;
   }

   /**
    * Gets the start date.
    *
    * @return the startDate
    */
   public String getStartDate()
   {
      return startDate;
   }

   /**
    * Gets the expire date.
    *
    * @return the expiryDate
    */
   public String getExpiryDate()
   {
      return expiryDate;
   }

   /**
    * Gets the card type.
    *
    * @return the card type
    */
   public String getCardtype()
   {
      return cardType;
   }

   /**
    * Gets the name on card.
    *
    * @return the nameOncard
    */
   public String getNameOncard()
   {
      return nameOncard;
   }

   /**
    * Gets the issue number.
    *
    * @return the issueNumber
    */
   public String getIssueNumber()
   {
      return issueNumber;
   }

   /**
    * Sets the start date.
    *
    * @param startDate the startDate to set
    */
   public void setStartDate(String startDate)
   {
      this.startDate = startDate;
   }

   /**
    * Sets the issue number.
    *
    * @param issueNumber the issueNumber to set
    */
   public void setIssueNumber(String issueNumber)
   {
      this.issueNumber = issueNumber;
   }

   /**
    * Sets the capture method.
    *
    * @param captureMethod the captureMethod to set
    */
   public void setCaptureMethod(String captureMethod)
   {
      this.captureMethod = captureMethod;
   }

   /**
    * Sets the track2Data.
    *
    * @param track2Data the track2Data to set.
    */
   public void setTrack2Data(String track2Data)
   {
      this.track2Data = track2Data;
   }

   /**
    * Gets the cv2.
    *
    * @return the cv2
    */
   public char[] getCv2()
   {
      char[] cv2Copy = cv2;
      return cv2Copy;
   }

   /**
    * Gets the post code.
    *
    * @return the post code
    */
   public String getPostCode()
   {
      return postCode;
   }
   /**
    * Gets the capture method.
    *
    * @return the capture method
    */
   public String getCaptureMethod()
   {
      return captureMethod;
   }
   /**
    * Gets the track2Data.
    *
    * @return the track2Data
    */
   public String getTrack2Data()
   {
      return track2Data;
   }

   /**
    * Gets minSecurityCodeLength.
    *
    * @return the minSecurityCodeLength
    */
   public int getMinSecurityCodeLength()
   {
      return minSecurityCodeLength;
   }

   /**
    * Gets maxSecurityCodeLength.
    *
    * @return the maxSecurityCodeLength
    */
   public int getMaxSecurityCodeLength()
   {
      return maxSecurityCodeLength;
   }

   /**
    * This method is used to validate card.
    *
    * @throws PaymentValidationException when validation fails.
    */
   public void validate() throws PaymentValidationException
   {
      validateCardType();
      validateCardNumber();
      validateNameOnCard();
      validateExpiryDateOnCard();
      validateSecurityCodeOnCard();
      validateStartDateOrIssueNumber();
      //TODO validation of address and post code.
      if (cv2AVSEnabled)
      {
        validateAddress();
        validatePostCode();
      }
   }

   /**
    * Method to validate the card type.
    *
    * @throws PaymentValidationException when validation fails.
    */
   private void validateCardType() throws PaymentValidationException
   {
      if (StringUtils.isBlank(cardType) || (!validateCardType(cardType)))
      {
        populateException("payment.cardType.invalid", "CardType",
           PaymentConstants.ERRORCODE_INVALIDCARDTYPE);
      }
   }

   /**
    * Method to validate the card number.
    *
    * @throws PaymentValidationException when validation fails.
    */
   private void validateCardNumber() throws PaymentValidationException
   {
      if (StringUtils.isBlank(String.valueOf(pan)) ||  (!validateCreditCardNumber()))
      {
         populateException("payment.pan.invalid", "CardNumber",
            PaymentConstants.ERRORCODE_INVALIDCARDNO);
      }
   }

   /**
    * Method to validate the name on card.
    *
    * @throws PaymentValidationException when validation fails.
    */
   private void validateNameOnCard() throws PaymentValidationException
   {
      if (StringUtils.isBlank(nameOncard) || (!validateName()))
      {
         populateException("payment.nameOncard.invalid", "CardName",
            PaymentConstants.ERRORCODE_INVALIDCARDNAME);
      }
   }

   /**
    * Method to validate the expiry date on card.
    *
    * @throws PaymentValidationException when validation fails.
    */
   private void validateExpiryDateOnCard() throws PaymentValidationException
   {
      if (StringUtils.isBlank(expiryDate) || !validateExpiryDate())
      {
          populateException("payment.expirydate.invalid", "ExpiryDate",
             PaymentConstants.ERRORCODE_INVALIDEXPIRYDATE);
      }
   }

   /**
    * Method to validate the security code on card.
    *
    * @throws PaymentValidationException when validation fails.
    */
   private void validateSecurityCodeOnCard() throws PaymentValidationException
   {
      if ((StringUtils.isNotBlank(String.valueOf(cv2)) && !validateSecurityCode())
               || (StringUtils.isBlank(String.valueOf(cv2)) && !validateSecurityCodeLength()))
      {
         populateException("payment.cv2.invalid", "SecurityCode",
            PaymentConstants.ERRORCODE_INVALIDSECURITYCODE);
      }
   }

   /**
    * Method to validate either issue number or start date.
    *
    * @throws PaymentValidationException when validation fails.
    */
   private void validateStartDateOrIssueNumber() throws PaymentValidationException
   {
      if (PaymentConstants.SWITCH.equals(cardType)
               || PaymentConstants.SOLO.equals(cardType))
      {
            if (StringUtils.isNotBlank(issueNumber) && !validateIssueNumber())
            {
               populateException("payment.issuenumber.invalid", "IssueNumber",
                  PaymentConstants.ERRORCODE_INVALIDISSUENO);
            }
            if (StringUtils.isNotBlank(startDate) && !validateStartDate())
            {
               populateException("payment.startdate.invalid", "ValidFrom",
                  PaymentConstants.ERRORCODE_INVALIDSTARTDATE);
            }
      }
   }

   /**
    * Method to validate card type.
    *
    * @param cardTypeValue the card type name which needs to be validated.
    * @return boolean if it does not match the pattern defined.
    */
   private boolean validateCardType(String cardTypeValue)
   {
      Matcher match = CARDTYPE_PATTERN.matcher(cardTypeValue);
      return match.matches();
   }

   /**
    * Method to validate Credit Card Number.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateCreditCardNumber()
   {
      return CARDNUMBER_PATTERN.matcher(String.valueOf(pan)).matches();
   }

   /**
    * Method to validate Name, Name on Card.
    *
    * @return boolean if it does not match the pattern defined.
    */
   private boolean validateName()
   {
      for (char nameLetter : nameOncard.toCharArray())
      {
         if (!(Character.isLetter(nameLetter) || nameLetter == '-' || nameLetter == '.'
            || nameLetter == '\'' || nameLetter == ' '))
         {
            return false;
         }
      }
      return true;
   }

   /**
    * Method to validate Security code.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateSecurityCode()
   {
      Matcher securityCodeMatcher = SECURITYCODE_PATTERN.matcher(String.valueOf(cv2));
      if (securityCodeMatcher.matches())
      {
         return validateSecurityCodeLength();
      }
      return securityCodeMatcher.matches();
   }

   /**
    * Method to validate Security code length.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateSecurityCodeLength()
   {
      if (cv2.length == minSecurityCodeLength || cv2.length == maxSecurityCodeLength)
      {
         return true;
      }
      return false;
   }

   /**
    * Method used to validate expiry date on the card.
    * @return true if expiry date is valid, otherwise false.
    */
   private boolean validateExpiryDate()
   {
      if (StringUtils.isBlank(expiryDate)
         || !DATE_PATTERN.matcher(expiryDate).matches()
         || Integer.parseInt(getExpiryMonth()) > MAX_MONTH)
      {
         return false;
      }
      String strExpiryYear = getExpiryYear();
      int expYearLength = strExpiryYear.length();

      Calendar cal = Calendar.getInstance();
      int curentDate = cal.get(Calendar.DAY_OF_MONTH) + 1;

      Date cardExpDate =
         getStartOrExpiryDate(
            String.valueOf(curentDate), getExpiryMonth(), strExpiryYear, expYearLength);

      boolean flag = true;
      Date todaysDate = cal.getTime();

      if (todaysDate.getTime() > cardExpDate.getTime())
      {
         flag = false;
      }
      return flag;
   }

   /**
    * Method used to validate start date.
    *
    * @return true when start date is valid, otherwise false.
    */
   private boolean validateStartDate()
   {
      String strStartYear = getStartYear();
      int startYearLength = strStartYear.length();
      int startMonth = Integer.parseInt(getStartMonth());
      if (!DATE_PATTERN.matcher(startDate).matches()
         || startMonth > MAX_MONTH)
      {
         return false;
      }
      boolean flag = true;

      Calendar cal = Calendar.getInstance();
      int curentDate = cal.get(Calendar.DAY_OF_MONTH);

      Date todaysDate = cal.getTime();
      Date cardStartDate =
         getStartOrExpiryDate(
            String.valueOf(curentDate), getStartMonth(), strStartYear, startYearLength);
      if (todaysDate.getTime() < cardStartDate.getTime())
      {
         flag = false;
      }
      return flag;
   }

   /**
    * Method to validate Issue Number.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateIssueNumber()
   {
      Matcher issueNumberMatcher =
         ISSUENUMBER_PATTERN.matcher(String.valueOf(issueNumber));
      return issueNumberMatcher.matches();
   }

   /**
    * Method to validate post code.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean checkPostCodeUKPattern()
   {
      Matcher postCodeMatcher =
         POSTCODE_UK_PATTERN.matcher(String.valueOf(postCode));
      return postCodeMatcher.matches();
   }

   /**
    * Validate address fields.
    *
    * @throws PaymentValidationException when validation fails.
    */
   private void validatePostCode() throws PaymentValidationException
   {
      if (StringUtils.isNotBlank(address.getCountryCode()))
      {
         if (!address.getCountryCode().equalsIgnoreCase("IE") && StringUtils.isBlank(postCode))
         {
            populateException("payment.postCode.invalid", "PostCode",
               PaymentConstants.ERRORCODE_INVALIDPOSTCODE);
         }
         else if (address.getCountryCode().equalsIgnoreCase("GB") && !checkPostCodeUKPattern())
         {
            populateException("payment.postCode.invalid", "PostCode",
               PaymentConstants.ERRORCODE_INVALIDPOSTCODE);
         }
      }
   }

  /**
   * Validate post code field.
   *
   * @throws PaymentValidationException when validation fails.
   */
   private void validateAddress() throws PaymentValidationException
   {
      if (StringUtils.isBlank(address.getStreetAddress1())
         || StringUtils.isBlank(address.getStreetAddress3()))
      {
         populateException("payment.address.invalid", "Address",
            PaymentConstants.ERRORCODE_INVALIDADDRESS);
      }
   }

   /**
    * Method to get the month of expire for the card.
    *
    * @return the expire month.
    */
   private String getExpiryMonth()
   {
      String[] expiryDateArr = StringUtils.split(expiryDate, '/');
      return expiryDateArr[0];
   }

   /**
    * Method to get the year of expire for the card.
    *
    * @return the expire year.
    */
   private String getExpiryYear()
   {
      String[] expiryDateArr = StringUtils.split(expiryDate, '/');
      return expiryDateArr[1];
   }

   /**
    * Method to get the month of start date for the card.
    *
    * @return the expire month.
    */
   private String getStartMonth()
   {
      String[] startDateArr = StringUtils.split(startDate, '/');
      return startDateArr[0];
   }

   /**
    * Method to get the year of start for the card.
    *
    * @return the expire year.
    */
   private String getStartYear()
   {
      String[] startDateArr = StringUtils.split(startDate, '/');
      return startDateArr[1];
   }

   /**
    * This method overwrites sensitive data.
    */
   public void purgeCardDetails()
   {
      Arrays.fill(pan, '*');
      Arrays.fill(cv2, '*');
      pan = null;
      cv2 = null;
      expiryDate = null;
      nameOncard = null;
      postCode = null;
      cardType = null;
      startDate = null;
      issueNumber = null;
   }

   /**
    * Gets masked card number.
    *
    * @return the masked card number.
    */
   public String getMaskedCardNumber()
   {
      StringBuilder maskedCardNumber =
         new StringBuilder(String.valueOf(pan));
      for (int i = MASK_START_INDEX; i < pan.length - MASK_END_INDEX; i++)
      {
         maskedCardNumber.setCharAt(i, '*');
      }
      return maskedCardNumber.toString();
   }

   /**
    * Method to populate exception.
    *
    * @param message the message code.
    * @param fieldName the associated field name.
    * @param errorCode the error code.
    *
    * @throws PaymentValidationException the Exception.
    */
   private void populateException(String message, String fieldName, int errorCode)
      throws PaymentValidationException
   {
      String errorMessage = PropertyResource.getProperty(message, MESSAGES_PROPERTY);
      LogWriter.logErrorMessage(errorMessage);
      throw new PaymentValidationException(errorCode, message, fieldName);
   }

   /**
    * This utility method returns the formatted date instance for the
    * entered month and year.
    *
    * @param argDay - Always the current day.
    * @param argMonth - Entered month to be formatted.
    * @param argYear - Entered year to be formatted.
    * @param length - the length of the year string.
    * @return - The formatted date instance.
    */
   private static Date getStartOrExpiryDate(String argDay, String argMonth, String argYear,
       int length)
   {
      Date cardDate = null;
      String exp = argDay + "/" + argMonth + "/" + argYear;
      try
      {
         if (length == 2)
         {
            SimpleDateFormat shortDateFormat =
               new SimpleDateFormat("dd/MM/yy");
            cardDate = shortDateFormat.parse(exp);
         }
         else
         {
            SimpleDateFormat longDateFormat =
               new SimpleDateFormat("dd/MM/yyyy");
            cardDate = longDateFormat.parse(exp);
         }
      }
      catch (ParseException e)
      {
         e.printStackTrace();
         throw new RuntimeException(e);
      }

      return cardDate;
   }

   /**
    * Gets the address.
    *
    * @return the address
    */
   public BillingAddress getAddress()
   {
      return address;
   }

   /**
    * Sets the address.
    *
    * @param address the address to set
    */
   public void setAddress(BillingAddress address)
   {
      this.address = address;
   }

   /**
    * Sets the cvvPolicies.
    *
    * @return the cvvPolicies
    */
   public Map<String, String> getCvvPolicies()
   {
      return cvvPolicies;
   }

   /**
    * Gets the cvvPolicies.
    *
    * @param cvvPolicies the cvvPolicies to set
    */
   public void setCvvPolicies(Map<String, String> cvvPolicies)
   {
      this.cvvPolicies = cvvPolicies;
   }

   /**
    * Sets the addressPolicies.
    *
    * @return the addressPolicies
    */
   public Map<String, String> getAddressPolicies()
   {
      return addressPolicies;
   }

   /**
    * Gets the addressPolicies.
    *
    * @param addressPolicies the addressPolicies to set
    */
   public void setAddressPolicies(Map<String, String> addressPolicies)
   {
      this.addressPolicies = addressPolicies;
   }

   /**
    * Sets the postCodePolicies.
    *
    * @return the postCodePolicies
    */
   public Map<String, String> getPostCodePolicies()
   {
      return postCodePolicies;
   }

   /**
    * Gets the postCodePolicies.
    *
    * @param postCodePolicies the postCodePolicies to set
    */
   public void setPostCodePolicies(Map<String, String> postCodePolicies)
   {
      this.postCodePolicies = postCodePolicies;
   }
  /**
   * Get the CV2AVS flag.
   *
   * @return cv2AVS the cv2AVS flag.
   */
   public Boolean getCv2AVSEnabled()
   {
      return cv2AVSEnabled;
   }

   /**
    * Set the CV2AVS flag.
    *
    * @param cv2AVS the cv2AVS flag.
    */
    public void setCv2AVSEnabled(Boolean cv2AVS)
    {
       this.cv2AVSEnabled = cv2AVS;
    }
}
