/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashHistoricTransaction.java $
 *
 * $Revision: $
 *
 * $Date: Oct 1, 2009$
 *
 * Author: veena.bn
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.domain.PaymentConstants.COMMA;
import static com.tui.uk.payment.domain.PaymentConstants.DATACASH_REFERENCE_LENGTH;
import static com.tui.uk.payment.domain.PaymentConstants.DATE_FORMAT;
import static com.tui.uk.payment.domain.PaymentConstants.RESPONSE_CODE_LENGTH;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CANCEL_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.FULFILL_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REVERSE_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REFUND_TRANSACTION;

import java.io.EOFException;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.ArrayList;

import javax.management.monitor.Monitor;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.PaymentStatus;
import com.tui.uk.config.ConfReader;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.payment.service.datacash.DataCashServiceFactory;
import com.tui.uk.payment.service.datacash.Response;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 *
 * This class represents DataCashHistoricTransaction and performs refund,fulfill and cancel on the
 * given DatacashReference number.This class implements all the necessary behaviors required for
 * dataCash historic transactions.
 *
 * @author veena.bn
 *
 */
public final class DataCashHistoricTransaction implements PaymentTransaction,Serializable
{
   /** The payment type. */
   private String paymentTypeCode;

   /** The payment method. */
   private PaymentMethod paymentMethod;

   /** The datacash reference id, to which refund should be happen. */
   private String datacashReference;

   /** The merchant reference id. */
   private String merchantReference;

   /** The auth code sent by the data-cash. */
   private String authCode;

   /** The transaction amount. */
   private Money transactionAmount;

   /**
    * Tracking data contains token,sessionId and inventory booking reference number required for
    * logging.
    */
   private String trackingData;

   /** The captureMethod value. */
   private String captureMethod;
   

	private String refundStatus;
	
   private String refundReferenceId;
   
	public String getRefundReferenceId() {
		return refundReferenceId;
	}

	public void setRefundReferenceId(String refundReferenceId) {
		this.refundReferenceId = refundReferenceId;
	}

	private String refundResponseCode;
	
	private String reason;
	

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}



	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	

	public String getRefundResponseCode() {
		return refundResponseCode;
	}

	public void setRefundResponseCode(String refundResponseCode) {
		this.refundResponseCode = refundResponseCode;
	}

   /**
    * The Constructor for refund transactions.
    *
    * @param paymentTypeCode the payment type
    * @param transactionAmount the transaction amount
    * @param datacashReference the datacashReference number.
    * @param merchantReference the merchantReference number.
    */
   public DataCashHistoricTransaction(String paymentTypeCode, Money transactionAmount,
                                      String datacashReference, String merchantReference)
   {
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
      this.datacashReference = datacashReference;
      this.merchantReference = merchantReference;
   }

   /**
    * The overloaded constructor for backEndPayment transactions.
    *
    * @param datacashReference the datacashReference
    * @param authCode the authCode
    * @param merchantReference the merchantReference
    */
   public DataCashHistoricTransaction(String datacashReference, String authCode,
                                      String merchantReference)
   {
      this.datacashReference = datacashReference;
      this.authCode = authCode;
      this.merchantReference = merchantReference;
   }

   /**
    * Gets the payment type.
    *
    * @return the payment type
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the datacash reference.
    *
    * @return the datacash reference
    */
   public String getDatacashReference()
   {
      return datacashReference;
   }

   /**
    * Gets the PaymentMethod associated with this Transaction.
    *
    * @return the PaymentMethod
    */
   public PaymentMethod getPaymentMethod()
   {
      return paymentMethod;
   }

   /**
    * Gets the authorization code.
    *
    * @return the authorization code
    */
   public String getAuthCode()
   {
      return authCode;
   }

   /**
    * Sets the PaymentMethod associated with this Transaction.PaymentMethod will be REFUND_CARD for
    * refund transactions and DATACASH for backEnd transactions.
    *
    * @param paymentMethod the PaymentMethod.
    */
   public void setPaymentMethod(PaymentMethod paymentMethod)
   {
      this.paymentMethod = paymentMethod;
   }

   /**
    * This method sets the tracking data. Tracking data contains token,sessionId and inventory
    * booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
      this.trackingData = trackingdata;
   }

   /**
    * This method is responsible for validating DatacashReference value.
    *
    * @throws PaymentValidationException the PaymentValidationException
    */
   public void validate() throws PaymentValidationException
   {
      Pattern referencePattern = Pattern.compile("^[0-9]*$");
      if (datacashReference.length() != DATACASH_REFERENCE_LENGTH
         || !referencePattern.matcher(datacashReference).matches())
      {
         throw new PaymentValidationException("reference.value.invalid");
      }
   }

   /**
    * Creates a <code>Money</code> object, with the absolute value of total amount.
    *
    * @return The <code>Money</code> object for the total amount.
    */
   private Money getTotalAmount()
   {
      return new Money(transactionAmount.getAmount().abs(), transactionAmount.getCurrency());
   }

   /**
    * Gets the ZERO transaction charge for Refund transaction.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return new Money(BigDecimal.ZERO, transactionAmount.getCurrency());
   }

   /**
    * Clears sensitive data present in the transaction.
    */
   public void clearSensitiveData()
   {
      // Do nothing, empty implementation.
   }

   /**
    * Gets essential transaction data associated with RefundHistoricTransaction.
    *
    * @return essentialTransactionData the essential transaction data.
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      EssentialTransactionData essentialTransactionData =
         new EssentialTransactionData(paymentTypeCode, transactionAmount,
            new Money(new BigDecimal(0), null), getPaymentMethod());
      if(ConfReader.getBooleanEntry("shorexid.enable", false)){
    	  if("1".equals(this.refundResponseCode)){
        	  essentialTransactionData.setPaymentStatus(PaymentStatus.FULFILLED);
        	  essentialTransactionData.getPaymentStatus().setDatacashResponseCode(this.refundResponseCode);
        	  
          }else{
        	  essentialTransactionData.setPaymentStatus(PaymentStatus.REJECTED);
        	  essentialTransactionData.getPaymentStatus().setDatacashResponseCode(this.refundResponseCode);
          }
      }

      essentialTransactionData.setTransactionReference(datacashReference);
      return essentialTransactionData;
   }

   /**
    * This method is responsible for refund transaction on the passed datacashreference.This method
    * accepts a negative transaction amount which indicates it is a refund.
    *
    * @param client the client account.
    * @param dataCashReference the dataCashReference
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void historicRefund(String client, String dataCashReference)
      throws DataCashServiceException
   {
      Response response =
         DataCashServiceFactory.getDataCashService(client).refund(dataCashReference,
            getTotalAmount());
      processTransaction(response, client, REVERSE_TRANSACTION);
   }

   /**
    * This method is responsible for refund transaction.This method accepts a negative transaction
    * amount which indicates it is a refund.
    *
    * @param client the client account.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void historicRefund(String client) throws DataCashServiceException
   {
      historicRefund(client, datacashReference);
   }

   /**
    * This method is responsible for fulfilling the actual payment. The transaction amount must be
    * positive .
    *
    * @param client The client account.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void historicFulfill(String client) throws DataCashServiceException
   {
      Response response =
         DataCashServiceFactory.getDataCashService(client).fulfill(datacashReference, authCode,
            null);
      processTransaction(response, client, FULFILL_TRANSACTION);
   }

   /**
    * This method is responsible for performing cancellation transaction.
    *
    * @param client the client account.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void historicCancel(String client) throws DataCashServiceException
   {
      if (datacashReference != null)
      {
         Response response =
            DataCashServiceFactory.getDataCashService(client).cancel(datacashReference);
         processTransaction(response, client, CANCEL_TRANSACTION);
      }
   }

   /**
    * This method is responsible for performing preRegisteredRefund transaction for the
    * dataCashReference number and the vTid passed.
    *
    * @param client the client account.
    * @param dataCashReference the dataCashReference number.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    * occurs.
    */
   public void preRegisteredRefund(String client, String dataCashReference)
      throws DataCashServiceException
   {
      Response response =
         DataCashServiceFactory.getDataCashService(client).preRegisteredRefund(dataCashReference,
            merchantReference, getTotalAmount(), captureMethod);
      processTransaction(response, client, REFUND_TRANSACTION);
   }



   //added by sreenivasulu for refund
   public void preRegisteredPayPalRefund(String client, String dataCashReference)
		      throws DataCashServiceException
		   {
		      Response response =
		         DataCashServiceFactory.getDataCashService(client).preRegisteredPayPalRefund(dataCashReference,
		            merchantReference, getTotalAmount());
		      processTransaction(response, client, REFUND_TRANSACTION);
		   }

   /**
    * This method will log the transaction details and checks if the transaction is successful and
    * sets the dataCashReference number.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the Datacash account vTid.
    * @param transactionType the datacash transaction type.
    *
    * @throws DataCashServiceException when transaction fails.
    */
   private void processTransaction(Response response, String vTid, String transactionType)
      throws DataCashServiceException
   {
      logTransaction(transactionType, response, vTid);
      // If the code = 1, the transaction is accepted.
      // In all other cases the transaction will fail and an exception
      // should be thrown.
      // DataCashServiceConstants.SUCCESS_STATUS should be set to 1.
      if (response.getCode() == DataCashServiceConstants.SUCCESS_STATUS) {
			if(StringUtils.isNotEmpty(this.refundReferenceId)&&ConfReader.getBooleanEntry("shorexid.enable",false)){  
			this.refundStatus="success";
			this.refundResponseCode=response.getCode()+"";
			datacashReference = response.getDatacashReference();
		//	updateUniqueIdWithTextFile(this.refundReferenceId,response.getCode());
			}			
			datacashReference = response.getDatacashReference();
			return;
		}
		this.refundStatus="rejected";
		this.refundResponseCode=Integer.toString(response.getCode());
		this.reason=response.getDescription();
		populateDataCashServiceException(response, vTid);
  }

   /**
    * This method will take the dataCash transactionType and cardResponse as the input and logs the
    * required details.
    *
    * @param transactionType the dataCash transaction type.
    * @param response The <code>CardResponse</code> object.
    * @param vTid the dataCash client account.
    *
    */
   private void logTransaction(String transactionType, Response response, String vTid)
   {
      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      String dateTime = dateFormat.format(date);
      StringBuilder trackingInfo = new StringBuilder();
      trackingInfo.append(transactionType).append(COMMA).append(dateTime).append(COMMA).append(
         trackingData).append(COMMA).append(merchantReference).append(COMMA).append(
         response.getDatacashReference()).append(COMMA).append(COMMA).append(COMMA);
      if (transactionAmount != null)
      {
         trackingInfo.append(transactionAmount.getCurrency()).append(COMMA).append(
            transactionAmount.getAmount()).append(COMMA);
      }
      else
      {
         trackingInfo.append(COMMA).append(COMMA);
      }
      trackingInfo.append(vTid).append(COMMA).append(getResponseCode(response.getCode())).append(
         COMMA).append(response.getDescription());
      LogWriter
         .logInfoMessage(trackingInfo.toString(), DataCashHistoricTransaction.class.getName());
   }

   /**
    * This method takes the dataCash response code as the input parameter and returns the three
    * digit code required for logging. ex:if it takes 1 is the input it returns 001.
    *
    * @param code the response code.
    *
    * @return String the response code.
    */
   private String getResponseCode(int code)
   {
      return StringUtils.leftPad(String.valueOf(code), RESPONSE_CODE_LENGTH, '0');
   }

   /**
    * To throw DataCashServiceException.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the data-cash account vTid.
    *
    * @throws DataCashServiceException when transaction fails.
    */
   private void populateDataCashServiceException(Response response, String vTid)
      throws DataCashServiceException
   {
      String responseMsg = response.getDescription();
      LogWriter.logErrorMessage(responseMsg);
      int errorCode = response.getCode();
      String errorMsg =
         PropertyResource.getProperty("datacash.response." + errorCode, MESSAGES_PROPERTY);
      if (StringUtils.isEmpty(errorMsg))
      {
         errorMsg = responseMsg;
      }
      LogWriter.logErrorMessage(PropertyResource.getProperty("datacash.generic.error",
         MESSAGES_PROPERTY)
         + vTid);
      LogWriter.logErrorMessage("Datacash Error: " + errorCode + "-" + errorMsg);
      throw new DataCashServiceException(errorCode, "datacash.response." + errorCode);
   }

   /**
    * Set the captureMethod value.
    * @param captureMethod the captureMethod to set
    */
   public void setCaptureMethod(String captureMethod)
   {
      this.captureMethod = captureMethod;
   }
   
	public boolean validateRequestWithTextFile(String refundId, File file) throws DataCashServiceException{
		boolean validateRefundId = true;
		ObjectInputStream ois = null;
		Object data = null;		
				
				try{
					if (!file.exists())
					file.createNewFile();
					else{
						int length=(int) file.length();
						if(length!=0){
						ois = new ObjectInputStream(new FileInputStream(file));
						data = ois.readObject();
						}
					}				
			} catch (EOFException eoex) {}
				catch(Exception ioex){
					ioex.printStackTrace();
				
			}
			if (data != null) {
				ArrayList<EssentialTransactionData> essentialData = (ArrayList<EssentialTransactionData>) data;
				for (EssentialTransactionData etdData : essentialData) {
					if (StringUtils.equals(refundId,
							etdData.getRefundReferenceId())) {
						updateTransactionStatus();
						validateRefundId = false;					     
					    LogWriter.logErrorMessage("Refund Unique ID validation Failed , error code is 03" );
					    throw new DataCashServiceException(03, "datacash.uid.error.03");

					}

				}
			}
		

		return validateRefundId;
	}

	/**
	 * 
	 */
	private void updateTransactionStatus() {
		  this.setRefundResponseCode("03");
		  this.setRefundStatus("rejected");
		  this.setReason("Duplicate Reference Id");
		
	}

	public synchronized void updateUniqueIdWithTextFile()throws DataCashServiceException {
		String refundReferenceId=this.refundReferenceId+"";
		String datacashResponseCode=this.refundResponseCode;
		EssentialTransactionData essentialTransactionData=getEssentialTransactionData();
		essentialTransactionData.setRefundReferenceId(refundReferenceId);
		essentialTransactionData.setRefundProcessedDate(new Date());
		essentialTransactionData.setPaymentStatus(PaymentStatus.FULFILLED);
		essentialTransactionData.getPaymentStatus().setPaymentStatus(datacashResponseCode);
		ObjectInputStream ois = null;
		
		ArrayList<EssentialTransactionData> essentialData=null;
		try {
			File file = new File(ConfReader.getConfEntry("shorex.file", null)+"/shorexid.ser");
			if (!file.exists()) {
				file.createNewFile();
				}			
			Object data=null;
			long length=file.length();
			if(length!=0){
			ois = new ObjectInputStream(new FileInputStream(file));		
			
			data = ois.readObject();
			ois.close();
			}
			if(data==null){
			 essentialData=new ArrayList<EssentialTransactionData>();
			}else{
			 essentialData = (ArrayList<EssentialTransactionData>) data;			
			}
			essentialData.add(essentialTransactionData);	
			
			
			File updateFile=new File(ConfReader.getConfEntry("shorex.file", null)+"/shorexid.ser");
	//		FileChannel channel = new RandomAccessFile(updateFile, "rw").getChannel();
	//		 FileLock lock = channel.lock();
			
			FileOutputStream fos=new FileOutputStream(updateFile);			
			ObjectOutputStream oos = new ObjectOutputStream(fos);			
			oos.writeObject(essentialData);
		
			oos.close();
	//		lock.release();
	//		channel.close();

		} catch (Exception dcse) {
			LogWriter.logErrorMessage("Error while updating the file : "
					+ dcse.getMessage());
			throw new DataCashServiceException("Error while updating the file :"+dcse);

		}

	}
}
