/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Payment.java,v $
 *
 * $Revision: 1.8 $
 *
 * $Date: 2008-05-15 12:19:02 $
 *
 * $Author : bibin.j$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import java.util.ArrayList;
import java.util.List;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.PaymentMethod;

/**
 * This class represents the Payment domain object.
 *
 * @author bibin.j
 */
public final class Payment
{

   /** Represents the list of payment transactions. */
   private List<PaymentTransaction> paymentTransactions = new ArrayList<PaymentTransaction>();

   /**
    * Gets the payment Transaction.
    *
    * @return the paymentTransactions
    */
   public List<PaymentTransaction> getPaymentTransactions()
   {
      return paymentTransactions;
   }

   /**
    * Sets the paymentTransaction.
    *
    * @param paymentTransaction the paymentTransaction to set
    */
   public void setPaymentTransaction(List<PaymentTransaction> paymentTransaction)
   {
      this.paymentTransactions = paymentTransaction;
   }

   /**
    * Adds to the paymentTransactions.
    *
    * @param transaction the transaction to set
    */
   public void addPaymentTransaction(PaymentTransaction transaction)
   {
      this.paymentTransactions.add(transaction);
   }

   /**
    * Gets the list of specific payment transactions based on passed PaymentMode. Eg., list
    * DatacashPaymentTransactions, list of CashPaymentTransactions, etc.,
    *
    * @param paymentMethod The payment method of the required payment transaction.
    *
    * @return the list of <code>PaymentTransaction</code> objects.
    */
   public List<PaymentTransaction> getPaymentTransactions(PaymentMethod paymentMethod)
   {
      List<PaymentTransaction> specificPaymentTransactions = new ArrayList<PaymentTransaction>();
      for (PaymentTransaction paymentTransaction : paymentTransactions)
      {
         if (paymentMethod == paymentTransaction.getPaymentMethod())
         {
            specificPaymentTransactions.add(paymentTransaction);
         }
      }
      return specificPaymentTransactions;
   }

   /**
    * Clears sensitive data in all transaction objects. This method is called just prior to
    * dereferencing the object. This intention is that no sensitive data resides in memory after the
    * object has been de-referenced, before it has been garbage collected. All sensitive data should
    * be held in character arrays--not Strings
    */
   public void clearSensitiveData()
   {
      for (PaymentTransaction transaction : paymentTransactions)
      {
         transaction.clearSensitiveData();
      }
   }

   /**
    * Gets the list of essential transaction data associated with payment transaction.
    *
    * @return essentialTransactionData the list of essential transaction data.
    */
   public List<EssentialTransactionData> getAllEssentialTransactionsData()
   {
      List<EssentialTransactionData> essentialTransactionData =
         new ArrayList<EssentialTransactionData>();
      for (PaymentTransaction transaction : paymentTransactions)
      {
         if (transaction != null && transaction.getPaymentMethod() != PaymentMethod.POST_PAYMENT)
         {
            essentialTransactionData.add(transaction.getEssentialTransactionData());
         }
      }
      return essentialTransactionData;
   }

   /**
    * Clears the List of the payment transactions.
    */
   public void clearPaymentTransactions()
   {
      paymentTransactions.clear();
   }

}
