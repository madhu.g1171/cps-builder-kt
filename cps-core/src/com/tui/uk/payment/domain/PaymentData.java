/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PaymentData.java,v $
 *
 * $Revision: 1.5 $
 *
 * $Date: 2008-05-14 07:14:57 $
 *
 * $author : bibin.j$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import static com.tui.uk.payment.dispatcher.DispatcherConstants.DEFAULT_FAILURE_COUNT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.MAX_FAILURE_COUNT;

import java.util.ArrayList;
import java.util.List;

import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;

/**
 * This class represents PaymentData.
 *
 *
 * @author bibin.j
 *
 */
public final class PaymentData
{

   /** Represents bookingComponent which should be set by client application
    * to hold any other non payment/booking related data to be displayed in Payment page. */
   private BookingInfo bookingInfo;

   /** The reference to Payment object. */
   private Payment payment;

   /** The failure count. */
   private int failureCount;

   /** Fraud Screening Card data list. */
   private List<FailedCardData> failedCardDataList = new ArrayList<FailedCardData>();

   /** The booking session identifier. */
   private String bookingSessionId;
   
   /** The Transaction Status. */
   private TransactionStatus transactionStatus;

  

   /**
    * The Constructor.
    *
    * @param bookComponent the bookingComponent.
    */
   public PaymentData(BookingComponent bookComponent)
   {
      this.bookingInfo = new BookingInfo(bookComponent);
   }

   /**
    * Constructor with <code>BookingInfo</code> object.
    *
    * @param bookingInfo The <code>BookingInfo</code> object.
    */
   public PaymentData(BookingInfo bookingInfo)
   {
      this.bookingInfo = bookingInfo;
   }

   /**
    * Get bookingInfo.
    *
    * @return the bookingComponent
    */
   public BookingInfo getBookingInfo()
   {
      return bookingInfo;
   }

   /**
    * Returns the payment object.
    *
    * @return payment payment.
    */
   public Payment getPayment()
   {
      return payment;
   }

   /**
    * Gets the failure count.
    *
    * @return the failureCount.
    */
   public int getFailureCount()
   {
      return failureCount;
   }

   /**
    * Gets the Fraud Screening Card Data list.
    *
    * @return the failedCardDataList.
    */
   public List<FailedCardData> getFailedCardDataList()
   {
      return failedCardDataList;
   }

   /**
    * Gets the booking session id.
    *
    * @return bookingSessionId.
    */
   public String getBookingSessionId()
   {
      return bookingSessionId;
   }

   /**
    * Set payment.
    *
    * @param payment payment.
    */
   public void setPayment(Payment payment)
   {
      this.payment = payment;
   }
   
   /**
    * Gets transaction status.
    *
    * @return transactionStatus.
    */
   public TransactionStatus getTransactionStatus()
   {
      return transactionStatus;
   }

   /**
    * Sets transaction status.
    * @param transactionStatus transactionStatus.
    */
   public void setTransactionStatus(TransactionStatus transactionStatus)
   {
      this.transactionStatus = transactionStatus;
   }

   /**
    * Sets the failure count by incrementing it.
    */
   public void setFailureCount()
   {
      failureCount++;
      String clientApplication =
         this.bookingInfo.getBookingComponent()
                .getClientApplication().getClientApplicationName();
      int defaultFailureCount = ConfReader.getIntEntry(
         clientApplication + "." + MAX_FAILURE_COUNT, 0);
      if (defaultFailureCount == 0)
      {
         defaultFailureCount =  ConfReader.getIntEntry(
          "default." + MAX_FAILURE_COUNT, DEFAULT_FAILURE_COUNT);
      }
      if (failureCount >= defaultFailureCount)
      {
         bookingInfo.setNewHoliday(Boolean.FALSE);
      }
   }

   /**
    * Updates the fraud screening card data list.
    *
    * @param failedCardDataList the fraud screening card data list to be added.
    */
   public void setFailedCardDataList(List<FailedCardData> failedCardDataList)
   {
      this.failedCardDataList.addAll(failedCardDataList);
   }

   /**
    * Sets the booking session id.
    *
    * @param bookingSessionId the booking session id.
    */
   public void setBookingSessionId(String bookingSessionId)
   {
      this.bookingSessionId = bookingSessionId;
   }

   /**
    * This method purges the card data associated with each of the FraudScreeningCardData object.
    */
   public void purgeSensitiveFailedCardData()
   {
      for (FailedCardData failedCardData : failedCardDataList)
      {
         failedCardData.purgeCardData();
      }
   }

}
