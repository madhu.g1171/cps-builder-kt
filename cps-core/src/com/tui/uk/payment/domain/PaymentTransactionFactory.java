/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentTransactionFactory.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.domain.PaymentConstants.COMMA;
import static com.tui.uk.payment.domain.PaymentConstants.HYPHEN;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH_ACSS;
import static com.tui.uk.payment.domain.PaymentConstants.THREED_NON_FATAL_DUP_RESP_ERROR;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.client.domain.PaymentMode;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BillingAddress;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;

/**
 * This class is responsible for creating a list of <code>PaymentTransaction</code> objects, from
 * the transactions selected in by the user in the Payment Page.
 *
 * @author sindhushree.g
 *
 */
public final class PaymentTransactionFactory
{

   /** The constant DOT. */
   public static final String DOT = ".";

   /** The constant COLON. */
   public static final String COLON = ":";

   /** The constant empty string array. */
   public static final String[] EMPTY_LIST = new String[1];

   /** The constant empty string. */
   public static final String EMPTY_LISTS = "";

   /**
    * Default constructor to stop instantiation.
    */
   private PaymentTransactionFactory()
   {
      // Stops instantiation of this class.
   }

   /**
    * This method validates the payment details entered by the user and creates a list of
    * <code>PaymentTransaction</code> objects if the entered values are valid.
    *
    * @param index The index of the transaction.
    * @param requestParameterMap A map containing values entered by the user.
    * @param bookingInfo The bookingInfo.
    *
    * @return The <code>Payment</code> object created from values entered by the user.
    *
    * @throws PaymentValidationException if the entered payment details are invalid.
    */
   public static PaymentTransaction getPaymentTransactions(int index,
      Map<String, String> requestParameterMap, BookingInfo bookingInfo)
      throws PaymentValidationException
   {
      String prefix = PaymentConstants.PAYMENT + index;
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      Money transactionAmount = null;
      Money transactionCharge = null;
      PaymentTransaction paymentTransaction = null;
      String paymentAmount = requestParameterMap.get(prefix + PaymentConstants.TRANSACTION_AMOUNT);
      Currency currency = bookingComponent.getTotalAmount().getCurrency();
      String paymentCharge = requestParameterMap.get(prefix + PaymentConstants.CHARGE);
      if (paymentAmount != null && paymentAmount.length() > 0)
      {
         validateAmount(paymentAmount);
         transactionAmount =
            new Money(BigDecimal.valueOf(Double.parseDouble(paymentAmount)), currency);
      }
      if (paymentCharge != null && paymentCharge.length() > 0)
      {
         validateAmount(paymentCharge);
         transactionCharge =
            new Money(BigDecimal.valueOf(Double.parseDouble(paymentCharge)), currency);
      }

      PaymentMethod paymentMethod =
    	         PaymentMethod.valueFromCode(requestParameterMap.get(prefix
    	            + PaymentConstants.PAYMENT_METHOD));

      paymentTransaction =
         getPaymentTransaction(index, bookingInfo, transactionCharge, transactionAmount,
            paymentMethod, requestParameterMap);
      if (paymentTransaction != null)
      {
         paymentTransaction.validate();
      }
      return paymentTransaction;
   }
   /*this method devloped for HCCS*/
   public static PaymentTransaction getHpsPaymentTransactions(int index,
		      Map<String, String> requestParameterMap, BookingInfo bookingInfo)
		      throws PaymentValidationException
	{

		String prefix = PaymentConstants.PAYMENT + index;
		
		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		Map<String, String> hccQueryResponseMap = bookingComponent.getHccCapturedData();

		BigDecimal transAmt = new BigDecimal(hccQueryResponseMap.get(prefix + PaymentConstants.TRANSACTION_AMOUNT));
		BigDecimal transChrge = new BigDecimal("0.0");
		Currency currency = Currency.getInstance(hccQueryResponseMap.get("CURRENCY"));
		Money transactionAmount = new Money(transAmt, currency);
		Money transactionCharge = new Money(transChrge, currency);

		String merchantReference = bookingComponent.getMerchantReference();
		String paymentTypeCode = hccQueryResponseMap.get(prefix + PaymentConstants.PAYMENT_TYPE_CODE);
		bookingComponent.getNonPaymentData().put(BookingConstants.IS_DATACASH_SELECTED, String.valueOf(true));
//		DataCashPaymentTransaction dataCashPaymentTransaction = new DataCashPaymentTransaction(paymentTypeCode,transactionAmount,
//				transactionCharge, merchantReference);

		DataCashPaymentTransaction dataCashPaymentTransaction = new DataCashPaymentTransaction(paymentTypeCode,transactionAmount,
				transactionCharge, createCnpCard(prefix, hccQueryResponseMap, bookingInfo), merchantReference);

		dataCashPaymentTransaction.setFatalErrorCodes(populateFatalErrors(
				bookingInfo.getBookingComponent().getClientApplication().getClientApplicationName()));
		
		/*
		 * dataCashPaymentTransaction.setNonFatalErrorCodes(populateNonFatalErrors(
		 * bookingInfo.getBookingComponent().getClientApplication().
		 * getClientApplicationName()));
		 */
		
		/*
		 * dataCashPaymentTransaction.setThreeDNonFatalErrorCodes(
		 * populateHcc3dNonFatalErrors(
		 * bookingInfo.getBookingComponent().getClientApplication().
		 * getClientApplicationName()));
		 */
		
		dataCashPaymentTransaction.setHccThreeDNonFatalErrorList(populateHcc3dNonFatalErrors(
				bookingInfo.getBookingComponent().getClientApplication().getClientApplicationName()));
		
		dataCashPaymentTransaction.setThreeDDuplicateRespErrorList(populate3dDuplicateRespError());

		dataCashPaymentTransaction.setTrackingData(bookingInfo.getTrackingData().toString());

		dataCashPaymentTransaction.setCaptureMethod(bookingComponent.getClientApplication().getCaptureMethod());

		String authCode = null;

		if (StringUtils.isNotBlank(authCode)) {
			dataCashPaymentTransaction.setAuthCode(authCode);
		}
		return dataCashPaymentTransaction;

	}

   /**
    * This method checks for the pattern
    * [&pound;{0-1}][-{0-1}][digit{0-n}][.{0-1}][digit{0-n}].Returns a boolean value if true.
    *
    * @param expr an input string whose pattern needs to be checked.
    *
    * @throws PaymentValidationException if the pattern does not match.
    */
   private static void validateAmount(String expr) throws PaymentValidationException
   {
      Pattern pattern = Pattern.compile("^[-]?[\\d]*[.]?[\\d]+$");
      Matcher match = pattern.matcher(expr);
      if (!match.matches())
      {
         throw new PaymentValidationException("amount.validation.failed");
      }
   }

   /**
    * This method is used to return a PaymentTransaction instance.
    *
    * @param index - The index value.
    * @param bookingInfo - The BookingInfo instance.
    * @param transactionCharge - The transactionCharge.
    * @param transactionAmount - The transaction amount.
    * @param requestParameterMap - The requestParameterMap.
    * @param paymentMethod - The payment method.
    *
    * @return - The PaymentTransaction instance.
    * @throws PaymentValidationException - The PaymentValidationException instance.
    */
   private static PaymentTransaction getPaymentTransaction(int index, BookingInfo bookingInfo,
      Money transactionCharge, Money transactionAmount, PaymentMethod paymentMethod,
      Map<String, String> requestParameterMap) throws PaymentValidationException
   {
      String prefix = PaymentConstants.PAYMENT + index;
      String authCode = requestParameterMap.get(prefix + PaymentConstants.AUTH_CODE);
      String paymentTypeCode = requestParameterMap.get(prefix + PaymentConstants.PAYMENT_TYPE_CODE);
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      PaymentTransaction paymentTransaction = null;

      switch (paymentMethod)
      {
         case CASH:
            paymentTransaction = new CashPaymentTransaction(paymentTypeCode, transactionAmount);
            break;
         case CHEQUE:
            paymentTransaction = new ChequePaymentTransaction(paymentTypeCode, transactionAmount);
            break;
         case VOUCHER:
            paymentTransaction =
               getVoucherPaymentTransaction(requestParameterMap, prefix, paymentTypeCode,
                  transactionAmount);
            break;
         case DATACASH:
            paymentTransaction =
               getDataCashPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
                  prefix, requestParameterMap, bookingInfo, authCode);
            break;
         case GIFT_CARD:
            paymentTransaction =
               getGiftCardPaymentTransaction(paymentTypeCode, transactionAmount, prefix,
                  requestParameterMap, bookingComponent);
            break;
         case POST_PAYMENT:
            paymentTransaction =
               getPostPaymentTransaction(bookingInfo, paymentTypeCode, requestParameterMap, prefix);
            break;
         case PDQ:
            paymentTransaction =
               new PDQPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
                  authCode);
            break;
         case PIN_PAD:
            paymentTransaction =
               getPinPadTransaction(paymentTypeCode, transactionAmount, authCode,
                  transactionCharge, index, requestParameterMap, bookingComponent);
            break;
         case NO_PAYMENT:
            paymentTransaction = new NoPaymentTransaction(paymentTypeCode, transactionAmount);
            break;
         case REFUND_CARD:
            paymentTransaction =
               getDataCashHistoricTransaction(paymentTypeCode, transactionAmount,
                  transactionCharge, prefix, requestParameterMap, bookingComponent,
                  bookingInfo.getTrackingData());
            break;
         default:
            handleDefaultTransaction();
      }
      return paymentTransaction;
   }

   /**
    * Creates the <code>PaymentTransaction</code> object for vouchers.
    *
    * @param requestParameterMap A map containing values entered by the user.
    * @param prefix The index prefix of the transaction.
    * @param paymentTypeCode The payment type code of the transaction.
    * @param transactionAmount The amount of the transaction.
    *
    * @return The created <code>PaymentTransaction</code> object.
    *
    * @throws PaymentValidationException when voucher amount validation fails.
    */
   private static PaymentTransaction getVoucherPaymentTransaction(
      Map<String, String> requestParameterMap, String prefix, String paymentTypeCode,
      Money transactionAmount) throws PaymentValidationException
   {
      Map<String, Money> voucherLineItems = new HashMap<String, Money>();
      for (int j = 0; j <= Integer.parseInt(requestParameterMap.get(prefix
         + PaymentConstants.VOUCHER_INDEX)); j++)
      {
         String voucherCode =
            requestParameterMap.get(prefix + PaymentConstants.VOUCHER_CODE + "[" + j + "]");

         String voucherAmount =
            requestParameterMap.get(prefix + PaymentConstants.VOUCHER_AMOUNT + "[" + j + "]");

         validateAmount(voucherAmount);

         voucherLineItems.put(
            voucherCode,
            new Money(BigDecimal.valueOf(Double.parseDouble(voucherAmount)), transactionAmount
               .getCurrency()));
      }
      return new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItems);
   }

   /**
    * Gets the DataCashPaymentTransaction object.
    *
    * @param paymentTypeCode the selected payment type code.
    * @param transactionAmount the transaction amount.
    * @param transactionCharge the transaction charge.
    * @param prefix the prefix.
    * @param requestParameterMap map of user entered details.
    * @param bookingInfo the BookingInfo.
    * @param authCode the authCode.
    *
    * @return the DataCashPaymentTransaction.
    */
   private static DataCashPaymentTransaction getDataCashPaymentTransaction(String paymentTypeCode,
      Money transactionAmount, Money transactionCharge, String prefix,
      Map<String, String> requestParameterMap, BookingInfo bookingInfo, String authCode)
   {
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      bookingComponent.getNonPaymentData().put(BookingConstants.IS_DATACASH_SELECTED,
         String.valueOf(true));
      DataCashPaymentTransaction dataCashPaymentTransaction =
         new DataCashPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
            createCnpCard(prefix, requestParameterMap, bookingInfo),
            getMerchantReference(bookingComponent));

      dataCashPaymentTransaction.setFatalErrorCodes(populateFatalErrors(bookingInfo
         .getBookingComponent().getClientApplication().getClientApplicationName()));
      dataCashPaymentTransaction.setNonFatalErrorCodes(populateNonFatalErrors(bookingInfo
         .getBookingComponent().getClientApplication().getClientApplicationName()));
      dataCashPaymentTransaction.setThreeDNonFatalErrorCodes(populate3dNonFatalErrors(bookingInfo
         .getBookingComponent().getClientApplication().getClientApplicationName()));
      dataCashPaymentTransaction.setThreeDDuplicateRespErrorList(populate3dDuplicateRespError());


      dataCashPaymentTransaction.setTrackingData(bookingInfo.getTrackingData().toString());

      dataCashPaymentTransaction.setCaptureMethod(bookingComponent.getClientApplication()
         .getCaptureMethod());

      if (StringUtils.isNotBlank(authCode))
      {
         dataCashPaymentTransaction.setAuthCode(authCode);
      }
      return dataCashPaymentTransaction;
   }

   /**
    * This method is responsible for creating card.
    *
    * @param prefix the prefix.
    * @param requestParameterMap the request parameter map.
    * @param bookingInfo the BookingInfo.
    * @return Card.
    */
   private static CnpCard createCnpCard(String prefix, Map<String, String> requestParameterMap,
      BookingInfo bookingInfo)
   {
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      char[] cardNumber =
         (requestParameterMap.get(prefix + PaymentConstants.CARD_NUMBER)).toCharArray();
      String nameOnCard = requestParameterMap.get(prefix + PaymentConstants.NAME_ON_CARD);
      String expiryMonth = requestParameterMap.get(prefix + PaymentConstants.EXPIRY_MONTH);
      String expiryYear = requestParameterMap.get(prefix + PaymentConstants.EXPIRY_YEAR);
    //boolean isHccTrue = ConfReader.getBooleanEntry("tuicommon.hcc", false);
		boolean isHccTrue = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
      char[] securityCode = null;
      if (isHccTrue) {
    	  char[] securityCode1 = {'0','0','0'};
    	  securityCode = securityCode1;
	} else {
		 securityCode =
		         (requestParameterMap.get(prefix + PaymentConstants.SECURITY_CODE)).toCharArray();
	}

     /* char[] securityCode =
         (requestParameterMap.get(prefix + PaymentConstants.SECURITY_CODE)).toCharArray();*/
      String postCode =
         StringUtils.upperCase(requestParameterMap.get(prefix + PaymentConstants.POST_CODE));
      String cardType = requestParameterMap.get(prefix + PaymentConstants.PAYMENT_TYPE_CODE);
      String expiryDate = expiryMonth.concat("/").concat(expiryYear);
      String houseno = requestParameterMap.get(prefix + PaymentConstants.HOUSENO);
      String streetAddress = requestParameterMap.get(prefix + PaymentConstants.STREETADDRESS);
      String town = requestParameterMap.get(prefix + PaymentConstants.TOWN);
      String county = requestParameterMap.get(prefix + PaymentConstants.COUNTY);
      String country = requestParameterMap.get(prefix + PaymentConstants.SELECTED_COUNTRY);
      String countryCode = requestParameterMap.get(prefix + PaymentConstants.SELECTED_COUNTRY_CODE);
     /* String additionalAddressLine1 =
         requestParameterMap.get(prefix + PaymentConstants.ADDITIONAL_ADDRESS_LINE1);
      String additionalAddressLine2 =
         requestParameterMap.get(prefix + PaymentConstants.ADDITIONAL_ADDRESS_LINE2);*/

      String additionalAddressLine1 =
    	         requestParameterMap.get(prefix + PaymentConstants.STREETADDRESS);
    	      String additionalAddressLine2 =
    	         requestParameterMap.get(DataCashServiceConstants.STREET_LINE2_KEY);

      // Some laser cards have a minimum as well as maximum security code length.
      // If client application sends minSecurityLength then that value is set to CnpCard
      // otherwise minSecurityLength is assigned with maxSecurityLength and set to CnpCard.
      int minSecurityLength = 0;
      int maxSecurityLength = 0;
      String key = PaymentMode.CUSTOMERNOTPRESENT.getCode();
      if (!bookingComponent.getPaymentType().containsKey(key))
      {
         key = PaymentMode.POSTPAYMENTGUARANTEE.getCode();
      }
      if (bookingComponent.getPaymentType() != null)
      {
         for (PaymentType paymentType : bookingComponent.getPaymentType().get(key))
         {
            if (paymentType.getPaymentCode().equalsIgnoreCase(cardType))
            {
               minSecurityLength = paymentType.getCardType().getMinSecurityCodeLength();
               maxSecurityLength = paymentType.getCardType().getSecurityCodeLength();
            }
         }
      }
      CnpCard card =
         new CnpCard(cardNumber, nameOnCard, expiryDate, securityCode, postCode, cardType,
            minSecurityLength, maxSecurityLength);
      String combinedStreetAddress =
         createCombinedStreetAddress(streetAddress, additionalAddressLine1, additionalAddressLine2);
      BillingAddress billingAddress =
         createBillingAddress(houseno, combinedStreetAddress, town, county, country, countryCode);
      card.setAddress(billingAddress);
      setPaymentDataMap(prefix, requestParameterMap, bookingInfo, postCode, houseno, streetAddress,
         town, county, country,countryCode, additionalAddressLine1, additionalAddressLine2);
      String client = bookingComponent.getClientApplication().getClientApplicationName();

      card.setCvvPolicies(populateCVVPolices(client));
      card.setAddressPolicies(populateAVSPolices(client));
      card.setPostCodePolicies(populatePCodePolices(client));
      boolean cv2AVSRequired = ConfReader.getBooleanEntry(client + ".CV2AVS.Enabled", false);
      card.setCv2AVSEnabled(cv2AVSRequired);
      if (cardType.equalsIgnoreCase("SOLO") || cardType.equalsIgnoreCase("SWITCH"))
      {
         String issueNumber = requestParameterMap.get(prefix + PaymentConstants.ISSUE_NUMBER);
         String startMonth = requestParameterMap.get(prefix + PaymentConstants.START_MONTH);
         String startYear = requestParameterMap.get(prefix + PaymentConstants.START_YEAR);
         if (StringUtils.isNotBlank(startMonth) && StringUtils.isNotBlank(startYear))
         {
            card.setStartDate(startMonth.concat("/").concat(startYear));
         }
         card.setIssueNumber(issueNumber);
      }
      return card;
   }

   /**
    * Method to populate post code policies.
    *
    * @param client the client.
    * @return postCodePolicies the post code policies.
    */
   private static Map<String, String> populatePCodePolices(String client)
   {
      // the accept and reject value are always in small case.
      Map<String, String> postCodePolicies = new HashMap<String, String>();
      String[] pCodePolicies =
         ConfReader.getStringValues(client + ".PCODE.policies", PaymentConstants.DEFAULT_POLICIES,
            COMMA);
      if (pCodePolicies.length > 0)
      {
         postCodePolicies.put(PaymentConstants.NOT_PROVIDED,
            pCodePolicies[PaymentConstants.NOT_PROVIDED_INDEX]);
         postCodePolicies.put(PaymentConstants.NOT_CHECKED,
            pCodePolicies[PaymentConstants.NOT_CHECKED_INDEX]);
         postCodePolicies.put(PaymentConstants.MATCHED,
            pCodePolicies[PaymentConstants.MATCHED_INDEX]);
         postCodePolicies.put(PaymentConstants.NOT_MATCHED,
            pCodePolicies[PaymentConstants.NOT_MATCHED_INDEX]);
         postCodePolicies.put(PaymentConstants.PARTIAL_MATCH,
            pCodePolicies[PaymentConstants.PARTIAL_MATCH_INDEX]);
      }
      else
      {
         postCodePolicies.put(PaymentConstants.NOT_PROVIDED, PaymentConstants.ACCEPT);
         postCodePolicies.put(PaymentConstants.NOT_CHECKED, PaymentConstants.ACCEPT);
         postCodePolicies.put(PaymentConstants.MATCHED, PaymentConstants.ACCEPT);
         postCodePolicies.put(PaymentConstants.NOT_MATCHED, PaymentConstants.ACCEPT);
         postCodePolicies.put(PaymentConstants.PARTIAL_MATCH, PaymentConstants.ACCEPT);
      }
      return postCodePolicies;
   }

   /**
    * Method to populate AVS policies.
    *
    * @param client the client.
    *
    * @return addressPolicies the address policies.
    */
   private static Map<String, String> populateAVSPolices(String client)
   {
      // the accept and reject value are always in small case.
      Map<String, String> addressPolicies = new HashMap<String, String>();
      String[] addPolicies =
         ConfReader.getStringValues(client + ".AVS.policies", PaymentConstants.DEFAULT_POLICIES,
            COMMA);
      if (addPolicies.length > 0)
      {
         addressPolicies.put(PaymentConstants.NOT_PROVIDED,
            addPolicies[PaymentConstants.NOT_PROVIDED_INDEX]);
         addressPolicies.put(PaymentConstants.NOT_CHECKED,
            addPolicies[PaymentConstants.NOT_CHECKED_INDEX]);
         addressPolicies.put(PaymentConstants.MATCHED, addPolicies[PaymentConstants.MATCHED_INDEX]);
         addressPolicies.put(PaymentConstants.NOT_MATCHED,
            addPolicies[PaymentConstants.NOT_MATCHED_INDEX]);
         addressPolicies.put(PaymentConstants.PARTIAL_MATCH,
            addPolicies[PaymentConstants.PARTIAL_MATCH_INDEX]);
      }
      else
      {
         addressPolicies.put(PaymentConstants.NOT_PROVIDED, PaymentConstants.ACCEPT);
         addressPolicies.put(PaymentConstants.NOT_CHECKED, PaymentConstants.ACCEPT);
         addressPolicies.put(PaymentConstants.MATCHED, PaymentConstants.ACCEPT);
         addressPolicies.put(PaymentConstants.NOT_MATCHED, PaymentConstants.ACCEPT);
         addressPolicies.put(PaymentConstants.PARTIAL_MATCH, PaymentConstants.ACCEPT);
      }
      return addressPolicies;
   }

   /**
    * Method to populate CVV policies.
    *
    * @param client the client.
    *
    * @return cvvPolicies the cvv policies.
    */
   private static Map<String, String> populateCVVPolices(String client)
   {
      // the accept and reject value are always in small case.
      Map<String, String> cvvPolicies = new HashMap<String, String>();
      cvvPolicies.put(PaymentConstants.NOT_PROVIDED, PaymentConstants.REJECT);
      cvvPolicies.put(PaymentConstants.NOT_CHECKED, PaymentConstants.ACCEPT);
      cvvPolicies.put(PaymentConstants.MATCHED, PaymentConstants.ACCEPT);
      cvvPolicies.put(PaymentConstants.NOT_MATCHED, PaymentConstants.REJECT);
      cvvPolicies.put(PaymentConstants.PARTIAL_MATCH, PaymentConstants.REJECT);

      return cvvPolicies;
   }

   /**
    * Gets the GiftCardPaymentTransaction object.
    *
    * @param paymentTypeCode the selected payment type code.
    * @param transactionAmount the transaction amount.
    * @param prefix the prefix.
    * @param requestParameterMap the map of user entered details.
    * @param bookingComponent the BookingComponent.
    *
    * @return the GiftCardPaymentTransaction object.
    */
   private static GiftCardPaymentTransaction getGiftCardPaymentTransaction(String paymentTypeCode,
      Money transactionAmount, String prefix, Map<String, String> requestParameterMap,
      BookingComponent bookingComponent)
   {
      bookingComponent.getNonPaymentData().put(BookingConstants.IS_DATACASH_SELECTED,
         String.valueOf(true));
      return new GiftCardPaymentTransaction(paymentTypeCode, transactionAmount, createGiftCard(
         prefix, requestParameterMap), getMerchantReference(bookingComponent));
   }

   /**
    * This method is responsible for creating card.
    *
    * @param prefix the prefix.
    * @param requestParameterMap the request parameter map.
    * @return Card.
    */
   private static GiftCard createGiftCard(String prefix, Map<String, String> requestParameterMap)
   {
      char[] cardNumber =
         (requestParameterMap.get(prefix + PaymentConstants.CARD_NUMBER)).toCharArray();
      char[] securityCode =
         (requestParameterMap.get(prefix + PaymentConstants.SECURITY_CODE)).toCharArray();
      String cardType = requestParameterMap.get(prefix + PaymentConstants.PAYMENT_TYPE_CODE);

      return new GiftCard(cardNumber, securityCode, cardType);
   }

   /**
    * Gets the PinPadPaymentTransaction object.
    *
    * @param paymentTypeCode the payment type code.
    * @param transactionAmount the transaction amount.
    * @param authCode the auth code.
    * @param transactionCharge the transaction charge.
    * @param index the index.
    * @param requestParameterMap the user entered details.
    * @param bookingComponent the BookingComponent.
    *
    * @return the PinPadPadPaymentTransaction object.
    */
   private static PINPADPaymentTransaction getPinPadTransaction(String paymentTypeCode,
      Money transactionAmount, String authCode, Money transactionCharge, int index,
      Map<String, String> requestParameterMap, BookingComponent bookingComponent)
   {
      String merchantCopy =
         requestParameterMap.get(PaymentConstants.PAYMENT + PaymentConstants.PINPAD_MERCHANT_COPY
            + index);
      String customerCopy =
         requestParameterMap.get(PaymentConstants.PAYMENT + PaymentConstants.PINPAD_CUSTOMER_COPY
            + index);
      bookingComponent.getNonPaymentData().put(PaymentConstants.PIN_PAD, String.valueOf(true));
      char[] cardNumber =
         (requestParameterMap.get(PaymentConstants.PAYMENT + index + PaymentConstants.CARD_NUMBER))
            .toCharArray();
      return new PINPADPaymentTransaction(paymentTypeCode, transactionAmount, authCode,
         merchantCopy, customerCopy, transactionCharge, new CpCard(cardNumber));
   }

   /**
    * Gets the RefundHistoricTransaction object.
    *
    * @param paymentTypeCode the payment type code.
    * @param transactionAmount the transaction amount.
    * @param transactionCharge the transaction charge.
    * @param prefix the prefix.
    * @param requestParameterMap the map of user entered details.
    * @param bookingComponent the BookingComponent.
    * @param trackingData the trackingData.
    *
    * @return the RefundHistoricTransaction object.
    */
   private static DataCashHistoricTransaction getDataCashHistoricTransaction(
      String paymentTypeCode, Money transactionAmount, Money transactionCharge, String prefix,
      Map<String, String> requestParameterMap, BookingComponent bookingComponent,
      TransactionTrackingData trackingData)
   {
      String dataCashReference =
         requestParameterMap.get(prefix + PaymentConstants.DATACASH_REFERENCE);
      bookingComponent.getNonPaymentData().put(BookingConstants.IS_DATACASH_SELECTED,
         String.valueOf(true));
      bookingComponent.getNonPaymentData().put(BookingConstants.SELECTED_DCR, dataCashReference);
      DataCashHistoricTransaction dataCashHistoricTransaction =
         new DataCashHistoricTransaction(paymentTypeCode, transactionAmount, dataCashReference,
            getMerchantReference(bookingComponent));
      dataCashHistoricTransaction.setPaymentMethod(PaymentMethod.REFUND_CARD);
      dataCashHistoricTransaction.setTrackingData(trackingData.toString());

      dataCashHistoricTransaction.setCaptureMethod(bookingComponent.getClientApplication()
         .getCaptureMethod());

      return dataCashHistoricTransaction;
   }

   /**
    * Gets the PostPaymentGuaranteeTransaction object.
    *
    * @param bookingInfo the bookingInfo.
    * @param paymentTypeCode the paymentTypeCode.
    * @param requestParameterMap the requestParameterMap.
    * @param prefix the prefix.
    * @return paymentTransaction the paymentTransaction.
    */
   private static PostPaymentGuaranteeTransaction getPostPaymentTransaction(
      BookingInfo bookingInfo, String paymentTypeCode, Map<String, String> requestParameterMap,
      String prefix)
   {
      Currency currency = bookingInfo.getBookingComponent().getTotalAmount().getCurrency();
      bookingInfo.setThreeDAuth(Boolean.TRUE);
      return new PostPaymentGuaranteeTransaction(createCnpCard(prefix, requestParameterMap,
         bookingInfo), paymentTypeCode, new Money(BigDecimal.ZERO, currency));
   }

   /**
    * Handles the case where payment method is invalid.
    */
   private static void handleDefaultTransaction()
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty("payment.paymentmethod.invalid",
         MESSAGES_PROPERTY));
      throw new IllegalArgumentException("payment.paymentmethod.invalid");
   }

   /**
    * Gets a unique merchant reference using <code>UUID</code>.
    *
    * @param bookingComponent the bookingComponent.
    *
    * @return merchantReference the generated unique merchant reference.
    */
	private static String getMerchantReference(BookingComponent bookingComponent) {
		String clientApp = bookingComponent.getClientApplication()
				.getClientApplicationName();
		if (clientApp.equalsIgnoreCase("NEWSKIES")
				|| clientApp.equalsIgnoreCase("NEWSKIESM")) {
			return clientApp
					+ '_'
					+ bookingComponent.getNonPaymentData().get(
							"booking_Reference_Number")
					+ '_'
					+ StringUtils.left(
							String.valueOf(UUID.randomUUID()).replaceAll(
									HYPHEN, ""),
							MAX_REFERENCE_LENGTH_ACSS
									- (clientApp.length() + bookingComponent
											.getNonPaymentData()
											.get("booking_Reference_Number")
											.length()));
		} else if(clientApp.equalsIgnoreCase("TUITHSHOREX") && ConfReader.getBooleanEntry("merchantid.enabled", false) ){
			
			if(!Objects.isNull(bookingComponent.getCruiseSummary()) && !Objects.isNull(bookingComponent.getCruiseSummary().getExcursionSummary())){
				
				String merchantId=bookingComponent.getCruiseSummary().getExcursionSummary().getMerchantID();
				if(StringUtils.isEmpty(merchantId)){
					merchantId=clientApp;
				}
				return merchantId
						+ StringUtils.left(String.valueOf(UUID.randomUUID())
								.replaceAll(HYPHEN, ""), MAX_REFERENCE_LENGTH
								- clientApp.length());
				
				
				
			}else{
				return clientApp
				+ StringUtils.left(String.valueOf(UUID.randomUUID())
						.replaceAll(HYPHEN, ""), MAX_REFERENCE_LENGTH
						- clientApp.length());
			}
			
			
		}
		else{
			return clientApp
					+ StringUtils.left(String.valueOf(UUID.randomUUID())
							.replaceAll(HYPHEN, ""), MAX_REFERENCE_LENGTH
							- clientApp.length());
		}

	}

   /**
    * Gets a unique merchant reference using client application code,dataCashReference number and
    * <code>UUID</code>.
    *
    * @param clientApplication the clientApplication name.
    * @param dataCashReference the dataCashReference number.
    *
    * @return merchantReference the generated unique merchant reference.
    */
   private static String getMerchantReferenceForHistoricTransaction(
      ClientApplication clientApplication, String dataCashReference)
   {
      StringBuilder merchantRefNumber = new StringBuilder();
      merchantRefNumber.append(clientApplication).append(HYPHEN).append(dataCashReference)
         .append(HYPHEN)
         .append(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, StringUtils.EMPTY));
      return (StringUtils.left(merchantRefNumber.toString(), MAX_REFERENCE_LENGTH));
   }

   /**
    * Method to create billing address.
    *
    * @param houseno the houseno.
    * @param town the town.
    * @param county the county.
    * @param country the country.
    * @param countryCode the countryCode.
    *
    * @return billingAddress the billingAddress.
    *
    */
   // CHECKSTYLE:OFF
   private static BillingAddress createBillingAddress(String houseno, String combinedStreetAddress,
      String town, String county, String country, String countryCode)
   {
      BillingAddress billingAddress = new BillingAddress(houseno, combinedStreetAddress);
      billingAddress.setStreetAddress3(town);
      billingAddress.setStreetAddress4(county);
      billingAddress.setCountry(country);
      billingAddress.setCountryCode(countryCode);
      return billingAddress;
   }

   /**
    * Method to create combined street address.
    *
    * @param streetAddress the street address line1.
    * @param additionalAddressLine1 the additionalAddressLine1
    * @param additionalAddressLine2 the additionalAddressLine2
    * @return
    */
   private static String createCombinedStreetAddress(String streetAddress,
      String additionalAddressLine1, String additionalAddressLine2)
   {
      String combinedAddress = null;
      if (StringUtils.isNotBlank(streetAddress))
      {
         StringBuilder address = new StringBuilder(streetAddress);
         if (StringUtils.isNotBlank(additionalAddressLine1))
         {
            address.append(COMMA).append(additionalAddressLine1);
         }
         if (StringUtils.isNotBlank(additionalAddressLine2))
         {
            address.append(COMMA).append(additionalAddressLine2);
         }
         combinedAddress = address.toString();
      }
      return combinedAddress;
   }

   /**
    * Method to put card holder billing details in payment data map.
    *
    * @param prefix the prefix.
    * @param requestParameterMap the requestParameterMap.
    * @param bookingInfo the bookingInfo.
    * @param houseno the houseno.
    * @param streetAddress the streetAddress.
    * @param town the town.
    * @param county the county.
    * @param countryCode the countryCode.
    * @param postCode the post code.
    *
    */
   private static void setPaymentDataMap(String prefix, Map<String, String> requestParameterMap,
      BookingInfo bookingInfo, String postCode, String houseno, String streetAddress, String town,
      String county, String country,String countryCode, String additionalAddressLine1,
      String additionalAddressLine2)
   {
      String cardHolderAddress2 =
         requestParameterMap.get(prefix + PaymentConstants.CARDHOLDER_ADDRESS2);
      String cardHoldersdayTimePhone =
         requestParameterMap.get(prefix + PaymentConstants.CARDHOLDER_DAYTIMEPHONE);
      String cardHoldersmobilePhone =
         requestParameterMap.get(prefix + PaymentConstants.CARDHOLDER_MOBILEPHONE);
      String cardHoldersemailAddress =
         requestParameterMap.get(prefix + PaymentConstants.CARDHOLDER_EMAILADDRESS);
      String cardHoldersemailAddress1 =
         requestParameterMap.get(prefix + PaymentConstants.CARDHOLDER_EMAILADDRESS1);

      Map<String, String> paymentData = null;
      paymentData = bookingInfo.getPaymentDataMap();

      paymentData.put(prefix + PaymentConstants.HOUSENO, houseno);
      paymentData.put(prefix + PaymentConstants.STREETADDRESS, streetAddress);
      paymentData.put(prefix + PaymentConstants.TOWN, town);
      paymentData.put(prefix + PaymentConstants.COUNTY, county);
      paymentData.put(prefix + PaymentConstants.POST_CODE, postCode);
      paymentData.put(prefix + PaymentConstants.CARDHOLDER_ADDRESS2, cardHolderAddress2);
      paymentData.put(prefix + PaymentConstants.CARDHOLDER_DAYTIMEPHONE, cardHoldersdayTimePhone);
      paymentData.put(prefix + PaymentConstants.CARDHOLDER_MOBILEPHONE, cardHoldersmobilePhone);
      paymentData.put(prefix + PaymentConstants.CARDHOLDER_EMAILADDRESS, cardHoldersemailAddress);
      paymentData.put(prefix + PaymentConstants.CARDHOLDER_EMAILADDRESS1, cardHoldersemailAddress1);
      paymentData.put(prefix + PaymentConstants.SELECTED_COUNTRY_CODE, countryCode);
      paymentData.put(prefix + PaymentConstants.SELECTED_COUNTRY, country);
      paymentData.put(prefix + PaymentConstants.ADDITIONAL_ADDRESS_LINE1, additionalAddressLine1);
      paymentData.put(prefix + PaymentConstants.ADDITIONAL_ADDRESS_LINE2, additionalAddressLine2);
      paymentData.put(PaymentConstants.DDDEPOSIT,requestParameterMap.get("depositType"));
   }

   // CHECKSTYLE:ON

   /**
    * Method that returns the fatal error codes list if the conf entry for the particular brand
    * exists , else returns global fatal error codes list.
    *
    * @param clientApplication is the name of the client application.
    * @return returns the fatalErrorCodes list
    *
    */
   private static List<String> populateFatalErrors(String clientApplication)
   {
      LogWriter.logInfoMessage("Fatal Error Codes assigned for " + clientApplication);
      return getErrorList(clientApplication, PaymentConstants.FATAL_ERROR);
   }

   /**
    * Method that returns the non fatal error codes list if the conf entry for the particular brand
    * exists , else returns global non fatal error codes list.
    *
    * @param clientApplication is the name of the client application.
    * @return returns the nonFatalErrorCodes list.
    *
    */
   private static List<String> populateNonFatalErrors(String clientApplication)
   {
      LogWriter.logInfoMessage("Non Fatal Error Codes assigned for " + clientApplication);
      return getErrorList(clientApplication, PaymentConstants.NON_FATAL_ERROR);

   }

   /**
    * Method that returns the 3d non fatal error codes list for the 3d enabled cards.
    *
    * @param clientApplication is the name of the client application.
    * @return returns the 3dNonFatalErrorCodes.
    */
   private static List<String> populate3dNonFatalErrors(final String clientApplication)
   {
      LogWriter.logInfoMessage("3d Non Fatal Error Codes assigned for " + clientApplication);
      return getErrorList(clientApplication, PaymentConstants.THREED_NON_FATAL_ERROR);
   }
   
   /**
    * Method that returns the 3d non fatal error codes list for the 3d enabled cards.
    *
    * @param clientApplication is the name of the client application.
    * @return returns the 3dNonFatalErrorCodes.
    */
   private static List<String> populateHcc3dNonFatalErrors(final String clientApplication)
   {
      LogWriter.logInfoMessage("3d Non Fatal Error Codes assigned for " + clientApplication);
      return getHccErrorList(clientApplication, PaymentConstants.HCC_NON_FATAL_ERROR);
   }

   /**
    * Method that returns the 3d non fatal error codes list for the 3d enabled cards.
    *
    * @param clientApplication is the name of the client application.
    * @return returns the 3dNonFatalErrorCodes.
    */
   private static List<String> populate3dDuplicateRespError()
   {
      LogWriter.logInfoMessage("3d Non Fatal Duplicate Response Error Codes assigned FOR CPS");
      return Arrays.asList(ConfReader.getStringValues(THREED_NON_FATAL_DUP_RESP_ERROR, null, ","));
   }

   /**
    * Method that returns list of fatal or non fatal error codes list.
    *
    * @param clientApplication is clientApplication.
    * @param errorKey is the fatal or non fatal key.
    * @return returns the list of fatal or non fatal error.
    */
   private static List<String> getErrorList(String clientApplication, String errorKey)
   {
      // List of errors.
      String errorList = ConfReader.getConfEntry(clientApplication + DOT + errorKey, EMPTY_LISTS);
      if (StringUtils.isNotEmpty(errorList))
      {
         LogWriter.logInfoMessage(errorList);
         return Arrays.asList(ConfReader.getStringValues(clientApplication + DOT + errorKey,
            EMPTY_LIST, COMMA));
      }
      else
      {
         LogWriter.logInfoMessage("by default " + COLON
            + ConfReader.getConfEntry(errorKey, EMPTY_LISTS));
         return Arrays.asList(ConfReader.getStringValues(errorKey, EMPTY_LIST, COMMA));
      }
   }
   
   /**
    * Method that returns list of fatal or non fatal error codes list.
    *
    * @param clientApplication is clientApplication.
    * @param errorKey is the fatal or non fatal key.
    * @return returns the list of fatal or non fatal error.
    */
   private static List<String> getHccErrorList(String clientApplication, String errorKey)
   {
      // List of errors.
      String errorList = ConfReader.getConfEntry(clientApplication + DOT + errorKey, EMPTY_LISTS);
      if (StringUtils.isNotEmpty(errorList))
      {
         LogWriter.logInfoMessage(errorList);
         return Arrays.asList(ConfReader.getStringValues(clientApplication + DOT + errorKey,
            EMPTY_LIST, COMMA));
      }
      else
      {
         LogWriter.logInfoMessage("by default " + COLON
            + ConfReader.getConfEntry(errorKey, EMPTY_LISTS));
         return Arrays.asList(ConfReader.getStringValues(errorKey, EMPTY_LIST, COMMA));
      }
   }

}
