/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BackEndDataCashPaymentTransaction.java,v $
 *
 * $Revision:  $
 *
 * $Date: 2009-03-18 04:35:06 $
 *
 * $author : bibin.j $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.domain.PaymentConstants.DATACASH_REFERENCE_LENGTH;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * This class contain methods to authorize a payment /refund , fulfill payment or refund,cancel
 * payment from TRACS.Make calls to data-cash service authPayment/authRefund/doPayment/doRefund
 * based on transaction type.
 *
 * @author bibin.j
 */
public final class BackEndDataCashPaymentTransaction implements PaymentTransaction
{
   /** Pattern for data-cash account number and data-cash reference number. */
   private static final Pattern DATACASH_ACCNO_OR_REFNO_PATTERN = Pattern.compile("[0-9]*");

   /** The data-cash account number. */
   private String dataCashAccount;

   /** The DataCashHistoricTransaction object. */
   private DataCashHistoricTransaction dataCashHistoricTransaction;

   /**
    * Tracking data contains token,sessionId and inventory booking reference number required for
    * logging.
    */
   private String trackingData;

   /**
    * The constructor for BackEndDataCashPaymentTransaction.
    *
    * @param dataCashAccount the dataCashAccount.
    * @param transactionId the transaction id.
    * @param authCode the authCode.
    * @param merchantReference the merchantReference
    *
    */
   public BackEndDataCashPaymentTransaction(String dataCashAccount, String transactionId,
                                            String authCode, String merchantReference)
   {
      this.dataCashAccount = dataCashAccount;
      this.dataCashHistoricTransaction =
         new DataCashHistoricTransaction(transactionId, authCode, merchantReference);
   }

   /**
    * This method is responsible for fulfilling a payment or refund.
    *
    * @return String that contains the following fields: 1. If the payment is successful, the status
    *         code will be "000" and the message will be a 32 digit Data Cash transaction id. 2. A
    *         vertical bar (|) to separate the status code and message. 3. If the payment is not
    *         successful, the status code will be non-zero (but zero padded to 3 digits), and the
    *         message will contain a textual explanation of the reason why the payment was not
    *         successful. 4. A vertical bar (|) to separate the message and the auth code. This
    *         vertical bar will be present even it there is no auth code. 5. An auth code that
    *         consists of up to 6 digits.
    */
   public String backEndFulfilPayment()
   {
      StringBuilder response = new StringBuilder();
      try
      {
         LogWriter.logInfoMessage("Validating input fields before fulfilling the payment");
         validateInputFields();
         LogWriter.logInfoMessage("Sending request to datacash for fulfilling the payment");
         this.dataCashHistoricTransaction.setTrackingData(trackingData);
         this.dataCashHistoricTransaction.setPaymentMethod(PaymentMethod.DATACASH);
         this.dataCashHistoricTransaction.historicFulfill(dataCashAccount);
         String datacashReference = this.dataCashHistoricTransaction.getDatacashReference();
         response.append(DataCashServiceConstants.DC_SUCCESS_CODE).append('|').append(
            datacashReference).append('|');
         if (null != this.getAuthCode())
         {
            response.append(this.getAuthCode());
         }
         LogWriter.logInfoMessage("Fulfill payment successful with dataCashReference id "
            + datacashReference);
      }
      catch (DataCashServiceException dcse)
      {
         String message = dcse.getMessage();
         populateResponse(response, dcse.getCode(), message);
         LogWriter.logErrorMessage(message, dcse);
      }
      catch (PaymentValidationException pve)
      {
         String message = pve.getMessage();
         populateResponse(response, pve.getCode(), message);
         LogWriter.logErrorMessage(message, pve);
      }
      return String.valueOf(response);
   }

   /**
    * This method is responsible for canceling an authorized payment/refund without fulfilling it.
    *
    *
    * @return String that contains the following fields: 1. If the payment is successful, the status
    *         code will be "000" and the message will be a 32 digit Data Cash transaction id. 2. A
    *         vertical bar (|) to separate the status code and message. 3. If the payment is not
    *         successful, the status code will be non-zero (but zero padded to 3 digits), and the
    *         message will contain a textual explanation of the reason why the payment was not
    *         successful. 4. A vertical bar (|) to separate the message and the auth code. This
    *         vertical bar will be present even it there is no auth code. 5. An auth code that
    *         consists of up to 6 digits.
    */
   public String backEndCancel()
   {
      StringBuilder response = new StringBuilder();
      try
      {
         LogWriter.logInfoMessage("Validating input fields before cancelling the payment");
         validateInputFields();
         LogWriter.logInfoMessage("Sending request to datacash for cancelling payment");
         this.dataCashHistoricTransaction.setTrackingData(trackingData);
         this.dataCashHistoricTransaction.setPaymentMethod(PaymentMethod.DATACASH);
         this.dataCashHistoricTransaction.historicCancel(dataCashAccount);
         String datacashReference = this.dataCashHistoricTransaction.getDatacashReference();
         response.append(DataCashServiceConstants.DC_SUCCESS_CODE).append('|').append(
            datacashReference).append('|');
         if (null != this.getAuthCode())
         {
            response.append(this.getAuthCode());
         }
         LogWriter.logInfoMessage("Cancelling payment successful with dataCashReference id "
            + datacashReference);
      }
      catch (DataCashServiceException dcse)
      {
         String message = dcse.getMessage();
         populateResponse(response, dcse.getCode(), message);
         LogWriter.logErrorMessage(message, dcse);
      }
      catch (PaymentValidationException pve)
      {
         String message = pve.getMessage();
         populateResponse(response, pve.getCode(), message);
         LogWriter.logErrorMessage(message, pve);
      }
      return String.valueOf(response);
   }

   /**
    * This method is responsible for card validation using pattern matching, bin validation, other
    * parameters sent.
    *
    * @throws PaymentValidationException the PaymentValidationException.
    */
   public void validate() throws PaymentValidationException
   {
      this.dataCashHistoricTransaction.validate();
      validateInputFields();
   }

   /**
    * This method is responsible for input field validation like data-cash reference number and
    * data-cash account number.
    *
    * @throws PaymentValidationException the PaymentValidationException.
    */
   private void validateInputFields() throws PaymentValidationException
   {
      if (StringUtils.isNotBlank(dataCashAccount) && !validateDataCashAccountNo())
      {
         String errorMessage =
            PropertyResource.getProperty("payment.datacashAccountNo.invalid", MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PaymentValidationException(PaymentConstants.ERRORCODE_INVALIDDATACASHACCNO,
            errorMessage);
      }
      if (StringUtils.isNotBlank(this.dataCashHistoricTransaction.getDatacashReference())
         && !validateDataCashReferenceNo())
      {
         String errorMessage =
            PropertyResource.getProperty("payment.datacashReferenceNo.invalid", MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PaymentValidationException(PaymentConstants.ERRORCODE_INVALIDDATACASHREFNO,
            errorMessage);
      }

   }

   /**
    * Clears sensitive data present in the transaction.
    */
   public void clearSensitiveData()
   {
      // Do nothing, empty implementation.
   }

   /**
    * Gets essential transaction data associated with data-cash transaction.
    *
    * @return essentialTransactionData the essential transaction data.
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      return null;
   }

   /**
    * Gets the payment type.
    *
    * @return the payment type
    */
   public String getPaymentTypeCode()
   {
      return this.dataCashHistoricTransaction.getPaymentTypeCode();
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return this.dataCashHistoricTransaction.getTransactionAmount();
   }

   /**
    * Gets the transaction charge.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return this.dataCashHistoricTransaction.getTransactionCharge();
   }

   /**
    * Gets the authorization code.
    *
    * @return the authorization code
    */
   public String getAuthCode()
   {
      return this.dataCashHistoricTransaction.getAuthCode();
   }

   /**
    * Gets the data-cash reference.
    *
    * @return the data-cash reference
    */
   public String getDatacashReference()
   {
      return this.dataCashHistoricTransaction.getDatacashReference();
   }

   /**
    * Gets the PaymentMethod associated with this Transaction.
    *
    * @return the PaymentMethod
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.DATACASH;
   }

   /**
    * This method sets the trackingData. Tracking data contains token,sessionId and inventory
    * booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
      this.trackingData = trackingdata;
   }

   /**
    * This method populates the response to be sent to TRACS.
    *
    * @param response the response.
    * @param code the error code.
    * @param message the error message.
    */
   private void populateResponse(StringBuilder response, int code, String message)
   {
      response.append(
         StringUtils.leftPad(String.valueOf(code),
            DataCashServiceConstants.DC_RESPONSE_CODE_LENGTH, '0')).append('|').append(message)
         .append('|');
      if (StringUtils.isNotBlank(this.getAuthCode()))
      {
         response.append(this.getAuthCode());
      }
   }

   /**
    * Method to validate data-cash account number.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateDataCashAccountNo()
   {
      Matcher datacashAccountNoMatcher =
         DATACASH_ACCNO_OR_REFNO_PATTERN.matcher(this.dataCashAccount);
      return datacashAccountNoMatcher.matches();
   }

   /**
    * Method to validate data-cash reference number.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateDataCashReferenceNo()
   {
      String dataCashReference = this.dataCashHistoricTransaction.getDatacashReference();
      if (dataCashReference.length() == DATACASH_REFERENCE_LENGTH
         && DATACASH_ACCNO_OR_REFNO_PATTERN.matcher(dataCashReference).matches())
      {
         return true;
      }
      return false;
   }
}
