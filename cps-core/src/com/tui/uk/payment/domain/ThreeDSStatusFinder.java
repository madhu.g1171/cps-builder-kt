package com.tui.uk.payment.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tui.uk.config.ConfReader;

/**
 * This class is responsible for determine the 3DS authorisation response by reading the ECI
 * indicator from Query Transaction Response.
 *
 * @author ganapna
 *
 */
public class ThreeDSStatusFinder
{

   /** the CPS securelogo applicablecardgroup constant. */
   private static final String CPS_SECURELOGO_APPLICABLECARDGROUP =
      "securelogo.applicablecardgroup";

   /** the CPS secure logo constant. */
   private static final String CPS_SECURELOGO = "securelogo.";

   /** the CPS eci constant. */
   private static final String CPS_ECI = "eci.";

   /** the CPS eci constant. */
   private static final String NOT_AUTHORIZED = "N";

   /**
    * This method gets the card group schemes from the configuration file.
    *
    * @return list of card schemes.
    */
   public static List<String> getGroupedCardScheme()
   {
      String[] groupedCardScheme =
         ConfReader.getStringValues(CPS_SECURELOGO_APPLICABLECARDGROUP, null, ",");
      // MasterCardGroup,VisaGroup
      List<String> applicableCardGroupList = new ArrayList<String>();
      for (int i = 0; i < groupedCardScheme.length; i++)
      {
         applicableCardGroupList.add(groupedCardScheme[i]);
      }
      return applicableCardGroupList;
   }

   /**
    * This method creates a map of card groups with its schemes are keys.
    *
    * @return map of card group schemes.
    */
   private Map<String, List<String>> getCardGroupCardSchemeMap()
   {
      Map<String, List<String>> cardGroupCardSchemeMap = new HashMap<String, List<String>>();
      List<String> applicableCardGroupList = getGroupedCardScheme();
      for (int i = 0; i < applicableCardGroupList.size(); i++)
      {
         // MasterCardGroup,VisaGroup
         // MasterCard,Switch,Maestro,Debit
         String[] cardSchemes =
            ConfReader.getStringValues(CPS_SECURELOGO
               + (applicableCardGroupList.get(i)).trim().toLowerCase(), null, ",");

         List<String> cardSchemeList = new ArrayList<String>();
         for (int j = 0; j < cardSchemes.length; j++)
         {
            cardSchemeList.add(cardSchemes[j]);
         }
         cardGroupCardSchemeMap.put(applicableCardGroupList.get(i), cardSchemeList);
      }
      return cardGroupCardSchemeMap;
   }

   /**
    * This method returns the cardgroup based on the card scheme.
    *
    * @param cardScheme The Card Scheme in the transaction.
    * @return the Card Group.
    */
   public String getCardSchemeGroup(String cardScheme)
   {
      String cardGroup = "NOT_FOUND";
      Map<String, List<String>> cardGroupCardSchemeMap = getCardGroupCardSchemeMap();
      Set<Map.Entry<String, List<String>>> set = cardGroupCardSchemeMap.entrySet();
      for (Map.Entry<String, List<String>> e : set)
      {
         if (e.getValue().contains(cardScheme))
         {
            cardGroup = e.getKey();
            break;
         }
      }
      return cardGroup;
   }

   /**
    * This method creates a map with card group as Key and 3DS status as value.
    *
    * @return Map with card group as Key and 3DS status as value.
    */
   private Map<String, String> createECIMatrix()
   {
      Map<String, String> map = new HashMap<String, String>();
      List<String> applicableCardGroupList = getGroupedCardScheme();
      String eci = null;
      for (int i = 0; i < applicableCardGroupList.size(); i++)
      {
         eci =
            ConfReader.getConfEntry(CPS_ECI + (applicableCardGroupList.get(i)).trim().toLowerCase()
               + ".Authorized", NOT_AUTHORIZED);
         map.put(applicableCardGroupList.get(i) + eci, "Y");
         eci =
            ConfReader.getConfEntry(CPS_ECI + (applicableCardGroupList.get(i)).trim().toLowerCase()
               + ".Attempted", NOT_AUTHORIZED);
         map.put(applicableCardGroupList.get(i) + eci, "A");
         eci =
            ConfReader.getConfEntry(CPS_ECI + (applicableCardGroupList.get(i)).trim().toLowerCase()
               + ".NotAuthorized", NOT_AUTHORIZED);
         map.put(applicableCardGroupList.get(i) + eci, NOT_AUTHORIZED);
      }
      return map;
   }

   /**
    * Simple Map look method.
    *
    * @param key The Key to find the Map.
    * @return returns the vallue for the Key.
    */
   private String lookup(String key)
   {
      Map<String, String> map = createECIMatrix();
      return map.get(key);
   }

   /**
    * This method will find the 3DS authorisation status based on eci value returned for the card
    * scheme.
    *
    * @param eci the eci code.
    * @param cardScheme either master card, visa, etc.,
    * @return they value based on key.
    */
   public String findThreeDSStatus(String eci, String cardScheme)
   {

      String cardSchemeGroup = getCardSchemeGroup(cardScheme);
      String key = cardSchemeGroup.trim() + eci.trim();
      String status = lookup(key);
      if (status == null)
      {
         status = NOT_AUTHORIZED;
      }
      return status;
   }
}
