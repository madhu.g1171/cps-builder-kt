/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashPaymentTransaction.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S $
 *
 * $Log : $
 */
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.domain.PaymentConstants.COMMA;
import static com.tui.uk.payment.domain.PaymentConstants.DATE_FORMAT;
import static com.tui.uk.payment.domain.PaymentConstants.ERROR_MESSAGE_KEY;
import static com.tui.uk.payment.domain.PaymentConstants.FATAL_ERROR;
import static com.tui.uk.payment.domain.PaymentConstants.HYPHEN;
import static com.tui.uk.payment.domain.PaymentConstants.NON_FATAL_ERROR;
import static com.tui.uk.payment.domain.PaymentConstants.RESPONSE_CODE_LENGTH;
import static com.tui.uk.payment.domain.PaymentConstants.THREED_NON_FATAL_ERROR;
import static com.tui.uk.payment.domain.PaymentConstants.HCC_NON_FATAL_ERROR;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.AUTH_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CANCEL_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.ERP_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.FULFILL_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PRE_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REFUND_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REVERSE_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.SUCCESS_STATUS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREED_SUCCESS_STATUS;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;

import com.datacash.errors.FailureReport;
import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.cacheeventlistener.TransactionStatusUtil;
import com.tui.uk.payment.service.datacash.AunthicatePayeeResXml;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.EMVAuthorizationResponse;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.CardResponse;
import com.tui.uk.payment.service.datacash.CardVerificationResponse;
import com.tui.uk.payment.service.datacash.DataCashCard;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.payment.service.datacash.DataCashServiceFactory;
import com.tui.uk.payment.service.datacash.QueryTransactionResponse;
import com.tui.uk.payment.service.datacash.Response;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * This class represents DataCashTransaction and depending on the TransactionAmount it takes either
 * payment or refund. if the transaction amount is negative it indicates RefundDataCashTransaction
 * else it is PaymentDataCashTransaction. This class implements all the necessary behaviors required
 * for datacash transactions.
 */

/**
 * @author Dinesh.Biswal2
 *
 */
public final class DataCashPaymentTransaction implements PaymentTransaction
{
   /** The payment type. */
   private String paymentTypeCode;

   /** The transaction amount. */
   private Money transactionAmount;

   /** The transaction charge. */
   private Money transactionCharge;

   /** The Card object, which contains card details. */
   private CnpCard card;

   /** The auth code sent by the data-cash. */
   private String authCode;

   /** The data-cash reference id, sent by the data-cash. */
   private String datacashReference;

   /** The string holding the Card Issuing Bank URL. */
   private String acsUrl;

   /** The String holding the payer authentication request. */
   private String paReq;

   /** The unique value for tracing transactions. */
   private String mdUrl;

   /** The 3D secure auth flag. */
   private boolean threeDSecureAuthFlag;

   /** The merchant reference id. */
   private String merchantReference;

   public void setMerchantReference(String merchantReference) {
	this.merchantReference = merchantReference;
}

/** The datacash return code. */
   private int returnCode;

   /** The datacash response message. */
   private String responseMsg;

   /** The card scheme. */
   private String cardScheme;

   public void setCardScheme(String cardScheme) {
	this.cardScheme = cardScheme;
}

/** The card issuing country. */
   private String country;

   public void setCountry(String country) {
	this.country = country;
}

/** The issuer. */
   private String issuer;

   public void setIssuer(String issuer) {
	this.issuer = issuer;
}

/** The avs response. */
   private String avsResponse;

   public void setAvsResponse(String avsResponse) {
	this.avsResponse = avsResponse;
}

/** The cv2 response. */
   private String cvvResponse;

   public void setCvvResponse(String cvvResponse) {
	this.cvvResponse = cvvResponse;
}

/** The referral code. */
   private String referralCode;
   
   private String emvStausCode;

   /** The referral code. */
   private String threeDSStatus = "N";
   /** The index from which the latest uuid should overwrite the old uuid.
    * This is used when there is a non fatat error while 3DS verification
    * and a normal auth needs to be done.*/
   private static final int REPLACEMENT_INDEX = 10;

   /**Switch to enabling Double Post Handling to counter 168,051 3D Secure Response codes.*/
   private static final String THREED_DOUBLE_POST_HANDLE_SWITCH ="3DSecure.ACS.doublePost.handler.switch";

	/**
	 * Configurable Time period for which duplicate thread must sleep for original
	 * thread to complete the fraud screening execution.
	 *  Note:Possible problems may
	 * occur if the time period is increased.Do not change the configuration.
	 */
	private static final String WAIT_TIME = "duplicate.thread.sleeptime";

   /**
    * Tracking data contains token,sessionId and inventory booking reference number required for
    * logging.
    */
   private String trackingData;

   /** The captureMethod value. */
   private String captureMethod;

  /** List that maintains fatalError.*/
   private List<String> fatalErrorList;

  /** List that maintains nonfatalError.*/
   private List<String> nonFatalErrorList;

   /** List that maintains 3dnonfatalErrors.*/
   private List<String> threeDNonFatalErrorList;

   /** List that maintains HCC 3dnonfatalErrors.*/
   private List<String> hccThreeDNonFatalErrorList;

/** List that maintains threeDDuplicateTrxErrorList.*/
   private List<String> threeDDuplicateRespErrorList;

   private String status;
   private String reason;
   private String time;

   public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getReason() {
	return reason;
}

public void setReason(String reason) {
	this.reason = reason;
}

public String getTime() {
	return time;
}

public void setTime(String time) {
	this.time = time;
}



/**
    * The constructor.
    *
    * @param datacashReference the datacashReference
    * @param authCode the authCode
    * @param merchantReference the merchantReference
    */
   public DataCashPaymentTransaction(String datacashReference, String authCode,
                                     String merchantReference)
   {
      this.datacashReference = datacashReference;
      this.authCode = authCode;
      this.merchantReference = merchantReference;
   }

   /**
    * The overloaded constructor.
    *
    * @param transactionCharge the transaction charge
    * @param card the card
    * @param paymentTypeCode the payment type
    * @param transactionAmount the transaction amount
    * @param merchantReference the merchantReference
    */
   public DataCashPaymentTransaction(String paymentTypeCode, Money transactionAmount,
             Money transactionCharge, CnpCard card, String merchantReference)
   {
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
      this.transactionCharge = transactionCharge;
      this.card = card;
      this.merchantReference = merchantReference;
   }


	public DataCashPaymentTransaction(String paymentTypeCode,
			Money transactionAmount, Money transactionCharge,
			String authCode, String datacashReference, String merchantReference,
			String returnCode, String cardScheme,
			String country, String issuer,String captureMethod) {
		this.paymentTypeCode=paymentTypeCode;
		this.transactionAmount=transactionAmount;
		this.transactionCharge=transactionCharge;
		this.authCode=authCode;
		this.datacashReference=datacashReference;
		this.merchantReference=merchantReference;
		this.country=country;
		this.returnCode=Integer.parseInt(returnCode);
		this.cardScheme=cardScheme;
		this.issuer=issuer;
		this.captureMethod=captureMethod;

	}

   /**
    * The overloaded constructor for HCCS.
    *
    * @param transactionCharge the transaction charge
    * @param transactionAmount the transaction amount
    * @param merchantReference the merchantReference
    */
   public DataCashPaymentTransaction(String paymentTypeCode, Money transactionAmount,
             Money transactionCharge, String merchantReference)
   {
	   this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
      this.transactionCharge = transactionCharge;
      this.merchantReference = merchantReference;
   }


	public DataCashPaymentTransaction(String datacashReference,
			String merchantReference, String status, String reason,String time) {

		this.datacashReference=datacashReference;
		this.merchantReference=merchantReference;
		this.status=status;
		this.reason=reason;
		this.time=time;
	}

   /**
    * Gets the payment type.
    *
    * @return the payment type
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the transaction charge.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return transactionCharge;
   }

   /**
    * Gets the authorization code.
    *
    * @return the authorization code
    */
   public String getAuthCode()
   {
      return authCode;
   }

   /**
    * Gets the response message.
    *
    * @return the responseMsg
    */
   public String getResponseMsg()
   {
      return responseMsg;
   }

   /**
    * Gets the CNP card object.
    *
    * @return the card
    */
   public CnpCard getCard()
   {
      return card;
   }

   /**
    * Sets the authorization code.
    *
    * @param authCode the authorization code
    */
   public void setAuthCode(String authCode)
   {
      this.authCode = authCode;
      this.referralCode = authCode;
   }

   /**
    * Method to set fatal error codes.
    *
    * @param fatalErrorsList is the fatal error list.
    */
   public void setFatalErrorCodes(List<String> fatalErrorsList)
   {
   this.fatalErrorList = fatalErrorsList;
   }

   /**
   * Method to set non fatal error codes .
   *
   * @param nonFatalErrorsList is the non fatal error list.
   */
  public void setNonFatalErrorCodes(List<String> nonFatalErrorsList)
  {
  this.nonFatalErrorList = nonFatalErrorsList;
  }

  /**
   * Method to set 3d non fatal error codes.
   *
   * @param threeDNonFatalErrorsList is the 3d non fatal error list.
   */
  public void setThreeDNonFatalErrorCodes(final List<String> threeDNonFatalErrorsList)
  {
   this.threeDNonFatalErrorList = threeDNonFatalErrorsList;
  }

  /** Gets the ThreeDNonFatalErrorCodes.
  *
  * @return the list of ThreeDNonFatalErrorCodes
  */
  public List<String> getThreeDNonFatalErrorCodes()
  {
   return threeDNonFatalErrorList;
  }

  /** Gets the ThreeDNonFatalErrorCodes for HCC.
  *
  * @return the list of ThreeDNonFatalErrorCodes
  */
 public List<String> getHccThreeDNonFatalErrorList() {
		return hccThreeDNonFatalErrorList;
  }

 /**
  * Method to set 3d non fatal error codes for HCC.
  *
  * @param threeDNonFatalErrorsList is the 3d non fatal error list.
  */
 public void setHccThreeDNonFatalErrorList(List<String> hccThreeDNonFatalErrorList) {
		this.hccThreeDNonFatalErrorList = hccThreeDNonFatalErrorList;
	}

   /**
	 * Method to set ThreeDDuplicateTrxError codes.
	 *
	 * @param ThreeDDuplicateTrxError is the ThreeDDuplicateTrxError error list.
	 */
	public void setThreeDDuplicateRespErrorList(List<String> threeDDuplicateRespErrorList)
	{
		this.threeDDuplicateRespErrorList = threeDDuplicateRespErrorList;
	}

	/** Gets the ThreeDDuplicateTrxError Codes.
	  *
	  * @return the list of ThreeDDuplicateTrxError Codes
	  */
	public List<String> getThreeDDuplicateRespErrorList()
	{
		return threeDDuplicateRespErrorList;
	}

   /**
    * Gets the datacash reference.
    *
    * @return the datacash reference
    */
   public String getDatacashReference()
   {
      return datacashReference;
   }

   /**
    * Gets the merchant reference.
    *
    * @return the merchant reference
    */
   public String getMerchantReference()
   {
      return merchantReference;
   }

   /**
    * Sets the datacash reference.
    *
    * @param datacashReference the datacash reference
    */
   public void setDatacashReference(String datacashReference)
   {
      this.datacashReference = datacashReference;
   }

   /**
    * Gets the PaymentMethod associated with this Transaction.
    *
    * @return the PaymentMethod
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.DATACASH;
   }

   /**
    * Get the Card Issuing Bank URL.
    *
    * @return the acsUrl
    */
   public String getAcsUrl()
   {
      return acsUrl;
   }

   /**
    * Get the payer authentication request.
    *
    * @return the paReq
    */
   public String getPaReq()
   {
      return paReq;
   }

   /**
    * Get the unique value for tracing transactions.
    *
    * @return the mdUrl
    */
   public String getMD()
   {
      return mdUrl;
   }

   /**
    * This method sets the trackingData. Tracking data contains token,sessionId and inventory
    * booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
      this.trackingData = trackingdata;
   }

   /**
    * This method is responsible for card validation using pattern matching and bin validation.
    *
    * @throws PaymentValidationException the PaymentValidationException
    */
   public void validate() throws PaymentValidationException
   {
      card.validate();
      getDataCashCard().validate(trackingData);
   }

   public String getEmvStausCode()
   {
      return emvStausCode;
   }
   
   public void setEmvStausCode(String emvStausCode)
   {
      this.emvStausCode = emvStausCode;
   }
   /**
    * This method is responsible for performing the "pre" transaction type. The amount must be
    * positive in order to do the pre payment authorization.
    *
    * @param client the client account.
    * @param clientAuthCode the authorization code of the client.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void authPayment(String client, String clientAuthCode) throws DataCashServiceException
   {
      DataCashCard dataCashCard = getDataCashCard();
      if (StringUtils.isNotBlank(this.authCode))
      {
         clientAuthCode = this.authCode;
      }
      CardResponse cardResponse =
         DataCashServiceFactory.getDataCashService(client).authPayment(dataCashCard,
            getTotalAmount(), clientAuthCode, getMerchantReference());
      processTransaction(cardResponse, client, PRE_TRANSACTION);
      authCode = cardResponse.getAuthCode();
      returnCode = cardResponse.getCode();
      cardScheme = cardResponse.getCardScheme();
      country = cardResponse.getCountry();
      issuer = cardResponse.getIssuer();
      avsResponse = cardResponse.getAddressPolicyResultCode();
      cvvResponse = cardResponse.getCvvPolicyResultCode();
      responseMsg = cardResponse.getDescription();
      String token = trackingData.substring(0, 36);
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      if (returnCode == SUCCESS_STATUS)
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_SUCCESS"));
      }
      else
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_FAILURE"));
      }
      // dataCashCard.purgeCardDetails();
   }

   //TODO added by Mukesh for HCC
   public void authHccPayment(String client, String clientAuthCode, String hpsDatacashReference) throws DataCashServiceException
   {
      //DataCashCard dataCashCard = new DataCashCard();
      DataCashCard dataCashCard = getDataCashCard();
      if(StringUtils.isNotEmpty(this.captureMethod)){
		   dataCashCard.setCaptureMethod(this.captureMethod);
	   }

      if (StringUtils.isNotBlank(this.authCode))
      {
         clientAuthCode = this.authCode;
      }

      /*CardResponse cardResponse1 =
         DataCashServiceFactory.getDataCashService(client).authPayment(dataCashCard,
            getTotalAmount(), clientAuthCode, getMerchantReference());*/

		CardResponse cardResponse = DataCashServiceFactory.getDataCashService(client).authHccPayment(dataCashCard,
				getTotalAmount(), clientAuthCode, getMerchantReference(), hpsDatacashReference);

      processHccTransaction(cardResponse, client, PRE_TRANSACTION);
      authCode = cardResponse.getAuthCode();
      returnCode = cardResponse.getCode();
      cardScheme = cardResponse.getCardScheme();
      country = cardResponse.getCountry();
      issuer = cardResponse.getIssuer();
      avsResponse = cardResponse.getAddressPolicyResultCode();
      cvvResponse = cardResponse.getCvvPolicyResultCode();
      responseMsg = cardResponse.getDescription();
      String token = trackingData.substring(0, 36);
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      if (returnCode == SUCCESS_STATUS)
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_SUCCESS"));
      }
      else
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_FAILURE"));
      }
      // dataCashCard.purgeCardDetails();
   }

   /**
    * This method is responsible for performing the "auth" transaction type.The transaction amount
    * must be positive in order to do the payment.
    *
    * @param client the client account.
    * @param clientAuthCode the authorization code of the client.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void doPayment(String client, String clientAuthCode) throws DataCashServiceException
   {
      DataCashCard dataCashCard = getDataCashCard();
      CardResponse cardResponse =
         DataCashServiceFactory.getDataCashService(client).doPayment(dataCashCard,
            getTotalAmount(), authCode, merchantReference);
      processTransaction(cardResponse, client, AUTH_TRANSACTION);
      authCode = cardResponse.getAuthCode();
      // dataCashCard.purgeCardDetails();
   }

   /**
    * This method is responsible for performing the "erp" transaction type.The transaction amount
    * should be negative in order to do the refund authorisation.
    *
    * @param client the client account.
    * @param clientAuthCode the authorization code of the client.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void authRefund(String client, String clientAuthCode) throws DataCashServiceException
   {
      DataCashCard dataCashCard = getDataCashCard();
      CardResponse cardResponse =
         DataCashServiceFactory.getDataCashService(client).authRefund(dataCashCard,
            getTotalAmount(), clientAuthCode, merchantReference);
      processTransaction(cardResponse, client, ERP_TRANSACTION);
      authCode = cardResponse.getAuthCode();
      // dataCashCard.purgeCardDetails();
   }

   /**
    * This method is responsible for performing the "refund" transaction type. This method accepts a
    * negative transaction amount which indicates it is a refund.
    *
    * @param client the client account.
    * @param clientAuthCode the authorization code of the client.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void doRefund(String client, String clientAuthCode) throws DataCashServiceException
   {
      DataCashCard dataCashCard = getDataCashCard();
      CardResponse cardResponse =
         DataCashServiceFactory.getDataCashService(client).doRefund(dataCashCard, getTotalAmount(),
            clientAuthCode, merchantReference);
      processTransaction(cardResponse, client, REFUND_TRANSACTION);
      authCode = cardResponse.getAuthCode();
      // dataCashCard.purgeCardDetails();
   }

   /**
    * This method is responsible for fulfilling the actual payment. The transaction amount must be
    * positive .
    *
    * @param client The client account.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void fulfill(String client) throws DataCashServiceException
   {
      Response response = DataCashServiceFactory.getDataCashService(client).
         fulfill(datacashReference, authCode, getTotalAmount());
      processTransaction(response, client, FULFILL_TRANSACTION);
   }

   /**
    * This method is responsible for performing cancellation transaction.
    *
    * @param client the client account.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void cancel(String client) throws DataCashServiceException
   {
      if (StringUtils.isNotBlank(datacashReference) && returnCode == SUCCESS_STATUS)
      {
         Response response =
            DataCashServiceFactory.getDataCashService(client).cancel(datacashReference);
         processTransaction(response, client, CANCEL_TRANSACTION);
      }
   }

   /**
    * This method is responsible for refund transaction.This method accepts a negative transaction
    * amount which indicates it is a refund.
    *
    * @param client the client account.
    * @param dataCashReference the dataCashReference
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void refund(String client, String dataCashReference) throws DataCashServiceException
   {
      Response response =
         DataCashServiceFactory.getDataCashService(client).refund(dataCashReference,
            getTotalAmount());
      processTransaction(response, client, REVERSE_TRANSACTION);
   }

   /**
    * This is responsible for card holder verification.
    *
    * @param vTid the data-cash client account.
    * @param merchantUrl the merchantUrl.
    * @param userAgent the userAgent.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void cardEnrolmentVerfication(String vTid, String merchantUrl, String userAgent)
      throws DataCashServiceException
   {
      DataCashCard dataCashCard = getDataCashCard();
      CardVerificationResponse response =
         DataCashServiceFactory.getDataCashService(vTid).cardEnrolmentVerfication(merchantUrl,
            userAgent, dataCashCard, getTotalAmount(), getMerchantReference());
      if (is3DSuccess(response, vTid, PRE_TRANSACTION))
      {
         datacashReference = response.getDatacashReference();
         authCode = response.getAuthCode();
         returnCode = response.getCode();
      }
      acsUrl = response.getAcsUrl();
      paReq = response.getPaReq();
      mdUrl = String.valueOf(UUID.randomUUID().getMostSignificantBits()).replaceAll(HYPHEN, "");
      // dataCashCard.purgeCardDetails();
   }

   /**
    * This is responsible for card holder verification.
    *
    * @param vTid the data-cash client account.
    * @param merchantUrl the merchantUrl.
    * @param userAgent the userAgent.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void hccCardEnrolmentVerfication(String vTid, String merchantUrl, String userAgent, String hpsDatacashReference)
      throws DataCashServiceException
   {

      DataCashCard dataCashCard = getDataCashCard();
	   if(StringUtils.isNotEmpty(this.captureMethod)){
		   dataCashCard.setCaptureMethod(this.captureMethod);
	   }


      CardVerificationResponse response =
         DataCashServiceFactory.getDataCashService(vTid).cardEnrolmentVerfication(merchantUrl,
            userAgent, dataCashCard, getTotalAmount(), getMerchantReference(), hpsDatacashReference);
      if (isHcc3DSuccess(response, vTid, PRE_TRANSACTION))
      {
         datacashReference = response.getDatacashReference();
         authCode = response.getAuthCode();
         returnCode = response.getCode();
      }
      acsUrl = response.getAcsUrl();
      paReq = response.getPaReq();
      mdUrl = String.valueOf(UUID.randomUUID().getMostSignificantBits()).replaceAll(HYPHEN, "");
      // dataCashCard.purgeCardDetails();
   }

   public String hpsQueryTransaction(String vTid, String hpsDatacashReference)
		      throws DataCashServiceException
		   {

		        String status = DataCashServiceFactory.getDataCashService(vTid).hpsQueryTransaction(hpsDatacashReference);
				return status;

		      // dataCashCard.purgeCardDetails();
		   }

   public void hcsSetupReq(String vTid, String merchantUrl, String userAgent)
		      throws DataCashServiceException
		   {
		      DataCashCard dataCashCard = getDataCashCard();
		      Map<String, String> response =
		         DataCashServiceFactory.getDataCashService(vTid).setUpReqHCS(merchantUrl,
		            userAgent, dataCashCard, getTotalAmount(), getMerchantReference());

		      // dataCashCard.purgeCardDetails();
		   }

   /**
    * This is responsible for card holder verification.
    * This method also handle the duplicate request fired to datacash due to duplicate post request from ACS.
    *
    * @param vTid the data-cash client account.
    * @param paRes the payer authentication response.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void authorize3DPayment(String vTid, String paRes) throws DataCashServiceException
   {
      CardVerificationResponse response =
         DataCashServiceFactory.getDataCashService(vTid).authorize3DPayment(paRes,
            datacashReference);
      String token = trackingData.substring(0, 36);
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      String hybrisSessionId=getHybrisSessionId(paymentData);
      if(isDuplicateRespError(response.getCode()))
		{
			logTransaction("duplicate_threedsecure_authorization_request", response, vTid);
			QueryTransactionResponse queryResponse=performQuery(vTid);
			if(Objects.nonNull(queryResponse)) {
				LogWriter.logInfoMessage("Query response for duplicate_threedsecure_authorization_request: "
						+ queryResponse.getCode() + ",token:" + token + ",JSESSIONID:" + hybrisSessionId);
				if(queryResponse.getCode() == SUCCESS_STATUS )
			{
				if(isTransactionSuccess(paymentData.getTransactionStatus())){
					throw new DataCashServiceException(response.getCode(), "Duplicate 3D response Error.");

				}
				else
				{
					try {
						int waitingTime=ConfReader.getIntEntry(WAIT_TIME, 0);
						LogWriter.logInfoMessage("Going to sleep duplicate thread for: "+ waitingTime);
						Thread.sleep(waitingTime);
					} catch (InterruptedException e) {
						LogWriter.logErrorMessage("Duplicate Thread Interrupted while waiting for first thread to complete Fraud Screening");
					}
					if(isTransactionSuccess(paymentData.getTransactionStatus())) {
						throw new DataCashServiceException(response.getCode(), "Duplicate 3D response Error.");
					}
				}

			}
		}
		}
      datacashReference = response.getDatacashReference();
      authCode = response.getAuthCode();
      returnCode = response.getCode();
      threeDSecureAuthFlag = Boolean.FALSE;
      cardScheme = response.getCardScheme();
      country = response.getCountry();
      issuer = response.getIssuer();
      avsResponse = response.getAddressPolicyResultCode();
      cvvResponse = response.getCvvPolicyResultCode();
      responseMsg = response.getDescription();
      Boolean threedSuccess = is3DSuccess(response, vTid, "threedsecure_authorization_request");

      if (threedSuccess)
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("THREED_PAYMENT_SUCCESS"));
      }
      if (returnCode == SUCCESS_STATUS)
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_SUCCESS"));
      }
      else
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_FAILURE"));
      }
   }

   /**
    * This is responsible for card holder verification.
    * This method also handle the duplicate request fired to datacash due to duplicate post request from ACS.
    *
    * @param vTid the data-cash client account.
    * @param paRes the payer authentication response.
    *
    * @throws DataCashServiceException the data cash service exception, whenever any exception
    *         occurs.
    */
   public void authorizeHcc3DPayment(String vTid, String paRes) throws DataCashServiceException
   {
      CardVerificationResponse response =
         DataCashServiceFactory.getDataCashService(vTid).authorize3DPayment(paRes,
            datacashReference);
      String token = trackingData.substring(0, 36);
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      String hybrisSessionId=getHybrisSessionId(paymentData);
      if(isDuplicateRespError(response.getCode()))
		{
			logTransaction("duplicate_threedsecure_authorization_request", response, vTid);
			QueryTransactionResponse queryResponse=performQuery(vTid);
			if(Objects.nonNull(queryResponse)) {
				LogWriter.logInfoMessage("Query response for duplicate_threedsecure_authorization_request: "
						+ queryResponse.getCode() + ",token:" + token + ",JSESSIONID:" + hybrisSessionId);
				if(queryResponse.getCode() == SUCCESS_STATUS )
			{
				if(isTransactionSuccess(paymentData.getTransactionStatus())){
					throw new DataCashServiceException(response.getCode(), "Duplicate 3D response Error.");

				}
				else
				{
					try {
						int waitingTime=ConfReader.getIntEntry(WAIT_TIME, 0);
						LogWriter.logInfoMessage("Going to sleep duplicate thread for: "+ waitingTime);
						Thread.sleep(waitingTime);
					} catch (InterruptedException e) {
						LogWriter.logErrorMessage("Duplicate Thread Interrupted while waiting for first thread to complete Fraud Screening");
					}
					if(isTransactionSuccess(paymentData.getTransactionStatus())) {
						throw new DataCashServiceException(response.getCode(), "Duplicate 3D response Error.");
					}
				}

			}
		}
		}
      datacashReference = response.getDatacashReference();
      authCode = response.getAuthCode();
      returnCode = response.getCode();
      threeDSecureAuthFlag = Boolean.FALSE;
      cardScheme = response.getCardScheme();
      country = response.getCountry();
      issuer = response.getIssuer();
      avsResponse = response.getAddressPolicyResultCode();
      cvvResponse = response.getCvvPolicyResultCode();
      responseMsg = response.getDescription();
      Boolean threedSuccess = isHcc3DSuccess(response, vTid, "threedsecure_authorization_request");

      if (threedSuccess)
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("THREED_PAYMENT_SUCCESS"));
      }
      if (returnCode == SUCCESS_STATUS)
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_SUCCESS"));
      }
      else
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("PAYMENT_PRE_AUTH_FAILURE"));
      }
   }


	private boolean isTransactionSuccess(TransactionStatus tSts)
	{
		return tSts.equals(TransactionStatus.FRAUD_SCREENING_PRE_AUTH_SUCCESS)
				|| tSts.equals(TransactionStatus.FRAUD_SCREENING_PRE_AUTH_FAILURE)
				|| tSts.equals(TransactionStatus.PAYMENT_PRE_AUTH_SUCCESS);
	}

	/**
	 * @param 3DS responseCode
	 * @return true if duplicate handle switch is true and response code belongs to
	 *         duplicate response error code configured.
	 */
	private boolean isDuplicateRespError(int responseCode)
	{
		return ConfReader.getBooleanEntry(THREED_DOUBLE_POST_HANDLE_SWITCH, false)
				&& !threeDDuplicateRespErrorList.isEmpty()
				&& threeDDuplicateRespErrorList.contains(String.valueOf(responseCode));
   }

   /**
    * Overwrites sensitive data.This method is called just prior to dereferencing the object. This
    * intention is that no sensitive data resides in memory after the object has been de-referenced,
    * before it has been garbage collected. All sensitive data should be held in character
    * arrays--not Strings
    */
   public void clearSensitiveData()
   {
      card.purgeCardDetails();
   }

   /**
    * Gets essential transaction data associated with datacash transaction.
    *
    * @return essentialTransactionData the essential transaction data.
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      EssentialTransactionData essentialTransactionData =
         new EssentialTransactionData(paymentTypeCode, transactionAmount, transactionCharge,
                                      getPaymentMethod());
      essentialTransactionData.setTransactionReference(datacashReference);
      essentialTransactionData.setMaskedCardNumber(card.getMaskedCardNumber());
      essentialTransactionData.setAuthCode(authCode);
      essentialTransactionData.setExpiryDate(card.getExpiryDate());
      essentialTransactionData.setNameOnCard(card.getNameOncard());
      essentialTransactionData.setThreeDSecureAuthFlag(threeDSecureAuthFlag);
      essentialTransactionData.setPaymentGatewayResponse(getDataCashResponseCode());
      return essentialTransactionData;
   }

   /**
    * Creates a <code>Money</code> object, with the absolute value of total amount. Total amount
    * will be calculated by adding the transaction amount and transaction charges.
    *
    * @return The <code>Money</code> object for the total amount.
    */
   private Money getTotalAmount()
   {
      if (null != transactionAmount)
      {
         BigDecimal totamount = (transactionAmount.add(transactionCharge)).getAmount().abs();
         return new Money(totamount, transactionAmount.getCurrency());
      }
      return null;
   }

   /**
    * This method will log the transaction details and checks if the transaction is successful and
    * sets the dataCashReference number.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the data-cash account vTid.
    * @param transactionType the data-cash transactionType.
    *
    * @throws DataCashServiceException when transaction fails.
    */
   private void processTransaction(Response response, String vTid, String transactionType)
      throws DataCashServiceException
   {
      logTransaction(transactionType, response, vTid);
      // If the code = 1, the transaction is accepted.
      // In all other cases the transaction will fail and an exception
      // should be thrown.
      // DataCashServiceConstants.SUCCESS_STATUS should be set to 1.
      if (response.getCode() == SUCCESS_STATUS || response.getCode() == THREED_SUCCESS_STATUS)
      {
         datacashReference = response.getDatacashReference();
         return;
      }
      populateDataCashServiceException(response, vTid, false);
   }

   /**
    * This method will log the transaction details and checks if the transaction is successful and
    * sets the dataCashReference number.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the data-cash account vTid.
    * @param transactionType the data-cash transactionType.
    *
    * @throws DataCashServiceException when transaction fails.
    */
   private void processHccTransaction(Response response, String vTid, String transactionType)
      throws DataCashServiceException
   {
      //logTransaction(transactionType, response, vTid);

      logHccsTransaction(transactionType, response, vTid);
      // If the code = 1, the transaction is accepted.
      // In all other cases the transaction will fail and an exception
      // should be thrown.
      // DataCashServiceConstants.SUCCESS_STATUS should be set to 1.
      if (response.getCode() == SUCCESS_STATUS || response.getCode() == THREED_SUCCESS_STATUS)
      {
         datacashReference = response.getDatacashReference();
         return;
      }
      populateDataCashServiceException(response, vTid, false);
   }

   /**
    * This method will take the dataCash transactionType and cardResponse as the input and logs the
    * required details.
    *
    * @param transactionType the dataCash transaction type.
    * @param response The <code>CardResponse</code> object.
    * @param vTid the dataCash client account.
    *
    */
   private void logTransaction(String transactionType, Response response, String vTid)
   {
      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      String dateTime = dateFormat.format(date);
      String token = trackingData.substring(0, 36);
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      String hybrisSessionId=getHybrisSessionId(paymentData);
      StringBuilder trackingInfo = new StringBuilder();
      trackingInfo.append(transactionType).append(COMMA).append(dateTime).append(COMMA).append(
         trackingData).append(COMMA).append(merchantReference).append(COMMA).append(
         response.getDatacashReference()).append(COMMA).append(card.getMaskedCardNumber()).append(
         COMMA).append(card.getCardtype()).append(COMMA).append(getTotalAmount().getCurrency())
         .append(COMMA).append(getTotalAmount().getAmount()).append(COMMA).append(vTid).append(
         COMMA).append(getResponseCode(response.getCode())).append(COMMA).append(
         response.getDescription()).append(COMMA).append("JSESSIONID:"+hybrisSessionId);
      LogWriter.logInfoMessage(trackingInfo.toString(), DataCashPaymentTransaction.class.getName());

   }
   /**
	 * @param paymentData
	 * @return Hybris SessionID if exists in nonpaymentData.
	 */
	private String getHybrisSessionId(PaymentData paymentData) {
		if (paymentData != null && paymentData.getBookingInfo() != null) {
			String hybrisSessionId = paymentData.getBookingInfo()
					.getBookingComponent().getNonPaymentData()
					.get("JSESSIONID");
			if (StringUtils.isBlank(hybrisSessionId)) {
				return StringUtils.EMPTY;
			}
			return hybrisSessionId;
		}
		return StringUtils.EMPTY;
	}

   /**
    * This method created for hccs
    */
   private void logHccsTransaction(String transactionType, Response response, String vTid)
   {
      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      String dateTime = dateFormat.format(date);
      StringBuilder trackingInfo = new StringBuilder();
      trackingInfo.append(transactionType).append(COMMA).append(dateTime).append(COMMA).append(
         trackingData).append(COMMA).append(merchantReference).append(COMMA).append(
         response.getDatacashReference()).append(COMMA).append(COMMA).append(vTid).append(
         COMMA).append(getResponseCode(response.getCode())).append(COMMA).append(
         response.getDescription());
      LogWriter.logInfoMessage(trackingInfo.toString(), DataCashPaymentTransaction.class.getName());

   }

   /**
    * To check 3D check is successful.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the data-cash account vTid.
    * @param transactionType the dataCash transaction type.
    *
    * @return isCardEnrollmentSuccessful the isCardEnrollmentSuccessful.
    * @throws DataCashServiceException when transaction fails.
    */
   private Boolean is3DSuccess(Response response, String vTid, String transactionType)
      throws DataCashServiceException
   {
      // If the code = 150, the verification is successful.
      // In all other cases the transaction will fail and an exception
      // should be thrown.
      String token = trackingData.substring(0, 36);
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      String errorType = getErrorType(response.getCode());

      if (StringUtils.isNotBlank(errorType) && errorType.equals(FATAL_ERROR))
      {
         logTransaction(transactionType, response, vTid);
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("THREED_PAYMENT_FAILURE"));
         populateDataCashServiceException(response, vTid, true);
      }
      else if (StringUtils.isNotBlank(errorType))
      {
         isNonFatalError(response, vTid, transactionType, errorType);
         return false;
      }
      else
      {
         processTransaction(response, vTid, transactionType);
         this.merchantReference = getNewMerchantReference();
      }

      return true;
   }
   /**
    * To check 3D check is successful.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the data-cash account vTid.
    * @param transactionType the dataCash transaction type.
    *
    * @return isCardEnrollmentSuccessful the isCardEnrollmentSuccessful.
    * @throws DataCashServiceException when transaction fails.
    */
   private Boolean isHcc3DSuccess(Response response, String vTid, String transactionType)
      throws DataCashServiceException
   {
      // If the code = 150, the verification is successful.
      // In all other cases the transaction will fail and an exception
      // should be thrown.
      String token = trackingData.substring(0, 36);
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      String errorType = getHccErrorType(response.getCode());

      if (StringUtils.isNotBlank(errorType) && errorType.equals(FATAL_ERROR))
      {
         logHccsTransaction(transactionType, response, vTid);
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("THREED_PAYMENT_FAILURE"));
         populateDataCashServiceException(response, vTid, true);
      }
      else if (StringUtils.isNotBlank(errorType))
      {
         isHccNonFatalError(response, vTid, transactionType, errorType);
         return false;
      }
      else
      {
         processHccTransaction(response, vTid, transactionType);
         this.merchantReference = getNewMerchantReference();
      }

      return true;
   }

   /**
    * If the errorType is non fatal or 3d non fatal then the transaction sent to authorisation.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid vTid the data-cash account vTid.
    * @param transactionType transactionType the dataCash transaction type.
    * @param errorType The type of error either nonfatal or 3dnonfatal.
    *
    * @return isCardEnrollmentSuccessful the isCardEnrollmentSuccessful.
    * @throws DataCashServiceException DataCashServiceException when transaction fails.
    */
   private Boolean isNonFatalError(Response response, String vTid,
       String transactionType, String errorType)throws DataCashServiceException
   {
      if (errorType.equals(NON_FATAL_ERROR))
      {
         logTransaction(transactionType, response, vTid);
         this.merchantReference = getNewMerchantReference();
         authPayment(vTid, null);
      }
      else if (errorType.equals(THREED_NON_FATAL_ERROR))
      {
         logTransaction(transactionType, response, vTid);
         datacashReference = response.getDatacashReference();
         this.merchantReference = getNewMerchantReference();
         authorize3DPayment(vTid, null);
      }
      return false;
   }

   /**
    * If the errorType is non fatal or 3d non fatal then the transaction sent to authorisation.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid vTid the data-cash account vTid.
    * @param transactionType transactionType the dataCash transaction type.
    * @param errorType The type of error either nonfatal or 3dnonfatal.
    *
    * @return isCardEnrollmentSuccessful the isCardEnrollmentSuccessful.
    * @throws DataCashServiceException DataCashServiceException when transaction fails.
    */
   private Boolean isHccNonFatalError(Response response, String vTid,
       String transactionType, String errorType)throws DataCashServiceException
   {
      if (errorType.equals(NON_FATAL_ERROR))
      {
          //logTransaction(transactionType, response, vTid);
         logHccsTransaction(transactionType, response, vTid);
         datacashReference = response.getDatacashReference();
         this.merchantReference = getNewMerchantReference();

         //authPayment(vTid, null);
         authHccPayment(vTid, null, datacashReference);
      }
      else if (errorType.equals(HCC_NON_FATAL_ERROR))
      {
         //logTransaction(transactionType, response, vTid);
    	  logHccsTransaction(transactionType, response, vTid);
         datacashReference = response.getDatacashReference();
         this.merchantReference = getNewMerchantReference();
         authorizeHcc3DPayment(vTid, null);
      }
      return false;
   }


   /**This method is responsible for getting a new merchant reference keeping the
    *  brand code intact at the beginning and changing only the last 10 characters
    *  of the reference. We may as well have generated a new uuid and attached the
    *  brand to this but unfortunately this class is a payment domain class and is
    *  not brand aware(Rightly so). Hence the below workaround.
    *
    *  @return merchantreference the new merchant reference.
    */
   private String getNewMerchantReference()
   {
       String uuid = UUID.randomUUID().toString().replaceAll("-", "");
       StringBuffer temp = new StringBuffer(this.merchantReference.
           substring(0, this.merchantReference.length() - REPLACEMENT_INDEX));
       return temp.append(uuid.substring(uuid.length() - REPLACEMENT_INDEX)).toString();
   }

   /**
    * To check type of error.
    *
    * @param code the response code.
    *
    * @return errorType the errorType.
    */
   private String getErrorType(int code)
   {
      String errorType = null;

      if (nonFatalErrorList.contains(String.valueOf(code)))
      {
         errorType = NON_FATAL_ERROR;
      }
      else if (fatalErrorList.contains(String.valueOf(code)))
      {
         errorType = FATAL_ERROR;
      }
      else if (threeDNonFatalErrorList.contains(String.valueOf(code)))
      {

         errorType = THREED_NON_FATAL_ERROR;
      }

      LogWriter.logInfoMessage(errorType + " : " + code + " received during 3D process");
      return errorType;
   }

   /**
    * To check type of error.
    *
    * @param code the response code.
    *
    * @return errorType the errorType.
    */
   private String getHccErrorType(int code)
   {
      String errorType = null;

      if (fatalErrorList.contains(String.valueOf(code)))
      {
         errorType = FATAL_ERROR;
      }
      else if (hccThreeDNonFatalErrorList.contains(String.valueOf(code)))
      {

         errorType = HCC_NON_FATAL_ERROR;
      }

      LogWriter.logInfoMessage(errorType + " : " + code + " received during 3D process");
      return errorType;
   }

   /**
    * To throw DataCashServiceException.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the data-cash account vTid.
    * @param isFatalError to distinguish between fatal and non fatal error.
    *
    * @throws DataCashServiceException when transaction fails.
    */
   private void populateDataCashServiceException(Response response, String vTid,
      boolean isFatalError) throws DataCashServiceException
   {
      responseMsg = response.getDescription();
      LogWriter.logErrorMessage(responseMsg);
      int errorCode = response.getCode();
      String errorMsg =
         PropertyResource.getProperty(ERROR_MESSAGE_KEY + errorCode, MESSAGES_PROPERTY);
      if (StringUtils.isEmpty(errorMsg))
      {
         errorMsg = responseMsg;
      }
      LogWriter.logErrorMessage(PropertyResource.getProperty("datacash.generic.error",
         MESSAGES_PROPERTY) + vTid);
      LogWriter.logErrorMessage("Datacash Error: " + errorCode + HYPHEN + errorMsg);
      int code = errorCode;
      if (isFatalError)
      {
         code = DataCashServiceConstants.ERRORCODE_FOR_FATAL_ERRRORS;
      }
      if (StringUtils.isNotBlank(response.getAddressPolicyResultCode())
          && response.getAddressPolicyResultCode()
          .equalsIgnoreCase(DataCashServiceConstants.MATCHED))
      {
         LogWriter.logInfoMessage("Address Result code: " + response.getAddressPolicyResultCode());
      }

      if (StringUtils.isNotBlank(response.getPostCodePolicyResultCode())
               && response.getPostCodePolicyResultCode()
               .equalsIgnoreCase(DataCashServiceConstants.MATCHED))
      {
         LogWriter.logInfoMessage("Post Code Result code: "
            + response.getPostCodePolicyResultCode());
      }

      throw new DataCashServiceException(code, ERROR_MESSAGE_KEY + errorCode);
   }

   /**
    * Gets the DataCashCard object from the Card object.
    *
    * @return dataCashCard the <code>DataCashCard</code> object.
    */
   private DataCashCard getDataCashCard()
   {
      Map<String, String> address = new HashMap<String, String>();

      String streetAddress1 = card.getAddress().getStreetAddress1();
      String streetAddress2 = card.getAddress().getStreetAddress2();
      String streetAddress3 = card.getAddress().getStreetAddress3();
      String streetAddress4 = card.getAddress().getStreetAddress4();
      if (StringUtils.isNotBlank(streetAddress1))
      {
         address.put(DataCashServiceConstants.STREET_NUMBER, streetAddress1);
      }
      if (StringUtils.isNotBlank(streetAddress2))
      {
         address.put(DataCashServiceConstants.STREETADDRESS, streetAddress2);
      }
      if (StringUtils.isNotBlank(streetAddress3))
      {
         address.put(DataCashServiceConstants.TOWN, streetAddress3);
      }
      if (StringUtils.isNotBlank(streetAddress4))
      {
         address.put(DataCashServiceConstants.COUNTY, streetAddress4);
      }

      DataCashCard dataCashCard =
         new DataCashCard(card.getPan(), card.getNameOncard(), card.getExpiryDate(), card.getCv2(),
                          card.getPostCode(), card.getCardtype(), card.getIssueNumber(), address,
                          card.getCvvPolicies(), card.getAddressPolicies(),
                          card.getPostCodePolicies());
      dataCashCard.setStartDate(card.getStartDate());
      dataCashCard.setCv2AVSEnabled(card.getCv2AVSEnabled());
      dataCashCard.setCaptureMethod(captureMethod);
      return dataCashCard;
   }

   /**
    * This method takes the dataCash response code as the input parameter and returns the three
    * digit code required for logging. ex:if it takes 1 is the input it returns 001.
    *
    * @param code the response code.
    *
    * @return String the response code.
    */
   private String getResponseCode(int code)
   {
      return StringUtils.leftPad(String.valueOf(code), RESPONSE_CODE_LENGTH, '0');
   }

   /**
    * Set the capture method value.
    * @param captureMethod the captureMethod to set
    */
   public void setCaptureMethod(String captureMethod)
   {
      this.captureMethod = captureMethod;
   }

   /**
    * Gets the card scheme.
    *
    * @return the cardScheme.
    */
   public String getCardScheme()
   {
      return cardScheme;
   }

   /**
    * Gets the country.
    *
    * @return the country.
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Gets the issuer.
    *
    * @return the issuer.
    */
   public String getIssuer()
   {
      return issuer;
   }

   /**
    * Gets the avs response.
    *
    * @return the avsResponse.
    */
   public String getAvsResponse()
   {
      return avsResponse;
   }

   /**
    * Gets the cvv response.
    *
    * @return the cvvResponse.
    */
   public String getCvvResponse()
   {
      return cvvResponse;
   }

   /**
    * Gets the referral code.
    *
    * @return the referral code.
    */
   public String getReferralCode()
   {
      return referralCode;
   }

   /**
    * Gets the data cash response code.
    *
    * @return the response code.
    */
   public String getDataCashResponseCode()
   {
      return getResponseCode(returnCode);
   }

   /**
    * Gets the threeDSStatus.
    *
    * @return threeDSStatus
    */
   public String getThreeDSStatus()
   {
      return threeDSStatus;
   }

   /**
    * Sets the authorization code.
    *
    * @param threeDSStatus the threeDSStatus.
    */
   public void setThreeDSStatus(String threeDSStatus)
   {
      this.threeDSStatus = threeDSStatus;
   }

   /**
    *
    * @param vTid the Datacash vTiD.
    * @throws DataCashServiceException The Datacash Service Exception.
    */
   public void queryPayment(String vTid) throws DataCashServiceException
   {
      QueryTransactionResponse response = performQuery(vTid);
      if (Objects.nonNull(response))
      {
         update3DSStatus(response);
      }

   }

   /**
   *
   * @param vTid the Datacash vTiD.
   * @throws DataCashServiceException The Datacash Service Exception.
   */
  public QueryTransactionResponse performQuery(String vTid) throws DataCashServiceException
  {
      QueryTransactionResponse response = null;
      if (StringUtils.isNotBlank(datacashReference))
      {
         response = DataCashServiceFactory.getDataCashService(vTid).query(datacashReference);
     }
     return response;

  }
   /**
   *
   * @param vTid the Datacash vTiD.
   * @throws DataCashServiceException The Datacash Service Exception.
   */
  public void update3DSStatus(QueryTransactionResponse response) throws DataCashServiceException
  {
         ThreeDSStatusFinder finder = new ThreeDSStatusFinder();
         setThreeDSStatus(finder.findThreeDSStatus(response.getEci(), getCardScheme()));
      }



	/**
	 * Method makes Authenticate Payer XML-RPC call to DataCash & returns the
	 * response XML.
	 * 
	 * @param bookingComponent
	 * @param token
	 * @param url
	 * @param browserDetails
	 * @return AunthicatePayeeResXml
	 * @throws DataCashServiceException
	 */
	public AunthicatePayeeResXml doEmvAuthentication(
			BookingComponent bookingComponent, String token, String url,
			Map<String, String> browserDetails) throws DataCashServiceException {
		String vTid = bookingComponent.getPaymentGatewayVirtualTerminalId();

		AunthicatePayeeResXml authPayeeResponse = DataCashServiceFactory
				.getDataCashService(vTid).doEmvAuthentication(bookingComponent,
						token, url, browserDetails);
		return authPayeeResponse;

	}

	/**
	 * Updates transaction details & logging Authorization details.
	 * 
	 * @param authPayeeResponse
	 * @param vTid
	 * @param string
	 */
	private void processEMVTransaction(EMVAuthorizationResponse authResponse,
			String vTid, String transactionType) {
		logEMVAuthTransaction(transactionType, authResponse, vTid);
		datacashReference = authResponse.getDsReference();
		this.returnCode = DataCashServiceConstants.SUCCESS_STATUS;
	}

	/**
	 * Handles EMV 3DS Authorization response processing.
	 * 
	 * @param response
	 * @param vTid
	 * @throws DataCashServiceException
	 */
	public void processPostEMVChallengeFlow(EMVAuthorizationResponse response,
			String vTid) throws DataCashServiceException {

		datacashReference = response.getDsReference();
		authCode = response.getAuthcode();
		returnCode = Integer.parseInt(StringUtils.defaultIfEmpty(
				response.getStatus(), "000"));
		threeDSecureAuthFlag = Boolean.FALSE;
		cardScheme = response.getCardScheme();
		country = response.getCountry();
		issuer = response.getIssuer();
		avsResponse = response.getCv2avsStatus();
		responseMsg = response.getReason();
		updateEMVAuthTransaction(response, vTid, PRE_TRANSACTION);

	}

	
	/**
	 * This is responsible for card holder verification.
	 * 
	 * @param vTid
	 * @param password
	 * @param currency
	 * @param amount
	 * @param catpureMethod
	 * @param hpsDatacashReference
	 * @param token
	 * @return EMVAuthorizationResponse
	 * @throws DataCashServiceException
	 */
	public EMVAuthorizationResponse doEmv3DSAuthorisation(String vTid,
			String password, String currency, String amount,
			String catpureMethod, String hpsDatacashReference, String token)
			throws DataCashServiceException {

		EMVAuthorizationResponse emvAuthResponse = DataCashServiceFactory
				.getDataCashService(vTid).doEMV3DSAuthorisation(vTid, password,
						currency, amount, catpureMethod, hpsDatacashReference,
						getMerchantReference(), token);

		setAuthCode(emvAuthResponse.getAuthcode());
		setDatacashReference(emvAuthResponse.getDsReference());
      if (!ConfReader.getBooleanEntry(DataCashServiceConstants.EMV_MERCH_REF_ENABLED, true))
      {
         setMerchantReference(emvAuthResponse.getMerchantreference());
      }
		setAuthCode(emvAuthResponse.getAuthcode());
		setCvvResponse(emvAuthResponse.getCv2avsStatus());
		setStatus(emvAuthResponse.getStatus());
		setReason(emvAuthResponse.getReason());
		setTime(emvAuthResponse.getTime());
		setCardScheme(emvAuthResponse.getCardScheme());
		setCountry(emvAuthResponse.getCountry());
		if ("1".equals(StringUtils.defaultString(emvAuthResponse.getStatus()))) {
			setThreeDSStatus("true");

		}
		return emvAuthResponse;
	}
   
   
   
   
	/**
	 * Populates DataCashServiceException with configured error message.
	 * @param response
	 * @param vTid
	 * @throws DataCashServiceException
	 */
	private void populateEMVDataCashServiceException(
			EMVAuthorizationResponse response, String vTid)
			throws DataCashServiceException {
		responseMsg = response.getReason();
		LogWriter.logErrorMessage(responseMsg);
		int errorCode = Integer.parseInt(response.getStatus());
		String errorMsg = PropertyResource.getProperty(ERROR_MESSAGE_KEY
				+ errorCode, MESSAGES_PROPERTY);
		if (StringUtils.isEmpty(errorMsg)) {
			errorMsg = responseMsg;
		}
		LogWriter.logErrorMessage(PropertyResource.getProperty(
				"datacash.generic.error", MESSAGES_PROPERTY) + vTid);
		LogWriter.logErrorMessage("Datacash Error: " + errorCode + HYPHEN
				+ errorMsg);
		int code = errorCode;

		throw new DataCashServiceException(code, ERROR_MESSAGE_KEY + errorCode);
	}
   
   
	/**
	 * To check EMV 3Ds check was successful.
	 * 
	 * @param response
	 * @param vTid
	 * @param transactionType
	 * @throws DataCashServiceException
	 */
	private void updateEMVAuthTransaction(EMVAuthorizationResponse response,
			String vTid, String transactionType)
			throws DataCashServiceException {
		String token = trackingData.substring(0, 36);
		UUID uuid = UUID.fromString(token);
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
				uuid);

		if (!StringUtils.equalsIgnoreCase(response.getStatus(), "1")) {
			logEMVAuthTransaction(transactionType, response, vTid);
			new TransactionStatusUtil().updateStatus(paymentData, token,
					paymentData.getTransactionStatus(),
					TransactionStatus.findByCodeTS(DataCashServiceConstants.PAYMENT_PRE_AUTH_FAILURE));
			populateEMVDataCashServiceException(response, vTid);
		} else {
			processEMVTransaction(response, vTid, transactionType);
			new TransactionStatusUtil().updateStatus(paymentData, token,
					paymentData.getTransactionStatus(),
					TransactionStatus.findByCodeTS(DataCashServiceConstants.PAYMENT_PRE_AUTH_SUCCESS));
			this.merchantReference = getNewMerchantReference();
		}

	}

	/**
	 * Logs the EMV 3DS transaction details.
	 * @param transactionType
	 * @param response
	 * @param vTid
	 */
	private void logEMVAuthTransaction(String transactionType,
			EMVAuthorizationResponse response, String vTid) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		String dateTime = dateFormat.format(date);
		StringBuilder trackingInfo = new StringBuilder();
		trackingInfo
				.append(transactionType)
				.append(COMMA)
				.append(dateTime)
				.append(COMMA)
				.append(trackingData)
				.append(COMMA)
				.append(merchantReference)
				.append(COMMA)
				.append(response.getDsReference())
				.append(COMMA)
				.append(COMMA)
				.append(vTid)
				.append(COMMA)
				.append(getResponseCode(Integer.valueOf(
						StringUtils.defaultIfEmpty(response.getStatus(), "0"))
						.intValue())).append(COMMA)
				.append(response.getReason());
		LogWriter.logInfoMessage(trackingInfo.toString(),
				DataCashPaymentTransaction.class.getName());

	}

}
