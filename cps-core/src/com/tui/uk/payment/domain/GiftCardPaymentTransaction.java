/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: Aug 5, 2009
 *
 * $Author: sindhushree.g
 *
 * $Log: $
*/
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.SUCCESS_STATUS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REDEEM_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PPT_REVERSE_TRANSACTION;
import static com.tui.uk.payment.domain.PaymentConstants.COMMA;
import static com.tui.uk.payment.domain.PaymentConstants.ERROR_MESSAGE_KEY;
import static com.tui.uk.payment.domain.PaymentConstants.DATE_FORMAT;
import static com.tui.uk.payment.domain.PaymentConstants.RESPONSE_CODE_LENGTH;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.DataCashCard;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.payment.service.datacash.DataCashServiceFactory;
import com.tui.uk.payment.service.datacash.Response;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * This class represents gift card transaction.
 *
 * @author sindhushree.g
 *
 */
public class GiftCardPaymentTransaction implements PaymentTransaction
{

   /** The payment type code. */
   private String paymentTypeCode;

   /** The transaction amount. */
   private Money transactionAmount;

   /** The GiftCard object. */
   private GiftCard giftCard;

   /** The merchant reference id. */
   private String merchantReference;

   /** The datacash reference. */
   private String datacashReference;

   /** The datacash return code. */
   private int returnCode;

   /** Tracking data contains token,sessionId and inventory booking reference number
    *  required for logging. */
   private String trackingData;


   /**
    * The constructor.
    *
    * @param paymentTypeCode the payment type code.
    * @param transactionAmount the transaction amount.
    * @param giftCard the GiftCard object.
    * @param merchantReference the merchant reference id.
    */
   public GiftCardPaymentTransaction(String paymentTypeCode, Money transactionAmount,
      GiftCard giftCard, String merchantReference)
   {
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
      this.giftCard = giftCard;
      this.merchantReference = merchantReference;
   }

   /**
    * This method clears the sensitive card data.
    */
   public void clearSensitiveData()
   {
      giftCard.purgeCardDetails();
   }

   /**
    * Gets the Essential Transaction Data.
    *
    * @return essentialTransactionData.
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      EssentialTransactionData essentialTransactionData = new EssentialTransactionData(
         paymentTypeCode, transactionAmount, getTransactionCharge(), PaymentMethod.GIFT_CARD);
      essentialTransactionData.setTransactionReference(datacashReference);
      return essentialTransactionData;
   }

   /**
    * Gets the payment method.
    *
    * @return the Gift Card Payment method.
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.GIFT_CARD;
   }

   /**
    * Gets the payment type code.
    *
    * @return paymentTypeCode.
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }

   /**
    * Gets the transaction amount.
    *
    * @return transactionAmount.
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the transaction charge.
    *
    * @return always 0, as the transaction charges are not applicable for gift cards.
    */
   public Money getTransactionCharge()
   {
      return new Money(BigDecimal.ZERO, transactionAmount.getCurrency());
   }
   /**
    * This method sets the tracking data.
    * Tracking data contains token,sessionId and inventory booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
      this.trackingData = trackingdata;
   }

   /**
    * This method validates the gift card details.
    *
    * @throws PaymentValidationException if validation fails.
    */
   public void validate() throws PaymentValidationException
   {
      giftCard.validate();
   }

   /**
    * This method is responsible for Gift Card redemption.
    *
    * @param vTid the datacash vTid.
    *
    * @throws DataCashServiceException if redemption fails.
    */
   public void redeemPayment(String vTid) throws DataCashServiceException
   {
      Response response = DataCashServiceFactory.getDataCashService(vTid).redeemPayment(
         new DataCashCard(giftCard.getPan(), giftCard.getCv2()), transactionAmount,
         merchantReference);
      returnCode = response.getCode();
      processTransaction(response, vTid, REDEEM_TRANSACTION);
   }

   /**
    * This method is responsible for reversal of any previous redemption transaction.
    *
    * @param vTid the datacash vTid.
    *
    * @throws DataCashServiceException if reversal fails.
    */
   public void reversePayment(String vTid) throws DataCashServiceException
   {
      if (returnCode == SUCCESS_STATUS)
      {
         Response response = DataCashServiceFactory.getDataCashService(vTid)
            .reverseGiftCardTransaction(datacashReference);
         processTransaction(response, vTid, PPT_REVERSE_TRANSACTION);
      }
   }

   /**
    * To check if the transaction is successful.
    *
    * @param response The <code>CardResponse</code> object.
    * @param vTid the data-cash account vTid.
    * @param transactionType the data-cash transactionType.
    *
    * @throws DataCashServiceException when transaction fails.
    */
   private void processTransaction(Response response, String vTid,
              String transactionType) throws DataCashServiceException
   {
      logTransaction(transactionType, response, vTid);
      // If the code = 1, the transaction is accepted.
      // In all other cases the transaction will fail and an exception
      // should be thrown.
      // DataCashServiceConstants.SUCCESS_STATUS should be set to 1.
      if (response.getCode() != DataCashServiceConstants.SUCCESS_STATUS)
      {
         String responseMsg = response.getDescription();
         LogWriter.logErrorMessage(responseMsg);
         int errorCode = response.getCode();
         String errorMsg = PropertyResource.getProperty(ERROR_MESSAGE_KEY  + errorCode,
            MESSAGES_PROPERTY);         
         if (StringUtils.isBlank(errorMsg))
         {
            errorMsg = responseMsg;
         }
         LogWriter.logErrorMessage(PropertyResource.getProperty("datacash.generic.error",
            MESSAGES_PROPERTY) + vTid);
         LogWriter.logErrorMessage("Datacash Error: " + errorCode + "-" + errorMsg);
         throw new DataCashServiceException(errorCode, ERROR_MESSAGE_KEY  + errorCode);
      }
      datacashReference = response.getDatacashReference();
   }

   /**
    * This method will take the dataCash transactionType and cardResponse
    * as the input and logs the required details.
    *
    * @param transactionType the dataCash transaction type.
    * @param response The <code>CardResponse</code> object.
    * @param vTid the dataCash client account.
    *
    */
   private void logTransaction(String transactionType, Response response, String vTid)
   {
      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      String dateTime = dateFormat.format(date);
      StringBuilder trackingInfo = new StringBuilder();
      trackingInfo.append(transactionType).append(COMMA)
             .append(dateTime).append(COMMA)
             .append(trackingData).append(COMMA)
             .append(merchantReference).append(COMMA)
             .append(response.getDatacashReference()).append(COMMA)
             .append(giftCard.getMaskedCardNumber()).append(COMMA)
             .append(giftCard.getCardtype()).append(COMMA)
             .append(getTransactionAmount().getCurrency()).append(COMMA)
             .append(getTransactionAmount().add(getTransactionCharge()).getAmount()).append(COMMA)
             .append(vTid).append(COMMA)
             .append(getResponseCode(response.getCode())).append(COMMA)
             .append(response.getDescription());
      LogWriter.logInfoMessage(trackingInfo.toString(),
         GiftCardPaymentTransaction.class.getName());

   }
   /**
    * This method takes the dataCash response code as the input parameter
    * and returns the three digit code required for logging. ex:if it takes
    * 1 is the input it returns 001.
    *
    * @param code the response code.
    *
    * @return String the response code.
    */
   private String getResponseCode(int code)
   {
      return StringUtils.leftPad(String.valueOf(code), RESPONSE_CODE_LENGTH, '0');
   }

}
