/*
 * Copyright (C)2006 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PaymentStore.java,v $
 *
 * $Revision: 1.7 $
 *
 * $Date: 2008-05-16 09:04:52 $
 *
 * $Author : bibin.j $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.cache.Cache;
import com.tui.uk.cache.CacheFactory;
import com.tui.uk.log.LogWriter;

/**
 * This represents the PaymentStore which stores <Code>PaymentData</code> against unique token
 * in cache. This gives the facility to add new PaymentData, get existing PaymentData and destroy
 * existing PaymentData.
 *
 * @author bibin.j
 */
public final class PaymentStore
{
   /** The single instance of this class. */
   private static final PaymentStore PAYMENT_STORE = new PaymentStore();

   /** Cache for storing payment data against token. */
   private static final Cache PAYMENTDATACACHE =
                                  CacheFactory.createCache(PaymentConstants.PAYMENTDATA);
   /** Cache for storing payment data against token. */
   private static final Cache EXPERIANCACHE =
                                  CacheFactory.createEternalCache("ExperianCache");

   /** Represents ReentrantReadWriteLock. */
   private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

   /** private constructor. */

   private PaymentStore()
   {
   }

   /**
    * Gets the Singleton instance of the PaymentStore.
    *
    * @return the singleton instance of the PaymentStore.
    */
   public static PaymentStore getInstance()
   {
      return PAYMENT_STORE;
   }

   /**
    * Returns the payment data for the given token.
    *
    * @param token token.
    * @return payment payment.
    */
   public PaymentData getPaymentData(UUID token)
   {
      readWriteLock.readLock().lock();
      PaymentData paymentData = null;
      try
      {
         paymentData = (PaymentData) PAYMENTDATACACHE.get(token);
			String hybrisSessionId=getHybrisSessionId(paymentData);
			LogWriter.logInfoMessage("PaymentData " + paymentData + " retrieved for the given token " + token+ ",JSESSIONID:"+hybrisSessionId);
      }
      finally
      {
         readWriteLock.readLock().unlock();
      }
      return paymentData;
   }

   /**
    * Creates a new instance of PaymentData and adds it to the CPSCACHE with key as a unique
    * random number and value as payment data. The unique random number is then
    * returned to the client for further access of PaymentData.
    *
    * @param paymentData PaymentData.
    * @return token.
    */
   public UUID newPaymentData(PaymentData paymentData)
   {
      UUID token = UUID.randomUUID();
      readWriteLock.writeLock().lock();
      try
      {
         PAYMENTDATACACHE.put(token, paymentData);
         String hybrisSessionId=getHybrisSessionId(paymentData);
         LogWriter.logInfoMessage("Addded new PaymentData to cache with key as a"
            + " unique random number " + token + " and value as payment data"+" for JSESSIONID: "+hybrisSessionId);
      }
      finally
      {
         readWriteLock.writeLock().unlock();
      }
      return token;
   }

   /**
    * Wipes sensitive data within the <code>Payment</code>.
    *
    * @param token token.
    */
   public void destroySensitiveData(UUID token)
   {
      readWriteLock.writeLock().lock();
      try
      {
         PaymentData paymentData = getPaymentData(token);
         String hybrisSessionId=getHybrisSessionId(paymentData);
         Payment payment=paymentData.getPayment();
         if(Objects.nonNull(payment))
         {
        	 paymentData.getPayment().clearSensitiveData();
         }
         paymentData.setPayment(null);
         LogWriter.logInfoMessage("Removed sensitive data from Payment for the"
            + " given token " + token+ ",JSESSIONID:"+hybrisSessionId);
      }
      finally
      {
         readWriteLock.writeLock().unlock();
      }
   }

   /**
    * Wipes sensitive data within the <code>Payment</code>, then removes the
    * <code>PaymentData</code> from cache.
    *
    * @param token token.
    */
   public void destroyPayment(UUID token)
   {
      readWriteLock.writeLock().lock();
      try
      {
         PaymentData paymentData = getPaymentData(token);
         String hybrisSessionId=getHybrisSessionId(paymentData);
         if (paymentData != null && paymentData.getPayment() != null)
         {
            paymentData.getPayment().clearSensitiveData();
            paymentData.setPayment(null);
         }
         PAYMENTDATACACHE.remove(token);
         LogWriter.logInfoMessage("Removed PaymentData from cache for the given token " + token+ ",JSESSIONID:"+hybrisSessionId);
      }
      finally
      {
         readWriteLock.writeLock().unlock();
      }
   }


   public void experianTokenData(String experianToken,Object object){

	      readWriteLock.writeLock().lock();
	      try
	      {
	    	  EXPERIANCACHE.put(experianToken, object);
	         LogWriter.logInfoMessage("Addded new experian token to cache with key as a "
	           + experianToken + " and value as  experian token object");
	      }
	      finally
	      {
	         readWriteLock.writeLock().unlock();
	      }
   }



      public Object getExperianTokenData(String experianToken){
    	  String eToken="";
           Object object=null;
	        readWriteLock.readLock().lock();
	       try
	      {
	    	  object=EXPERIANCACHE.get(experianToken);
	         LogWriter.logInfoMessage("experian token " + eToken
	            + " retrieved for the given key " + experianToken);
	      }
	      finally
	      {
	         readWriteLock.readLock().unlock();
	      }

	   return object;

   }

	/**
	 * @param paymentData
	 * @return Hybris SessionID if exists in nonpaymentData.
	 */
	private String getHybrisSessionId(PaymentData paymentData) {
		if (paymentData != null && paymentData.getBookingInfo() != null) {
			String hybrisSessionId = paymentData.getBookingInfo()
					.getBookingComponent().getNonPaymentData()
					.get("JSESSIONID");
			if (StringUtils.isBlank(hybrisSessionId)) {
				return StringUtils.EMPTY;
			}
			return hybrisSessionId;
		}
		return StringUtils.EMPTY;
	}
	
	
}
