/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: Aug 4, 2009
 *
 * $Author: sindhushree.g
 *
 * $Log: $
*/
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class represents the Gift Card object.
 *
 * @author sindhushree.g
 *
 */
public class GiftCard implements Card
{

   /** Pattern for card number. */
   private static final Pattern CARDNUMBER_PATTERN = Pattern.compile("[0-9]{16}+");

   /** Pattern for security code. */
   private static final Pattern SECURITYCODE_PATTERN = Pattern.compile("[0-9]{4}+");

   /** Holds the start index for masking the card number. */
   private static final int MASK_START_INDEX = 6;

   /** Holds the end index for masking the card number. */
   private static final int MASK_END_INDEX = 4;

   /** The pan. */
   private char[] pan;

   /** The pin. */
   private char[] cv2;

   /** The card type. */
   private String cardType;

   /**
    * The constructor.
    *
    * @param pan the pan.
    * @param cv2 the pin.
    * @param cardType the card type.
    */
   public GiftCard(char[] pan, char[] cv2, String cardType)
   {
      char[] panCopy = pan;
      char[] cv2Copy = cv2;
      this.pan = panCopy;
      this.cv2 = cv2Copy;
      this.cardType = cardType;
   }

   /**
    * Gets the card type.
    *
    * @return cardType.
    */
   public String getCardtype()
   {
      return cardType;
   }

   /**
    * Gets the cv2.
    *
    * @return cv2.
    */
   public char[] getCv2()
   {
      char[] cv2Copy = cv2;
      return cv2Copy;
   }

   /**
    * Gets the expiry date.
    *
    * @return always null.
    */
   public String getExpiryDate()
   {
      return null;
   }

   /**
    * Gets the masked card number.
    *
    * @return maskedCardNumber.
    */
   public String getMaskedCardNumber()
   {
      StringBuilder maskedCardNumber =
                  new StringBuilder(String.valueOf(pan));
      for (int i = MASK_START_INDEX; i < pan.length - MASK_END_INDEX; i++)
      {
          maskedCardNumber.setCharAt(i, '*');
      }
      return maskedCardNumber.toString();
   }

   /**
    * Gets the name on card.
    *
    * @return always null.
    */
   public String getNameOncard()
   {
      return null;
   }

   /**
    * Gets the pan.
    *
    * @return pan.
    */
   public char[] getPan()
   {
      char[] panCopy = pan;
      return panCopy;
   }

   /**
    * Purges gift card details.
    */
   public void purgeCardDetails()
   {
      Arrays.fill(pan, '*');
      Arrays.fill(cv2, '*');
      pan = null;
      cv2 = null;
   }

   /**
    * Validates the gift card details.
    *
    * @throws PaymentValidationException if validation fails.
    */
   public void validate() throws PaymentValidationException
   {
      if (StringUtils.isBlank(String.valueOf(pan)) ||  (!validateCreditCardNumber()))
      {
         populateException("payment.pan.invalid", "CardNumber",
            PaymentConstants.ERRORCODE_INVALIDCARDNO);
      }
      else if (StringUtils.isBlank(String.valueOf(cv2)) || (!validateSecurityCode()))
      {
         populateException("payment.cv2.invalid", "SecurityCode",
            PaymentConstants.ERRORCODE_INVALIDSECURITYCODE);
      }
   }

   /**
    * Method to validate Credit Card Number.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateCreditCardNumber()
   {
      return CARDNUMBER_PATTERN.matcher(String.valueOf(pan)).matches();
   }

   /**
    * Method to validate Security code.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateSecurityCode()
   {
      return SECURITYCODE_PATTERN.matcher(String.valueOf(cv2)).matches();
   }

   /**
    * Method to populate exception.
    *
    * @param message the message code.
    * @param fieldName the associated field name.
    * @param errorCode the error code.
    *
    * @throws PaymentValidationException the Exception.
    */
   private void populateException(String message, String fieldName, int errorCode)
      throws PaymentValidationException
   {
      String errorMessage = PropertyResource.getProperty(message, MESSAGES_PROPERTY);
      LogWriter.logErrorMessage(errorMessage);
      throw new PaymentValidationException(errorCode, message, fieldName);
   }

}