/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: ChequePaymentTransaction.java,v $
 *
 * $Revision:  $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S.j$
 *
 * $Log : $
 *
 */

package com.tui.uk.payment.domain;

import java.math.BigDecimal;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.exception.PaymentValidationException;

 /**
 * This Class represents Cheque Payment Transaction.
 */
public final class ChequePaymentTransaction implements PaymentTransaction
{

    /** The payment type. */
    private String paymentTypeCode;

    /** The transaction amount. */
    private Money transactionAmount;

    /**
     * The Constructor.
     *
     * @param paymentTypeCode the payment type
     * @param transactionAmount the transaction amount
     */
    public ChequePaymentTransaction(String paymentTypeCode, Money transactionAmount)
    {
        this.paymentTypeCode = paymentTypeCode;
        this.transactionAmount = transactionAmount;

    }


    /**
     * Gets the payment type.
     *
     * @return the payment type
     */
    public String getPaymentTypeCode()
    {
       return paymentTypeCode;
    }

    /**
     * Gets the transaction amount.
     *
     * @return the transaction amount
     */
     public Money getTransactionAmount()
     {
        return transactionAmount;
     }

     /**
      * Gets the ZERO transaction charge for Cheque transaction.
      *
      * @return the transaction charge
      */
      public Money getTransactionCharge()
      {
         return new Money(BigDecimal.ZERO, transactionAmount.getCurrency());
      }

     /**
      * Gets the PaymentMethod associated with this Transaction.
      *
      * @return the PaymentMethod
      */
     public PaymentMethod getPaymentMethod()
     {
        return PaymentMethod.CHEQUE;
     }

   /**
    * Dummy method for validation.
    *
    * @throws PaymentValidationException Since, this is a dummy implementation
    *    this exception will not be thrown for cash transactions.
    */
   public void validate() throws PaymentValidationException
   {
      // Empty implementation.
      // Cheque Payment Transaction does not have any validations.
   }

   /**
    * Clears sensitive data present in the transaction.
    */
   public void clearSensitiveData()
   {
      //Do nothing, empty implementation.
   }

   /**
    * Gets essential transaction data associated with cheque transaction.
    *
    * @return essentialTransactionData the essential transaction data
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      return new EssentialTransactionData(paymentTypeCode,
         transactionAmount, getTransactionCharge(), getPaymentMethod());
   }
   /**
    * This method sets the trackingData.
    * Tracking data contains token,sessionId and inventory booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
       //TODO:need to implement this.
   }
}

