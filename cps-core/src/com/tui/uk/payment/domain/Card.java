/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Card.java,v $
 *
 * $Revision: 1.4 $
 *
 * $Date: 2008-05-14 07:14:57 $
 *
 * $author : bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This is an interface for different cards like Cnp cards, gift cards.
 *
 * @author sindhushree.g
 *
 */
public interface Card
{

   /**
    * This method validates the card details.
    *
    * @throws PaymentValidationException when validation fails.
    */
   void validate() throws PaymentValidationException;

   /**
    * This method returns the card number.
    *
    * @return the pan.
    */
   char[] getPan();

   /**
    * This method gets the expiry date.
    *
    * @return the expiry date.
    */
   String getExpiryDate();

   /**
    * This method gets the card type.
    *
    * @return the card type.
    */
   String getCardtype();

   /**
    * This method gets the name on card.
    *
    * @return the name on card.
    */
   String getNameOncard();

   /**
    * This method gets the cv2.
    *
    * @return the cv2.
    */
   char[] getCv2();

   /**
    * This method gets the masked card number.
    *
    * @return the masked card number.
    */
   String getMaskedCardNumber();

   /**
    * This method is responsible for purging sensitive information of the card.
    */
   void purgeCardDetails();

}
