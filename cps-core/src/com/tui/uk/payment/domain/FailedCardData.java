/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import com.tui.uk.client.domain.Money;

/**
 * This holds failed card information.
 *
 * @author sindhushree.g
 */
public class FailedCardData
{

   /** The cnpCard. */
   private CnpCard cnpCard;

   /** The authorizationCode name. */
   private String authorizationCode;

   /** The avsResponse. */
   private String avsResponse;

   /** The authorizedBy3dSecurity. */
   private String authorizedBy3dSecurity;

   /** The paymentGatewayreferenceNumber. */
   private String paymentGatewayreferenceNumber;

   /** The merchantReferenceNumber. */
   private String merchantReferenceNumber;

   /** The paymentGatewayResponseCode. */
   private String paymentGatewayResponseCode;

   /** The referralCode. */
   private String referralCode;

   /** The cardCharge. */
   private Money cardCharge;

   /** The cardAmount. */
   private Money cardAmount;

   /** The issuer. */
   private String issuer;

   /** The cardScheme. */
   private String cardScheme;

   /** The country. */
   private String country;

   /** The cvv response. */
   private String cvvResponse;

   /** The billingPhoneNumber. */
   private String billingPhoneNumber;

   /** The response message. */
   private String responseMsg;

   /**
    * The constructor.
    *
    * @param cnpCard the cnpCard.
    * @param cardAmount the cardAmount.
    * @param cardCharge the cardCharge.
    *
    */
   public FailedCardData(CnpCard cnpCard, Money cardAmount, Money cardCharge)
   {
      this.cnpCard = cnpCard;
      this.cardAmount = cardAmount;
      this.cardCharge = cardCharge;
   }

   /**
    * Sets the authorization code.
    *
    * @param authorizationCode the authorizationCode to set.
    */
   public void setAuthorizationCode(String authorizationCode)
   {
      this.authorizationCode = authorizationCode;
   }

   /**
    * Sets the avs response.
    *
    * @param avsResponse the avsResponse to set.
    */
   public void setAvsResponse(String avsResponse)
   {
      this.avsResponse = avsResponse;
   }

   /**
    * Sets the authorizedBy3Security data.
    *
    * @param authorizedBy3dSecurity the authorizedBy3dSecurity to set.
    */
   public void setAuthorizedBy3dSecurity(String authorizedBy3dSecurity)
   {
      this.authorizedBy3dSecurity = authorizedBy3dSecurity;
   }

   /**
    * Sets the payment gateway reference number.
    *
    * @param paymentGatewayreferenceNumber the paymentGatewayreferenceNumber to set.
    */
   public void setPaymentGatewayreferenceNumber(String paymentGatewayreferenceNumber)
   {
      this.paymentGatewayreferenceNumber = paymentGatewayreferenceNumber;
   }

   /**
    * Sets the merchant reference number.
    *
    * @param merchantReferenceNumber the merchantReferenceNumber to set.
    */
   public void setMerchantReferenceNumber(String merchantReferenceNumber)
   {
      this.merchantReferenceNumber = merchantReferenceNumber;
   }

   /**
    * Sets the payment gateway response code.
    *
    * @param paymentGatewayResponseCode the paymentGatewayResponseCode to set.
    */
   public void setPaymentGatewayResponseCode(String paymentGatewayResponseCode)
   {
        this.paymentGatewayResponseCode = paymentGatewayResponseCode;
   }

   /**
    * Sets the referral code.
    *
    * @param referralCode the referralCode to set.
    */
   public void setReferralCode(String referralCode)
   {
      this.referralCode = referralCode;
   }

   /**
    * Sets the issuer.
    *
    * @param issuer the issuer to set.
    */
   public void setIssuer(String issuer)
   {
      this.issuer = issuer;
   }

   /**
    * Sets the card scheme.
    *
    * @param cardScheme the cardScheme to set.
    */
   public void setCardScheme(String cardScheme)
   {
      this.cardScheme = cardScheme;
   }

   /**
    * Sets the country.
    *
    * @param country the country to set.
    */
   public void setCountry(String country)
   {
      this.country = country;
   }

   /**
    * Sets the cvv response.
    *
    * @param cvvresponse the cvvresponse to set.
    */
   public void setCvvresponse(String cvvresponse)
   {
      this.cvvResponse = cvvresponse;
   }

   /**
    * Sets the billing phone number.
    *
    * @param billingPhoneNumber the billingPhoneNumber to set.
    */
   public void setBillingPhoneNumber(String billingPhoneNumber)
   {
      this.billingPhoneNumber = billingPhoneNumber;
   }

   /**
    * Sets the response message.
    *
    * @param responseMsg the response message to set.
    */
   public void setResponseMsg(String responseMsg)
   {
      this.responseMsg = responseMsg;
   }

   /**
    * Gets the cnp card.
    *
    * @return the cnpCard.
    */
   public CnpCard getCnpCard()
   {
      return cnpCard;
   }

   /**
    * Gets the authorization code.
    *
    * @return the authorizationCode.
    */
   public String getAuthorizationCode()
   {
      return authorizationCode;
   }

   /**
    * Gets the avs response.
    *
    * @return the avsResponse.
    */
   public String getAvsResponse()
   {
      return avsResponse;
   }

   /**
    * Gets authorized by 3D data.
    *
    * @return the authorizedBy3dSecurity.
    */
   public String isAuthorizedBy3dSecurity()
   {
      return authorizedBy3dSecurity;
   }

   /**
    * Gets the payment gateway reference number.
    *
    * @return the paymentGatewayreferenceNumber.
    */
   public String getPaymentGatewayreferenceNumber()
   {
      return paymentGatewayreferenceNumber;
   }

   /**
    * Gets the merchant reference number.
    *
    * @return the merchantReferenceNumber.
    */
   public String getMerchantReferenceNumber()
   {
      return merchantReferenceNumber;
   }

   /**
    * Gets the payment gateway response code.
    *
    * @return the paymentGatewayResponseCode.
    */
   public String getPaymentGatewayResponseCode()
   {
      return paymentGatewayResponseCode;
   }

   /**
    * Gets the referral code.
    *
    * @return the referralCode.
    */
   public String getReferralCode()
   {
      return referralCode;
   }

   /**
    * Gets the card charge.
    *
    * @return the cardCharge.
    */
   public Money getCardCharge()
   {
      return cardCharge;
   }

   /**
    * Gets the card amount.
    *
    * @return the cardAmount.
    */
   public Money getCardAmount()
   {
      return cardAmount;
   }

   /**
    * Gets the issuer.
    *
    * @return the issuer.
    */
   public String getIssuer()
   {
      return issuer;
   }

   /**
    * Gets the card scheme.
    *
    * @return the cardScheme.
    */
   public String getCardScheme()
   {
      return cardScheme;
   }

   /**
    * Gets the country.
    *
    * @return the country.
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Gets the cvv response.
    *
    * @return the cvvResponse.
    */
   public String getCvvResponse()
   {
      return cvvResponse;
   }

   /**
    * Gets the billing phone number.
    *
    * @return the billingPhoneNumber.
    */
   public String getBillingPhoneNumber()
   {
      return billingPhoneNumber;
   }

   /**
    * Gets the response message.
    *
    * @return the response message.
    */
   public String getResponseMsg()
   {
      return responseMsg;
   }

   /**
    * Purges card information.
    */
   public void purgeCardData()
   {
      cnpCard.purgeCardDetails();
   }

}
