/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PDQPaymentTransaction.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This Class represents PDQ Transaction.
 */
public final class PDQPaymentTransaction implements PaymentTransaction
{

   /** Holds the maximum possible length of the authorization code. */
   private static final int MAX_AUTH_CODE_LENGTH = 25;

   /** Holds the minimum possible length of the authorization code. */
   private static final int MIN_AUTH_CODE_LENGTH = 4;

   /** Holds the minimum possible length of the authorization code for American Express. */
   private static final int MIN_AMEX_AUTH_CODE_LENGTH = 2;

   /** The payment type. */
   private String paymentTypeCode;

   /** The authorization code. */
   private String authCode;

   /** The transaction amount. */
   private Money transactionAmount;

   /** The transaction charge. */
   private Money transactionCharge;

   /**
    * The Constructor.
    *
    * @param transactionCharge the transaction charge
    * @param paymentTypeCode the paymentTypeCode type
    * @param authCode the authCode code
    * @param transactionAmount the transaction amount
    */
   public PDQPaymentTransaction(String paymentTypeCode, Money transactionAmount,
            Money transactionCharge, String authCode)
   {
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
      this.transactionCharge = transactionCharge;
      this.authCode = authCode;

   }

   /**
    * Gets the payment type.
    *
    * @return the payment type
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the transaction charge.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return transactionCharge;
   }

   /**
    * Gets the authorization code.
    *
    * @return the authorization code
    */
   public String getAuthCode()
   {
      return authCode;
   }

   /**
    * Gets the PaymentMethod associated with this Transaction.
    *
    * @return the PaymentMethod
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.PDQ;
   }

   /**
    * This method is responsible for validating the authorization code length.
    *
    * @throws PaymentValidationException when validation fails.
    */
   public void validate() throws PaymentValidationException
   {
      String errorMessageKey = "payment.authcode.invalid";
      if (authCode == null)
      {
         String errorMessage = PropertyResource.getProperty(errorMessageKey, MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PaymentValidationException(errorMessageKey);
      }
      int minLength = MIN_AUTH_CODE_LENGTH;
      if (paymentTypeCode.equals(PaymentConstants.AMERICAN_EXPRESS))
      {
        minLength = MIN_AMEX_AUTH_CODE_LENGTH;
      }
      if (authCode.length() < minLength || authCode.length() > MAX_AUTH_CODE_LENGTH)
      {
         String errorMessage = PropertyResource.getProperty(errorMessageKey, MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PaymentValidationException(errorMessageKey);
      }
   }

   /**
    * Clears sensitive data present in the transaction.
    */
   public void clearSensitiveData()
   {
      //Do nothing, empty implementation.
   }

   /**
    * Gets essential transaction data associated with PDQ payment transaction.
    *
    * @return essentialTransactionData the essential transaction data.
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      EssentialTransactionData essentialTransactionData = new EssentialTransactionData(
               paymentTypeCode, transactionAmount, transactionCharge, getPaymentMethod());
      essentialTransactionData.setTransactionReference(authCode);
      return essentialTransactionData;
   }
   /**
    * This method sets the trackingData.
    * Tracking data contains token,sessionId and inventory booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
       //TODO:need to implement this.
   }
}
