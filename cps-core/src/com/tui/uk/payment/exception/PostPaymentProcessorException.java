/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: $
 * 
 * $Revision: $
 * 
 * $Date: $
 * 
 * $Author: $
 * 
 * $Log: $
 */
package com.tui.uk.payment.exception;

/**
 * This exception is thrown whenever there is a problem during post payment
 * processing.
 * 
 * @author sindhushree.g
 * 
 */
@SuppressWarnings("serial")
public final class PostPaymentProcessorException extends Exception
{

   /**
    * Default constructor for PaymentValidationException class.
    */
   public PostPaymentProcessorException()
   {
      super();
   }

   /**
    * Constructs PostPaymentProcessorException with the specified
    * validation exception message.
    * 
    * @param message the detail message.
    */
   public PostPaymentProcessorException(String message)
   {
      super(message);
   }

   /**
    * Defines the constructor for PostPaymentProcessorException with
    * detailed message and specified cause.
    * 
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public PostPaymentProcessorException(String message, Throwable cause)
   {
      super(message, cause);
   }

   /**
    * Defines the constructor for PostPaymentProcessorException with
    * specified cause.
    * 
    * @param cause - The specified cause.
    */
   public PostPaymentProcessorException(Throwable cause)
   {
      super(cause);
   }

}
