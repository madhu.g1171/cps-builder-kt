/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: AddressFinderServiceException.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2008-05-15 08:25:26 $
 * 
 * $Author: bibin.j@sonata-software.com $
 * 
 * 
 * $Log: $.
 */

package com.tui.uk.payment.exception;

/**
 * This is the exception thrown from the address finder service layer.
 * 
 * @author bibin.j@sonata-software.com
 */
@SuppressWarnings("serial")
public final class AddressFinderServiceException extends Exception
{
   /**
    * Default constructor for AddressFinderServiceException class.
    */
   public AddressFinderServiceException()
   {
      super();
   }

   /**
    * Constructs AddressFinderServiceException with detail message.
    * 
    * @param message the detail message.
    */
   public AddressFinderServiceException(String message)
   {
      super(message);
   }

   /**
    * Defines the constructor for AddressFinderServiceException with
    * detailed message and specified cause.
    * 
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public AddressFinderServiceException(String message, Throwable cause)
   {
      super(message, cause);
   }

   /**
    * Defines the constructor for AddressFinderServiceException with
    * specified cause.
    * 
    * @param cause - The specified cause.
    */
   public AddressFinderServiceException(Throwable cause)
   {
      super(cause);
   }
}
