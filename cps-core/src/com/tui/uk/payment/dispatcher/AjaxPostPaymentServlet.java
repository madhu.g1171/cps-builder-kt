/*
 * Copyright (C)2013 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: AjaxPostPaymentServlet.java,v $
 *
 * $Revision: 1.5 $
 *
 * $Date: 2008-05-30 13:13:00 $
 *
 * $Author: pushparaja.g $
 */
package com.tui.uk.payment.dispatcher;

import static com.tui.uk.payment.dispatcher.DispatcherConstants.ACCEPT_LANGUAGE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.HYPHEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.PRE_AUTHENTICATION_SESSION_TIMEOUT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.SESSION_MAX_INTERVAL;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.THREE_D_SECURE_CONF_VALUE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.USER_AGENT;

import java.io.IOException;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.payment.processor.FraudScreeningProcessor;
import com.tui.uk.payment.processor.FraudScreeningProcessorImpl;
import com.tui.uk.payment.processor.PostPaymentProcessor;
import com.tui.uk.payment.processor.PostPaymentProcessorFactory;

/**
 * The Class AjaxPostPaymentServlet. This class is used to handle the payment request
 *
 * @author pushparaja.g
 */
@SuppressWarnings("serial")
public final class AjaxPostPaymentServlet extends PostPaymentServlet
{

   /** The 3D secure overlay url constant. */
   private static final String THREE_D_SECURE_OVERLAY_URL =
      "/common/threeDSecureOverlayForward.jsp?";

   /** The brand parameter name. */
   private static final String BRAND_CODE_PARAM_NAME = "b=";

   /** The tomcat instance request parameter. */
   private static final String TOMCAT_PARAM_NAME = "&tomcat=";

   /** The post payment URL. */
   private static final String POST_PAYMENT_URL = "postPaymentUrl";

   /** The payment URL. */
   private static final String PAYMENT_URL = "paymentUrl";

   /** The visit URL. */
   private static final String VISIT_URL = "visiturl";

   /** The visit URL. */
   private static final String YES = "yes";

   /** The Text/plain content type. */
   private static final String TEXT_CONTENT_TYPE = "text/plain";

   /** The UTF-8 encoding. */
   private static final String UTF_ENCODING = "UTF-8";


   /**
    * This method is responsible for performing all the necessary action that has to be taken once
    * the payment page is submitted.
    *
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    *
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the POST could not be
    *            handled.
    */
   @SuppressWarnings("unchecked")
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      String token = request.getParameter(TOKEN);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
      BookingInfo bookingInfo = paymentData.getBookingInfo();
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();

      verifySessionId(bookingInfo, request.getSession().getId());

      // If price changes due to browser back and forward buttons
      String priceChangeUrl = priceChangeUrl(bookingComponent);
      if (StringUtils.isNotBlank(priceChangeUrl))
      {
         sendAjaxResponse(response, priceChangeUrl);
         return;
      }

      request.setAttribute(POST_PAYMENT_URL, paymentPageAbsoluteUrl(request));
      if (!bookingInfo.getNewHoliday())
      {
         sendAjaxResponse(response, request.getHeader(DispatcherConstants.REFERER));
         return;
      }
      if (bookingInfo.getThreeDAuth() && paymentData.getPayment().getPaymentTransactions() != null)
      {
         sendAjaxResponse(response, getPostPaymentUrl(bookingComponent, null));
         return;
      }

      resetBookingInfo(bookingInfo);

      String paymentFailureUrl = bookingComponent.getPaymentFailureURL();

      // If the failure count is greater than maximum failure count, then the application
      // should redirect to payment failure page and should stop processing.
      if (isPaymentFailureRedirect(paymentData, paymentFailureUrl))
      {
         sendAjaxResponse(response, paymentFailureUrl);
         return;
      }

      //Go back to payment page with error message in case of TRACS down time.
      if(checkForDownTimeError(request))
      {
         String errorMessage = "DOWNTIME_ERROR|" + (String)request.
                  getAttribute("downTimeErrorMsg");
         LogWriter.logErrorMessage(errorMessage);
         sendAjaxResponse(response, "ERROR_MESSAGE:" + errorMessage);
         return;
      }
      
      PostPaymentProcessor postPaymentProcessor =
         PostPaymentProcessorFactory.getPostPaymentProcessor(
            bookingComponent.getClientApplication(), paymentData, request.getParameterMap());
      String url = null;
      String browserLocale = request.getHeader(ACCEPT_LANGUAGE);
      Locale locale =
         new Locale(StringUtils.substringBefore(browserLocale, HYPHEN),
            StringUtils.substringAfterLast(browserLocale, HYPHEN));

      try
      {
         postPaymentProcessor.preProcess();
         postPaymentProcessor.process();

         // doing card holder verification in 3D secure transactions.
         // for 3d secure transaction URl will be bank URL.
         // if it is not 3D secure transactions, URL will be null.
         url =
            postPaymentProcessor.postProcess(super.buildTermUrl(request),
               request.getHeader(USER_AGENT));

         logThreeDData(postPaymentProcessor.getThreeDData(), token, bookingComponent
            .getClientApplication().getClientApplicationName());
         // perform fraud screening only for datacash payment transaction.
         if (paymentData.getPayment().getPaymentTransactions(PaymentMethod.DATACASH).size() > 0)
         {
            processFraudScreening(url, token, bookingComponent);
         }

      }
      catch (PostPaymentProcessorException ppe)
      {
         String errorMessage = getErrorMessage(ppe.getMessage(), locale, null);
         LogWriter.logErrorMessage(errorMessage, ppe);
         processException(bookingInfo, errorMessage);
         sendAjaxResponse(response, "ERROR_MESSAGE:" + errorMessage);
         return;
      }
      catch (PaymentValidationException pve)
      {
         String errorMessage =
            getDataCashFatalErrorMessage(pve.getCode(), locale, pve.getMessage());
         LogWriter.logErrorMessage(errorMessage, pve);
         processException(bookingInfo, errorMessage);
         processPaymentValidationException(request, response, pve, paymentData, locale);
         return;
      }

      if (StringUtils.isNotBlank(url)
         && ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
            .getClientApplicationName() + THREE_D_SECURE_CONF_VALUE, false))
      {
         // while moving to the bank page increase session timeout to 1hr.
         request.getSession().setMaxInactiveInterval(
            ConfReader.getIntEntry(PRE_AUTHENTICATION_SESSION_TIMEOUT, SESSION_MAX_INTERVAL));

         setSessionData(postPaymentProcessor, request, url);

         StringBuilder threedsUrl = new StringBuilder();
         threedsUrl = buildThreeDSUrl(request, threedsUrl);

         threedsRedirect(request, response, threedsUrl);
      }
      else
      {
         // if URL is null getting post payment url from bookingComponent or conf file.
         url = getPostPaymentUrl(bookingComponent, url);
         sendAjaxResponse(response, url);
      }
   }

   /**
    * This method is used as a common method to send the AJAX response the UI.
    *
    * @param response the HttpServletResponse object
    * @param out the AJAX output
    *
    */
   private void sendAjaxResponse(HttpServletResponse response, String output) throws IOException
   {
      response.setContentType(TEXT_CONTENT_TYPE);
      response.setCharacterEncoding(UTF_ENCODING);
      response.getWriter().write(output);
   }

   /**
    * This method is used to build the 3D security url.
    *
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param threedsUrl the 3D security url.
    *
    * @return threedsUrl
    */
   protected StringBuilder buildThreeDSUrl(HttpServletRequest request, StringBuilder threedsUrl)
   {
      threedsUrl.append(THREE_D_SECURE_OVERLAY_URL);
      threedsUrl.append(BRAND_CODE_PARAM_NAME);
      threedsUrl.append(request.getParameter("b"));
      threedsUrl.append(TOMCAT_PARAM_NAME);
      threedsUrl.append(request.getParameter("tomcat"));
      return threedsUrl;
   }

   /**
    * This method is used to redirect to 3D Secure pagen.
    *
    * @param response HttpResponse
    * @param threedsUrl 3D Secure URL
    * @param clientApplicationName brand name
    * @throws ServletException
    */
   protected void threedsRedirect(HttpServletRequest request, HttpServletResponse response,
      StringBuilder threedsUrl) throws IOException, ServletException
   {
      request.setAttribute(POST_PAYMENT_URL, paymentPageAbsoluteUrl(request));
      request.setAttribute(PAYMENT_URL, paymentPageAbsoluteUrl(request));
      request.getSession().setAttribute(VISIT_URL, YES);

      RequestDispatcher requestDispatcher = request.getRequestDispatcher(threedsUrl.toString());
      requestDispatcher.forward(request, response);
   }

   /**
    * This method is processing PaymentValidationException.
    *
    * @param request the HttpServletRequest object.
    * @param response the HttpServletResponse object.
    * @param pve the PaymentValidationException object.
    * @param paymentData the PaymentData object.
    *
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the POST could not be
    *            handled.
    */
   protected void processPaymentValidationException(HttpServletRequest request,
      HttpServletResponse response, PaymentValidationException pve, PaymentData paymentData,
      Locale locale) throws ServletException, IOException
   {
      BookingInfo bookingInfo = paymentData.getBookingInfo();
      String paymentFailureUrl = bookingInfo.getBookingComponent().getPaymentFailureURL();
      if (StringUtils.isNotBlank(paymentFailureUrl))
      {
         paymentData.setFailureCount();
      }
      if (isPaymentFailureRedirect(paymentData, paymentFailureUrl))
      {
         sendAjaxResponse(response, paymentFailureUrl);
      }
      else
      {
         bookingInfo.setErrorFields(pve.getErrorFields());
         String errorMessage =
            getDataCashFatalErrorMessage(pve.getCode(), locale, pve.getMessage());
         sendAjaxResponse(response, "ERROR_MESSAGE:" + errorMessage);
      }
      FraudScreeningProcessor fraudScreeningProcessor =
         new FraudScreeningProcessorImpl(request.getParameter(TOKEN));
      fraudScreeningProcessor.updateFailureCardData();
      return;
   }

   /**
    * This method is checks for TRACS down time error.
    *
    * @param httpRequest the HttpServletRequest object.
    *
    *  @return isDown the boolean
    */
   public boolean checkForDownTimeError(HttpServletRequest httpRequest)
   {
      boolean isDown = false;
      if(httpRequest.getAttribute("RedirectToPaymentPage") != null)
      {
         if(httpRequest.getAttribute("downTimeErrorMsg") != null)
         {
            isDown = true;
         }
      }
      return isDown;
   }
}
