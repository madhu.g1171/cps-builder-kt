/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PromotionalCodeServlet.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-15 08:25:26 $
 *
 * $Author: bibin.j@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.payment.dispatcher;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.DATE_FORMAT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.COMMA;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.BALANCE_TYPE;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.promotionaldiscount.PromotionalDiscountServiceImpl;
import com.tui.uk.promotionaldiscount.constants.PromotionalDiscountConstants;
import com.tui.uk.promotionaldiscount.domain.AbtaDetails;
import com.tui.uk.promotionaldiscount.domain.PromotionalDiscountRequest;
import com.tui.uk.promotionaldiscount.domain.PromotionalDiscountResponse;
import com.tui.uk.promotionaldiscount.exception.PromotionalDiscountServiceException;
import com.tui.uk.promotionaldiscount.tracs.domain.TracsRequest;
import com.tui.uk.promotionaldiscount.tracs.domain.TracsResponse;

/**
 * This class performs the following operations. 1)Form the promotional
 * discount object with tracs session id and promotional discount field
 * value from ui. 2)Call the promotional discount service passing
 * promotional discount request. 3)Returns the promotional discount
 * response.
 *
 * @author bibin.j@sonata-software.com
 */
@SuppressWarnings("serial")
public final class PromotionalCodeServlet extends CommonPaymentServlet
{

   /** Key for invalid promo code. */
   private static final String INVALID = ".invalid";

   /** Key for promo code not applicable. */
   private static final String NOT_APPLICABLE = ".discount.notapplicable";

   /** Key for expired promo code. */
   private static final String EXPIRED = ".expired";

   /** Key for promo code not validated. */
   private static final String NOT_VALIDATED = ".notvalidated";

   /** Key for promo code message. */
   private static final String PROMO_CODE = "promotionalcode.";

   /** Key for default. */
   private static final String DEFAULT = "default";

   /** Key for promo code already used. */
   private static final String ALREADY_USED = ".alreadyused";

   /** Pattern for Promotional Code . */
   private static final Pattern PROMOTIONALCODE = Pattern.compile("[A-Za-z0-9-]{1,8}+");

   /** Pattern for Portland Promotional Code . */
   private static final Pattern PORTLAND_PROMOTIONALCODE = Pattern.compile("[A-Za-z0-9]{21}+");

   /** The constant for holding PROMOCODE_SERVICE. */
   private static final String PROMOCODE_SERVICE =
      "promotionalCodeService";

   /**
    * Receives an HTTP POST request and handles the request. The HTTP POST
    * method allows the client to send data of unlimited length to the Web
    * server once and is useful when posting information such as credit
    * card numbers.
    *
    * @param request the HttpServletRequest object that contains the
    *           request the client has made of the servlet.
    * @param response the HttpServletResponse object that contains the
    *           response the servlet sends to the client.
    *
    * @throws IOException the IOException thrown if an input or output
    *            error is detected when the servlet handles the request
    * @throws ServletException the ServletException thrown if the request
    *            for the POST could not be handled.
    */
   protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException
   {
      String promotionalCode = request.getParameter(PromotionalDiscountConstants.PROMOTIONALCODE);
      String balanceType = request.getParameter(BALANCE_TYPE);

      BookingInfo bookingInfo = PaymentStore.getInstance().getPaymentData(
         UUID.fromString(request.getParameter(TOKEN))).getBookingInfo();
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      try
      {
         String browserLocale = request.getHeader("Accept-Language");
         Locale locale = new Locale(StringUtils.substringBefore(browserLocale, "-"),
               StringUtils.substringAfterLast(browserLocale, "-"));
         if (!validatePromoCode(promotionalCode))
         {
            String error = PropertyResource.getProperty(PROMO_CODE + "invalid",
               MESSAGES_PROPERTY, locale);
            LogWriter.logErrorMessage(error);
            throw new PromotionalDiscountServiceException(error);
         }
         TracsRequest requestMessage =
            createPromotionalDiscountRequest(promotionalCode, bookingComponent);
         TracsResponse responseMessage =
            new PromotionalDiscountServiceImpl().process(requestMessage);
         PromotionalDiscountResponse promoDiscountResponse =
            (PromotionalDiscountResponse) responseMessage;
         String promoDiscount = promoDiscountResponse.getDiscount();
         if (promoDiscount != null)
         {
            logPromoCodeDetails(promoDiscountResponse, promotionalCode,
               bookingInfo.getTrackingData(),
                  bookingComponent.getClientApplication().getClientApplicationName());
            bookingInfo.updateDiscount(new Money(new BigDecimal(Double.parseDouble(promoDiscount)),
               bookingComponent.getTotalAmount().getCurrency()),
                  PromotionalDiscountConstants.PROMOCODE, balanceType);
            bookingComponent.setTracsBookMinusId(((PromotionalDiscountResponse) responseMessage)
                  .getTokenId().trim());
            bookingComponent.getNonPaymentData().put(PromotionalDiscountConstants.IS_PROMO_APPLIED,
               String.valueOf(true));
            bookingComponent.getNonPaymentData().put(PromotionalDiscountConstants.PROMO_DISCOUNT,
               promoDiscount);
            bookingComponent.getNonPaymentData().put(
               PromotionalDiscountConstants.PROMO_CODE, promotionalCode);
            bookingComponent.getNonPaymentData().put(PromotionalDiscountConstants.BOOK_MINUS_ID,
               ((PromotionalDiscountResponse) responseMessage).getTokenId().trim());
            response.getWriter().write(promoDiscount + "|"
                  + bookingInfo.getCalculatedTotalAmount().getAmount());
         }
         else
         {
            bookingInfo.updateDiscount(new Money(BigDecimal.ZERO, bookingComponent.getTotalAmount()
               .getCurrency()), PromotionalDiscountConstants.PROMOCODE, balanceType);
            bookingComponent.getNonPaymentData().put(PromotionalDiscountConstants.IS_PROMO_APPLIED,
               String.valueOf(false));
            String message = getErrorMessage(((PromotionalDiscountResponse) responseMessage)
               .getStatusCode(), bookingComponent, locale);
            LogWriter.logErrorMessage(message);
            throw new PromotionalDiscountServiceException(message);
         }
      }
      catch (PromotionalDiscountServiceException pcse)
      {
         logPromoCodeDetails(null, promotionalCode, bookingInfo.getTrackingData(),
             bookingComponent.getClientApplication().getClientApplicationName());
         bookingInfo.updateDiscount(new Money(BigDecimal.ZERO, bookingComponent.getTotalAmount()
            .getCurrency()), PromotionalDiscountConstants.PROMOCODE, balanceType);
         String message = pcse.getMessage();
         LogWriter.logErrorMessage(message, pcse);
         bookingComponent.setErrorMessage(message);
         response.getWriter().write(message + "|" + bookingInfo.getCalculatedTotalAmount().
            getAmount() + "|" + promotionalCode);
      }
   }

   /**
    * The doGet method. Receives an HTTP GET request and handles the
    * request by calling the doPost method.
    *
    * @param request the HttpServletRequest object that contains the
    *           request the client has made of the servlet.
    * @param response the HttpServletResponse object that contains the
    *           response the servlet sends to the client.
    *
    * @throws IOException the IO exception thrown if an input or output
    *            error is detected when the Servlet handles the request.
    * @throws ServletException the ServletException thrown if the request
    *            could not be handled.
    */
   protected void doGet(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException
   {
      doPost(request, response);
   }

   /**
    * This method creates PromotionalDiscountRequest with the passed in
    * promotionalCode and bookingComponent and returns the
    * promotionalDiscountRequest.
    *
    * @param bookingComponent the bookingComponent object has all the
    *           booking related information.
    * @param promotionalCode the promotionalCode.
    * @return TracsRequest the PromotionalDiscountRequest.
    * @throws PromotionalDiscountServiceException the Exception thrown when
    *         connection with TRACS fails..
    */
   private TracsRequest createPromotionalDiscountRequest(String promotionalCode,
      BookingComponent bookingComponent) throws PromotionalDiscountServiceException
   {
      PromotionalDiscountRequest promotionalCodeRequest =
         new PromotionalDiscountRequest(promotionalCode, bookingComponent.getTracsBookMinusId(),
         new AbtaDetails(bookingComponent.getAbtaDetails().getAbtaNumber(), bookingComponent
         .getAbtaDetails().getAbtaUserId(), bookingComponent.getAbtaDetails().getTracsWebSiteId()));
      promotionalCodeRequest.setTracsConnectionUrl(
         getShadowWebSeverUrl(bookingComponent.getPackageType().getCode(),
            bookingComponent.getClientApplication().getClientApplicationName()));
      promotionalCodeRequest.setTracsConnectionTimeOut(ConfReader.getIntEntry(
         "tracs.ConnectionTimeOut", PromotionalDiscountConstants.DEFAULT_TIMEOUT));
      return promotionalCodeRequest;
   }

   /**
    * Returns the url that will be used to communicate with TRACS.
    *
    * @param packageType the package type.
    * @param clientAppName the client application name.
    * @return url the Shadow web server url.
    */
   private String getShadowWebSeverUrl(String packageType, String clientAppName)
   {
      String promCodeServiceUrl = ConfReader.getConfEntry(
         "promcodeservice." + clientAppName + ".Url", null);
      String webSiteId = ConfReader.getConfEntry("promcodeservice.nonpdp.Url", null);
      if (StringUtils.isNotBlank(packageType)
               && StringUtils.equalsIgnoreCase(packageType, PromotionalDiscountConstants.PDP))
      {
         webSiteId =  ConfReader.getConfEntry("promcodeservice.pdp.Url", null);
      }
      promCodeServiceUrl = promCodeServiceUrl + webSiteId;
      if (StringUtils.isBlank(promCodeServiceUrl))
      {
         return ConfReader.getConfEntry("promcodeservice.default.Url", null) + webSiteId;
      }
      return promCodeServiceUrl;
   }

   /**
    * Method to validate Promo code.
    *
    * @param promoCode the promotional code.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private static boolean validatePromoCode(String promoCode)
   {
      if (promoCode != null)
      {
         if (promoCode.length() > 0
            && promoCode.length() <= PromotionalDiscountConstants.DEFAULT_PROM_CODE_LENGTH)
         {
            Matcher promoCodeMatcher = PROMOTIONALCODE.matcher(promoCode);
            return promoCodeMatcher.matches();
         }
         else if (promoCode.length() == PromotionalDiscountConstants.CRP_PROM_CODE_LENGTH)
         {
            Matcher promoCodeMatcher = PORTLAND_PROMOTIONALCODE.matcher(promoCode);
            return promoCodeMatcher.matches();
         }
      }
      return false;
   }
   /**
    * This method takes the statusCode,bookingComponent and locale as the input and returns the
    * corresponding error message.
    *
    * @param statusCode the status code.
    * @param bookingComponent the BookingComponent object.
    * @param locale the locale
    *
    * @return String the error message.
    */
   private String getErrorMessage(String statusCode,
               BookingComponent bookingComponent, Locale locale)
   {
      int errorCode = 0;
      if (StringUtils.isNotBlank(getErrorCode(statusCode)))
      {
         errorCode = Integer.parseInt(getErrorCode(statusCode));
      }
      String key = "";
      if (statusCode.contains("Error"))
      {
         key = getErrorMessageKey(errorCode);
      }
      else if (statusCode.contains("Warning"))
      {
         key = getWarningMessageKey(errorCode);
      }
      else
      {
         key = NOT_APPLICABLE;
      }
      String message = PropertyResource.getProperty(PROMO_CODE + bookingComponent.
         getClientApplication().getClientApplicationName() + key, MESSAGES_PROPERTY, locale);
      if (message == null)
      {
         message = PropertyResource.getProperty(PROMO_CODE + DEFAULT + key,
            MESSAGES_PROPERTY, locale);
      }
      return message;
   }
   /**
    * This method gets the status code and checks for the pattern and
    * replaces it with an empty string at the applicable places.
    *
    * @param statusCode the code to be replaced by pattern.
    *
    * @return code the stripped code.
    */
   private String getErrorCode(String statusCode)
   {
      return PromotionalDiscountConstants.PROMOTIONALCODEERROR_PATTERN
         .matcher(statusCode).replaceAll("");
   }

   /**
    * This method receives error code number as parameter and sends the
    * corresponding key.
    *
    * @param errorCode the integer value of the warning code.
    *
    * @return key corresponding to the error code.
    */
   private String getErrorMessageKey(int errorCode)
   {
      String key = "";
      switch (errorCode)
      {
         case PromotionalDiscountConstants.ERRORCODE_120:
            key = INVALID;
            break;
         case PromotionalDiscountConstants.ERRORCODE_121:
         case PromotionalDiscountConstants.ERRORCODE_123:
         case PromotionalDiscountConstants.ERRORCODE_124:
            key = NOT_APPLICABLE;
            break;
         case PromotionalDiscountConstants.ERRORCODE_122:
            key = EXPIRED;
            break;
         default:
            key = NOT_VALIDATED;
            break;
      }
      return key;
   }

   /**
    * This method receives error code number as parameter and sends the
    * corresponding key.
    *
    * @param errorCode the integer value of the error code.
    *
    * @return key the key corresponding to the error code.
    */
   private String getWarningMessageKey(int errorCode)
   {
      String key = "";
      switch (errorCode)
      {
         case PromotionalDiscountConstants.WARNINGCODE_ELEVEN:
            key = EXPIRED;
            break;
         case PromotionalDiscountConstants.WARNINGCODE_SIXTEEN:
            key = NOT_APPLICABLE;
            break;
         case PromotionalDiscountConstants.WARNINGCODE_125:
            key = ALREADY_USED;
            break;
         default:
            key = NOT_VALIDATED;
            break;
      }
      return key;
   }

   /**
    * This method logs the details like dataTime,token,sessionId,promoCode,
    * statusCode and description.
    *
    * @param promoDiscountResponse the promotional discount response.
    * @param promotionalCode the promotionalCode.
    * @param trackingData the transaction tracking data.
    * @param clientApplicationName the client application name.
    *
    */
   private static void logPromoCodeDetails(PromotionalDiscountResponse promoDiscountResponse,
          String promotionalCode, TransactionTrackingData trackingData,
                   String clientApplicationName)
   {
      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      String dateTime = dateFormat.format(date);
      StringBuilder loggingInfo = new StringBuilder();
      String statusCode = "";
      String description = "";
      if (promoDiscountResponse != null)
      {
         statusCode = promoDiscountResponse.getStatusCode();
         description = promoDiscountResponse.getDescription();
      }
      loggingInfo.append(PROMOCODE_SERVICE).append(COMMA)
                 .append(dateTime).append(COMMA)
                 .append(trackingData.getToken()).append(COMMA)
                 .append(trackingData.getSessionId()).append(COMMA)
                 .append(clientApplicationName).append(COMMA)
                 .append(promotionalCode).append(COMMA)
                 .append(statusCode).append(COMMA)
                 .append(description);
      LogWriter.logInfoMessage(loggingInfo.toString(), PromotionalCodeServlet.class.getName());
   }
}