package com.tui.uk.payment.dispatcher;

import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.payment.service.datacash.AunthicatePayeeResXml;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.payment.processor.FraudScreeningProcessor;
import com.tui.uk.payment.processor.FraudScreeningProcessorImpl;
import com.tui.uk.payment.processor.PostPaymentProcessor;
import com.tui.uk.payment.processor.PostPaymentProcessorFactory;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * This class handles Authenticate Payer request & response, displays challenge flow
 * or Frictionless Flow or error flow based on response status.
 * 
 * @author raghunandan.d
 *
 */
public class EMVAuthProcessServlet extends CommonPaymentServlet {

	private static final long serialVersionUID = 1L;

	private static final String UNKNOWN = "unknown";

	private static final String FALSE = "false";

	private static final String N = "N";

	private static final String Y = "Y";

	private static final String TEXT_CONTENT_TYPE = "text/html";

	private static final String UTF_ENCODING = "UTF-8";

	private static final String[] IP_HEADER_CANDIDATES = { "X-Forwarded-For",
			"Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_X_FORWARDED_FOR",
			"HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP",
			"HTTP_FORWARDED_FOR", "HTTP_FORWARDED", "HTTP_VIA", "REMOTE_ADDR" };
	
   private static final String DEF_IPV4_REGEX =
      "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." + "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
         + "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." + "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
   
   private static final Pattern IPV4_PATTERN = Pattern.compile(
      ConfReader.getConfEntry(DataCashServiceConstants.EMV_IP_ADDRESS_REGEX, DEF_IPV4_REGEX));

	private static final String EMV3DS_CHALLENGE_FLOW_CODE_LIST = "emv3ds.challenge.flow.codes";
	private static final String EMV3DS_FRICTIONLESS_CODE_LIST = "emv3ds.frictionless.codes";

	private static final String DEF_EMV3DS_CHALLENGE_CODE = "2525";
	private static final String DEF_EMV3DS_FRICTIONLESS_CODE = "2550";

	private static final String TEXT_CONTENT_TYPE_TEXT_PLAIN = "text/plain";

	private static final String FULL_SCREEN = "FULL_SCREEN";

	private static final String WIDTH_250_HEIGHT_400 = "250_X_400";

	private static final String WIDTH_390_HEIGHT_400 = "390_X_400";

	private static final String WIDTH_500_HEIGHT_400 = "500_X_600";

	private static final String WIDTH_600_HEIGHT_400 = "600_X_400";

	private static String EMV_3DS_AUTH_PAYER_ERROR = "emv.3ds.auth.payer.error.";

	private static final String GENERIC = "generic";

	private static final String EMV_AUTH_PAYER_GENERIC_ERROR_MSG = "<div class='tech-error'>"
			+ "<style>.tech-error{background:#d40e14;color:white;font-size:18px;font-family:arial;padding:2rem;margin:0 auto;max-width:760px;}</style>"
			+ "There's been a technical error submitting your card details. Please select another payment method<p style='display:none'>ERROR_MESSAGE</p></div>";

	private static final String BOOKING_COMPONENT = "bookingComponent";

	private static final String TWELVE_HUNDRED = "1200";

	private static final String EMV_3DS_AUTH_PAYER_ERROR_VALIDATION = "emv.3ds.auth.payer.error.validation";

	private static final String EMV_3DS_AUTH_PAYER_ERROR_DATACASH = "emv.3ds.auth.payer.error.datacash";

	private static final String EMV_3DS_AUTH_PAYER_ERROR_GENERIC = "emv.3ds.auth.payer.error.generic";

	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AunthicatePayeeResXml authenticationResponse = null;
		String token = request.getParameter(TOKEN);

		Map<String, String> browserDetails = populateBrowserDetails(request);

		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
				UUID.fromString(token));
		paymentData.getBookingInfo().setNewSetupReq(true);
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		verifySessionId(bookingInfo, request.getSession().getId());

		PostPaymentProcessor postPaymentProcessor = PostPaymentProcessorFactory
				.getPostPaymentProcessor(
						bookingComponent.getClientApplication(), paymentData,
						request.getParameterMap());

		String brand = request.getParameter("b");
		String tomcatInstance = request.getParameter("tomcat");

		try {

			if (paymentData.getTransactionStatus() == TransactionStatus.EMV_INITIATE_AUTH_COMPLETED) {
				LogWriter
						.logInfoMessage(" EMV 3DS Authenticate Payer started"
								+ " , token :" + token);
				postPaymentProcessor.preProcess();
				postPaymentProcessor.process();
				DataCashPaymentTransaction dataCashPaymentTransaction = getPaymentTransaction(paymentData);
				paymentData
						.setTransactionStatus(TransactionStatus.EMV_AUTH_INPROGRESS);
				authenticationResponse = dataCashPaymentTransaction
						.doEmvAuthentication(
								bookingComponent,
								token,
								getEmv3dsRedirectURL(request, brand,
										tomcatInstance), browserDetails);
				request.getSession().setAttribute(BOOKING_COMPONENT,
						bookingComponent);
			}
			processAuthenticationResponse(request, response,
					authenticationResponse, paymentData, bookingInfo,
					bookingComponent, postPaymentProcessor,
					getPaymentTransaction(paymentData));

		} catch (DataCashServiceException dse) {
			LogWriter.logErrorMessage("Error occured at Authentication :"
					+ dse.getMessage() + " , token=" + token);
			processHccPaymentException(request, response, paymentData);
			sendAjaxResponse(response, getEMVHccErrorMessage(dse));
		} catch (PostPaymentProcessorException ppe) {
			LogWriter
					.logErrorMessage("Error occured at Authentication population :"
							+ ppe.getMessage() + " , token=" + token);
			processHccPaymentException(request, response, paymentData);
			sendAjaxResponse(response, getEMVHccErrorMessage(ppe));
		} catch (PaymentValidationException pve) {
			LogWriter
					.logErrorMessage("Error occured at Authentication population :"
							+ pve.getMessage() + " , token=" + token);
			processHccPaymentException(request, response, paymentData);
			sendAjaxResponse(response, getEMVHccErrorMessage(pve));
		} catch (Exception ex) {
			LogWriter
					.logErrorMessage("Error occured at Authentication population - Cannot proceed after transaction is purged:"
							+ ex.getMessage() + " , token=" + token);
			sendAjaxResponse(response, getEMVHccErrorMessage(ex));
		}

	}

	/**
	 * Method returns DataCash Payment Transaction details.
	 * 
	 * @param paymentData
	 * @return DataCashPaymentTransaction
	 */
	private DataCashPaymentTransaction getPaymentTransaction(
			PaymentData paymentData) {
		if (paymentData != null && paymentData.getPayment() != null
				&& paymentData.getPayment().getPaymentTransactions() != null
				&& paymentData.getPayment().getPaymentTransactions().size() > 0) {
			return (DataCashPaymentTransaction) paymentData.getPayment()
					.getPaymentTransactions().get(0);
		}
		return null;

	}

	/**
	 * Method processes Authenticate Payer response & displays challenge flow or
	 * Frictionless Flow or error flow based on response status
	 * 
	 * @param request
	 * @param response
	 * @param authenticationResponse
	 * @param paymentData
	 * @param bookingInfo
	 * @param bookingComponent
	 * @param postPaymentProcessor
	 * @param dataCashPaymentTransaction
	 * @throws IOException
	 * @throws DataCashServiceException
	 */
	private void processAuthenticationResponse(HttpServletRequest request,
			HttpServletResponse response,
			AunthicatePayeeResXml authenticationResponse,
			PaymentData paymentData, BookingInfo bookingInfo,
			BookingComponent bookingComponent,
			PostPaymentProcessor postPaymentProcessor,
			DataCashPaymentTransaction dataCashPaymentTransaction)
			throws IOException, DataCashServiceException {
		List<String> challengeCodeList = Arrays.asList(StringUtils.split(
				ConfReader.getConfEntry(EMV3DS_CHALLENGE_FLOW_CODE_LIST,
						DEF_EMV3DS_CHALLENGE_CODE), ","));

		List<String> frictionlessCodeList = Arrays.asList(StringUtils.split(
				ConfReader.getConfEntry(EMV3DS_FRICTIONLESS_CODE_LIST,
						DEF_EMV3DS_FRICTIONLESS_CODE), ","));

		if (Objects.nonNull(authenticationResponse)
				&& Objects.nonNull(dataCashPaymentTransaction)
				&& (challengeCodeList.contains(authenticationResponse
						.getStatus()))
				&& paymentData.getTransactionStatus() == TransactionStatus.EMV_AUTH_INPROGRESS) {
			LogWriter
					.logInfoMessage(" EMV 3DS Challenge Flow in progress with status: "
							+ authenticationResponse.getStatus()
							+ " , token :"
							+ request.getParameter(TOKEN));
			updateTransactionDetails(authenticationResponse,
					dataCashPaymentTransaction);
			paymentData
					.setTransactionStatus(TransactionStatus.EMV_CHALLENGE_FLOW_INPROGRESS);
			response.setContentType(TEXT_CONTENT_TYPE);
			response.setCharacterEncoding(UTF_ENCODING);
			response.getWriter().write(
					authenticationResponse.getRedirect_html());
		} else if (Objects.nonNull(authenticationResponse)
				&& Objects.nonNull(dataCashPaymentTransaction)
				&& (frictionlessCodeList.contains(authenticationResponse
						.getStatus()))
				&& paymentData.getTransactionStatus() == TransactionStatus.EMV_AUTH_INPROGRESS) {
			LogWriter
					.logInfoMessage(" EMV 3DS Frictionless Flow in progress with status: "
							+ authenticationResponse.getStatus()
							+ " , token :"
							+ request.getParameter(TOKEN));
			updateTransactionDetails(authenticationResponse,
					dataCashPaymentTransaction);
			postPaymentProcessor.emv3DSPostProcess(
					bookingComponent.getPaymentGatewayVirtualTerminalId(),
					request.getParameter(TOKEN));
			paymentData
					.setTransactionStatus(TransactionStatus.EMV_FRICTIONLESS_FLOW_COMPLETED);
			response.setContentType(TEXT_CONTENT_TYPE);
			response.setCharacterEncoding(UTF_ENCODING);
			response.getWriter().write(
					authenticationResponse.getRedirect_html());
		} else {
			String responseStatus = StringUtils.EMPTY;
			if (authenticationResponse != null) {
				responseStatus = authenticationResponse.getStatus();
			}

			LogWriter
					.logInfoMessage("Authentication status verification failed and status ="
							+ responseStatus);
			if (StringUtils.isNotBlank(bookingInfo.getBookingComponent()
					.getPaymentFailureURL())) {
				paymentData.setFailureCount();
			}
			FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
					request.getParameter(TOKEN));
			fraudScreeningProcessor.updateFailureCardData();
			String defaultMsg = ConfReader.getConfEntry(
					EMV_3DS_AUTH_PAYER_ERROR + GENERIC,
					EMV_AUTH_PAYER_GENERIC_ERROR_MSG);
			String errorMessage = ConfReader.getConfEntry(
					EMV_3DS_AUTH_PAYER_ERROR + responseStatus, defaultMsg);
			LogWriter.logErrorMessage(errorMessage);
			sendAjaxResponse(response, errorMessage);
		}
	}

	/**
	 * Method updates Transaction details.
	 * 
	 * @param authenticationResponse
	 * @param dataCashPaymentTransaction
	 */
	private void updateTransactionDetails(
			AunthicatePayeeResXml authenticationResponse,
			DataCashPaymentTransaction dataCashPaymentTransaction) {
		dataCashPaymentTransaction.setDatacashReference(StringUtils
				.defaultString(authenticationResponse.getDatacash_reference()));
		
      if (!ConfReader.getBooleanEntry(DataCashServiceConstants.EMV_MERCH_REF_ENABLED, true))
      {
         dataCashPaymentTransaction.setMerchantReference(
            StringUtils.defaultString(authenticationResponse.getMerchantreference()));

      }
		
		dataCashPaymentTransaction.setStatus(StringUtils
				.defaultString(authenticationResponse.getStatus()));
		dataCashPaymentTransaction.setReason(StringUtils
				.defaultString(authenticationResponse.getReason()));
		dataCashPaymentTransaction.setEmvStausCode(authenticationResponse
				.getStatus());
	}

	/**
	 * Method returns browser details as Map using request header and request
	 * parameter.
	 * 
	 * @param request
	 * @return Map<String, String>
	 */
	private Map<String, String> populateBrowserDetails(
			HttpServletRequest request) {
		Map<String, String> browserDetails = new HashMap<String, String>();

		String browserLocale = request
				.getHeader(DataCashServiceConstants.BROWSER_ACCEPT_LANGUAGE);
		Locale locale = new Locale(StringUtils.substringBefore(browserLocale,
				DataCashServiceConstants.HYPHEN),
				StringUtils.substringAfterLast(browserLocale,
						DataCashServiceConstants.HYPHEN));
		String language = StringUtils.upperCase(locale.getLanguage());

		String userAgent = request
				.getHeader(DataCashServiceConstants.BROWSER_USER_AGENT);

		String acceptHeader = request
				.getHeader(DataCashServiceConstants.BROWSER_ACCEPT);

		String remoteAddress = getClientIpAddress(request);

		browserDetails
				.put(DataCashServiceConstants.BROWSER_CHALLENGE_WINDOW_SIZE,
						getChallengeWindowSize(request
								.getParameter(DataCashServiceConstants.BROWSER_SCREEN_WIDTH)));
		browserDetails.put(DataCashServiceConstants.BROWSER_ACCEPT_HEADER,
				acceptHeader);
		browserDetails
				.put(DataCashServiceConstants.BROWSER_COLOUR_DEPTH,
						request.getParameter(DataCashServiceConstants.BROWSER_COLOUR_DEPTH));
		String javaEnabled = N;
		if (!StringUtils.equalsIgnoreCase(request
				.getParameter(DataCashServiceConstants.BROWSER_JAVA_ENABLED),
				FALSE)) {
			javaEnabled = Y;
		}
		browserDetails.put(DataCashServiceConstants.BROWSER_JAVA_ENABLED,
				javaEnabled);
		browserDetails.put(DataCashServiceConstants.BROWSER_LANGUAGE, language);
		browserDetails
				.put(DataCashServiceConstants.BROWSER_SCREEN_HEIGHT,
						request.getParameter(DataCashServiceConstants.BROWSER_SCREEN_HEIGHT));
		browserDetails
				.put(DataCashServiceConstants.BROWSER_SCREEN_WIDTH,
						request.getParameter(DataCashServiceConstants.BROWSER_SCREEN_WIDTH));

		browserDetails.put(DataCashServiceConstants.BROWSER_TIME_ZONE, request
				.getParameter(DataCashServiceConstants.BROWSER_TIME_ZONE));
		browserDetails.put(DataCashServiceConstants.BROWSER, userAgent);
		
      if (ConfReader.getBooleanEntry(DataCashServiceConstants.EMV_IP_ADDRESS_ENABLED, false))
      {
         LogWriter.logInfoMessage("IP address is enabled for EMV3DS");
         Matcher matcher = IPV4_PATTERN.matcher(remoteAddress);
         if (matcher.matches())
         {
            browserDetails.put(DataCashServiceConstants.REMOTEADDRESS, remoteAddress);
            LogWriter.logInfoMessage("Valid IPV4 address");
         }
         else
         {
            browserDetails.put(DataCashServiceConstants.REMOTEADDRESS, StringUtils.EMPTY);
            LogWriter.logInfoMessage("Invalid IPV4/IPV6 address");
         }

      }
      else
      {
         LogWriter.logInfoMessage("IP address is disabled for EMV3DS");
         browserDetails.put(DataCashServiceConstants.REMOTEADDRESS, StringUtils.EMPTY);
      }
		
		
		return browserDetails;
	}

	/**
	 * Returns Authenticate Payer Challenge IFrame supported window size using
	 * browserWidth parameter.
	 * 
	 * @param browserWidth
	 * @return String
	 */
	private String getChallengeWindowSize(String browserWidth) {
		String challengeWindowSize = FULL_SCREEN;
		Integer width = Integer.parseInt(StringUtils.defaultIfEmpty(
				browserWidth, TWELVE_HUNDRED));
		if (width <= 390) {
			challengeWindowSize = WIDTH_250_HEIGHT_400;
		} else if (width <= 500) {
			challengeWindowSize = WIDTH_390_HEIGHT_400;
		} else if (width <= 600) {
			challengeWindowSize = WIDTH_500_HEIGHT_400;
		} else if (width <= 768) {
			challengeWindowSize = WIDTH_600_HEIGHT_400;
		}
		return challengeWindowSize;
	}

	/**
	 * Returns EMV 3DS redirect URL.
	 * 
	 * @param request
	 * @param brand
	 * @param tomcatInstance
	 * @return String
	 */
	private String getEmv3dsRedirectURL(HttpServletRequest request,
			String brand, String tomcatInstance) {

		final String COLON = ":";
		String token = request.getParameter(DispatcherConstants.TOKEN);
		StringBuffer strBuffer = new StringBuffer(ConfReader.getConfEntry(
				"hcc.protocol", "https") + "://");
		strBuffer
				.append(request.getServerName())
				.append(Boolean.valueOf(ConfReader.getConfEntry(
						"hcc.dev.environment", "false")) ? COLON
						+ ConfReader.getConfEntry("hcc.dev.environment.port",
								"9090") : "").append(request.getContextPath())
				.append("/postauthprocess?b=").append(brand).append("&token=")
				.append(token).append("&tomcat=").append(tomcatInstance);
		return strBuffer.toString();

	}

	/**
	 * Method will return IP address using client request
	 * 
	 * @param request
	 * @return String
	 */
	private static String getClientIpAddress(HttpServletRequest request) {

		for (String header : IP_HEADER_CANDIDATES) {
			String ip = request.getHeader(header);
			if (ip != null && ip.length() != 0 && !UNKNOWN.equalsIgnoreCase(ip)) {
			   LogWriter.logDebugMessage("Client IP from request header: "+ip);
				return ip;
			}
		}
		LogWriter.logDebugMessage("Client IP from request remote address: "+request.getRemoteAddr());
		return request.getRemoteAddr();
	}

	/**
	 * Updating failure count for re-processing.
	 * 
	 * @param request
	 * @param response
	 * @param paymentData
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void processHccPaymentException(HttpServletRequest request,
			HttpServletResponse response, PaymentData paymentData)
			throws ServletException, IOException {
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		String paymentFailureUrl = bookingInfo.getBookingComponent()
				.getPaymentFailureURL();
		if (StringUtils.isNotBlank(paymentFailureUrl)) {
			paymentData.setFailureCount();
		}
		FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
				request.getParameter(TOKEN));
		fraudScreeningProcessor.updateFailureCardData();
	}

	/**
	 * Populate configured error message based on type of exception.
	 * 
	 * @param exception
	 * @return String
	 */
	private String getEMVHccErrorMessage(Exception exception) {
		String errorMsg = StringUtils.EMPTY;
		if (exception instanceof PostPaymentProcessorException) {
			errorMsg = ConfReader
					.getConfEntry(EMV_3DS_AUTH_PAYER_ERROR_VALIDATION,
							exception.getMessage());

		} else if (exception instanceof PaymentValidationException) {
			errorMsg = ConfReader.getConfEntry(
					EMV_3DS_AUTH_PAYER_ERROR_DATACASH, exception.getMessage());
		} else {
			errorMsg = ConfReader.getConfEntry(
					EMV_3DS_AUTH_PAYER_ERROR_GENERIC, exception.getMessage());
		}
		return errorMsg;
	}

	/**
	 * This method is used as a common method to send the AJAX response the UI.
	 *
	 * @param response
	 *            the HttpServletResponse object
	 * @param out
	 *            the AJAX output
	 *
	 */
	private void sendAjaxResponse(HttpServletResponse response, String output)
			throws IOException {
		response.setContentType(TEXT_CONTENT_TYPE_TEXT_PLAIN);
		response.setCharacterEncoding(UTF_ENCODING);
		response.getWriter().write(output);
	}

}
