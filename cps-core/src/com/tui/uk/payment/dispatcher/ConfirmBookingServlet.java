/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ConfirmBookingServlet.java$
 *
 * $Revision: $
 *
 * $Date: Jun 14, 2008$
 *
 * Author: Vinothkumar.k
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentStore;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.BOOKING_INFO;

/**
 * The Class ConfirmBookingServlet. This class is used to get the value of
 * the booked holiday and return it back to web layer again. This
 * class will be called when user confirms the booking and returns back to
 * payment page.
 * It is added mainly for IE6, as IE6 renders page from cache when browser back button click,
 * even though we are removing cache explicitly when page loads.
 *
 * @author vinothkumar.k
 */
@SuppressWarnings("serial")
public final class ConfirmBookingServlet extends HttpServlet
{
   /**
    * This method reads the newHoliday and checks whether to
    * display the confirm booking button or not.
    *
    * @param request the HttpServletRequest object that contains the request the client has
    * made of the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet
    * sends to the client.
    *
    * @throws IOException the IOException thrown if an input or output error is detected when the
    * servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the GET could
    * not be handled.
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
   {
      String token = request.getParameter(TOKEN);
      BookingInfo bookingInfo = null;
      if (StringUtils.isNotBlank(token))
      {
         bookingInfo = PaymentStore.getInstance().getPaymentData(
                          UUID.fromString(token)).getBookingInfo();
      }
      else
      {
         bookingInfo = (BookingInfo) request.getSession().getAttribute(BOOKING_INFO);
      }
      String newHoliday = bookingInfo.getNewHoliday().toString();
      LogWriter.logInfoMessage("Retrieved new holiday attribute:" + newHoliday);
      response.getWriter().write(newHoliday);
   }

   /**
    * Receives an HTTP POST request and handles the request by calling the doGet method.
    *
    * @param request the HttpServletRequest object that contains the request the client has
    * made of the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet
    * sends to the client.
    *
    * @throws IOException the IO exception thrown if an input or output error is detected when the
    * Servlet handles the request.
    * @throws ServletException the ServletException thrown if the request could not be handled.
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
   {
      doGet(request, response);
   }

}
