package com.tui.uk.payment.dispatcher;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.HYPHEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH_ACSS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CLIENT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HOST_NAME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PASSWORD;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;

import com.datacash.client.Agent;
import com.datacash.errors.FailureReport;
import com.datacash.logging.Logger;
import com.datacash.util.XMLDocument;
import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.client.domain.Discount;
import com.tui.uk.client.domain.InitiateAuthResponse;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;

public class HccProcessServlet extends HttpServlet
{

   private static final String DEF_EMV_3DS_STATUS = "2500";

   private static final String EMV3DS_ENABLED = ".emv3ds.enabled";

   private static final long serialVersionUID = 1L;

   private static final String DATACASH_CONNECTION_TIMEOUT = "datacash.ConnectionTimeOut";

   private static final int DEFAULT_TIMEOUT = 60000;

   private static final String DATACASH_ERROR = "datacash.generic.error";

   private static final String NEXT_LINE = "\n";

   /** The Text/plain content type. */
   private static final String HTML_CONTENT_TYPE = "text/html";

   /** The UTF-8 encoding. */
   private static final String UTF_ENCODING = "UTF-8";

   private static final String ADDRESS1_REGEX =
      "(?=^.{1,32}$)^[a-zA-Z0-9]+([\\-a-zA-Z0-9,&\\(\\)\\'\\u2018\\u2019\\.\\/\\s]*)+$";

   private static final String NAME_REGEX =
      "(?=^.{1,32}$)^[a-zA-Z0-9]+([\\-a-zA-Z0-9,&\\(\\)\\'\\u2018\\u2019\\.\\/\\s]*)+$";

   private static final String HOUSENUM_REGX =
      "(?=^.{1,32}$)^[a-zA-Z0-9]+([\\-a-zA-Z0-9,&\\(\\)\\'\\u2018\\u2019\\.\\/\\s]*)+$";

   private static final String GB_POSTCODE_REGEX =
      "(?=^.{1,8}$)^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkmnprstuvwyxA-HJKSNMPRVTUYWX])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))\\s*$";

   private static final String GB_TOWN_REGEX = "(?=^.{1,25}$)^[a-zA-Z\\'\\., \\-\\s*]+$";

   private static final String IE_POSTCODE_REGEX =
      "(?=^.{1,8}$)^[a-zA-Z0-9]+(?:\\s[a-zA-Z0-9]+)*$|^$";

   private static final String CURRENCY = "CURRENCY";

   /** The comma separator. */
   private static final String COMMA_SEPARATOR = ",";

   /** The dot separator. */
   private static final String DOT_SEPARATOR = ".";

   private static final String PERSONAL_DETAILS = "personaldetails_";

   private static final String EMV3DS_ENABLED_CODE_LIST = "emv3ds.supported.status.codes";
   
   private static final String EMV3DS_FALLBACK_ENABLED_CODE_LIST = "emv.3ds.fallback.status.codes";
   
   private static final String EMV_3DS_INIT_AUTH_ERROR = "emv.3ds.init.auth.error.";
   
   private static final String GENERIC = "generic";
   
   private static final String GENERIC_MSG = "Looks like something has gone wrong and we're unable "
   		+ "to take payment from this card right now. Please select another payment method from the "
   		+ "options above or refresh the page to pay with another card.";
   
   private static final String EMV_3DS_INIT_AUTH_WAIT_TIME = "emv.3ds.init.auth.wait.time";
   
   private static final String WAIT_TIME = "waitTime";
   
   private static final String IA_HTML_CONTENT = "iaHtmlContent";
   
   private static final String ERROR_MESSAGE = "errorMessage";
   
   private static final Integer EIGHT_THOUSAND = 8000;
   
   
   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {

      String token = request.getParameter(TOKEN);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
      paymentData.getBookingInfo().setNewSetupReq(true);
      BookingInfo bookingInfo = paymentData.getBookingInfo();
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      Map<String, String> hcsMap = bookingComponent.getHccsMap();
      String hpsDatacashReference = hcsMap.get(DataCashServiceConstants.HCC_DATACASH_RS_KEY);
      Map<String, String> hccQueryMap = null;
      try
      {
         hccQueryMap = getHccQueryXml(hpsDatacashReference, bookingInfo);
      }
      catch (PaymentValidationException pve)
      {
         String errorMsg = ConfReader.getConfEntry("hcc.datacash.error", pve.getMessage());
         sendAjaxResponse(response, errorMsg);
         return;
      }
      catch (Exception exception)
      {
         String errorMsg = ConfReader.getConfEntry("hcc.generic.error", exception.getMessage());
         sendAjaxResponse(response, errorMsg);
         return;
      }

      String statusCode = hccQueryMap.get(DataCashServiceConstants.STATUS_RS);
      String captureStatus = hccQueryMap.get(DataCashServiceConstants.HCC_CAPTURE_STATUS_KEY);

      hcsMap.put(DataCashServiceConstants.HCC_CAPTURE_STATUS_KEY, captureStatus);
      request.setAttribute(DataCashServiceConstants.HCC_CAPTURE_STATUS_KEY, captureStatus);

      // Handling -ve scenario on query transaction and field Validation.
		if (captureStatus.equals(DataCashServiceConstants.STATUS_POPULATED) && statusCode.equals("1")) {
			if (!validateAddressAndName(bookingComponent.getHccCapturedData(),
					bookingComponent.getClientApplication().getClientApplicationName())) {
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/common/hccValidationError.jsp?b="
						+ request.getParameter("b") + "&tomcat=" + request.getParameter("tomcat") + "");
				requestDispatcher.forward(request, response);
			} else {
				createHccNonPaymentData(bookingComponent);
				updateHccBookingComponent(bookingComponent);
				//EMV 3DS flow entry point - Initiate Authentication call
				processEMV3DS(request, response, bookingComponent, hcsMap, token);
				
            RequestDispatcher requestDispatcher =
               request.getRequestDispatcher("/common/hcsprocess.jsp?b=" + request.getParameter("b")
                  + "&tomcat=" + request.getParameter("tomcat") + "");

            requestDispatcher.forward(request, response);
			}

		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/common/hccError.jsp?b="
					+ request.getParameter("b") + "&tomcat=" + request.getParameter("tomcat") + "");
			requestDispatcher.forward(request, response);

		}

   }

 
	/**
	 * Method handles EMV 3DS Initiate Authentication call & response
	 * processing.
	 * 
	 * @param request
	 * @param response
	 * @param bookingComponent
	 * @param hcsMap
	 * @param token
	 * @throws IOException
	 * @throws ServletException
	 */
	private void processEMV3DS(HttpServletRequest request,
			HttpServletResponse response, BookingComponent bookingComponent,
			Map<String, String> hcsMap, String token) throws IOException,
			ServletException {
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
				UUID.fromString(token));
		String emv3dsEnabled = ConfReader.getConfEntry(bookingComponent
				.getClientApplication().getClientApplicationName()
				+ EMV3DS_ENABLED, "false");
		LogWriter.logInfoMessage(" Emv3ds envabled for client :"
				+ bookingComponent.getClientApplication()
						.getClientApplicationName() + EMV3DS_ENABLED + ":"
				+ emv3dsEnabled + " , token :" + token);
		if (StringUtils.equalsIgnoreCase(emv3dsEnabled, "true")) {
			paymentData
					.setTransactionStatus(TransactionStatus.EMV_INITIATE_AUTH_IN_PROGRESS);
			InitiateAuthResponse intiateAuthResponse = new InitiateAuthResponse(
					getInitiateAuthenticationRequest(bookingComponent, hcsMap,
							token));
			processInitAuthenticateStatus(bookingComponent,
					intiateAuthResponse, request, response, token);
		}
	}

	/**
	 * Method handles Initiate Authentication response.
	 * 
	 * @param bookingComponent
	 * @param initiateAuthResponse
	 * @param request
	 * @param response
	 * @param token
	 * @throws IOException
	 * @throws ServletException
	 */
	private void processInitAuthenticateStatus(
			BookingComponent bookingComponent,
			InitiateAuthResponse initiateAuthResponse,
			HttpServletRequest request, HttpServletResponse response,
			String token) throws IOException, ServletException {
		List<String> emvFallbackCodeList = Arrays.asList(StringUtils.split(
				ConfReader.getConfEntry(EMV3DS_FALLBACK_ENABLED_CODE_LIST,
						StringUtils.EMPTY), ","));

		List<String> emvSupportedList = Arrays.asList(StringUtils.split(
				ConfReader.getConfEntry(EMV3DS_ENABLED_CODE_LIST,
						DEF_EMV_3DS_STATUS), ","));

		LogWriter.logInfoMessage(" EMV3DS Supported codes :"
				+ ConfReader.getConfEntry(EMV3DS_ENABLED_CODE_LIST,
						StringUtils.EMPTY) + ":" + " , token :" + token);

		LogWriter.logInfoMessage(" EMV3DS Fallback list :"
				+ ConfReader.getConfEntry(EMV3DS_FALLBACK_ENABLED_CODE_LIST,
						StringUtils.EMPTY) + ":" + " , token :" + token);

		boolean isEmvEnabled = emvSupportedList.contains(StringUtils
				.defaultString(initiateAuthResponse.getStatus()));

		if (isEmvEnabled) {
			LogWriter.logInfoMessage(" EMV 3DS is supported with status : "
					+ initiateAuthResponse.getStatus() + " , token :" + token);
			PaymentStore
					.getInstance()
					.getPaymentData(UUID.fromString(token))
					.setTransactionStatus(
							TransactionStatus.EMV_INITIATE_AUTH_COMPLETED);
			request.setAttribute(IA_HTML_CONTENT,
					initiateAuthResponse.getRedirectHtml());
			request.getSession().setAttribute(WAIT_TIME,
					ConfReader.getIntEntry(EMV_3DS_INIT_AUTH_WAIT_TIME, EIGHT_THOUSAND));
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("/common/emv3dsprocess.jsp?b="
							+ request.getParameter("b") + "&tomcat="
							+ request.getParameter("tomcat") + "");
			requestDispatcher.forward(request, response);
		} else if (!isEmvEnabled
				&& emvFallbackCodeList.contains(StringUtils
						.defaultString(initiateAuthResponse.getStatus()))) {
			LogWriter.logInfoMessage(" EMV 3DS not supported with status : "
					+ initiateAuthResponse.getStatus()
					+ " fallback to 3DS1, token :" + token);
			PaymentStore
					.getInstance()
					.getPaymentData(UUID.fromString(token))
					.setTransactionStatus(
							TransactionStatus.EMV_INITIATE_AUTH_FALLBACK);
			bookingComponent
					.setMerchantReference(getMerchantReference(bookingComponent));
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("/common/hcsprocess.jsp?b="
							+ request.getParameter("b") + "&tomcat="
							+ request.getParameter("tomcat") + "");

			requestDispatcher.forward(request, response);
		} else {
			LogWriter.logInfoMessage(" EMV 3DS not supported with status : "
					+ initiateAuthResponse.getStatus()
					+ " display error, token :" + token);
			PaymentStore
					.getInstance()
					.getPaymentData(UUID.fromString(token))
					.setTransactionStatus(
							TransactionStatus.EMV_INITIATE_AUTH_FAILED);

			String defaultMsg = ConfReader.getConfEntry(EMV_3DS_INIT_AUTH_ERROR
					+ GENERIC, GENERIC_MSG);
			String errorMessage = ConfReader.getConfEntry(
					EMV_3DS_INIT_AUTH_ERROR + initiateAuthResponse.getStatus(),
					defaultMsg);
			request.setAttribute(ERROR_MESSAGE, errorMessage);
			LogWriter.logErrorMessage(errorMessage);

			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("/common/emvError.jsp?b="
							+ request.getParameter("b") + "&tomcat="
							+ request.getParameter("tomcat") + "");

			requestDispatcher.forward(request, response);
		}

	}

	/**
	 * Method makes XML-RPC call to DataCash with Initiate Authentication method
	 * & returns XML response.
	 * 
	 * @param bookingComponent
	 * @param hcsMap
	 * @param token
	 * @return XMLDocument
	 */
	private XMLDocument getInitiateAuthenticationRequest(
			BookingComponent bookingComponent, Map<String, String> hcsMap,
			String token) {
		XMLDocument emvResponse = null;
		XMLDocument emvXmlDocument = null;
		String vTid = null;

		try {
			emvXmlDocument = new XMLDocument();
			vTid = bookingComponent.getPaymentGatewayVirtualTerminalId();
			String password = ConfReader.getConfEntry(vTid, null);
			setAuthentication(emvXmlDocument, vTid, password);

			Hashtable<String, String> currency = new Hashtable<String, String>();
			currency.put(DataCashServiceConstants.EMV_3DS_IA_REQ_CURRENCY,
					bookingComponent.getPayableAmount().getCurrency()
							.getCurrencyCode());
			emvXmlDocument.set(DataCashServiceConstants.EMV_3DS_IA_REQ_AMOUNT,
			   emv3dsTotalAmount(bookingComponent),
					currency);

			emvXmlDocument.set(
					DataCashServiceConstants.EMV_3DS_IA_REQ_CAPTUREMETHOD,
					DataCashServiceConstants.EMV_3DS_IA_REQ_ECOMM);

			emvXmlDocument
					.set(DataCashServiceConstants.EMV_3DS_IA_REQ_METHOD,
							DataCashServiceConstants.EMV_3DS_IA_REQ_INITIATE_AUTHENTICATION);
			emvXmlDocument
					.set(DataCashServiceConstants.EMV_3DS_IA_REQ_AUTHENTICATION_CHANNEL,
							DataCashServiceConstants.EMV_3DS_IA_REQ_PAYER_BROWSER);
			emvXmlDocument
					.set(DataCashServiceConstants.EMV_3DS_IA_REQ_PAYER_PURPOSE,
							DataCashServiceConstants.EMV_3DS_IA_REQ_PAYMENT_TRANSACTION);
			String hpsDCR = hcsMap
					.get(DataCashServiceConstants.HCC_DATACASH_RS_KEY);

			Hashtable<String, String> type = new Hashtable<String, String>();
			type.put(
					DataCashServiceConstants.EMV_3DS_IA_REQ_CARD_DETAILS_TYPE,
					DataCashServiceConstants.EMV_3DS_IA_REQ_CARD_DETAILS_FROM_HPS);
			emvXmlDocument.set(
					DataCashServiceConstants.EMV_3DS_IA_REQ_CARD_DETAILS,
					hpsDCR, type);

			LogWriter.logDebugMessage(emvXmlDocument.getSanitizedDoc());
			emvResponse = getAgent().request(emvXmlDocument, new Logger());
			LogWriter.logDebugMessage(emvResponse.getSanitizedDoc());
			LogWriter
					.logInfoMessage(" Intiate Authenticate Response :"
							+" datacash_reference="
							+ emvResponse
									.get(DataCashServiceConstants.EMV_DATACASH_REFERENCE)
							+ " , status="
							+ emvResponse.get(DataCashServiceConstants.STATUS)
							+ ", reason="
							+ emvResponse
									.get(DataCashServiceConstants.EMV_INITIATE_AUTH_REASON)
							+ ", information="
							+ emvResponse
									.get(DataCashServiceConstants.EMV_INITIATE_AUTH_INFO));
		} catch (FailureReport e) {
			LogWriter
					.logErrorMessage("Error occured at datacash for Initiate Authentiate request  :"
							+ e.getMessage() + " ,token=" + token);
			handleFailureReport(e, vTid);
		} catch (JDOMException ex) {
			LogWriter
					.logErrorMessage("Error occured at creating the xmlDocument object :"
							+ ex.getMessage() + " ,token=" + token);
			LogWriter.logErrorMessage(ex.getMessage(), ex);

		} catch (Exception ex) {
			LogWriter
					.logErrorMessage("Error at Initiate authenticate request :"
							+ ex.getMessage() + " ,token=" + token);
			LogWriter.logErrorMessage(ex.getMessage(), ex);
		}
		return emvResponse;
	}
	
   private boolean validateAddressAndName(Map<String, String> hccQueryMap, String clientApp)
   {
      String prefix = PaymentConstants.PAYMENT + 0;
      List<String> errorFields = new ArrayList<String>();
      {
         validateInputField(hccQueryMap.get(prefix + PaymentConstants.NAME_ON_CARD), NAME_REGEX,
            errorFields, "NAME_ON_CARD");
         validateInputField(hccQueryMap.get(prefix + PaymentConstants.HOUSENO), HOUSENUM_REGX,
            errorFields, "HOUSENO");
         if ("GB".equalsIgnoreCase(hccQueryMap.get(prefix + PaymentConstants.SELECTED_COUNTRY_CODE))
            && !isIESupportedApplication(clientApp))
         {
            validateInputField(hccQueryMap.get(prefix + PaymentConstants.TOWN), GB_TOWN_REGEX,
               errorFields, "TOWN");
            validateInputField(hccQueryMap.get(prefix + PaymentConstants.POST_CODE),
               GB_POSTCODE_REGEX, errorFields, "POSTCODE");
         }
         else if ("IE"
            .equalsIgnoreCase(hccQueryMap.get(prefix + PaymentConstants.SELECTED_COUNTRY_CODE))
            && isIESupportedApplication(clientApp))
         {
            if (Objects.isNull(hccQueryMap.get(prefix + PaymentConstants.POST_CODE))
               && Objects.isNull(hccQueryMap.get(prefix + PaymentConstants.STREETADDRESS)))
            {
               errorFields.add("STREET_OR_POST");
            }
            else
            {
               if (!Objects.isNull(hccQueryMap.get(prefix + PaymentConstants.STREETADDRESS)))
               {
                  validateInputField(hccQueryMap.get(prefix + PaymentConstants.STREETADDRESS),
                     ADDRESS1_REGEX, errorFields, "STREET_ADDRESS");
               }
               if (!Objects.isNull(hccQueryMap.get(prefix + PaymentConstants.POST_CODE)))
               {
                  validateInputField(hccQueryMap.get(prefix + PaymentConstants.POST_CODE),
                     IE_POSTCODE_REGEX, errorFields, "POST_CODE");
               }
            }
         }
      }
      LogWriter.logDebugMessage("Validation Errors : " + errorFields);
      return errorFields.isEmpty();
   }

   public void validateInputField(final String fieldValue, String pattern, List<String> errors,
      String field)
   {
      if (isNotValidField(fieldValue, pattern))
      {
         errors.add(field);
      }
   }

   /**
    * Validate field.
    *
    * @param field the field
    * @param regEx the reg ex
    * @return true, if checks if is not valid field
    */

   private boolean isNotValidField(final String field, final String regEx)
   {
      if (Objects.nonNull(field) && Pattern.matches(regEx, decodeValue(field)))
      {
         return false;
      }
      return true;
   }

   /**
    * @param bookingComponent
    * @return
    */
   private boolean isIESupportedApplication(String clientApp)
   {
      boolean flag = false;
      if (StringUtils.equalsIgnoreCase("TUIFALCON", clientApp)
         || StringUtils.equalsIgnoreCase("TUIFALCONFO", clientApp)
         || StringUtils.equalsIgnoreCase("ANCFALCON", clientApp)
         || StringUtils.equalsIgnoreCase("ANCFJFO", clientApp))
      {
         flag = true;
      }
      return flag;
   }

   private Map<String, String> getHccQueryXml(String hpsDatacashReference,
      BookingInfo bookingInfo) throws PaymentValidationException
   {
      Map<String, String> hccsmap = null;
      BookingComponent bookingComponent=bookingInfo.getBookingComponent();

      XMLDocument xmlDocument = null;
      String queryStatus = null;
      String vTid = bookingComponent.getPaymentGatewayVirtualTerminalId();
      String password = ConfReader.getConfEntry(vTid, null);
      String prefix = PaymentConstants.PAYMENT + 0;
      try
      {
         xmlDocument = new XMLDocument();
         xmlDocument.set(DataCashServiceConstants.CLIENT, vTid);
         xmlDocument.set(DataCashServiceConstants.PASSWORD, password);
         xmlDocument.set(DataCashServiceConstants.DATACASH_REFERENCE, hpsDatacashReference);
         xmlDocument.set(DataCashServiceConstants.TRANSACT_TYPE, "query");

         LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
         XMLDocument response = null;
         try
         {
            long startTime = System.currentTimeMillis();
            response = getAgent().request(xmlDocument, new Logger());
            long elapsedTime = System.currentTimeMillis() - startTime;
            LogWriter.logDebugMessage("Time taken for HCC query transaction : " + elapsedTime);

            queryStatus = response.get(DataCashServiceConstants.CAPTURE_STATUS);
            Map<String, String> hccCapturedData = bookingComponent.getHccCapturedData();
            if(hccCapturedData==null && isDotNetClient(bookingComponent)){
				hccCapturedData=getHccCapturedDataMap(bookingInfo);
			}
            if (hccCapturedData == null)
            {
               hccCapturedData = getHccCapturedData(bookingComponent);
            }
            // Map<String, String> hccCapturedData = new HashMap<String, String>();
            hccCapturedData.put(prefix + PaymentConstants.NAME_ON_CARD,
               decodeValue(response.get(DataCashServiceConstants.DYNAMIC_CAPF1)));
            hccCapturedData.put(prefix + PaymentConstants.SELECTED_COUNTRY_CODE,
               response.get(DataCashServiceConstants.DYNAMIC_CAPF2));
            hccCapturedData.put(prefix + PaymentConstants.HOUSENO,
               decodeValue(response.get(DataCashServiceConstants.DYNAMIC_CAPF3)));
            hccCapturedData.put(prefix + PaymentConstants.STREETADDRESS,
               decodeValue(response.get(DataCashServiceConstants.DYNAMIC_CAPF4)));
            hccCapturedData.put(DataCashServiceConstants.STREET_LINE2_KEY,
               decodeValue(response.get(DataCashServiceConstants.DYNAMIC_CAPF5)));
            hccCapturedData.put(prefix + PaymentConstants.TOWN,
               decodeValue(response.get(DataCashServiceConstants.DYNAMIC_CAPF6)));
            hccCapturedData.put(prefix + PaymentConstants.POST_CODE,
               decodeValue(response.get(DataCashServiceConstants.DYNAMIC_CAPF7)));
            String cardScheme = transform(response.get(DataCashServiceConstants.CARD_SCHEME),
               response.get(DataCashServiceConstants.HCC_PAN));
            if (!isCardSchemaSupported(cardScheme, bookingComponent.getClientApplication(),
               hccCapturedData.get(CURRENCY)))
            {
               LogWriter.logDebugMessage("Card Not Supported : " + cardScheme);
               throw new PaymentValidationException("Card Not Supported");
            }
            hccCapturedData.put(prefix + PaymentConstants.PAYMENT_TYPE_CODE, cardScheme);
            hccCapturedData.put(prefix + PaymentConstants.TYPE,
               response.get(DataCashServiceConstants.CARD_SCHEME));
            // hccCapturedData.put(DataCashServiceConstants.CARD_SCHEME_KEY,
            // response.get(DataCashServiceConstants.CARD_SCHEME));
            hccCapturedData.put(prefix + PaymentConstants.SELECTED_COUNTRY,
               response.get(DataCashServiceConstants.DYNAMIC_CAPF2));
            hccCapturedData.put(prefix + PaymentConstants.ISSUER_COUNTRY,
               response.get(DataCashServiceConstants.CAPTURED_COUNTRY));

            hccCapturedData.put(prefix + PaymentConstants.EXPIRY_MONTH,
               response.get(DataCashServiceConstants.CAPTURED_EXP_DATE).substring(0, 2));
            hccCapturedData.put(prefix + PaymentConstants.EXPIRY_YEAR,
               response.get(DataCashServiceConstants.CAPTURED_EXP_DATE).substring(2));

            hccCapturedData.put(DataCashServiceConstants.ISSUER_KEY,
               response.get(DataCashServiceConstants.ISSUER));
            hccCapturedData.put(prefix + PaymentConstants.CARD_NUMBER,
               response.get(DataCashServiceConstants.HCC_PAN));
            // added default value for cardType
            hccCapturedData.put("cardtype", getCardType(cardScheme));
      			if (!ConfReader.getBooleanEntry(bookingComponent
						.getClientApplication().getClientApplicationName()
						+ ".lowDepositFeatureAvailable", false)) {
					hccCapturedData.put(BookingConstants.SELECTED_DEPOSIT_TYPE,
							hccCapturedData.get("depositType"));
				}

            String status = response.get(DataCashServiceConstants.STATUS_RS);
            bookingComponent.setHccCapturedData(hccCapturedData);
            hccsmap = bookingComponent.getHccsMap();
            hccsmap.put(DataCashServiceConstants.STATUS_RS, status);
            hccsmap.put(DataCashServiceConstants.HCC_CAPTURE_STATUS_KEY, queryStatus);

         }
         catch (FailureReport e)
         {
            handleFailureReport(e, vTid);
         }

         LogWriter.logDebugMessage(response.getSanitizedDoc());

      }
      catch (IOException e)
      {
         LogWriter.logErrorMessage(e.getMessage(), e);
      }
      catch (JDOMException e)
      {
         LogWriter.logErrorMessage(e.getMessage(), e);
      }
      return hccsmap;
   }

   private boolean isCardSchemaSupported(String cardScheme, ClientApplication clientApplication,
      String currency)
   {
      // Get the supported payment types from the conf file based on client name and currency
      List<String> supportedPaymentTypes = Arrays.asList(StringUtils.split(ConfReader.getConfEntry(
         clientApplication.getClientApplicationName() + DOT_SEPARATOR + currency + ".cnpcards",
         null), COMMA_SEPARATOR));
      LogWriter.logInfoMessage(
				"CardSchemaSupported::"
						+ ConfReader.getConfEntry(
								clientApplication.getClientApplicationName()
										+ DOT_SEPARATOR + currency
										+ ".cnpcards", null), COMMA_SEPARATOR);
      LogWriter.logInfoMessage("Supported payment types::"+supportedPaymentTypes);
      LogWriter.logInfoMessage("Card scheme::"+cardScheme);
      return supportedPaymentTypes.contains(cardScheme);
   }
   /**
    * @param bookingComponent
    * @return
    */
   private Map<String, String> getHccCapturedData(BookingComponent bookingComponent)
   {
      HashMap<String, String> hccCapturedData = new HashMap<String, String>();
      hccCapturedData.put("payment_0" + PaymentConstants.TRANSACTION_AMOUNT,
         bookingComponent.getPayableAmount().getAmount().toString());
      hccCapturedData.put("CURRENCY", bookingComponent.getPayableAmount().getCurrency().toString());
      hccCapturedData.put("depositType", "fullCost");
      bookingComponent.setHccCapturedData(hccCapturedData);
      return hccCapturedData;
   }

   private Agent getAgent()
   {
      Agent agent = new Agent();
      // This is the place u would get appropriate host
      String host = ConfReader.getConfEntry(HOST_NAME, null);

      if (StringUtils.isBlank(host))
      {
         LogWriter.logErrorMessage(
            PropertyResource.getProperty("datacash.host.notfound", MESSAGES_PROPERTY));
         throw new RuntimeException(
            PropertyResource.getProperty("datacash.host.notfound", MESSAGES_PROPERTY));
      }
      agent.setHost(host);
      agent.setTimeout(ConfReader.getIntEntry(DATACASH_CONNECTION_TIMEOUT, DEFAULT_TIMEOUT));
      return agent;

   }

   private void handleFailureReport(FailureReport failureReport, String vTid)
   {
      String errorMessage = PropertyResource.getProperty(DATACASH_ERROR, MESSAGES_PROPERTY) + vTid
         + NEXT_LINE + failureReport.getMessage();
      LogWriter.logErrorMessage(errorMessage, failureReport);
      throw new RuntimeException(errorMessage);
   }

   private String getCardType(String cardScheme)
   {

      if (StringUtils.contains(cardScheme, "Debit") || StringUtils.contains(cardScheme, "Electron")
         || StringUtils.contains(cardScheme, "American")||StringUtils.contains(cardScheme,"Purchasing"))
      {
         String scheme[] = cardScheme.split(" ");
         cardScheme = scheme[0] + "_" + scheme[1];
      }
      return ConfReader.getConfEntry(cardScheme.toUpperCase(), "Debit");
      /*
       * Map<String,String> cardtype=new HashMap<String,String>();
       * cardtype.put("VISA Debit","Debit"); cardtype.put("VISA","Credit");
       * cardtype.put("Debit Mastercard","Debit"); cardtype.put("Mastercard","Credit");
       * cardtype.put("VISA Electron","Debit"); cardtype.put("Maestro","Gift");
       */

   }

   /**
    * This method is used as a common method to send the AJAX response the UI.
    *
    * @param response the HttpServletResponse object
    * @param out the AJAX output
    *
    */
   private void sendAjaxResponse(HttpServletResponse response, String output) throws IOException
   {
      response.setContentType(HTML_CONTENT_TYPE);
      response.setCharacterEncoding(UTF_ENCODING);
      response.getWriter().write(output);
   }

   /**
    * @param string
    * @return
    */
   private String transform(String cardScheme, String pan)
   {

      switch (cardScheme)
      {
         case "VISA":
            cardScheme = "VISA";
            break;
         case "VISA Debit":
            cardScheme = "VISA_DELTA";
            break;
         case "Debit Mastercard":
            cardScheme = "DEBIT_MASTERCARD";
            break;
         case "Mastercard":
            cardScheme = findMastercardType(pan);
            break;
         case "Maestro":
            cardScheme = "MAESTRO";
            break;
         case "VISA Electron":
            cardScheme = "VISA_ELECTRON";
            break;
         case "American Express":
            cardScheme = "AMERICAN_EXPRESS";
            break;
         case "VISA Purchasing":
             cardScheme = "VISA_PURCHASING";
             break;

      }
      return cardScheme;
   }

   public String findMastercardType(String cardNumber)
   {
      String cardType = "MASTERCARD";
      String thomsonCardRange = ConfReader.getConfEntry("ThomsonCreditcard.BINRange", null);
      String[] thomsonCardNumberList = null;
      if (null != thomsonCardRange && StringUtils.isNotEmpty(thomsonCardRange))
      {
         thomsonCardNumberList = thomsonCardRange.split(",");
      }
      if (null != thomsonCardNumberList && StringUtils.isNotEmpty(thomsonCardRange))
      {
         for (String thomsonCardNumber : thomsonCardNumberList)
         {
            if (thomsonCardNumber.startsWith(cardNumber.substring(0, 6)))
            {
               return "TUI_MASTERCARD";
            }
         }
      }
      String masterCardRange = ConfReader.getConfEntry("MasterCardTUIGiftCard.BINRange", null);
      String[] masterCardRangeList = null;
      if (null != masterCardRange && StringUtils.isNotEmpty(masterCardRange))
      {
         masterCardRangeList = masterCardRange.split(",");
      }
      if (null != masterCardRangeList && StringUtils.isNotEmpty(masterCardRange))
      {
         for (String masterCardNumber : masterCardRangeList)
         {
            if (masterCardNumber.startsWith(cardNumber.substring(0, 6)))
            {
               return "MASTERCARD_GIFT";
            }
         }
      }
      return cardType;

   }

   public static String decodeValue(String value)
   {
      if (!StringUtils.isEmpty(value))
      {
         try
         {
            return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
         }
         catch (UnsupportedEncodingException ex)
         {
            throw new RuntimeException(ex.getCause());
         }
      }
      return StringUtils.EMPTY;
   }

	private boolean isDotNetClient(BookingComponent bookingComponent) {
		String clientApp=bookingComponent.getClientApplication().getClientApplicationName();
		return StringUtils.equals(clientApp,"NEWSKIES") || StringUtils.equals(clientApp,"NEWSKIESM");
	}

	/**
	 * @param bookingComponent
	 * @return
	 */
	private Map<String, String> getHccCapturedDataMap(
			BookingInfo bookingInfo) {
		Map<String, String> hccCapturedData=new HashMap<String,String>();
		hccCapturedData.put("payment_0" + PaymentConstants.TRANSACTION_AMOUNT, bookingInfo.getCalculatedPayableAmount().getAmount().toString());
		hccCapturedData.put("CURRENCY", bookingInfo.getCalculatedPayableAmount().getCurrency().getCurrencyCode());
		hccCapturedData.put("DEPOSIT_TYPE", "fullCost");

		return hccCapturedData;
	}

	private void setAuthentication(XMLDocument xmlDocument, String vTid,
			String password) {
		xmlDocument.set(CLIENT, vTid);
		xmlDocument.set(PASSWORD, password);
	}


	 /**
	    * This method is responsible for creating non payment data for HCC.
	    */
	   protected void createHccNonPaymentData(BookingComponent bookingComponent)
		{
			Map<String, String> hccRequestParameterMap = new HashMap<String, String>();
			hccRequestParameterMap = bookingComponent.getHccCapturedData();
			for (Entry<String, String> entry : hccRequestParameterMap.entrySet()) {
				String entryKey = entry.getKey();
				if (bookingComponent.getNonPaymentData() == null) {
					bookingComponent.setNonPaymentData(new HashMap<String, String>());
				}
				if (!entryKey.startsWith(DispatcherConstants.PAYMENT)) {
					bookingComponent.getNonPaymentData().put(entryKey, entry.getValue());
				}
			}
		}




	   /**
	    * Updates the booking component with the data entered in the payment page. Updates passenger
	    * information and promotional discount data, if applicable.
	    */
	   private void updateHccBookingComponent(BookingComponent bookingComponent)
	   {
	      updatePassengerInformation(bookingComponent);

	      ContactInfo contactInfo = new ContactInfo();

	      Map<String, String> hccRequestParameterMap = new HashMap<String, String>();
			hccRequestParameterMap = bookingComponent.getHccCapturedData();

	      if (hccRequestParameterMap.containsKey("mobilePhone"))
	      {
	         contactInfo.setAltPhoneNumber(hccRequestParameterMap.get("mobilePhone"));
	      }
	      if (hccRequestParameterMap.containsKey("city"))
	      {
	         contactInfo.setCity(hccRequestParameterMap.get("city"));
	      }
	      if (hccRequestParameterMap.containsKey("country"))
	      {
	         contactInfo.setCountry(hccRequestParameterMap.get("country"));
	      }
	      if (hccRequestParameterMap.containsKey("county"))
	      {
	         contactInfo.setCounty(hccRequestParameterMap.get("county"));
	      }
	      if (hccRequestParameterMap.containsKey("emailAddress"))
	      {
	         contactInfo.setEmailAddress(hccRequestParameterMap.get("emailAddress"));
	      }
	      if (hccRequestParameterMap.containsKey("dayTimePhone"))
	      {
	         contactInfo.setPhoneNumber(hccRequestParameterMap.get("dayTimePhone"));
	      }
	      if (hccRequestParameterMap.containsKey("postCode"))
	      {
	         contactInfo.setPostCode(hccRequestParameterMap.get("postCode"));
	      }
	      if (hccRequestParameterMap.containsKey("houseName"))
	      {
	         contactInfo.setStreetAddress1(hccRequestParameterMap.get("houseName")
	            + hccRequestParameterMap.get("addressLine1"));
	      }
	      if (hccRequestParameterMap.containsKey("addressLine2"))
	      {
	         contactInfo.setStreetAddress2(hccRequestParameterMap.get("addressLine2"));
	      }

	      if (hccRequestParameterMap.containsKey("addressLine1"))
	      {
	         bookingComponent.setContactInfo(contactInfo);
	      }

	      Discount discount = new Discount();
	      if (bookingComponent.getNonPaymentData().containsKey("promotionalCode"))
	      {
	         discount.setDiscountId(bookingComponent.getNonPaymentData().get("promotionalCode"));
	      }
	      if (bookingComponent.getNonPaymentData().containsKey("promoDiscount"))
	      {
	         discount.setDiscountAmount(bookingComponent.getNonPaymentData().get("promoDiscount"));
	      }

	      if (bookingComponent.getNonPaymentData().containsKey("promotionalCode"))
	      {
	         bookingComponent.setDiscount(discount);
	      }
	   }

	   /**
	    * Updates the passenger information.
	    */
	   protected void updatePassengerInformation(BookingComponent bookingComponent)
	   {
	      updatePassengerInfo(0,bookingComponent);
	   }

	   /**
	    * Updates the passenger information.
	    *
	    * @param passengerIndex the passenger index.
	    */
	   protected void updatePassengerInfo(int passengerIndex,BookingComponent bookingComponent)
	   {
		   Map<String,String> 	requestParameterMap=bookingComponent.getHccCapturedData();
	      if (bookingComponent.getPassengerRoomSummary() != null)
	      {
	         for (List<PassengerSummary> passengerSummaryList : bookingComponent
	            .getPassengerRoomSummary().values())
	         {
	            for (PassengerSummary passengerSummary : passengerSummaryList)
	            {
	               if (requestParameterMap.containsKey(PERSONAL_DETAILS + passengerIndex + "_title"))
	               {
	                  passengerSummary.setTitle(requestParameterMap.get(PERSONAL_DETAILS
	                     + passengerIndex + "_title"));
	               }
	               if (requestParameterMap.containsKey(PERSONAL_DETAILS + passengerIndex + "_foreName"))
	               {
	                  passengerSummary.setForeName(requestParameterMap.get(PERSONAL_DETAILS
	                     + passengerIndex + "_foreName"));
	               }
	               if (requestParameterMap.containsKey(PERSONAL_DETAILS + passengerIndex + "_surName"))
	               {
	                  passengerSummary.setLastName(requestParameterMap.get(PERSONAL_DETAILS
	                     + passengerIndex + "_surName"));
	               }
	               passengerIndex++;
	            }
	         }
	      }
	   }


   /**
    *
    * Gets a unique merchant reference using <code>UUID</code>.
    *
    * @param bookingComponent the bookingComponent.
    *
    * @return merchantReference the generated unique merchant reference.
    * 
    * This needs to moved to common method as it is referred in both Payment Servlet and HccProcessServlet 
    * 
    */
   private static String getMerchantReference(BookingComponent bookingComponent)
   {
      String clientApp = bookingComponent.getClientApplication().getClientApplicationName();
      if (clientApp.equalsIgnoreCase("NEWSKIES") || clientApp.equalsIgnoreCase("NEWSKIESM"))
      {
         return clientApp + '_'
            + bookingComponent.getNonPaymentData().get("booking_Reference_Number") + '_'
            + StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
               MAX_REFERENCE_LENGTH_ACSS - (clientApp.length()
                  + bookingComponent.getNonPaymentData().get("booking_Reference_Number").length()));
      }
      else if (clientApp.equalsIgnoreCase("TUITHSHOREX")
         && ConfReader.getBooleanEntry("merchantid.enabled", false))
      {

         if (!Objects.isNull(bookingComponent.getCruiseSummary())
            && !Objects.isNull(bookingComponent.getCruiseSummary().getExcursionSummary()))
         {

            String merchantId =
               bookingComponent.getCruiseSummary().getExcursionSummary().getMerchantID();
            if (StringUtils.isEmpty(merchantId))
            {
               merchantId = clientApp;
            }
            return merchantId
               + StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
                  MAX_REFERENCE_LENGTH - clientApp.length());

         }
         else
         {
            return clientApp
               + StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
                  MAX_REFERENCE_LENGTH - clientApp.length());
         }
      }
      else
      {
         return clientApp
            + StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
               MAX_REFERENCE_LENGTH - clientApp.length());
      }
   }

   /**
    * @param bookingComponent
    * @return  returns the total amount
    */
   private String emv3dsTotalAmount(BookingComponent bookingComponent)
   {

      Map<String, String> hccQueryResponseMap = bookingComponent.getHccCapturedData();

      BigDecimal transAmt =
         new BigDecimal(hccQueryResponseMap.get("payment_0" + PaymentConstants.TRANSACTION_AMOUNT));
      BigDecimal transChrge = new BigDecimal("0.0");
      Currency currency = Currency.getInstance(hccQueryResponseMap.get("CURRENCY"));
      Money transactionAmount = new Money(transAmt, currency);
      Money transactionCharge = new Money(transChrge, currency);
      BigDecimal totalAmount = (transactionAmount.add(transactionCharge)).getAmount().abs();
      return totalAmount.toString();

   }

}
