package com.tui.uk.payment.dispatcher;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.RetriveThreedAuthResponse;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.processor.FraudScreeningProcessor;
import com.tui.uk.payment.processor.FraudScreeningProcessorImpl;
import com.tui.uk.payment.processor.PostPaymentProcessor;
import com.tui.uk.payment.processor.PostPaymentProcessorFactory;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.payment.service.datacash.DataCashServiceFactory;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * This class handles post Authenticate flow & responsible for retrieve status
 * request & response processing & Authorization / Pay flow will be completed
 * depending on status.
 * 
 * @author raghunandan.d
 *
 */
public class PostEmvAuthProcessServlet extends CommonPaymentServlet {

	private static final long serialVersionUID = 1L;

	/** The dot separator. */
	private static final String DATACASH_ERROR = "datacash.generic.error";

	/** The POST_PAYMENT_URL. */
	private static final String POST_PAYMENT_URL = "postPaymentUrl";

	/** The BEFORE_CONFIRM_PAGE. */
	private static String BEFORE_CONFIRM_PAGE = "/common/beforeconfirm.jsp";
	/** The BEFORE_CONFIRM_PAGE. */

	private static final String EMV_3DS_AZ_PAY_ERROR = "emv.3ds.az.pay.error.";

	private static final String GENERIC = "generic";

	private static final String GENERIC_MSG = "Looks like something has gone wrong and we're unable "
			+ "to take payment from this card right now. Please select another payment method from the "
			+ "options above or refresh the page to pay with another card.";

	private static final String EMV_3DS_AZ_PAY_ERROR_CODES = "emv.3ds.az.pay.error.codes";
	private static final String DEF_EMV_3DS_AZ_PAY_ERROR_CODE = "2543";
	
   private static final String EMV_AUTH_RETURN_SUCCESS_CODES = "emv.auth.success.codes";


	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String token = request.getParameter(TOKEN);
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
				UUID.fromString(token));
		paymentData.getBookingInfo().setNewSetupReq(true);
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		String brand = request.getParameter("b");
		String tomcatInstance = request.getParameter("tomcat");

		verifySessionId(bookingInfo, request.getSession().getId());

		DataCashPaymentTransaction dataCashPaymentTransaction = (DataCashPaymentTransaction) paymentData
				.getPayment().getPaymentTransactions().get(0);

		String paymentFailureUrl = bookingComponent.getPaymentFailureURL();
		String failureUrl = getEmv3dsRedirectURL(request, brand, tomcatInstance);
		String url = getPostPaymentUrl(bookingComponent, null);
      List<String> emvAuthReturnSuccessCodesList = Arrays.asList(StringUtils.split(
         ConfReader.getConfEntry(EMV_AUTH_RETURN_SUCCESS_CODES,
               "2550,2551"), ","));
		PostPaymentProcessor postPaymentProcessor = PostPaymentProcessorFactory
				.getPostPaymentProcessor(
						bookingComponent.getClientApplication(), paymentData,
						request.getParameterMap());
		try {
			if (isPaymentFailureRedirect(paymentData, paymentFailureUrl)) {
				LogWriter.logInfoMessage("Redirecting to payment failure URL"
						+ " for the token= " + token);
				request.setAttribute(POST_PAYMENT_URL, failureUrl);
				request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(
						request, response);
			} else if (paymentData.getTransactionStatus() == TransactionStatus.EMV_CHALLENGE_FLOW_INPROGRESS
					&& StringUtils.equalsIgnoreCase(
							dataCashPaymentTransaction.getStatus(), "2525")) {
				LogWriter
						.logInfoMessage("Proceed to Authorization / Pay for Challenge Flow"
								+ " for the token= " + token);
				processAuthorization(request, response, token,
						bookingComponent, dataCashPaymentTransaction,
						postPaymentProcessor);
			} else if (paymentData.getTransactionStatus() == TransactionStatus.EMV_FRICTIONLESS_FLOW_COMPLETED
					&& emvAuthReturnSuccessCodesList.contains(dataCashPaymentTransaction.getEmvStausCode())) {
				LogWriter
						.logInfoMessage("Proceed to Authorization / Pay for Frictionless Flow"
								+ " for the token= " + token);
				request.setAttribute(POST_PAYMENT_URL, url);
				request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(
						request, response);
			} else {
				LogWriter
						.logErrorMessage("Authentication Payer is failed with the code ="
								+ dataCashPaymentTransaction.getStatus()
								+ " for the token= " + token);
				showErrorPage(request, response);
			}

		} catch (DataCashServiceException ex) {
			List<String> errorCodeList = Arrays.asList(StringUtils.split(
					ConfReader.getConfEntry(EMV_3DS_AZ_PAY_ERROR_CODES,
							DEF_EMV_3DS_AZ_PAY_ERROR_CODE), ","));
			if (Objects.nonNull(ex)
					&& (errorCodeList.contains(String.valueOf(ex.getCode())))) {
				LogWriter
						.logErrorMessage("Authorization / Pay is failed with the code ="
								+ ex.getCode() + " for the token= " + token);
				String defaultMsg = ConfReader.getConfEntry(
						EMV_3DS_AZ_PAY_ERROR + GENERIC, GENERIC_MSG);
				String errorMessage = ConfReader.getConfEntry(
						EMV_3DS_AZ_PAY_ERROR + ex.getCode(), defaultMsg);
				request.setAttribute("errorMessage", errorMessage);
				LogWriter.logErrorMessage(errorMessage, ex);
				processHccPaymentException(request, response, paymentData);
				showErrorPage(request, response);
			} else {
				String errorMessage = PropertyResource.getProperty(
						DATACASH_ERROR, MESSAGES_PROPERTY)
						+ bookingComponent.getPaymentGatewayVirtualTerminalId()
						+ "\n" + ex.getMessage();
				LogWriter.logErrorMessage(errorMessage, ex);
				processHccPaymentException(request, response, paymentData);
				showErrorPage(request, response);
			}
		}

	}

	/**
	 * Method handles Retrieve Authentication Status and Authorization / Pay
	 * call request & response handling.
	 * 
	 * @param request
	 * @param response
	 * @param token
	 * @param bookingComponent
	 * @param dataCashPaymentTransaction
	 * @param postPaymentProcessor
	 * @throws DataCashServiceException
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processAuthorization(HttpServletRequest request,
			HttpServletResponse response, String token,
			BookingComponent bookingComponent,
			DataCashPaymentTransaction dataCashPaymentTransaction,
			PostPaymentProcessor postPaymentProcessor)
			throws DataCashServiceException, ServletException, IOException {
		RetriveThreedAuthResponse retriveThreedAuthRes = DataCashServiceFactory
				.getDataCashService(
						bookingComponent.getPaymentGatewayVirtualTerminalId())
				.getEMVRetrieveAuthResponse(
						bookingComponent.getPaymentGatewayVirtualTerminalId(),
						bookingComponent.getHccsMap().get(
								"hpsDatacashReference"), token);

		if (Objects.nonNull(retriveThreedAuthRes)
				&& StringUtils.equals(retriveThreedAuthRes.getStatus(), "2550")) {
			LogWriter
					.logInfoMessage("Challenge Authentication is successful with the code ="
							+ retriveThreedAuthRes.getStatus()
							+ " for the token= " + token);
			dataCashPaymentTransaction
					.setDatacashReference(retriveThreedAuthRes.getDsReference());
         if (!ConfReader.getBooleanEntry(DataCashServiceConstants.EMV_MERCH_REF_ENABLED, true))
         {
            dataCashPaymentTransaction
               .setMerchantReference(retriveThreedAuthRes.getMerchantreference());
         }
			dataCashPaymentTransaction.setStatus(retriveThreedAuthRes
					.getStatus());
			dataCashPaymentTransaction.setReason(retriveThreedAuthRes
					.getReason());
			dataCashPaymentTransaction.setTime(retriveThreedAuthRes.getTime());
			postPaymentProcessor.emv3DSPostProcess(
					bookingComponent.getPaymentGatewayVirtualTerminalId(),
					request.getParameter(TOKEN));
			request.setAttribute(POST_PAYMENT_URL,
					getPostPaymentUrl(bookingComponent, null));
			request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(request,
					response);
		} else {
			LogWriter
					.logInfoMessage("Challenge Authentication is failed with the code ="
							+ retriveThreedAuthRes.getStatus()
							+ " for the token= " + token);
			throw new DataCashServiceException(
					"Challenge Authentication Failed with the code "
							+ retriveThreedAuthRes.getStatus());
		}
	}

	/**
	 * Forwards the request to error page.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void showErrorPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute(POST_PAYMENT_URL, "/cps/common/error.jsp");
		request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(request,
				response);
	}

	/**
	 * Updating failure count for re-processing.
	 * 
	 * @param request
	 * @param response
	 * @param paymentData
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void processHccPaymentException(HttpServletRequest request,
			HttpServletResponse response, PaymentData paymentData)
			throws ServletException, IOException {
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		String paymentFailureUrl = bookingInfo.getBookingComponent()
				.getPaymentFailureURL();
		if (StringUtils.isNotBlank(paymentFailureUrl)) {
			paymentData.setFailureCount();
		}
		FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
				request.getParameter(TOKEN));
		fraudScreeningProcessor.updateFailureCardData();
	}

	/**
	 * Returns EMV 3DS payment failure URL.
	 * 
	 * @param request
	 * @param brand
	 * @param tomcatInstance
	 * @return String
	 */
	private String getEmv3dsRedirectURL(HttpServletRequest request,
			String brand, String tomcatInstance) {

		final String COLON = ":";
		String token = request.getParameter(DispatcherConstants.TOKEN);
		StringBuffer strBuffer = new StringBuffer(ConfReader.getConfEntry(
				"hcc.protocol", "https") + "://");
		strBuffer
				.append(request.getServerName())
				.append(Boolean.valueOf(ConfReader.getConfEntry(
						"hcc.dev.environment", "false")) ? COLON
						+ ConfReader.getConfEntry("hcc.dev.environment.port",
								"9090") : "").append(request.getContextPath())
				.append("/paymentFailure?b=").append(brand).append("&token=")
				.append(token).append("&tomcat=").append(tomcatInstance);
		return strBuffer.toString();

	}
}
