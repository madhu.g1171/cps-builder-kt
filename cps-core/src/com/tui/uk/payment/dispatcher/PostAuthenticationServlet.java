/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PostAuthenticationServlet.java,v $
 *
 * $Revision: 1.5 $
 *
 * $Date: 2009-03-2 08:25:26 $
 *
 * $Author: bibin.j $
 *
 *
 * $Log: $ .
 *
 */
package com.tui.uk.payment.dispatcher;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.ACCEPT_LANGUAGE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.AUTH_ERROR;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.DATACASH_RESPONSE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.HYPHEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.MD;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.PARES;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.SESSION_MIN_INTERVAL;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.domain.PaymentConstants.THREED_NON_FATAL_DUP_RESP_ERROR;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.processor.FraudScreeningProcessor;
import com.tui.uk.payment.processor.FraudScreeningProcessorImpl;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * The class PostAuthenticationServlet. This class is used to perform all the post authentication
 * activities.
 *
 * @author bibin.j
 */
@SuppressWarnings("serial")
public final class PostAuthenticationServlet extends CommonPaymentServlet
{
	/** The PAYMENT. */
	private static final String PAYMENT = "payment.";

	/** The DATA_CASH_ERROR. */
	private static final String DATA_CASH_ERROR = ".datacash.error";

	/** The DEFAULT_VALUE. */
	private static final String DEFAULT_VALUE = "default";

	/** The POST_PAYMENT_URL. */
	private static final String POST_PAYMENT_URL = "postPaymentUrl";

	/** The BEFORE_CONFIRM_PAGE. */
	private static String BEFORE_CONFIRM_PAGE = "/common/beforeconfirm.jsp";


	/** The POST_AUTHENTICATION_SESSION_TIMEOUT. */
	private static final String POST_AUTHENTICATION_SESSION_TIMEOUT =
			"postAuthenticationSessionTimeOut.3DSecure";

	/** The auth failed code. */
	private static final int AUTH_FAILED_CODE = 7;

	/** The CSRF TOKEN for post request to Hybris. */
	private static final String CSRF_TOKEN = "csrfToken";

	/**
	 * This method is responsible for performing all the necessary action that has to be taken once
	 * the control returns to CPS from bank page.
	 *
	 * @param request the HttpServletRequest object that contains the request the client has
	 * made of the servlet.
	 * @param response the HttpServletResponse object that contains the response the servlet
	 * sends to the client.
	 *
	 * @throws IOException the IOException thrown if an input or output error is detected when the
	 * servlet handles the request
	 * @throws ServletException the ServletException thrown if the request for the POST could
	 * not be handled.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
			{
		String token = request.getParameter(TOKEN);
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		//boolean hccSwitch = ConfReader.getBooleanEntry("tuicommon.hcc", false);
		boolean hccSwitch = ConfReader.getBooleanEntry(
				bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);

		String hybrisSessionId=getHybrisSessionId(paymentData);
		if(bookingComponent.getClientApplication().getClientApplicationName().equalsIgnoreCase("TUITHSHOREX")){
			BEFORE_CONFIRM_PAGE = "/tuithshorex/beforeconfirm.jsp";
		}
		verifySessionId(bookingInfo, request.getSession().getId());

		// If price changes due to browser back and forward buttons
		String priceChangeUrl = priceChangeUrl(bookingComponent);
		if (StringUtils.isNotBlank(priceChangeUrl))
		{
			response.sendRedirect(priceChangeUrl);
			return;
		}

		bookingInfo.setErrorFields("");
		bookingComponent.setErrorMessage("");
		request.setAttribute("paymentUrl", paymentPageAbsoluteUrl(request));
		String paymentFailureUrl = bookingComponent.getPaymentFailureURL();

		//Go back to payment page with error message in case of TRACS or Vision down time.
		if(checkForDownTimeRedirection(request))
		{
			String errorMessage = null;
			if(request.getAttribute("downTimeErrorMsg") != null)
			{
				errorMessage = "DOWNTIME_ERROR|" + (String)request.
						getAttribute("downTimeErrorMsg");
			}
			LogWriter.logErrorMessage(errorMessage);
			request.setAttribute(POST_PAYMENT_URL, getPostPaymentFailureUrl(paymentData,
					paymentFailureUrl, request));
			request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(request, response);
			return;
		}

		if (isPaymentFailureRedirect(paymentData, paymentFailureUrl))
		{
			request.setAttribute(POST_PAYMENT_URL, paymentFailureUrl);
			request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(request, response);
			return;
		}
		else if (!bookingInfo.getThreeDAuth() && bookingInfo.getNewHoliday())
		{
			String browserLocale = request.getHeader(ACCEPT_LANGUAGE);
			Locale locale = new Locale(StringUtils.substringBefore(browserLocale, HYPHEN), StringUtils
					.substringAfterLast(browserLocale, HYPHEN));
			try
			{
				String paRes = request.getParameter(PARES);
				String mdUrl = request.getParameter(MD);

				LogWriter.logInfoMessage("MD from bank :" + mdUrl + " for token :" + token);
				LogWriter.logDebugMessage("PaRes from bank :" + paRes + " for token :" + token);

				for (PaymentTransaction paymentTransaction : paymentData.getPayment()
						.getPaymentTransactions(PaymentMethod.DATACASH))
				{
					LogWriter.logInfoMessage("Sending request to Datacash for authorization");
					DataCashPaymentTransaction dataCashPaymentTransaction = (DataCashPaymentTransaction)
							paymentTransaction;

					//The MD field gives you a mechanism of tracing transactions.
					//This field should be populated with a unique value.
					//This value will be returned to you, with the PaRes message,
					//when the bank authorization has finished.
					//The MD value send by CPS to bank and got from PaRes message should be same.

					if (ConfReader.getBooleanEntry("3DSecure.MD.validation", true)
							&& !mdUrl.equalsIgnoreCase(dataCashPaymentTransaction.getMD()))
					{
						LogWriter.logInfoMessage("MD validation failed.");
						bookingComponent.setErrorMessage(PropertyResource.getProperty(
								AUTH_ERROR, MESSAGES_PROPERTY, locale));
						LogWriter.logInfoMessage(PropertyResource.getProperty(
								DATACASH_RESPONSE, MESSAGES_PROPERTY, locale));
						request.setAttribute(POST_PAYMENT_URL, "/common/error.jsp");
						request.getRequestDispatcher(BEFORE_CONFIRM_PAGE)
						.forward(request, response);
						return;
					}
					//tracking data is being set into payment transaction for logging purpose.
					dataCashPaymentTransaction.setTrackingData(bookingInfo.getTrackingData().toString());
					// doing auth payment.
					if(Boolean.valueOf(hccSwitch)) {
						dataCashPaymentTransaction.authorizeHcc3DPayment(bookingComponent
								.getPaymentGatewayVirtualTerminalId(), paRes);
					}else {
						dataCashPaymentTransaction.authorize3DPayment(bookingComponent
								.getPaymentGatewayVirtualTerminalId(), paRes);
					}

					dataCashPaymentTransaction.queryPayment(bookingComponent
							.getPaymentGatewayVirtualTerminalId());

				}
				processFraudScreening(token);
				bookingInfo.setThreeDAuth(Boolean.TRUE);
			}
			catch (DataCashServiceException dcse)
			{
				if (Arrays.asList(ConfReader.getStringValues(THREED_NON_FATAL_DUP_RESP_ERROR, null, ","))
						.contains(String.valueOf(dcse.getCode())))
				{
					LogWriter.logInfoMessage("Returning duplicate thread to before confirmation wihout updating any status.,"+token+",JESSSIONID:"+hybrisSessionId);
					try {
						processFraudScreeningBasedOnTRxnStatus(token);
						updatePostPaymentUrl(bookingComponent, request);
						request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(request, response);
						return;
					} catch (DataCashServiceException de) {
						LogWriter.logErrorMessage("Fraudscreening failed for duplicate threeD Auth Request."+token+",JESSSIONID:"+hybrisSessionId);
					}
				}
				String errorMessage = getDataCashFatalErrorMessage(dcse.getCode(), locale,
						getDatacashErrorMessageKey(bookingComponent.getClientApplication()
								.getClientApplicationName()));
				processException(bookingInfo, errorMessage);
				LogWriter.logErrorMessage(errorMessage + " for token :" + token, dcse);

				if (StringUtils.isNotBlank(paymentFailureUrl))
				{
					paymentData.setFailureCount();
				}

				FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
						token);
				fraudScreeningProcessor.updateFailureCardData();

				request.setAttribute(POST_PAYMENT_URL, getPostPaymentFailureUrl(paymentData,
						paymentFailureUrl, request));
				request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(request, response);
				return;
			}

		decreasedSessionTimeOut(request);
		updatePostPaymentUrl(bookingComponent,request);
		LogWriter.logInfoMessage("Forwarding to Post Payment URL: "+request.getAttribute(POST_PAYMENT_URL)+" for token: "+token+",JSESSIONID: "+hybrisSessionId);
		request.getRequestDispatcher(BEFORE_CONFIRM_PAGE).forward(request, response);
			}

		}
	/**
	 * @param paymentData
	 * @return Hybris SessionID if exists in nonpaymentData.
	 */
	private String getHybrisSessionId(PaymentData paymentData) {
		if (paymentData != null && paymentData.getBookingInfo() != null) {
			String hybrisSessionId = paymentData.getBookingInfo()
					.getBookingComponent().getNonPaymentData()
					.get("JSESSIONID");
			if (StringUtils.isBlank(hybrisSessionId)) {
				return StringUtils.EMPTY;
			}
			return hybrisSessionId;

		}
		return StringUtils.EMPTY;
	}

	private void processFraudScreeningBasedOnTRxnStatus(String token) throws DataCashServiceException
	{
		FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
		TransactionStatus trxStatus = PaymentStore.getInstance().getPaymentData(UUID.fromString(
				token)).getTransactionStatus();
		if (!trxStatus.equals(TransactionStatus.FRAUD_SCREENING_PRE_AUTH_SUCCESS)
				&& (trxStatus.equals(TransactionStatus.FRAUD_SCREENING_PRE_AUTH_FAILURE)
						|| !fraudScreeningProcessor.processFraudScreening()))
		{
			throw new DataCashServiceException(AUTH_FAILED_CODE, "Not Authorised");
		}
	}


	private void updatePostPaymentUrl(BookingComponent bookingComponent, HttpServletRequest request)
	{
		String postPaymentUrl = getPostPaymentUrl(bookingComponent, null);
		if (bookingComponent.getClientApplication().getClientApplicationName().equalsIgnoreCase("TUITHSHOREX"))
		{
			postPaymentUrl = bookingComponent.getClientDomainURL()+"/destinations/book/confirmation";
		}
		request.setAttribute(POST_PAYMENT_URL, postPaymentUrl);
		if (bookingComponent.getNonPaymentData() != null
				&& StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get(CSRF_TOKEN)))
			{
				request.setAttribute(CSRF_TOKEN, bookingComponent.getNonPaymentData().get(CSRF_TOKEN));
			}
		}

	/**
	 * This method is responsible for reducing cps session time out.
	 * While moving to the bank page increased session timeout,
	 * now reducing that time.
	 *
	 * @param request the HttpServletRequest object.
	 */
	private void decreasedSessionTimeOut(HttpServletRequest request)
	{
		request.getSession().setMaxInactiveInterval(
				ConfReader.getIntEntry(POST_AUTHENTICATION_SESSION_TIMEOUT, SESSION_MIN_INTERVAL));
	}

	/**
	 * Gets the message key for "datacash.error" based on
	 * client application from message.prop.
	 * @param clientApplicationName the client application name.
	 * @return key the key.
	 */
	private String getDatacashErrorMessageKey(String clientApplicationName)
	{
		String key = PAYMENT + DEFAULT_VALUE + DATA_CASH_ERROR;

		if (StringUtils.isNotBlank(clientApplicationName))
		{
			key = PAYMENT + clientApplicationName + DATA_CASH_ERROR;
		}
		return key;
	}

	/**
	 * This method is responsible for contacting fraud screening and getting appropriate status from
	 * fraud screening application.
	 *
	 * @param token the token.
	 *
	 * @throws DataCashServiceException if fraud screening application rejects the transactions.
	 */
	private void processFraudScreening(String token) throws DataCashServiceException
	{
		FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
		if (!fraudScreeningProcessor.processFraudScreening())
		{
			throw new DataCashServiceException(AUTH_FAILED_CODE, "Not Authorised");
		}
	}

	/**
	 * Gets the post payment failure url.
	 *
	 * @param paymentData the payment data.
	 * @param paymentFailureUrl the payment failure url.
	 * @param request the request object.
	 *
	 * @return the url.
	 */
	private String getPostPaymentFailureUrl(PaymentData paymentData, String paymentFailureUrl,
			HttpServletRequest request)
	{
		if (isPaymentFailureRedirect(paymentData, paymentFailureUrl))
		{
			return paymentFailureUrl;
		}
		return paymentPageAbsoluteUrl(request);
	}

	/**
	 * The doGet method.
	 * Receives an HTTP GET request and handles the request by calling the doPost method.
	 *
	 * @param request the HttpServletRequest object that contains the request the client has
	 * made of the servlet.
	 * @param response the HttpServletResponse object that contains the response the servlet
	 * sends to the client.
	 *
	 * @throws IOException the IO exception thrown if an input or output error is detected when the
	 * Servlet handles the request.
	 * @throws ServletException the ServletException thrown if the request could not be handled.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
			{
		doPost(request, response);
			}

	/**
	 * This method is checks for TRACS/VISION down time error.
	 *
	 * @param httpRequest the HttpServletRequest object.
	 *
	 * @return isDown the boolean
	 */
	public boolean checkForDownTimeRedirection(HttpServletRequest httpRequest)
	{
		boolean isDown = false;
		if(httpRequest.getAttribute("RedirectToPaymentPage") != null)
		{
			if(httpRequest.getAttribute("downTimeErrorMsg") != null)
			{
				isDown = true;
			}
		}
		return isDown;
	}

}
