/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: DispatcherConstants.java$
 *
 * $Revision: $
 *
 * $Date: May 23, 2008$
 *
 * Author:
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.dispatcher;

/**
 * This class contains all the constants for charges and discounts.
 */
public final class DispatcherConstants
{

   /**
    * Private constructor to stop instantiation of this class.
    */
   private DispatcherConstants()
   {
      // Do nothing.
   }

   /** The Constant CARD_CHARGE. */
   public static final String CARD_CHARGE = "cardCharge";

   /** The Constant TOKEN. */
   public static final String TOKEN = "token";
   
   /** The Constant TOKEN. */
   public static final String PAGESET_ID = "pageSetID";


   /** The Constant BOOKING_COMPONENT. */
   public static final String BOOKING_INFO = "bookingInfo";

   /** The Constant TODAY_SAVING. */
   public static final String TODAY_SAVING = "todaySaving";

   /** The Constant PRICE_BEAT. */
   public static final String PRICE_BEAT = "priceBeat";

   /** The Constant BALANCE_TYPE. */
   public static final String BALANCE_TYPE = "balanceType";

   /** The Constant FULL_BALANCE. */
   public static final String FULL_BALANCE = "FullBalance";

   /** The Constant DISCOUNT_PERCENT. */
   public static final String DISCOUNT_PERCENT = "Discount %";

   /** The Constant DEPOSIT_TYPE. */
   public static final String DEPOSIT_TYPE = "depositType";

   /** The Constant FULL_COST. */
   public static final String FULL_COST = "fullCost";

   /** The Constant PAYMENT. */
   public static final String PAYMENT = "payment_";

   /** The Constant PRICEBEAT. */
   public static final String PRICEBEAT = "PriceBeat";

   /** The Constant DISCRETIONARY. */
   public static final String DISCRETIONARY = "DISCRETIONARY";

   /** The Constant DISCRETIONARY_AMOUNT. */
   public static final String DISCRETIONARY_AMOUNT = "DiscretionaryAmount";

   /** The Constant HOPLA_THM. */
   public static final String HOPLA_THM = "HOPLA_THM";

   /** The Constant HOPLA_PEG. */
   public static final String HOPLA_PEG = "HOPLA_PEG";

   /** The total amount without card charges. */
   public static final String AMOUNT_WITHOUT_CARDCHARGE = "amtWithoutCardCharge";

   /** The constant for selected card. */
   public static final String SELECTED_CARD = "selectedCard";

   /** The PARTIAL_PAYMENT. */
   public static final String PARTIAL_PAYMENT = "partialPayment";

   /** The PART_PAYMENT. */
   public static final String PART_PAYMENT = "partPayment";

   /** The default failure count. */
   public static final int DEFAULT_FAILURE_COUNT = 3;

   /** Constant for percentage calculation. */
   public static final double PERCENTAGE_FACTOR = 100.0;

   /** The tomcat instance. */
   public static final String TOMCAT_INSTANCE = "tomcat";
   
   /** The expiry url. */
   public static final String HCC_EXPIRY_URL = "hccExpiry.jsp";

   /** The constant PARES. */
   public static final String PARES = "PaRes";

   /** The constant MD. */
   public static final String MD = "MD";

   /** The constant BRAND. */
   public static final String BRAND = "b";

   /** The constant AUTH_ERROR. */
   public static final String AUTH_ERROR = "auth.error";

   /** The constant DATACASH_RESPONSE. */
   public static final String DATACASH_RESPONSE = "datacash.response.7";

   /** The constant BRAND_CODE. */
   public static final String BRAND_CODE = "brandcode.";

   /** The constant ACCEPT_LANGUAGE. */
   public static final String ACCEPT_LANGUAGE = "Accept-Language";

   /** The constant MAX_FAILURE_COUNT. */
   public static final String MAX_FAILURE_COUNT = "max.failure.count";

   /** The constant POST_AUTH_SERVLET. */
   public static final String POST_AUTH_SERVLET =  "/postAuthServlet?token=";

   /** The constant POST_AUTH_SERVLET. */
   public static final String THREE_D_SERVLET =  "/3dspage?token=";

   /** The constant BRAND_VALUE. */
   public static final String BRAND_VALUE = "&b=";

   /** The constant TOMCAT_VALUE. */
   public static final String TOMCAT_VALUE = "&tomcat=";

   /** The constant USER_AGENT. */
   public static final String USER_AGENT = "user-Agent";

   /** The constant MSIE. */
   public static final String MSIE = "MSIE";

   /** The constant FIREFOX. */
   public static final String FIREFOX = "Firefox";

   /** The constant CHROME. */
   public static final String CHROME = "Chrome";

   /** The constant REFERER. */
   public static final String REFERER = "REFERER";

   /** The constant HTTPS. */
   public static final String HTTPS = "https";

   /** The constant SESSION_MAX_INTERVAL. */
   public static final int SESSION_MAX_INTERVAL = 3600;

   /** The constant SESSION_MIN_INTERVAL. */
   public static final int SESSION_MIN_INTERVAL = 1800;

   /** The constant THREE_D_SECURE_CONF_VALUE. */
   public static final String THREE_D_SECURE_CONF_VALUE = ".3DSecure";

   /** The PRE_AUTHENTICATION_SESSION_TIMEOUT. */
   public static final String PRE_AUTHENTICATION_SESSION_TIMEOUT =
      "preAuthenticationSessionTimeOut.3DSecure";

   /** The COMMA. */
   public static final String COMMA = ",";

   /** The constant for holding the date format. */
   public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

   /** The constant for holding ADDDRESS_FINDER_SERVICE. */
   public static final String ADDDRESS_FINDER_SERVICE = "addressFinderService";

   /** The constant for HYPHEN. */
   public static final String HYPHEN = "-";

   /** The constant for holding QUESTION_MARK symbol. */
   public static final String QUESTION_MARK = "?";

   /** The constant for holding AMPERSAND symbol. */
   public static final String AMPERSAND = "&";

   /** The constant for holding TERMURL. */
   public static final String TERMURL = "TermUrl";

   /** The constant for holding PAREQ. */
   public static final String PAREQ = "PaReq";

   /** The constant for holding ACSURL. */
   public static final String ACSURL = "ACSURL";

   /** The constant for CUSTOMER_TYPE. */
   public static final String CUSTOMER_TYPE = "customerType";

   /** The constant for SELECTED_TRANSACTIONS. */
   public static final String SELECTED_TRANSACTIONS = "selectedTransactions";
}