package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.config.ConfReader;

/**
 * Servlet implementation class CookieLegislationServlet.
 */
public class CookieLegislationServlet extends CommonPaymentServlet
{
   /**
    * serialVersionUID.
    */
   private static final long serialVersionUID = 1L;

   /**
    * Default constructor.
    */
   public CookieLegislationServlet()
   {
      // TODO Auto-generated constructor stub
   }

   /**
    * HttpServlet doGet.
    * 
    * @param request HttpServletRequest.
    * @param response HttpServletResponse.
    * @throws ServletException ServletException.
    * @throws IOException IOException.
    * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
    * 
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      // TODO Auto-generated method stub

   }

   /**
    * HttpServlet doPost.
    * 
    * @param request HttpServletRequest.
    * @param response HttpServletResponse.
    * @throws ServletException ServletException.
    * @throws IOException IOException.
    * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      // TODO Auto-generated method stub

      Cookie legislationCookie = new Cookie(BookingConstants.COOKIE_NAME, "true");
      URL url = new URL(request.getRequestURL().toString());
      String code = request.getParameter("b");
      legislationCookie.setPath(ConfReader.getConfEntry("brandcode." + code + "." + "cookiepath",
         "/"));
      String brandDomain =
         (String) ConfReader.getConfEntry("brandcode." + code + "." + "domain", "/");
      legislationCookie.setDomain(brandDomain);
      legislationCookie.setMaxAge(BookingConstants.COOKIE_TWO_YEARS);
      response.addCookie(legislationCookie);
      PrintWriter out = response.getWriter();
      response.setContentType("text/html");
      String brandName = request.getParameter("brand");
      if (request.getParameter("hiddennonjsflag") != null)
      {
         String pathToBeRedirected =
            getPropertyForBrand(brandName, BookingConstants.COOKIE_NONJS_LINK);
         response.sendRedirect(pathToBeRedirected);

      }

   }

   /**
    * Fetch the specific property from the Map using the key and the brand passed.
    * 
    * @param key the key that needs to be inserted in the map.
    * @param brand the brand value passed.
    * @return returns the value for the key.
    */
   public static String getPropertyForBrand(String brand, String key)
   {
      StringBuilder propertyKey = new StringBuilder();
      propertyKey.append(brand);
      propertyKey.append(".");
      propertyKey.append(key);
      return com.tui.uk.config.ConfReader.getConfEntry(propertyKey.toString(), "");
   }
}
