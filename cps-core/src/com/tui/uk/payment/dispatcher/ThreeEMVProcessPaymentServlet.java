/*
 * Copyright (C)2006 TUI UK Ltd
 *
 * TUI UK Ltd,
 * Columbus House,
 * Westwood Way,
 * Westwood Business Park,
 * Coventry,
 * United Kingdom
 * CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence
 * any actual or intended publication of this source code.
 *
 * $RCSfile:   ThreeDSecureServlet.java$
 *
 * $Revision:   $
 *
 * $Date:   18 Mar 2010$
 *
 * Author: ganapna
 *
 *
 * $Log:   $
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.ConfReader;

/**
 * The class ThreeDSecureServlet. This class is responsible for redirecting to the 3DS branded page
 * using the GET method.
 *
 * @author ganapna
 */
@SuppressWarnings("serial")
public class ThreeEMVProcessPaymentServlet extends CommonPaymentServlet
{

   /**The constant for successful return code.*/
   public static final String RETURNCODE_SUCCESS = "001";

   /**The constant for threeDS return code.*/
   public static final String RETURNCODE_THREEDS = "150";


   /** The Text/plain content type. */
   private static final String TEXT_CONTENT_TYPE = "text/plain";

   /** The UTF-8 encoding. */
   private static final String UTF_ENCODING = "UTF-8";
   
   
   /**
    * This method is responsible for redirecting to the 3DS branded page using the GET method.
    *
    * @param request the HttpServletRequest object that contains the request the client has made of
    *        the servlet.
    * @param resp the HttpServletResponse object that contains the response the servlet sends to the
    *        client.
    * @throws ServletException the ServletException thrown if the request for the GET could not be
    *         handled.
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *         servlet handles the request
    * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
    *      javax.servlet.http.HttpServletResponse)
    */
   @Override
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
	   
		response.setContentType(TEXT_CONTENT_TYPE);
		response.setCharacterEncoding(UTF_ENCODING);
		StringBuilder threedsUrl = new StringBuilder();
		threedsUrl.append(
				StringUtils.substringBefore(request.getRequestURL().toString(), request.getContextPath()));
		threedsUrl.append(request.getContextPath());
		threedsUrl.append("/3dsemvpage?token=");
		threedsUrl.append(request.getParameter(DispatcherConstants.TOKEN));
		threedsUrl.append(DispatcherConstants.BRAND_VALUE);
		threedsUrl.append(request.getParameter(DispatcherConstants.BRAND));
		threedsUrl.append(DispatcherConstants.TOMCAT_VALUE);
		threedsUrl.append(request.getParameter(DispatcherConstants.TOMCAT_INSTANCE));
		response.getWriter().write(threedsUrl.toString());

   }
}
