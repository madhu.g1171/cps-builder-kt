/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: AddressFinderServlet.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-15 08:25:26 $
 *
 * $Author: bibin.j@sonata-software.com $
 *
 *
 * $Log: $.
 *
 */
package com.tui.uk.payment.dispatcher;

import static com.tui.uk.payment.dispatcher.DispatcherConstants.ADDDRESS_FINDER_SERVICE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.COMMA;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.DATE_FORMAT;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.AddressFinderServiceException;
import com.tui.uk.payment.service.capscan.addressfinder.Address;
import com.tui.uk.payment.service.capscan.addressfinder.AddressConstants;
import com.tui.uk.payment.service.capscan.addressfinder.AddressFinderService;

/**
 * The AddressFinderServlet class.
 * It calls the address finder service to process the request(house number/post code)
 * and returns the Address.
 *
 * @author bibin.j@sonata-software.com
 */
@SuppressWarnings("serial")
public final class AddressFinderServlet extends HttpServlet
{

    /**
     * Receives an HTTP POST request and handles the request.
     * The HTTP POST method calls the AddressFinderService to find the address with the passed in
     * houseNo and postCode.
     *
     * @param request the HttpServletRequest object that contains the request the client has
     * made of the servlet.
     * @param response the HttpServletResponse object that contains the response the servlet
     * sends to the client.
     *
     * @throws IOException the IOException thrown if an input or output error is detected when the
     * servlet handles the request
     * @throws ServletException the ServletException thrown if the request for the POST could
     * not be handled.
     */
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException
    {
       String addr = "";
       PrintWriter out = response.getWriter();
       response.setContentType("text/xml");
       response.setHeader("Cache-Control", "no-cache");
       String postCode = request.getParameter(AddressConstants.POST_CODE);
       String houseName = request.getParameter(AddressConstants.HOUSE_NAME);
       try
       {
          AddressFinderService addressFinderService = new AddressFinderService();
          Address address = addressFinderService.process(houseName, postCode);
          addr = address.getAddress();
          out.println(address.getAddress());
       }
       catch (AddressFinderServiceException afse)
       {
          LogWriter.logErrorMessage(afse.getMessage());
          out.println("<error>no response</error>");
       }
       finally
       {
          logAddressFinderDetails(request, houseName, postCode, addr);
          out.flush();
       }
    }

   /**
     * The doGet method.
     * Receives an HTTP GET request and handles the request by calling the doPost method.
     *
     * @param request the HttpServletRequest object that contains the request the client has
     * made of the servlet.
     * @param response the HttpServletResponse object that contains the response the servlet
     * sends to the client.
     *
     * @throws IOException the IO exception thrown if an input or output error is detected when the
     * Servlet handles the request.
     * @throws ServletException the ServletException thrown if the request could not be handled.
     */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException
    {
       doPost(request, response);
    }

   /**
    * This method logs the details like dataTime,token,sessionId,houseName,postCode
    * and address returned.
    *
    * @param request the HttpServletRequest object that contains the request the client has
    * made of the servlet.
    * @param houseName the houseName entered by the user.
    * @param postCode the postCode entered by the user.
    * @param address the Address returned for the houseName and postCode entered.
    *
    */
   private void logAddressFinderDetails(HttpServletRequest request, String houseName,
          String postCode, String address)
   {
      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      String dateTime = dateFormat.format(date);
      StringBuilder loggingInfo = new StringBuilder();
      loggingInfo.append(ADDDRESS_FINDER_SERVICE).append(COMMA)
             .append(dateTime).append(COMMA)
             .append(request.getParameter(DispatcherConstants.TOKEN)).append(COMMA)
             .append(request.getSession().getId()).append(COMMA)
             .append(houseName).append(COMMA)
             .append(postCode).append(COMMA)
             .append(address);
      LogWriter.logInfoMessage(loggingInfo.toString(), AddressFinderServlet.class.getName());
   }
}
