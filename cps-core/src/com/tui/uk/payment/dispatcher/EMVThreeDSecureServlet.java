package com.tui.uk.payment.dispatcher;

import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;

/**
 * The class EMVThreeDSecureServlet. This class is responsible for redirecting
 * to the EMV 3DS branded page using the GET method.
 *
 * @author raghunandan.d
 */
@SuppressWarnings("serial")
public class EMVThreeDSecureServlet extends CommonPaymentServlet {

	private static final String WAIT_TIME = "waitTime";

	private static final String EMV_3DS_INIT_AUTH_WAIT_TIME = "emv.3ds.init.auth.wait.time";

	private static final Integer EIGHT_THOUSAND = 8000;

	/**
	 * This method is responsible for redirecting to the EMV 3DS branded page
	 * using the GET method.
	 *
	 * @param request
	 *            the HttpServletRequest object that contains the request the
	 *            client has made of the servlet.
	 * @param resp
	 *            the HttpServletResponse object that contains the response the
	 *            servlet sends to the client.
	 * @throws ServletException
	 *             the ServletException thrown if the request for the GET could
	 *             not be handled.
	 * @throws IOException
	 *             the IOException thrown if an input or output error is
	 *             detected when the servlet handles the request
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {
		String token = request.getParameter(TOKEN);
		LogWriter.logInfoMessage(" EMV 3DS Page redirection" + " , token :"
				+ token);
		request.getSession().setAttribute(
				WAIT_TIME,
				ConfReader.getIntEntry(EMV_3DS_INIT_AUTH_WAIT_TIME,
						EIGHT_THOUSAND));
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("/common/emv3ds.jsp?b="
						+ request.getParameter("b") + "&tomcat="
						+ request.getParameter("tomcat") + "");
		requestDispatcher.forward(request, resp);
	}
}
