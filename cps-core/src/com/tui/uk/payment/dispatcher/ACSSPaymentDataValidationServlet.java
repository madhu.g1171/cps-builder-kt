/**
 * 
 */
package com.tui.uk.payment.dispatcher;


import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.ACCEPT_LANGUAGE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.DEFAULT_FAILURE_COUNT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.HYPHEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.MAX_FAILURE_COUNT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;



import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH_ACSS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CLIENT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DATACASH_REFERENCE_RS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_1;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_2;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_3;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_4;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_5;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_6;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_7;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_8;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_9;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.EXPIRY_URL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_DATACASH_RS_KEY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_SESSION_KEY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_TRANSACTION_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_URL_KEY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HOST_NAME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HPS_URL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.MERCHANT_REFERENCE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PAGE_SET_ID;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PASSWORD;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.RETURN_URL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.SESSION_ID;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.SETUP;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.exception.ValidationException;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.payment.processor.FraudScreeningProcessor;
import com.tui.uk.payment.processor.FraudScreeningProcessorImpl;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.validation.constants.ValidationConstants;
import com.tui.uk.validation.validator.NonPaymentDataValidator;
import com.tui.uk.validation.validator.Validator;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;

import com.datacash.client.Agent;
import com.datacash.client.Amount;
import com.datacash.errors.FailureReport;
import com.datacash.logging.Logger;
import com.datacash.util.XMLDocument;
/**
 * @author sreenivasulu.k
 *
 */
public class ACSSPaymentDataValidationServlet extends HttpServlet {
	
	   /** The Text/plain content type. */
	   private static final String TEXT_CONTENT_TYPE = "text/plain";

	   /** The UTF-8 encoding. */
	   private static final String UTF_ENCODING = "UTF-8";
	   
	   private static final String DATACASH_3DFATAL_ERROR = "datacash.3Dfatal.error";

	   /** The PAYMENT_DEFAULT_DATACASH_ERROR constant. */
	   private static final String PAYMENT_DEFAULT_DATACASH_ERROR = "payment.default.datacash.error";

	   /** The NONPAYMENTDATAVALIDATION_DEFAULT constant. */
	   private static final String NONPAYMENTDATAVALIDATION_DEFAULT =
	      "nonpaymentdatavalidation.default.";

	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String token = request.getParameter(TOKEN);
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		
		Map<String, String[]> requestParameterMap=request.getParameterMap();
		
		 String browserLocale = request.getHeader(ACCEPT_LANGUAGE);
		 Locale locale =
		         new Locale(StringUtils.substringBefore(browserLocale, HYPHEN),
		            StringUtils.substringAfterLast(browserLocale, HYPHEN));
		
		try{
		LogWriter.logInfoMessage(requestParameterMap+"========================================");

			if (bookingComponent.getHccsMap().get("hpsUrl")== null && bookingComponent.getHccsMap().get("hpsSession")== null) {
				setHccParameterMap(bookingComponent,requestParameterMap);
				validate(bookingComponent);
				updateContactInfo(bookingComponent);
				bookingComponent.setHccsMap(setUpReqHCS(bookingInfo, request));
				for (int i = 0; i < 2; i++) {
					if (bookingComponent.isSetUpFails()) {
						bookingComponent.setHccsMap(setUpReqHCS(bookingInfo,
								request));
					}
				}
			}
		String acssAjaxResponse;
		if(bookingComponent.isSetUpFails()){
			acssAjaxResponse="setuperror:"+bookingComponent.getHccsMap().get("setUpFailErrorUrl");

		}else{
			acssAjaxResponse="Success:"+bookingComponent.getHccsMap().get("hpsUrl")+"?HPS_SessionID="+bookingComponent.getHccsMap().get("hpsSession");
	//		https://testserver.datacash.com/hps/?HPS_SessionID=15071465
		}
		
		 sendAjaxResponse(response, acssAjaxResponse);
		
		}catch(PostPaymentProcessorException pve){
			System.out.println("Exception Occured............."+pve.getMessage());			
		            LogWriter.logErrorMessage("Exception occured at non payment datavalidation...", pve);
		           bookingComponent.setErrorMessage(pve.getMessage());
		           String errorMessage = getErrorMessage(pve.getMessage(), locale, null);
		     //      String errorMessage="Error occured at non payment data validation...............";
		            bookingInfo.setErrorFields(errorMessage);	
		            bookingInfo.getBookingComponent().setErrorMessage(errorMessage);
		            sendAjaxResponse(response, "validationerror:" + errorMessage);
		            return;
		}
		
		
	}
	
	public void setHccParameterMap(BookingComponent bookingComponent,Map<String,String[]> requestParameterMap){
		
		Map<String,String> parameterMap=bookingComponent.getNonPaymentData();
		if(parameterMap==null){
			parameterMap = new HashMap<String, String>();
		}
		
	      for (Entry<String, String[]> paramEntry : requestParameterMap.entrySet())
	      {
	    	  String entryKey = paramEntry.getKey();
	    	  if (!entryKey.startsWith(DispatcherConstants.PAYMENT)) {
	    		  parameterMap.put(paramEntry.getKey(), paramEntry.getValue()[0]);
				}
	    	 

	      }
		
	}
	
	
	
	
	
	public void validate(BookingComponent bookingComponent) throws PostPaymentProcessorException
	   {
	      // if you pass true, this method will throw exception if any
	      // validation fail.
	      // if you pass false, this method will give error map if validation
	      // fails.
	      try
	      {
	         if (ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
	            .getClientApplicationName() + ".3DSecure", false))
	         {
	            Validator validator =
	               new NonPaymentDataValidator(bookingComponent.getClientApplication());
	            validator.validate(bookingComponent.getNonPaymentData(), true);
	         }
	      }
	      catch (ValidationException ve)
	      {
	         throw new PostPaymentProcessorException(ve.getMessage(), ve);
	      }
	   }
	
	

	
	 protected void processPaymentValidationException(HttpServletRequest request,
		      HttpServletResponse response, PaymentValidationException pve, PaymentData paymentData,
		      Locale locale) throws ServletException, IOException
		   {
		      BookingInfo bookingInfo = paymentData.getBookingInfo();
		      String paymentFailureUrl = bookingInfo.getBookingComponent().getPaymentFailureURL();
		         LogWriter.logInfoMessage("payment Failure Url  is :.............. "+paymentFailureUrl);
		         sendAjaxResponse(response, paymentFailureUrl);
		      
		    /*  else
		      {
		         bookingInfo.setErrorFields(pve.getErrorFields());
		         String errorMessage =
		            getDataCashFatalErrorMessage(pve.getCode(), locale, pve.getMessage());
		         sendAjaxResponse(response, "ERROR_MESSAGE:" + errorMessage);
		      }
		      FraudScreeningProcessor fraudScreeningProcessor =
		         new FraudScreeningProcessorImpl(request.getParameter(TOKEN));
		      fraudScreeningProcessor.updateFailureCardData();*/
		      return;
		   }
	   private void sendAjaxResponse(HttpServletResponse response, String output) throws IOException
	   {
	      response.setContentType(TEXT_CONTENT_TYPE);
	      response.setCharacterEncoding(UTF_ENCODING);
	      response.getWriter().write(output);
	   }

	
	
	   protected final String getErrorMessage(String key, Locale locale, String defaultKey)
	   {
	      String[] keyData = StringUtils.split(key, '|');
	      String message = PropertyResource.getProperty(keyData[0], MESSAGES_PROPERTY, locale);
	      if (StringUtils.isNotBlank(message))
	      {
	         message = MessageFormat.format(message, (Object[]) keyData);
	      }
	      else
	      {
	         if (StringUtils.isNotBlank(defaultKey)
	            && StringUtils.equals(defaultKey, PAYMENT_DEFAULT_DATACASH_ERROR))
	         {
	            keyData[0] = PAYMENT_DEFAULT_DATACASH_ERROR;

	         }
	         else
	         {
	            keyData[0] =
	               NONPAYMENTDATAVALIDATION_DEFAULT
	                  + StringUtils.substringAfter(StringUtils.substringAfter(keyData[0],
	                     ValidationConstants.NON_PAYMENT_DATA_VALIDATION), ".");
	         }
	         message = PropertyResource.getProperty(keyData[0], MESSAGES_PROPERTY, locale);
	      }
	      return message;
	   }
	   
	   private void updateContactInfo(BookingComponent bookingComponent) {
			ContactInfo contactInfo = bookingComponent.getContactInfo();
			Map<String,String> nonPaymentData=bookingComponent.getNonPaymentData();
			contactInfo.setHouseDetails(nonPaymentData.get("houseName"));
			contactInfo.setStreetAddress1(nonPaymentData.get("addressLine1"));
			contactInfo.setStreetAddress2(nonPaymentData.get("addressLine2"));
			contactInfo.setCity(nonPaymentData.get("city"));
			contactInfo.setPostCode(nonPaymentData.get("postCode"));
			contactInfo.setCountry(nonPaymentData.get("country"));
			contactInfo.setCounty(nonPaymentData.get("county"));
			contactInfo.setPhoneNumber(nonPaymentData.get("mobilePhone"));
			contactInfo.setAltPhoneNumber(nonPaymentData.get("dayTimePhone"));
			
		}
	   
	   public Map<String,String> setUpReqHCS(BookingInfo bookingInfo, HttpServletRequest request){

			BookingComponent bookingComponent = bookingInfo.getBookingComponent();
			ContactInfo contactInfo = bookingComponent.getContactInfo();
			String merchantReference = getMerchantReference(bookingComponent);
			bookingComponent.setMerchantReference(merchantReference);

			String vTid = bookingComponent.getPaymentGatewayVirtualTerminalId();
			String password = ConfReader.getConfEntry(vTid, null);

			String returnUrl = getHccReturnURL(request);
			String expiryUrl = getHccExpiryURL(request);
			String pageSetId = getPageSetId(bookingComponent.getClientApplication().getClientApplicationName(), bookingComponent);

			Map<String, String> hcsMap = new HashMap<String, String>();
			XMLDocument xmlDocument1;
			try {
				xmlDocument1 = new XMLDocument();
				xmlDocument1.set(HCC_TRANSACTION_TYPE, SETUP);

				if (returnUrl != null && returnUrl != "") {
					xmlDocument1.set(RETURN_URL, returnUrl);
				}

				if(expiryUrl != null && expiryUrl != ""){
					xmlDocument1.set(EXPIRY_URL, expiryUrl);
				}

				if (pageSetId != null && pageSetId != "") {
					xmlDocument1.set(PAGE_SET_ID, pageSetId);
				}

				if (contactInfo.getHouseDetails() != null && contactInfo.getHouseDetails() != "") {
					xmlDocument1.set(DYN_DATA_1, contactInfo.getHouseDetails());
				}

				if (contactInfo.getStreetAddress1() != null && contactInfo.getStreetAddress1() != "") {
					xmlDocument1.set(DYN_DATA_2, contactInfo.getStreetAddress1());
				}

				if ( contactInfo.getStreetAddress2() != null &&  contactInfo.getStreetAddress2() != "") {
					xmlDocument1.set(DYN_DATA_3, contactInfo.getStreetAddress2());
				}

				if (contactInfo.getCity() != null && contactInfo.getCity() != "") {
					xmlDocument1.set(DYN_DATA_4, contactInfo.getCity());
				}

				if (contactInfo.getCountryName() != null && contactInfo.getCountryName() != "") {
					xmlDocument1.set(DYN_DATA_5, contactInfo.getCountryName());
				}

				if (contactInfo.getPostCode() != null && contactInfo.getPostCode()!="") {
					xmlDocument1.set(DYN_DATA_6, contactInfo.getPostCode());
				}
				
	         if (contactInfo.getCountry() != null && contactInfo.getCountry() != "")
	         {
	            xmlDocument1.set(DYN_DATA_8, contactInfo.getCountry());
	         }

				xmlDocument1.set(MERCHANT_REFERENCE, merchantReference);

				xmlDocument1.set(CLIENT, vTid);
				xmlDocument1.set(PASSWORD, password);

				LogWriter.logInfoMessage("**********"+xmlDocument1.getSanitizedDoc());
				XMLDocument response = null;
				try {
					response = getAgent().request(xmlDocument1, new Logger());
				} catch (FailureReport e) {
					e.printStackTrace();
				}
				String value = response.get(HPS_URL);
				String value1 = response.get(SESSION_ID);
				String hpsDatacashReference = response.get(DATACASH_REFERENCE_RS);
				String reason = response.get(DataCashServiceConstants.REASON);
				String status = response.get(DataCashServiceConstants.STATUS);

				//Handling -ve scenario for setUp Request
				String url = "";

				if (reason != "ACCEPTED" && !status.equals("1")) {
					// constructing the url for error page
					url = "/common/hccError.jsp?b=" + request.getParameter("b")
							+ "&tomcat=" + request.getParameter("tomcat") + "";
					LogWriter.logInfoMessage("url >>>--->"+ url);
					bookingComponent.setSetUpFails(true);

				}else{
					bookingComponent.setSetUpFails(false);
				}

				LogWriter.logInfoMessage(response.getSanitizedDoc());

				hcsMap.put(HCC_URL_KEY, value);
				hcsMap.put(HCC_SESSION_KEY, value1);
				hcsMap.put(HCC_DATACASH_RS_KEY, hpsDatacashReference);
				hcsMap.put(DataCashServiceConstants.SETUP_FAIL_ERROR_URL, url);
				String hccSwtch = "true";
				hcsMap.put("hccSwitch", hccSwtch);
				bookingInfo.setNewSetupReq(false);
				LogWriter.logInfoMessage("********** after new Setup Req ********* " + bookingInfo.getNewSetupReq());

			} catch (IOException e) {
				LogWriter.logErrorMessage(e.getMessage(), e);
			} catch (JDOMException e) {
				LogWriter.logErrorMessage(e.getMessage(), e);
			}
			return hcsMap;
		}


	   private Agent getAgent()
		{
			Agent agent = new Agent();
			//This is the place u would get appropriate host
			String host = ConfReader.getConfEntry(HOST_NAME, null);

			if (StringUtils.isBlank(host))
			{
				LogWriter.logErrorMessage(PropertyResource.getProperty("datacash.host.notfound",
						MESSAGES_PROPERTY));
				throw new RuntimeException(PropertyResource.getProperty("datacash.host.notfound",
						MESSAGES_PROPERTY));
			}
			agent.setHost(host);
			agent.setTimeout(ConfReader.getIntEntry("datacash.ConnectionTimeOut", 60000));
			return agent;
		}

	   private String getHccReturnURL(HttpServletRequest request) {


		   final String COLON = ":";
		   String token = request.getParameter(DispatcherConstants.TOKEN);
		   String tomIns = request.getParameter(DispatcherConstants.TOMCAT_INSTANCE);
		   String brandCode = request.getParameter("b");
		   //StringBuffer strBuffer = new StringBuffer(request.getScheme() + "://");
		   StringBuffer strBuffer =new StringBuffer(ConfReader.getConfEntry("hcc.protocol", "https")+"://");
		 // StringBuffer strBuffer=new StringBuffer("https://");
		   strBuffer.append(request.getServerName())
	      .append(Boolean.valueOf(ConfReader.getConfEntry("hcc.dev.environment", "false"))?COLON+ ConfReader.getConfEntry("hcc.dev.environment.port", "9090"):"")
		   .append(request.getContextPath())
		   .append("/hccprocess?b=").append(brandCode)
		   .append("&token=").append(token)
		   .append("&tomcat=").append(tomIns);
		return strBuffer.toString();

	   }

	   private String getHccExpiryURL(HttpServletRequest request) {


		   final String COLON = ":";
		   StringBuffer strBuffer =new StringBuffer(ConfReader.getConfEntry("hcc.protocol", "https")+"://");
		   //StringBuffer strBuffer=new StringBuffer("https://");
		   strBuffer.append(request.getServerName())
	      .append(Boolean.valueOf(ConfReader.getConfEntry("hcc.dev.environment", "false"))?COLON+ ConfReader.getConfEntry("hcc.dev.environment.port", "9090"):"")
		   .append(request.getContextPath())
		   .append("/common/").append(DispatcherConstants.HCC_EXPIRY_URL);

		return strBuffer.toString();

	   }

	   private String getPageSetId(String clientApp, BookingComponent bookingComponent)
		{

			if (null != bookingComponent.getLockYourPriceSummary()
					&& bookingComponent.getLockYourPriceSummary().isSelected()) {
				clientApp = "LYP_" + bookingComponent.getClientApplication();
			}
	         if (Boolean.valueOf(bookingComponent.getNonPaymentData().get("isMarellaBooking")))
	         {
	            clientApp = "M_" + clientApp;
	         }
	         else if (null == bookingComponent.getFlightSummary()) {
				if (!clientApp.contains("CRUISE")) {
					clientApp = clientApp + "AO";
				}
			}

			return ConfReader.getConfEntry(clientApp + "." + DispatcherConstants.PAGESET_ID, null);
		}

	    /**
	    *
	    * Gets a unique merchant reference using <code>UUID</code>.
	    *
	    * @param bookingComponent the bookingComponent.
	    *
	    * @return merchantReference the generated unique merchant reference.
	    */
	   private static String getMerchantReference(BookingComponent bookingComponent)
		{
			String clientApp = bookingComponent.getClientApplication().getClientApplicationName();
			if (clientApp.equalsIgnoreCase("NEWSKIES") || clientApp.equalsIgnoreCase("NEWSKIESM")) {
				return clientApp + '_' + bookingComponent.getNonPaymentData().get("booking_Reference_Number") + '_'
						+ StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
								MAX_REFERENCE_LENGTH_ACSS - (clientApp.length()
										+ bookingComponent.getNonPaymentData().get("booking_Reference_Number").length()));
			} else {
				return clientApp + StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
						MAX_REFERENCE_LENGTH - clientApp.length());
			}

		}
	
	   
	   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		   
		   doGet(request, response);
		   
	   }

}
