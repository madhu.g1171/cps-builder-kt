/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: CommonPaymentServlet.java$
 *
 * $Revision: $
 *
 * $Date: Jul 12, 2008$
 *
 * Author: Vijayalakshmi.d
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.dispatcher;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.DEFAULT_FAILURE_COUNT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.MAX_FAILURE_COUNT;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.validation.constants.ValidationConstants;

/**
 * This is common servlet * class for CPC and it returns * common redirection URL which can be
 * accessed by different servlets. Currently it gives Payment page URL and Post payment URL.
 *
 * @author Vijayalakshmi.d
 */

@SuppressWarnings("serial")
public class CommonPaymentServlet extends HttpServlet
{

   /** The DATACASH_3DFATAL_ERROR constant. */
   private static final String DATACASH_3DFATAL_ERROR = "datacash.3Dfatal.error";

   /** The PAYMENT_DEFAULT_DATACASH_ERROR constant. */
   private static final String PAYMENT_DEFAULT_DATACASH_ERROR = "payment.default.datacash.error";

   /** The NONPAYMENTDATAVALIDATION_DEFAULT constant. */
   private static final String NONPAYMENTDATAVALIDATION_DEFAULT =
      "nonpaymentdatavalidation.default.";

   /** The TFLYREDESIGN is used as key to access conf entry. */
   private static final String TFLY_REDESIGN = "Tfly.redesign";

   /** The WSS_PHASE1 is used as key to access conf entry. */
   private static final String WSS_PHASE1 = "WSS.WSS_PHASE1";
   
   /** flag for the FAE  **/
   public static final String CHECK_IN_PAYMENT= "isCheckinPayment";

   /**
    * This method gets the corresponding payment page URL depending on the client application.
    *
    * @param clientName ClientApplication.
    * @return URL the URL to be redirected.
    */
   protected final String getPaymentUrl(ClientApplication clientName)
   {
      String url;
      switch (clientName)
      {
         case WISHBuildYorOwn:
            url = "/shop/byo/shopPackagePayment.jsp";
            break;
         case WISHAccommodationOnly:
            url = "/shop/ao/shopAccommodationPayment.jsp";
            break;
         case ThomsonBuildYourOwn:
            url = "/thomson/byo/payment.jsp";
            break;
         case ThomsonAccommodationOnly:
            url = "/thomson/thao/thaoPayment.jsp";
            break;
         case ThomsonCruise:
            url = "/thomson/cruise/payment.jsp";
            break;
         case WISHCRUISE:
            url = "shop/cruise/cruisePayment.jsp";
            break;
         case WISHOLBP:
            url = "shop/postbooking/shopPostBookingPayment.jsp";
            break;
         case BRAC:
            url = "brac/bracPaymentPage.jsp";
            break;
         case GENERICJAVA:
            url = "/generic/genericJavaPayment.jsp";
            break;
         case GENERICDOTNET:
            url = "/generic/genericDotNetPayment.jsp";
            break;
         case NEWSKIES:
            url = "/newskies/paymentPage.jsp";
            break;
         case NEWSKIESM:
            url = "/newskiesm/paymentPage.jsp";
            break;
         case HUGOBuildYourOwn:
            url = "/hugo/hugoPaymentPage.jsp";
            break;
         case FALCONBuildYourOwn:
            url = "/falcon/falconPaymentPage.jsp";
            break;
         case PORTLAND:
            url = "/portland/payment.jsp";
            break;
         case TFLY:
            url = "/tfly/tFlyPaymentPage.jsp";
            // TODO: Once Tfly Redesign goes live, following if block should be removed.
            if (ConfReader.getBooleanEntry(TFLY_REDESIGN, false))
            {
               url = "/tflyredesign/tFlyPaymentPage.jsp";
            }
            break;
         case FCFO:
            url = "/fcfo/fcfoPaymentPage.jsp";
            break;
         case FCAO:
            url = "/fcao/fcaoPayment.jsp";
            break;
         case GREENFIELDBeach:
            url = "/greenfield/beach/beachPaymentPage.jsp";
            break;
         case GREENFIELDSimply:
            url = "/greenfield/simply/jsp/simplyPaymentPage.jsp";
            break;
         case GREENFIELDCruise:
            url = "/greenfield/cruise/cruisePaymentPage.jsp";
            break;
         case WSS:
            url = "/wss/paymentPage.jsp";
            // TODO: Once WSS phase2 goes live, following if block should be removed.
            if (ConfReader.getBooleanEntry(WSS_PHASE1, false))
            {
               url = "/wss/phase1/paymentPage.jsp";
            }
            break;
         case TRACS:
            url = "/tracs/paymentPage.jsp";
            break;
         case B2CShoreExcursions:
            url = "/shorex/b2c/paymentPage.jsp";
            break;
         case B2BShoreExcursions:
            url = "/shorex/b2b/paymentPage.jsp";
            break;
         case ThomsonFHPI:
            url = "/thomson/byoredesign/payment.jsp";
            break;
         case FCX:
            url = "/fcx/fcxPaymentPage.jsp";
            break;
         case THX:
            url = "/thx/thxPaymentPage.jsp";
            break;
         case FIRSTCHOICE:
            url = "/firstchoice/firstchoicePaymentPage.jsp";
            break;
         case THOMSON:
            url = "/thomson/thomsonPaymentPage.jsp";
            break;

         case AtcomRes:
            url = "/atcomres/atcomresPaymentPage.jsp";
            break;

         case MANAGEFIRSTCHOICE:
            url = "/managefc/paymentPage.jsp";
            break;

         case MANAGETHOMSON:
            url = "/manageth/paymentPage.jsp";
            break;

         case HYBRISFALCON:
            url = "/hybrisfalcon/falconPaymentPage.jsp";
            break;

         case HYBRISTHCRUISE:
            url = "/hybristhcruise/paymentPage.jsp";
            break;

         case HYBRISFALCONFO:
             url = "/falconfo/falconFOPaymentPage.jsp";
             break;

         case FALCONFOMOBILE:
             url = "/mobilefalconfo/mobileFalconFOPaymentPage.jsp";
             break;

         case HYBRISTHFO:
            url = "/hybristhfo/flightsOnlyPaymentPage.jsp";
            break;

         case MFLIGHTONLY:
            url = "/mflightonly/mobilePaymentPage.jsp";
            break;

         case THOMSONMOBILE:
            url = "/mthomson/mobilePaymentPage.jsp";
            break;

         case FIRSTCHOICEMOBILE:
            url = "/mfirstchoice/mobilePaymentPage.jsp";
            break;

         case FALCONMOBILE:
              url = "/mfalcon/mobilePaymentPage.jsp";
              break;

         case ANCTHOMSON:

         case ANCFIRSTCHOICE:

         case ANCTHFO:

         case ANCTHCRUISE:

         case ANCFJFO:

         case ANCFALCON:
        	 
         case ANCSKICS:
        	 
         case ANCSKIES:
            url = "/amendandcancel/ancPaymentPage.jsp";
            break;
         case CRUISEMOBILE:
             url = "/mcruise/mobilePaymentPage.jsp";
             break;

         case TUITH:
             url = "/tuith/tuithPaymentPage.jsp";
             break;

         case TUIFC:
             url = "/tuifc/tuifcPaymentPage.jsp";
             break;

         case TUIFALCON:
             url = "/tuifalcon/tuifalconPaymentPage.jsp";
             break;

         case TUITHCRUISE:
             url = "/tuithcruise/tuithcruisePaymentPage.jsp";
             break;

         case TUITHRIVERCRUISE:
             url = "/tuithcruise/tuithcruisePaymentPage.jsp";
             break;

         case TUICRUISEDEALS:
             url = "/tuicruisedeals/tuicruisedealsPaymentPage.jsp";
             break;

         case TUIHMCRUISE:
             url = "/tuihmcruise/tuihmcruisePaymentPage.jsp";
             break;

         case TUITHSHOREX:
             url = "/tuithshorex/tuithshorexPaymentPage.jsp";
             break;
         case TUITHFO:
             url = "/tuithfo/tuithfoPaymentPage.jsp";
             break;
         case TUIFALCONFO:
             url = "/tuifalconfo/tuifalconFOPaymentPage.jsp";
             break;
         case TUICS:
             url = "/tuicommon/tuicommonPaymentPage.jsp";
             break;
         case TUIES:
             url = "/tuicommon/tuicommonPaymentPage.jsp";
             break;
             
         // throw the exception since it is not valid clientapplication.
         default:
            String errorMessage =
               PropertyResource.getProperty("payment.clienturl.unknown", MESSAGES_PROPERTY)
                  + clientName;
            LogWriter.logErrorMessage(errorMessage);
            throw new IllegalArgumentException(errorMessage);
      }
      return url;
   }

   /**
    * This method gets the URL to redirect back to the client application after the successful
    * payment.
    *
    * @param clientName ClientApplication.
    * @return URL the URL to be redirected
    */
   protected final String getPostPaymentRedirectionUrl(BookingComponent bookingComponent)
   {
      final String path;
      switch (bookingComponent.getClientApplication())
      {
         case WISHBuildYorOwn:
            path = ConfReader.getConfEntry("wishbyo.postPaymentUrl", null);
            break;
         case WISHAccommodationOnly:
            path = ConfReader.getConfEntry("wishao.postPaymentUrl", null);
            break;
         case ThomsonBuildYourOwn:
            path = ConfReader.getConfEntry("thomsonbyo.postPaymentUrl", null);
            break;
         case ThomsonAccommodationOnly:
            path = ConfReader.getConfEntry("thomsonao.postPaymentUrl", null);
            break;
         case BRAC:
            path = ConfReader.getConfEntry("brac.postPaymentUrl", null);
            break;
         case WISHCRUISE:
            path = ConfReader.getConfEntry("wishcruise.postPaymentUrl", null);
            break;
         case WISHOLBP:
            path = ConfReader.getConfEntry("wisholbp.postPaymentUrl", null);
            break;
         case GENERICJAVA:
         case GENERICDOTNET:
            path = ConfReader.getConfEntry("generic.postPaymentUrl", null);
            break;
         case NEWSKIES:
            path = ConfReader.getConfEntry("newskies.postPaymentUrl", null);
            break;
         case NEWSKIESM:
            path = ConfReader.getConfEntry("newskiesm.postPaymentUrl", null);
            break;
         case KRONOSCALLCENTER:
            path = ConfReader.getConfEntry("kronoscallcenter.postPaymentUrl", null);
            break;
         case HUGOBuildYourOwn:
            path = ConfReader.getConfEntry("hugobyo.postPaymentUrl", null);
            break;
         case FALCONBuildYourOwn:
            path = ConfReader.getConfEntry("falconbyo.postPaymentUrl", null);
            break;
         case PORTLAND:
            path = ConfReader.getConfEntry("portland.postPaymentUrl", null);
            break;
         case FCAO:
            path = ConfReader.getConfEntry("fcao.postPaymentUrl", null);
            break;
         case GREENFIELDBeach:
            path = ConfReader.getConfEntry("greenfieldBeach.postPaymentUrl", null);
            break;
         case GREENFIELDSimply:
            path = ConfReader.getConfEntry("greenfieldSimply.postPaymentUrl", null);
            break;
         case GREENFIELDCruise:
            path = ConfReader.getConfEntry("greenfieldCruise.postPaymentUrl", null);
            break;
         case TRACS:
            path = ConfReader.getConfEntry("tracs.postPaymentUrl", null);
            break;
         case FCX:
            path = ConfReader.getConfEntry("fcx.postPaymentUrl", null);
            break;
         case THX:
            path = ConfReader.getConfEntry("thx.postPaymentUrl", null);
            break;
         case FIRSTCHOICE:
            path = ConfReader.getConfEntry("firstchoice.postPaymentUrl", null);
            break;
         case THOMSON:
            path = ConfReader.getConfEntry("thomson.postPaymentUrl", null);
            break;
         // throw the exception since it is not valid clientapplication.
         case AtcomRes:
            path = ConfReader.getConfEntry("atcomres.postPaymentUrl", null);
            break;
         case MANAGEFIRSTCHOICE:
            path = ConfReader.getConfEntry("managefc.postPaymentUrl", null);
            break;
         case MANAGETHOMSON:
            path = ConfReader.getConfEntry("manageth.postPaymentUrl", null);
            break;
         case HYBRISFALCON:
            path = ConfReader.getConfEntry("hybrisfalcon.postPaymentUrl", null);
            break;
         case HYBRISTHCRUISE:
            path = ConfReader.getConfEntry("hybristhcruise.postPaymentUrl", null);
            break;
         case HYBRISFALCONFO:
            path = ConfReader.getConfEntry("hybrisfalconfo.postPaymentUrl", null);
            break;
         case FALCONFOMOBILE:
             path = ConfReader.getConfEntry("falconfomobile.postPaymentUrl", null);
             break;
         case HYBRISTHFO:
             path = ConfReader.getConfEntry("hybristhfo.postPaymentUrl", null);
             break;
         case MFLIGHTONLY:
            path = ConfReader.getConfEntry("mflightonly.postPaymentUrl", null);
            break;
         case THOMSONMOBILE:
        	 if(bookingComponent.isMultiCentre()==true){
                 path = ConfReader.getConfEntry("mthomsonMC.postPaymentUrl", null);
        	 }else{
            path = ConfReader.getConfEntry("mthomson.postPaymentUrl", null);
            }
            break;
         case FIRSTCHOICEMOBILE:
            path = ConfReader.getConfEntry("mfirstchoice.postPaymentUrl", null);
            break;
         case FALCONMOBILE:
             path = ConfReader.getConfEntry("mfalcon.postPaymentUrl", null);
             break;
         case ANCTHOMSON:
            path = getFaePostPaymentUrl(bookingComponent);
            break;
         case ANCFIRSTCHOICE:
            path = getFaePostPaymentUrl(bookingComponent);
            break;
         case ANCTHFO:
            path = ConfReader.getConfEntry("amendandcancel.thfo.postPaymentUrl", null);
            break;
         case ANCTHCRUISE:
            path = ConfReader.getConfEntry("amendandcancel.thcruise.postPaymentUrl", null);
            break;
         case ANCFALCON:
            path = getFaePostPaymentUrl(bookingComponent);
            break;
         case ANCFJFO:
             path = ConfReader.getConfEntry("amendandcancel.fjfo.postPaymentUrl", null);
             break;
         case ANCSKICS:
             path = ConfReader.getConfEntry("amendandcancel.ancskics.postPaymentUrl", null);
             break;
         case ANCSKIES:
             path = ConfReader.getConfEntry("amendandcancel.ancskies.postPaymentUrl", null);
             break;
         case CRUISEMOBILE:
             path = ConfReader.getConfEntry("mcruise.postPaymentUrl", null);
             break;
         case TUITH:
             path = ConfReader.getConfEntry("tuith.postPaymentUrl", null);
             break;
         case TUIFC:
             path = ConfReader.getConfEntry("tuifc.postPaymentUrl", null);
             break;
         case TUIFALCON:
             path = ConfReader.getConfEntry("tuifalcon.postPaymentUrl", null);
             break;
         case TUITHCRUISE:
             path = ConfReader.getConfEntry("tuithcruise.postPaymentUrl", null);
             break;
         case TUITHRIVERCRUISE:
             path = ConfReader.getConfEntry("tuithrivercruise.postPaymentUrl", null);
             break;
         case TUICRUISEDEALS:
             path = ConfReader.getConfEntry("tuicruisedeals.postPaymentUrl", null);
             break;
         case TUIHMCRUISE:
             path = ConfReader.getConfEntry("tuihmcruise.postPaymentUrl", null);
             break;
         case TUITHSHOREX:
             path = ConfReader.getConfEntry("tuithshorex.postPaymentUrl", null);
             break;
         case TUITHFO:
             path = ConfReader.getConfEntry("tuithfo.postPaymentUrl", null);
             break;
             
         case TUIFALCONFO:
             path = ConfReader.getConfEntry("tuifalconfo.postPaymentUrl", null);
             break;
             
         case TUICS:
             path = ConfReader.getConfEntry("tuics.postPaymentUrl", null);
             break;
             
         case TUIES:
             path = ConfReader.getConfEntry("tuies.postPaymentUrl", null);
             break;
             

         default:
            String errorMessage =
               PropertyResource.getProperty("payment.clientredirecturl.unknown", MESSAGES_PROPERTY)
                  + bookingComponent.getClientApplication();
            LogWriter.logErrorMessage(errorMessage);
            throw new IllegalArgumentException(errorMessage);

      }
      return path;
   }
   
   private String getFaePostPaymentUrl(BookingComponent bookingComponent)
   {

      String isCheckinPayment = bookingComponent.getNonPaymentData().get(CHECK_IN_PAYMENT);
      String faeApplicableBrands = ConfReader.getConfEntry("fae.applicableBrands", null);

      if (ConfReader.getBooleanEntry("fae.paymentpage", false)
         && StringUtils.contains(faeApplicableBrands,
            bookingComponent.getClientApplication().getClientApplicationName())
         && (isCheckinPayment != null && isCheckinPayment.equalsIgnoreCase("true")))
      {

         return ConfReader.getConfEntry(
            "amendandcancel." + bookingComponent.getClientApplication() + ".fae.postPaymentUrl",
            null);

      }
      else
      {
         return ConfReader.getConfEntry(
            "amendandcancel." + bookingComponent.getClientApplication() + ".postPaymentUrl", null);
      }

   }

   protected final String getDDSetupRedirectURL(BookingComponent bookingComponent){

String key = "directdebit."+bookingComponent.getClientApplication().getClientApplicationName()+".setup.url" ;
final String  path = ConfReader.getConfEntry(key, null);

           return  path;

   }

   /**
    * This method gets the error message from the supplied key. The key may also contain some
    * dynamic data, which will be supplied as pipe separated string. In the split string, the 1st
    * element will always be the key and from the 2nd element onwards, dynamic data will be
    * considered from the 2nd element if it is available. The dynamic data will replace all {n} in
    * the message with the corresponding array element. Thus message should not have {0}, as {0}
    * will be replaced by the key itself. In the messages, dynamic data should start from
    * {1},{2},...
    *
    * @param key the key for which the message has to be fetched.
    * @param locale the <code>Locale</code> in which, the message has to be fetched.
    * @param defaultKey the default key for which the message has to be fetched.
    *
    * @return the message fetched from the properties file.
    */
   protected final String getErrorMessage(String key, Locale locale, String defaultKey)
   {
      String[] keyData = StringUtils.split(key, '|');
      String message = PropertyResource.getProperty(keyData[0], MESSAGES_PROPERTY, locale);
      if (StringUtils.isNotBlank(message))
      {
         message = MessageFormat.format(message, (Object[]) keyData);
      }
      else
      {
         if (StringUtils.isNotBlank(defaultKey)
            && StringUtils.equals(defaultKey, PAYMENT_DEFAULT_DATACASH_ERROR))
         {
            keyData[0] = PAYMENT_DEFAULT_DATACASH_ERROR;

         }
         else
         {
            keyData[0] =
               NONPAYMENTDATAVALIDATION_DEFAULT
                  + StringUtils.substringAfter(StringUtils.substringAfter(keyData[0],
                     ValidationConstants.NON_PAYMENT_DATA_VALIDATION), ".");
         }
         message = PropertyResource.getProperty(keyData[0], MESSAGES_PROPERTY, locale);
      }
      return message;
   }

   /**
    * This method is responsible for getting fatal error message.
    *
    * @param errorCode the error code.
    * @param locale the Locale object.
    * @param key the key.
    *
    * @return error message.
    */
   protected String getDataCashFatalErrorMessage(int errorCode, Locale locale, String key)
   {
      String message = null;
      if (DataCashServiceConstants.ERRORCODE_FOR_FATAL_ERRRORS == errorCode)
      {
         message = PropertyResource.getProperty(DATACASH_3DFATAL_ERROR, MESSAGES_PROPERTY, locale);
      }
      else
      {
         message = getErrorMessage(key, locale, PAYMENT_DEFAULT_DATACASH_ERROR);
      }
      return message;
   }

   /**
    * This method is responsible for verifying whether the session id from request and that stored
    * in tracking data are equal. If not will throw an IllegalArgumentException.
    *
    * @param bookingInfo the BookingInfo.
    * @param sessionId the session id.
    *
    */
   protected void verifySessionId(BookingInfo bookingInfo, String sessionId)
   {
      LogWriter.logInfoMessage("Session from BookingInfo : "
         + bookingInfo.getTrackingData().getSessionId());
      LogWriter.logInfoMessage("Session from Request : " + sessionId);
      if (!bookingInfo.getTrackingData().getSessionId().equalsIgnoreCase(sessionId))
      {
         String errorMessage =
            PropertyResource.getProperty("booking.sessionId.unknown", MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new IllegalArgumentException(errorMessage);
      }
   }

   /**
    * This method is responsible for setting tracking data with sessionId and token if it is null,
    * otherwise will verify the session id.
    *
    * @param bookingInfo the BookingInfo.
    * @param token the token.
    * @param sessionId the session id.
    * @param ipAddress the ip address.
    *
    */
   protected void updateSessionId(BookingInfo bookingInfo, String token, String sessionId,
      String ipAddress)
   {
      if (bookingInfo.getTrackingData() == null)
      {
         TransactionTrackingData trackingData = new TransactionTrackingData(token, sessionId);
         trackingData.setIpAddress(ipAddress);
         bookingInfo.setTrackingData(trackingData);
      }
      else
      {
         verifySessionId(bookingInfo, sessionId);
      }
   }

   /**
    * This method is responsible for forming the postPaymentUrl.
    *
    * @param bookingComponent the object which holds booking component.
    * @param url the post payment url.
    *
    * @return String the postPaymentUrl.
    */
   protected String getPostPaymentUrl(BookingComponent bookingComponent, String url)
   {
      if (StringUtils.isBlank(url))
      {
         url = bookingComponent.getPaymentSuccessUrl();
         if (StringUtils.isBlank(url))
         {
            url =
               bookingComponent.getClientDomainURL()
                  + getPostPaymentRedirectionUrl(bookingComponent);
         }
      }
      LogWriter.logInfoMessage("URL " + url);
      return url;
   }

   /**
    * This method is responsible for deciding whether to redirect to payment failure url or not
    *  when an exception occurs.
    *
    * @param paymentData the object which holds booking information and payment data.
    * @param paymentFailureUrl the payment failure url.
    *
    * @return boolean
    */
   protected Boolean isPaymentFailureRedirect(PaymentData paymentData, String paymentFailureUrl)
   {
      String clientApplication =
         paymentData.getBookingInfo().getBookingComponent().getClientApplication()
            .getClientApplicationName();
      int failureCount = ConfReader.getIntEntry(clientApplication + "." + MAX_FAILURE_COUNT, 0);
      if (failureCount == 0)
      {
         failureCount =
            ConfReader.getIntEntry("default." + MAX_FAILURE_COUNT, DEFAULT_FAILURE_COUNT);
      }
      if (paymentFailureUrl != null && paymentData.getFailureCount() >= failureCount)
      {
         return Boolean.TRUE;
      }
      return Boolean.FALSE;
   }

   /**
    * This method is responsible for setting the error message and resetting the card charge if
    * present.
    *
    * @param bookingInfo the bookingInfo.
    * @param errorMessage the error message.
    */
   protected void processException(BookingInfo bookingInfo, String errorMessage)
   {
      bookingInfo.getBookingComponent().setErrorMessage(errorMessage);
      if (bookingInfo.getCalculatedCardCharge() != null)
      {
         bookingInfo.resetCardCharge();
      }
   }

   /**
    * This method builds the absolute payment page url to which redirection happens.
    *
    * @param request http servlet request
    *
    * @return the absolute payment page url.
    */
   protected String paymentPageAbsoluteUrl(HttpServletRequest request)
   {
      String termUrl =
         StringUtils.substringBefore(request.getRequestURL().toString(), request.getContextPath())
            + request.getContextPath() + "/pageRender" + "?token="
            + request.getParameter(DispatcherConstants.TOKEN) + DispatcherConstants.BRAND_VALUE
            + request.getParameter(DispatcherConstants.BRAND) + DispatcherConstants.TOMCAT_VALUE
            + request.getParameter(DispatcherConstants.TOMCAT_INSTANCE);
      return ConfReader.getConfEntry("termURLProtocol", DispatcherConstants.HTTPS) + ":"
         + StringUtils.substringAfter(termUrl, ":");
   }

   /**
    * This method builds the relative payment page url for forwarding the request.
    *
    * @param request http servlet request
    *
    * @return the relative payment page url.
    */
   protected String paymentPageRelativeUrl(HttpServletRequest request)
   {
      return "/pageRender" + "?token=" + request.getParameter(DispatcherConstants.TOKEN)
         + DispatcherConstants.BRAND_VALUE + request.getParameter(DispatcherConstants.BRAND)
         + DispatcherConstants.TOMCAT_VALUE
         + request.getParameter(DispatcherConstants.TOMCAT_INSTANCE);
   }

   /**
    * This method is to give prepayment url if the price changes.
    *
    * @param bookingComponent token form client app
    * @return prepayment url
    */
   public String priceChangeUrl(BookingComponent bookingComponent)
   {
      String priceChangeUrl = "";
      Boolean priceStatus = bookingComponent.getPriceStatus();
      if (priceStatus != null && priceStatus)
      {
         priceChangeUrl = bookingComponent.getPrePaymentUrl();
      }
      return priceChangeUrl;
   }
}