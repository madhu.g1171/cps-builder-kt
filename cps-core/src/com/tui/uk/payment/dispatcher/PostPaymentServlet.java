/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: PaymentServlet.java,v $
 * 
 * $Revision: 1.5 $
 * 
 * $Date: 2008-05-15 08:25:26 $
 * 
 * $Author: thomas.pm $
 * 
 * 
 * $Log: not supported by cvs2svn $ Revision 1.4 2008/05/14 09:57:21 veena.bn added validation
 * methods.
 */
package com.tui.uk.payment.dispatcher;

import static com.tui.uk.payment.dispatcher.DispatcherConstants.ACCEPT_LANGUAGE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.BRAND;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.BRAND_VALUE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.HTTPS;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.HYPHEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.POST_AUTH_SERVLET;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.PRE_AUTHENTICATION_SESSION_TIMEOUT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.REFERER;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.SESSION_MAX_INTERVAL;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.THREE_D_SECURE_CONF_VALUE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOMCAT_INSTANCE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOMCAT_VALUE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.USER_AGENT;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.payment.processor.FraudScreeningProcessor;
import com.tui.uk.payment.processor.FraudScreeningProcessorImpl;
import com.tui.uk.payment.processor.PostPaymentProcessor;
import com.tui.uk.payment.processor.PostPaymentProcessorFactory;

/**
 * The Class PaymentServlet. This class is used to read the value of the fields in the payment
 * section and set the values in the payment object.
 * 
 * @author thomas.pm
 */
@SuppressWarnings("serial")
public class PostPaymentServlet extends CommonPaymentServlet
{
   /** The COLON. */
   private static final String COLON = ":";

   /** The auth failed code. */
   private static final int AUTH_FAILED_CODE = 7;
   
   /** The Text/plain content type. */
   private static final String TEXT_CONTENT_TYPE = "text/plain";

   /** The UTF-8 encoding. */
   private static final String UTF_ENCODING = "UTF-8";

   /**
    * This method is responsible for performing all the necessary action that has to be taken once
    * the payment page is submitted.
    * 
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    * 
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the POST could not be
    *            handled.
    */
   @SuppressWarnings("unchecked")
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      String token = request.getParameter(TOKEN);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
      BookingInfo bookingInfo = paymentData.getBookingInfo();
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();

      verifySessionId(bookingInfo, request.getSession().getId());

      // If price changes due to browser back and forward buttons
      String priceChangeUrl = priceChangeUrl(bookingComponent);
      if (StringUtils.isNotBlank(priceChangeUrl))
      {
         response.sendRedirect(priceChangeUrl);
         return;
      }
      
      boolean isHccTrue = ConfReader.getBooleanEntry(
    	         bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
      
      request.setAttribute("postPaymentUrl", paymentPageAbsoluteUrl(request));
      // Skipping the ajax call to fix the browser back issue from confirmation page ..
      if (!bookingInfo.getNewHoliday() && !isHccTrue)
      {
         response.sendRedirect(request.getHeader(DispatcherConstants.REFERER));
         return;
      }
      if (bookingInfo.getThreeDAuth() && paymentData.getPayment().getPaymentTransactions() != null)
      {
         response.sendRedirect(getPostPaymentUrl(bookingComponent, null));
         return;
      }

      resetBookingInfo(bookingInfo);

      String paymentFailureUrl = bookingComponent.getPaymentFailureURL();

      // If the failure count is greater than maximum failure count, then the application
      // should redirect to payment failure page and should stop processing.
      if (isPaymentFailureRedirect(paymentData, paymentFailureUrl))
      {
         response.sendRedirect(paymentFailureUrl);
         return;
      }

      PostPaymentProcessor postPaymentProcessor =
         PostPaymentProcessorFactory.getPostPaymentProcessor(
            bookingComponent.getClientApplication(), paymentData, request.getParameterMap());
      String url = null;
      String browserLocale = request.getHeader(ACCEPT_LANGUAGE);
      Locale locale =
         new Locale(StringUtils.substringBefore(browserLocale, HYPHEN),
            StringUtils.substringAfterLast(browserLocale, HYPHEN));

      try
      {
         postPaymentProcessor.preProcess();
         postPaymentProcessor.process();

         // doing card holder verification in 3D secure transactions.
         // for 3d secure transaction URl will be bank URL.
         // if it is not 3D secure transactions, URL will be null.
         url =
            postPaymentProcessor.postProcess(buildTermUrl(request), request.getHeader(USER_AGENT));

         logThreeDData(postPaymentProcessor.getThreeDData(), token, bookingComponent
            .getClientApplication().getClientApplicationName());
         // perform fraud screening only for datacash payment transaction.
         if (paymentData.getPayment().getPaymentTransactions(PaymentMethod.DATACASH).size() > 0)
         {
            processFraudScreening(url, token, bookingComponent);
         }

      }
      catch (PostPaymentProcessorException ppe)
      {
    	  if (isHccTrue)
          {
             sendAjaxResponse(response, getHccErrorMessage(ppe));
             String errorMessage = getErrorMessage(ppe.getMessage(), locale, null);
             LogWriter.logErrorMessage(errorMessage, ppe);
             return;
          }
         String errorMessage = getErrorMessage(ppe.getMessage(), locale, null);
         LogWriter.logErrorMessage(errorMessage, ppe);
         processException(bookingInfo, errorMessage);
         response.sendRedirect(request.getHeader(REFERER));
         return;
      }
      catch (PaymentValidationException pve)
      {
    	  if (isHccTrue)
          {
             processHccPaymentException(request, response, paymentData);
             sendAjaxResponse(response, getHccErrorMessage(pve));
             return;
          }
         String errorMessage =
            getDataCashFatalErrorMessage(pve.getCode(), locale, pve.getMessage());
         LogWriter.logErrorMessage(errorMessage, pve);
         processException(bookingInfo, errorMessage);
         processPaymentValidationException(request, response, pve, paymentData);
         return;
      }

      if (StringUtils.isNotBlank(url)
         && ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
            .getClientApplicationName() + THREE_D_SECURE_CONF_VALUE, false))
      {
         // while moving to the bank page increase session timeout to 1hr.
         request.getSession().setMaxInactiveInterval(
            ConfReader.getIntEntry(PRE_AUTHENTICATION_SESSION_TIMEOUT, SESSION_MAX_INTERVAL));

         setSessionData(postPaymentProcessor, request, url);

         StringBuilder threedsUrl = new StringBuilder();
         threedsUrl = buildThreeDSUrl(request, threedsUrl);

         threedsRedirect(request, response, threedsUrl);
      }
      else
      {
         // if URL is null getting post payment url from bookingComponent or conf file.
         url = getPostPaymentUrl(bookingComponent, url);
         response.sendRedirect(url);
      }
   }
   
   private String getHccErrorMessage(Exception exception)
   {

      String errorMsg = "";
      if (exception instanceof PostPaymentProcessorException)
      {
         errorMsg = ConfReader.getConfEntry("hcc.validation.error", exception.getMessage());

      }
      else if (exception instanceof PaymentValidationException)
      {
         errorMsg = ConfReader.getConfEntry("hcc.datacash.error", exception.getMessage());
      }
      else
      {
         errorMsg = ConfReader.getConfEntry("hcc.generic.error", exception.getMessage());
      }

      return errorMsg;
   }
   
   private void sendAjaxResponse(HttpServletResponse response, String output) throws IOException
   {
      response.setContentType(TEXT_CONTENT_TYPE);
      response.setCharacterEncoding(UTF_ENCODING);
      response.getWriter().write(output);
   }

   /**
    * This method is used to redirect to 3D Secure pagen.
    * 
    * @param response HttpResponse
    * @param threedsUrl 3D Secure URL
    * @param clientApplicationName brand name
    */
   protected void threedsRedirect(HttpServletRequest request, HttpServletResponse response,
      StringBuilder threedsUrl) throws IOException, ServletException
   {
      response.sendRedirect(ConfReader.getConfEntry("termURLProtocol", DispatcherConstants.HTTPS)
         + COLON + StringUtils.substringAfter(threedsUrl.toString(), COLON));
   }

   /**
    * This method is used to log 3D data and cps token.
    * 
    * @param threeDData threed data map
    * @param token cps token
    * @param clientApplicationName brand name
    */
   protected void logThreeDData(Map<String, String> threeDData, String token,
      String clientApplicationName)
   {
      if (threeDData != null && !threeDData.isEmpty())
      {
         LogWriter.logInfoMessage("MD :" + threeDData.get(DispatcherConstants.MD) + "  CPS Token :"
            + token + "  Brand Name:" + clientApplicationName);
         LogWriter.logInfoMessage("Term url :" + threeDData.get(DispatcherConstants.TERMURL)
            + "  CPS Token :" + token + "  Brand Name:" + clientApplicationName);
      }

   }

   /**
    * This method is used to build the 3D security url.
    * 
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param threedsUrl the 3D security url.
    * 
    * @return threedsUrl
    */
   protected StringBuilder buildThreeDSUrl(HttpServletRequest request, StringBuilder threedsUrl)
   {
      threedsUrl.append(StringUtils.substringBefore(request.getRequestURL().toString(),
         request.getContextPath()));
      threedsUrl.append(request.getContextPath());
      threedsUrl.append(DispatcherConstants.THREE_D_SERVLET);
      threedsUrl.append(request.getParameter(DispatcherConstants.TOKEN));
      threedsUrl.append(DispatcherConstants.BRAND_VALUE);
      threedsUrl.append(request.getParameter(DispatcherConstants.BRAND));
      threedsUrl.append(DispatcherConstants.TOMCAT_VALUE);
      threedsUrl.append(request.getParameter(DispatcherConstants.TOMCAT_INSTANCE));
      return threedsUrl;
   }

   /**
    * This method is processing PaymentValidationException.
    * 
    * @param request the HttpServletRequest object.
    * @param response the HttpServletResponse object.
    * @param pve the PaymentValidationException object.
    * @param paymentData the PaymentData object.
    * 
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the POST could not be
    *            handled.
    */
   protected void processPaymentValidationException(HttpServletRequest request,
      HttpServletResponse response, PaymentValidationException pve, PaymentData paymentData)
      throws ServletException, IOException
   {
      BookingInfo bookingInfo = paymentData.getBookingInfo();
      String paymentFailureUrl = bookingInfo.getBookingComponent().getPaymentFailureURL();
      if (StringUtils.isNotBlank(paymentFailureUrl))
      {
         paymentData.setFailureCount();
      }
      if (isPaymentFailureRedirect(paymentData, paymentFailureUrl))
      {
         response.sendRedirect(paymentFailureUrl);
      }
      else
      {
         bookingInfo.setErrorFields(pve.getErrorFields());
         response.sendRedirect(request.getHeader(REFERER));
      }
      FraudScreeningProcessor fraudScreeningProcessor =
         new FraudScreeningProcessorImpl(request.getParameter(TOKEN));
      fraudScreeningProcessor.updateFailureCardData();
      return;
   }

   /**
    * This method build termUrl.
    * 
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * 
    * @return termUrl the termUrl.
    */
   protected String buildTermUrl(HttpServletRequest request)
   {
      StringBuilder termUrl = new StringBuilder();
      termUrl.append(StringUtils.substringBefore(request.getRequestURL().toString(),
         request.getContextPath()));
      termUrl.append(request.getContextPath());
      termUrl.append(POST_AUTH_SERVLET);
      termUrl.append(request.getParameter(TOKEN));
      termUrl.append(BRAND_VALUE);
      termUrl.append(request.getParameter(BRAND));
      termUrl.append(TOMCAT_VALUE);
      termUrl.append(request.getParameter(TOMCAT_INSTANCE));
      return ConfReader.getConfEntry("termURLProtocol", HTTPS) + COLON
         + StringUtils.substringAfter(termUrl.toString(), COLON);
   }

   /**
    * If authorization is successful, fraud screening request processing will be done through this
    * method.
    * 
    * @param url the bank url.
    * @param token the token.
    * @param bookingComponent the booking component.
    * 
    * @throws PaymentValidationException if fraud screening application rejects.
    */
   protected void processFraudScreening(String url, String token, BookingComponent bookingComponent)
      throws PaymentValidationException
   {
      if (url == null
         && StringUtils.isNotBlank(bookingComponent.getPaymentGatewayVirtualTerminalId()))
      {
         FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
         if (!fraudScreeningProcessor.processFraudScreening())
         {
            throw new PaymentValidationException(AUTH_FAILED_CODE, "Not authorised");
         }
      }
   }

   /**
    * Resets the booking info object's details.
    * 
    * @param bookingInfo the BookingInfo object.
    */
   protected void resetBookingInfo(BookingInfo bookingInfo)
   {
      if (bookingInfo.getNewHoliday())
      {
         bookingInfo.reset();
      }

      // Set the error field to blank.
      bookingInfo.setErrorFields("");
      bookingInfo.getBookingComponent().setErrorMessage("");

      // Set the fraud screening response to blank.
      bookingInfo.setFraudScreeningResponse(StringUtils.EMPTY);
   }

   /**
    * Sets the necessary session data.
    * 
    * @param postPaymentProcessor the PostPaymentProcessor.
    * @param request the HttpServletRequest.
    * @param url the url.
    */
   protected void setSessionData(PostPaymentProcessor postPaymentProcessor,
      HttpServletRequest request, String url)
   {
      HttpSession session = request.getSession();
      if (!postPaymentProcessor.getThreeDData().isEmpty())
      {
         session.setAttribute(DispatcherConstants.MD,
            postPaymentProcessor.getThreeDData().get(DispatcherConstants.MD));
         session.setAttribute(DispatcherConstants.TERMURL, postPaymentProcessor.getThreeDData()
            .get(DispatcherConstants.TERMURL));
         session.setAttribute(DispatcherConstants.PAREQ,
            postPaymentProcessor.getThreeDData().get(DispatcherConstants.PAREQ));
         session.setAttribute(DispatcherConstants.ACSURL, url);
      }
   }
   
   protected void processHccPaymentException(HttpServletRequest request,
		      HttpServletResponse response, PaymentData paymentData) throws ServletException, IOException
		   {
		      BookingInfo bookingInfo = paymentData.getBookingInfo();
		      String paymentFailureUrl = bookingInfo.getBookingComponent().getPaymentFailureURL();
		      if (StringUtils.isNotBlank(paymentFailureUrl))
		      {
		         paymentData.setFailureCount();
		      }
		      FraudScreeningProcessor fraudScreeningProcessor =
		         new FraudScreeningProcessorImpl(request.getParameter(TOKEN));
		      fraudScreeningProcessor.updateFailureCardData();
		      return;
		   }

}