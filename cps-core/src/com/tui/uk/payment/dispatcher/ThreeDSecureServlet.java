/*
 * Copyright (C)2006 TUI UK Ltd
 *
 * TUI UK Ltd,
 * Columbus House,
 * Westwood Way,
 * Westwood Business Park,
 * Coventry,
 * United Kingdom
 * CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence
 * any actual or intended publication of this source code.
 *
 * $RCSfile:   ThreeDSecureServlet.java$
 *
 * $Revision:   $
 *
 * $Date:   18 Mar 2010$
 *
 * Author: ganapna
 *
 *
 * $Log:   $
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tui.uk.domain.BookingInfo;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;

/**
 * The class ThreeDSecureServlet. This class is responsible for redirecting to the 3DS branded page
 * using the GET method.
 *
 * @author ganapna
 */
@SuppressWarnings("serial")
public class ThreeDSecureServlet extends CommonPaymentServlet
{

   /**The constant for successful return code.*/
   public static final String RETURNCODE_SUCCESS = "001";

   /**The constant for threeDS return code.*/
   public static final String RETURNCODE_THREEDS = "150";

   /**
    * This method is responsible for redirecting to the 3DS branded page using the GET method.
    *
    * @param request the HttpServletRequest object that contains the request the client has made of
    *        the servlet.
    * @param resp the HttpServletResponse object that contains the response the servlet sends to the
    *        client.
    * @throws ServletException the ServletException thrown if the request for the GET could not be
    *         handled.
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *         servlet handles the request
    * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
    *      javax.servlet.http.HttpServletResponse)
    */
   @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse resp)
      throws ServletException, IOException
   {
      // resp.sendRedirect(req.getContextPath() + "/common/bankForwardPage.jsp");

      PaymentData paymentData =
         PaymentStore.getInstance().getPaymentData(
            UUID.fromString(request.getParameter(DispatcherConstants.TOKEN)));
      BookingInfo bookingInfo = paymentData.getBookingInfo();

      verifySessionId(bookingInfo, request.getSession().getId());
      request.setAttribute("postPaymentUrl", paymentPageAbsoluteUrl(request));
      request.setAttribute("paymentUrl", paymentPageAbsoluteUrl(request));
      request.getSession().setAttribute("visiturl", "yes");
      //This is introduced as part of Shorex Rebranding , The form data as to be Not POST but GET to Confirmation page
     
      if (!bookingInfo.getNewHoliday())
      {
         RequestDispatcher requestDispatcher =
            request.getRequestDispatcher(paymentPageRelativeUrl(request));
         requestDispatcher.forward(request, resp);
         return;
      }

      if (paymentData.getPayment() != null)
      {
         DataCashPaymentTransaction paymentTransaction = (DataCashPaymentTransaction)
                  paymentData.getPayment().getPaymentTransactions().get(0);

         if( !(paymentTransaction.getDataCashResponseCode().equals(RETURNCODE_SUCCESS)
                  || paymentTransaction.getDataCashResponseCode().equals(RETURNCODE_THREEDS)))
         {
            RequestDispatcher requestDispatcher =
                     request.getRequestDispatcher(paymentPageRelativeUrl(request));
                  requestDispatcher.forward(request, resp);
                  return;
         }
      }

      RequestDispatcher requestDispatcher =
         request.getRequestDispatcher("/common/bankForwardPage.jsp?b=" + request.getParameter("b")
            + "&tomcat=" + request.getParameter("tomcat") + "");
      requestDispatcher.forward(request, resp);
   }
}
