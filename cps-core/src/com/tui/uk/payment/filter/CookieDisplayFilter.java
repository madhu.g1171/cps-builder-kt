package com.tui.uk.payment.filter;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;

/**
 * This class is used for handling cookie legislation related cookie.
 * 
 * @author rajarao.r
 * 
 */
public class CookieDisplayFilter implements Filter
{
   /**
    * API specific method.
    * 
    * @see javax.servlet.Filter#destroy()
    */
   public void destroy()
   {
      // TODO Auto-generated method stub

   }

   /**
    * API specific method used for check the cookie .
    * 
    * @param req ServletRequest object.
    * @param resp ServletResponse object .
    * @param fc FilterChain object.
    * @throws IOException IOException.
    * @throws ServletException ServletException.
    * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
    *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
    */
   public void doFilter(ServletRequest req, ServletResponse resp, FilterChain fc)
      throws IOException, ServletException
   {
      HttpServletRequest httpRequest = (HttpServletRequest) req;

      HttpServletResponse httpResponse = (HttpServletResponse) resp;

      String clientappbrand = null;

      String token = httpRequest.getParameter(DispatcherConstants.TOKEN);

      String code = (String) httpRequest.getParameter("b");

      String bCode = (String) ConfReader.getConfEntry("brandcode." + code, "");

      if (token != null)
      {
         UUID uuid = null;
         try
         {
            uuid = UUID.fromString(token);

         }
         catch (NumberFormatException nfe)
         {
            if (bCode != null)
            {

               String cookieswitch = ConfReader.getConfEntry(bCode

               + ".CookieLegislationSwitch", "false");

               httpRequest.setAttribute("cookieswitch", cookieswitch);

            }
            if (this.checkForCookie(httpRequest, BookingConstants.COOKIE_NAME))
            {
               httpRequest.setAttribute("cookiepresent", "true");

            }
            // TODO throw exeption
            nfe.printStackTrace();

         }

         if (uuid != null)
         {

            PaymentData paymentData = PaymentStore.getInstance()

            .getPaymentData(uuid);

            if (paymentData != null)
            {

               BookingInfo bookingInfo = paymentData.getBookingInfo();

               BookingComponent bookingComponent = bookingInfo

               .getBookingComponent();

               ClientApplication cl = bookingComponent

               .getClientApplication();

               clientappbrand = cl.getClientApplicationName();

               String cookieswitch = ConfReader.getConfEntry(

               clientappbrand + ".CookieLegislationSwitch",

               "false");

               httpRequest.setAttribute("cookieswitch", cookieswitch);

            }
            else if (bCode != null)
            {

               String cookieswitch =
                  ConfReader.getConfEntry(bCode + ".CookieLegislationSwitch", "false");

               httpRequest.setAttribute("cookieswitch", cookieswitch);

            }
            else
            {

               httpRequest.setAttribute("cookieswitch", "true");

            }

         }

      }
      else if (bCode != null)
      {

         String cookieswitch = ConfReader.getConfEntry(bCode + ".CookieLegislationSwitch", "false");

         httpRequest.setAttribute("cookieswitch", cookieswitch);

      }
      else
      {

         httpRequest.setAttribute("cookieswitch", "true");

      }

      if (this.checkForCookie(httpRequest, BookingConstants.COOKIE_NAME))
      {
         httpRequest.setAttribute("cookiepresent", "true");

      }

      fc.doFilter(httpRequest, httpResponse);

   }

   /**
    * API specific method .
    * 
    * @param fconfig Filterconfig object.
    * @throws ServletException ServletException.
    * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
    */
   public void init(FilterConfig fconfig) throws ServletException
   {
      // TODO Auto-generated method stub

   }

   /**
    * This method is used for checking if the cookie present or not.
    * 
    * @param request HttpServletRequest.
    * @param cookieName legislation cookieName.
    * @return boolean.
    */
   private boolean checkForCookie(HttpServletRequest request, String cookieName)
   {

      if (request != null && request.getCookies() != null)
      {
         for (Cookie cookie : request.getCookies())
         {
            if (cookie != null)
            {
               if (StringUtils.equalsIgnoreCase(cookie.getName(), cookieName))
               {
                  return true;
               }
            }
         }
      }
      return false;
   }

}
