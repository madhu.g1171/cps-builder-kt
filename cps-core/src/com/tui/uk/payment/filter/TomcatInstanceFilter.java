/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: TomcatInstanceFilter.java,v $
 *
 * $Revision: 1.0$
 *
 * $Date: 2008-08-12 07:59:25$
 *
 * $author: bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.filter;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.lang.StringUtils;

/**
 * The servlet filter to identify which tomcat instance is used to service the
 * XML-RPC requests.
 *
 * @author bibin.j@sonata-software.com
 */
public final class TomcatInstanceFilter implements Filter {
	/**
	 * This is the name of the configuration parameter.This parameter is set in the
	 * server.xml.
	 */
	private static final String CONFIG_PARAMETER_NAME = "jvmRoute";

	/**
	 * The name of the cookie set by this filter.
	 */
	private static final String COOKIE_NAME = "tomcat";

	/**
	 * The saved FilterConfig that we have to use for this filter.
	 */
	private FilterConfig filterConfig = null;

	/**
	 * The Tomcat instance id that is configured for this instance of Tomcat. This
	 * must be present.
	 */
	private String tomcatInstance = null;

	/**
	 * Initialize the filter. Get the tomcat instance id from the servlet context.
	 *
	 * @param filterConf the FilterConfig.
	 * @throws ServletException the Exception
	 */
	public void init(FilterConfig filterConf) throws ServletException {
		this.filterConfig = filterConf;
		tomcatInstance = filterConfig.getServletContext().getInitParameter(CONFIG_PARAMETER_NAME);
		if (tomcatInstance == null) {
			throw new ServletException(
					"No 'tomcat-instance' parameter specified in the server 'Context' " + "(in server.xml)");
		}
	}

	/**
	 * Destroy the filter. There is nothing we have to do when this filter is
	 * destroyed.
	 */
	public void destroy() {
		this.filterConfig = null;
	}

	/**
	 * Process the servlet request/response. The only change we make is to replace
	 * the response with one that has the Tomcat instance id attached to it as a
	 * cookie.
	 *
	 * @param request  the ServletRequest.
	 * @param response the ServletResponse.
	 * @param chain    the FilterChain.
	 * @throws IOException      the Exception
	 * @throws ServletException the Exception
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (filterConfig == null) {
			return;
		}
		MetadataResponse modifiedResponse = new MetadataResponse((HttpServletResponse) response);
	//	addSameSiteForSessionCookie((HttpServletRequest) request, (HttpServletResponse) response);
		chain.doFilter(request, modifiedResponse);		

	}

	/**
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * This adds Secure,HttpOnly and SameSite=none attributes to the JSESSIONID cookie  in the response Header.
	 * This is introduced to make our application compliant with the Chrome's new SameSite-by-default cookie
	 * behavior which by default sets the cookie samesite attribute to Lax by default if no value is set. Due to this 
	 * the session cookie is not being sent in cross site request and creates a new session for it when we got the response.
	 * Thus the validation fails when compared with the session in booking info.
	 * Setting it to secure and samesite=none will allow the cookie to be sent in cross site request. 
	 */
	private void addSameSiteForSessionCookie(HttpServletRequest request, HttpServletResponse response) {
		Cookie jsessionIdCookie = getCookie(request, "JSESSIONID");
		if (Objects.isNull(jsessionIdCookie)) {
			response.addHeader("Set-Cookie", "JSESSIONID=" + request.getSession().getId() + "; Secure;Path="
					+ request.getContextPath() + "; HttpOnly; SameSite=none");
		}
	}

	/**
	 * @param request
	 * @param cookieName
	 * @return the cookie if the provided cookie name present in request with the
	 *         path same as that of the content path else returns null.
	 */
	public Cookie getCookie(HttpServletRequest request, String cookieName) {
		if (request.getCookies() != null) {
			for (Cookie cookie : request.getCookies()) {
				if (StringUtils.equalsIgnoreCase(cookie.getName(), cookieName)
						&& StringUtils.equalsIgnoreCase(request.getContextPath(), cookie.getPath())) {
					return cookie;
				}
			}
		}
		return null;
	}

	/**
	 * Wrapper for the response. We add the tomcat instance id to the metadata of
	 * the response.
	 */
	private final class MetadataResponse extends HttpServletResponseWrapper {
		/**
		 * The MetadataResponse method.
		 * 
		 * @param response the HttpServletResponse.
		 */
		private MetadataResponse(HttpServletResponse response) {
			super(response);
			Cookie cookie = new Cookie(COOKIE_NAME, tomcatInstance);
			addCookie(cookie);
		}

	}

}
