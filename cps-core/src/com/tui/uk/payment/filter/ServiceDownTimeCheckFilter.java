/*
 * Copyright (C)2015 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: ServiceDownTimeCheckFilter.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2015-07-13 15:43:00 $
 * 
 * $Author: atul.rao@sonata-software.com $
 * 
 * $Log: $
 */
package com.tui.uk.payment.filter;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.FlightDetails;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyConstants;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;

/**
 * The servlet filter to check for TRACS/VISION down time for the applications configured in
 * cps.conf. During the down time, if there is a request for rendering the payment page or making
 * the payment or post authentication, it will be redirected to payment page with the error message
 * and the payment button will be disabled.
 * 
 * @author atul.rao
 */
public class ServiceDownTimeCheckFilter implements Filter
{
   /**
    * Destroy the filter. There is nothing we have to do when this filter is destroyed.
    */
   public void destroy()
   {
      // TODO Auto-generated method stub
   }

   /**
    * Process the servlet request/response. If the token and bookingComponent are available, the
    * isDepartureDateInvalid method is called to check for departure date within 31-Oct-2015 and
    * checkForDownTime method is called to check for TRACS/VISION down time. The applications for
    * which this check needs to be made is specified in cps.conf.
    *
    * @param servletRequest the ServletRequest.
    * @param servletResponse the ServletResponse.
    * @param filterChain the FilterChain.
    * 
    * @throws IOException the Exception
    * @throws ServletException the Exception
    */
   public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException
   {
      // The flag to check whether the filter chain call should be skipped or not.
      BookingComponent bookingComp = null;

      HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
      HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
      String clientAppBrand = null;
      String token = httpServletRequest.getParameter(DispatcherConstants.TOKEN);
      if (token != null)
      {
         UUID uuid = null;
         try
         {
            uuid = UUID.fromString(token);
         }
         catch (NumberFormatException nfe)
         {
            LogWriter.logErrorMessage("Error while getting the uuid from token : " + token + ". "
               + nfe.getMessage());
         }
         if (uuid != null)
         {
            PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
            if (paymentData != null)
            {
               BookingInfo bookingInfo = paymentData.getBookingInfo();
               if (bookingInfo != null)
               {
                  bookingComp = bookingInfo.getBookingComponent();
                  if (bookingComp != null && bookingComp.getClientApplication() != null)
                  {

                     clientAppBrand = bookingComp.getClientApplication()
                                                     .getClientApplicationName();
                     if (clientAppBrand != null)
                     {
                        String startTimeAMPM = StringUtils.EMPTY;
                        String endTimeAMPM = StringUtils.EMPTY;
                        boolean downTimeSwitch =
                           ConfReader.getBooleanEntry(clientAppBrand + "."
                              + PropertyConstants.DOWNTIME_SWITCH, false);
                        if (downTimeSwitch)
                        {
                           /*
                            * The flag to check whether the departure date is invalid or not. 
                            * If the departure date is on or before the configured departure 
                            * date it is invalid.
                            */
                           boolean invalidDepartureDate = false;
                           LogWriter.logInfoMessage("Down time switch is set to "
                              + "true for the application : " + clientAppBrand + " .Token : "
                              + token);
                           String[] thomsonAndFirstChoiceArray =
                              new String[] { "THOMSON", "FIRSTCHOICE", "HYBRISFALCON" };

                           /*
                            * Get the List of applications from cps.conf for which the departure
                            * date validation needs to be done.
                            */
                           String[] deptDateCheckAppArray =
                              ConfReader.getStringValues(
                                 PropertyConstants.DEPARTUREDATEVALIDATON_ARRAY,
                                 thomsonAndFirstChoiceArray, ",");
                           List<String> deptDateCheckAppList = null;
                           if (deptDateCheckAppArray != null && deptDateCheckAppArray.length != 0)
                           {
                              deptDateCheckAppList =
                                 new ArrayList<String>(Arrays.asList(deptDateCheckAppArray));
                           }
                           if (deptDateCheckAppList.contains(clientAppBrand))
                           {
                              /*
                               * Check if departure date is on or before the configured departure
                               * date or not.
                               */
                              LogWriter.logInfoMessage("Check if departure date is"
                                 + " on or before the configured departure date"
                                 + " or not for the application : " + clientAppBrand + " .Token : "
                                 + token);
                              invalidDepartureDate =
                                 isDepartureDateInvalid(bookingComp, clientAppBrand);
                           }
                           if ((deptDateCheckAppList.contains(clientAppBrand)
                                && invalidDepartureDate)
                              || (!deptDateCheckAppList.contains(clientAppBrand)))
                           {
                              // Get the TRACS/VISION down time range from cps.conf
                              String[] timeRangeArray =
                                 ConfReader.getStringValues(clientAppBrand + "."
                                    + PropertyConstants.DOWNTIME_TIMERANGE, null, ",");
                              if (timeRangeArray != null && timeRangeArray.length == 2)
                              {
                                 try
                                 {
                                    SimpleDateFormat simpleDateFormat =
                                       new SimpleDateFormat("HH:mm:ss");
                                    SimpleDateFormat simpleDateFormatNoSeconds =
                                       new SimpleDateFormat("HH:mm");
                                    SimpleDateFormat simpleDateFormatAMPM =
                                       new SimpleDateFormat("hh:mm a");
                                    String startTimeString = timeRangeArray[0];
                                    String endTimeString = timeRangeArray[1];
                                    String[] startTimeStringArray = startTimeString.split(":");
                                    String[] endTimeStringArray = startTimeString.split(":");
                                    Date startTime = null;
                                    Date endTime = null;

                                    if (startTimeStringArray.length == 3)
                                    {
                                       startTime = simpleDateFormat.parse(startTimeString);
                                    }
                                    else
                                    {
                                       startTime = simpleDateFormatNoSeconds
                                                         .parse(startTimeString);
                                    }
                                    startTimeAMPM = simpleDateFormatAMPM.format(startTime);

                                    if (endTimeStringArray.length == 3)
                                    {
                                       endTime = simpleDateFormat.parse(endTimeString);
                                    }
                                    else
                                    {
                                       endTime = simpleDateFormatNoSeconds.parse(endTimeString);
                                    }
                                    endTimeAMPM = simpleDateFormatAMPM.format(endTime);

                                    /*
                                     * If checkForDownTime returns true, redirect to payment page
                                     * and show the down time error message else call the next
                                     * filter.
                                     */
                                    if (checkForDownTime(startTime, endTime))
                                    {
                                       LogWriter.logInfoMessage("The current time falls "
                                          + "in the down time range" + " for the application : "
                                          + clientAppBrand + " .Token : " + token
                                          + ". Redirect to payment page with error message.");
                                       String downTimeErrorMsg =
                                          PropertyResource.getProperty("downtime" + "."
                                             + clientAppBrand + "." + "errorMsg",
                                             PropertyConstants.MESSAGES_PROPERTY);

                                       httpServletRequest
                                          .setAttribute("disablePaymentButton", true);
                                       httpServletRequest.setAttribute(
                                          "downTimeErrorMsg",
                                          updateDownTimeErrorMessage(httpServletRequest,
                                             bookingComp, clientAppBrand, downTimeErrorMsg,
                                             startTimeAMPM, endTimeAMPM));

                                       if (!httpServletRequest.getRequestURI().contains(
                                          "pageRender"))
                                       {
                                          httpServletRequest.setAttribute("RedirectToPaymentPage",
                                             "RedirectToPaymentPage");
                                          if (!clientAppBrand.equals("THOMSON")
                                             && !clientAppBrand.equals("FIRSTCHOICE")
                                             && !clientAppBrand.equals("HYBRISFALCON"))
                                          {
                                             if (!httpServletRequest.getRequestURI().contains(
                                                "postAuthServlet"))
                                             {
                                                if (httpServletRequest
                                                   .getHeader(DispatcherConstants.REFERER) != null
                                                   && httpServletRequest.getHeader(
                                                      DispatcherConstants.REFERER).contains(
                                                      "pageRender"))
                                                {
                                                   LogWriter
                                                      .logInfoMessage("Redirecting to payment page"
                                                         + " with error message "
                                                         + "for the application : "
                                                         + clientAppBrand + " .Token : " + token);
                                                   httpServletResponse
                                                      .sendRedirect(httpServletRequest
                                                         .getHeader(DispatcherConstants.REFERER));
                                                   return;
                                                }
                                                else
                                                {
                                                   String pageRenderURL = StringUtils.EMPTY;
                                                   pageRenderURL =
                                                      pageRenderURL
                                                         + httpServletRequest.getScheme() + "://"
                                                         + httpServletRequest.getServerName();
                                                   if (httpServletRequest.getServerPort() != 80)
                                                   {
                                                      pageRenderURL =
                                                         pageRenderURL + ":"
                                                            + httpServletRequest.getServerPort();
                                                   }
                                                   if ("ThomsonCruise".equals(clientAppBrand))
                                                   {
                                                      pageRenderURL =
                                                         pageRenderURL + "/cps/webcruise/"
                                                            + "pageRender" + "?"
                                                            + httpServletRequest.getQueryString();
                                                   }
                                                   else if ("FALCONBYO".equals(clientAppBrand))
                                                   {
                                                      pageRenderURL =
                                                         pageRenderURL + "/cps/fcfalcon/"
                                                            + "pageRender" + "?"
                                                            + httpServletRequest.getQueryString();
                                                   }
                                                   else
                                                   {
                                                      pageRenderURL =
                                                         pageRenderURL + "/cps/pageRender" + "?"
                                                            + httpServletRequest.getQueryString();
                                                   }

                                                   LogWriter
                                                      .logInfoMessage("Constructing the pageRender"
                                                         + " URL and redirecting to payment page"
                                                         + " with error message for the"
                                                         + " application : " + clientAppBrand
                                                         + " .Token : " + token);
                                                   httpServletResponse.sendRedirect(pageRenderURL);
                                                   return;
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                                 catch (ParseException pe)
                                 {
                                    LogWriter.logErrorMessage("Error while parsing the"
                                       + " start time or end time" + " from cps.conf file. "
                                       + pe.getMessage() + " .Token : " + token);
                                 }
                              }
                              else
                              {
                                 LogWriter.logErrorMessage("Error while fetching the"
                                    + " start time or end time"
                                    + " from cps.conf file. The start and end time needs to be"
                                    + " configured in cps conf file in the format"
                                    + " starttime,endtime" + " in HH:mm:ss 24 hour format"
                                    + " .Token : " + token);
                              }
                           }

                        }
                        else
                        {
                           LogWriter
                              .logInfoMessage("Down time switch is false for the application : "
                                 + clientAppBrand + " .Token : " + token);
                        }
                     }
                  }
               }
            }
         }
      }

      filterChain.doFilter(httpServletRequest, httpServletResponse);
   }

   /**
    * Initialize the filter. There is nothing we have to do when this filter is initialized.
    *
    * @param fconfig the FilterConfig.
    * 
    * @throws ServletException the Exception
    */
   public void init(FilterConfig fconfig) throws ServletException
   {
      // TODO Auto-generated method stub
   }

   /**
    * Checks if the current time falls in the range of start time and end time.
    *
    * @param startDownTime the down time start range.
    * @param endDownTime the down time end range.
    * 
    * @return isCurrentTimeDown the boolean
    */
   public boolean checkForDownTime(Date startDownTime, Date endDownTime)
   {
      boolean isCurrentTimeDown = false;
      try
      {
         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
         Date currentTime = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
         Date zeroTime = simpleDateFormat.parse("00:00:00");
         Date maxTime = simpleDateFormat.parse("23:59:59");
         if (startDownTime.getTime() > endDownTime.getTime())
         {
            if (((currentTime.getTime() > startDownTime.getTime())
                  && (currentTime.getTime() <= maxTime.getTime()))
               || ((currentTime.getTime() >= zeroTime.getTime())
                    && (currentTime.getTime() < endDownTime.getTime())))
            {
               isCurrentTimeDown = true;
            }
         }
         else
         {
            if ((currentTime.getTime() > startDownTime.getTime())
               && (currentTime.getTime() < endDownTime.getTime()))
            {
               isCurrentTimeDown = true;
            }
         }
      }
      catch (ParseException pe)
      {
         LogWriter.logErrorMessage("Error while parsing the current time. " + pe.getMessage());
      }

      return isCurrentTimeDown;
   }

   /**
    * Check if departure date is valid or not, i.e., on or before the configured departure date or
    * not. The applications for which this check needs to be made is specified in cps.conf.
    *
    * @param bookComponent the BookingComponent.
    * @param clientAppName the Client Application Name.
    * 
    * @return departureDateInvalid the boolean
    */
   public boolean isDepartureDateInvalid(BookingComponent bookComponent, String clientAppName)
   {
      boolean departureDateInvalid = false;
      try
      {
         if ((bookComponent != null) && (bookComponent.getFlightSummary() != null))
         {
            List<FlightDetails> outBoundFlightList =
               (List<FlightDetails>) bookComponent.getFlightSummary().getOutboundFlight();
            Iterator<FlightDetails> outBoundFlightIterator = null;
            SimpleDateFormat departureDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String departureCheckDateString =
               ConfReader.getConfEntry(PropertyConstants.DOWNTIME_DEPARTUREDATE,StringUtils.EMPTY);
            Date departureCheckDate = departureDateFormat.parse(departureCheckDateString);
            if ((outBoundFlightList != null) && (outBoundFlightList.size() != 0))
            {
               outBoundFlightIterator = outBoundFlightList.iterator();
               while (outBoundFlightIterator.hasNext())
               {
                  FlightDetails flightDetails = outBoundFlightIterator.next();
                  if (flightDetails != null)
                  {
                     if (flightDetails.getDepartureDateTime() != null)
                     {
                        Date departureDate = flightDetails.getDepartureDateTime();
                        if (departureDate != null)
                        {
                           if (!departureDate.after(departureCheckDate))
                           {
                              LogWriter.logInfoMessage(clientAppName + " : Departure date "
                                 + departureDate + " is within " + departureCheckDateString);
                              departureDateInvalid = true;
                           }
                           break;
                        }
                     }
                  }
               }
            }
         }
      }
      catch (ParseException pe)
      {
         LogWriter.logErrorMessage("Error while parsing the departure date"
            + " from the cps config file. The departure date should be configured"
            + " in the format dd-MM-yyyy HH:mm:ss . " + pe.getMessage());
      }

      return departureDateInvalid;
   }

   /**
    * Update the down time error message with formatted start time, 
    * formatted end time and home page link.
    * 
    * @param httpRequest the HttpServletRequest
    * @param bookingComp the BookingComponent
    * @param clientName the Client Application Name
    * @param downTimeMsg the Down Time Error Message
    * @param startTimeAmPm the formatted start time
    * @param endTimeAmPm the formatted end time
    * 
    * @return downTimeMsg
    */
   public String updateDownTimeErrorMessage(HttpServletRequest httpRequest,
      BookingComponent bookingComp, String clientName, String downTimeMsg, String startTimeAmPm,
      String endTimeAmPm)
   {
      if (downTimeMsg != null && !StringUtils.isEmpty(downTimeMsg))
      {
         String downTimeHomePageURL = null;

         if ("WSS".equals(clientName))
         {
            if ("19000".equalsIgnoreCase(httpRequest.getParameter("b")))
            {
               downTimeHomePageURL =
                  ConfReader.getConfEntry(clientName + ".thomson."
                     + PropertyConstants.DOWNTIME_HOMEPAGE_URL, StringUtils.EMPTY);
            }
            else if ("21000".equalsIgnoreCase(httpRequest.getParameter("b")))
            {
               downTimeHomePageURL =
                  ConfReader.getConfEntry(clientName + ".fcfalcon."
                     + PropertyConstants.DOWNTIME_HOMEPAGE_URL, StringUtils.EMPTY);
            }
            else
            {
               downTimeHomePageURL =
                  ConfReader.getConfEntry(clientName + ".fcsun."
                     + PropertyConstants.DOWNTIME_HOMEPAGE_URL, StringUtils.EMPTY);
            }
         }
         else
         {
            downTimeHomePageURL =
               ConfReader.getConfEntry(clientName + "." + PropertyConstants.DOWNTIME_HOMEPAGE_URL,
                  StringUtils.EMPTY);
         }

         if (startTimeAmPm != null)
         {
            downTimeMsg = downTimeMsg.replace("_starttime_", startTimeAmPm.trim());
         }
         if (endTimeAmPm != null)
         {
            downTimeMsg = downTimeMsg.replace("_endtime_", endTimeAmPm.trim());
         }

         if (downTimeHomePageURL != null)
         {
            downTimeMsg = downTimeMsg.replace("_homepagelink_", downTimeHomePageURL);
         }
         else
         {
            if (bookingComp != null)
            {
               if ((bookingComp.getClientURLLinks() != null)
                  && (bookingComp.getClientURLLinks().getHomePageURL() != null))
               {
                  downTimeMsg =
                     downTimeMsg.replace("_homepagelink_", bookingComp.getClientURLLinks()
                        .getHomePageURL());
               }
            }
         }
      }
      else
      {
         downTimeMsg = StringUtils.EMPTY;
      }

      return downTimeMsg;
   }
}
