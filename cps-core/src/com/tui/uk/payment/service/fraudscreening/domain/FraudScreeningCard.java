/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

import java.util.Arrays;

/**
 * This Class Represents Cruise Details.
 *
 * @author Jaleel
 */
public class FraudScreeningCard
{

   /** Holds the start index for masking the card number. */
   private static final int MASK_START_INDEX = 6;

   /** Holds the end index for masking the card number. */
   private static final int MASK_END_INDEX = 4;

   /** Card number.A String of 13 to 19 digits. */
   private char[] pan;

   /** The name on card. */
   private String nameOncard;

   /** The expire date. */
   private String expiryDate;

   /**
    * The constructor.
    *
    * @param pan the pan.
    * @param expiryDate the expiryDate.
    * @param nameOncard the nameOncard.
    *
    */

   public FraudScreeningCard(char[] pan, String nameOncard, String expiryDate)
   {
      this.pan = new char[pan.length];
      for (int i = 0; i < pan.length; i++)
      {
         this.pan[i] = pan[i];
      }
      this.nameOncard = nameOncard;
      this.expiryDate = expiryDate;
   }

   /**
    * Gets the expire date.
    *
    * @return the expiryDate
    */
   public String getExpiryDate()
   {
      return expiryDate;
   }

   /**
    * Gets the name on card.
    *
    * @return the nameOncard
    */
   public String getNameOncard()
   {
      return nameOncard;
   }

   /**
    * Purges sensitive information.
    */
   public void purgeSensitiveCardData()
   {
      Arrays.fill(pan, '*');
      pan = null;
      expiryDate = null;
      nameOncard = null;
   }

   /**
    * Gets masked card number.
    *
    * @return the masked card number.
    */
   public String getMaskedCardNumber()
   {
      StringBuilder maskedCardNumber =
         new StringBuilder(String.valueOf(pan));
      for (int i = MASK_START_INDEX; i < pan.length - MASK_END_INDEX; i++)
      {
         maskedCardNumber.setCharAt(i, '*');
      }
      return maskedCardNumber.toString();
   }

   /**
    * Method to populate all CardNumber.
    *
    * @param isLogging XML is for logging or not.
    *
    * @return the CardNumber details.
    */
   public String toString(boolean isLogging)
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<cardNumber>");

      if (isLogging)
      {
         stringBuilder.append(getMaskedCardNumber());
      }
      else
      {
         stringBuilder.append(pan);
      }

      stringBuilder.append("</cardNumber>");

      return stringBuilder.toString();
   }
}
