/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This Class Represents Cruise Details.
 * @author Jaleel
 */

public class Cruise
{
   /** The cruise name. */
   private String cruiseName;

   /** The sellingCode. */
   private String sellingCode;

   /** The portOfCallResort. */
   private String portOfCallResort;

   /** The stayDuration. */
   private String stayDuration;

   /**
    * The constructor.
    *
    * @param cruiseName the cruiseName.
    * @param sellingCode the sellingCode.
    * @param portOfCallResort the portOfCallResort.
    * @param stayDuration the stayDuration.
    *
    */
   public Cruise(String cruiseName, String sellingCode, String portOfCallResort,
           String stayDuration)
   {
      this.cruiseName = cruiseName;
      this.sellingCode = sellingCode;
      this.portOfCallResort = portOfCallResort;
      this.stayDuration = stayDuration;
   }

   /**
    * Method to populate all Cruise details.
    *
    * @return the cruise details.
    */
   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<Cruise>");
      stringBuilder.append("<cruiseName>").append(cruiseName).append("</cruiseName>");
      stringBuilder.append("<sellingCode>").append(sellingCode).append("</sellingCode>");
      stringBuilder.append("<portOfCallResort>")
        .append(portOfCallResort).append("</portOfCallResort>");
      stringBuilder.append("<stayDuration>").append(stayDuration).append("</stayDuration>");
      stringBuilder.append("</Cruise>");

      return stringBuilder.toString();
   }
}
