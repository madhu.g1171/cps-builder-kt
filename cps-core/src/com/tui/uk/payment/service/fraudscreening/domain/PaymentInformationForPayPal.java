package com.tui.uk.payment.service.fraudscreening.domain;

import com.tui.uk.payment.service.fraudscreening.FraudScreeningCardData;

/**
 * This class represents Payment information for PayPal service required for fraudscrrening
 * @author tanveer.bp
 *
 */
public class PaymentInformationForPayPal {

	private String countrycode;
	private String email;
	private String firstname;
	private String lastname;
	private String middlename;
	private String payerid;
	private String payerstatus;
	private String paymentstatus;
	private String paymenttype;
	/**
	 * @return the countrycode
	 */
	public String getCountrycode() {
		return countrycode;
	}
	/**
	 * @param countrycode the countrycode to set
	 */
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the middlename
	 */
	public String getMiddlename() {
		return middlename;
	}
	/**
	 * @param middlename the middlename to set
	 */
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	/**
	 * @return the payerid
	 */
	public String getPayerid() {
		return payerid;
	}
	/**
	 * @param payerid the payerid to set
	 */
	public void setPayerid(String payerid) {
		this.payerid = payerid;
	}
	/**
	 * @return the payerstatus
	 */
	public String getPayerstatus() {
		return payerstatus;
	}
	/**
	 * @param payerstatus the payerstatus to set
	 */
	public void setPayerstatus(String payerstatus) {
		this.payerstatus = payerstatus;
	}
	/**
	 * @return the paymentstatus
	 */
	public String getPaymentstatus() {
		return paymentstatus;
	}
	/**
	 * @param paymentstatus the paymentstatus to set
	 */
	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}
	/**
	 * @return the paymenttype
	 */
	public String getPaymenttype() {
		return paymenttype;
	}
	/**
	 * @param paymenttype the paymenttype to set
	 */
	public void setPaymenttype(String paymenttype) {
		this.paymenttype = paymenttype;
	}
	
	/**
	    * Method to populate all PaymentInformation.
	    *
	    * @param isLogging XML is for logging or not.
	    *
	    * @return the PaymentInformation.
	    */
	   public String toString(boolean isLogging)
	   {
	      StringBuilder stringBuilder = new StringBuilder();
	      stringBuilder.append("<paymentInformation>");
	      stringBuilder.append("<paypalInfo>");
	      stringBuilder.append("<countrycode>").append(countrycode).append("</countrycode>");
	      stringBuilder.append("<email>").append(email).append("</email>");
	      stringBuilder.append("<firstname>").append(firstname).append("</firstname>");
	      stringBuilder.append("<lastname>").append(lastname).append("</lastname>");
	      stringBuilder.append("<middlename>").append(middlename).append("</middlename>");
	      stringBuilder.append("<payerid>").append(payerid).append("</payerid>");
	      stringBuilder.append("<payerstatus>").append(payerstatus).append("</payerstatus>");
	      stringBuilder.append("<paymentstatus>").append(paymentstatus).append("</paymentstatus>");
	      stringBuilder.append("<paymenttype>").append(paymenttype).append("</paymenttype>");
	      stringBuilder.append("</paypalInfo>");
	      stringBuilder.append("</paymentInformation>");

	      return stringBuilder.toString();
	   }
}
