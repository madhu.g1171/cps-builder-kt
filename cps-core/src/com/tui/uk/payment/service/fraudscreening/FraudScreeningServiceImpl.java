/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening;

import java.io.IOException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;

import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.service.fraudscreening.domain.OrderInformation;
import com.tui.uk.payment.service.fraudscreening.exception.FraudScreeningServiceException;

/**
 * This Class Represents FraudScreeningServiceImpl.
 * 
 * @author Jaleel
 */
public class FraudScreeningServiceImpl
{

	/** The FraudScreeningRequest. */
	private FraudScreeningRequest fraudScreeningRequest;

	/** The counter. */
	private int counter = 0;

	/** The connection time out. */
	private static final int CONNECTION_TIME_OUT = 10000;

	/**
	 * The constructor.
	 * 
	 * @param fraudScreeningRequest the fraudScreeningRequest.
	 */
	public FraudScreeningServiceImpl(FraudScreeningRequest fraudScreeningRequest)
	{
		this.fraudScreeningRequest = fraudScreeningRequest;
	}

	/**
	 * Method to invoke fraudscreen application.
	 * 
	 * @return the FraudScreeningResponse.
	 * 
	 * @throws FraudScreeningServiceException if connection times out
	 */
	public FraudScreeningResponse send() throws FraudScreeningServiceException
	{
		return send(fraudScreeningRequest.toString(),
				ConfReader.getConfEntry("fraudscreening.url", ""));
	}

	/**
	 * Method to invoke fraud screen application.
	 * 
	 * @param xmlRequest the xmlRequest.
	 * @param fraudScreenUrl fraudscreening stub url.
	 * 
	 * @return the FraudScreeningResponse.
	 * 
	 * @throws FraudScreeningServiceException if there is an exception during connection.
	 */
	private FraudScreeningResponse send(String xmlRequest, String fraudScreenUrl)
			throws FraudScreeningServiceException
			{
		// Turn off Apache HttpClient request body logging to prevent PAN number in debug logs.
		Logger.getLogger("httpclient.wire.content").setLevel(org.apache.log4j.Level.OFF);
		String nxmlrequest = updateReservedCharacter(xmlRequest);
		String userName = ConfReader.getConfEntry("fraudscreen.username", "tui import user test");
		String password = ConfReader.getConfEntry("fraudscreen.password", "5Hastamu");

		int recoveryattempts = ConfReader.getIntEntry("fraudscreen.recoveryattempts", 0);
		boolean recoverymode = ConfReader.getBooleanEntry("fraudscreen.recoverymode", false);

		int connectionTimeOut =
				ConfReader.getIntEntry("fraudscreening.connection.timeout", CONNECTION_TIME_OUT);
		HttpClient httpClient = null;
		int totalConnectionAttempts = ConfReader.getIntEntry("fraudscreening.connection.attempts", 2);

		HttpClientParams httpClientParams = new HttpClientParams();
		httpClientParams.setConnectionManagerTimeout(connectionTimeOut);
		httpClientParams.setSoTimeout(connectionTimeOut);
		httpClient = new HttpClient(httpClientParams);
		//Fixed timeout issue by adding connection establish timeout parameter.
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(connectionTimeOut);
		httpClient.getParams().setAuthenticationPreemptive(true);
		// Override default recovery mechanism of apache http client.
		httpClient.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler(recoveryattempts, recoverymode));

		httpClient.getState().setCredentials(
				new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM),
				new UsernamePasswordCredentials(userName, password));

		PostMethod postMethod = null;
		postMethod = new PostMethod(fraudScreenUrl);
		RequestEntity requestEntity1 = null;
		FraudScreeningResponse fraudScreeningResponse = null;

		try
		{
			requestEntity1 = new StringRequestEntity("content", null, null);
			postMethod.setRequestEntity(requestEntity1);
			postMethod.setRequestHeader("Content-type", "text/xml; charset=utf-8");

			httpClient.getParams().setAuthenticationPreemptive(true);
			httpClient.getState().setCredentials(
					new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM),
					new UsernamePasswordCredentials(userName, password));

			RequestEntity requestEntity = new StringRequestEntity(nxmlrequest, "text/xml", "utf-8");
			postMethod.setRequestEntity(requestEntity);
			httpClient.executeMethod(postMethod);
			LogWriter.logInfoMessage("Req sent to Accertify");
			return new FraudScreeningResponse(postMethod.getResponseBodyAsString());
		}
		catch (IOException e)
		{
			counter++;
			if (counter <= totalConnectionAttempts)
			{
				LogWriter.logInfoMessage("Trying to Send the request. Attempt # " + counter);
				//Fixed for [Ticket#2016021210000701], Response obj created and assign to send()
				fraudScreeningResponse = send(nxmlrequest, fraudScreenUrl);
			}
			else
			{
				throw new FraudScreeningServiceException(e);
			}
		}
		finally
		{
			postMethod.releaseConnection();
		}
		return fraudScreeningResponse;
			}

	/**
	 * This method is used to convert xml reserved characters into hexadecimal value.
	 * 
	 * @param originalUnprotectedString original string.
	 * @return updated string.
	 */
	public String updateReservedCharacter(String originalUnprotectedString)
	{
		boolean anyCharactersProtected = false;

		StringBuffer stringBuffer = new StringBuffer();
		for (int initialValue = 0; initialValue < originalUnprotectedString.length(); initialValue++)
		{
			char individualChar = originalUnprotectedString.charAt(initialValue);

			boolean controlCharacter = individualChar < 32;
			boolean unicodeButNotAscii = individualChar > 126;
			boolean reservChar = individualChar == '&';

			if (reservChar || unicodeButNotAscii || controlCharacter)
			{
				stringBuffer.append("&#" + (int) individualChar + ";");
				anyCharactersProtected = true;
			}
			else
			{
				stringBuffer.append(individualChar);
			}
		}
		if (!anyCharactersProtected)
		{
			return originalUnprotectedString;
		}

		return stringBuffer.toString();
	}

	/**
	 * This method is to give FraudScreeningResponse object by setting the OrderInformation.
	 * 
	 * @param orderInfo Represents order information.
	 * 
	 * @return the FraudScreeningResponse object.
	 * 
	 * @throws FraudScreeningServiceException if fraud screening process fails.
	 */
	public FraudScreeningResponse send(OrderInformation orderInfo)
			throws FraudScreeningServiceException
			{
		fraudScreeningRequest.setOrderInformation(orderInfo);
		return send(fraudScreeningRequest.toString(),
				ConfReader.getConfEntry("fraudscreening.bookingdetails.url", ""));
			}

	/**
	 * This method is to purge sensitive card data.
	 */
	public void purgeSensitiveCardData()
	{
		fraudScreeningRequest.purgeCardData();
	}

	/**
	 * Logs the fraud screening request.
	 * 
	 * @param transactionType the transaction type.
	 * @param status the status.
	 */
	public void logFraudScreeningRequest(String transactionType, String status)
	{
		fraudScreeningRequest.logRequest(transactionType, status);
	}
	/**
	 * Method to invoke fraudscreen application for paypal transaction.
	 * 
	 * @return the FraudScreeningResponse.
	 * 
	 * @throws FraudScreeningServiceException if connection times out
	 */
	public FraudScreeningResponse sendToAccertify() throws FraudScreeningServiceException
	{
		return send(fraudScreeningRequest.toString("PayPal Transaction"),
				ConfReader.getConfEntry("fraudscreening.url", ""));
	}
}
