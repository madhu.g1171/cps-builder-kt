/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This Class Represents Passenger Details.
 * @author Jaleel
 */


public class Passenger
{
   /** The cruise name. */
   private String passengerTitle;

   /** The sellingCode. */
   private String passengerFirstName;

   /** The portOfCallResort. */
   private String passengerLastName;

   /** The stayDuration. */
   private String paxAgeGroup;

   /** The cruise name. */
   private String isLead;

   /** The ShippingInformation. */
   private ContactInformation shippingInformation;

   /**
    * The constructor.
    *
    * @param passengerTitle the passengerTitle.
    * @param passengerFirstName the passengerFirstName.
    * @param passengerLastName the passengerLastName.
    * @param paxAgeGroup the paxAgeGroup.
    * @param isLead the isLead.
    *
    */
   public Passenger(String passengerTitle, String passengerFirstName, String passengerLastName,
           String paxAgeGroup, String isLead)
   {
      this.passengerTitle = passengerTitle;
      this.passengerFirstName = passengerFirstName;
      this.passengerLastName = passengerLastName;
      this.paxAgeGroup = paxAgeGroup;
      this.isLead = isLead;
   }

   /**
    * Gets the ShippingInformation.
    * @return the ShippingInformation.
   */
   public ContactInformation getShippingInformation()
   {
      return shippingInformation;
   }

   /**
    * Sets the ShippingInformation.
    * @param shippingInformation the ShippingInformation.
   */
   public void setShippingInformation(ContactInformation shippingInformation)
   {
      this.shippingInformation = shippingInformation;
   }

   /**
    * Method to populate all ShippingInformation details.
    *
    * @return the ShippingInformation details.
    */
   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<passenger>");
      stringBuilder.append("<passengerTitle>").append(passengerTitle).append("</passengerTitle>");
      stringBuilder.append("<passengerFirstName>")
      .append(passengerFirstName).append("</passengerFirstName>");
      stringBuilder.append("<passengerLastName>")
      .append(passengerLastName).append("</passengerLastName>");
      stringBuilder.append("<paxAgeGroup>").append(paxAgeGroup).append("</paxAgeGroup>");
      stringBuilder.append("<isLead>").append(isLead).append("</isLead>");
      if (shippingInformation != null)
      {
         stringBuilder.append(shippingInformation.toString());
      }
      stringBuilder.append("</passenger>");

      return stringBuilder.toString();
   }

}
