package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This class represents Payment information for PayPal service required for fraudscrrening
 * @author tanveer.bp
 *
 */
public class PaymentInformationForDirectDebit
{

	private String bankAccount;
	private String sortCode;
	private String firstName;
	private String lastName;

	public String getBankAccount()
	{
		return bankAccount;
	}

	public void setBankAccount(String bankAccount)
	{
		this.bankAccount = bankAccount;
	}

	public String getSortCode()
	{
		return sortCode;
	}

	public void setSortCode(String sortCode)
	{
		this.sortCode = sortCode;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	    * Method to populate all PaymentInformation.
	    *
	    * @param isLogging XML is for logging or not.
	    *
	    * @return the PaymentInformation.
	    */
	   public String toString(boolean isLogging)
	   {
	      StringBuilder stringBuilder = new StringBuilder();
	      stringBuilder.append("<paymentInformation>");
	      stringBuilder.append("<ddInfo>");
	      stringBuilder.append("<bankaccount>").append(bankAccount).append("</bankaccount>");
	      stringBuilder.append("<sortcode>").append(sortCode).append("</sortcode>");
	      stringBuilder.append("</ddInfo>");
	      stringBuilder.append("</paymentInformation>");
	      return stringBuilder.toString();
	   }
}
