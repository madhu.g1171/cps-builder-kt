/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This Class Represents Flight Details.
 * @author Jaleel
 */

public class Flight
{
   /** The airlineName name. */
   private String airlineName;

   /** The departFlightNumber. */
   private String outBoundDepartFlightNumber;

   /** The departTime. */
   private String outBoundDepartTime;

   /** The departCity. */
   private String outBoundDepartCity;

   /** The departAirportCode name. */
   private String outBoundDepartAirportCode;

   /** The arriveTime. */
   private String outBoundArriveTime;

   /** The arriveCity. */
   private String outBoundArriveCity;

   /** The arriveAirportCode. */
   private String outBoundArriveAirportCode;

   /** The departFlightNumber. */
   private String inBoundDepartFlightNumber;

   /** The departTime. */
   private String inBoundDepartTime;

   /** The departCity. */
   private String inBoundDepartCity;

   /** The departAirportCode name. */
   private String inBoundDepartAirportCode;

   /** The arriveTime. */
   private String inBoundArriveTime;

   /** The arriveCity. */
   private String inBoundArriveCity;

   /** The arriveAirportCode. */
   private String inBoundArriveAirportCode;

   /** The ticketType. */
   private String ticketType;


   /**
    * The constructor.
    *
    * @param airlineName the airlineName.
    * @param outBoundDepartFlightNumber the departFlightNumber.
    * @param outBoundDepartTime the departTime.
    * @param outBoundDepartCity the departCity.
    * @param outBoundDepartAirportCode the departAirportCode.
    * @param outBoundArriveTime the arriveTime.
    * @param outBoundArriveCity the arriveCity.
    * @param outBoundArriveAirportCode the arriveAirportCode.
    * @param inBoundDepartFlightNumber the departFlightNumber.
    * @param inBoundDepartTime the departTime.
    * @param inBoundDepartCity the departCity.
    * @param inBoundDepartAirportCode the departAirportCode.
    * @param inBoundArriveTime the arriveTime.
    * @param inBoundArriveCity the arriveCity.
    * @param inBoundArriveAirportCode the arriveAirportCode.
    * @param ticketType the ticketType.
    *
    */
   // CHECKSTYLE:OFF
   public Flight(String airlineName, String outBoundDepartFlightNumber, String outBoundDepartTime,
           String outBoundDepartCity, String outBoundDepartAirportCode,
           String outBoundArriveTime, String outBoundArriveCity,
           String outBoundArriveAirportCode, String inBoundDepartFlightNumber,
           String inBoundDepartTime, String inBoundDepartCity, String inBoundDepartAirportCode,
           String inBoundArriveTime, String inBoundArriveCity,
           String inBoundArriveAirportCode, String ticketType){
      this.airlineName = airlineName;
      this.outBoundDepartFlightNumber = outBoundDepartFlightNumber;
      this.outBoundDepartTime = outBoundDepartTime;
      this.outBoundDepartCity = outBoundDepartCity;
      this.outBoundDepartAirportCode = outBoundDepartAirportCode;
      this.outBoundArriveTime = outBoundArriveTime;
      this.outBoundArriveCity = outBoundArriveCity;
      this.outBoundArriveAirportCode = outBoundArriveAirportCode;
      this.inBoundDepartFlightNumber = inBoundDepartFlightNumber;
      this.inBoundDepartTime = inBoundDepartTime;
      this.inBoundDepartCity = inBoundDepartCity;
      this.inBoundDepartAirportCode = inBoundDepartAirportCode;
      this.inBoundArriveTime = inBoundArriveTime;
      this.inBoundArriveCity = inBoundArriveCity;
      this.inBoundArriveAirportCode = inBoundArriveAirportCode;
      this.ticketType = ticketType;
   }
   // CHECKSTYLE:ON

   /**
    * Method to populate all Flight details.
    *
    * @return the Flight details.
    */
   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<Flight>");
      stringBuilder.append("<airlineName>").append(airlineName).append("</airlineName>");
      stringBuilder.append("<outboundFlightNumber>").append(outBoundDepartFlightNumber)
        .append("</outboundFlightNumber>");
      stringBuilder.append("<outboundDepartureTime>").append(outBoundDepartTime)
      .append("</outboundDepartureTime>");
      stringBuilder.append("<outboundDepartureCity>").append(outBoundDepartCity)
      .append("</outboundDepartureCity>");
      stringBuilder.append("<outboundDepartureAirportCode>").append(outBoundDepartAirportCode)
      .append("</outboundDepartureAirportCode>");
      stringBuilder.append("<outboundArrivalTime>").append(outBoundArriveTime)
      .append("</outboundArrivalTime>");
      stringBuilder.append("<outboundArrivalCity>").append(outBoundArriveCity)
      .append("</outboundArrivalCity>");
      stringBuilder.append("<outboundArrivalAirportCode>").append(outBoundArriveAirportCode)
        .append("</outboundArrivalAirportCode>");
      stringBuilder.append("<inboundFlightNumber>").append(inBoundDepartFlightNumber)
      .append("</inboundFlightNumber>");
      stringBuilder.append("<inboundDepartureTime>").append(inBoundDepartTime)
        .append("</inboundDepartureTime>");
      stringBuilder.append("<inboundDepartureCity>").append(inBoundDepartCity)
        .append("</inboundDepartureCity>");
      stringBuilder.append("<inboundDepartureAirportCode>").append(inBoundDepartAirportCode)
        .append("</inboundDepartureAirportCode>");
      stringBuilder.append("<inboundArrivalTime>").append(inBoundArriveTime)
        .append("</inboundArrivalTime>");
      stringBuilder.append("<inboundArrivalCity>").append(inBoundArriveCity)
        .append("</inboundArrivalCity>");
      stringBuilder.append("<inboundArrivalAirportCode>").append(inBoundArriveAirportCode)
        .append("</inboundArrivalAirportCode>");
      stringBuilder.append("<ticketType>").append(ticketType).append("</ticketType>");
      stringBuilder.append("</Flight>");

      return stringBuilder.toString();
   }
}
