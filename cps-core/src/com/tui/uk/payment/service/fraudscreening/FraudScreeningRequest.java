/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening;

import static com.tui.uk.payment.domain.PaymentConstants.DATE_FORMAT;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.service.fraudscreening.domain.*;

/**
 * This Class Represents FraudScreeningRequest.
 *
 * @author Jaleel
 */
public class FraudScreeningRequest
{
   /** The uniqueIdentifier. */
   private String uniqueIdentifier;

   /** The orderInformation. */
   private OrderInformation orderInformation;

   /** The request source. */
   private RequestSource requestSource;

   /** The passenger list. */
   private List<Passenger> passengerDetails = new ArrayList<Passenger>();

   /** The paymentInformation. */
   private PaymentInformation paymentInformation;

   /** The Flight details. */
   private Flight flight;

   /** The Accommodation. */
   private Accommodation accommodation;

   /** The Cruise. */
   private Cruise cruise;

   /** The comma separator. */
   private static final String COMMA_SEPARATOR = ",";

   /** The payment information count. */
   private static final int PAYMENT_INFO_COUNT = 7;

	/** The Accommodation. */
	private PaymentInformationForPayPal paymentInformationForPayPal;


   private PaymentInformationForDirectDebit paymentInformationForDirectDebit;

   /**
    * The constructor.
    *
    * @param uniqueIdentifier the uniqueIdentifier.
    *
    */
   public FraudScreeningRequest(String uniqueIdentifier)
   {
      this.uniqueIdentifier = uniqueIdentifier;
   }

   /**
    * Method to populate all FraudScreeningRequest details.
    *
    * @return the FraudScreeningRequest details.
    */
   @Override
   public String toString()
   {
      String xmlString = getXml(false);
      LogWriter.logDebugMessage(getXml(true));
      return xmlString;
   }

   /**
    * Gets the XML in string format.
    *
    * @param isLogging if the XML is required for lgging.
    *
    * @return the XML String.
    */
   private String getXml(boolean isLogging)
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<?xml version='1.0' encoding='utf-8'?>");
      stringBuilder.append("<request>");
      stringBuilder.append("<bookingSessionIdentifier>").append(uniqueIdentifier)
        .append("</bookingSessionIdentifier>");
      if (orderInformation != null)
      {
         stringBuilder.append(orderInformation.toString());
      }
      if (paymentInformation != null)
      {
         stringBuilder.append(paymentInformation.toString(isLogging));
      }
      if (paymentInformationForDirectDebit != null)
      {
         stringBuilder.append(paymentInformationForDirectDebit.toString(isLogging));
      }
      if (requestSource != null)
      {
         stringBuilder.append(requestSource.toString());
      }
      if (passengerDetails != null)
      {
         stringBuilder.append("<pnrData>");
         stringBuilder.append("<passengerDetails>");
         for (Passenger passenger : passengerDetails)
         {
            stringBuilder.append(passenger.toString());
         }
         stringBuilder.append("</passengerDetails>");
         stringBuilder.append("</pnrData>");
      }
      stringBuilder.append("<pnrDetail>");
      if (flight != null)
      {
         stringBuilder.append(flight.toString());
      }
      if (accommodation != null)
      {
         stringBuilder.append(accommodation.toString());
      }
      if (cruise != null)
      {
         stringBuilder.append(cruise.toString());
      }
      stringBuilder.append("</pnrDetail>");

      stringBuilder.append("</request>");

      return stringBuilder.toString().replaceAll(">null<", "><");
   }

   /**
    * Gets the orderInformation description.
    *
    * @return the orderInformation
    */
   public OrderInformation getOrderInformation()
   {
      return orderInformation;
   }

   /**
    * Sets orderInformation field.
    *
    * @param orderInformation the orderInformation to set.
    */
   public void setOrderInformation(OrderInformation orderInformation)
   {
      this.orderInformation = orderInformation;
   }

   /**
    * Gets the request source.
    *
    * @return the request source.
    */
   public RequestSource getRequestSource()
   {
      return requestSource;
   }

   /**
    * Sets request source field.
    *
    * @param requestSource the request source to set.
    */
   public void setRequestSource(RequestSource requestSource)
   {
      this.requestSource = requestSource;
   }

   /**
    * Gets the paymentInformation description.
    *
    * @return the paymentInformation
    */

   public PaymentInformation getPaymentInformation()
   {
      return paymentInformation;
   }

   /**
    * Sets paymentInformation field.
    *
    * @param paymentInformation the paymentInformation to set.
    */
   public void setPaymentInformation(PaymentInformation paymentInformation)
   {
      this.paymentInformation = paymentInformation;
   }

   /**
    * Gets the passengerDetails.
    *
    * @return the passengerDetails
    */
   public List<Passenger> getPassengerDetails()
   {
      return passengerDetails;
   }

   /**
     * Sets passengerDetails field.
     *
     * @param passengerDetails the passengerDetails to set.
   */
   public void setPassengerDetails(List<Passenger> passengerDetails)
   {
      this.passengerDetails = passengerDetails;
   }

   /**
    * Gets the flight.
    *
    * @return the flight
    */
   public Flight getFlight()
   {
      return flight;
   }

   /**
    * Sets flight field.
    * @param flight the flight to set.
   */
   public void setFlight(Flight flight)
   {
      this.flight = flight;
   }

   /**
    * Gets the accommodation.
    *
    * @return the accommodation
    */
   public Accommodation getAccommodation()
   {
      return accommodation;
   }

   /**
    * Sets accommodation field.
    * @param accommodation the accommodation to set.
   */
   public void setAccommodation(Accommodation accommodation)
   {
      this.accommodation = accommodation;
   }

   /**
    * Gets the passengerDetails.
    *
    * @return the passengerDetails
    */
   public Cruise getCruise()
   {
      return cruise;
   }

   /**
    * Sets cruise field.
    * @param cruise the cruise to set.
   */
   public void setCruise(Cruise cruise)
   {
      this.cruise = cruise;
   }

   /**
    * Purges sensitive card information.
    */
   public void purgeCardData()
   {
      paymentInformation.purgeSensitiveCardData();
   }

   /**
    * This method is responsible for logging fraud screening request.
    *
    * @param transactionType the transaction type.
    * @param status the status returned from fraud screening.
    */
   public void logRequest(String transactionType, String status)
   {
      StringBuilder logMessage = new StringBuilder();

      logMessage.append(transactionType);
      logMessage.append(COMMA_SEPARATOR);

      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      logMessage.append(dateFormat.format(date));
      logMessage.append(COMMA_SEPARATOR);

      logMessage.append(this.requestSource.getCpsUniqueToken());
      logMessage.append(COMMA_SEPARATOR);

      logMessage.append(this.requestSource.getCpsSessionId());
      logMessage.append(COMMA_SEPARATOR);

      if (this.orderInformation != null)
      {
         logMessage.append(this.orderInformation.getBookingReferenceNumber());
      }
      logMessage.append(COMMA_SEPARATOR);

      if (this.paymentInformation != null)
      {
         logMessage.append(this.paymentInformation.getPaymentLoggingInformation());
      }
      else
      {
         for (int i = 0; i < PAYMENT_INFO_COUNT; i++)
         {
            logMessage.append(COMMA_SEPARATOR);
         }
      }

      logMessage.append(status);
      logMessage.append(COMMA_SEPARATOR);

      logMessage.append(this.uniqueIdentifier);

      if (status.equalsIgnoreCase("Not Sent"))
      {
         logMessage.append(COMMA_SEPARATOR);
         logMessage.append(getXml(true));
      }

      LogWriter.logInfoMessage(logMessage.toString(), FraudScreeningRequest.class.getName());
   }
	/**
	 * @return the paymentInformationForPayPal
	 */
	public PaymentInformationForPayPal getPaymentInformationForPayPal() {
		return paymentInformationForPayPal;
	}

	/**
	 * @param paymentInformationForPayPal the paymentInformationForPayPal to set
	 */
	public void setPaymentInformationForPayPal(
			PaymentInformationForPayPal paymentInformationForPayPal) {
		this.paymentInformationForPayPal = paymentInformationForPayPal;
	}
	/**
	 * Method to populate all FraudScreeningRequest details for paypal transaction.
	 *
	 * @return the FraudScreeningRequest details.
	 */

	public String toString(String payPal)
	{
		StringBuilder stringBuilder = new StringBuilder();
		String xmlString = getXml(false);
		if (paymentInformationForPayPal != null)
		{
			xmlString = xmlString.replace("</request>", stringBuilder.append(paymentInformationForPayPal.toString(false))+"</request>");
		}
		LogWriter.logDebugMessage(xmlString);
		return xmlString;
	}

   public PaymentInformationForDirectDebit getPaymentInformationForDirectDebit()
   {
      return paymentInformationForDirectDebit;
   }

   public void setPaymentInformationForDirectDebit(
            PaymentInformationForDirectDebit paymentInformationForDirectDebit)
   {
      this.paymentInformationForDirectDebit = paymentInformationForDirectDebit;
   }
}
