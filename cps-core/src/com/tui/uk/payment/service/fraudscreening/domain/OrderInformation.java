/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This Class Represents OrderInformation Details.
 * @author Jaleel
 */

public class OrderInformation
{
   /** The bookingReferenceNumber. */
   private String bookingReferenceNumber;

   /** The bookingDateTime. */
   private String bookingDateTime;

   /** The bookingTotalPrice. */
   private String bookingTotalPrice;

   /**
    * The constructor.
    *
    * @param bookingReferenceNumber the bookingReferenceNumber.
    * @param bookingDateTime the bookingDateTime.
    * @param bookingTotalPrice the bookingTotalPrice.
    *
    *
    */
   public OrderInformation(String bookingReferenceNumber, String bookingDateTime,
           String bookingTotalPrice)
   {
      this.bookingReferenceNumber = bookingReferenceNumber;
      this.bookingDateTime = bookingDateTime;
      this.bookingTotalPrice = bookingTotalPrice;
   }

   /**
    * Method to populate all OrderInformation details.
    *
    * @return the OrderInformation details.
    */
   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<orderInformation>");
      stringBuilder.append("<bookingReferenceNumber>").append(bookingReferenceNumber)
        .append("</bookingReferenceNumber>");
      stringBuilder.append("<bookingDateTime>")
        .append(bookingDateTime).append("</bookingDateTime>");
      stringBuilder.append("<bookingTotalPrice>")
        .append(bookingTotalPrice).append("</bookingTotalPrice>");
      stringBuilder.append("</orderInformation>");

      return stringBuilder.toString();
   }

   /**
    * Gets the booking reference number.
    *
    * @return the booking reference number.
    */
   public String getBookingReferenceNumber()
   {
      return bookingReferenceNumber;
   }

}
