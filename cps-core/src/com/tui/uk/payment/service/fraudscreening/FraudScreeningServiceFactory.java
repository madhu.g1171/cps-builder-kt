package com.tui.uk.payment.service.fraudscreening;

import java.util.HashMap;
import java.util.Map;

/**
  * This class is a factory to create FraudScreeningService objects.
  * @author rajarao.r
  *
  */
public final class  FraudScreeningServiceFactory
{
      /**
       *
       *This map will contain the FraudScreeningServiceImpl objects with bookingSessionIdas keys.
       *
       */
       private static Map<String , FraudScreeningServiceImpl> fraudScreeningServiceMap =
        new HashMap<String , FraudScreeningServiceImpl>();
   /**
    * private constructor.
    */
   private FraudScreeningServiceFactory()
   {
      super();
   }

   /**
    * This method will return  FraudScreeningServiceImpl object from the factory.
    *
    * @param bookingSessionId BookingSessionId of client
    * @return  FraudScreeningServiceImpl
    */
   public static FraudScreeningServiceImpl getFraudScreeningServiceImpl(String bookingSessionId)
   {
      return fraudScreeningServiceMap.get(bookingSessionId);
   }
   /**
    * This method will always creates new FraudScreeningServiceImpl object and put it in fraudScreeningServiceMap.
    * @param bookingSessionId BookingSessionId of client
    * @param fraudScreeningRequest FraudScreeningRequest object
    * @return FraudScreeningServiceImpl FraudScreeningService object
    */
   public static FraudScreeningServiceImpl updateFraudScreeningServiceImpl(String bookingSessionId ,
           FraudScreeningRequest fraudScreeningRequest)
   {
      FraudScreeningServiceImpl   fraudScreeningServiceImpl =
      new FraudScreeningServiceImpl(fraudScreeningRequest);
      fraudScreeningServiceMap.put(bookingSessionId, fraudScreeningServiceImpl);
      return fraudScreeningServiceImpl;
   }
   /**
    * This method is to remove the FraudScreeningService object.
    * @param bookingSessionId  BookingSessionId of client
    */
   public static void remove(String bookingSessionId)
   {
      fraudScreeningServiceMap.remove(bookingSessionId);
   }

}
