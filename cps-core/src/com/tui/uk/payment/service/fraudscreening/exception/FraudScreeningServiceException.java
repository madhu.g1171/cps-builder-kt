/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.payment.service.fraudscreening.exception;

/**
 * This exception is thrown whenever there is a problem during post payment
 * processing.
 *
 * @author Jaleel
 *
 */
@SuppressWarnings("serial")
public final class FraudScreeningServiceException extends Exception
{

   /**
    * Default constructor for FraudScreeningServiceException class.
    */
   public FraudScreeningServiceException()
   {
      super();
   }

   /**
    * Constructs FraudScreeningServiceException with the specified
    * validation exception message.
    *
    * @param message the detail message.
    */
   public FraudScreeningServiceException(String message)
   {
      super(message);
   }

   /**
    * Defines the constructor for FraudScreeningServiceException with
    * detailed message and specified cause.
    *
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public FraudScreeningServiceException(String message, Throwable cause)
   {
      super(message, cause);
   }

   /**
    * Defines the constructor for FraudScreeningServiceException with
    * specified cause.
    *
    * @param cause - The specified cause.
    */
   public FraudScreeningServiceException(Throwable cause)
   {
      super(cause);
   }

}
