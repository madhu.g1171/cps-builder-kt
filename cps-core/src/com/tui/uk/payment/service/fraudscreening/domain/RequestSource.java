/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This Class Represents OtherInformation Details.
 * @author Jaleel
 */

public class RequestSource
{
   /** The ipAddress. */
   private String ipAddress;

   /** The appsessionId. */
   private String appsessionId;

   /** The cpssessionId. */
   private String cpssessionId;

   /** The cpsUniqueToken. */
   private String cpsUniqueToken;

   /** The shopNumber. */
   private String shopNumber;

   /** The salesChannel. */
   private String salesChannel;



   /**
    * The constructor.
    *
    * @param ipAddress the ipAddress.
    * @param appsessionId the appsessionId.
    * @param cpssessionId the cpssessionId.
    * @param cpsUniqueToken the cpsUniqueToken.
    * @param shopNumber the shopNumber.
    * @param salesChannel the salesChannel.
    *
    */
   public RequestSource(String ipAddress, String appsessionId, String cpssessionId,
           String cpsUniqueToken, String shopNumber, String salesChannel)
   {
      this.ipAddress = ipAddress;
      this.appsessionId = appsessionId;
      this.cpssessionId = cpssessionId;
      this.cpsUniqueToken = cpsUniqueToken;
      this.shopNumber = shopNumber;
      this.salesChannel = salesChannel;
   }

   /**
    * Method to populate all other details like ipaddress , shopdetails.
    *
    * @return the other details.
    */
   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<otherInformation>");
      stringBuilder.append("<ipAddress>").append(ipAddress).append("</ipAddress>");
      stringBuilder.append("<appsessionId>").append(appsessionId).append("</appsessionId>");
      stringBuilder.append("<cpssessionId>").append(cpssessionId).append("</cpssessionId>");
      stringBuilder.append("<cpsUniqueToken>").append(cpsUniqueToken).append("</cpsUniqueToken>");
      stringBuilder.append("<shopNumber>").append(shopNumber).append("</shopNumber>");
      stringBuilder.append("<salesChannel>").append(salesChannel).append("</salesChannel>");
      stringBuilder.append("</otherInformation>");

      return stringBuilder.toString();
   }

   /**
    * Gets the cps unique token.
    *
    * @return the cpsUniqueToken.
    */
   public String getCpsUniqueToken()
   {
      return cpsUniqueToken;
   }

   /**
    * Gets the cps unique token.
    *
    * @return the cpsUniqueToken.
    */
   public String getCpsSessionId()
   {
      return cpssessionId;
   }

}
