/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening;

import java.math.BigDecimal;


import com.tui.uk.payment.service.fraudscreening.domain.AuthInfo;
import com.tui.uk.payment.service.fraudscreening.domain.ContactInformation;
import com.tui.uk.payment.service.fraudscreening.domain.FraudScreeningCard;

/**
 * This Class Represents Flight Details.
 * @author Jaleel
 */


public class FraudScreeningCardData
{
   /** The fraudScreeningCard. */
   private FraudScreeningCard fraudScreeningCard;

   /** The AuthInfo. */
   private AuthInfo authInfo;

   /** The currency. */
   private String currency;

   /** The cardAmount. */
   private BigDecimal cardAmount;

   /** The cardCharge. */
   private BigDecimal cardCharge;

   /** The billingAddress. */
   private ContactInformation billingAddress;

   /** The boolean to represent fraud card. */
   private boolean failedCard;

   /**
    * The constructor.
    *
    * @param authInfo the authInfo.
    * @param fraudScreeningCard the fraudScreeningCard.
    * @param currency the currency.
    * @param cardAmount the cardAmount.
    * @param cardCharge the cardCharge.
    * @param billingAddress the billingAddress.
    */
   public FraudScreeningCardData(AuthInfo authInfo, FraudScreeningCard fraudScreeningCard,
           String currency, BigDecimal cardAmount, BigDecimal cardCharge,
           ContactInformation billingAddress)
   {
      this.authInfo = authInfo;
      this.fraudScreeningCard = fraudScreeningCard;
      this.currency = currency;
      this.cardAmount = cardAmount;
      this.cardCharge = cardCharge;
      this.billingAddress = billingAddress;
   }

   /**
    * Sets if the card is failed or not.
    *
    * @param failedCard is failed card.
    */
   public void setFailedCard(boolean failedCard)
   {
      this.failedCard = failedCard;
   }

   /**
    * Method to populate all card details.
    *
    * @param isLogging if the XML is for logging or not.
    *
    * @return the card details.
    */
   public String toString(boolean isLogging)
   {
      StringBuilder stringBuilder = new StringBuilder();
      if (authInfo.getPaymentGatewayResponseCode().equals("001") && !failedCard)
      {
         stringBuilder.append("<cardInfo>");
      }
      else
      {
          stringBuilder.append("<failedCardInfo>");
      }
      stringBuilder.append("<cardholderName>").append(fraudScreeningCard.getNameOncard())
        .append("</cardholderName>");
      stringBuilder.append("<cardAmount>").append(cardAmount).append("</cardAmount>");
      stringBuilder.append("<cardCharge>").append(cardCharge).append("</cardCharge>");
      stringBuilder.append(fraudScreeningCard.toString(isLogging));
      stringBuilder.append("<cardScheme>").append(authInfo.getCardScheme()).append("</cardScheme>");
      stringBuilder.append("<currency>").append(currency).append("</currency>");
      stringBuilder.append("<cardIssuer>").append(authInfo.getIssuer()).append("</cardIssuer>");
      stringBuilder.append("<cardIssuedCountry>").append(authInfo.getCountry())
      .append("</cardIssuedCountry>");
      stringBuilder.append("<expireDate>").append(fraudScreeningCard.getExpiryDate())
      .append("</expireDate>");

      // CHECKSTYLE:OFF
      stringBuilder.append("<billingAddress>").append(billingAddress == null ? "" : billingAddress
              .getAddress()).append("</billingAddress>");
      stringBuilder.append("<billingAddress2>").append(billingAddress == null ? "" : billingAddress
              .getAddress2()).append("</billingAddress2>");
      stringBuilder.append("<billingCounty>").append(billingAddress == null ? "" : billingAddress.getCounty())
        .append("</billingCounty>");
      stringBuilder.append("<billingCity>").append(billingAddress == null ? "" : billingAddress.getCity())
        .append("</billingCity>");
      stringBuilder.append("<billingZipCode>").append(billingAddress == null ? "" : billingAddress
              .getZipCode()).append("</billingZipCode>");
      stringBuilder.append("<billingCountry>").append(billingAddress == null ? "" : billingAddress
              .getCountry()).append("</billingCountry>");
      stringBuilder.append("<billingPhoneNumber>").append(billingAddress == null ? "" : billingAddress
              .getPhoneNumber()).append("</billingPhoneNumber>");
      stringBuilder.append("<authorizationInformation>");

      //CHECKSTYLE:ON
      stringBuilder.append("<authorizationCode>").append(authInfo.getAuthorizationCode())
        .append("</authorizationCode>");
      stringBuilder.append("<avsResponse>").append(authInfo.getAvsResponse())
      .append("</avsResponse>");
      stringBuilder.append("<cvvResponse>").append(authInfo.getCv2response())
      .append("</cvvResponse>");
      stringBuilder.append("<authorizedBy3dSecurity>").append(authInfo.getAuthorizedBy3dSecurity())
        .append("</authorizedBy3dSecurity>");
      stringBuilder.append("<paymentGatewayreferenceNumber>")
        .append(authInfo.getPaymentGatewayReferenceNumber())
        .append("</paymentGatewayreferenceNumber>");
      stringBuilder.append("<merchantReferenceNumber>")
      .append(authInfo.getMerchantReferenceNumber()).append("</merchantReferenceNumber>");
      stringBuilder.append("<paymentGatewayResponseCode>")
      .append(authInfo.getPaymentGatewayResponseCode()).append("</paymentGatewayResponseCode>");
      stringBuilder.append("<referralCode>")
      .append(authInfo.getReferralCode()).append("</referralCode>");
      stringBuilder.append("</authorizationInformation>");
      if (authInfo.getPaymentGatewayResponseCode().equals("001") && !failedCard)
      {
         stringBuilder.append("</cardInfo>");
      }
      else
      {
          stringBuilder.append("</failedCardInfo>");
      }

      return stringBuilder.toString();

   }

   /**
    * Gets the auth info.
    *
    * @return the authInfo.
    */
   public AuthInfo getAuthInfo()
   {
      return authInfo;
   }

   /**
    * Purges sensitive card information.
    */
   public void purgeSensitiveCardData()
   {
      fraudScreeningCard.purgeSensitiveCardData();
   }

   /**
    * Gets the currency.
    *
    * @return the currency.
    */
   public String getCurrency()
   {
      return currency;
   }

   /**
    * Gets the card amount.
    *
    * @return the cardAmount.
    */
   public BigDecimal getCardAmount()
   {
      return cardAmount;
   }

   /**
    * Gets the masked card number.
    *
    * @return the masked card number.
    */
   public String getMaskedCardNumber()
   {
      return fraudScreeningCard.getMaskedCardNumber();
   }

   /**
    * If the card has failed or not.
    *
    * @return failedCard.
    */
   public boolean isFailedCard()
   {
      return failedCard;
   }

}
