/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.service.fraudscreening.exception.FraudScreeningServiceException;

/**
 * This class holds information sent from Accertify as response to the Fraud Screening Request.
 *
 * @author sindhushree.g
 *
 */
public class FraudScreeningResponse
{

   /** The transaction id. */
   private String transactionId;

   /** The cross reference. */
   private String crossReference;

   /** The rules tripped. */
   private String rulesTripped;

   /** The total score. */
   private String totalScore;

   /** The recommendation code. */
   private String recommendationCode;

   /** The remarks. */
   private String remarks;

   /**
    * Creates the response object from the XML response string.
    *
    * @param xmlResponse the XML response string.
    *
    * @throws FraudScreeningServiceException if XML was not parsed.
    */
   public FraudScreeningResponse(String xmlResponse) throws FraudScreeningServiceException
   {
      LogWriter.logDebugMessage(xmlResponse);
      parse(xmlResponse);
   }

   /**
    * This method is responsible for parsing the XML response string.
    *
    * @param xmlResponse the XML response string.
    *
    * @throws FraudScreeningServiceException if XML was not parsed.
    */
   private void parse(String xmlResponse) throws FraudScreeningServiceException
   {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      Document dom = null;
      try
      {
         DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
         dom = documentBuilder.parse(new InputSource(new StringReader(xmlResponse)));
      }
      catch (ParserConfigurationException pce)
      {
         LogWriter.logErrorMessage(pce.getMessage());
         throw new FraudScreeningServiceException(pce.getMessage());
      }
      catch (SAXException se)
      {
         LogWriter.logErrorMessage(se.getMessage());
         throw new FraudScreeningServiceException(se.getMessage());
      }
      catch (IOException ioe)
      {
         LogWriter.logErrorMessage(ioe.getMessage());
         throw new FraudScreeningServiceException(ioe.getMessage());
      }

      checkError(dom);

      transactionId = dom.getElementsByTagName("transaction-id").item(0).getTextContent();
      crossReference = dom.getElementsByTagName("cross-reference").item(0).getTextContent();
      rulesTripped = dom.getElementsByTagName("rules-tripped").item(0).getTextContent();
      totalScore = dom.getElementsByTagName("total-score").item(0).getTextContent();
      recommendationCode = dom.getElementsByTagName("recommendation-code").item(0)
            .getTextContent();
      remarks = dom.getElementsByTagName("remarks").item(0).getTextContent();
   }

   /**
    * Checks id there are any errors while parsing.
    *
    * @param dom the document object.
    *
    * @throws FraudScreeningServiceException if any of the data is null.
    */
   private void checkError(Document dom) throws FraudScreeningServiceException
   {
       if (dom == null || dom.getElementsByTagName("transaction-id").item(0) == null
               || dom.getElementsByTagName("cross-reference").item(0) == null
               || dom.getElementsByTagName("rules-tripped").item(0) == null
               || dom.getElementsByTagName("total-score").item(0) == null
               || dom.getElementsByTagName("recommendation-code").item(0) == null
               || dom.getElementsByTagName("remarks").item(0) == null)
      {
         throw new FraudScreeningServiceException("Error from fraud screening service");
      }
   }

   /**
    * Gets the transaction id.
    *
    * @return the transactionId.
    */
   public String getTransactionId()
   {
      return transactionId;
   }

   /**
    * Gets the cross reference.
    *
    * @return the crossReference.
    */
   public String getCrossReference()
   {
      return crossReference;
   }

   /**
    * Gets the rules tripped.
    *
    * @return the rulesTripped.
    */
   public String getRulesTripped()
   {
      return rulesTripped;
   }

   /**
    * Gets the total score.
    *
    * @return the totalScore.
    */
   public String getTotalScore()
   {
      return totalScore;
   }

   /**
    * Gets the recommendation code.
    *
    * @return the recommendationCode.
    */
   public String getRecommendationCode()
   {
        return recommendationCode;
   }

   /**
    * Gets the remarks.
    *
    * @return the remarks.
    */
   public String getRemarks()
   {
      return remarks;
   }

}
