/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This Class Represents Accommodation Details.
 * @author Jaleel
 */

public class Accommodation
{
   /** The accommodationName name. */
   private String accommodationName;

   /** The sellingCode. */
   private String sellingCode;

   /** The resort. */
   private String resort;

   /** The destination. */
   private String destination;

   /** The country name. */
   private String country;

   /** The checkinDate. */
   private String checkinDate;

   /** The checkoutDate. */
   private String checkoutDate;

   /** The stayDuration. */
   private String stayDuration;

   /** The inventory. */
   private String inventory;

   /** The packageType. */
   private String packageType;

   /**
    * The constructor.
    *
    * @param accommodationName the accommodationName.
    * @param sellingCode the sellingCode.
    * @param resort the resort.
    * @param destination the destination.
    * @param country the country.
    * @param checkinDate the checkinDate.
    * @param checkoutDate the checkoutDate.
    * @param stayDuration the stayDuration.
    * @param inventory the inventory.
    * @param packageType the packageType.
    *
    */
   public Accommodation(String accommodationName, String sellingCode, String resort,
           String destination, String country, String checkinDate, String checkoutDate,
           String stayDuration, String inventory, String packageType)
   {
      this.accommodationName = accommodationName;
      this.sellingCode = sellingCode;
      this.resort = resort;
      this.destination = destination;
      this.country = country;
      this.checkinDate = checkinDate;
      this.checkoutDate = checkoutDate;
      this.stayDuration = stayDuration;
      this.inventory = inventory;
      this.packageType = packageType;
   }

   /**
    * Method to populate all Accomodation details.
    *
    * @return the Accomodation details.
    */
   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<Accommodation>");
      stringBuilder.append("<accommodationName>")
      .append(accommodationName).append("</accommodationName>");
      stringBuilder.append("<sellingCode>").append(sellingCode).append("</sellingCode>");
      stringBuilder.append("<resort>").append(resort).append("</resort>");
      stringBuilder.append("<destination>").append(destination).append("</destination>");
      stringBuilder.append("<country>").append(country).append("</country>");
      stringBuilder.append("<checkinDate>").append(checkinDate).append("</checkinDate>");
      stringBuilder.append("<checkoutDate>").append(checkoutDate).append("</checkoutDate>");
      stringBuilder.append("<stayDuration>").append(stayDuration).append("</stayDuration>");
      stringBuilder.append("<inventory>").append(inventory).append("</inventory>");
      stringBuilder.append("<packageType>").append(packageType).append("</packageType>");
      stringBuilder.append("</Accommodation>");

      return stringBuilder.toString();
   }
}
