/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;


/**
 * This Class Represents Flight Details.
 * @author Jaleel
 */


public class AuthInfo
{

   /** The authorizationCode name. */
   private String authorizationCode;

   /** The avsResponse. */
   private String avsResponse;

   /** The authorizedBy3dSecurity. */
   private String authorizedBy3dSecurity;

   /** The paymentGatewayreferenceNumber. */
   private String paymentGatewayReferenceNumber;

   /** The merchantReferenceNumber. */
   private String merchantReferenceNumber;

   /** The paymentGatewayResponseCode. */
   private String paymentGatewayResponseCode;

   /** The referralCode. */
   private String referralCode;

   /** The issuer. */
   private String issuer;

   /** The cardScheme. */
   private String cardScheme;

   /** The country. */
   private String country;

   /** The cv2response. */
   private String cv2response;

   /** The response message. */
   private String responseMsg;

   /**
    * The constructor.
    * @param authorizationCode the authorizationCode.
    * @param avsResponse the avsResponse.
    * @param authorizedBy3dSecurity the authorizedBy3dSecurity.
    * @param paymentGatewayReferenceNumber the paymentGatewayreferenceNumber.
    * @param merchantReferenceNumber the merchantReferenceNumber.
    * @param paymentGatewayResponseCode the paymentGatewayResponseCode.
    * @param referralCode the referralCode.
    * @param issuer the issuer.
    * @param cardScheme the cardScheme.
    * @param country the country.
    * @param cv2response the cv2response.
    * @param responseMsg the response message.
    */
   //CHECKSTYLE:OFF
   public AuthInfo(String authorizationCode, String avsResponse,
           String authorizedBy3dSecurity, String paymentGatewayReferenceNumber,
           String merchantReferenceNumber, String paymentGatewayResponseCode,
           String referralCode, String issuer, String cardScheme, String country,
           String cv2response, String responseMsg)
   {
      this.authorizationCode = authorizationCode;
      this.avsResponse = avsResponse;
      this.authorizedBy3dSecurity = authorizedBy3dSecurity;
      this.paymentGatewayReferenceNumber = paymentGatewayReferenceNumber;
      this.merchantReferenceNumber = merchantReferenceNumber;
      this.paymentGatewayResponseCode = paymentGatewayResponseCode;
      this.referralCode = referralCode;
      this.issuer = issuer;
      this.country = country;
      this.cardScheme = cardScheme;
      this.cv2response = cv2response;
      this.responseMsg = responseMsg;
   }
   //CHECKSTYLE:ON

   /**
    * Gets the authorizationCode.
    *
    * @return the authorizationCode.
    */
   public String getAuthorizationCode()
   {
      return authorizationCode;
   }

   /**
    * Gets the avsResponse.
    *
    * @return the avsResponse.
    */
   public String getAvsResponse()
   {
      return avsResponse;
   }

   /**
    * Gets the authorizedBy3dSecurity.
    *
    * @return the authorizedBy3dSecurity.
    */

   //CHECKSTYLE:OFF
   public String getAuthorizedBy3dSecurity()
   {
      return authorizedBy3dSecurity;
   }

   /**
    * Gets the paymentGatewayreferenceNumber.
    *
    * @return the paymentGatewayreferenceNumber.
    */
   public String getPaymentGatewayReferenceNumber()
   {
      return paymentGatewayReferenceNumber;
   }

   /**
    * Gets the merchantReferenceNumber.
    *
    * @return the merchantReferenceNumber.
    */
   public String getMerchantReferenceNumber()
   {
      return merchantReferenceNumber;
   }

   /**
    * Gets the paymentGatewayResponseCode.
    *
    * @return the paymentGatewayResponseCode.
    */
   public String getPaymentGatewayResponseCode()
   {
      return paymentGatewayResponseCode;
   }

   /**
    * Gets the referralCode.
    *
    * @return the referralCode.
    */
   public String getReferralCode()
   {
      return referralCode;
   }

   /**
    * Gets the issuer.
    *
    * @return the issuer.
    */
   public String getIssuer()
   {
      return issuer;
   }

   /**
    * Gets the cardScheme.
    *
    * @return the cardScheme.
    */
   public String getCardScheme()
   {
      return cardScheme;
   }

   /**
    * Gets the country.
    *
    * @return the country.
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Gets the cv2response.
    *
    * @return the cv2response.
    */
   public String getCv2response()
   {
      return cv2response;
   }

   /**
    * Gets the response message.
    *
    * @return the response message.
    */
   public String getResponseMsg()
   {
	  return responseMsg;
   }

}
