/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

/**
 * This Class Represents Information Details.
 * @author Jaleel
 */

public class ContactInformation
{
   /** The emailAddress . */
   private String emailAddress;

   /** The Address. */
   private String address;

   /** The Address2. */
   private String address2;

   /** The City. */
   private String city;

   /** The County . */
   private String county;

   /** The ZipCode. */
   private String zipCode;

   /** The country. */
   private String country;

   /** The phoneNumber. */
   private String phoneNumber;

   /** The alternatePhoneNumber. */
   private String alternatePhoneNumber;


   /**
    * The constructor.
    *
    * @param address the address.
    * @param address2 the address2.
    * @param city the city.
    * @param county the county.
    * @param zipCode the zipCode.
    * @param country the country.
    * @param phoneNumber the phoneNumber.
    *
    */
   public ContactInformation(String address, String address2,
           String city, String county, String zipCode,
           String country, String phoneNumber)
   {
      this.address = address;
      this.address2 = address2;
      this.city = city;
      this.county = county;
      this.zipCode = zipCode;
      this.country = country;
      this.phoneNumber = phoneNumber;
   }

   /**
    * Method to populate all Information details.
    *
    * @return the Information details.
    */
   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<shippingInformation>");
      stringBuilder.append("<emailAddress>").append(emailAddress).append("</emailAddress>");
      stringBuilder.append("<shippingAddress>").append(address).append("</shippingAddress>");
      stringBuilder.append("<shippingAddress2>").append(address2).append("</shippingAddress2>");
      stringBuilder.append("<shippingCity>").append(city).append("</shippingCity>");
      stringBuilder.append("<shippingCounty>").append(county).append("</shippingCounty>");
      stringBuilder.append("<shippingZipCode>").append(zipCode).append("</shippingZipCode>");
      stringBuilder.append("<shippingCountry>").append(country).append("</shippingCountry>");
      stringBuilder.append("<shippingPhoneNumber>").append(phoneNumber)
        .append("</shippingPhoneNumber>");
      stringBuilder.append("<shippingAlternatePhoneNumber>").append(alternatePhoneNumber)
        .append("</shippingAlternatePhoneNumber>");
      stringBuilder.append("</shippingInformation>");

      return stringBuilder.toString();
   }

   /**
    * Gets the emailAddress.
    *
    * @return the emailAddress
    */
   public String getEmailAddress()
   {
      return emailAddress;
   }

   /**
    * Gets the Address.
    *
    * @return the Address
    */
   public String getAddress()
   {
      return address;
   }

   /**
    * Gets the Address2.
    *
    * @return the Address2
    */
   public String getAddress2()
   {
      return address2;
   }

   /**
    * Gets the City.
    *
    * @return the City
    */
   public String getCity()
   {
      return city;
   }

   /**
    * Gets the County.
    *
    * @return the County
    */
   public String getCounty()
   {
      return county;
   }

   /**
    * Gets the ZipCode.
    *
    * @return the ZipCode
    */
   public String getZipCode()
   {
      return zipCode;
   }

   /**
    * Gets the Country.
    *
    * @return the Country
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Gets the PhoneNumber.
    *
    * @return the PhoneNumber
    */
   public String getPhoneNumber()
   {
      return phoneNumber;
   }

   /**
    * Gets the alternatePhoneNumber.
    *
    * @return the alternatePhoneNumber
    */
   public String getAlternatePhoneNumber()
   {
      return alternatePhoneNumber;
   }

   /**
    * Sets the emailAddress.
    * @param emailAddress the emailAddress.
   */
   public void setEmailAddress(String emailAddress)
   {
      this.emailAddress = emailAddress;
   }

   /**
    * Sets the alternatePhoneNumber.
    * @param alternatePhoneNumber the alternatePhoneNumber.
   */
   public void setAlternatePhoneNumber(String alternatePhoneNumber)
   {
      this.alternatePhoneNumber = alternatePhoneNumber;
   }

}
