/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashServiceConstants.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2008-05-07 14:24:20 $
 *
 * $Author: thomas.pm $
 *
 * $Log: not supported by cvs2svn $
 */
package com.tui.uk.payment.service.datacash;

/**
 * This contains all the constants required for datacash service.
 *
 * @author thomas.pm
 *
 */
public final class DataCashServiceConstants
{
	/**
	 * Private constructor to stop instantiation of this class.
	 */
	private DataCashServiceConstants()
	{
		// Do nothing.
	}

	/** The constant for card number. */
	public static final String PAN = "pan";

	/** The constant for start date. */
	public static final String START_DATE = "startdate";

	/** The constant for end date. */
	public static final String EXPIRY_DATE = "expirydate";

	/** The constant for Issue Number. */
	public static final String ISSUE_NUMBER = "issuenumber";

	/** The constant for CV2 in card. */
	public static final String CV2 = "cv2";

	/** The constant for post code. */
	public static final String POST_CODE = "postcode";

	/** The constant for street number. */
	public static final String STREET_NUMBER = "street_address1";

	/** The constant for name on card. */
	public static final String NAME_ON_CARD = "name";

	/** The constant for amount. */
	public static final String AMOUNT =
			"Request.Transaction.TxnDetails.amount";

	/** The constant for SUCCESS STATUS. */
	public static final int SUCCESS_STATUS = 1;

	/** The constant for THREED_SUCCESS. */
	public static final int THREED_SUCCESS_STATUS = 150;

	/** The constant for FAILURE STATUS. */
	public static final int FAILURE_STATUS = 7;

	/** The constant for status. */
	public static final String STATUS_RS = "Response.status";

	/** The constant for reason. */
	public static final String REASON_RS = "Response.reason";

	/** The constant for description. */
	public static final String DESCRIPTION = "Response.description";

	/** The constant for historic transaction reference. */
	public static final String DATACASH_REFERENCE =
			"Request.Transaction.HistoricTxn.reference";

	/** The constant for data cash reference response. */
	public static final String DATACASH_REFERENCE_RS =
			"Response.datacash_reference";

	 public static final String REASON = "Response.reason";

		/** The constants for HCC session ID */
	public static final String STATUS = "Response.status";

	/** The constant for merchant reference request. */
	public static final String MERCHANT_REFERENCE =
			"Request.Transaction.TxnDetails.merchantreference";

	/** The constant for merchant reference response. */
	public static final String MERCHANT_REFERENCE_RS =
			"Response.merchantreference";
	
   /** The constants for passing merch ref during emv3ds */
   public static final String SETUP_MERCHANTREFERENCE =
         "Response.merchantreference";

	/** The constant for issuer. */
	public static final String ISSUER_RS = "Response.CardTxn.issuer";

	/** The constant for country. */
	public static final String COUNTRY_RS = "Response.CardTxn.country";

	/** The constant for card scheme. */
	public static final String CARD_SCHEME_RS =
			"Response.CardTxn.card_scheme";

	/** The constant for card request authorization code. */
	public static final String AUTH_CODE =
			"Request.Transaction.CardTxn.authcode";

	/** The constant for historic authorization code. */
	public static final String AUTHORI_CODE =
			"Request.Transaction.HistoricTxn.authcode";

	/** The constant for card response authorization code. */
	public static final String AUTH_CODE_RS = "Response.CardTxn.authcode";

	/** The constant for time. */
	public static final String TIME_RS = "Response.time";

	/** The constant for mode. */
	public static final String MODE_RS = "Response.mode";

	/** The constant for host name. */
	public static final String HOST_NAME = "datacash.HostName";

	/** The constant for host name. */
	public static final String PAYPAL_HOST_NAME = "datacash.PaypalHostName";

	/** The constant for PAYPAL_DEV_MODE. */
	public static final String PAYPAL_DEV_MODE = "datacash.Paypal.devmode";

	/** The constant for HCC setup. */
	public static final String SETUP = "setup";

	/** The constant for authorized transaction. */
	public static final String AUTH_TRANSACTION = "auth";

	/** The constant for pre authorized transaction. */
	public static final String PRE_TRANSACTION = "pre";

	/** The constant for erp transaction. */
	public static final String ERP_TRANSACTION = "erp";

	/** The constant for refund transaction. */
	public static final String REFUND_TRANSACTION = "refund";

	/** The constant for fulfill transaction. */
	public static final String FULFILL_TRANSACTION = "fulfill";

	/** The constant for cancel transaction. */
	public static final String CANCEL_TRANSACTION = "cancel";

	/** The constant for reverse transaction. */
	public static final String REVERSE_TRANSACTION = "txn_refund";

	/** The constant for redeem transaction. */
	public static final String REDEEM_TRANSACTION = "redeem";

	/** The constant for PPT reverse transaction. */
	public static final String PPT_REVERSE_TRANSACTION = "reverse";

	/** The constant for card transaction type. */
	public static final String TRANSACTION_TYPE =
			"Request.Transaction.CardTxn.method";

	/** The constant for historic transaction type. */
	public static final String TRANSACT_TYPE =
			"Request.Transaction.HistoricTxn.method";

	/** The constants for HCC transaction type */
	public static final String HCC_TRANSACTION_TYPE = "Request.Transaction.HpsTxn.method";

	/** The constants for HCC return url */
	public static final String RETURN_URL = "Request.Transaction.HpsTxn.return_url";

	/** The constants for HCC expiry url */
	public static final String EXPIRY_URL = "Request.Transaction.HpsTxn.expiry_url";

	/** The constants for HCC page set ID */
	public static final String PAGE_SET_ID = "Request.Transaction.HpsTxn.page_set_id";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_1 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_1";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_2 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_2";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_3 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_3";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_4 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_4";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_5 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_5";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_6 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_6";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_7 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_7";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_8 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_8";

	/** The constants for HCC dynamic data 1 */
	public static final String DYN_DATA_9 = "Request.Transaction.HpsTxn.DynamicData.dyn_data_9";

   public static final String CV2AVS_POSTCODE =
      "Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.postcode";

   public static final String CV2AVS_STREETADDRESS1 =
      "Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.street_address1";

	/** The constants for HCC hps url */
	public static final String HPS_URL = "Response.HpsTxn.hps_url";

	/** The constants for HCC session ID */
	public static final String SESSION_ID = "Response.HpsTxn.session_id";

	/** The constants for HCC Query Trans capture status */
	public static final String CAPTURE_STATUS = "Response.HpsTxn.capture_status";

	/** The constants for HCC country */
	public static final String CAPTURED_COUNTRY = "Response.HpsTxn.country";

	/** The constants for HCC captured expiry date */
	public static final String CAPTURED_EXP_DATE = "Response.HpsTxn.expirydate";

	/** The constants for HCC card issuer */
	public static final String ISSUER = "Response.HpsTxn.issuer";

	/** The constants for HCC card or pan */
	public static final String HCC_PAN = "Response.HpsTxn.pan";

	/** The constants for HCC card scheme */
	public static final String CARD_SCHEME = "Response.HpsTxn.card_scheme";

	/** The constants for HCC Dynamic capture */
	public static final String DYNAMIC_CAPF2 = "Response.HpsTxn.DynamicCapture.capf2";

	/** The constants for HCC Dynamic capture */
	public static final String DYNAMIC_CAPF3 = "Response.HpsTxn.DynamicCapture.capf3";

	/** The constants for HCC Dynamic capture */
	public static final String DYNAMIC_CAPF4 = "Response.HpsTxn.DynamicCapture.capf4";

	/** The constants for HCC Dynamic capture */
	public static final String DYNAMIC_CAPF5 = "Response.HpsTxn.DynamicCapture.capf5";

	/** The constants for HCC Dynamic capture */
	public static final String DYNAMIC_CAPF6 = "Response.HpsTxn.DynamicCapture.capf6";

	/** The constants for HCC Dynamic capture */
	public static final String DYNAMIC_CAPF7 = "Response.HpsTxn.DynamicCapture.capf7";

	/** The constants for HCC Dynamic capture */
	public static final String DYNAMIC_CAPF1 = "Response.HpsTxn.DynamicCapture.capf1";


	/** The constants for HCC url */
	public static final String HCC_URL_KEY = "hpsUrl";

	/** The constants for HCC session ID */
	public static final String HCC_SESSION_KEY = "hpsSession";

	/** The constants for HCC Datacash Reference*/
	public static final String HCC_DATACASH_RS_KEY = "hpsDatacashReference";

	/** The constants for 3 times setup request fail check error page url */
	public static final String SETUP_FAIL_ERROR_URL = "setUpFailErrorUrl";

	/** The constants for HCC Query status key */
	public static final String HCC_CAPTURE_STATUS_KEY = "captureStatus";

	/** The constants for HCC Query status Populated */
	public static final String STATUS_POPULATED = "populated";

	/** The constants for HCC Query status Populated */
	public static final String NAME_ON_CARD_KEY = "nameOnCard";

	/** The constants for HCC Query status Populated */
	public static final String COUNTRY_CODE_KEY = "countryCode";

	/** The constants for HCC Query status Populated */
	public static final String HOUSE_NAME_KEY = "houseName";

	/** The constants for HCC Query status Populated */
	public static final String STREET_LINE1_KEY = "streetLine1";

	/** The constants for HCC Query status Populated */
	public static final String STREET_LINE2_KEY = "addressLine2";

	/** The constants for HCC Query status Populated */
	public static final String TOWN_CITY_KEY = "town_city";

	/** The constants for HCC Query status Populated */
	public static final String POSTCODE_KEY = "postCode";

	/** The constants for HCC Query status Populated */
	public static final String CARD_SCHEME_KEY = "cardScheme";

	/** The constants for HCC Query status Populated */
	public static final String COUNTRY_KEY = "country";

	/** The constants for HCC Query status Populated */
	public static final String CAPTURED_DATE_KEY = "capturedExpDate";

	/** The constants for HCC Query status Populated */
	public static final String ISSUER_KEY = "issuer";

	/** The constants for HCC Query status Populated */
	public static final String PAN_KEY = "pan";

	/** The constant for client. */
	public static final String CLIENT = "Request.Authentication.client";

	/** The constant for password. */
	public static final String PASSWORD = "Request.Authentication.password";

	/** The constant for capture method. */
	public static final String CAPTURE_METHOD =
			"Request.Transaction.TxnDetails.capturemethod";

	/** The constant for Terminal. */
	public static final String TERMINAL =
			"Request.Transaction.CardTxn.Terminal";

	/** The constant for terminal_capabilities. */
	public static final String TERMINAL_CAPABILTIES =
			"Request.Transaction.CardTxn.Terminal.terminal_capabilities";

	/** The constant for features_capabilities. */
	public static final String FEATURE_CAPABILTIES =
			"Request.Transaction.CardTxn.Terminal.features_capabilities";

	/** The constant for card_details. */
	public static final String CARD_DETAILS =
			"Request.Transaction.CardTxn.card_details";

	/** The constant for term_type. */
	public static final String ICC_TERM_TYPE =
			"Request.Transaction.CardTxn.ICC.term_type";

	/** The constant for term_type_value. */
	public static final String ICC_TERM_TYPE_VALUE = "01";

	/** The constant for ic_reader. */
	public static final String IC_READER = "ic_reader";

	/** The constant for magnetic_stripe_reader. */
	public static final String MAGNETIC_STRIPE_READER =
			"magnetic_stripe_reader";

	/** The constant for manual_card_entry. */
	public static final String MANUAL_CARD_ENTRY = "manual_card_entry";

	/** The constant for pin_pad_available. */
	public static final String PIN_PAD_AVAILABLE = "pin_pad_available";

	/** The constant for track2_data. */
	public static final String TRACK2DATA = "track2_data";

	/** The constant for track2_data type. */
	public static final String TRACK2DATA_TYPE = "type";

	/** The constant for terminal id type. */
	public static final String TERMINALID_TYPE = "id";

	/** The constant for TRUE. */
	public static final String TRUE = "true";

	/** The constant for FALSE. */
	public static final String FALSE = "false";

	/** The constant for capture swiped. */
	public static final String CAPTUREMETHOD_SWIPED = "swiped";

	/** The constant for capture CNP. */
	public static final String CAPTUREMETHOD_CNP = "cnp";

	/** The constant for cv2 not provided. */
	public static final String NOT_PROVIDED = "notprovided";

	/** The constant for cv2 not checked. */
	public static final String NOT_CHECKED = "notchecked";

	/** The constant for cv2 matched. */
	public static final String MATCHED = "matched";

	/** The constant for cv2 not matched. */
	public static final String NOT_MATCHED = "notmatched";

	/** The constant for cv2 partial match. */
	public static final String PARTIAL_MATCH = "partialmatch";

	/** The constant for cv2 policy . */
	public static final String CV2_POLICY =
			"Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.cv2_policy";

	/** The constant for post code policy . */
	public static final String POSTCODE_POLICY =
			"Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.postcode_policy";

	/** The constant for post code result . */
	public static final String POSTCODE_RESULT =
			"Response.CardTxn.Cv2Avs.postcode_result";

	/** The constant for post code result . */
	public static final String CVV_RESULT =
			"Response.CardTxn.Cv2Avs.cv2_result";

	/** The constant for address policy . */
	public static final String ADDRESS_POLICY =
			"Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.address_policy";

	/** The constant for address result . */
	public static final String ADDRESS_RESULT =
			"Response.CardTxn.Cv2Avs.address_result";

	/** The constant for accept. */
	public static final String ACCEPT = "accept";

	/** The constant for reject. */
	public static final String REJECT = "reject";

	/** The constant for card response ACS URL. */
	public static final String ACS_URL =
			"Response.CardTxn.ThreeDSecure.acs_url";

	/** The constant for card response authorization code. */
	public static final String PAREQ_MESSAGE =
			"Response.CardTxn.ThreeDSecure.pareq_message";

	/** The constant for 3D-Secure. */
	public static final String THREEDSECURE_VERIFY =
			"Request.Transaction.TxnDetails.ThreeDSecure.verify";

	/** The constant for 3D-Secure merchant URL. */
	public static final String THREEDSECURE_MERCHANT_URL =
			"Request.Transaction.TxnDetails.ThreeDSecure.merchant_url";

	/** The constant for 3D-Secure purchase description. */
	public static final String THREEDSECURE_PURCAHSE_DESC =
			"Request.Transaction.TxnDetails.ThreeDSecure.purchase_desc";

	/** The constant for 3D-Secure purchase date and time. */
	public static final String THREEDSECURE_PURCHASE_DATETIME =
			"Request.Transaction.TxnDetails.ThreeDSecure.purchase_datetime";

	/** The constant for 3D-Secure browser device category. */
	public static final String THREEDSECURE_BROWSER_DEVICE_CATEGORY =
			"Request.Transaction.TxnDetails.ThreeDSecure.Browser.device_category";

	/** The constant for 3D-Secure browser acceptable headers. */
	public static final String THREEDSECURE_BROWSER_HEADERS =
			"Request.Transaction.TxnDetails.ThreeDSecure.Browser.accept_headers";

	/** The constant for 3D-Secure browser user agent. */
	public static final String THREEDSECURE_BROWSER_USERAGENT =
			"Request.Transaction.TxnDetails.ThreeDSecure.Browser.user_agent";

	/** The constant for card enrollment check. */
	public static final int CARDENROLLMENT_STATUS = 150;

	/** The constant for card enrollment check. */
	public static final int CARDENROLLMENT_FAILED = 168;

	/** The multiplier constant for getting proper date. */
	public static final int TIME_MULTIPLIER = 1000;

	/** The constant for card response authorization code. */
	public static final String PARES_MESSAGE = "Request.Transaction.HistoricTxn.pares_message";

	/** The constant for PPT transaction pan. */
	public static final String PPT_TXN_PAN = "Request.Transaction.PPTCardTxn.pan";

	/** The constant for PPT transaction method. */
	public static final String PPT_TXN_METHOD = "Request.Transaction.PPTCardTxn.method";

	/** The constant for PPT transaction pin. */
	public static final String PPT_TXN_PIN = "Request.Transaction.PPTCardTxn.pin";

	/** The constant for PPT transaction reference. */
	public static final String PPT_TXN_REFERENCE = "Request.Transaction.PPTCardTxn.reference";

	/** The constant for card transaction card details. */
	public static final String CARDTXN_CARDDETAILS = "Request.Transaction.CardTxn.card_details";

	/** The constant for pre registered. */
	public static final String PREREGISTERED = "preregistered";

	/** The payment transaction type. */
	public static final String PAYMENT_TRANS_TYPE = "P";

	/** The refund transaction type. */
	public static final String REFUND_TRANS_TYPE = "R";

	/** The data-cash response code length. */
	public static final int DC_RESPONSE_CODE_LENGTH = 3;

	/** The data-cash success code. */
	public static final String DC_SUCCESS_CODE = "001";

	/** Error code if issue number is missing. */
	public static final int ERRORCODE_ISSUENOMISSING = 909;

	/** Error code if there is issue number mismatch. */
	public static final int ERRORCODE_ISSUENOMISMATCH = 910;

	/** Error code if issue number is invalid. */
	public static final int ERRORCODE_INVALIDISSUENO = 911;

	/** Error code if card start date is missing. */
	public static final int ERRORCODE_STARTDATEMISSING = 912;

	/** Error code if there is data-cash card type mismatch. */
	public static final int ERRORCODE_DATACASHCARDMISMATCH = 913;

	/** The carriage return. */
	public static final char CARRIAGE_RETURN = 0xD;

	/** The lined feed. */
	public static final char LINE_FEED = 0xA;

	/** Error code if there is fatal errors. */
	public static final int ERRORCODE_FOR_FATAL_ERRRORS = 555;

	/** Error code if there is general errors. */
	public static final int ERRORCODE_FOR_GENERAL_ERRRORS = 666;

	/** The street address two.Will hold remaining address details. */
	public static final String STREETADDRESS = "street_address2";

	/** The street address three.Will hold town/city. */
	public static final String TOWN = "street_address3";

	/** The street address four.Will hold county. */
	public static final String COUNTY = "street_address4";

	/** The constant for ECI. */
	public static final String ECI = "Response.QueryTxnResult.ThreeDSecure.eci";




	/** The SET UP for direct debt setup. */
	public static final String DD_SETUP = "Request.Transaction.DirectDebitTxn.method";

	/** The sort code for direct debt. */
	public static final String DD_SORT_CODE = "Request.Transaction.DirectDebitTxn.sortcode";

	/** The account number for direct debt. */
	public static final String DD_ACCOUNT_NUMBER = "Request.Transaction.DirectDebitTxn.accountnumber";
	/** The account name for direct debt. */
	public static final String DD_ACCOUNT_NAME = "Request.Transaction.DirectDebitTxn.accountname";






	/** The method is data cash response for direct debt. */
	public static final String DD_METHOD_RS = "Response.DirectDebitTxn.method";

	/** The stage is data cash response for direct debt. */
	public static final String DD_STAGE_RS = "Response.DirectDebitTxn.stage";
	/** The start date is data cash response for direct debt. */
	public static final String DD_START_DATE_RS = "Response.DirectDebitTxn.startdate";

	/** added for header element. */
	public static final String RESPONSE_BatchInputTxn="Response.BatchInputTxn";

	/** added for header element. */
	public static final String RESPONSE_INFORMATION="Response.information";

	//Following constants have been added for paypal constants

	/** added for paypal method element. */
	public static final String PAYPAL_METHOD="Request.Transaction.PayPalTxn.method";

	/** added for paypal method element. */
	public static final String PAYPAL_DATACASH_REFERENCE="Request.Transaction.PayPalTxn.reference";

	/** added for paypal method element. */
	public static final String PAYPAL_MERCHANT_REFERENCE="Request.Transaction.PayPalTxn.merchantreference";


	/** added for paypal method element. */
	public static final String PAYPAL_RETURN_URL="Request.Transaction.PayPalTxn.return_url";

	/** added for paypal method element. */
	public static final String PAYPAL_CANCEL_URL="Request.Transaction.PayPalTxn.cancel_url";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_TYPE="Request.Transaction.PayPalTxn.billing_type";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYMENT_ACTION="Request.Transaction.PayPalTxn.payment_action";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYMENT_TYPE="Request.Transaction.PayPalTxn.payment_type";


	/** added for paypal method element. */
	public static final String PAYPAL_BRAND_NAME="Request.Transaction.PayPalTxn.brand_name";

	/** added for paypal method element. */
	public static final String PAYPAL_CART_BORDER_COLOUR="Request.Transaction.PayPalTxn.cart_border_colour";

	/** added for paypal method element. */
	public static final String PAYPAL_CHANNEL_TYPE="Request.Transaction.PayPalTxn.channel_type";

	/** added for paypal method element. */
	public static final String PAYPAL_HEADER_STYLE="Request.Transaction.PayPalTxn.header_style";

	/** added for paypal method element. */
	public static final String PAYPAL_INVNUM="Request.Transaction.PayPalTxn.invnum";




	/** added for paypal method element. */
	public static final String PAYPAL_LOCALE_CODE="Request.Transaction.PayPalTxn.localecode";

	/** added for paypal method element. */
	public static final String PAYPAL_LOGO_IMAGE="Request.Transaction.PayPalTxn.logo_image";

	/** added for paypal method element. */
	public static final String PAYPAL_MAX_AMOUNT="Request.Transaction.PayPalTxn.max_amount";

	/** added for paypal method element. */
	public static final String PAYPAL_NO_SHIPPING="Request.Transaction.PayPalTxn.no_shipping";

	/** added for paypal method element. */
	public static final String PAYPAL_REQ_BILLING_TYPE="Request.Transaction.PayPalTxn.req_billing_address";

	/** added for paypal method element. */
	public static final String PAYPAL_REQ_BILLILNG_ADDRESS="Request.Transaction.PayPalTxn.req_billing_address";


	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_AGREEMENT_DESCRIPTION="Request.Transaction.PayPalTxn.billing_agreement_description";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_AGREEMENT_CUSTOM="Request.Transaction.PayPalTxn.billing_agreement_custom";






	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_AMOUNT="Request.Transaction.PayPalTxn.ShippingAddress.amount";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_CITY="Request.Transaction.PayPalTxn.ShippingAddress.city";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE="Request.Transaction.PayPalTxn.ShippingAddress.country_code";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_NAME="Request.Transaction.PayPalTxn.ShippingAddress.name";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_POSTCODE="Request.Transaction.PayPalTxn.ShippingAddress.postcode";


	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_REGION="Request.Transaction.PayPalTxn.ShippingAddress.region";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1="Request.Transaction.PayPalTxn.ShippingAddress.street_address1";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS2="Request.Transaction.PayPalTxn.ShippingAddress.street_address2";


	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_TELEPHONE_NUMBER="Request.Transaction.PayPalTxn.ShippingAddress.telephone_number";


	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_CITY="Request.Transaction.PayPalTxn.BillingAddress.city";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_COUNTRY_CODE="Request.Transaction.PayPalTxn.BillingAddress.country_code";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_NAME="Request.Transaction.PayPalTxn.BillingAddress.name";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_POSTCODE="Request.Transaction.PayPalTxn.BillingAddress.postcode";


	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_REGION="Request.Transaction.PayPalTxn.BillingAddress.region";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_STREET_ADDRESS1="Request.Transaction.PayPalTxn.BillingAddress.street_address1";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_STREET_ADDRESS2="Request.Transaction.PayPalTxn.BillingAddress.street_address2";

	public static final String LOGFILE_DATE_FORMAT="dd_MM_yyyy_hh_mm_ss";

	public static final String UNDER_SCORE="_";
	//funding_source 
		public static final String FUNDING_SOURCE="Request.Transaction.PayPalTxn.funding_source";

	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_CLIENT="Request.Authentication.client";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_PASSWORD = "Request.Authentication.password";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_MERCHANTREFERENCE = "Request.Transaction.TxnDetails.merchantreference";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_CURRENCY = "currency";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_AMOUNT = "Request.Transaction.TxnDetails.amount";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_CAPTUREMETHOD = "Request.Transaction.TxnDetails.capturemethod";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_ECOMM = "ecomm";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_METHOD = "Request.Transaction.Emv3dsTxn.method";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_INITIATE_AUTHENTICATION = "initiate_authentication";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_AUTHENTICATION_CHANNEL = "Request.Transaction.Emv3dsTxn.authentication_channel";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_PAYER_BROWSER = "payer_browser";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_PAYER_PURPOSE = "Request.Transaction.Emv3dsTxn.purpose";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_PAYMENT_TRANSACTION = "payment_transaction";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_CARD_DETAILS = "Request.Transaction.Emv3dsTxn.card_details";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_CARD_DETAILS_TYPE = "type";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_REQ_CARD_DETAILS_FROM_HPS = "from_hps";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_AUTHENTICATION_CHANNEL = "Response.Emv3dsTxn.authentication_channel";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_METHOD_CALL = "Response.Emv3dsTxn.method_call";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_REDIRECT_HTML = "Response.Emv3dsTxn.redirect_html";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_TOKEN = "Response.Emv3dsTxn.token";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_DATACASH_REFERENCE = "Response.datacash_reference";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_MERCHANTREFERENCE = "Response.merchantreference";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_MODE = "Response.mode";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_REASON = "Response.reason";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_STATUS = "Response.status";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_IA_RES_TIME = "Response.time";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_REQ_CLIENT="Request.Authentication.client";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_REQ_PASSWORD = "Request.Authentication.password";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_REQ_METHOD = "Request.Transaction.HistoricTxn.method";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_REQ_AUTHENTICATION_STATUS = "retrieve_emv3ds_authentication_status";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_REQ_REFERENCE = "Request.Transaction.HistoricTxn.reference";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_ACS_ECI = "Response.Emv3dsTxn.acs_eci";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_ACS_TRANSACTION_ID = "Response.Emv3dsTxn.acs_transaction_id";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_AUTHENTICATION_STATUS = "Response.Emv3dsTxn.authentication_status";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_AUTHENTICATION_TOKEN = "Response.Emv3dsTxn.authentication_token";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_AUTHENTICATION_VERSION = "Response.Emv3dsTxn.authentication_version";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_DS_TRANSACTION_ID = "Response.Emv3dsTxn.ds_transaction_id";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_PROTOCOL_VERSION = "Response.Emv3dsTxn.protocol_version";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_RESULT = "Response.Emv3dsTxn.result";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_TRANSACTION_ID = "Response.Emv3dsTxn.transaction_id";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_TRANSACTION_STATUS = "Response.Emv3dsTxn.transaction_status";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_DATACASH_REFERENCE = "Response.datacash_reference";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_MERCHANTREFERENCE = "Response.merchantreference";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_REASON = "Response.reason";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_STATUS = "Response.status";
	
	/** EMV 3DS call elements. */
	public static final String EMV_3DS_RA_RES_TIME = "Response.time";
	
	
	
/** added for EMV3DS request element **/	
public static final String PREFERENCE = "Request.Transaction.TxnDetails.Emv3ds.preference";

/** added for EMV3DS request element **/	
public static final String REDIRECT_URL = "Request.Transaction.TxnDetails.Emv3ds.redirect_url";

/** added for EMV3DS request element **/	
public static final String AUTHENTICATION_METHOD = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.Authentication.authentication_method";

/** added for EMV3DS request element **/	
public static final String TIME = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.Authentication.time";

/** added for EMV3DS request element **/	
public static final String ACS_TRANSACTION_ID = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.IssuerAuthentication.acs_transaction_id";

/** added for EMV3DS request element **/	
public static final String ISSUERAUTHENTICATION_TIME = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.IssuerAuthentication.time";

/** added for EMV3DS request element **/	
public static final String ISSUERAUTHENTICATION_TYPE = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.IssuerAuthentication.type";

/** added for EMV3DS request element **/	
public static final String ADD_CARD_ATTEMPTS = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.add_card_attempts";

/** added for EMV3DS request element **/	
public static final String ANNUAL_ACTIVITY = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.annual_activity";

/** added for EMV3DS request element **/	
public static final String CREATION_DATE = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.creation_date";

/** added for EMV3DS request element **/	
public static final String LAST_UPDATED = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.last_updated";

/** added for EMV3DS request element **/	
public static final String PASSWORD_LAST_CHANGED = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.password_last_changed";

/** added for EMV3DS request element **/	
public static final String RECENT_ACTIVITY = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.recent_activity";

/** added for EMV3DS request element **/	
public static final String SHIPPING_ADDRESS_DATE = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.History.shipping_address_date";

/** added for EMV3DS request element **/	
public static final String ACCOUNT_ID = "Request.Transaction.TxnDetails.Emv3ds.Customer.Account.id";

/** added for EMV3DS request element **/	
public static final String CUSTOMER_EMAIL = "Request.Transaction.TxnDetails.Emv3ds.Customer.email";

/** added for EMV3DS request element **/	
public static final String CUSTOMER_FIRST_NAME = "Request.Transaction.TxnDetails.Emv3ds.Customer.first_name";

/** added for EMV3DS request element **/	
public static final String CUSTOMER_LAST_NAME = "Request.Transaction.TxnDetails.Emv3ds.Customer.last_name";

/** added for EMV3DS request element **/	
public static final String CUSTOMER_MOBILE_PHONE = "Request.Transaction.TxnDetails.Emv3ds.Customer.mobile_phone";

/** added for EMV3DS request element **/	
public static final String CUSTOMER_PHONE = "Request.Transaction.TxnDetails.Emv3ds.Customer.phone";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_COMPANY = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.company";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_STREET = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.street";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_STREET2 = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.street2";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_CITY = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.city";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_STATE_PROVINCE = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.state_province";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_STATE_PROVINCE_CODE = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.state_province_code";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_COUNTRY = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.country";

/** added for EMV3DS request element **/	
public static final String BILLINGADDRESS_POSTCODE_ZIP = "Request.Transaction.TxnDetails.Emv3ds.Customer.BillingAddress.postcode_zip";

/** added for EMV3DS request element **/	
public static final String ADDRESS_COMPANY = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.company";

/** added for EMV3DS request element **/	
public static final String ADDRESS_STREET = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.street";

/** added for EMV3DS request element **/	
public static final String ADDRESS_STREET2 = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.street2";

/** added for EMV3DS request element **/	
public static final String ADDRESS_CITY = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.city";

/** added for EMV3DS request element **/	
public static final String ADDRESS_STATE_PROVINCE = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.state_province";

/** added for EMV3DS request element **/	
public static final String ADDRESS_STATE_PROVINCE_CODE = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.state_province_code";

/** added for EMV3DS request element **/	
public static final String ADDRESS_COUNTRY = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.country";

/** added for EMV3DS request element **/	
public static final String ADDRESS_POSTCODE_ZIP = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.postcode_zip";

/** added for EMV3DS request element **/	
public static final String ADDRESS_SOURCE = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.source";

/** added for EMV3DS request element **/	
public static final String ADDRESS_SAME_AS_BILLING = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Address.same_as_billing";

/** added for EMV3DS request element **/	
public static final String CONTACT_EMAIL = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Contact.email";

/** added for EMV3DS request element **/	
public static final String CONTACT_FIRST_NAME = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Contact.first_name";

/** added for EMV3DS request element **/	
public static final String CONTACT_LAST_NAME = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Contact.last_name";

/** added for EMV3DS request element **/	
public static final String CONTACT_MOBILE_PHONE = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Contact.mobile_phone";

/** added for EMV3DS request element **/	
public static final String CONTACT_PHONE = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Contact.phone";

/** added for EMV3DS request element **/	
public static final String CONTACT_SAME_AS_BILLING = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.Contact.same_as_billing";

/** added for EMV3DS request element **/	
public static final String SHIPPING_METHOD = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.shipping_method";

/** added for EMV3DS request element **/	
public static final String ORIGIN_POSTCODE_ZIP = "Request.Transaction.TxnDetails.Emv3ds.Customer.ShippingDetails.origin_postcode_zip";

/** added for EMV3DS request element **/	
public static final String CHALLENGE_WINDOW_SIZE = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.challenge_window_size";

/** added for EMV3DS request element **/	
public static final String ACCEPT_HEADER = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.accept_header";

/** added for EMV3DS request element **/	
public static final String COLOUR_DEPTH = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.colour_depth";

/** added for EMV3DS request element **/	
public static final String JAVA_ENABLED = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.java_enabled";

/** added for EMV3DS request element **/	
public static final String BROWSERDETAILS_LANGUAGE = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.language";

/** added for EMV3DS request element **/	
public static final String SCREEN_HEIGHT = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.screen_height";

/** added for EMV3DS request element **/	
public static final String SCREEN_WIDTH = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.screen_width";

/** added for EMV3DS request element **/	
public static final String BROWSERDETAILS_TIME_ZONE = "Request.Transaction.TxnDetails.Emv3ds.Device.BrowserDetails.time_zone";

/** added for EMV3DS request element **/	
public static final String DEVICE_BROWSER = "Request.Transaction.TxnDetails.Emv3ds.Device.browser";

/** added for EMV3DS request element **/	
public static final String DEVICE_IP_ADDRESS = "Request.Transaction.TxnDetails.Emv3ds.Device.ip_address";

/** added for EMV3DS request element **/	
public static final String AP_RES_ACS_TRANSACTION_ID = "Response.Emv3dsTxn.acs_transaction_id";

/** added for EMV3DS request element **/	
public static final String AP_RES_DS_TRANSACTION_ID = "Response.Emv3dsTxn.ds_transaction_id";

/** added for EMV3DS request element **/	
public static final String AP_RES_TRANSACTION_ID = "Response.Emv3dsTxn.transaction_id";

/** added for EMV3DS request element **/	
public static final String AP_RES_AUTHENTICATION_STATUS = "Response.Emv3dsTxn.authentication_status";

/** added for EMV3DS request element **/	
public static final String AP_RES_AUTHENTICATION_VERSION = "Response.Emv3dsTxn.authentication_version";

/** added for EMV3DS request element **/	
public static final String AP_RES_TRANSACTION_STATUS = "Response.Emv3dsTxn.transaction_status";

/** added for EMV3DS request element **/	
public static final String AP_RES_REDIRECT_HTML = "Response.Emv3dsTxn.redirect_html";

/** added for EMV3DS request element **/	
public static final String AP_RES_PROTOCOL_VERSION = "Response.Emv3dsTxn.protocol_version";

/** added for EMV3DS request element **/	
public static final String AP_RES_DATACASH_REFERENCE = "Response.datacash_reference";

/** added for EMV3DS request element **/	
public static final String AP_RES_MERCHANTREFERENCE = "Response.merchantreference";

/** added for EMV3DS request element **/	
public static final String AP_RES_MODE = "Response.mode";

/** added for EMV3DS request element **/	
public static final String AP_RES_REASON = "Response.reason";

/** added for EMV3DS request element **/	
public static final String AP_RES_STATUS = "Response.status";

/** added for EMV3DS request element **/	
public static final String AP_RES_TIME = "Response.time";

/** EMV 3DS call elements. */
public static final String BROWSER_ACCEPT_LANGUAGE = "Accept-Language";

/** EMV 3DS call elements. */
public static final String HYPHEN = "-";

/** EMV 3DS call elements. */
public static final String BROWSER_USER_AGENT = "user-Agent";

/** EMV 3DS call elements. */
public static final String BROWSER_ACCEPT = "Accept";

/** EMV 3DS call elements. */
public static final String BROWSER_ACCEPT_HEADER = "accept_header";

/** EMV 3DS call elements. */
public static final String BROWSER_COLOUR_DEPTH = "colour_depth";

/** EMV 3DS call elements. */
public static final String BROWSER_JAVA_ENABLED = "java_enabled";

/** EMV 3DS call elements. */
public static final String BROWSER_LANGUAGE = "language";

/** EMV 3DS call elements. */
public static final String BROWSER_SCREEN_HEIGHT = "screen_height";

/** EMV 3DS call elements. */
public static final String BROWSER_SCREEN_WIDTH = "screen_width";

/** EMV 3DS call elements. */
public static final String BROWSER_TIME_ZONE = "time_zone";

/** EMV 3DS call elements. */
public static final String BROWSER = "browser";

/** EMV 3DS call elements. */
public static final String REMOTEADDRESS = "remoteAddress";

/** EMV 3DS call elements. */
public static final String EMV_MERCH_REF_ENABLED = "emv.3ds.merchantreference.enabled";

public static final String EMV_IP_ADDRESS_ENABLED = "emv.3ds.ipaddress.enabled";

public static final String EMV_IP_ADDRESS_REGEX = "emv.3ds.ipaddress.regex";

public static final String EMV_DATACASH_REFERENCE = "Response.datacash_reference";

public static final String EMV_REDIRECT_HTML = "Response.Emv3dsTxn.redirect_html";

public static final String EMV_INITIATE_AUTH_INFO = "Response.information";

public static final String EMV_INITIATE_AUTH_REASON = "Response.reason";

public static final String BROWSER_CHALLENGE_WINDOW_SIZE = "challenge_window_size";

public static final String AUTHENTICATE_PAYER = "authenticate_payer";

public static final String NO_PREFERENCE = "no_preference";

public static final String PAYMENT_0_STREET_ADDRESS1 = "payment_0_street_address1";

public static final String PAYMENT_0_STREET_ADDRESS2 = "payment_0_street_address2";

public static final String PAYMENT_0_STREET_ADDRESS3 = "payment_0_street_address3";

public static final String PAYMENT_0_ISSUERCOUNTRY = "payment_0_issuerCountry";

public static final String PAYMENT_0_POSTCODE = "payment_0_postCode";

public static final String SAME = "same";

public static final String EMV_3DS_AZ_REQ_CARD_DETAILS_TYPE = "type";

public static final String EMV_3DS_AZ_REQ_CARD_DETAILS_VALUE = "emv3ds";

public static final String EMV_3DS_AZ_REQ_CURRENCY = "currency";

public static final String EMV_3DS_AZ_REQ_MERCHANTREFERENCE = "Request.Transaction.TxnDetails.merchantreference";

public static final String EMV_3DS_AZ_REQ_AMOUNT = "Request.Transaction.TxnDetails.amount";

public static final String EMV_3DS_AZ_REQ_CAPTUREMETHOD = "Request.Transaction.TxnDetails.capturemethod";

public static final String EMV_3DS_AZ_REQ_METHOD = "Request.Transaction.CardTxn.method";

public static final String EMV_3DS_AZ_REQ_METHOD_PRE = "pre";

public static final String EMV_3DS_AZ_REQ_CARD_DETAILS = "Request.Transaction.CardTxn.card_details";

public static final String PAYMENT_PRE_AUTH_FAILURE = "PAYMENT_PRE_AUTH_FAILURE";

public static final String PAYMENT_PRE_AUTH_SUCCESS = "PAYMENT_PRE_AUTH_SUCCESS";

}
