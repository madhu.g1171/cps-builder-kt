/*
\ * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashService.java,v $
 *
 * $Revision: 1.10 $
 *
 * $Date: 2008-05-13 08:31:45 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $ Revision 1.9 2008/05/08 07:26:23
 * sindhushree.g Modified to resolve findbug issues.
 *
 * Revision 1.8 2008/05/07 14:28:52 thomas.pm Added Log History.
 *
 */
package com.tui.uk.payment.service.datacash;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.ADDRESS_POLICY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.AMOUNT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.AUTHORI_CODE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.AUTH_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CANCEL_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CAPTURE_METHOD;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CARDTXN_CARDDETAILS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CLIENT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CV2_POLICY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DATACASH_REFERENCE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.ERP_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.FULFILL_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HOST_NAME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.MERCHANT_REFERENCE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PARES_MESSAGE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PASSWORD;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PPT_REVERSE_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PPT_TXN_METHOD;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PPT_TXN_REFERENCE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PREREGISTERED;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PRE_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REDEEM_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REFUND_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REVERSE_TRANSACTION;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREEDSECURE_BROWSER_DEVICE_CATEGORY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREEDSECURE_BROWSER_HEADERS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREEDSECURE_BROWSER_USERAGENT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREEDSECURE_MERCHANT_URL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREEDSECURE_PURCAHSE_DESC;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREEDSECURE_PURCHASE_DATETIME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.THREEDSECURE_VERIFY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TRANSACTION_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TRANSACT_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_SETUP;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_SORT_CODE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_ACCOUNT_NUMBER;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_ACCOUNT_NAME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_METHOD_RS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_STAGE_RS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_START_DATE_RS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.SUCCESS_STATUS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.RESPONSE_BatchInputTxn;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PAYPAL_HOST_NAME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.POSTCODE_POLICY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PAYPAL_DEV_MODE;
import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.domain.PaymentConstants.COMMA;
import static com.tui.uk.payment.domain.PaymentConstants.DATE_FORMAT;
import static com.tui.uk.payment.domain.PaymentConstants.ERROR_MESSAGE_KEY;
import static com.tui.uk.payment.domain.PaymentConstants.FATAL_ERROR;
import static com.tui.uk.payment.domain.PaymentConstants.HYPHEN;
import static com.tui.uk.payment.domain.PaymentConstants.NON_FATAL_ERROR;
import static com.tui.uk.payment.domain.PaymentConstants.RESPONSE_CODE_LENGTH;
import static com.tui.uk.payment.domain.PaymentConstants.THREED_NON_FATAL_ERROR;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.math.BigDecimal;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.datacash.client.Agent;
import com.datacash.client.Amount;
import com.datacash.errors.FailureReport;
import com.datacash.logging.Logger;
import com.datacash.util.XMLDocument;
import com.tui.uk.payment.service.datacash.AunthicatePayeeResXml;
import com.tui.uk.client.domain.AuthorizePayPalTransaction;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingData;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.client.domain.EMVAuthorizationResponse;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PayPalAuthorizeResponse;
import com.tui.uk.client.domain.PayPalCaptureRequest;
import com.tui.uk.client.domain.PayPalCaptureResponse;
import com.tui.uk.client.domain.PayPalComponent;
import com.tui.uk.client.domain.PayPalCustomer;
import com.tui.uk.client.domain.PayPalCustomerVerificationDetails;
import com.tui.uk.client.domain.PayPalFundReleaseResponse;
import com.tui.uk.client.domain.PayPalItems;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutData;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutResponse;
import com.tui.uk.client.domain.PayPalFundReleaseResponse;
import com.tui.uk.client.domain.ShippingAddress;
import com.tui.uk.client.domain.BillingAddress;
import com.tui.uk.client.domain.PayPalQueryTransactionResponse;
import com.tui.uk.client.domain.PayPalTransaction;
import com.tui.uk.client.domain.RetriveThreedAuthResponse;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.ApplicationDataConstants;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.LoggerServiceFrameWork;
import com.tui.uk.log.LoggerServiceFrameWorkImpl;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;
import com.tui.uk.xmlrpc.handlers.CommonPaymentServiceHandler;

import org.apache.xmlrpc.XmlRpcException;

import com.datacash.util.XMLDocument;
import com.datacash.client.CardDetails;

import java.util.zip.Deflater;
import java.util.zip.Inflater;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.DatatypeConverter;

import java.io.Reader;
import java.util.Objects;

/**
 * This provides service from DataCash for card payments. This is responsible for performing the
 * following actions: 1. pre-authorization 2. authorized payment 3. refund authorization 4.
 * authorized refund 5. fulfill transaction 6. cancel transaction 7. reverse transaction.
 *
 * @author thomas.pm
 */
public final class DataCashService
{
	/** The constant for next line. */
	private static final String NEXT_LINE = "\n";

	/** The constant for default connection time out. */
	private static final int DEFAULT_TIMEOUT = 60000;

	/** The constant for datacash connection time out. */
	private static final String DATACASH_CONNECTION_TIMEOUT = "datacash.ConnectionTimeOut";

	/** The constant for data-cash error. */
	private static final String DATACASH_ERROR = "datacash.generic.error";
	
	/** The data-cash account vTid. */
	private String vTid;

	/** The password for the client. */
	private String password;

	/** The flag for the multiple environment setup of vTID. */
	private boolean multipleEnvEnabled;

	/** setUpPayPalRequestXMLDocument*/
	private XMLDocument setUpPayPalRequestXMLDocument = null;

	/** authorizePayPalTransactionRequestXMLDocument */
	private XMLDocument authorizePayPalTransactionRequestXMLDocument = null;

	/** authorizePayPalTransactionResponseXMLDocument*/
	private XMLDocument authorizePayPalTransactionResponseXMLDocument = null;

	/** doVoidPayPalTransactionRequestXMLDocument*/
	private XMLDocument doVoidPayPalTransactionRequestXMLDocument = null;

	/** doVoidPayPalTransactionResponseXMLDocument*/
	private XMLDocument doVoidPayPalTransactionResponseXMLDocument = null;

	XMLDocument response = null;

	/** doQueryPayPalTransactionRequestXMLDocument*/
	private XMLDocument doQueryPayPalTransactionRequestXMLDocument = null;

	/** doQueryPayPalTransactionResponseXMLDocument*/
	private XMLDocument doQueryPayPalTransactionResponseXMLDocument = null;

	/** payPalConnectionAttempts*/
	private int payPalConnectionAttempts = 0;

	/** PayPalSetExpressCheckoutResponse*/
	private PayPalSetExpressCheckoutResponse payPalSetExpressCheckoutResponse = null ;

	/** PayPalAuthorizeResponse*/
	private PayPalAuthorizeResponse payPalAuthorizeResponse = null;

	/** PayPalFundReleaseResponse*/
	private PayPalFundReleaseResponse payPalFundReleaseResponse = null;

	/** PayPalQueryTransactionResponse*/
	private PayPalQueryTransactionResponse payPalQueryTransactionResponse = null;

	private UUID cpsToken ;
	/**
	 * The Constructor.
	 *
	 * @param vTid the datacash vTid.
	 */
	public DataCashService(String vTid)
	{
		this.vTid = vTid;
		this.password = ConfReader.getConfEntry(vTid, null);
		this.multipleEnvEnabled = ConfReader.getBooleanEntry(vTid + ".multipleEnvEnabled", false);
		if (StringUtils.isBlank(this.password))
		{
			String errorMessage =
					PropertyResource.getProperty("datacash.password.notfound", MESSAGES_PROPERTY) + vTid;
			LogWriter.logErrorMessage(errorMessage);
			throw new RuntimeException(errorMessage);
		}
	}

	/**
	 * This is responsible for performing the "pre" transaction type.
	 *
	 * @param amount the money.
	 * @param card the data cash card
	 * @param authCode the authorization code.
	 * @param merchantReference the merchantReference
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardResponse authPayment(DataCashCard card, Money amount, String authCode,
			String merchantReference)
	{
		return processAuth(card, amount, authCode, PRE_TRANSACTION, merchantReference);
	}

	/**
	 * TODO added by Mukesh for hcc
	 * This is responsible for performing the "pre" transaction type for HCC.
	 *
	 * @param amount the money.
	 * @param card the data cash card
	 * @param authCode the authorization code.
	 * @param merchantReference the merchantReference
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardResponse authHccPayment(DataCashCard card, Money amount, String authCode,
			String merchantReference, String hpsDatacashReference)
	{
		return processHccAuth(card, amount, authCode, PRE_TRANSACTION, merchantReference, hpsDatacashReference);
	}

	/**
	 * This is responsible for performing the "pre" transaction type.
	 *
	 * @param amount the money.
	 * @param card the data cash card.
	 * @param merchantReference the merchantReference.
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public Response redeemPayment(DataCashCard card, Money amount, String merchantReference)
	{
		Response response = null;
		try
		{
			XMLDocument xmlDocument = card.getGiftCardXmlDocument(REDEEM_TRANSACTION);

			setAuthentication(xmlDocument);

			xmlDocument.set(new Amount(amount.getAmount().toString(), amount.getCurrency()
					.getCurrencyCode()));
			xmlDocument.set(MERCHANT_REFERENCE, merchantReference);

			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());

			XMLDocument responseXml = getAgent().request(xmlDocument, new Logger());

			response = new Response(responseXml);

			LogWriter.logDebugMessage(responseXml.getSanitizedDoc());
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return response;
	}

	/**
	 * This is responsible for performing the "auth" transaction type.
	 *
	 * @param amount the money.
	 * @param card the data cash card.
	 * @param authCode the authorization code.
	 * @param merchantReference the merchantReference
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardResponse doPayment(DataCashCard card, Money amount, String authCode,
			String merchantReference)
	{
		return processAuth(card, amount, authCode, AUTH_TRANSACTION, merchantReference);
	}

	/**
	 * This is responsible for performing the "erp" transaction type.
	 *
	 * @param amount the money.
	 * @param card the data cash card.
	 * @param authCode the authorization code.
	 * @param merchantReference the merchantReference
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardResponse authRefund(DataCashCard card, Money amount, String authCode,
			String merchantReference)
	{
		return processAuth(card, amount, authCode, ERP_TRANSACTION, merchantReference);
	}

	/**
	 * This is responsible for performing the "refund" transaction type.
	 *
	 * @param amount the money.
	 * @param card the data cash card.
	 * @param authCode the authorization code.
	 * @param merchantReference the merchantReference
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardResponse doRefund(DataCashCard card, Money amount, String authCode,
			String merchantReference)
	{
		return processAuth(card, amount, authCode, REFUND_TRANSACTION, merchantReference);
	}

	/**
	 * This is responsible for performing the authorization of passed transaction type. Whenever this
	 * method catch exceptions from datacash such as Failure Report, then it throws a
	 * RunTimeException. Since only when the status code is returned from Datacash is 7, client can
	 * recover the flow, hence DataCashService Exception will be thrown.
	 *
	 *
	 * @param amount the money.
	 * @param transactionType the transaction type.
	 * @param card the <code>Card</code> object.
	 * @param authCode the authorization code.
	 * @param merchantReference the merchantReference
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	private CardResponse processAuth(DataCashCard card, Money amount, String authCode,
			String transactionType, String merchantReference)
	{
		CardResponse cardResponse = null;
		try
		{
			XMLDocument xmlDocument =
					getAuthXmlDocument(card, amount, authCode, transactionType, merchantReference);
			XMLDocument response = getAgent().request(xmlDocument, new Logger());
			cardResponse = new CardResponse(response);
			LogWriter.logDebugMessage(response.getSanitizedDoc());
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return cardResponse;
	}

	/**
	 * TODO added by Mukesh
	 * This Method for HCC
	 * @param amount the money.
	 * @param transactionType the transaction type.
	 * @param card the <code>Card</code> object.
	 * @param authCode the authorization code.
	 * @param merchantReference the merchantReference
	 *
	 * @return cardResponse the card response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */

	private CardResponse processHccAuth(DataCashCard card, Money amount, String authCode,
			String transactionType, String merchantReference, String hpsDatacashReference)
	{
		CardResponse cardResponse = null;
		try
		{
			XMLDocument xmlDocument =
					getHpsAuthXmlDocument(card, amount, authCode, transactionType, merchantReference, hpsDatacashReference);
			 if (ConfReader.getBooleanEntry("hcc.cv2avs.enabled", false))
	         {
				 card.populateAddressDetails(xmlDocument);
				 card.populateExtendedPolicy(xmlDocument, card.getCountry());
	         }
			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			XMLDocument response = getAgent().request(xmlDocument, new Logger());
			cardResponse = new CardResponse(response);
			LogWriter.logDebugMessage(response.getSanitizedDoc());
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return cardResponse;
	}

	/**
	 * This is responsible for fulfilling the actual payment.
	 *
	 * @param dataCashReference the data cash reference.
	 * @param amount the amount.
	 * @param authCode the authorization code.
	 *
	 * @return response the Response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public Response fulfill(String dataCashReference, String authCode, Money amount)
	{
		return processTransaction(dataCashReference, authCode, amount, FULFILL_TRANSACTION);
	}

	/**
	 * This is responsible for performing cancellation transaction.
	 *
	 * @param dataCashReference the data cash reference.
	 *
	 * @return response the Response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public Response cancel(String dataCashReference)
	{
		return processTransaction(dataCashReference, null, null, CANCEL_TRANSACTION);
	}

	/**
	 * This method is responsible for performing reversal of the pevious redemption transaction.
	 *
	 * @param dataCashReference the data cash reference.
	 *
	 * @return response the Response.
	 */
	public Response reverseGiftCardTransaction(String dataCashReference)
	{
		Response response = null;
		try
		{
			XMLDocument xmlDocument = new XMLDocument();

			setAuthentication(xmlDocument);
			xmlDocument.set(PPT_TXN_METHOD, PPT_REVERSE_TRANSACTION);
			xmlDocument.set(PPT_TXN_REFERENCE, dataCashReference);

			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());

			XMLDocument responseXml = getAgent().request(xmlDocument, new Logger());

			response = new Response(responseXml);

			LogWriter.logDebugMessage(responseXml.getSanitizedDoc());
		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return response;
	}

	/**
	 * This is responsible for refund transaction.
	 *
	 * @param dataCashReference the data cash reference.
	 * @param amount the amount.
	 *
	 * @return response the Response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public Response refund(String dataCashReference, Money amount)
	{
		return processTransaction(dataCashReference, null, amount, REVERSE_TRANSACTION);
	}

	/**
	 * This method is responsible for preRegisteredRefund transaction.
	 *
	 * @param dataCashReference the data cash reference number.
	 * @param merchantReference the merchant reference number.
	 * @param amount the transaction amount.
	 * @param captureMethod the capture method value.
	 *
	 * @return response the dataCash Response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public Response preRegisteredRefund(String dataCashReference, String merchantReference,
			Money amount, String captureMethod)
	{
		return processPreRegisteredTransaction(dataCashReference,
				merchantReference, amount, REFUND_TRANSACTION, captureMethod);
	}

	//TODO PayPal refund service
	public Response preRegisteredPayPalRefund(String dataCashReference, String merchantReference,
			Money amount)
	{


		Logger logger = new Logger();
		Response response = null;
		XMLDocument paypalRefundRequest=null;
		try
		{
			paypalRefundRequest=new XMLDocument();
			setAuthentication(paypalRefundRequest);
			paypalRefundRequest.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD,PayPalConstants.PAYPAL_REFUND);
			paypalRefundRequest.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_DATACASH_REFERENCE,dataCashReference);
			if(null!=amount){
				paypalRefundRequest.set(DataCashServiceConstants.AMOUNT,amount.getAmount().toString());
			}
			LogWriter.logDebugMessage(paypalRefundRequest.getSanitizedDoc());
			XMLDocument xmlResponse =
					getPaypalAgent().request(paypalRefundRequest, logger);
			LogWriter.logInfoMessage(getResponseStatus(xmlResponse,"txn_refund"));
			LogWriter.logDebugMessage(xmlResponse.getSanitizedDoc());
			response = new Response(xmlResponse);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		catch(Exception ex){

			LogWriter.logErrorMessage(ex.getMessage());
		}
		return response;
	}

	/**
	 * This is responsible for card holder verification.
	 *
	 * @param merchantUrl the merchantUrl.
	 * @param userAgent the userAgent.
	 * @param card the data cash card.
	 * @param amount the money.
	 * @param merchantReference the merchantReference.
	 *
	 * @return response the CardVerificationResponse.
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardVerificationResponse cardEnrolmentVerfication(String merchantUrl, String userAgent,
			DataCashCard card, Money amount, String merchantReference)
	{
		CardVerificationResponse cardVerficationResponse = null;
		try
		{
			XMLDocument xmlDocument =
					getAuthXmlDocument(card, amount, null, PRE_TRANSACTION, merchantReference);
			addThreeDSecureFeatures(xmlDocument, merchantUrl, userAgent);
			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			XMLDocument response = getAgent().request(xmlDocument, new Logger());
			cardVerficationResponse = new CardVerificationResponse(response);
			LogWriter.logInfoMessage("ACS url :" + cardVerficationResponse.getAcsUrl());
			LogWriter.logDebugMessage("PaReq :" + cardVerficationResponse.getPaReq());
			LogWriter.logDebugMessage(response.getSanitizedDoc());
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return cardVerficationResponse;
	}


	/**
	 * This is responsible for card holder verification.
	 *
	 * @param merchantUrl the merchantUrl.
	 * @param userAgent the userAgent.
	 * @param card the data cash card.
	 * @param amount the money.
	 * @param merchantReference the merchantReference.
	 *
	 * @return response the CardVerificationResponse.
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardVerificationResponse cardEnrolmentVerfication(String merchantUrl, String userAgent,
			DataCashCard card, Money amount, String merchantReference, String hpsDatacashReference)
	{
		CardVerificationResponse cardVerficationResponse = null;
		try
		{
			XMLDocument xmlDocument1 =
					getHpsAuthXmlDocument(card, amount, null, PRE_TRANSACTION, merchantReference, hpsDatacashReference);
			addThreeDSecureFeatures(xmlDocument1, merchantUrl, userAgent);
         if (ConfReader.getBooleanEntry("hcc.cv2avs.enabled", false))
         {
            card.populateExtendedPolicy(xmlDocument1, card.getCountry());
            card.populateAddressDetails(xmlDocument1);
         }
			LogWriter.logDebugMessage(xmlDocument1.getSanitizedDoc());
			XMLDocument response = getAgent().request(xmlDocument1, new Logger());
			cardVerficationResponse = new CardVerificationResponse(response);
			LogWriter.logInfoMessage("ACS url :" + cardVerficationResponse.getAcsUrl());
			LogWriter.logDebugMessage("PaReq :" + cardVerficationResponse.getPaReq());
			LogWriter.logDebugMessage(response.getSanitizedDoc());

		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return cardVerficationResponse;
	}


   public String hpsQueryTransaction(String hpsDatacashReference)
	{
		String Status  =
				getHpsQueryXml(hpsDatacashReference);
		return Status;

	}


	public Map<String,String> setUpReqHCS(String merchantUrl, String userAgent,
			DataCashCard card, Money amount, String merchantReference){

		Map<String, String> hcsMap = new HashMap<String, String>();
		XMLDocument xmlDocument1;
		int status;
		try {
			xmlDocument1 = new XMLDocument();
			xmlDocument1.set("Request.Transaction.HpsTxn.method", "setup");
			xmlDocument1.set("Request.Transaction.HpsTxn.return_url", "");
			xmlDocument1.set("Request.Transaction.HpsTxn.expiry_url", "www.abcdsyz.com");
			xmlDocument1.set("Request.Transaction.HpsTxn.page_set_id", "1234");
			xmlDocument1.set("Request.Transaction.HpsTxn.DynamicData.dyn_data_3", "show");
			xmlDocument1.set("Request.Transaction.HpsTxn.DynamicData.dyn_data_4", "http://shop.example.com/goback.html");
			//xmlDocument1.set(MERCHANT_REFERENCE, "7929456943744006342");
			xmlDocument1.set(MERCHANT_REFERENCE, merchantReference);
			xmlDocument1.set(new Amount(amount.getAmount().toString(), amount.getCurrency()
					.getCurrencyCode()));
			//CLIENT
			//xmlDocument1.set(CLIENT, "99639992");
			//xmlDocument1.set(PASSWORD, "RdRQv7g");

			setAuthentication(xmlDocument1);
			LogWriter.logInfoMessage("**********"+xmlDocument1.getSanitizedDoc());
			XMLDocument response1 = null;
			try {
				response1 = getAgent().request(xmlDocument1, new Logger());
				response1.get("");
			} catch (FailureReport e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String value = response1.get("Response.HpsTxn.hps_url");
			String value1 = response1.get("Response.HpsTxn.session_id");
			LogWriter.logInfoMessage("**********  " + value);
			LogWriter.logInfoMessage("**********  " + value1);
			LogWriter.logInfoMessage(response1.getSanitizedDoc());

			hcsMap.put("hpsUrl", value);
			hcsMap.put("hpsSession", value1);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hcsMap;
	}

	/**
	 * This is responsible for card holder verification.
	 *
	 * @param paRes the payer authentication response.
	 * @param datacashReference the datacashReference.
	 *
	 * @return response the Response.
	 * @throws RuntimeException when there is an unexpected error.
	 */
	public CardVerificationResponse authorize3DPayment(String paRes, String datacashReference)
	{
		CardVerificationResponse cardVerficationResponse = null;

		String modifiedPaRes = null;
		if (null != paRes)
		{
			modifiedPaRes =
					paRes.replaceAll(String.valueOf(DataCashServiceConstants.CARRIAGE_RETURN), "")
					.replaceAll(String.valueOf(DataCashServiceConstants.LINE_FEED), "").trim();
		}
		LogWriter.logDebugMessage("PaRes :" + paRes);
		try
		{
			XMLDocument xmlDocument = getTransactionXmlDocument(datacashReference, null, null, null);
			addThreeDSecureAuthDetails(xmlDocument, modifiedPaRes);
			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			XMLDocument response = getAgent().request(xmlDocument, new Logger());
			cardVerficationResponse = new CardVerificationResponse(response);
         LogWriter.logDebugMessage(response.getSanitizedDoc());
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return cardVerficationResponse;
	}

	/**
	 * This is responsible for processing the actual payment, cancellation and refunding. Whenever we
	 * catch exceptions from datacash such as FailureReport then it throws a RunTimeException.
	 *
	 * @param dataCashReference the data cash reference.
	 * @param amount the amount.
	 * @param transactionType the transaction type.
	 * @param authCode the authorization code.
	 *
	 * @return response the Response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	private Response processTransaction(String dataCashReference, String authCode, Money amount,
			String transactionType)
	{
		Logger logger = new Logger();
		Response response = null;
		try
		{
			XMLDocument xmlResponse =
					getAgent().request(
							getTransactionXmlDocument(dataCashReference, authCode, amount, transactionType),
							logger);
			LogWriter.logDebugMessage(xmlResponse.getSanitizedDoc());
			response = new Response(xmlResponse);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return response;
	}

	/**
	 * This method is responsible for processing the preRegister refunding. Whenever we
	 * catch exceptions from dataCash such as FailureReport it throws a RunTimeException.
	 *
	 * @param dataCashReference the data cash reference number.
	 * @param merchantReference the merchantReference number.
	 * @param amount the transaction amount.
	 * @param transactionType the transaction type.
	 * @param captureMethod the capture method value.
	 *
	 * @return response the Response.
	 *
	 * @throws RuntimeException when there is an unexpected error.
	 */
	private Response processPreRegisteredTransaction(String dataCashReference,
			String merchantReference, Money amount, String transactionType, String captureMethod)
	{
		Logger logger = new Logger();
		Response response = null;
		try
		{
			XMLDocument xmlResponse =
					getAgent().request(
							getPreRegisteredTransactionXmlDocument(dataCashReference, merchantReference,
									amount, transactionType, captureMethod), logger);
			LogWriter.logDebugMessage(xmlResponse.getSanitizedDoc());
			response = new Response(xmlResponse);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return response;
	}

	/**
	 * This is responsible for retrieving <code>Agent</code> object. It retrieves required
	 * information from the conf file. Whenever, we are not able to read the conf file or the entry
	 * is not found in the conf file,then it throws a runtime exception.
	 *
	 * @return the <code>Agent</code> object.
	 *
	 */
	private Agent getAgent()
	{
		Agent agent = new Agent();
		//This is the place u would get appropriate host
		String host = ConfReader.getConfEntry(HOST_NAME, null);

		if (StringUtils.isBlank(host))
		{
			LogWriter.logErrorMessage(PropertyResource.getProperty("datacash.host.notfound",
					MESSAGES_PROPERTY));
			throw new RuntimeException(PropertyResource.getProperty("datacash.host.notfound",
					MESSAGES_PROPERTY));
		}
		agent.setHost(host);
		agent.setTimeout(ConfReader.getIntEntry(DATACASH_CONNECTION_TIMEOUT, DEFAULT_TIMEOUT));
		return agent;
	}

	private Agent getPaypalAgent()
	{
		Agent agent = new Agent();
		String host = null;

		boolean payPalDevMode = ConfReader.getBooleanEntry(PAYPAL_DEV_MODE,false);

		if(!payPalDevMode)
		{
			LogWriter.logInfoMessage("datacash  server host entry is being read from cps configuration file.");
			host = ConfReader.getConfEntry(HOST_NAME, "");
		}
		else
		{
			LogWriter.logInfoMessage("PayPal accredation host entry is being read from cps configuration file for test environments ");
			host = ConfReader.getConfEntry(PAYPAL_HOST_NAME, "");
		}

		if (StringUtils.isBlank(host))
		{
			LogWriter.logErrorMessage(PropertyResource.getProperty("datacash.host.notfound",
					MESSAGES_PROPERTY));
			throw new RuntimeException(PropertyResource.getProperty("datacash.host.notfound",
					MESSAGES_PROPERTY));
		}
		agent.setHost(host);
		agent.setTimeout(ConfReader.getIntEntry(DATACASH_CONNECTION_TIMEOUT, DEFAULT_TIMEOUT));
		return agent;
	}


	/**
	 * This will create the <code>XMLDocument</code> required for all authorization of the
	 * transaction types. Whenever we catch exceptions from datacash such as IOException,
	 * JDOMException, then it throws a RunTimeException as its an application error.
	 *
	 * @param card The <code>Card</code> object of the transaction.
	 * @param amount The amount involved in the transaction.
	 * @param authCode The client auth code.
	 * @param transactionType The transaction type of the transaction.
	 * @param merchantReference the merchantReference
	 *
	 * @return The created <code>XMLDocument</code>.
	 */
	private XMLDocument getAuthXmlDocument(DataCashCard card, Money amount, String authCode,
			String transactionType, String merchantReference)
	{
		XMLDocument xmlDocument = card.getAuthXmlDocument(transactionType, authCode);

		setAuthentication(xmlDocument);

		xmlDocument.set(new Amount(amount.getAmount().toString(), amount.getCurrency()
				.getCurrencyCode()));
		xmlDocument.set(MERCHANT_REFERENCE, merchantReference);

		if (this.multipleEnvEnabled)
		{
			//xmlDocument.set(CAPTURE_METHOD, CAPTUREMETHOD_CNP);
			xmlDocument.set(CAPTURE_METHOD, card.getCaptureMethod());
		}
		LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
		return xmlDocument;
	}

	private XMLDocument getHpsAuthXmlDocument(DataCashCard card, Money amount, String authCode,
			String transactionType, String merchantReference, String hpsDatacashReference)
	{
		XMLDocument xmlDocument = card.getHpsAuthXmlDocument(transactionType, authCode, hpsDatacashReference);

		setAuthentication(xmlDocument);
		xmlDocument.set(new Amount(amount.getAmount().toString(),
		amount.getCurrency() .getCurrencyCode()));
		xmlDocument.set(MERCHANT_REFERENCE, merchantReference);

		if (this.multipleEnvEnabled)
		{
			//xmlDocument.set(CAPTURE_METHOD, CAPTUREMETHOD_CNP);
			xmlDocument.set(CAPTURE_METHOD, card.getCaptureMethod());
		}
		LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
		return xmlDocument;
	}


	//TODO need to check after POC
	private String getHpsQueryXml(String hpsDatacashReference)
	{

		XMLDocument xmlDocument = null;
		String queryStatus = null;
		try {
			xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);
			xmlDocument.set("Request.Transaction.HistoricTxn.reference", hpsDatacashReference);
			xmlDocument.set("Request.Transaction.HistoricTxn.method", "query");

			//xmlDocument.set(CLIENT, "99639992");
			//xmlDocument.set(PASSWORD, "RdRQv7g");

			//LogWriter.logInfoMessage("**********"+xmlDocument.getSanitizedDoc());
			XMLDocument response = null;
			try {
				response = getAgent().request(xmlDocument, new Logger());
				/*String value = response.get("Response.HpsTxn.cv2_captured");
				String value1 = response.get("Response.HpsTxn.expirydate");
				String value2 = response.get("Response.HpsTxn.pan");
				LogWriter.logInfoMessage("**********cv2_captured "+value);
				LogWriter.logInfoMessage("**********expirydate "+value1);
				LogWriter.logInfoMessage("**********pan "+value2);*/
				queryStatus = response.get("Response.HpsTxn.capture_status");

			} catch (FailureReport e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//LogWriter.logInfoMessage(response.getSanitizedDoc());


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
		return queryStatus;
	}

	/**
	 * This will create the <code>XMLDocument</code> required for transaction types. Whenever we
	 * catch exceptions from datacash such as IOException, JDOMException, then it throws a
	 * RunTimeException.
	 *
	 * @param dataCashReference The datacash reference returned by the auth.
	 * @param authCode The auth code returned by datacash during auth.
	 * @param amount The amount involved in the transaction.
	 * @param transactionType The transaction type of the transaction.
	 *
	 * @return The created <code>XMLDocument</code>.
	 */
	private XMLDocument getTransactionXmlDocument(String dataCashReference, String authCode,
			Money amount, String transactionType)
	{
		XMLDocument xmlDocument;
		try
		{
			xmlDocument = new XMLDocument();

			setAuthentication(xmlDocument);

			xmlDocument.set(DATACASH_REFERENCE, dataCashReference);

			if (authCode != null)
			{
				xmlDocument.set(AUTHORI_CODE, authCode);
			}

			if (amount != null)
			{
				xmlDocument.set(AMOUNT, amount.getAmount().toString());
			}
			if (null != transactionType)
			{
				xmlDocument.set(TRANSACT_TYPE, transactionType);
			}
		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
		return xmlDocument;
	}

	/**
	 * This will create the <code>XMLDocument</code> required for preRegistered refund transaction type.
	 * Whenever we catch exceptions from dataCash such as IOException, JDOMException, then it throws a
	 * RunTimeException.
	 *
	 * @param dataCashReference the dataCash reference number.
	 * @param merchantReference the merchantReference number.
	 * @param amount the amount involved in the transaction.
	 * @param transactionType the type of the transaction.
	 * @param captureMethod the capture method value.
	 *
	 * @return the <code>XMLDocument</code> created.
	 */
	private XMLDocument getPreRegisteredTransactionXmlDocument(String dataCashReference,
			String merchantReference, Money amount, String transactionType, String captureMethod)
	{
		XMLDocument xmlDocument;
		try
		{
			xmlDocument = new XMLDocument();

			setAuthentication(xmlDocument);

			setTransactionDetails(xmlDocument, amount, merchantReference, captureMethod);

			setCardTransactionDetails(xmlDocument, transactionType, dataCashReference);
		}
		catch (IOException ioe)
		{
			String message = ioe.getMessage();
			LogWriter.logErrorMessage(message, ioe);
			throw new RuntimeException(message, ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
		return xmlDocument;
	}

	/**
	 * Sets the authentication details for the given <code>XMLDocument</code>. Whenever, we are
	 * not able to read the conf file or the entry is not found in the conf file, then it throws a
	 * Runtime exception.
	 *
	 * @param xmlDocument The <code>XMLDocument</code> object for which, authentication details are
	 *        updated.
	 */
	private void setAuthentication(XMLDocument xmlDocument)
	{
		xmlDocument.set(CLIENT, vTid);
		xmlDocument.set(PASSWORD, password);
	}

	/**
	 * This method sets the transaction details for the given <code>XMLDocument</code>.
	 *
	 * @param xmlDocument the <code>XMLDocument</code> object for which, the transaction details are
	 *        updated.
	 * @param amount the transaction amount.
	 * @param merchantReference the merchantReference number.
	 * @param captureMethod the capture method value.
	 */
	private void setTransactionDetails(XMLDocument xmlDocument, Money amount,
			String merchantReference, String captureMethod)
	{
		xmlDocument.set(MERCHANT_REFERENCE, merchantReference);
		xmlDocument.set(new Amount(amount.getAmount().toString(), amount.getCurrency()
				.getCurrencyCode()));
		if (this.multipleEnvEnabled)
		{
			xmlDocument.set(CAPTURE_METHOD, captureMethod);
		}
	}

	/**
	 * This method sets the card transaction details for the given <code>XMLDocument</code>.
	 *
	 * @param xmlDocument the <code>XMLDocument</code> object for which, the card transaction
	 *        details are updated.
	 * @param transactionType the transactionType.
	 * @param dataCashReference the dataCashReference number.
	 */
	private void setCardTransactionDetails(XMLDocument xmlDocument, String transactionType,
			String dataCashReference)
	{
		xmlDocument.set(TRANSACTION_TYPE, transactionType);
		Hashtable<String, String> preRegisteredType = new Hashtable<String, String>();
		preRegisteredType.put("type", PREREGISTERED);
		xmlDocument.set(CARDTXN_CARDDETAILS, dataCashReference, preRegisteredType);
	}

	/**
	 * Add the three secure features.
	 *
	 * @param xmlDocument the XMLDocument.
	 * @param merchantUrl the merchantUrl.
	 * @param userAgent the userAgent.
	 */
	private void addThreeDSecureFeatures(XMLDocument xmlDocument, String merchantUrl,
			String userAgent)
	{
		xmlDocument.set(THREEDSECURE_VERIFY, "yes");
		xmlDocument.set(THREEDSECURE_MERCHANT_URL, merchantUrl);
		xmlDocument.set(THREEDSECURE_PURCAHSE_DESC, "TUI");
		DateFormat purchaseDate = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		xmlDocument.set(THREEDSECURE_PURCHASE_DATETIME, purchaseDate.format(new Date()));
		xmlDocument.set(THREEDSECURE_BROWSER_DEVICE_CATEGORY, "0");
		LogWriter.logDebugMessage("Browser Device Category : 0");
		xmlDocument.set(THREEDSECURE_BROWSER_HEADERS, "*/*");
		// xmlDocument.set(THREEDSECURE_BROWSER_USERAGENT, "IE/7.0");
		xmlDocument.set(THREEDSECURE_BROWSER_USERAGENT, userAgent);
		LogWriter.logDebugMessage("User Agent :" + userAgent);
	}

	/**
	 * Add the three secure authorization details.
	 *
	 * @param xmlDocument the XMLDocument.
	 * @param paRes the paRes.
	 */
	private void addThreeDSecureAuthDetails(XMLDocument xmlDocument, String paRes)
	{
		if (null != paRes)
		{
			Hashtable<String, String> threeDAuthData = new Hashtable<String, String>();
			xmlDocument.set(TRANSACT_TYPE, "threedsecure_authorization_request", threeDAuthData);
			xmlDocument.set(PARES_MESSAGE, paRes);
		}
		else
		{
			xmlDocument.set(TRANSACT_TYPE, "threedsecure_authorization_request");
		}
	}

	/**
	 * Handles the FailureReport returned from data cash.
	 *
	 * @param failureReport the instance of <code>FailureReport</code>.
	 */
	private void handleFailureReport(FailureReport failureReport)
	{
		String errorMessage =
				PropertyResource.getProperty(DATACASH_ERROR, MESSAGES_PROPERTY) + vTid + NEXT_LINE
				+ failureReport.getMessage();
		LogWriter.logErrorMessage(errorMessage, failureReport);
		throw new RuntimeException(errorMessage);
	}

	/**
	 * This method will send the Query Transaction Request to Datacash.
	 *
	 * @param datacashReference The datacash Reference.
	 * @return the QueryTransactionResponse xml object.
	 */
	public QueryTransactionResponse query(String datacashReference)
	{
		Logger logger = new Logger();
		QueryTransactionResponse response = null;
		try
		{
			XMLDocument xmlDocument =
					getAgent().request(getQueryRequestXML(datacashReference), logger);
			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			response = new QueryTransactionResponse(xmlDocument);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return response;
	}

	/**
	 * Creates Query Transaction XML request.
	 *
	 * @param datacashReference The datacash Reference.
	 * @return Query Transaction XML Object.
	 */
	private XMLDocument getQueryRequestXML(String datacashReference)
	{
		XMLDocument xmlDocument;

		try
		{
			xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);
			setQueryTransaction(xmlDocument, datacashReference);
		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
		return xmlDocument;
	}

	/**
	 * adds the Datacash reference to the Query Transaction Xml Object.
	 *
	 * @param xmlDocument The Query Transaction XML Document.
	 * @param datacashReference the Datacash Reference.
	 */
	private void setQueryTransaction(XMLDocument xmlDocument, String datacashReference)
	{
		xmlDocument.set(TRANSACT_TYPE, "query");
		xmlDocument.set(DATACASH_REFERENCE, datacashReference);
	}






	/*  The following method is for Hybris.*/
	@SuppressWarnings("unchecked")
	public XMLDocument getDDSetupXmlResponse(String datacashVTid,Map directDebitDetails)throws DataCashServiceException{
		String currentDate = null;
		/**
		 * ddSetup log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("directDebit.log.folderPath", "./logs/directDebit/");
		/**
		 * ddsetup log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("ddSetup.log.switch", "OFF");


		XMLDocument xmlDocument=null;

		XMLDocument response=null;


		try{
			xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);

			String sortcode=(String) directDebitDetails.get("sortCode");
			String accountnumber=(String) directDebitDetails.get("accountNumber");
			String firstName=(String) directDebitDetails.get("firstName");
			String lastName=(String) directDebitDetails.get("lastName");
			String method=(String) directDebitDetails.get("method");
			String merchant_reference=(String) directDebitDetails.get("merchantreference");
			String accountName=firstName+" "+lastName;
			xmlDocument.set(MERCHANT_REFERENCE,merchant_reference);
			xmlDocument.set(DD_SETUP,method);
			xmlDocument.set(DD_SORT_CODE,sortcode);
			xmlDocument.set(DD_ACCOUNT_NUMBER,accountnumber);
			xmlDocument.set(DD_ACCOUNT_NAME,accountName);

			LogWriter.logInfoMessage("Parameters for dd set up: \t sortcode="+sortcode+",accountnumber="+accountnumber+",accountname="+accountName+",method="+method+",merchant_reference="+merchant_reference);
			LogWriter.logDebugMessage("Before sending Request to datacash:\n"+xmlDocument.getSanitizedDoc());
			LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "ddSetupMandate",logFolderPath);
			}

			response = getAgent().request(xmlDocument, new Logger());
			LogWriter.logDebugMessage("Response From Datacash:\t method ="+response.get(DataCashServiceConstants.DD_METHOD_RS)+"\tstage = "+response.get(DataCashServiceConstants.DD_STAGE_RS)+"\t startDate="+response.get(DataCashServiceConstants.DD_START_DATE_RS)+"\t merchantReference = "+response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS)+"\t mode="+response.get(DataCashServiceConstants.MODE_RS)+"\t reason" +response.get(DataCashServiceConstants.REASON_RS)+"\t status = "+response.get(DataCashServiceConstants.STATUS_RS)+"\t datacashReference = "+response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
			LogWriter.logDebugMessage("Response From Datacash:\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "ddSetupMandate",logFolderPath);
			}



		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}

		return response;


	}







	//The following method is for atcore integration...
	@SuppressWarnings("unchecked")
	public XMLDocument getDDSetupXmlResponse(String datacashVTid,Map<String,String> applicationData,
			Object[] directDebitDetails)throws DataCashServiceException{

		XMLDocument xmlDocument=null;

		XMLDocument response=null;
		String currentDate = null;
		/**
		 * ddSetup log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("directDebit.log.folderPath", "./logs/directDebit/");
		/**
		 * ddsetup log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("ddSetup.log.switch", "OFF");

		try{

			xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);

			String sortcode=(String) directDebitDetails[0];
			String accountnumber=(String) directDebitDetails[1];
			String firstName=(String) directDebitDetails[2];
			String lastName=(String) directDebitDetails[3];
			String method=(String) directDebitDetails[4];
			String merchant_reference=(String) directDebitDetails[5];

			String accountName=firstName+" "+lastName;

			xmlDocument.set(MERCHANT_REFERENCE,merchant_reference);
			xmlDocument.set(DD_SETUP,method);
			xmlDocument.set(DD_SORT_CODE,sortcode);
			xmlDocument.set(DD_ACCOUNT_NUMBER,accountnumber);
			xmlDocument.set(DD_ACCOUNT_NAME,accountName);

			LogWriter.logInfoMessage("Parameters for dd set up: \t sortcode="+sortcode+",accountnumber="+accountnumber+",accountname="+accountName+",method="+method+",merchant_reference="+merchant_reference);
			LogWriter.logDebugMessage("Before sending Request to datacash:\n"+xmlDocument.getSanitizedDoc());
			LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "ddSetupMandate",logFolderPath);
			}

			response = getAgent().request(xmlDocument, new Logger());
			LogWriter.logDebugMessage("Response From Datacash:\t method ="+response.get(DataCashServiceConstants.DD_METHOD_RS)+"\tstage = "+response.get(DataCashServiceConstants.DD_STAGE_RS)+"\t startDate="+response.get(DataCashServiceConstants.DD_START_DATE_RS)+"\t merchantReference = "+response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS)+"\t mode="+response.get(DataCashServiceConstants.MODE_RS)+"\t reason" +response.get(DataCashServiceConstants.REASON_RS)+"\t status = "+response.get(DataCashServiceConstants.STATUS_RS)+"\t datacashReference = "+response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
			LogWriter.logDebugMessage("Response From Datacash:\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "ddSetupMandate",logFolderPath);
			}


		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}

		return response;


	}



	@SuppressWarnings("unchecked")
	public XMLDocument getDrawDownXmlResponse(String datacashVTid,Map<String,String> applicationData,
			Object[] drawdownDetails)throws DataCashServiceException{

		XMLDocument xmlDocument=null;

		XMLDocument response=null;
		String currentDate = null;
		/**
		 * ddSubmitDrawdown log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("directDebit.log.folderPath", "./logs/directDebit/");
		/**
		 * ddSubmitDrawdown log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("ddSubmitDrawdown.log.switch", "OFF");

		try{

			xmlDocument=getDrawDownXmlDocument(drawdownDetails);
			LogWriter.logDebugMessage("Before sending Request to datacash:\n"+xmlDocument.getSanitizedDoc());
			LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "ddSubmitDrawdown",logFolderPath);
			}
			response = getAgent().request(xmlDocument, new Logger());
			LogWriter.logDebugMessage("Response From Datacash:\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "ddSubmitDrawdown",logFolderPath);
			}


		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return response;

	}









	public XMLDocument getDrawDownXmlDocument( Object[] drawdownDetails){

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String formattedDate = sdf.format(date);

		String type = "";
		XMLDocument xmlDocument = null;
		try {

			Object[] zeroIndex = (Object[]) drawdownDetails[0];
			type = (String) zeroIndex[4];

			Element request = new Element(ApplicationDataConstants.REQUEST);
			Element authentication = new Element(
					ApplicationDataConstants.AUTHENTICATION);
			Element client = new Element(ApplicationDataConstants.CLIENT);
			Element password = new Element(ApplicationDataConstants.PASSWORD);
			client.setText(this.vTid);
			password.setText(this.password);

			authentication.addContent(client);
			authentication.addContent(password);
			request.addContent(authentication);
			Element transaction = new Element(
					ApplicationDataConstants.TRANSACTION);
			request.addContent(transaction);
			Element batchInputTxn = new Element(
					ApplicationDataConstants.BATCHINPUTTXN);
			Element batchfile = new Element(ApplicationDataConstants.BATCHFILE);

			if (StringUtils.equals(type, "ddrefund")) {
				batchfile.setAttribute(new Attribute(
						ApplicationDataConstants.FORMAT, "xml_directcredit"));

			} else {
				batchfile.setAttribute(new Attribute(
						ApplicationDataConstants.FORMAT, "xml_directdebit"));

			}

			Element batchInputRequest = new Element(
					ApplicationDataConstants.BATCHINPUTREQUEST);

			Element header = new Element(ApplicationDataConstants.HEADER);
			Element format = new Element(ApplicationDataConstants.FORMAT);
			Element headerReference = new Element(
					ApplicationDataConstants.REFERENCE);
			if (StringUtils.equals(type, "ddrefund")) {
				format.setText("xml_directcredit");
			} else {
				format.setText("xml_directdebit");
			}
			headerReference.setText("batchref" + formattedDate);
			header.addContent(format);
			header.addContent(headerReference);
			batchInputRequest.addContent(header);

			Element trasactions = new Element(
					ApplicationDataConstants.TRANSACTIONS);

			transaction.addContent(batchInputTxn);
			batchInputTxn.addContent(batchfile);

			batchInputRequest.addContent(trasactions);

			String sdatacashReferenceNumber = null;
			String samount = null;
			String sduedate = null;
			String smethod = null;
			String smerchantReferenceNumber = null;
			String tran_code = null;

			Element innerRequest = null;
			Element innerAuthentication = null;
			Element innerClient = null;
			Element innerPassword = null;
			Element innerTransaction = null;
			Element txnDetails = null;
			Element merchantreference = null;
			Element amount = null;
			Element historicTxn = null;
			Element method = null;
			Element reference = null;
			Element duedate = null;
			Element txn_count = null;
			Element txn_amount = null;
			BigDecimal total = BigDecimal.ZERO;

			Object object[] = null;

			for (int i = 0; i < drawdownDetails.length; i++) {

				object = (Object[]) drawdownDetails[i];

				sdatacashReferenceNumber = (String) object[0];
				samount = (String) object[1];
				sduedate = (String) object[2];
				tran_code = (String) object[3];
				smethod = (String) object[4];
				smerchantReferenceNumber = (String) object[5];
				LogWriter.logInfoMessage("{datacashReferenceNumber = "+sdatacashReferenceNumber+" , amount = "+samount+" , duadate = "+sduedate+" , tran_code = "+tran_code+" , method = "+smethod+" , merchantReference = "+smerchantReferenceNumber);

				innerRequest = new Element(ApplicationDataConstants.REQUEST);

				innerAuthentication = new Element(
						ApplicationDataConstants.AUTHENTICATION);

				innerClient = new Element(ApplicationDataConstants.CLIENT);
				innerPassword = new Element(ApplicationDataConstants.PASSWORD);
				innerClient.setText(this.vTid);
				innerPassword.setText(this.password);

				innerAuthentication.addContent(innerClient);
				innerAuthentication.addContent(innerPassword);
				innerRequest.addContent(innerAuthentication);

				innerTransaction = new Element(
						ApplicationDataConstants.TRANSACTION);
				txnDetails = new Element(ApplicationDataConstants.TXNDETAILS);


				amount = new Element(ApplicationDataConstants.AMOUNT);
				amount.setText(samount);
				total = total.add(new BigDecimal(samount));

				method = new Element(ApplicationDataConstants.METHOD);
				merchantreference = new Element(
						ApplicationDataConstants.MERCHANTREFERENCE);
				reference = new Element(ApplicationDataConstants.REFERENCE);

				txnDetails.addContent(merchantreference);
				txnDetails.addContent(amount);

				historicTxn = new Element(ApplicationDataConstants.HISTORICTXN);
				historicTxn.addContent(method);
				historicTxn.addContent(reference);

				if (StringUtils.equals(smethod, "drawdown") && sduedate != null) {
					duedate = new Element(ApplicationDataConstants.DUEDATE);
					duedate.setText(sduedate);
					duedate.setAttribute(new Attribute(
							ApplicationDataConstants.TRAN_CODE, tran_code));
					historicTxn.addContent(duedate);
					innerTransaction.addContent(txnDetails);

				}

				if (StringUtils.equalsIgnoreCase(smethod, "cancel")) {
					smethod = "cancel";
					sdatacashReferenceNumber = (String) object[5];
					smerchantReferenceNumber = (String) object[0];
					sdatacashReferenceNumber = truncateDatacashReference(sdatacashReferenceNumber);
					LogWriter
					.logInfoMessage("16 digit DPG Reference number after truncation to 11 digit is "
							+ sdatacashReferenceNumber);
				}

				method.setText(smethod);
				merchantreference.setText(smerchantReferenceNumber);
				reference.setText(sdatacashReferenceNumber);

				if (StringUtils.equals(smethod, "ddrefund")) {
					innerTransaction.addContent(txnDetails);
				}

				innerTransaction.addContent(historicTxn);
				innerRequest.addContent(innerTransaction);
				trasactions.addContent(innerRequest);

			}

			XMLOutputter outputter = new XMLOutputter();
			String batchString = outputter.outputString(batchInputRequest);
			LogWriter
			.logDebugMessage("BatchInputRequestXml for drawdowns====================================================================================================================================================================================================\n");
			LogWriter.logDebugMessage(batchString);
			LogWriter
			.logDebugMessage("======================================================================================================================================================================================================================================\n");

			String str = convertToZlibBase64(batchString);
			batchfile.setText(str);

			txn_count = new Element(ApplicationDataConstants.TXN_COUNT);
			txn_count.setText(drawdownDetails.length + "");
			txn_amount = new Element(ApplicationDataConstants.TOTAL_AMOUNT);
			txn_amount.setText(total + "");

			batchInputTxn.addContent(txn_count);
			batchInputTxn.addContent(txn_amount);

			xmlDocument = new XMLDocument(new Document(request));

		} catch (IOException ioe) {
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		} catch (JDOMException jdome) {
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}

		return xmlDocument;
	}

	/* This method truncates given input String by stripping the input from given starting index */
	private String truncateDatacashReference(final String datacashReference) {

		return StringUtils.reverse(StringUtils.substring(StringUtils.reverse(datacashReference), 0,11));

	}


	public Object[] getddDrawdownBatchStatus (Map<String, String> applicationData,Map<String,String> drawDownStatus)throws DataCashServiceException
	{


		XMLDocument xmlDocument=null;
		XMLDocument response=null;
		Object[] drawdownStatus=null;
		String currentDate = null;
		/**
		 * ddSubmitDrawdown log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("directDebit.log.folderPath", "./logs/directDebit/");
		/**
		 * ddSubmitDrawdown log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("getDDDrawDownBatchStatus.log.switch", null);
		try{
			xmlDocument=new XMLDocument();
			setAuthentication(xmlDocument);
			setDrawDownBatchDetails(xmlDocument,drawDownStatus);
			LogWriter.logDebugMessage("Before sending Request to datacash:\n"+xmlDocument.getSanitizedDoc());
			LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "getDDDrawDownBatchStatus",logFolderPath);
			}

			response = getAgent().request(xmlDocument, new Logger());

			LogWriter.logInfoMessage("Respose from datacash for getddDrawdownBatchStatus");

			LogWriter.logDebugMessage("Response From datacash:\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "getDDDrawDownBatchStatus",logFolderPath);
			}



			/*  if(!response.get(DataCashServiceConstants.STATUS_RS).equals(""+SUCCESS_STATUS))
		   {
			     String responseMsg = response.get(DataCashServiceConstants.DESCRIPTION);
			      LogWriter.logErrorMessage(responseMsg);
			      String errorCode = response.get(DataCashServiceConstants.STATUS_RS);
			      String errorMsg =
			         PropertyResource.getProperty(ERROR_MESSAGE_KEY + errorCode, MESSAGES_PROPERTY);
			      if (StringUtils.isEmpty(errorMsg))
			      {
			         errorMsg = responseMsg;
			      }
			      throw new DataCashServiceException(Integer.parseInt(errorCode),ERROR_MESSAGE_KEY + errorCode);
		   }
			 */

			if(response.get(DataCashServiceConstants.STATUS_RS).equals(""+SUCCESS_STATUS)){
				String responseString= response.get(RESPONSE_BatchInputTxn);


				String decodedString= convertBase64ZlibToString(responseString);
				SAXBuilder saxBuilder=new SAXBuilder("org.apache.xerces.parsers.SAXParser");
				Reader stringReader=new StringReader(decodedString);
				Document jdomDocument=saxBuilder.build(stringReader);

				drawdownStatus=processBatchDrawDowns(jdomDocument);


			}
			else
			{
				/*drawdownStatus=new Object[7];
			  drawdownStatus[0]=response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS);
			  drawdownStatus[1]="";
			  drawdownStatus[2]=response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS);
			  drawdownStatus[3]=response.get(DataCashServiceConstants.MODE_RS);
			  drawdownStatus[4]=response.get(DataCashServiceConstants.REASON_RS);
			  drawdownStatus[5]=response.get(DataCashServiceConstants.STATUS_RS);
			  drawdownStatus[6]=response.get(DataCashServiceConstants.TIME_RS);*/

				/*  drawdownStatus=new Object[5];
              drawdownStatus[0]=response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS);
              drawdownStatus[1]=response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS);
              drawdownStatus[2]=response.get(DataCashServiceConstants.REASON_RS);
              drawdownStatus[3]=response.get(DataCashServiceConstants.STATUS_RS);
              drawdownStatus[4]=response.get(DataCashServiceConstants.TIME_RS);*/


				Object ddReference = (Object)response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS);
				Object merchantreference       = (Object)response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS);
				Object reson       = (Object)response.get(DataCashServiceConstants.REASON_RS);
				Object status     = (Object)response.get(DataCashServiceConstants.STATUS_RS);
				Object time     = (Object)response.get(DataCashServiceConstants.TIME_RS);
				Object[] failureResponseObj;
				failureResponseObj=new Object[]{merchantreference,ddReference,reson,status,time};
				drawdownStatus=new Object[]{failureResponseObj};
			}


		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		// return response;
		return drawdownStatus;

	}


	public void setDrawDownBatchDetails(XMLDocument xmlDocument , Map<String,String> drawDownStatus){

		String datacashreference=(String)drawDownStatus.get("datacashReferenceNumber");
		String method=(String)drawDownStatus.get("method");
		String merchantreference=(String)drawDownStatus.get("merchantReferenceNumber");

		LogWriter.logInfoMessage("datacashReferenceNumber passed from client :"+datacashreference);
		LogWriter.logInfoMessage("method passed from client :"+method);



		xmlDocument.set(DATACASH_REFERENCE,datacashreference);
		xmlDocument.set(TRANSACT_TYPE,method);




	}


	public Object[] processBatchDrawDowns(Document document){
		Object[] object=null;

		try{
			Element rootNode = document.getRootElement();
			Element transactions= (Element)rootNode.getChildren().get(1);
			Element response=null;
			Element innerElement=null;

			ArrayList arrayList=null;
			String singleResponse="";

			List responseElements=transactions.getChildren();
			object=new Object[responseElements.size()];

			List responseInnerElements=null;
			for(int i=0;i<responseElements.size();i++){

				arrayList=new ArrayList();
				response=(Element)responseElements.get(i);
				responseInnerElements=response.getChildren();
				for(Object element:responseInnerElements){
					innerElement=(Element)element;

					if(innerElement.getName().equals("DirectDebitTxn>") || innerElement.getName().equals("CardTxn>")){
					}
					else
					{
						arrayList.add(innerElement.getText());
						singleResponse+=innerElement.getName()+" = "+innerElement.getText() +" ,";
					}

				}
				//below code is for swapping the status and reason attributes

				Collections.swap(arrayList, 2, 3);
				LogWriter.logInfoMessage(singleResponse+"=================");
				LogWriter.logInfoMessage(arrayList+"=================");
				object[i]=arrayList.toArray();
				arrayList.clear();


			}



		}
		catch (Exception jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		return object;
	}

	/*below method is for truncting 16 digit string to 11 digit*/
	private void truncatingtoLast11digit(ArrayList arrayList){
		String fullDigitdatacashreference = (String)arrayList.get(1);
		LogWriter.logInfoMessage("16digit DPG Reference number before truncation to 11 digit is"+fullDigitdatacashreference);
		StringBuilder stbr =new StringBuilder(fullDigitdatacashreference);
		String datacashreference=stbr.substring(5);
		arrayList.set(1, datacashreference);
	}


	public XMLDocument getCancelBatchXmlResponse(Map<String, String> applicationData,Object[] cancelMandateDetails)throws DataCashServiceException
	{


		XMLDocument xmlDocument=null;
		XMLDocument response=null;
		String currentDate = null;
		/**
		 * ddSubmitDrawdown log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("directDebit.log.folderPath", "./logs/directDebit/");
		/**
		 * ddSubmitDrawdown log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("submitCancelMandateBatch.log.switch", "OFF");
		try{
			xmlDocument=getCancelBatchXmlDocument(cancelMandateDetails);
			LogWriter.logInfoMessage("Sending Request XML to DPG for processing");
			LogWriter.logDebugMessage("sending Request to datacash:\n"+xmlDocument.getSanitizedDoc());
			LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "submitCancelMandateBatch",logFolderPath);
			}
			response = getAgent().request(xmlDocument, new Logger());
			LogWriter.logDebugMessage("Response From Datacash:\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "submitCancelMandateBatch",logFolderPath);
			}
			LogWriter.logInfoMessage("Received response XML from DPG after processing request");

			/*   if(!response.get(DataCashServiceConstants.STATUS_RS).equals(""+SUCCESS_STATUS))
			   {
				     String responseMsg = response.get(DataCashServiceConstants.DESCRIPTION);
				      LogWriter.logErrorMessage(responseMsg);
				      String errorCode = response.get(DataCashServiceConstants.STATUS_RS);
				      String errorMsg =
				         PropertyResource.getProperty(ERROR_MESSAGE_KEY + errorCode, MESSAGES_PROPERTY);
				      if (StringUtils.isEmpty(errorMsg))
				      {
				         errorMsg = responseMsg;
				      }
				      throw new DataCashServiceException(Integer.parseInt(errorCode), ERROR_MESSAGE_KEY + errorCode);
			   }*/

		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}

		return response;
	}






	public XMLDocument getCancelBatchXmlDocument(Object[] cancelMandateDetails)
	{

		XMLDocument xmlDocument=null;

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String formattedDate = sdf.format(date);

		try{


			Element request = new Element(ApplicationDataConstants.REQUEST);
			Element authentication=new Element(ApplicationDataConstants.AUTHENTICATION);
			Element client=new Element(ApplicationDataConstants.CLIENT);
			Element password=new Element(ApplicationDataConstants.PASSWORD);
			client.setText(this.vTid);
			password.setText(this.password);

			authentication.addContent(client);
			authentication.addContent(password);
			request.addContent(authentication);
			Element transaction=new Element(ApplicationDataConstants.TRANSACTION);
			request.addContent(transaction);
			Element batchInputTxn=new Element(ApplicationDataConstants.BATCHINPUTTXN);
			Element batchfile=new Element(ApplicationDataConstants.BATCHFILE);
			batchfile.setAttribute(new Attribute(ApplicationDataConstants.FORMAT, "xml_directdebit"));
			Element batchInputRequest=new Element(ApplicationDataConstants.BATCHINPUTREQUEST);
			Element header=new Element(ApplicationDataConstants.HEADER);
			Element format=new Element(ApplicationDataConstants.FORMAT);
			Element headerReference=new Element(ApplicationDataConstants.REFERENCE);
			format.setText("xml_directdebit");
			headerReference.setText("batchref"+formattedDate);
			header.addContent(format);
			header.addContent(headerReference);
			batchInputRequest.addContent(header);





			Element trasactions=new Element(ApplicationDataConstants.TRANSACTIONS);

			transaction.addContent(batchInputTxn);
			batchInputTxn.addContent(batchfile);

			batchInputRequest.addContent(trasactions);

			String sdatacashReferenceNumber=null;
			/*		   String samount=null;
		   String sduedate=null;*/
			String smethod=null;
			String smerchantReferenceNumber=null;
			//   String tran_code=null;


			Element innerRequest=null;
			Element innerAuthentication=null;
			Element innerClient=null;
			Element innerPassword=null;
			Element innerTransaction=null;
			Element txnDetails=null;
			Element merchantreference=null;
			Element amount=null;
			Element historicTxn=null;
			Element method=null;
			Element reference=null;
			Element duedate=null;
			Element txn_count=null;
			Element txn_amount=null;
			int total=0;


			Object object[]=null;

			for(int i=0;i<cancelMandateDetails.length;i++){

				object=(Object[])cancelMandateDetails[i];

				sdatacashReferenceNumber=(String)object[0];
				//	   samount=(String)object[1];
				//	   sduedate=(String)object[2];
				//	   tran_code=(String)object[3];
				smethod=(String)object[1];
				smerchantReferenceNumber=(String)object[2];


				LogWriter.logInfoMessage("{datacashReferenceNumber = "+sdatacashReferenceNumber+" , method = "+smethod+" , merchantReference = "+smerchantReferenceNumber);




				innerRequest=new Element(ApplicationDataConstants.REQUEST);

				innerAuthentication=new Element(ApplicationDataConstants.AUTHENTICATION);



				innerClient=new Element(ApplicationDataConstants.CLIENT);
				innerPassword=new Element(ApplicationDataConstants.PASSWORD);
				innerClient.setText(this.vTid);
				innerPassword.setText(this.password);






				innerAuthentication.addContent(innerClient);
				innerAuthentication.addContent(innerPassword);
				innerRequest.addContent(innerAuthentication);


				innerTransaction=new Element(ApplicationDataConstants.TRANSACTION);
				txnDetails=new Element(ApplicationDataConstants.TXNDETAILS);
				merchantreference=new Element(ApplicationDataConstants.MERCHANTREFERENCE);
				merchantreference.setText(smerchantReferenceNumber);

				//	   amount=new Element(ApplicationDataConstants.AMOUNT);
				//	   amount.setText(samount);
				//	   total+=Integer.parseInt(samount);

				txnDetails.addContent(merchantreference);
				//	   txnDetails.addContent(amount);

				innerTransaction.addContent(txnDetails);
				historicTxn=new Element(ApplicationDataConstants.HISTORICTXN);

				method=new Element(ApplicationDataConstants.METHOD);
				method.setText(smethod);

				reference=new Element(ApplicationDataConstants.REFERENCE);
				reference.setText(sdatacashReferenceNumber);

				//       duedate=new Element(ApplicationDataConstants.DUEDATE);
				//       duedate.setText(sduedate);
				//       duedate.setAttribute(new Attribute(ApplicationDataConstants.TRAN_CODE,tran_code));


				historicTxn.addContent(method);
				historicTxn.addContent(reference);
				//       historicTxn.addContent(duedate);

				innerTransaction.addContent(historicTxn);



				innerRequest.addContent(innerTransaction);

				trasactions.addContent(innerRequest);

			}


			XMLOutputter outputter = new XMLOutputter();
			String batchString = outputter.outputString(batchInputRequest);

			LogWriter.logDebugMessage("Batch Input Request for submitCancelMandate method : ==============================================================================");
			LogWriter.logDebugMessage(batchString);
			LogWriter.logDebugMessage("===================================================================================================================================");

			String str=this.convertToZlibBase64(batchString);
			batchfile.setText(str);
			txn_count=new Element(ApplicationDataConstants.TXN_COUNT);
			txn_count.setText(cancelMandateDetails.length+"");
			txn_amount=new Element(ApplicationDataConstants.TOTAL_AMOUNT);
			txn_amount.setText(total+"");

			batchInputTxn.addContent(txn_count);
			batchInputTxn.addContent(txn_amount);
			xmlDocument=new XMLDocument(new Document(request));





		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}

		return xmlDocument;


	}






	public Object[] getCancelMandateBatchStatus(Map<String,String> applicationData,Map<String,String> getBatchDetails)throws DataCashServiceException
	{

		String reference=getBatchDetails.get("datacashReferenceNumber");
		String method=getBatchDetails.get("method");

		LogWriter.logInfoMessage("datacashReferenceNumber passed from client :"+reference);
		LogWriter.logInfoMessage("method passed from client :"+method);

		Object[] objectResponse=null;
		XMLDocument xmlDocument=null;
		XMLDocument response =null;
		String currentDate = null;
		/**
		 * ddSubmitDrawdown log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("directDebit.log.folderPath", "./logs/directDebit/");
		/**
		 * ddSubmitDrawdown log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("getCancelMandateBatchStatus.log.switch", "OFF");

		try{
			xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);
			setQueryTransaction(xmlDocument,reference);

			LogWriter.logInfoMessage("Sending Request XML to DPG for processing");
			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();
			if(loggerSwitch.equalsIgnoreCase("ON")){

				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "getCancelMandateBatchStatus",logFolderPath);
			}
			response = getAgent().request(xmlDocument, new Logger());
			LogWriter.logInfoMessage("Received response XML from DPG after processing request");
			LogWriter.logDebugMessage(response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "getCancelMandateBatchStatus",logFolderPath);
			}




			/*   if(!response.get(DataCashServiceConstants.STATUS_RS).equals(""+SUCCESS_STATUS))
		   {
			     String responseMsg = response.get(DataCashServiceConstants.DESCRIPTION);
			      LogWriter.logErrorMessage(responseMsg);
			      String errorCode = response.get(DataCashServiceConstants.STATUS_RS);
			      String errorMsg =
			         PropertyResource.getProperty(ERROR_MESSAGE_KEY + errorCode, MESSAGES_PROPERTY);
			      if (StringUtils.isEmpty(errorMsg))
			      {
			         errorMsg = responseMsg;
			      }
			      throw new DataCashServiceException(Integer.parseInt(errorCode), ERROR_MESSAGE_KEY + errorCode);
		   }*/




			if(response.get(DataCashServiceConstants.STATUS_RS).equals(""+SUCCESS_STATUS))
			{

				String responseString= response.get(RESPONSE_BatchInputTxn);
				String decodedString= convertBase64ZlibToString(responseString);
				SAXBuilder saxBuilder=new SAXBuilder("org.apache.xerces.parsers.SAXParser");
				Reader stringReader=new StringReader(decodedString);
				Document jdomDocument=saxBuilder.build(stringReader);


				objectResponse=processCancelBatchStatusResponseXML(jdomDocument);
			}
			else
			{
				objectResponse=new Object[4];
				objectResponse[0]=response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS);
				objectResponse[1]=response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS);
				objectResponse[2]=response.get(DataCashServiceConstants.REASON_RS);
				objectResponse[3]=response.get(DataCashServiceConstants.STATUS_RS);


				Object merchantreference = (Object)response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS);
				Object ddReference = (Object)response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS);
				Object reson = (Object)response.get(DataCashServiceConstants.REASON_RS);
				Object status  = (Object)response.get(DataCashServiceConstants.STATUS_RS);
				Object[] failureResponseObj;
				failureResponseObj=new Object[]{merchantreference,ddReference,reson,status};
				objectResponse=new Object[]{failureResponseObj};

			}


		}
		catch (IOException ioe)
		{
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
		catch (JDOMException jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		catch (FailureReport failureReport)
		{
			handleFailureReport(failureReport);
		}
		return objectResponse;

	}

	public Object[] processCancelBatchStatusResponseXML(Document document){


		Object[] object=null;

		try{

			//Document document=xmlDocument.getJDOMDocument();

			Element rootNode = document.getRootElement();



			Element transactions= (Element)rootNode.getChildren().get(1);

			Element response=null;
			Element innerElement=null;

			ArrayList arrayList=null;
			String singleResponse="";

			List responseElements=transactions.getChildren();
			object=new Object[responseElements.size()];

			List responseInnerElements=null;
			for(int i=0;i<responseElements.size();i++){

				arrayList=new ArrayList();
				response=(Element)responseElements.get(i);
				responseInnerElements=response.getChildren();
				for(Object element:responseInnerElements){
					innerElement=(Element)element;
					if(innerElement.getName().equals("CardTxn") || innerElement.getName().equals("time") ||innerElement.getName().equals("DirectDebitTxn>")){
					}
					else
					{
						arrayList.add(innerElement.getText());
						singleResponse+=innerElement.getName()+" = "+innerElement.getText()+" ,";
					}

				}
				LogWriter.logDebugMessage(singleResponse+"================");
				Collections.swap(arrayList, 2, 3);
				LogWriter.logDebugMessage(arrayList+"================");
				object[i]=arrayList.toArray();
				arrayList.clear();


			}
		}
		catch (Exception jdome)
		{
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new RuntimeException(message, jdome);
		}
		return object;

	}



	public String convertToZlibBase64(String batchInputString){


		String encoded=null;
		try{
			byte[] bytes=batchInputString.getBytes("UTF-8");
			Deflater deflater = new Deflater();
			deflater.setInput(bytes);
			deflater.finish();

			byte[] bytesCompressed = new byte[Short.MAX_VALUE];

			int numberOfBytesAfterCompression = deflater.deflate(bytesCompressed);

			byte[] returnValues = new byte[numberOfBytesAfterCompression];

			Byte[] b=new Byte[numberOfBytesAfterCompression];;

			System.arraycopy
			(
					bytesCompressed,
					0,
					returnValues,
					0,
					numberOfBytesAfterCompression
					);

			encoded = DatatypeConverter.printBase64Binary(returnValues);
		}
		catch(Exception ex){

		}
		return encoded;
	}



	public String convertBase64ZlibToString(String batchInputString){

		byte[] bytesToDecompress=DatatypeConverter.parseBase64Binary(batchInputString);


		byte[] returnValues = null;

		Inflater inflater = new Inflater();

		int numberOfBytesToDecompress = bytesToDecompress.length;

		inflater.setInput
		(
				bytesToDecompress,
				0,
				numberOfBytesToDecompress
				);

		int bufferSizeInBytes = numberOfBytesToDecompress;

		int numberOfBytesDecompressedSoFar = 0;
		List<Byte> bytesDecompressedSoFar = new ArrayList<Byte>();

		try
		{
			while (inflater.needsInput() == false)
			{
				byte[] bytesDecompressedBuffer = new byte[bufferSizeInBytes];

				int numberOfBytesDecompressedThisTime = inflater.inflate
						(
								bytesDecompressedBuffer
								);

				numberOfBytesDecompressedSoFar += numberOfBytesDecompressedThisTime;

				for (int b = 0; b < numberOfBytesDecompressedThisTime; b++)
				{
					bytesDecompressedSoFar.add(bytesDecompressedBuffer[b]);
				}
			}

			returnValues = new byte[bytesDecompressedSoFar.size()];
			for (int b = 0; b < returnValues.length; b++)
			{
				returnValues[b] = (byte)(bytesDecompressedSoFar.get(b));
			}

		}
		catch (Exception dfe)
		{
			dfe.printStackTrace();
		}

		inflater.end();


		String result=null;
		try{
			result=new String(returnValues,0,returnValues.length,"UTF-8");
		}catch(Exception ex){}

		LogWriter.logDebugMessage("=============================================================================================================================================================================================================");


		LogWriter.logDebugMessage(result);

		LogWriter.logDebugMessage("=============================================================================================================================================================================================================");


		return result;


	}

	public XMLDocument populateSetUpPayPalRequestXMLDocument(
			PayPalSetExpressCheckoutData paypalExpressCheckout,String currencyRate) throws IOException {

		XMLDocument xmlDocument = null;
		try {
			xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);

			Hashtable<String,String> currency=new Hashtable<String,String>();
			currency.put("currency",currencyRate);
			xmlDocument.set(DataCashServiceConstants.AMOUNT,
					paypalExpressCheckout.getAmount(),currency);
			xmlDocument.set(DataCashServiceConstants.MERCHANT_REFERENCE,
					paypalExpressCheckout.getMerchantReference());
			if (StringUtils.equalsIgnoreCase(paypalExpressCheckout.getCardType(), PayPalConstants.PAYPAL_CREDIT)){
			xmlDocument.set(DataCashServiceConstants.FUNDING_SOURCE,"Finance");
			}
			xmlDocument.set(DataCashServiceConstants.PAYPAL_CANCEL_URL,
					paypalExpressCheckout.getCancelUrl());

			xmlDocument.set(DataCashServiceConstants.PAYPAL_RETURN_URL,
					paypalExpressCheckout.getReturnUrl());
			xmlDocument.set(DataCashServiceConstants.PAYPAL_METHOD,
					DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_SETEXPRESSCHECKOUT);
			if (StringUtils. isNotBlank(paypalExpressCheckout.getBillingType())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_BILLING_TYPE,
						paypalExpressCheckout.getBillingType());
			}
			xmlDocument.set(DataCashServiceConstants.PAYPAL_PAYMENT_ACTION,
					paypalExpressCheckout.getPaymentAction());

			if (StringUtils. isNotBlank(paypalExpressCheckout.getInvnum())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_INVNUM,
						paypalExpressCheckout.getInvnum());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout
					.getShippingAddressRequired())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_NO_SHIPPING,
						paypalExpressCheckout.getShippingAddressRequired());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout.getLocaleCode())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_LOCALE_CODE,
						paypalExpressCheckout.getLocaleCode());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout.getLogoImage())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_LOGO_IMAGE,
						paypalExpressCheckout.getLogoImage());
			}
			if (StringUtils
					. isNotBlank(paypalExpressCheckout.getCartBorderColor())) {
				xmlDocument.set(
						DataCashServiceConstants.PAYPAL_CART_BORDER_COLOUR,
						paypalExpressCheckout.getCartBorderColor());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout.getBrandType())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_BRAND_NAME,
						paypalExpressCheckout.getBrandType());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout.getHeaderStyle())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_HEADER_STYLE,
						paypalExpressCheckout.getHeaderStyle());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout.getChannelType())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_CHANNEL_TYPE,
						paypalExpressCheckout.getChannelType());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout.getPaymentType())) {
				xmlDocument.set(DataCashServiceConstants.PAYPAL_PAYMENT_TYPE,
						paypalExpressCheckout.getPaymentType());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout
					.getBillingAgreementDescription())) {
				xmlDocument
				.set(DataCashServiceConstants.PAYPAL_BILLING_AGREEMENT_DESCRIPTION,
						paypalExpressCheckout
						.getBillingAgreementDescription());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout
					.getBilling_agreement_custom())) {
				xmlDocument
				.set(DataCashServiceConstants.PAYPAL_BILLING_AGREEMENT_CUSTOM,
						paypalExpressCheckout
						.getBilling_agreement_custom());
			}
			if (StringUtils. isNotBlank(paypalExpressCheckout
					.getBillingAddressRequired())) {
				xmlDocument.set(
						DataCashServiceConstants.PAYPAL_REQ_BILLILNG_ADDRESS,
						paypalExpressCheckout.getBillingAddressRequired());
			}

			if (null != paypalExpressCheckout.getShippingAddress()) {
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getShippingAddress().getCity())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_SHIPPING_ADDRESS_CITY,
							paypalExpressCheckout.getShippingAddress()
							.getCity());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getShippingAddress().getName())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_SHIPPING_ADDRESS_NAME,
							paypalExpressCheckout.getShippingAddress()
							.getName());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getShippingAddress().getPostcode())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_SHIPPING_ADDRESS_POSTCODE,
							paypalExpressCheckout.getShippingAddress()
							.getPostcode());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getShippingAddress().getCountryCode())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE,
							paypalExpressCheckout.getShippingAddress()
							.getCountryCode());
				}
				if(paypalExpressCheckout.getShippingAddress()
						.getCountryCode().equals("US")){
					if (StringUtils. isNotBlank(paypalExpressCheckout
							.getShippingAddress().getRegion())) {
						xmlDocument
						.set(DataCashServiceConstants.PAYPAL_SHIPPING_ADDRESS_REGION,
								paypalExpressCheckout.getShippingAddress()
								.getRegion());
					}
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getShippingAddress().getStreetAddessOne())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1,
							paypalExpressCheckout.getShippingAddress()
							.getStreetAddessOne());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getShippingAddress().getTelephone_number())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_SHIPPING_ADDRESS_TELEPHONE_NUMBER,
							paypalExpressCheckout.getShippingAddress()
							.getTelephone_number());
				}

			}

			if (null != paypalExpressCheckout.getBillingAddress()) {

				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getBillingAddress().getCity())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_BILLING_ADDRESS_CITY,
							paypalExpressCheckout.getBillingAddress()
							.getCity());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getBillingAddress().getCountryCode())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_BILLING_ADDRESS_COUNTRY_CODE,
							paypalExpressCheckout.getBillingAddress()
							.getCountryCode());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getBillingAddress().getName())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_BILLING_ADDRESS_NAME,
							paypalExpressCheckout.getBillingAddress()
							.getName());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getBillingAddress().getPostcode())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_BILLING_ADDRESS_POSTCODE,
							paypalExpressCheckout.getBillingAddress()
							.getPostcode());
				}
				if (StringUtils. isNotBlank(paypalExpressCheckout
						.getBillingAddress().getStreetAddessOne())) {
					xmlDocument
					.set(DataCashServiceConstants.PAYPAL_BILLING_ADDRESS_STREET_ADDRESS1,
							paypalExpressCheckout.getBillingAddress()
							.getStreetAddessOne());
				}
				if(paypalExpressCheckout.getBillingAddress()
						.getCountryCode().equals("US")){
					if (StringUtils. isNotBlank(paypalExpressCheckout
							.getBillingAddress().getRegion())) {
						xmlDocument
						.set(DataCashServiceConstants.PAYPAL_BILLING_ADDRESS_REGION,
								paypalExpressCheckout.getBillingAddress()
								.getRegion());
					}
				}


			}

		} catch (JDOMException ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return xmlDocument;
	}

	public PayPalSetExpressCheckoutResponse populatesetUpPayPalResponseXMLDocument(
			XMLDocument response) throws IOException {

		PayPalSetExpressCheckoutResponse paypalExpressCheckoutResponse = new PayPalSetExpressCheckoutResponse();
		paypalExpressCheckoutResponse.setAcknowledgement(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ACK));
		paypalExpressCheckoutResponse.setBuild(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUILD));
		paypalExpressCheckoutResponse.setCorrelationId(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_CORRELATIONID_RS));
		paypalExpressCheckoutResponse.setDatacashReference(response
				.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
		paypalExpressCheckoutResponse.setMerchantReference(response
				.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
		paypalExpressCheckoutResponse.setPayPalVersion(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_VERSION));
		paypalExpressCheckoutResponse
		.setReason(response.get(DataCashServiceConstants.REASON_RS));
		paypalExpressCheckoutResponse.setToken(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TOKEN_RS));
		paypalExpressCheckoutResponse
		.setStatus(response.get(DataCashServiceConstants.STATUS_RS));

		LogWriter.logMethodEnd("setUpPayPal  :   -");

		return paypalExpressCheckoutResponse;
	}


	public PayPalSetExpressCheckoutResponse verifyPayPalSetExpressCheckoutResponse(PayPalSetExpressCheckoutResponse payPalSetExpressCheckoutResponse){

		XMLDocument setUpPayPalResponseXMLDocument = null;
		while(payPalConnectionAttempts<=DispatcherConstants.DEFAULT_FAILURE_COUNT){

			payPalConnectionAttempts++;

			if(payPalSetExpressCheckoutResponse.getStatus().equals(PayPalConstants.COMMUNICATION_ERROR)||payPalSetExpressCheckoutResponse.getStatus().equals(PayPalConstants.PAYPAL_NO_RESPONSE)){

				try {
					// ideal to query the paypal , instead of fairing the requets again

					setUpPayPalResponseXMLDocument = getPaypalAgent().request(setUpPayPalRequestXMLDocument, new Logger());
					LogWriter.logInfoMessage(getResponseStatus(setUpPayPalResponseXMLDocument,"setup")+" : cps token -> "+getCpsToken());
					payPalSetExpressCheckoutResponse = populatesetUpPayPalResponseXMLDocument(setUpPayPalResponseXMLDocument);

				}catch (FailureReport fr) {
					// TODO Auto-generated catch block
					LogWriter.logErrorMessage(fr.getMessage());
				} catch (Exception ex) {
					LogWriter.logErrorMessage(ex.getMessage());
				}
			}
		}
		return payPalSetExpressCheckoutResponse;
	}

	/**
	 *
	 * @param paypalExpressCheckout
	 * @return PayPalSetExpressCheckoutResponse
	 */

	public PayPalSetExpressCheckoutResponse getPaypalSECResponse(
			PayPalSetExpressCheckoutData paypalExpressCheckout,BookingComponent bookingComponent) {
		XMLDocument setUpPayPalResponseXMLDocument = null;

		String currentDate = null;
		/**
		 * setUpPayPal log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("payPal.log.folderPath", "./logs/payPal/");
		/**
		 * setUpPayPal log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("setUpPayPal.log.switch", "OFF");

		LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();

		try {
			setUpPayPalRequestXMLDocument = populateSetUpPayPalRequestXMLDocument(paypalExpressCheckout,bookingComponent.getTotalAmount().getCurrency().getCurrencyCode());

			LogWriter.logInfoMessage("Setup PayPal request xml for cpsToken :"+getCpsToken());

			LogWriter.logDebugMessage(setUpPayPalRequestXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(setUpPayPalRequestXMLDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "setUpPayPal",logFolderPath);
			}
			setUpPayPalResponseXMLDocument = getPaypalAgent().request(setUpPayPalRequestXMLDocument, new Logger());

			LogWriter.logInfoMessage(getResponseStatus(setUpPayPalResponseXMLDocument, "set_express_checkout")+" : cps token -> "+getCpsToken());

			LogWriter.logDebugMessage(setUpPayPalResponseXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(setUpPayPalResponseXMLDocument.getSanitizedDoc(), currentDate, "setUpPayPal",logFolderPath);
			}
			PayPalSetExpressCheckoutResponse payPalSetExpressCheckoutResponse = populatesetUpPayPalResponseXMLDocument(setUpPayPalResponseXMLDocument);

			return verifyPayPalSetExpressCheckoutResponse(payPalSetExpressCheckoutResponse);

		} catch (FailureReport fr) {
			// TODO Auto-generated catch block
			LogWriter.logErrorMessage(fr.getMessage());
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return payPalSetExpressCheckoutResponse;
	}


	public XMLDocument getExpressCheckoutDetails(String dataCashReference,
			String merchantReference) {

		XMLDocument response = null;
		String logCurrentDate = null;
		/**
		 * getExpressCheckoutDetails log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("payPal.log.folderPath", "./logs/payPal/");
		/**
		 * getExpressCheckoutDetails log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("getExpressCheckoutDetails.log.switch", "OFF");
		LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();


		try {
            ////Time in milli seconds
			XMLDocument xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);
			xmlDocument.set(DataCashServiceConstants.MERCHANT_REFERENCE,
					merchantReference + ""+System.currentTimeMillis());
			xmlDocument.set(DataCashServiceConstants.PAYPAL_DATACASH_REFERENCE,
					dataCashReference);
			xmlDocument.set(DataCashServiceConstants.PAYPAL_METHOD,
					DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_GETEXPRESSCHECKOUT);

			LogWriter.logDebugMessage("get_express_checkout_details request xmls for cpsToken :"+getCpsToken() +"\n"+xmlDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				logCurrentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "getExpressCheckoutDetails",logFolderPath);
			}
			response = getPaypalAgent().request(xmlDocument, new Logger());
			LogWriter.logInfoMessage(getResponseStatus(response,DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_GETEXPRESSCHECKOUT)+" : cps token -> "+getCpsToken());
			LogWriter.logDebugMessage("get_express_checkout_details response xmls for cpsToken : "+getCpsToken()+"\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), logCurrentDate, "getExpressCheckoutDetails",logFolderPath);
			}
		} catch (FailureReport fr) {
			// TODO Auto-generated catch block
			LogWriter.logErrorMessage(fr.getMessage());
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}

		return response;

	}

	public XMLDocument doExpressCheckoutPayment(String dataCashReference,
			String merchantReference, String amount , String currencyRate, PayPalSetExpressCheckoutData  paypalSetExpressCheckoutData ) {

		XMLDocument response = null;
		String currentDate = null;
		/**
		 * doExpressCheckoutPayment log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("payPal.log.folderPath", "./logs/payPal/");
		/**
		 * doExpressCheckoutPayment log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("doExpressCheckoutPayment.log.switch", "OFF");
		LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();

		try {
			XMLDocument xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);
			xmlDocument.set(DataCashServiceConstants.MERCHANT_REFERENCE,
					merchantReference + ""+System.currentTimeMillis());
			xmlDocument.set(DataCashServiceConstants.PAYPAL_DATACASH_REFERENCE,
					dataCashReference);
			xmlDocument.set(DataCashServiceConstants.PAYPAL_METHOD,
					DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_DOEXPRESSCHECKOUT);
			Hashtable<String, String> currency = new Hashtable<String, String>();
			currency.put("currency", currencyRate);
			xmlDocument.set(DataCashServiceConstants.AMOUNT, amount, currency);


			if(paypalSetExpressCheckoutData.getPaypalItems()!=null){
				int idValue=0;
				for(PayPalItems paypalItem : paypalSetExpressCheckoutData.getPaypalItems()){

					Hashtable<String,String> id=new Hashtable<String,String>();
					id.put("id",String.valueOf(idValue++));

					xmlDocument.set("Request.Transaction.PayPalTxn.Items.Item",null,id);

					if(!StringUtils.isEmpty(paypalItem.getName())){
						xmlDocument.set("Request.Transaction.PayPalTxn.Items.Item.name",paypalItem.getName());

					}
					if(!StringUtils.isEmpty(paypalItem.getAmount())){
						xmlDocument.set("Request.Transaction.PayPalTxn.Items.Item.amount",amount);
					}
					if(paypalItem.getQuantity()!=null){
						xmlDocument.set("Request.Transaction.PayPalTxn.Items.Item.quantity",paypalItem.getQuantity().longValue()+"");
					}

				}

			}

			LogWriter.logDebugMessage("do_express_checkout_payment request xmls for cpsToken :"+getCpsToken()+"\n"+xmlDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "doExpressCheckoutPayment",logFolderPath);
			}
			response = getPaypalAgent().request(xmlDocument, new Logger());
			LogWriter.logInfoMessage(getResponseStatus(response,DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_DOEXPRESSCHECKOUT)+" : cps token -> "+getCpsToken());
			LogWriter.logDebugMessage("do_express_checkout_payment response xmls for cpsToken :"+getCpsToken()+"\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "doExpressCheckoutPayment",logFolderPath);
			}
		} catch (FailureReport fr) {

			LogWriter.logErrorMessage(fr.getMessage());
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}

		return response;

	}

	public PayPalCustomerVerificationDetails getPaypalVerificationDetails(
			String token) {

		PayPalCustomerVerificationDetails paypalCustomerVerificationDetails = null;
		try {

			UUID uuid = UUID.fromString(token);
			PaymentData paymentData = PaymentStore.getInstance()
					.getPaymentData(uuid);
			BookingInfo bookingInfo = paymentData.getBookingInfo();
			BookingComponent bookingComponent = bookingInfo
					.getBookingComponent();
			PayPalComponent paypalComponent = bookingComponent
					.getPaypalComponent();
			PayPalSetExpressCheckoutData paypalExpressCheckout = paypalComponent
					.getPayPalExpressCheckout();
			CommonPaymentServiceHandler commonPaymentServiceHandler = new CommonPaymentServiceHandler();

			XMLDocument getExpressCheckoutDoc = this.getExpressCheckoutDetails(
					paypalComponent.getPayPalExpressCheckoutResponse()
					.getDatacashReference(), paypalComponent
					.getPayPalExpressCheckoutResponse()
					.getMerchantReference());

			paypalCustomerVerificationDetails = new PayPalCustomerVerificationDetails();

			if (isSuccess(getExpressCheckoutDoc.get(DataCashServiceConstants.STATUS_RS))) {
				// paypalCustomerVerificationDetails.setAddressStatus(xmlDocument.get("Response.PayPalTxn.BillingAddress.address_status"));

				paypalCustomerVerificationDetails=populateGetExpressCheckDetails(getExpressCheckoutDoc);
				ShippingAddress shippingAddress=getShippingAddress(getExpressCheckoutDoc);
				paypalCustomerVerificationDetails.setShippingAddress(shippingAddress);
				BillingAddress billingAddress=getBillingAddress(getExpressCheckoutDoc);
				paypalCustomerVerificationDetails.setBillingAddress(billingAddress);

			} else {
				paypalCustomerVerificationDetails
				.setDatacashReferance(getExpressCheckoutDoc
						.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
				paypalCustomerVerificationDetails
				.setMerchantReference(getExpressCheckoutDoc
						.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
				paypalCustomerVerificationDetails.setReason(getExpressCheckoutDoc
						.get(DataCashServiceConstants.RESPONSE_INFORMATION));
				paypalCustomerVerificationDetails.setStatus(getExpressCheckoutDoc
						.get(DataCashServiceConstants.STATUS_RS));
				paypalCustomerVerificationDetails.setReason(getExpressCheckoutDoc
						.get(DataCashServiceConstants.REASON_RS));
				return paypalCustomerVerificationDetails;
			}

			//** Populate the item Amount from paypalExpressCheckout , as there is only one quantity and the amount equals user Deposite type selection*/



			XMLDocument doExpressCheckoutPaymentDoc = this.doExpressCheckoutPayment(
					paypalComponent.getPayPalExpressCheckoutResponse()
					.getDatacashReference(), paypalComponent
					.getPayPalExpressCheckoutResponse()
					.getMerchantReference(), paypalExpressCheckout
					.getAmount(),bookingComponent.getTotalAmount().getCurrency().getCurrencyCode(),paypalComponent.getPayPalExpressCheckout());

			if (isSuccess(doExpressCheckoutPaymentDoc.get(DataCashServiceConstants.STATUS_RS))){

				paypalCustomerVerificationDetails=populateDoExpressCheckoutPaymentDetails(paypalCustomerVerificationDetails,doExpressCheckoutPaymentDoc);

			} else {
				paypalCustomerVerificationDetails
				.setDatacashReferance(doExpressCheckoutPaymentDoc
						.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
				paypalCustomerVerificationDetails
				.setMerchantReference(doExpressCheckoutPaymentDoc
						.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
				paypalCustomerVerificationDetails.setReason(doExpressCheckoutPaymentDoc
						.get(DataCashServiceConstants.RESPONSE_INFORMATION));
				paypalCustomerVerificationDetails.setStatus(doExpressCheckoutPaymentDoc
						.get(DataCashServiceConstants.STATUS_RS));
				paypalCustomerVerificationDetails.setReason(doExpressCheckoutPaymentDoc
						.get(DataCashServiceConstants.REASON_RS));
				return paypalCustomerVerificationDetails;
			}

		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());

		}

		return paypalCustomerVerificationDetails;

	}

	private ShippingAddress getShippingAddress(XMLDocument getExpressCheckoutDoc){

		ShippingAddress shippingAddress = new ShippingAddress();
		shippingAddress
		.setCity(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_CITY_RS));
		shippingAddress
		.setName(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_NAME_RS));
		shippingAddress
		.setCountryCode(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE_RS));
		shippingAddress
		.setRegion(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_REGION_RS));
		shippingAddress
		.setStreetAddessOne(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1_RS));
		shippingAddress
		.setPostcode(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_POSTCODE_RS));

		return shippingAddress;
	}

	private BillingAddress getBillingAddress(XMLDocument getExpressCheckoutDoc){
		BillingAddress billingAddress = new BillingAddress();
		billingAddress
		.setCity(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BILLING_ADDRESS_CITY_RS));
		billingAddress
		.setName(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BILLING_ADDRESS_NAME_RS));
		billingAddress
		.setCountryCode(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BILLING_ADDRESS_COUNTRY_CODE_RS));
		billingAddress
		.setPostcode(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BILLING_ADDRESS_POSTCODE_RS));
		billingAddress
		.setStreetAddessOne(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BILLING_ADDRESS_STREET_ADDRESS1_RS));
		billingAddress
		.setAddressStatus(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BILLING_ADDRESS_STATUS_RS));

		return billingAddress;
	}

	private PayPalCustomerVerificationDetails populateGetExpressCheckDetails(XMLDocument getExpressCheckoutDoc){

		PayPalCustomerVerificationDetails paypalCustomerVerificationDetails=new PayPalCustomerVerificationDetails();

		paypalCustomerVerificationDetails
		.setCountryCode(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_COUNTRY_CODE_RS));
		paypalCustomerVerificationDetails
		.setFirstName(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_FIRSTNAME_RS));
		paypalCustomerVerificationDetails
		.setHandlingAmount(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_HANDLING_AMT_RS));
		paypalCustomerVerificationDetails
		.setInsuranceAmount(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_INSURANCEAMT_RS));
		paypalCustomerVerificationDetails
		.setLastName(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_LAST_NAME_RS));
		paypalCustomerVerificationDetails
		.setCurrencyCode(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_CURRENCY_CODE_RS));
		paypalCustomerVerificationDetails
		.setCustomerEmailId(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_EMAIL_RS));
		paypalCustomerVerificationDetails
		.setPayerId(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PAYERID_RS));
		paypalCustomerVerificationDetails
		.setPayerStatus(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PAYER_STATUS_RS));
		paypalCustomerVerificationDetails
		.setShippingCost(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPAMT_RS));
		paypalCustomerVerificationDetails
		.setShippingDiscountAmount(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPDISAMT_RS));
		paypalCustomerVerificationDetails
		.setItemAmount(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ITEMAMT_RS));

		paypalCustomerVerificationDetails
		.setAddressId(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ADDRESSID_RS));
		paypalCustomerVerificationDetails
		.setAddressStatus(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ADDRESS_STATUS_RS));
		paypalCustomerVerificationDetails
		.setAllowedPaymentMethod(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ALLOWED_PAYMENT_METHOD_RS));
		paypalCustomerVerificationDetails
		.setBusinessName(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUSINESS_RS));
		paypalCustomerVerificationDetails
		.setCheckoutStatus(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_CHECKOUT_STATUS_RS));
		paypalCustomerVerificationDetails
		.setCustom(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_CUSTOM_RS));
		paypalCustomerVerificationDetails
		.setFinancingFeeAmout(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_FINANCING_FEE_AMT_RS));
		paypalCustomerVerificationDetails
		.setFinancingMonthlyPayment(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_FINANCING_MONTHLY_PAYMENT_RS));
		paypalCustomerVerificationDetails
		.setFinancingTotalCost(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_FINANCING_TOTAL_COST_RS));
		paypalCustomerVerificationDetails
		.setFinancingTerm(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_FINANCING_TERM_RS));
		paypalCustomerVerificationDetails
		.setCartChangeTolarance(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_CARTCHANGE_TOLERANCE_RS));
		paypalCustomerVerificationDetails
		.setIsFinancing(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ISFINANCING_RS));

		paypalCustomerVerificationDetails
		.setMiddleName(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_MIDDLE_NAME_RS));
		paypalCustomerVerificationDetails
		.setNoteText(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_NOTETEXT_RS));
		paypalCustomerVerificationDetails
		.setNotifyUrl(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_NOTIFYURL_RS));
		paypalCustomerVerificationDetails
		.setPhoneNumber(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PHONE_NUM_RS));
		paypalCustomerVerificationDetails
		.setSalutation(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SALUTATION_RS));
		paypalCustomerVerificationDetails
		.setPaymentRequestId(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PAYMENT_REQUEST_ID_RS));
		paypalCustomerVerificationDetails
		.setShippingCalculationMode(getExpressCheckoutDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_CALCULATION_MODE_RS));
		return paypalCustomerVerificationDetails;
	}

	private PayPalCustomerVerificationDetails populateDoExpressCheckoutPaymentDetails(PayPalCustomerVerificationDetails paypalCustomerVerificationDetails,XMLDocument doExpressCheckoutPaymentDoc)
	{
		paypalCustomerVerificationDetails
		.setInsuranceOptionSelected(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_INSURANCE_OPTION_SELECTED_RS));
		paypalCustomerVerificationDetails
		.setPaymentStatus(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PAYMENT_STATUS_RS));
		paypalCustomerVerificationDetails
		.setPaymentType(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PAYMENT_TYPE_RS));
		paypalCustomerVerificationDetails
		.setCorrelationId(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_CORRELATIONID_RS));
		paypalCustomerVerificationDetails
		.setBillingAgreementId(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BILLING_AGREEMENT_ID_RS));
		paypalCustomerVerificationDetails
		.setAcknowledgement(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ACK));
		// paypalCustomerVerificationDetails.setAddressId(xmlDocument2.get(""));
		paypalCustomerVerificationDetails
		.setTransactionId(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_ID_RS));
		paypalCustomerVerificationDetails
		.setTransactionType(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_TYPE_RS));
		paypalCustomerVerificationDetails
		.setDatacashReferance(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
		paypalCustomerVerificationDetails
		.setMerchantReference(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
		paypalCustomerVerificationDetails
		.setBuild(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUILD));
		paypalCustomerVerificationDetails
		.setAmount(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AMT));
		paypalCustomerVerificationDetails
		.setOrderTime(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ORDERTIME));
		paypalCustomerVerificationDetails
		.setPayPalVersion(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_VERSION));

		paypalCustomerVerificationDetails
		.setTimestamp(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TIMESTAMP));
		paypalCustomerVerificationDetails
		.setTaxCharged(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TAXAMT));
		paypalCustomerVerificationDetails.setStatus(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstants.STATUS_RS));
		paypalCustomerVerificationDetails.setReason(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstants.REASON_RS));
		paypalCustomerVerificationDetails
		.setCurrencyExchangeRate(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_EXCHANGE_RATE));
		paypalCustomerVerificationDetails
		.setSettledAmount(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SETTLE_AMT));
		paypalCustomerVerificationDetails
		.setToken(doExpressCheckoutPaymentDoc
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TOKEN_RS));

		return paypalCustomerVerificationDetails;
	}

	public XMLDocument doCaptureResponse(
			PayPalCaptureRequest paypalCaptureRequest) {

		XMLDocument xmlDocument = null;

		XMLDocument response = null;
		String currentDate = null;
		/**
		 * doCapturePayPalTransaction log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("payPal.log.folderPath", "./logs/payPal/");
		/**
		 * doCapturePayPalTransaction log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("doCapturePayPalTransaction.log.switch", "OFF");

		LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();



		try {

			xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);
			xmlDocument.set(DataCashServiceConstants.AMOUNT,
					paypalCaptureRequest.getAmount());
			xmlDocument.set(DataCashServiceConstants.PAYPAL_METHOD,
					DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_DOCAPTURE);
			xmlDocument
			.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_DATACASH_REFERENCE,
					paypalCaptureRequest.getDatacashReferance());
			if (StringUtils. isNotBlank(paypalCaptureRequest.getIsCompleted())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_COMPLETED,
						paypalCaptureRequest.getIsCompleted());
			}
			if (StringUtils. isNotBlank(paypalCaptureRequest.getInvnum())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_INVNUM,
						paypalCaptureRequest.getInvnum());
			}
			if (StringUtils. isNotBlank(paypalCaptureRequest
					.getMessageSubmissionId())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_MSGSUB,
						paypalCaptureRequest.getMessageSubmissionId());
			}

			if (StringUtils. isNotBlank(paypalCaptureRequest.getNoteText())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_NOTE,
						paypalCaptureRequest.getNoteText());
			}
			if (StringUtils. isNotBlank(paypalCaptureRequest.getSoftDescriptor())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SOFT_DESCRIPTOR,
						paypalCaptureRequest.getSoftDescriptor());
			}
			if(paypalCaptureRequest
					.getAirlineItenaryData()!=null){


				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getClearingCount())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_CLEARING_COUNT,
							paypalCaptureRequest.getAirlineItenaryData()
							.getClearingCount());
				}

				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getClearingSequence())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_CLEARING_SEQUENCE,
							paypalCaptureRequest.getAirlineItenaryData()
							.getClearingSequence());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getIssuingCarrierCode())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_ISSUING_CARRIER_CODE,
							paypalCaptureRequest.getAirlineItenaryData()
							.getIssuingCarrierCode());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getPassengerName())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_PASSENGER_NAME,
							paypalCaptureRequest.getAirlineItenaryData()
							.getPassengerName());
				}

				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getRestrictedTicket())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_RESTRICTED_TICKET,
							paypalCaptureRequest.getAirlineItenaryData()
							.getRestrictedTicket());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getTicketNumber())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_TICKET_NUMBER,
							paypalCaptureRequest.getAirlineItenaryData()
							.getTicketNumber());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getCustomerCode())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_CUSTOMER_CODE,
							paypalCaptureRequest.getAirlineItenaryData()
							.getCustomerCode());
				}

				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getIssueDate())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_ISSUE_DATE,
							paypalCaptureRequest.getAirlineItenaryData()
							.getIssueDate());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getTotalFare())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_TOTAL_FARE,
							paypalCaptureRequest.getAirlineItenaryData()
							.getTotalFare());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getTotalFee())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_TOTAL_FEE,
							paypalCaptureRequest.getAirlineItenaryData()
							.getTotalFee());
				}

				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getTotal_taxes())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_TOTAL_TAXES,
							paypalCaptureRequest.getAirlineItenaryData()
							.getTotal_taxes());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getTravelAgencyCode())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_TRAVEL_AGENCY_CODE,
							paypalCaptureRequest.getAirlineItenaryData()
							.getTravelAgencyCode());
				}
				if (StringUtils. isNotBlank(paypalCaptureRequest
						.getAirlineItenaryData().getTravelAgencyName())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_TRAVEL_AGENCY_NAME,
							paypalCaptureRequest.getAirlineItenaryData()
							.getTravelAgencyName());
				}

				if(paypalCaptureRequest.getAirlineItenaryData().getFlightDetails()!=null){

					Hashtable<String,String> leg_id=new Hashtable<String,String>();
					leg_id.put("leg_id","0");
					xmlDocument.set("Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails",null,leg_id);

					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getArrivalAirportCode())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_ARRIVAL_AIRPORT_CODE,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails()
								.getArrivalAirportCode());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getDepartureAirportCode())) {
						xmlDocument
						.set("Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.departure_airport_code",
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails()
								.getDepartureAirportCode());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getCarrierCode())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_CARRIER_CODE,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getCarrierCode());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getDepartureTime())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_DEPARTURE_TIME,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getDepartureTime());
					}

					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getFareBasisCode())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_FARE_BASIS_CODE,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getFareBasisCode());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getFlightNumber())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_FLIGHT_NUMBER,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getFlightNumber());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getServiceClass())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_SERVICE_CLASS,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getServiceClass());
					}

					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getStopoverCode())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_STOPOVER_CODE,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getStopoverCode());
					}
					if (StringUtils
							. isNotBlank(paypalCaptureRequest.getAirlineItenaryData()
									.getFlightDetails().getTravelDate())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_TRAVEL_DATE,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getTravelDate());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getConjunctionTicket())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_CONJUCTION_TICKET,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails()
								.getConjunctionTicket());
					}

					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getCouponNumber())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_COUPON_NUMBER,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getCouponNumber());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getEndorsementRestrictions())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_ENDORS_OR_RESTRS,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails()
								.getEndorsementRestrictions());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails()
							.getExchangeTicket())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_EXCHANGE_TICKET,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getExchangeTicket());
					}

					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails().getFare())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_FARE,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getFare());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails().getFee())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_FARE,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getFee());
					}
					if (StringUtils. isNotBlank(paypalCaptureRequest
							.getAirlineItenaryData().getFlightDetails().getTaxes())) {
						xmlDocument
						.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_TAXES,
								paypalCaptureRequest.getAirlineItenaryData()
								.getFlightDetails().getTaxes());
					}


				}



			}


			LogWriter.logDebugMessage("paypal do_capture request xml for cpsToken :"+getCpsToken()+"\n"+xmlDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(xmlDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "doCapturePayPalTransaction",logFolderPath);
			}
			response = getPaypalAgent().request(xmlDocument, new Logger());
			LogWriter.logInfoMessage(getResponseStatus(response,DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_DOCAPTURE)+":cpsToken ->"+getCpsToken());
			LogWriter.logDebugMessage("paypal do_capture response xml for cpsToken : "+getCpsToken()+"\n"+response.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(response.getSanitizedDoc(), currentDate, "doCapturePayPalTransaction",logFolderPath);
			}

		} catch (FailureReport fr) {

			LogWriter.logErrorMessage(fr.getMessage());
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}

		return response;

	}

	public PayPalCaptureResponse doCapturePaypalTransaction(String cpsToken,
			PayPalCaptureRequest paypalCaptureRequest) {

		PayPalCaptureResponse paypalCaptureResponse = null;
		try {

			response = this.doCaptureResponse(paypalCaptureRequest);
			paypalCaptureResponse = new PayPalCaptureResponse();
			paypalCaptureResponse
			.setAcknowledgement(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ACK));
			paypalCaptureResponse
			.setAmount(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AMT));
			paypalCaptureResponse
			.setCurrencyExchangeRate(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_EXCHANGE_RATE));
			paypalCaptureResponse
			.setBuild(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUILD));
			paypalCaptureResponse
			.setTransactionCharge(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TAXAMT));
			paypalCaptureResponse
			.setMessageSubmissionId(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_MSGSUB_ID));
			paypalCaptureResponse
			.setOrderTime(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ORDERTIME));
			paypalCaptureResponse
			.setPaymentStatus(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PAYMENT_STATUS_RS));

			paypalCaptureResponse
			.setPaymentType(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PAYMENT_TYPE_RS));
			paypalCaptureResponse
			.setSettledAmount(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SETTLE_AMT));
			paypalCaptureResponse
			.setTaxCharged(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TAXAMT));
			paypalCaptureResponse
			.setTimestamp(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TIMESTAMP));

			paypalCaptureResponse
			.setTransactionId(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_ID_RS));
			paypalCaptureResponse
			.setTransactionType(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_TYPE_RS));
			paypalCaptureResponse
			.setPayPalVersion(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_VERSION));
			paypalCaptureResponse
			.setAuthorizationId(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AUTHORIZATION_ID_RS));
			paypalCaptureResponse
			.setParentTransactionId(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_PARENT_TRANSACTION_ID_RS));
			paypalCaptureResponse
			.setReceiptNumber(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_RECEIPT_RS));
			paypalCaptureResponse.setDatacashReferance(response
					.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
			paypalCaptureResponse.setReason(response
					.get(DataCashServiceConstants.REASON_RS));
			paypalCaptureResponse.setStatus(response
					.get(DataCashServiceConstants.STATUS_RS));

			ShippingAddress shippingAddress = new ShippingAddress();
			shippingAddress
			.setCity(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_CITY_RS));
			shippingAddress
			.setName(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_NAME_RS));
			shippingAddress
			.setCountryCode(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE_RS));
			shippingAddress
			.setRegion(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_REGION_RS));
			shippingAddress
			.setStreetAddessOne(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1_RS));
			shippingAddress
			.setPostcode(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_POSTCODE_RS));
			paypalCaptureResponse.setShippingAddress(shippingAddress);

		} catch (Exception ex) {

			LogWriter.logErrorMessage(ex.getMessage());

		}
		return paypalCaptureResponse;
	}
	/**
	 * This method is responsible for populating data in an xml request.
	 * @param authorizePayPalTransaction
	 * @param payPalExpressCheckoutResponse
	 * @return
	 * @throws IOException
	 */
	public XMLDocument populateAuthorizePayPalTransactionRequestXML(
			AuthorizePayPalTransaction authorizePayPalTransaction,
			String merchantRef) throws IOException {

		XMLDocument xmlDocument = null;

		try {
			xmlDocument = new XMLDocument();

			setAuthentication(xmlDocument);

			if (StringUtils.isNotBlank(authorizePayPalTransaction
					.getTransactionMethod())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD,
						DataCashServiceConstantsForPayPalTransaction
						.PAYPAL_METHOD_DOAUTHORIZATION);
			}
			if (StringUtils.isNotBlank(authorizePayPalTransaction
					.getDatacashReferance())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_DATACASH_REFERENCE,
						authorizePayPalTransaction
						.getDatacashReferance());
			}
			if (StringUtils.isNotBlank(authorizePayPalTransaction
					.getAmount())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_AUTHORIZE_AMOUNT,
						authorizePayPalTransaction.getAmount());
			}
			if (StringUtils.isNotBlank(authorizePayPalTransaction
					.getMessageSubmissionId())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_MSGSUB,
						authorizePayPalTransaction
						.getMessageSubmissionId());
			}
			if (Objects.nonNull(authorizePayPalTransaction.getShippingAddress())) {

				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getAmount())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_AMOUNT,
							authorizePayPalTransaction
							.getShippingAddress().getAmount());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getCity())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_CITY,
							authorizePayPalTransaction
							.getShippingAddress().getCity());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getCountryCode())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE,
							authorizePayPalTransaction
							.getShippingAddress()
							.getCountryCode());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getName())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_NAME,
							authorizePayPalTransaction
							.getShippingAddress().getName());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getPostcode())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_POSTCODE,
							authorizePayPalTransaction
							.getShippingAddress().getPostcode());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getRegion())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_REGION,
							authorizePayPalTransaction
							.getShippingAddress().getRegion());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getStreetAddessOne())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1,
							authorizePayPalTransaction
							.getShippingAddress()
							.getStreetAddessOne());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getStreetAddessTwo())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS2,
							authorizePayPalTransaction
							.getShippingAddress()
							.getStreetAddessTwo());
				}
				if (StringUtils.isNotBlank(authorizePayPalTransaction
						.getShippingAddress().getTelephone_number())) {
					xmlDocument
					.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_TELEPHONE_NUMBER,
							authorizePayPalTransaction
							.getShippingAddress()
							.getTelephone_number());
				}
			}

			/*xmlDocument
			.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_MERCHANT_REFERENCE,
					merchantRef);*/

		} catch (JDOMException ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return xmlDocument;
	}
	/**
	 * This method is responsible populating the datacash response for succesfully authorized paypal transaction
	 * @param response
	 * @return
	 * @throws IOException
	 */
	public PayPalAuthorizeResponse populateAuthorizePayPalTransactionResponse(
			XMLDocument response)  throws IOException{

		PayPalAuthorizeResponse payPalAuthorizeResponse = new PayPalAuthorizeResponse();

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ACK))) {

			payPalAuthorizeResponse.setAcknowledgement(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ACK));
		}

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.STATUS_RS))) {

			LogWriter
			.logInfoMessage("Response received for Do_Authorization PayPal transaction from datacash service...: ");

			payPalAuthorizeResponse.setStatus(
					response.get(DataCashServiceConstants.STATUS_RS));

		}

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUILD))) {

			payPalAuthorizeResponse
			.setBuild(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUILD));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.DATACASH_REFERENCE_RS))) {

			payPalAuthorizeResponse.setDatacashReferance(response
					.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_VERSION))) {

			payPalAuthorizeResponse
			.setPayPalVersion(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_VERSION));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.REASON_RS))) {

			payPalAuthorizeResponse.setReason(response
					.get(DataCashServiceConstants.REASON_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TIMESTAMP))) {

			payPalAuthorizeResponse
			.setTimestamp(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TIMESTAMP));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_ID_RS))) {

			payPalAuthorizeResponse
			.setTransactionId(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_ID_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS))) {

			payPalAuthorizeResponse.setShippingAddress(new ShippingAddress());
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_CITY_RS))) {

				payPalAuthorizeResponse
				.getShippingAddress()
				.setCity(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_CITY_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_NAME_RS))) {

				payPalAuthorizeResponse
				.getShippingAddress()
				.setName(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_NAME_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE_RS))) {

				payPalAuthorizeResponse
				.getShippingAddress()
				.setCountryCode(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_REGION_RS))) {

				payPalAuthorizeResponse
				.getShippingAddress()
				.setRegion(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_REGION_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1_RS))) {

				payPalAuthorizeResponse
				.getShippingAddress()
				.setStreetAddessOne(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_POSTCODE_RS))) {

				payPalAuthorizeResponse
				.getShippingAddress()
				.setPostcode(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_POSTCODE_RS));
			}
		}
		if (StringUtils.isNotBlank(response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS))) {

			payPalAuthorizeResponse.setMerchantReferance(response
					.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
		}
		if (StringUtils.isNotBlank(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_MSGSUB_ID))) {

			payPalAuthorizeResponse
			.setMessageSubmissionId(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_MSGSUB_ID));
		}
		return payPalAuthorizeResponse;
	}
	/**
	 * This method verifies the response data , constucts query/resend request to datacash
	 * @param payPalAuthorizeResponse
	 */
	public PayPalAuthorizeResponse verifyPayPalAuthorizeResponse(PayPalAuthorizeResponse payPalAuthorizeResponse){

		while(++payPalConnectionAttempts<=DispatcherConstants.DEFAULT_FAILURE_COUNT){

			if(payPalAuthorizeResponse.getStatus().equals(PayPalConstants.COMMUNICATION_ERROR)||payPalAuthorizeResponse.getStatus().equals(PayPalConstants.PAYPAL_NO_RESPONSE)){

				try {
					// ideal to query the paypal , instead of fairing the requets again
					authorizePayPalTransactionResponseXMLDocument = getPaypalAgent().request(authorizePayPalTransactionRequestXMLDocument, new Logger());

					payPalAuthorizeResponse = populateAuthorizePayPalTransactionResponse(authorizePayPalTransactionResponseXMLDocument);

				}catch (FailureReport fr) {
					// TODO Auto-generated catch block
					LogWriter.logErrorMessage(fr.getMessage());
				} catch (Exception ex) {
					LogWriter.logErrorMessage(ex.getMessage());
				}
			}
		}
		return payPalAuthorizeResponse;

	}
	/** This method is responsible for Creating do_Authorize PayPal Transaction XML request and contacting Datacash.
	 *
	 * @param authorizePayPalTransaction
	 * @param payPalExpressCheckoutResponse
	 * @return
	 */
	public PayPalAuthorizeResponse doAuthorizePayPalTransaction(
			AuthorizePayPalTransaction authorizePayPalTransaction, String Ref) {
		String currentDate = null;
		/**
		 * doAuthorizePayPalTransaction log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("payPal.log.folderPath", "./logs/payPal/");
		/**
		 * doAuthorizePayPalTransaction log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("doAuthorizePayPalTransaction.log.switch", "OFF");
		LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();

		try {

			LogWriter
			.logInfoMessage("Sending Do_Authorization for PayPal transaction to datacash service...: ");

			authorizePayPalTransactionRequestXMLDocument = populateAuthorizePayPalTransactionRequestXML(
					authorizePayPalTransaction, Ref);

			LogWriter.logDebugMessage("DO_AUTHORIZE XML REQUEST For cpsToken:"+getCpsToken()
					+ authorizePayPalTransactionRequestXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(authorizePayPalTransactionRequestXMLDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "doAuthorizePayPalTransaction",logFolderPath);
			}

			authorizePayPalTransactionResponseXMLDocument = getPaypalAgent().request(authorizePayPalTransactionRequestXMLDocument, new Logger());
			LogWriter.logInfoMessage(getResponseStatus(authorizePayPalTransactionResponseXMLDocument,DataCashServiceConstantsForPayPalTransaction
					.PAYPAL_METHOD_DOAUTHORIZATION)+" : cps token -> "+getCpsToken());
			LogWriter.logDebugMessage("DO_AUTHORIZE XML RESPONSE for cpsToken:"+getCpsToken()
					+ authorizePayPalTransactionResponseXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(authorizePayPalTransactionResponseXMLDocument.getSanitizedDoc(), currentDate, "doAuthorizePayPalTransaction",logFolderPath);
			}

			payPalAuthorizeResponse = populateAuthorizePayPalTransactionResponse(authorizePayPalTransactionResponseXMLDocument);

			//	return verifyPayPalAuthorizeResponse(payPalAuthorizeResponse);
			return payPalAuthorizeResponse;

		} catch (FailureReport fr) {
			// TODO Auto-generated catch block
			LogWriter.logErrorMessage(fr.getMessage());
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return payPalAuthorizeResponse;
	}

	/**
	 * This method is responsible for populating data in an xml request.
	 *
	 * @param authorizePayPalTransaction
	 * @param payPalExpressCheckoutResponse
	 * @return
	 * @throws IOException
	 */
	public XMLDocument populateDoVoidPayPalTransactionRequestXML(
			PayPalCustomer payPalCustomer) throws IOException {

		XMLDocument xmlDocument = null;

		try {

			xmlDocument = new XMLDocument();

			setAuthentication(xmlDocument);

			if (StringUtils.isNotBlank(payPalCustomer
					.getTransactionMethod())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD,
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_DOVOID);
			}
			if (StringUtils.isNotBlank(payPalCustomer
					.getDatacashReference())) {
				xmlDocument
				.set(DataCashServiceConstantsForPayPalTransaction.PAYPAL_DATACASH_REFERENCE,
						payPalCustomer.getDatacashReference());
			}

		} catch (JDOMException ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return xmlDocument;
	}
	/**
	 * This method is responsible populating the datacash response for succesfully authorized paypal transaction
	 * @param response
	 * @return
	 * @throws IOException
	 */
	public PayPalFundReleaseResponse populateDoVoidPayPalTransactionResponse(
			XMLDocument response)  throws IOException{

		PayPalFundReleaseResponse payPalFundReleaseResponse = new PayPalFundReleaseResponse();

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ACK))) {

			payPalFundReleaseResponse.setAcknowledgement(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_ACK));
		}

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.STATUS_RS))) {

			LogWriter
			.logInfoMessage("Response received for Do_Void PayPal transaction from datacash service...: ");

			payPalFundReleaseResponse.setStatus(
					response.get(DataCashServiceConstants.STATUS_RS));
		}

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUILD))) {

			payPalFundReleaseResponse
			.setBuild(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_BUILD));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.DATACASH_REFERENCE_RS))) {

			payPalFundReleaseResponse.setDatacashReferance(response
					.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_VERSION))) {

			payPalFundReleaseResponse
			.setPayPalVersion(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_VERSION));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.REASON_RS))) {

			payPalFundReleaseResponse.setReason(response
					.get(DataCashServiceConstants.REASON_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TIMESTAMP))) {

			payPalFundReleaseResponse
			.setTimestamp(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TIMESTAMP));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_ID_RS))) {

			payPalFundReleaseResponse
			.setTransactionId(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_ID_RS));
		}
		if (response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS) != null
				&& !StringUtils.isEmpty(response
						.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS))) {

			payPalFundReleaseResponse.setShippingAddress(new ShippingAddress());

			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_CITY_RS))) {

				payPalFundReleaseResponse
				.getShippingAddress()
				.setCity(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_CITY_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_NAME_RS))) {

				payPalFundReleaseResponse
				.getShippingAddress()
				.setName(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_NAME_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE_RS))) {

				payPalFundReleaseResponse
				.getShippingAddress()
				.setCountryCode(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_REGION_RS))) {

				payPalFundReleaseResponse
				.getShippingAddress()
				.setRegion(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_REGION_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1_RS))) {

				payPalFundReleaseResponse
				.getShippingAddress()
				.setStreetAddessOne(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1_RS));
			}
			if (StringUtils.isNotBlank(response
					.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_POSTCODE_RS))) {

				payPalFundReleaseResponse
				.getShippingAddress()
				.setPostcode(response.get(
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_SHIPPING_ADDRESS_POSTCODE_RS));
			}

		}
		return payPalFundReleaseResponse;
	}
	/**
	 * This method verifies the response data , constucts query/resend request to datacash
	 * @param payPalAuthorizeResponse
	 */
	public PayPalFundReleaseResponse verifyDoVoidPayPalTransactionResponse(PayPalFundReleaseResponse payPalFundReleaseResponse){

		while(++payPalConnectionAttempts<=DispatcherConstants.DEFAULT_FAILURE_COUNT){

			if(payPalFundReleaseResponse.getStatus().equals(PayPalConstants.PAYPAL_NO_RESPONSE)||payPalFundReleaseResponse.getStatus().equals(PayPalConstants.COMMUNICATION_ERROR)){

				try {
					// ideal to query the paypal , instead of firering the requets again
					doVoidPayPalTransactionResponseXMLDocument = getPaypalAgent().request(doVoidPayPalTransactionRequestXMLDocument, new Logger());
					LogWriter.logInfoMessage(getResponseStatus(doVoidPayPalTransactionResponseXMLDocument,DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_DOVOID)+" : cps token -> "+getCpsToken());

					payPalFundReleaseResponse = populateDoVoidPayPalTransactionResponse(doVoidPayPalTransactionResponseXMLDocument);
				}catch (FailureReport fr) {
					// TODO Auto-generated catch block
					LogWriter.logErrorMessage(fr.getMessage());
				} catch (Exception ex) {
					LogWriter.logErrorMessage(ex.getMessage());
				}
			}
		}
		return payPalFundReleaseResponse;

	}
	/**
	 * This method is responsible for Creating do_Authorize PayPal Transaction
	 * XML request.
	 *
	 * @param authorizePayPalTransaction
	 * @param payPalExpressCheckoutResponse
	 * @return
	 */
	public PayPalFundReleaseResponse cancelPayPalTransaction(
			PayPalCustomer payPalCustomer) {
		String currentDate = null;
		/**
		 * cancelPayPalTransaction log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("payPal.log.folderPath", "./logs/payPal/");
		/**
		 * cancelPayPalTransaction log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("cancelPayPalTransaction.log.switch", "OFF");

		LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();




		try {

			LogWriter
			.logInfoMessage("Sending Do_Void for PayPal transaction to datacash service...: ");

			doVoidPayPalTransactionRequestXMLDocument = populateDoVoidPayPalTransactionRequestXML(payPalCustomer);

			LogWriter.logDebugMessage("Do_Void XML REQUEST for cpsToken :"+getCpsToken()
					+ doVoidPayPalTransactionRequestXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(doVoidPayPalTransactionRequestXMLDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "cancelPayPalTransaction",logFolderPath);
			}

			doVoidPayPalTransactionResponseXMLDocument = getPaypalAgent().request(doVoidPayPalTransactionRequestXMLDocument, new Logger());

			LogWriter.logInfoMessage(getResponseStatus(doVoidPayPalTransactionResponseXMLDocument,DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_DOVOID)+" : cps token = "+getCpsToken());
			LogWriter.logDebugMessage("Do_Void XML RESPONSE for cpsToken:"+getCpsToken()
					+ doVoidPayPalTransactionResponseXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(doVoidPayPalTransactionResponseXMLDocument.getSanitizedDoc(), currentDate, "cancelPayPalTransaction",logFolderPath);
			}
			payPalFundReleaseResponse = populateDoVoidPayPalTransactionResponse(doVoidPayPalTransactionResponseXMLDocument);

			return verifyDoVoidPayPalTransactionResponse(payPalFundReleaseResponse);

		} catch (FailureReport fr) {
			// TODO Auto-generated catch block
			LogWriter.logErrorMessage(fr.getMessage());
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return payPalFundReleaseResponse;
	}
	/**
	 * This method is responsible populating the datacash response for succesfully authorized paypal transaction
	 * @param response
	 * @return
	 * @throws IOException
	 */
	public PayPalQueryTransactionResponse populateDoQueryPayPalTransactionResponse(
			XMLDocument response) throws IOException{

		PayPalQueryTransactionResponse payPalQueryTransactionResponse = new PayPalQueryTransactionResponse();
		PayPalTransaction payPalTransaction = new PayPalTransaction() ;

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.STATUS_RS))) {
			LogWriter
			.logInfoMessage("Response received for Do_Query PayPal transaction from datacash service...: ");

			payPalQueryTransactionResponse.setStatus(response
					.get(DataCashServiceConstants.STATUS_RS));
		}

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.REASON_RS))) {

			payPalQueryTransactionResponse.setReason(response.get(DataCashServiceConstants.REASON_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.STATUS_RS))) {
			LogWriter
			.logInfoMessage("Response received for Do_Query PayPal transaction from datacash service...: ");

			payPalQueryTransactionResponse.setStatus(response
					.get(DataCashServiceConstants.STATUS_RS));
		}

		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.REASON_RS))) {

			payPalQueryTransactionResponse.setReason(response.get(DataCashServiceConstants.REASON_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.RESPONSE_INFORMATION))) {

			payPalQueryTransactionResponse.setInformation(response.get(DataCashServiceConstants.RESPONSE_INFORMATION));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstants.DATACASH_REFERENCE_RS))) {

			payPalQueryTransactionResponse.setDatacashReferance(response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_DATE))) {
			payPalQueryTransactionResponse.setPayPalTransaction(payPalTransaction);
			payPalQueryTransactionResponse.getPayPalTransaction().setTransactionDate(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_DATE));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_STATUS))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setStatus(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_STATUS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_REASON))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setReason(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_REASON));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_MERCHANT_REFERENCE))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setMerchantReference(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_MERCHANT_REFERENCE));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_DATACASH_REFERENCE))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setDatacashReference(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_DATACASH_REFERENCE));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_QUERY_METHOD))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setQueryMethod(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_QUERY_METHOD));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_PAYMENTSTATUS))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setPaymentStatus(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_PAYMENTSTATUS));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_PENDINGREASON))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setPendingReason(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_PENDINGREASON));
		}
		if (StringUtils.isNotBlank(response
				.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_AMOUNT))) {
			payPalQueryTransactionResponse.getPayPalTransaction().setAmount(response.get(DataCashServiceConstantsForPayPalTransaction.PAYPAL_TRANSACTION_AMOUNT));
		}
		return payPalQueryTransactionResponse;
	}
	/**
	 * This method is responsible for populating data in an xml request.
	 *
	 * @param authorizePayPalTransaction
	 * @param payPalExpressCheckoutResponse
	 * @return
	 * @throws IOException
	 */
	public XMLDocument populateDoQueryPayPalTransactionRequestXML(
			PayPalCustomer payPalCustomer) throws IOException {

		XMLDocument xmlDocument = null;

		try {

			xmlDocument = new XMLDocument();

			setAuthentication(xmlDocument);

			if (StringUtils.isNotBlank(payPalCustomer.getTransactionMethod())) {

				xmlDocument
				.set(DataCashServiceConstants.TRANSACT_TYPE,
						DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_QUERY);
			}
			if (StringUtils.isNotBlank(payPalCustomer.getDatacashReference())) {

				xmlDocument
				.set(DataCashServiceConstants.DATACASH_REFERENCE,
						payPalCustomer.getDatacashReference());
			}

		} catch (JDOMException ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return xmlDocument;
	}
	/**
	 * This method verifies the response data , constucts query/resend request to datacash
	 * @param payPalAuthorizeResponse
	 */
	public PayPalQueryTransactionResponse verifyDoQueryPayPalTransactionResponse(PayPalQueryTransactionResponse payPalQueryTransactionResponse){

		while(++payPalConnectionAttempts<=DispatcherConstants.DEFAULT_FAILURE_COUNT){

			if(StringUtils.isNotBlank(payPalQueryTransactionResponse.getStatus())||StringUtils.isNotBlank(payPalQueryTransactionResponse.getPayPalTransaction().getStatus())){

				if(payPalQueryTransactionResponse.getPayPalTransaction().getStatus().equals(PayPalConstants.COMMUNICATION_ERROR)||payPalQueryTransactionResponse.getPayPalTransaction().getStatus().equals(PayPalConstants.PAYPAL_NO_RESPONSE)){

					try {
						// ideal to query the paypal , instead of firering the requets again
						doQueryPayPalTransactionResponseXMLDocument = getPaypalAgent().request(doQueryPayPalTransactionRequestXMLDocument, new Logger());
                        LogWriter.logInfoMessage(getResponseStatus(doQueryPayPalTransactionResponseXMLDocument, "query")+" : cps token -> "+getCpsToken());
						payPalQueryTransactionResponse = populateDoQueryPayPalTransactionResponse(doQueryPayPalTransactionResponseXMLDocument);
					}catch (FailureReport fr) {
						// TODO Auto-generated catch block
						LogWriter.logErrorMessage(fr.getMessage());
					} catch (Exception ex) {
						LogWriter.logErrorMessage(ex.getMessage());
					}
				}
			}
		}
		return payPalQueryTransactionResponse;

	}
	/**
	 * This method is responsible for querying PayPal Transaction
	 * XML request.
	 *
	 * @param authorizePayPalTransaction
	 * @param payPalExpressCheckoutResponse
	 * @return
	 */
	public PayPalQueryTransactionResponse queryPayPalTransaction(PayPalCustomer payPalCustomer){
		String currentDate = null;
		/**
		 * queryPayPalTransaction log file path
		 */
		String logFolderPath = ConfReader.getConfEntry("payPal.log.folderPath", "./logs/directDebit/");
		/**
		 * queryPayPalTransaction log enabled/disabled
		 */
		String loggerSwitch = ConfReader.getConfEntry("queryPayPalTransaction.log.switch", "OFF");

		LoggerServiceFrameWork loggerServiceFrameWork = getLoggerService();



		try {

			LogWriter
			.logInfoMessage("Sending Query request for PayPal transaction to datacash service...: ");

			doQueryPayPalTransactionRequestXMLDocument = populateDoQueryPayPalTransactionRequestXML(payPalCustomer);

			LogWriter.logDebugMessage("Query XML REQUEST for cpsToken:"+getCpsToken()
					+ doQueryPayPalTransactionRequestXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				currentDate = loggerServiceFrameWork.logRequest(doQueryPayPalTransactionRequestXMLDocument.getSanitizedDoc(), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "queryPayPalTransaction",logFolderPath);
			}

			doQueryPayPalTransactionResponseXMLDocument = getPaypalAgent().request(doQueryPayPalTransactionRequestXMLDocument, new Logger());
			LogWriter.logInfoMessage(getResponseStatus(doQueryPayPalTransactionResponseXMLDocument,DataCashServiceConstantsForPayPalTransaction.PAYPAL_METHOD_QUERY)+" : cps token -> "+getCpsToken());
			LogWriter.logDebugMessage("Query XML RESPONSE for cpsToken:"+getCpsToken()
					+ doQueryPayPalTransactionResponseXMLDocument.getSanitizedDoc());
			if(loggerSwitch.equalsIgnoreCase("ON")){
				loggerServiceFrameWork.logResponse(doQueryPayPalTransactionResponseXMLDocument.getSanitizedDoc(), currentDate, "queryPayPalTransaction",logFolderPath);
			}

			payPalQueryTransactionResponse = populateDoQueryPayPalTransactionResponse(doQueryPayPalTransactionResponseXMLDocument);

			return verifyDoQueryPayPalTransactionResponse(payPalQueryTransactionResponse);

		} catch (FailureReport fr) {
			// TODO Auto-generated catch block
			LogWriter.logErrorMessage(fr.getMessage());
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());
		}
		return payPalQueryTransactionResponse;
	}


	private boolean isSuccess(String status){

		if(StringUtils.isBlank(status))
			return false;
		else{
			return StringUtils.equals(PayPalConstants.DATACASH_SUCCESS,status);
		}


	}

	public UUID getCpsToken() {
		return cpsToken;
	}

	public void setCpsToken(UUID cpsToken) {
		this.cpsToken = cpsToken;
	}
	private LoggerServiceFrameWork getLoggerService(){
		LoggerServiceFrameWork loggerServiceFrameWork = new LoggerServiceFrameWorkImpl();
		return loggerServiceFrameWork;

	}


	private String getResponseStatus(XMLDocument response , String method){
		String resMsg=PayPalConstants.PAYPAL+PayPalConstants.COLON+method+PayPalConstants.COLON;
		String errorMsg=PayPalConstants.PAYPAL_EMPTY;
		resMsg+=PayPalConstants.STATUS_CODE+response.get(PayPalConstants.STATUS_RS)+PayPalConstants.COLON;
		resMsg+=PayPalConstants.PAYPAL_REASON_AR+response.get(PayPalConstants.REASON)+PayPalConstants.COLON;
		resMsg+=PayPalConstants.DC_REFERENCE+response.get(PayPalConstants.RESPONSE_DATACASH_REFERENCE)+PayPalConstants.COLON;
	   resMsg+=PayPalConstants.MERCHANT_REFERENCE+response.get(PayPalConstants.RESPONSE_MERCHANT_REFERENCE);

		if(StringUtils.isNotEmpty(response.get(PayPalConstants.RESPONSE_PAYPAL_ERRORS))){
		errorMsg+=PayPalConstants.PAYPAL_ERROR_AR+response.get(PayPalConstants.RESPONSE_PAYPAL_ERROR_CODE)+PayPalConstants.COLON;
		errorMsg+=PayPalConstants.PAYPAL_LONGMESSAGE+response.get(PayPalConstants.RESPONSE_PAYPAL_ERROR_LONGMESSAGE)+PayPalConstants.COLON;
		errorMsg+=PayPalConstants.PAYPAL_SHORTMESSAGE+response.get(PayPalConstants.RESPONSE_PAYPAL_ERROR_SHORTMESSAGE);
		}
		if(!StringUtils.isEmpty(errorMsg)){
			resMsg+=PayPalConstants.COLON+errorMsg;
		}
		return resMsg;
	}

	public Map<String,String> revokeDDSetup(String datacashVTid,
            Map<String, String> revokeDDSetupDetails)throws DataCashServiceException {
       Map<String,String> responseMap=new HashMap<String, String>();
       String merchentreference = revokeDDSetupDetails.get("merchantreference");
       String method = revokeDDSetupDetails.get("method");
       String datacashreference = revokeDDSetupDetails.get("datacashreference");


       try {
        XMLDocument xmlDocument = new XMLDocument();
        setAuthentication(xmlDocument);
        xmlDocument.set(MERCHANT_REFERENCE, merchentreference);
        xmlDocument.set(TRANSACT_TYPE, method);
        xmlDocument.set(DATACASH_REFERENCE, datacashreference);
        LogWriter.logInfoMessage("Parameters for revokeDDSetup : \t merchentreference="+merchentreference+",method="+method+",datacashreference="+datacashreference);
        LogWriter.logDebugMessage("Before sending Request to datacash:\n"+xmlDocument.getSanitizedDoc());

              response = getAgent().request(xmlDocument, new Logger());


       // XMLDocument   response = getAgent().request(xmlDocument, new Logger());

        LogWriter.logDebugMessage("Before sending Request to datacash:\n"+response.getSanitizedDoc());
        responseMap.put("method", response.get(DataCashServiceConstants.DD_METHOD_RS));
        responseMap.put("merchantReferenceNumber", response.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
        responseMap.put("reason", response.get(DataCashServiceConstants.REASON_RS));
        responseMap.put("status", response.get(DataCashServiceConstants.STATUS_RS));
        if(!response.get(DataCashServiceConstants.STATUS_RS).equalsIgnoreCase("1")){
            responseMap.put("datacashReferenceNumber",response.get(DataCashServiceConstants.DATACASH_REFERENCE_RS) );
          //  return responseMap;
        }


    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (JDOMException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
       catch (FailureReport fr) {
           // TODO Auto-generated catch block
           LogWriter.logErrorMessage(fr.getMessage());
          // handleFailureReport(fr);


    }
       return responseMap;
	}
	
	
	
	/**
	 * Method makes Authenticate Payer XML-RPC call to DataCash & returns the
	 * response XML.
	 * 
	 * @param bookingComponent
	 * @param token
	 * @param url
	 * @param browserDetails
	 * @return AunthicatePayeeResXml
	 * @throws DataCashServiceException
	 */
	public AunthicatePayeeResXml doEmvAuthentication(
			BookingComponent bookingComponent, String token, String url,
			Map<String, String> browserDetails) throws DataCashServiceException {

		String prefix = PaymentConstants.PAYMENT + 0;
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
				UUID.fromString(token));
		paymentData.getBookingInfo().setNewSetupReq(true);
		Map<String, String> hccCapturedData = bookingComponent
				.getHccCapturedData();
		ContactInfo contactInfo = bookingComponent.getContactInfo();

		Map<String, String> hcsMap = bookingComponent.getHccsMap();
		String hpsDatacashReference = hcsMap
				.get(DataCashServiceConstants.HCC_DATACASH_RS_KEY);
		XMLDocument xmlDocument = null;
		String fname = StringUtils.EMPTY;
		String lname = StringUtils.EMPTY;
		AunthicatePayeeResXml authPayeeRes = null;
		try {
			xmlDocument = new XMLDocument();

			xmlDocument.set(DataCashServiceConstants.CLIENT, vTid);
			xmlDocument.set(DataCashServiceConstants.PASSWORD, password);
			xmlDocument.set(DataCashServiceConstants.TRANSACT_TYPE,
					DataCashServiceConstants.AUTHENTICATE_PAYER);
			xmlDocument.set(DataCashServiceConstants.DATACASH_REFERENCE,
					hpsDatacashReference);
			xmlDocument.set(DataCashServiceConstants.PREFERENCE,
					DataCashServiceConstants.NO_PREFERENCE);
			xmlDocument.set(DataCashServiceConstants.REDIRECT_URL, url);
			xmlDocument.set(DataCashServiceConstants.CUSTOMER_EMAIL,
					contactInfo.getEmailAddress());
			String fullName = hccCapturedData.get(prefix
					+ PaymentConstants.NAME_ON_CARD);
			String[] arrStr = StringUtils.split(fullName, "\\+");
			fname = arrStr[0];
			if (arrStr.length == 2) {
				lname = arrStr[1];
			}

			xmlDocument
					.set(DataCashServiceConstants.CUSTOMER_FIRST_NAME, fname);
			xmlDocument.set(DataCashServiceConstants.CUSTOMER_LAST_NAME, lname);
			xmlDocument.set(DataCashServiceConstants.CUSTOMER_MOBILE_PHONE,
					StringUtils.defaultString(contactInfo.getPhoneNumber()));
			xmlDocument.set(DataCashServiceConstants.CUSTOMER_PHONE,
					StringUtils.defaultString(contactInfo.getAltPhoneNumber()));

			xmlDocument.set(DataCashServiceConstants.BILLINGADDRESS_COMPANY,
					StringUtils.EMPTY);
			xmlDocument
					.set(DataCashServiceConstants.BILLINGADDRESS_STREET,
							StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_STREET_ADDRESS1)));

			xmlDocument
					.set(DataCashServiceConstants.BILLINGADDRESS_STREET2,
							StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_STREET_ADDRESS2)));
			xmlDocument
					.set(DataCashServiceConstants.BILLINGADDRESS_CITY,
							StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_STREET_ADDRESS3)));
			xmlDocument.set(
					DataCashServiceConstants.BILLINGADDRESS_STATE_PROVINCE,
					StringUtils.EMPTY);
			xmlDocument
					.set(DataCashServiceConstants.BILLINGADDRESS_STATE_PROVINCE_CODE,
							StringUtils.EMPTY);
			xmlDocument
					.set(DataCashServiceConstants.BILLINGADDRESS_COUNTRY,
							StringUtils.upperCase(StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_ISSUERCOUNTRY))));
			xmlDocument.set(
					DataCashServiceConstants.BILLINGADDRESS_POSTCODE_ZIP,
					StringUtils.defaultString(hccCapturedData
							.get(DataCashServiceConstants.PAYMENT_0_POSTCODE)));

			xmlDocument.set(DataCashServiceConstants.ADDRESS_COMPANY,
					StringUtils.EMPTY);
			xmlDocument
					.set(DataCashServiceConstants.ADDRESS_STREET,
							StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_STREET_ADDRESS1)));
			xmlDocument
					.set(DataCashServiceConstants.ADDRESS_STREET2,
							StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_STREET_ADDRESS2)));
			xmlDocument
					.set(DataCashServiceConstants.ADDRESS_CITY,
							StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_STREET_ADDRESS3)));
			xmlDocument.set(DataCashServiceConstants.ADDRESS_STATE_PROVINCE,
					StringUtils.EMPTY);
			xmlDocument.set(
					DataCashServiceConstants.ADDRESS_STATE_PROVINCE_CODE,
					StringUtils.EMPTY);
			xmlDocument
					.set(DataCashServiceConstants.ADDRESS_COUNTRY,
							StringUtils.upperCase(StringUtils.defaultString(hccCapturedData
									.get(DataCashServiceConstants.PAYMENT_0_ISSUERCOUNTRY))));
			xmlDocument.set(DataCashServiceConstants.ADDRESS_POSTCODE_ZIP,
					StringUtils.defaultString(hccCapturedData
							.get(DataCashServiceConstants.PAYMENT_0_POSTCODE)));
			xmlDocument.set(DataCashServiceConstants.ADDRESS_SOURCE,
					StringUtils.EMPTY);
			xmlDocument.set(DataCashServiceConstants.ADDRESS_SAME_AS_BILLING,
					DataCashServiceConstants.SAME);
			xmlDocument.set(DataCashServiceConstants.CONTACT_EMAIL,
					contactInfo.getEmailAddress());
			xmlDocument.set(DataCashServiceConstants.CONTACT_FIRST_NAME, fname);
			xmlDocument.set(DataCashServiceConstants.CONTACT_LAST_NAME, lname);
			xmlDocument.set(DataCashServiceConstants.CONTACT_MOBILE_PHONE,
					contactInfo.getPhoneNumber());
			xmlDocument.set(DataCashServiceConstants.CONTACT_PHONE,
					contactInfo.getAltPhoneNumber());
			xmlDocument.set(DataCashServiceConstants.CONTACT_SAME_AS_BILLING,
					DataCashServiceConstants.SAME);

			xmlDocument.set(DataCashServiceConstants.SHIPPING_METHOD,
					StringUtils.EMPTY);
			xmlDocument.set(DataCashServiceConstants.ORIGIN_POSTCODE_ZIP,
					contactInfo.getPostCode());

			xmlDocument
					.set(DataCashServiceConstants.CHALLENGE_WINDOW_SIZE,
							browserDetails
									.get(DataCashServiceConstants.BROWSER_CHALLENGE_WINDOW_SIZE));
			xmlDocument
					.set(DataCashServiceConstants.ACCEPT_HEADER,
							browserDetails
									.get(DataCashServiceConstants.BROWSER_ACCEPT_HEADER));
			xmlDocument
					.set(DataCashServiceConstants.COLOUR_DEPTH, browserDetails
							.get(DataCashServiceConstants.BROWSER_COLOUR_DEPTH));
			xmlDocument
					.set(DataCashServiceConstants.JAVA_ENABLED, browserDetails
							.get(DataCashServiceConstants.BROWSER_JAVA_ENABLED));
			xmlDocument.set(DataCashServiceConstants.BROWSERDETAILS_LANGUAGE,
					browserDetails
							.get(DataCashServiceConstants.BROWSER_LANGUAGE));
			xmlDocument
					.set(DataCashServiceConstants.SCREEN_HEIGHT,
							browserDetails
									.get(DataCashServiceConstants.BROWSER_SCREEN_HEIGHT));
			xmlDocument
					.set(DataCashServiceConstants.SCREEN_WIDTH, browserDetails
							.get(DataCashServiceConstants.BROWSER_SCREEN_WIDTH));
			xmlDocument.set(DataCashServiceConstants.BROWSERDETAILS_TIME_ZONE,
					browserDetails
							.get(DataCashServiceConstants.BROWSER_TIME_ZONE));

			xmlDocument.set(DataCashServiceConstants.DEVICE_BROWSER,
					browserDetails.get(DataCashServiceConstants.BROWSER));
			xmlDocument.set(DataCashServiceConstants.DEVICE_IP_ADDRESS,
					browserDetails.get(DataCashServiceConstants.REMOTEADDRESS));
			LogWriter
					.logInfoMessage("Sending Authenticate request to datacash with"
							+ hpsDatacashReference
							+ " , metheod=authenticate_payer"
							+ " , redirect url=" + url + ",token=" + token);

			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			XMLDocument xmlResponse = getAgent().request(xmlDocument,
					new Logger());
			LogWriter.logDebugMessage(xmlResponse.getSanitizedDoc());
			LogWriter.logInfoMessage(" Authenticate Response :"
					+" datacash_reference="
					+ xmlResponse.get("Response.datacash_reference")
					+ " , merchantreference="
					+ xmlResponse.get("Response.merchantreference")
					+ " , status=" + xmlResponse.get("Response.status")
					+ ", reason=" + xmlResponse.get("Response.reason")
					+ ", information="
					+ xmlResponse.get("Response.information") + " , token="
					+ token);

			authPayeeRes = authPayeeResMapping(xmlResponse, bookingComponent);
		} catch (FailureReport failureReport) {
			LogWriter
					.logErrorMessage("Error Occured At EMV 3DS Authentication :"
							+ failureReport.getMessage() + " , token=" + token);
			LogWriter
					.logErrorMessage(failureReport.getMessage(), failureReport);
			throw new DataCashServiceException(failureReport.getMessage());

		} catch (IOException ex) {
			LogWriter
					.logErrorMessage("Error Occured At xmlDocument object creation :"
							+ ex.getMessage() + " , token=" + token);
			LogWriter.logErrorMessage(ex.getMessage(), ex);
			throw new DataCashServiceException(ex.getMessage(), ex);
		} catch (JDOMException jdome) {
			LogWriter
					.logErrorMessage("Error Occured At xmlDocument object creation :"
							+ jdome.getMessage() + " , token=" + token);
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new DataCashServiceException(jdome.getMessage(), jdome);
		}
		return authPayeeRes;

	}
	  
	  
	private AunthicatePayeeResXml authPayeeResMapping(
			XMLDocument authPayeeResp, BookingComponent bookingComponent) {

		AunthicatePayeeResXml aunthicatePayeeResXml = new AunthicatePayeeResXml(
				authPayeeResp);

		aunthicatePayeeResXml.setAcs_transaction_id(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_ACS_TRANSACTION_ID));
		aunthicatePayeeResXml.setDs_transaction_id(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_DS_TRANSACTION_ID));
		aunthicatePayeeResXml.setTransaction_id(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_TRANSACTION_ID));
		aunthicatePayeeResXml.setAuthentication_status(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_AUTHENTICATION_STATUS));
		aunthicatePayeeResXml.setAuthentication_version(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_AUTHENTICATION_VERSION));
		aunthicatePayeeResXml.setTransaction_status(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_TRANSACTION_STATUS));
		aunthicatePayeeResXml.setRedirect_html(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_REDIRECT_HTML));
		aunthicatePayeeResXml.setProtocol_version(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_PROTOCOL_VERSION));

		aunthicatePayeeResXml.setDatacash_reference(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_DATACASH_REFERENCE));
		aunthicatePayeeResXml.setMerchantreference(authPayeeResp
				.get(DataCashServiceConstants.AP_RES_MERCHANTREFERENCE));
		// aunthicatePayeeResXml.setMode(authPayeeResp.get(DataCashServiceConstants.AP_RES_MODE));
		aunthicatePayeeResXml.setReason(authPayeeResp.get(DataCashServiceConstants.AP_RES_REASON));
		aunthicatePayeeResXml.setStatus(authPayeeResp.get(DataCashServiceConstants.AP_RES_STATUS));
		// aunthicatePayeeResXml.setTime(authPayeeResp.get(DataCashServiceConstants.AP_RES_TIME));

		return aunthicatePayeeResXml;

	}

	/**
	 * Method handles Authorization / Pay request and response.
	 * 
	 * @param vTid
	 * @param password
	 * @param currency
	 * @param amount
	 * @param catpureMethod
	 * @param hpsDatacashReference
	 * @param merchentRef
	 * @param token
	 * @return EMVAuthorizationResponse
	 * @throws DataCashServiceException
	 */
	public EMVAuthorizationResponse doEMV3DSAuthorisation(String vTid,
			String password, String currency, String amount,
			String catpureMethod, String hpsDatacashReference,
			String merchentRef, String token) throws DataCashServiceException {

		XMLDocument response = null;
		try {

			XMLDocument xmlDocument = new XMLDocument();
			String logMessage = "EMV 3DS Authorization request details reference="
					+ hpsDatacashReference
					+ " method=pre"
					+ "  merchantrefence="
					+ merchentRef
					+ " and amount="
					+ amount + ", token=" + token;

			setAuthentication(xmlDocument);
			Hashtable<String, String> authAttru = new Hashtable<String, String>();
			authAttru.put(
					DataCashServiceConstants.EMV_3DS_AZ_REQ_CARD_DETAILS_TYPE,
					DataCashServiceConstants.EMV_3DS_AZ_REQ_CARD_DETAILS_VALUE);
			Hashtable<String, String> currencyAtrribute = new Hashtable<String, String>();
			currencyAtrribute.put(
					DataCashServiceConstants.EMV_3DS_AZ_REQ_CURRENCY, currency);

			xmlDocument.set(
					DataCashServiceConstants.EMV_3DS_AZ_REQ_MERCHANTREFERENCE,
					merchentRef);
			xmlDocument.set(DataCashServiceConstants.EMV_3DS_AZ_REQ_AMOUNT,
					amount, currencyAtrribute);
			xmlDocument.set(
					DataCashServiceConstants.EMV_3DS_AZ_REQ_CAPTUREMETHOD,
					catpureMethod);
			xmlDocument.set(DataCashServiceConstants.EMV_3DS_AZ_REQ_METHOD,
					DataCashServiceConstants.EMV_3DS_AZ_REQ_METHOD_PRE);
			xmlDocument.set(
					DataCashServiceConstants.EMV_3DS_AZ_REQ_CARD_DETAILS,
					hpsDatacashReference, authAttru);

			LogWriter.logInfoMessage(logMessage);
			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			response = getAgent().request(xmlDocument, new Logger());
			LogWriter.logDebugMessage(response.getSanitizedDoc());
			LogWriter.logInfoMessage(" EMV 3DS Authorization datacash response :"
					+" datacash_reference="
					+ response.get("Response.datacash_reference")
					+ " , merchantreference="
					+ response.get("Response.merchantreference") + " , status="
					+ response.get("Response.status") + ", reason="
					+ response.get("Response.reason") + ", information="
					+ response.get("Response.information") + " , card_scheme="
					+ response.get("Response.CardTxn.card_scheme"));
		} catch (IOException ioe) {
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new DataCashServiceException(ioe.getMessage(), ioe);
		} catch (JDOMException jdome) {
			LogWriter.logErrorMessage(jdome.getMessage(), jdome);
			throw new DataCashServiceException(jdome.getMessage(), jdome);
		} catch (FailureReport failureReport) {
			LogWriter
					.logErrorMessage(failureReport.getMessage(), failureReport);
			throw new DataCashServiceException(failureReport.getMessage());
		}
		return new EMVAuthorizationResponse(response);

	}

	/**
	 * Method makes XML-RPC call to Retrieve Authentication Status & returns XML
	 * response.
	 * 
	 * @param vTid
	 * @param datacashReference
	 * @param token
	 * @return RetriveThreedAuthResponse
	 * @throws DataCashServiceException
	 */
	public RetriveThreedAuthResponse getEMVRetrieveAuthResponse(String vTid,
			String datacashReference, String token)
			throws DataCashServiceException {
		XMLDocument retriveResponse = null;

		try {
			XMLDocument xmlDocument = new XMLDocument();
			setAuthentication(xmlDocument);

			xmlDocument
					.set(TRANSACT_TYPE,
							DataCashServiceConstants.EMV_3DS_RA_REQ_AUTHENTICATION_STATUS);
			xmlDocument.set(DATACASH_REFERENCE, datacashReference);

			LogWriter.logDebugMessage(xmlDocument.getSanitizedDoc());
			LogWriter
					.logInfoMessage("Retriving EMV 3DS request with datacash reference="
							+ datacashReference
							+ " , trans_type=retrieve_emv3ds_authentication_status"
							+ " , token=" + token);

			retriveResponse = getAgent().request(xmlDocument, new Logger());
			LogWriter.logDebugMessage(retriveResponse.getSanitizedDoc());
			LogWriter
					.logInfoMessage("EMV 3DS Retrive Authentication Status response :"
							+" datacash_reference="
							+ retriveResponse
									.get("Response.datacash_reference")
							+ " , merchantreference="
							+ retriveResponse.get("Response.merchantreference")
							+ " , status="
							+ retriveResponse.get("Response.status")
							+ ", reason="
							+ retriveResponse.get("Response.reason")
							+ ", information="
							+ retriveResponse.get("Response.information")
							+ " , token=" + token);
		} catch (IOException ioe) {
			LogWriter.logErrorMessage(ioe.getMessage(), ioe);
			throw new DataCashServiceException(ioe.getMessage(), ioe);
		} catch (JDOMException jdome) {
			String message = jdome.getMessage();
			LogWriter.logErrorMessage(message, jdome);
			throw new DataCashServiceException(jdome.getMessage(), jdome);
		} catch (FailureReport failureReport) {
			LogWriter
					.logErrorMessage(failureReport.getMessage(), failureReport);
			throw new DataCashServiceException(failureReport.getMessage());
		}
		return new RetriveThreedAuthResponse(retriveResponse);

	}
		

		   
}
