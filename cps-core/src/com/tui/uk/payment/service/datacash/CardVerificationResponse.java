/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CardVerificationResponse.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-03-02 14:21:54 $
 *
 * $Author: bibin.@sonata-software.com $
 *
 * $Log: $
 */
package com.tui.uk.payment.service.datacash;

import com.datacash.util.XMLDocument;

/**
 * This class holds data-cash return values for 3Dsecure transactions.
 *
 * @author bibin.j
 */
public class CardVerificationResponse extends CardResponse
{
   /**
    * The string holding the Card Issuing Bank URL.
    */
   private String acsUrl;

   /**
    * The String holding the payer authentication request.
    */
   private String paReq;

   /**
    * This constructs CardResponse from the <code>XMLDocument</code> object.
    *
    * @param xmlDocument the <code>XMLDocument</code> of the data-cash.
    */
   public CardVerificationResponse(XMLDocument xmlDocument)
   {
      super(xmlDocument);
      acsUrl = xmlDocument.get(DataCashServiceConstants.ACS_URL);
      paReq = xmlDocument.get(DataCashServiceConstants.PAREQ_MESSAGE);
   }

   /**
    * Getting the Card Issuing Bank URL.
    *
    * @return the acsUrl
    */
   public String getAcsUrl()
   {
      return acsUrl;
   }

   /**
    * Getting the payer authentication request.
    *
    * @return the paReq
    */
   public String getPaReq()
   {
      return paReq;
   }
}
