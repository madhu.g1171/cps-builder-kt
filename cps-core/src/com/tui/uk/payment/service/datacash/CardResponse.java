/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CardResponse.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2008-05-07 14:21:54 $
 *
 * $Author: thomas.pm@sonata-software.com $
 *
 * $Log: not supported by cvs2svn $
 */
package com.tui.uk.payment.service.datacash;

import com.datacash.util.XMLDocument;

/**
 * This class holds the details that the datacash returns for the transactions.
 * This is mainly used for authorization transactions.
 *
 * @author thomas.pm
 */
public class CardResponse extends Response
{
   /**
    * The String holding The Card Issuing Bank.
    */
   private String issuer;

   /**
    * The String holding The card scheme.
    */
   private String cardScheme;

   /**
    * The String holding The Country of Issue.
    */
   private String country;

   /**
    * The String holding Authorisation code for successful transactions.
    */
   private String authCode;

   /**
    * This constructs CardResponse from the <code>XMLDocument</code> object.
    *
    * @param xmlDocument the <code>XMLDocument</code> of the datacash.
    */
   public CardResponse(XMLDocument xmlDocument)
   {
      super(xmlDocument);
      issuer = xmlDocument.get(DataCashServiceConstants.ISSUER_RS);
      cardScheme = xmlDocument.get(DataCashServiceConstants.CARD_SCHEME_RS);
      country = xmlDocument.get(DataCashServiceConstants.COUNTRY_RS);
      authCode = xmlDocument.get(DataCashServiceConstants.AUTH_CODE_RS);
   }

   /**
    * Getting the Issuer.
    *
    * @return the issuer
    */
   public String getIssuer()
   {
      return issuer;
   }

   /**
    * Getting the card scheme.
    *
    * @return the cardScheme
    */
   public String getCardScheme()
   {
      return cardScheme;
   }

   /**
    * Getting the country.
    *
    * @return the country
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Getting the authorized code.
    *
    * @return the authCode
    */
   public String getAuthCode()
   {
      return authCode;
   }

}
