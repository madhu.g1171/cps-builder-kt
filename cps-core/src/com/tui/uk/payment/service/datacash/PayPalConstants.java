/**
 *
 */
package com.tui.uk.payment.service.datacash;

/**
 * @author sreenivasulu.k
 *
 */
public class PayPalConstants {



	public static final String COMMUNICATION_ERROR ="569";
	public static final String PAYPAL_NO_RESPONSE ="572";
    public static final String PAYPAL = "PayPal";
    public static final String PAYPAL_REFUND="txn_refund";
    public static final String COLON = " : ";
	public static final String STATUS_CODE ="Status code -> ";
    public static final String PAYPAL_REASON_AR = "reason -> ";
    public static final String PAYPAL_LONGMESSAGE="long_message -> ";
    public static final String PAYPAL_SHORTMESSAGE = "short_message -> ";
    public static final String PAYPAL_EMPTY = "";
    public static final String PAYPAL_ERROR_AR = "error_code -> ";
    public static final String DC_REFERENCE = "DC reference ->";
    public static final String MERCHANT_REFERENCE = "merchant reference -> ";
    

	public static final String FRAUD_STATUS ="10";
	public static final String FRAUD_ACK ="failure";
	public static final String FRAUD_MSG ="Fraudscreening failure";
	public static final String DATACASH_SUCCESS ="1";

	public static final String FRAUD_SCREENING_PRE_AUTH_SUCCESS ="FRAUD_SCREENING_PRE_AUTH_SUCCESS";
	public static final String FRAUD_SCREENING_PRE_AUTH_FAILURE ="FRAUD_SCREENING_PRE_AUTH_FAILURE";
	public static final String FRAUD_SCREENING_FULFILL_SUCCESS ="FRAUD_SCREENING_PRE_AUTH_SUCCESS";
	public static final String FRAUD_SCREENING_FULFILL_FAILURE ="FRAUD_SCREENING_PRE_AUTH_FAILURE";
	
	/** The constant for status. */
	public static final String STATUS_RS = "Response.status";
	
	/** The constant for reason. */
	public static final String REASON = "Response.reason";
	/** The constant for status. */
   public static final String RESPONSE_DATACASH_REFERENCE = "Response.datacash_reference";
   
   /** The constant for reason. */
   public static final String RESPONSE_MERCHANT_REFERENCE = "Response.merchantreference";
	
	/** The constant for errors. */
	public static final String RESPONSE_PAYPAL_ERRORS = "Response.PayPalTxn.Errors";
	
	/** The constant for errorcode. */
	public static final String RESPONSE_PAYPAL_ERROR_CODE = "Response.PayPalTxn.Errors.Error.error_code";
	
	/** The constant for long message. */
	public static final String RESPONSE_PAYPAL_ERROR_LONGMESSAGE = "Response.PayPalTxn.Errors.Error.long_message";
	
	/** The constant for short message. */
	public static final String RESPONSE_PAYPAL_ERROR_SHORTMESSAGE = "Response.PayPalTxn.Errors.Error.short_message";
	
    public static final String PAYPAL_CREDIT = "payPalCredit";

	/**
	 *
	 */
	public PayPalConstants() {
		// TODO Auto-generated constructor stub
			}





}
