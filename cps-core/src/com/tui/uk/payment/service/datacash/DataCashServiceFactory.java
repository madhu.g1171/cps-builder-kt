/*
* Copyright (C)2008 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   DataCashServiceFactory.java$
*
* $Revision:   $
*
* $Date:   Jun 16, 2008$
*
* Author: veena.bn
*
*
* $Log:   $
*/
package com.tui.uk.payment.service.datacash;

import java.util.HashMap;
import java.util.Map;

import com.tui.uk.log.LogWriter;


/**
 * The DataCashServiceFactory returns the same DataCashService instance for the given client.
 *
 * @author veena.bn
 */
public final class DataCashServiceFactory
{
   /**
    * Private constructor to avoid instantiation.
    */
   private DataCashServiceFactory()
   {
   }

   /** This is a cache for keeping the instances of DataCashService of different clients. */
   private static Map<String, DataCashService> dataCashServiceMap =
         new HashMap<String, DataCashService>();

   /**
    * This method searches for the instance in the map with the given client as key
    * and returns it.If instance not present it creates and stores in the cache.
    *
    * @param client the data-cash client account(Same as Vtid).
    * @return the data cash service
    */
   public static DataCashService getDataCashService(String client)
   {
      DataCashService datacashService = dataCashServiceMap.get(client);
      if (datacashService == null)
      {
         LogWriter.logInfoMessage("Allocating new DataCashService for the client"
            + client);
         datacashService = new DataCashService(client);
         dataCashServiceMap.put(client, datacashService);
      }
      return datacashService;
   }

}
