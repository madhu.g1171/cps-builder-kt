/*
* Copyright (C)2008 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
 * $RCSfile: DataCashCard.java,v $
*
* $Revision: 1.5 $
*
* $Date: 2008-05-07 14:23:10 $
*
* $Author: thomas.pm $
*
* $Log: not supported by cvs2svn $
*/
package com.tui.uk.payment.service.datacash;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.domain.PaymentConstants.COMMA;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.ACCEPT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.ADDRESS_POLICY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.AUTH_CODE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CAPTUREMETHOD_CNP;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CAPTURE_METHOD;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CARD_DETAILS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CV2_POLICY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.FALSE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.FEATURE_CAPABILTIES;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.ICC_TERM_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.ICC_TERM_TYPE_VALUE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.IC_READER;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.MAGNETIC_STRIPE_READER;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.MANUAL_CARD_ENTRY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.MATCHED;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.NOT_CHECKED;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.NOT_MATCHED;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.NOT_PROVIDED;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PARTIAL_MATCH;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PIN_PAD_AVAILABLE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.POSTCODE_POLICY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PPT_TXN_METHOD;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PPT_TXN_PAN;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PPT_TXN_PIN;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.REJECT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TERMINAL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TERMINALID_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TERMINAL_CAPABILTIES;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TRACK2DATA;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TRACK2DATA_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TRANSACTION_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.TRUE;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;
import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.datacash.client.CardDetails;
import com.datacash.client.errors.BadRequest;
import com.datacash.errors.FailureReport;
import com.datacash.util.CardInfo;
import com.datacash.util.XMLDocument;
import com.tui.uk.cacheeventlistener.TransactionStatusUtil;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 *  This class allows a payment card to be used within DataCash.
 *  The payment cards (Card) contains the basic card details.
 *  This class adds behavior that allows the card details to be written to XML according
 *  to the DataCash specification.
 *
 * @author thomas.pm
 */
public final class DataCashCard
{

   /** The Switch card. */
   private static final String SWITCH = "SWITCH";

   /** The Maestro card. */
   private static final String MAESTRO = "MAESTRO";

   /** The American  express card. */
   private static final String AMEX = "AMERICAN_EXPRESS";

   /** The constant for holding the date format. */
   private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

   /** The constant for holding the string action required for logging purpose. */
   private static final String ACTION = "binvalidation";

   /** The constant for SUCCESS. */
   private static final String SUCCESS = "Success";

   /** The constant for EXPECTED_CARDTYPE. */
   private static final String EXPECTED_CARDTYPE = "Expected card type";

   /** The constant for CUSTOMER_ENTERED_CARDTYPE. */
   private static final String CUSTOMER_ENTERED_CARDTYPE = "Customer entered card type";

   /** The COMMA. */
   private static final String COMMA = ",";

   /** The COLON. */
   private static final String COLON = ":";

   /** The card type. */
   private String cardType;

   /** The country. */
   private String country;

   /** Card number.A String of 13 to 19 digits. */
   private char[] pan;

   /** The name on card. */
   private String nameOncard;

   /** The expire date. */
   private String expiryDate;

   /** The CVV number of the card. */
   private char[] cv2;

   /** The post code of the card holder. */
   private String postCode;

   /** The start date. */
   private String startDate;

   /** The issue number. */
   private String issueNumber;

   /** The auth Xml document. */
   private XMLDocument xmlDocument;

   /**Map containing address details.*/
   private Map<String, String> address = new HashMap<String, String>();

   /**Map containing cvv policy  details.*/
   private Map<String, String> cvvPolicies = new HashMap<String, String>();

   /**Map containing address policy details.*/
   private Map<String, String> addressPolicies = new HashMap<String, String>();

   /**Map containing post code policy details.*/
   private Map<String, String> postCodePolicies = new HashMap<String, String>();

   /** For CV2AVS enabled flag. */
   private Boolean cv2AVSEnabled = Boolean.FALSE;

   /** The capture method. */
   private String captureMethod;

   /** toekn. */
   private String token;

   /** uuid. */
   private UUID uuid;

   /** The paymentData. */
   private PaymentData paymentData;

   /**
    * Constructor to create DataCashCard object for Cnp Cards.
    *
    * @param pan the card number.
    * @param nameOncard the name on the card.
    * @param expiryDate expiry date of the card.
    * @param cv2 the cv2 of the card.
    * @param postCode the post code of the card.
    * @param cardType the card type.
    * @param issueNumber the issue number.
    * @param address the address
    * @param cvvPolicies the cvvPolicies.
    * @param addressPolicies the addressPolicies.
    * @param postCodePolicies the postCodePolicies.
    */
   //CHECKSTYLE:OFF
   public DataCashCard(char[] pan, String nameOncard, String expiryDate, char[] cv2,
      String postCode, String cardType, String issueNumber, Map<String, String> address,
      Map<String, String> cvvPolicies, Map<String, String> addressPolicies,
      Map<String, String> postCodePolicies)
   {
      char[] panCopy = pan;
      char[] cv2Copy = cv2;

      this.pan = panCopy;
      this.nameOncard = nameOncard;
      this.expiryDate = expiryDate;
      this.cv2 = cv2Copy;
      this.postCode = postCode;
      this.cardType = cardType;
      this.issueNumber = issueNumber;
      this.address = address;
      this.cvvPolicies = cvvPolicies;
      this.addressPolicies = addressPolicies;
      this.postCodePolicies = postCodePolicies;
      populateXmlDocument();
   }
   //CHECKSTYLE:ON

/**
    * Constructor to create DataCashCard object for Cnp Cards.
    *
    * @param pan the card number.
    * @param cv2 the cv2 of the card.
    */
   public DataCashCard(char[] pan, char[] cv2)
   {
      char[] panCopy = pan;
      char[] cv2Copy = cv2;
      this.pan = panCopy;
      this.cv2 = cv2Copy;
   }
   
   /*defauld constructor created for hccs*/
   public DataCashCard()
   {
      
   }

   /**
    * Constructor to create DataCashCard object for Ground-Trader Cards.
    *
    * @param pan the card number.
    * @param nameOncard the name on the card.
    * @param expiryDate expiry date of the card.
    * @param cv2 the cv2 of the card.
    * @param postCode the post code of the card.
    * @param cardType the card type.
    * @param track2Data the track2data information
    * @param captureMethod the capture method.
    * @param tid the tid.
    *
    */
   //CHECKSTYLE:OFF
   public DataCashCard(char[] pan, String nameOncard, String expiryDate, char[] cv2,
      String postCode, String cardType, String track2Data, String captureMethod, String tid)
   {
      char[] panCopy = pan;
      char[] cv2Copy = cv2;
      this.pan = panCopy;
      this.nameOncard = nameOncard;
      this.expiryDate = expiryDate;
      this.cv2 = cv2Copy;
      this.postCode = postCode;
      this.cardType = cardType;
      populateGtXmlDocument(track2Data, captureMethod, tid);
   }
   //CHECKSTYLE:ON

   /**
    * Populates the Xml Document for the card details of this object.
    */
   private void populateXmlDocument()
   {
      try
      {
         xmlDocument = new XMLDocument();
         xmlDocument.set(buildCardDetails());
         populateExtendedPolicy();
      }
      catch (IOException ioe)
      {
         handleException(ioe);
      }
      catch (JDOMException jdome)
      {
         handleException(jdome);
      }
   }

   /**
    * Populates the xml document for Ground Trader.
    *
    * @param track2Data the track2data.
    * @param gtCaptureMethod the capture method.
    * @param tid the tid.
    */
   private void populateGtXmlDocument(String track2Data, String gtCaptureMethod, String tid)
   {
      try
      {
         if (track2Data != null)
         {
            xmlDocument = new XMLDocument();
            populateTrack2Data(track2Data);
         }
         else
         {
            populateXmlDocument();
         }
         if (!gtCaptureMethod.equalsIgnoreCase(CAPTUREMETHOD_CNP))
         {
            populateTerminalData(tid);
         }
         xmlDocument.set(CAPTURE_METHOD, gtCaptureMethod);
      }
      catch (IOException ioe)
      {
         handleException(ioe);
      }
      catch (JDOMException jdome)
      {
         handleException(jdome);
      }
   }

   /**
    * Populates the Xml Document for the Gift Card.
    *
    * @param transactionType the transaction type.
    *
    * @return xmlDocument the Xml document for the gift card.
    */
   public XMLDocument getGiftCardXmlDocument(String transactionType)
   {
      try
      {
         xmlDocument = new XMLDocument();
         xmlDocument.set(PPT_TXN_PAN, String.valueOf(this.pan));
         xmlDocument.set(PPT_TXN_METHOD, transactionType);
         xmlDocument.set(PPT_TXN_PIN, String.valueOf(this.cv2));
      }
      catch (IOException ioe)
      {
         handleException(ioe);
      }
      catch (JDOMException jdome)
      {
         handleException(jdome);
      }
      return xmlDocument;
   }

   /**
    * Sets the start date.
    *
    * @param startDate the start date of the card.
    */
   public void setStartDate(String startDate)
   {
      this.startDate = startDate;
   }

   /**
    * Sets the issue number.
    *
    * @param issueNumber the issue number of the card.
    */
   public void setIssueNumber(String issueNumber)
   {
      this.issueNumber = issueNumber;
   }

   /**
    * This method is responsible for validating the card details.
    *
    * @throws PaymentValidationException whenever datacash validation fails.
    * @throws RuntimeException when there is an unexpected error.
    */
   public void validate() throws PaymentValidationException
   {
      validate(null);
   }

   /**
    * This method is responsible for validating the card details.
    *
    * @param trackingData the trackingData which contains token,sessionId and
    *        invBookingRef.
    *
    * @throws PaymentValidationException whenever datacash validation fails.
    * @throws RuntimeException when there is an unexpected error.
    */
   public void validate(String trackingData) throws PaymentValidationException
   {
      CardInfo cardInfo = new CardInfo(ConfReader.getConfEntry("CreditCard.location", null));
      token = trackingData.substring(0, 36);
      uuid = UUID.fromString(token);
      paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      try
      {
         XMLDocument xmlDoc = new XMLDocument();
         xmlDoc.set(buildCardDetails());
         xmlDoc.setCardInfo(cardInfo);
         cardInfo.setStrict(true);
         BadRequest badRequest = cardInfo.validateCardInfo();
         validateCardType(cardInfo, trackingData);
         validateIssueNumber(cardInfo, trackingData);
         validateStartDate(cardInfo, trackingData);
         if (badRequest != null && cardInfo.getPanLength() > 0)
         {
            // Error is reported
            // Handle the error
            List<FailureReport> errors = Arrays.asList(badRequest.getFailureReports());
            String key = StringUtils.split(errors.get(0).toString(), ':')[0];
            String errorMessage = errors.get(0).getMessage();
            LogWriter.logErrorMessage(errorMessage);
            logBinValidation(trackingData, errorMessage);
            new TransactionStatusUtil().updateStatus(paymentData, token,
                    paymentData.getTransactionStatus(),
                    TransactionStatus.findByCodeTS("BIN_VALIDATION_FAILURE"));
            throw new PaymentValidationException(key);
         }
         logBinValidation(trackingData, SUCCESS);
         new TransactionStatusUtil().updateStatus(paymentData, token,
                 paymentData.getTransactionStatus(),
                 TransactionStatus.findByCodeTS("BIN_VALIDATION_SUCCESS"));
      }
      catch (IOException ioe)
      {
         handleException(ioe);
      }
      catch (JDOMException jdome)
      {
         handleException(jdome);
      }
    }


   /**
    * Creates the CardDetails object for the current DataCashCard object.
    *
    * @return The create CardDetails object.
    */
   @SuppressWarnings("unchecked")
   public CardDetails buildCardDetails()
   {
      CardDetails cardDetails = new CardDetails();
      cardDetails.put(DataCashServiceConstants.PAN, String.valueOf(pan));
      cardDetails.put(DataCashServiceConstants.EXPIRY_DATE, expiryDate);
      if (StringUtils.isNotBlank(String.valueOf(cv2)))
      {
         cardDetails.put(DataCashServiceConstants.CV2, String.valueOf(cv2));
      }
      if (startDate != null)
      {
         cardDetails.put(DataCashServiceConstants.START_DATE, startDate);
      }
      if (issueNumber != null)
      {
         cardDetails.put(DataCashServiceConstants.ISSUE_NUMBER, issueNumber);
      }
      if (postCode != null)
      {
         cardDetails.put(DataCashServiceConstants.POST_CODE, postCode);
      }
      if (nameOncard != null)
      {
         cardDetails.put(DataCashServiceConstants.NAME_ON_CARD, nameOncard);
      }
      StringBuilder combinedAddress = new StringBuilder();
      combinedAddress = populateAddress(combinedAddress);
      if (StringUtils.isNotBlank(String.valueOf(combinedAddress)))
      {
         cardDetails.put(DataCashServiceConstants.STREET_NUMBER,
            String.valueOf(combinedAddress));
      }
      return cardDetails;
   }

   /**
   * Method which populates the address.
   *
   * @param combinedAddress the combined address.
   * @return address.
   */
   private StringBuilder populateAddress(StringBuilder combinedAddress)
   {
      String streetAddress1 = address.get(DataCashServiceConstants.STREET_NUMBER);
      String streetAddress2 = address.get(DataCashServiceConstants.STREETADDRESS);
      String streetAddress3 = address.get(DataCashServiceConstants.TOWN);
      String streetAddress4 = address.get(DataCashServiceConstants.COUNTY);
      if (StringUtils.isNotBlank(streetAddress1))
      {
         combinedAddress.append(streetAddress1).append(COMMA);
      }
      if (StringUtils.isNotBlank(streetAddress2))
      {
         combinedAddress.append(streetAddress2).append(COMMA);
      }
      if (StringUtils.isNotBlank(streetAddress3))
      {
         combinedAddress.append(streetAddress3).append(COMMA);
      }
      if (StringUtils.isNotBlank(streetAddress4))
      {
         combinedAddress.append(streetAddress4);
      }
      return combinedAddress;
   }

   /**
    * Validates the switch cards. Since, the card length for Switch/Maestro cards are not available,
    * it is validated separately.
    *
    * @param cardInfo the <code>CardInfo</code> object, which contains the details
    *    of the entered card number.
    * @param trackingData the trackingData which contains token,sessionId
    *        and invBookingRef.
    *
    * @throws PaymentValidationException if validation fails.
    */
   private void validateCardType(CardInfo cardInfo, String trackingData)
          throws PaymentValidationException
   {
      String givenCardType = cardType.replace('_', ' ');
      String actualCardType = cardInfo.getScheme();
      
      if("TUI Mastercard".equalsIgnoreCase(givenCardType))
      {
         givenCardType = "Mastercard";
      }
      if("MASTERCARD GIFT".equalsIgnoreCase(givenCardType))
      {
         givenCardType = "Mastercard";
      }
 
      if(StringUtils.isBlank(actualCardType)){
          throw new PaymentValidationException("datacash.cardtype.mismatch");
         }
      

      if (!givenCardType.equalsIgnoreCase(actualCardType))
      {
         //TODO: This should be taken care in UI by providing proper card types
         // for Switch and Maestro.
         if (!(givenCardType.equalsIgnoreCase(SWITCH) && actualCardType
                  .equalsIgnoreCase(MAESTRO)))
         {
            String errorMessage = PropertyResource.getProperty("datacash.cardtype.mismatch",
               MESSAGES_PROPERTY);
            LogWriter.logErrorMessage(errorMessage);
            logBinValidation(trackingData, getValidateCardTypeErrorMessage(
                     errorMessage, actualCardType));
            new TransactionStatusUtil().updateStatus(paymentData, token,
                    paymentData.getTransactionStatus(),
                    TransactionStatus.findByCodeTS("BIN_VALIDATION_FAILURE"));
            throw new PaymentValidationException(
               DataCashServiceConstants.ERRORCODE_DATACASHCARDMISMATCH,
               "datacash.cardtype.mismatch", "CardType|CardNumber");
         }
      }
   }

   /**
    * Validates the issue number for card if it is not null and length more than zero.
    *
    * @param cardInfo the <code>CardInfo</code> object, which contains the details
    *        of the entered card number.
    * @param trackingData the trackingData which contains token,sessionId
    *        and invBookingRef.
    *
    * @throws PaymentValidationException if validation fails.
    */
   private void validateIssueNumber(CardInfo cardInfo, String trackingData)
             throws PaymentValidationException
   {
      if (cardInfo.getIssueDigits() != null && cardInfo.getIssueDigits() > 0)
      {
         if (StringUtils.isEmpty(issueNumber))
         {
            String errorMessage = PropertyResource.getProperty("datacash.issuenumber.missing",
               MESSAGES_PROPERTY);
            LogWriter.logErrorMessage(errorMessage);
            logBinValidation(trackingData, errorMessage);
            new TransactionStatusUtil().updateStatus(paymentData, token,
                    paymentData.getTransactionStatus(),
                    TransactionStatus.findByCodeTS("BIN_VALIDATION_FAILURE"));
            throw new PaymentValidationException(
               DataCashServiceConstants.ERRORCODE_ISSUENOMISSING,
               "datacash.issuenumber.missing", "IssueNumber");
         }
         else if (issueNumber.length() != cardInfo.getIssueDigits())
         {
            String errorMessage = PropertyResource.getProperty("datacash.issuelength.mismatch",
               MESSAGES_PROPERTY);
            LogWriter.logErrorMessage(errorMessage);
            logBinValidation(trackingData, errorMessage);
            new TransactionStatusUtil().updateStatus(paymentData, token,
                    paymentData.getTransactionStatus(),
                    TransactionStatus.findByCodeTS("BIN_VALIDATION_FAILURE"));
            throw new PaymentValidationException(
               DataCashServiceConstants.ERRORCODE_ISSUENOMISMATCH,
               "datacash.issuelength.mismatch", "IssueNumber");
         }
      }
   }

   /**
    * Validates the start date if required for card.
    *
    * @param cardInfo the <code>CardInfo</code> object, which contains the details
    *    of the entered card number.
    * @param trackingData the trackingData which contains token,sessionId
    *        and invBookingRef.
    *
    * @throws PaymentValidationException if validation fails.
    */
   private void validateStartDate(CardInfo cardInfo, String trackingData)
             throws PaymentValidationException
   {
      if (cardInfo.isStartDateRequired() && StringUtils.isEmpty(startDate))
      {
         String errorMessage = PropertyResource.getProperty("datacash.startdate.missing",
            MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         logBinValidation(trackingData, errorMessage);
         new TransactionStatusUtil().updateStatus(paymentData, token,
                 paymentData.getTransactionStatus(),
                 TransactionStatus.findByCodeTS("BIN_VALIDATION_FAILURE"));
            throw new PaymentValidationException(
         DataCashServiceConstants.ERRORCODE_STARTDATEMISSING,
            "datacash.startdate.missing", "ValidFrom");
      }
   }

   /**
    * This will create the <code>XMLDocument</code> required for all authorization of the
    * transaction types.
    *
    * @param transactionType the transaction type.
    * @param authCode the auth code.
    *
    * @return The created <code>XMLDocument</code>.
    */
   public XMLDocument getHpsAuthXmlDocument(String transactionType, String authCode, String hpsDatacashReference)
   {
	   XMLDocument xmlDocument = null;
	try {
		
		xmlDocument = new XMLDocument();
		Hashtable amt = new Hashtable();
		amt.put("type", "from_hps");
		xmlDocument.set("Request.Transaction.CardTxn.card_details", hpsDatacashReference, amt); 
		xmlDocument.set(TRANSACTION_TYPE, transactionType);
		if (authCode != null)
	      {
	         xmlDocument.set(AUTH_CODE, authCode);
	      }
			
		
	} catch (IOException ioe) {
		handleException(ioe);
	} catch (JDOMException jdome) {
		handleException(jdome);
	}
      
      return xmlDocument;
   }
   
   /**
    * This will create the <code>XMLDocument</code> required for all authorization of the
    * transaction types.
    *
    * @param transactionType the transaction type.
    * @param authCode the auth code.
    *
    * @return The created <code>XMLDocument</code>.
    */
   public XMLDocument getAuthXmlDocument(String transactionType, String authCode)
   {
      xmlDocument.set(TRANSACTION_TYPE, transactionType);
      if (authCode != null)
      {
         xmlDocument.set(AUTH_CODE, authCode);
      }
      return xmlDocument;
   }

   /**
    * Populates the extended policy data.
    */
   private void populateExtendedPolicy()
   {
      String issuedCountry = getCardIssuedCountry();
      this.country = issuedCountry;
      LogWriter.logInfoMessage("Card issued country: "
                                  + issuedCountry, DataCashCard.class.getName());
      String [] defaultCountryList = {"gbr"};
      String [] countries =
          ConfReader.getStringValues("countries.CV2AVS.applicable", defaultCountryList, COMMA);
      List<String> countryList = Arrays.asList(countries);

      if ((issuedCountry != null && !countryList.contains(issuedCountry))
               || cardType.equalsIgnoreCase(AMEX))
      {
         xmlDocument.set(CV2_POLICY, null, getDefaultCv2Policy());
         xmlDocument.set(POSTCODE_POLICY, null, getDefaultPostCodeAndAddressPolicy());
         xmlDocument.set(ADDRESS_POLICY, null, getDefaultPostCodeAndAddressPolicy());
      }
      else
      {
      xmlDocument.set(CV2_POLICY, null, getCv2Policy());
         xmlDocument.set(POSTCODE_POLICY, null, getPostCodePolicy());
         xmlDocument.set(ADDRESS_POLICY, null, getAddressPolicy());
      }
   }

   /**
    * Populates the extended policy data.
    */
   public void populateExtendedPolicy(XMLDocument document, String issuedCountry)
   {
      LogWriter.logInfoMessage("Card issued country: " + issuedCountry,
         DataCashCard.class.getName());
      String[] defaultCountryList = { "gbr" };
      String[] countries =
         ConfReader.getStringValues("countries.CV2AVS.applicable", defaultCountryList, COMMA);
      List<String> countryList = Arrays.asList(countries);

      if ((issuedCountry != null && !countryList.contains(issuedCountry))
         || cardType.equalsIgnoreCase(AMEX))
      {
         document.set(CV2_POLICY, null, getDefaultCv2Policy());
         document.set(POSTCODE_POLICY, null, getDefaultPostCodeAndAddressPolicy());
         document.set(ADDRESS_POLICY, null, getDefaultPostCodeAndAddressPolicy());
      }
      else
      {
         document.set(CV2_POLICY, null, getCv2Policy());
         document.set(POSTCODE_POLICY, null, getPostCodePolicy());
         document.set(ADDRESS_POLICY, null, getAddressPolicy());
      }
   }

   /**
    * Populates the extended policy data.
    */
   public void populateAddressDetails(XMLDocument document)
   {
      document.set(buildCardDetailsForHcc());
   }

   public CardDetails buildCardDetailsForHcc()
   {
      CardDetails cardDetails = new CardDetails();
      if (postCode != null)
      {
         cardDetails.put(DataCashServiceConstants.POST_CODE, postCode);
      }
      StringBuilder combinedAddress = new StringBuilder();
      combinedAddress = populateAddress(combinedAddress);
      if (StringUtils.isNotBlank(String.valueOf(combinedAddress)))
      {
         cardDetails.put(DataCashServiceConstants.STREET_NUMBER, String.valueOf(combinedAddress));
      }
      return cardDetails;
   }

   /**
    *  Method to get card issued country.
    *
    * @return country the card issued country..
    */
    private String getCardIssuedCountry()
   {
       CardInfo cardInfo = new CardInfo(ConfReader.getConfEntry("CreditCard.location", null));
       try
       {
          XMLDocument xmlDoc = new XMLDocument();
          xmlDoc.set(buildCardDetails());
          xmlDoc.setCardInfo(cardInfo);
          cardInfo.setStrict(true);
       }
       catch (IOException ioe)
       {
          handleException(ioe);
       }
       catch (JDOMException jdome)
       {
          handleException(jdome);
       }
       if (cardInfo != null && cardInfo.getCountry() != null)
       {
          return cardInfo.getCountry();
       }
       return "";
   }

    /**
     * Method to get the address policy.
     * @return extendedPolicy the table of address policies.
     */
   private Hashtable<String, String> getAddressPolicy()
    {
       Hashtable<String, String> extendedPolicy = new Hashtable<String, String>();
       extendedPolicy.put(NOT_PROVIDED, addressPolicies.get(NOT_PROVIDED));
       extendedPolicy.put(NOT_CHECKED, addressPolicies.get(NOT_CHECKED));
       extendedPolicy.put(MATCHED, addressPolicies.get(MATCHED));
       extendedPolicy.put(NOT_MATCHED, addressPolicies.get(NOT_MATCHED));
       extendedPolicy.put(PARTIAL_MATCH, addressPolicies.get(PARTIAL_MATCH));
       return extendedPolicy;
    }

   /**
    * Method to get the post code policy.
    * @return extendedPolicy the table of post code policies.
    */
    private Hashtable<String, String> getPostCodePolicy()
    {
       Hashtable<String, String> extendedPolicy = new Hashtable<String, String>();
       extendedPolicy.put(NOT_PROVIDED, postCodePolicies.get(NOT_PROVIDED));
       extendedPolicy.put(NOT_CHECKED, postCodePolicies.get(NOT_CHECKED));
       extendedPolicy.put(MATCHED, postCodePolicies.get(MATCHED));
       extendedPolicy.put(NOT_MATCHED, postCodePolicies.get(NOT_MATCHED));
       extendedPolicy.put(PARTIAL_MATCH, postCodePolicies.get(PARTIAL_MATCH));
       return extendedPolicy;
    }

    /**
     * Method to get the cv2 policy.
     * @return extendedPolicy the table of cv2 policies.
     */
    private Hashtable<String, String> getCv2Policy()
    {
       Hashtable<String, String> cv2Policy = new Hashtable<String, String>();
       cv2Policy.put(NOT_PROVIDED, cvvPolicies.get(NOT_PROVIDED));
       cv2Policy.put(NOT_CHECKED, cvvPolicies.get(NOT_CHECKED));

       cv2Policy.put(MATCHED, cvvPolicies.get(MATCHED));
       cv2Policy.put(NOT_MATCHED, cvvPolicies.get(NOT_MATCHED));
       cv2Policy.put(PARTIAL_MATCH, cvvPolicies.get(PARTIAL_MATCH));
       return cv2Policy;
   }

   /**
    * Returns a Hash table containing cv2 policy.
    *
    * @return the hash table of cv2policies
    */
   private Hashtable<String, String> getDefaultCv2Policy()
   {
      Hashtable<String, String> cv2Policy = new Hashtable<String, String>();
      cv2Policy.put(NOT_PROVIDED, REJECT);
      cv2Policy.put(NOT_CHECKED, ACCEPT);
      cv2Policy.put(MATCHED, ACCEPT);
      cv2Policy.put(NOT_MATCHED, REJECT);
      cv2Policy.put(PARTIAL_MATCH, REJECT);
      return cv2Policy;
   }

   /**
    * Returns a Hash table containing post code and address policies.
    *
    * @return the hash table of post-code and address policies
    */
   private Hashtable<String, String> getDefaultPostCodeAndAddressPolicy()
   {
      Hashtable<String, String> extendedPolicy = new Hashtable<String, String>();
      extendedPolicy.put(NOT_PROVIDED, ACCEPT);
      extendedPolicy.put(NOT_CHECKED, ACCEPT);
      extendedPolicy.put(MATCHED, ACCEPT);
      extendedPolicy.put(NOT_MATCHED, ACCEPT);
      extendedPolicy.put(PARTIAL_MATCH, ACCEPT);
      return extendedPolicy;
   }

   /**
    * Populate the track2 data details.
    *
    * @param track2Info the track2Data.
    */
   private void populateTrack2Data(String track2Info)
   {
      Hashtable<String, String> track2DataXml = new Hashtable<String, String>();
      track2DataXml.put(TRACK2DATA_TYPE, TRACK2DATA);
      xmlDocument.set(CARD_DETAILS, track2Info, track2DataXml);
   }

   /**
    * This method populates terminal data.
    *
    * @param tid the tid.
    */
   private void populateTerminalData(String tid)
   {
      Hashtable<String, String> terminal = new Hashtable<String, String>();
      terminal.put(TERMINALID_TYPE, tid);
      xmlDocument.set(TERMINAL, null, terminal);
      Hashtable<String, String> terminalCapabilties = new Hashtable<String, String>();
      terminalCapabilties.put(IC_READER, FALSE);
      terminalCapabilties.put(MAGNETIC_STRIPE_READER, TRUE);
      terminalCapabilties.put(MANUAL_CARD_ENTRY, TRUE);
      xmlDocument.set(TERMINAL_CAPABILTIES, null, terminalCapabilties);
      Hashtable<String, String> featureCapabilties = new Hashtable<String, String>();
      featureCapabilties.put(PIN_PAD_AVAILABLE, FALSE);
      xmlDocument.set(FEATURE_CAPABILTIES, null, featureCapabilties);
      xmlDocument.set(ICC_TERM_TYPE, ICC_TERM_TYPE_VALUE);
   }

   /**
    * Purges the sensitive details of the card.
    */
   public void purgeCardDetails()
   {
      if (pan != null)
      {
         Arrays.fill(pan, '*');
         pan = null;
      }
      if (cv2 != null)
      {
         Arrays.fill(cv2, '*');
         cv2 = null;
      }
   }

   /**
    * Handles the exceptions thrown by DataCash.
    *
    * @param throwable the <code>Throwable</code> object.
    */
   private void handleException(Throwable throwable)
   {
      String message = throwable.getMessage();
      LogWriter.logErrorMessage(message, throwable);
      throw new RuntimeException(message);
   }

   /**
    * This method will take the trackingData and message as the input
    * parameters and logs the required details.
    *
    * @param trackingData the trackingData which contains token,sessionId
    *        and invBookingRef.
    * @param message the message.
    *
    *
    */
   private void logBinValidation(String trackingData, String message)
   {
      Date date = new Date();
      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      String dateTime = dateFormat.format(date);
      StringBuilder trackingInfo = new StringBuilder();
      trackingInfo.append(ACTION).append(COMMA)
                  .append(dateTime).append(COMMA)
                  .append(trackingData).append(COMMA)
                  .append(cardType).append(COMMA)
                  .append(country).append(COMMA)
                  .append(message);
      LogWriter.logInfoMessage(trackingInfo.toString(),
         DataCashCard.class.getName());
   }

   /**
    * This method will take the error message and the expected card type as the input
    * and appends the customer entered cardType and expected cardType to the error message
    * and returns the error message required for logging purpose.
    *
    * @param errorMessage the errorMessage .
    * @param expectedCardType the actual cardType.
    *
    * @return String the error message.
    */
   private String getValidateCardTypeErrorMessage(String errorMessage, String expectedCardType)
   {
       StringBuilder loggingErrorMessage = new StringBuilder();
       loggingErrorMessage.append(errorMessage)
                          .append(CUSTOMER_ENTERED_CARDTYPE).append(COLON)
                          .append(cardType.replace('_', ' '))
                          .append(" ")
                          .append(EXPECTED_CARDTYPE).append(COLON)
                          .append(expectedCardType);
       return loggingErrorMessage.toString();

   }

   /**
    * Gets the country.
    *
    * @return the country
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Sets the cvvPolicies.
    *
    * @return the cvvPolicies
    */
   public Map<String, String> getCvvPolicies()
   {
      return cvvPolicies;
   }

   /**
    * Sets the addressPolicies.
    *
    * @return the addressPolicies
    */
   public Map<String, String> getAddressPolicies()
   {
      return addressPolicies;
   }

   /**
    * Get the startDate.
    *
    * @return the startDate
    */
   public String getStartDate()
   {
      return startDate;
   }

   /**
    * Sets the postCodePolicies.
    *
    * @return the postCodePolicies
    */
   public Map<String, String> getPostCodePolicies()
   {
      return postCodePolicies;
   }

   /**
    * Get the address.
    *
    * @return the address
    */
   public Map<String, String> getAddress()
   {
      return address;
   }

   /**
    * Get the CV2AVS flag.
    *
    * @return the cv2AVS flag.
    */
    public Boolean getCv2AVSEnabled()
    {
       return cv2AVSEnabled;
    }

    /**
     * Set the CV2AVS flag.
     *
     * @param cv2AVS the cv2AVS flag.
     */
     public void setCv2AVSEnabled(Boolean cv2AVS)
     {
        this.cv2AVSEnabled = cv2AVS;
     }

     /**
      * Get the capture method.
      * @return the captureMethod
      */
     public String getCaptureMethod()
     {
        return captureMethod;
     }

     /**
      * Set the capture method.
      * @param captureMethod the captureMethod to set
      */
     public void setCaptureMethod(String captureMethod)
     {
        this.captureMethod = captureMethod;
     }

}
