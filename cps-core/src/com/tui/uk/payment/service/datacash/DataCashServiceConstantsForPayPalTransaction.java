package com.tui.uk.payment.service.datacash;

/**
 * The all DataCash service constants for PayPal transaction
 *
 * @author tanveer.bp
 *
 */
public class DataCashServiceConstantsForPayPalTransaction {

	/** added for paypal method element. */
	public static final String PAYPAL_LOGIN_URL = "PayPal.LoginUrl";

	/** added for paypal method element. */
	public static final String PAYPAL_METHOD = "Request.Transaction.PayPalTxn.method";

	/** added for paypal method element datacsah reference. */
	public static final String PAYPAL_DATACASH_REFERENCE = "Request.Transaction.PayPalTxn.reference";

	/** added for paypal method element datacsah reference. */
	public static final String PAYPAL_MERCHANT_REFERENCE = "Request.Transaction.TxnDetails.merchantreference";

	/** added for paypal method element merchant reference. */
	public static final String PAYPAL_AUTHORIZE_AMOUNT = "Request.Transaction.TxnDetails.amount";

	/** added for paypal method element. */
	public static final String PAYPAL_RETURN_URL = "Request.Transaction.PayPalTxn.return_url";

	/** added for paypal method element. */
	public static final String PAYPAL_CANCEL_URL = "Request.Transaction.PayPalTxn.cancel_url";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_TYPE = "Request.Transaction.PayPalTxn.billing_type";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYMENT_ACTION = "Request.Transaction.PayPalTxn.payment_action";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYMENT_TYPE = "Request.Transaction.PayPalTxn.payment_type";

	/** added for paypal method element. */
	public static final String PAYPAL_BRAND_NAME = "Request.Transaction.PayPalTxn.brand_name";

	/** added for paypal method element. */
	public static final String PAYPAL_CART_BORDER_COLOUR = "Request.Transaction.PayPalTxn.cart_border_colour";

	/** added for paypal method element. */
	public static final String PAYPAL_CHANNEL_TYPE = "Request.Transaction.PayPalTxn.channel_type";

	/** added for paypal method element. */
	public static final String PAYPAL_HEADER_STYLE = "Request.Transaction.PayPalTxn.header_style";

	/** added for paypal method element. */
	public static final String PAYPAL_INVNUM = "Request.Transaction.PayPalTxn.invnum";

	/** added for paypal method element. */
	public static final String PAYPAL_COMPLETED = "Request.Transaction.PayPalTxn.completed";

	/** added for paypal method element. */
	public static final String PAYPAL_LOCALE_CODE = "Request.Transaction.PayPalTxn.localecode";

	/** added for paypal method element. */
	public static final String PAYPAL_LOGO_IMAGE = "Request.Transaction.PayPalTxn.logo_image";

	/** added for paypal method element. */
	public static final String PAYPAL_MAX_AMOUNT = "Request.Transaction.PayPalTxn.max_amount";

	/** added for paypal method element. */
	public static final String PAYPAL_NO_SHIPPING = "Request.Transaction.PayPalTxn.no_shipping";

	/** added for paypal method element. */
	public static final String PAYPAL_REQ_BILLING_TYPE = "Request.Transaction.PayPalTxn.req_billing_address";

	/** added for paypal method element. */
	public static final String PAYPAL_REQ_BILLILNG_ADDRESS = "Request.Transaction.PayPalTxn.req_billing_address";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_AGREEMENT_DESCRIPTION = "Request.Transaction.PayPalTxn.billing_agreement_description";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_AGREEMENT_CUSTOM = "Request.Transaction.PayPalTxn.billing_agreement_custom";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_AMOUNT = "Request.Transaction.PayPalTxn.ShippingAddress.amount";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_CITY = "Request.Transaction.PayPalTxn.ShippingAddress.city";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE = "Request.Transaction.PayPalTxn.ShippingAddress.country_code";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_NAME = "Request.Transaction.PayPalTxn.ShippingAddress.name";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_POSTCODE = "Request.Transaction.PayPalTxn.ShippingAddress.postcode";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_REGION = "Request.Transaction.PayPalTxn.ShippingAddress.region";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1 = "Request.Transaction.PayPalTxn.ShippingAddress.street_address1";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS2 = "Request.Transaction.PayPalTxn.ShippingAddress.street_address2";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_TELEPHONE_NUMBER = "Request.Transaction.PayPalTxn.ShippingAddress.telephone_number";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_CITY = "Request.Transaction.PayPalTxn.BillingAddress.city";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_COUNTRY_CODE = "Request.Transaction.PayPalTxn.BillingAddress.country_code";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_NAME = "Request.Transaction.PayPalTxn.BillingAddress.name";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_POSTCODE = "Request.Transaction.PayPalTxn.BillingAddress.postcode";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_REGION = "Request.Transaction.PayPalTxn.BillingAddress.region";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_STREET_ADDRESS1 = "Request.Transaction.PayPalTxn.BillingAddress.street_address1";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_STREET_ADDRESS2 = "Request.Transaction.PayPalTxn.BillingAddress.street_address2";

	/** added for paypal method element. */
	public static final String PAYPAL_MSGSUB = "Request.Transaction.PayPalTxn.message_submission_id";

	/** added for paypal method element. */
	public static final String PAYPAL_NOTE = "Request.Transaction.PayPalTxn.note";

	/** added for paypal method element. */
	public static final String PAYPAL_SOFT_DESCRIPTOR = "Request.Transaction.PayPalTxn.soft_descriptor";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_CLEARING_COUNT = "Request.Transaction.PayPalTxn.AirlineItineraryData.clearing_count";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_CLEARING_SEQUENCE = "Request.Transaction.PayPalTxn.AirlineItineraryData.clearing_sequence";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_ISSUING_CARRIER_CODE = "Request.Transaction.PayPalTxn.AirlineItineraryData.issuing_carrier_code";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_PASSENGER_NAME = "Request.Transaction.PayPalTxn.AirlineItineraryData.passenger_name";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_RESTRICTED_TICKET = "Request.Transaction.PayPalTxn.AirlineItineraryData.restricted_ticket";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_TICKET_NUMBER = "Request.Transaction.PayPalTxn.AirlineItineraryData.ticket_number";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_CUSTOMER_CODE = "Request.Transaction.PayPalTxn.AirlineItineraryData.customer_code";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_ISSUE_DATE = "Request.Transaction.PayPalTxn.AirlineItineraryData.issue_date";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_TOTAL_FARE = "Request.Transaction.PayPalTxn.AirlineItineraryData.total_fare";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_TOTAL_FEE = "Request.Transaction.PayPalTxn.AirlineItineraryData.total_fee";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_TOTAL_TAXES = "Request.Transaction.PayPalTxn.AirlineItineraryData.total_taxes";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_TRAVEL_AGENCY_CODE = "Request.Transaction.PayPalTxn.AirlineItineraryData.travel_agency_code";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_TRAVEL_AGENCY_NAME = "Request.Transaction.PayPalTxn.AirlineItineraryData.travel_agency_name";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_ARRIVAL_AIRPORT_CODE = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.arrival_airport_code";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_CARRIER_CODE = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.carrier_code";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_DEPARTURE_TIME = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.departure_time";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_FARE_BASIS_CODE = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.fare_basis_code";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_FLIGHT_NUMBER = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.flight_number";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_SERVICE_CLASS = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.service_class";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_STOPOVER_CODE = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.stopover_code";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_TRAVEL_DATE = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.travel_date";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_CONJUCTION_TICKET = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.conjunction_ticket";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_COUPON_NUMBER = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.coupon_number";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_ENDORS_OR_RESTRS = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.endorsement_or_restrictions";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_EXCHANGE_TICKET = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.exchange_ticket";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_FARE = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.fare";

	/** added for paypal method element. */
	public static final String PAYPAL_AIRLINE_ITINERYDATA_FLIGHT_DETAILS_TAXES = "Request.Transaction.PayPalTxn.AirlineItineraryData.FlightDetails.taxes";

	/**
	 * Following constants have been added as part of paypal response
	 */
	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS = "Response.PayPalTxn.ShippingAddress";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_CITY_RS = "Response.PayPalTxn.ShippingAddress.city";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_NAME_RS = "Response.PayPalTxn.ShippingAddress.name";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_COUNTRY_CODE_RS = "Response.PayPalTxn.ShippingAddress.country_code";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_REGION_RS = "Response.PayPalTxn.ShippingAddress.region";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_STREET_ADDRESS1_RS = "Response.PayPalTxn.ShippingAddress.street_address1";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_ADDRESS_POSTCODE_RS = "Response.PayPalTxn.ShippingAddress.postcode";

	/** added for paypal method element. */

	public static final String PAYPAL_BILLING_ADDRESS_CITY_RS = "Response.PayPalTxn.BillingAddress.city";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_NAME_RS = "Response.PayPalTxn.BillingAddress.name";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_COUNTRY_CODE_RS = "Response.PayPalTxn.BillingAddress.country_code";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_POSTCODE_RS = "Response.PayPalTxn.BillingAddress.postcode";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_STREET_ADDRESS1_RS = "Response.PayPalTxn.BillingAddress.street_address1";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_ADDRESS_STATUS_RS = "Response.PayPalTxn.BillingAddress.address_status";

	public static final String PAYPAL_COUNTRY_CODE_RS = "Response.PayPalTxn.countrycode";

	/** added for paypal method element. */
	public static final String PAYPAL_FIRSTNAME_RS = "Response.PayPalTxn.firstname";

	/** added for paypal method element. */
	public static final String PAYPAL_HANDLING_AMT_RS = "Response.PayPalTxn.handlingamt";

	/** added for paypal method element. */
	public static final String PAYPAL_INSURANCEAMT_RS = "Response.PayPalTxn.insuranceamt";

	/** added for paypal method element. */
	public static final String PAYPAL_LAST_NAME_RS = "Response.PayPalTxn.lastname";

	/** added for paypal method element. */
	public static final String PAYPAL_CURRENCY_CODE_RS = "Response.PayPalTxn.currencycode";

	public static final String PAYPAL_EMAIL_RS = "Response.PayPalTxn.email";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYERID_RS = "Response.PayPalTxn.payerid";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYER_STATUS_RS = "Response.PayPalTxn.payerstatus";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPAMT_RS = "Response.PayPalTxn.shippingamt";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPDISAMT_RS = "Response.PayPalTxn.shipdiscamt";

	/** added for paypal method element. */
	public static final String PAYPAL_ITEMAMT_RS = "Response.PayPalTxn.itemamt";

	public static final String PAYPAL_ADDRESSID_RS = "Response.PayPalTxn.addressid";

	/** added for paypal method element. */
	public static final String PAYPAL_ADDRESS_STATUS_RS = "Response.PayPalTxn.addressstatus";

	/** added for paypal method element. */
	public static final String PAYPAL_ALLOWED_PAYMENT_METHOD_RS = "Response.PayPalTxn.allowedpaymentmethod";

	/** added for paypal method element. */
	public static final String PAYPAL_BUSINESS_RS = "Response.PayPalTxn.business";

	/** added for paypal method element. */
	public static final String PAYPAL_CHECKOUT_STATUS_RS = "Response.PayPalTxn.checkoutstatus";

	/** added for paypal method element. */
	public static final String PAYPAL_CUSTOM_RS = "Response.PayPalTxn.custom";

	public static final String PAYPAL_FINANCING_FEE_AMT_RS = "Response.PayPalTxn.financingfeeamt";

	/** added for paypal method element. */
	public static final String PAYPAL_FINANCING_MONTHLY_PAYMENT_RS = "Response.PayPalTxn.financingmonthlypayment";

	/** added for paypal method element. */
	public static final String PAYPAL_FINANCING_TOTAL_COST_RS = "Response.PayPalTxn.financingtotalcost";

	/** added for paypal method element. */
	public static final String PAYPAL_FINANCING_TERM_RS = "Response.PayPalTxn.financingterm";

	/** added for paypal method element. */
	public static final String PAYPAL_CARTCHANGE_TOLERANCE_RS = "Response.PayPalTxn.cartchangetolerance";

	/** added for paypal method element. */
	public static final String PAYPAL_ISFINANCING_RS = "Response.PayPalTxn.isfinancing";

	public static final String PAYPAL_MIDDLE_NAME_RS = "Response.PayPalTxn.middlename";

	/** added for paypal method element. */
	public static final String PAYPAL_NOTETEXT_RS = "Response.PayPalTxn.notetext";

	/** added for paypal method element. */
	public static final String PAYPAL_NOTIFYURL_RS = "Response.PayPalTxn.notifyurl";

	/** added for paypal method element. */
	public static final String PAYPAL_PHONE_NUM_RS = "Response.PayPalTxn.phonenum";

	/** added for paypal method element. */
	public static final String PAYPAL_SALUTATION_RS = "Response.PayPalTxn.salutation";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYMENT_REQUEST_ID_RS = "Response.PayPalTxn.paymentrequestid";

	/** added for paypal method element. */
	public static final String PAYPAL_SHIPPING_CALCULATION_MODE_RS = "Response.PayPalTxn.shippingcalculationmode";

	/** added for paypal method element. */
	public static final String PAYPAL_INSURANCE_OPTION_SELECTED_RS = "Response.PayPalTxn.insuranceoptionselected";

	/** added for paypal method element. */
	public static final String PAYPAL_CORRELATIONID_RS = "Response.PayPalTxn.correlationid";

	/** added for paypal method element. */
	public static final String PAYPAL_BILLING_AGREEMENT_ID_RS = "Response.PayPalTxn.billing_agreement_id";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_ID_RS = "Response.PayPalTxn.transactionid";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_TYPE_RS = "Response.PayPalTxn.transactiontype";

	/** added for paypal method element. */
	public static final String PAYPAL_TOKEN_RS = "Response.PayPalTxn.token";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYMENT_STATUS_RS = "Response.PayPalTxn.paymentstatus";

	/** added for paypal method element. */
	public static final String PAYPAL_PAYMENT_TYPE_RS = "Response.PayPalTxn.paymenttype";

	/** added for paypal method element. */
	public static final String PAYPAL_ACK = "Response.PayPalTxn.ack";

	/** added for paypal method element. */
	public static final String PAYPAL_BUILD = "Response.PayPalTxn.build";

	/** added for paypal method element. */
	public static final String PAYPAL_AMT = "Response.PayPalTxn.amt";

	/** added for paypal method element. */
	public static final String PAYPAL_ORDERTIME = "Response.PayPalTxn.ordertime";

	/** added for paypal method element. */
	public static final String PAYPAL_VERSION = "Response.PayPalTxn.version";

	/** added for paypal method element. */
	public static final String PAYPAL_TIMESTAMP = "Response.PayPalTxn.timestamp";

	/** added for paypal method element. */
	public static final String PAYPAL_TAXAMT = "Response.PayPalTxn.taxamt";

	/** added for paypal method element. */
	public static final String PAYPAL_EXCHANGE_RATE = "Response.PayPalTxn.exchangerate";

	/** added for paypal method element. */
	public static final String PAYPAL_SETTLE_AMT = "Response.PayPalTxn.settleamt";

	/** added for paypal method element. */
	public static final String PAYPAL_MSGSUB_ID = "Response.PayPalTxn.msgsubid";

	/** added for paypal method element. */
	public static final String PAYPAL_AUTHORIZATION_ID_RS = "Response.PayPalTxn.authorizationid";

	/** added for paypal method element. */
	public static final String PAYPAL_PARENT_TRANSACTION_ID_RS = "Response.PayPalTxn.parenttransactionid";

	/** added for paypal method element. */
	public static final String PAYPAL_RECEIPT_RS = "Response.PayPalTxn.receipt";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_DATE = "Response.QueryTxnResult.transaction_date";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_STATUS = "Response.QueryTxnResult.status";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_REASON = "Response.QueryTxnResult.reason";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_MERCHANT_REFERENCE = "Response.QueryTxnResult.merchant_reference";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_DATACASH_REFERENCE = "Response.QueryTxnResult.datacash_reference";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_QUERY_METHOD = "Response.QueryTxnResult.PayPalTxn.method";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_PAYMENTSTATUS = "Response.QueryTxnResult.PayPalTxn.paymentstatus";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_PENDINGREASON = "Response.QueryTxnResult.PayPalTxn.pendingreason";

	/** added for paypal method element. */
	public static final String PAYPAL_TRANSACTION_AMOUNT = "Response.QueryTxnResult.amount";

	/** The constant for historic transaction type. */
	public static final String PAYPAL_METHOD_SETEXPRESSCHECKOUT ="set_express_checkout";

	/** The constant for historic transaction type. */
	public static final String PAYPAL_METHOD_GETEXPRESSCHECKOUT ="get_express_checkout_details";

	/** The constant for historic transaction type. */
	public static final String PAYPAL_METHOD_DOEXPRESSCHECKOUT ="do_express_checkout_payment";

	/** The constant for historic transaction type. */
	public static final String PAYPAL_METHOD_DOAUTHORIZATION ="do_authorization";

	/** The constant for historic transaction type. */
	public static final String PAYPAL_METHOD_DOCAPTURE ="do_capture";

	/** The constant for historic transaction type. */
	public static final String PAYPAL_METHOD_DOVOID ="do_void";

	/** The constant for historic transaction type. */
	public static final String PAYPAL_METHOD_QUERY ="query";
	/**
	 * Private constructor to stop instantiation of this class.
	 */
	private DataCashServiceConstantsForPayPalTransaction() {
		// Do nothing.
	}

}
