/*
* Copyright (C)2008 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
 * $RCSfile: Mode.java,v $
*
* $Revision: 1.3 $
*
* $Date: 2008-05-07 14:22:30 $
*
* $Author: thomas.pm $
*
* $Log: not supported by cvs2svn $
*/
package com.tui.uk.payment.service.datacash;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;

/**
 * The domain of DataCash operating modes.
 * The modes present currently are:
 * 1. TEST
 * 2. LIVE
 * 3. ACCREDITATION
 *
 * @author thomas.pm
 */
public enum Mode
{
   /** TEST mode of DataCash operation. */
   TEST("TEST"),
   /** LIVE mode of DataCash operation. */
   LIVE("LIVE"),
   /** ACCREDITATION mode of DataCash operation. */
   ACCREDITATION("ACCREDITATION");

   /** Holds the code for mode. */
   private final String code;


   /**
    * Initializes this enumeration with the mode type code.
    *
    * @param code the passenger type code
    */
   private Mode(String code)
   {
      this.code = code;
   }

   /**
    * Get the mode type code.
    *
    * @return the code.
    */
   public String getCode()
   {
      return this.code;
   }

   /**
    * Determine the mode for a specified code.
    *
    * @param requiredCode the code of the required mode.
    * @return mode the Mode.
    * @throws IllegalArgumentException if the code does not relate to a known
    *         mode.
    */
   public static Mode findByCode(String requiredCode)
   {
      for (Mode mode : values())
      {
         if (mode.getCode().equals(requiredCode))
         {
            return mode;
         }
      }
      String errorMessage = PropertyResource.getProperty("datacash.mode.unknown",
         MESSAGES_PROPERTY) + requiredCode;
      LogWriter.logErrorMessage(errorMessage);
      throw new IllegalArgumentException(errorMessage);
   }

   /**
    * Method to non serialize the class.
    *
    * @param out the ObjectOutputStream.
    * @throws IOException the IO Exception.
    */
   public final void writeObject(ObjectOutputStream out)throws IOException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty("class.non.serialized",
         MESSAGES_PROPERTY));
      throw new IOException(PropertyResource.getProperty("class.non.serialized",
         MESSAGES_PROPERTY));
   }

   /**
    * Method to non deserialize the class.
    *
    * @param inputStream the ObjectInputStream.
    * @throws IOException the IO Exception.
    */
   public final void readObject(ObjectInputStream inputStream)throws IOException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty("class.non.deserialized",
         MESSAGES_PROPERTY));
      throw new IOException(PropertyResource.getProperty("class.non.deserialized",
         MESSAGES_PROPERTY));
   }

}
