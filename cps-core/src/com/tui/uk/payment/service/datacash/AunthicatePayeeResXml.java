package com.tui.uk.payment.service.datacash;

import com.tui.uk.payment.service.datacash.CardResponse;
import com.datacash.util.XMLDocument;

/**
 * The class is used for mapping Authenticate Payer Response.
 * 
 * @author raghunandan.d
 *
 */
public class AunthicatePayeeResXml extends CardResponse {

	private String redirect_html;
	private String acs_transaction_id;
	private String ds_transaction_id;
	private String transaction_id;
	private String status_reason_code;
	private String protocol_version;
	private String acs_eci;
	private String authentication_status;
	private String transaction_status;
	private String authentication_token;
	private String authentication_version;
	private String result;
	private String datacash_reference;
	private String merchantreference;
	private String mode;
	private String reason;
	private String status;
	private String time;

	public AunthicatePayeeResXml(XMLDocument response) {
		super(response);
	}

	public String getRedirect_html() {
		return redirect_html;
	}

	public void setRedirect_html(String redirect_html) {
		this.redirect_html = redirect_html;
	}

	public String getAcs_transaction_id() {
		return acs_transaction_id;
	}

	public void setAcs_transaction_id(String acs_transaction_id) {
		this.acs_transaction_id = acs_transaction_id;
	}

	public String getDs_transaction_id() {
		return ds_transaction_id;
	}

	public void setDs_transaction_id(String ds_transaction_id) {
		this.ds_transaction_id = ds_transaction_id;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getStatus_reason_code() {
		return status_reason_code;
	}

	public void setStatus_reason_code(String status_reason_code) {
		this.status_reason_code = status_reason_code;
	}

	public String getProtocol_version() {
		return protocol_version;
	}

	public void setProtocol_version(String protocol_version) {
		this.protocol_version = protocol_version;
	}

	public String getAcs_eci() {
		return acs_eci;
	}

	public void setAcs_eci(String acs_eci) {
		this.acs_eci = acs_eci;
	}

	public String getAuthentication_status() {
		return authentication_status;
	}

	public void setAuthentication_status(String authentication_status) {
		this.authentication_status = authentication_status;
	}

	public String getTransaction_status() {
		return transaction_status;
	}

	public void setTransaction_status(String transaction_status) {
		this.transaction_status = transaction_status;
	}

	public String getAuthentication_token() {
		return authentication_token;
	}

	public void setAuthentication_token(String authentication_token) {
		this.authentication_token = authentication_token;
	}

	public String getAuthentication_version() {
		return authentication_version;
	}

	public void setAuthentication_version(String authentication_version) {
		this.authentication_version = authentication_version;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getDatacash_reference() {
		return datacash_reference;
	}

	public void setDatacash_reference(String datacash_reference) {
		this.datacash_reference = datacash_reference;
	}

	public String getMerchantreference() {
		return merchantreference;
	}

	public void setMerchantreference(String merchantreference) {
		this.merchantreference = merchantreference;
	}

	/*
	 * public String getMode() { return mode; } public void setMode(String mode)
	 * { this.mode = mode; }
	 */
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	/*
	 * public String getTime() { return time; } public void setTime(String time)
	 * { this.time = time; }
	 */

}
