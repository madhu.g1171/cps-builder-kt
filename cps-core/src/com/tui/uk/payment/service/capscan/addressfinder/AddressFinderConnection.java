/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AddressFinderConnection.java,v $
 *
 * $Revision: 1.0$
 *
 * $Date: 2008-06-23 08:08:25$
 *
 * $author: bibin.j@sonata-software.com$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.capscan.addressfinder;

import com.tui.uk.payment.exception.AddressFinderServiceException;
/**
 * This interface has a search method which returns the address details<code>Address</code> for
 * the given criteria(houseNo/postCode).
 *
 * @author bibin.j@sonata-software.com
 */
public interface AddressFinderConnection
{
   /**
    * The method which returns the address details<code>Address</code> for the given
    * criteria(houseNo/postCode).
    *
    * @param houseNo the house number.
    * @param postCode the post code.
    * @return Address the address.
    * @throws AddressFinderServiceException when connection with matchcode server fails.
    */
   Address search(String houseNo, String postCode) throws AddressFinderServiceException;
}
