/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: AddressConstants.java$
 *
 * $Revision: $
 *
 * $Date: May 23, 2008$
 *
 * $Author: bibin.j@sonata-software.com $
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.service.capscan.addressfinder;


/**
 * The interface AddressConstants.
 * It contains all the constants required for address finder service.
 *
 * @author bibin.j@sonata-software.com
 */
public final class AddressConstants
{
   /**
    * Private constructor to stop instantiation of this class.
    */
   private AddressConstants()
   {
      // Do nothing.
   }

   /**The constant indicating position of house name in address results array.*/
   public static final int HOUSENAME = 0;

   /**The constant indicating position of house number in address results array.*/
   public static final int HOUSENUM = 1;

   /**The constant indicating position of street name in address results array.*/
   public static final int STREET = 2;

   /**The constant indicating position of locality in address results array.*/
   public static final int LOCALITY = 3;

   /**The constant indicating position of town in address results array.*/
   public static final int TOWN = 4;

   /**The constant indicating position of county in address results array.*/
   public static final int COUNTY = 5;

   /**The constant indicating position of post code in address results array.*/
   public static final int POSTCODE = 6;

   /**The constant indicating ambiguous list count in address results array.*/
   public static final int AMBLISTCOUNT = 7;

   /**The constant indicating position of ambiguous list in address results array.*/
   public static final int AMBLIST = 8;

   /** The constant indicating list of address fields. */
   public static final String ADDRESSES = "addresses";

   /** The constant indicating the odd building number. */
   public static final String ODDS = "Odds";

   /** The constant indicating the even building number. */
   public static final String EVENS = "Evens";

   /** The constant indicating particular address field. */
   public static final String ADDRESS = "address";

   /** The constant indicating house name in address results array.*/
   public static final String HOUSE_NAME = "houseName";

   /** The constant indicating house number in address results array.*/
   public static final String HOUSE_NUMBER = "houseNumber";

   /** The constant indicating street in address results array.*/
   public static final String STREET_NAME = "street";

   /** The constant indicating locality in address results array.*/
   public static final String LOCALITY_NAME = "locality";

   /** The constant indicating town in address results array.*/
   public static final String TOWN_NAME = "town";

   /** The constant indicating county in address results array.*/
   public static final String COUNTY_NAME = "county";

   /** The constant indicating post code in address results array.*/
   public static final String POST_CODE = "postcode";

   /** The constant WITHOUT_BUILDING_NAME. */
   public static final String WITHOUT_BUILDING_NAME = "Without Building Name:";

   /** The constant WITHOUT_BUILDING_NUMBER. */
   public static final String WITHOUT_BUILDING_NUMBER = "Without Building Number:";
}