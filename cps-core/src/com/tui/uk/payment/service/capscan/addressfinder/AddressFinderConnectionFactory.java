/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AddressFinderConnectionFactory.java,v $
 *
 * $Revision: 1.0$
 *
 * $Date: 2008-06-23 08:08:25$
 *
 * $author: bibin.j@sonata-software.com$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.capscan.addressfinder;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.AddressFinderServiceException;
/**
 * This class gets the capscan connection.
 *
 * @author bibin.j@sonata-software.com
 */
public final class AddressFinderConnectionFactory
{
   /**
    * The default constructor to prevent instantiation.
    */
   private AddressFinderConnectionFactory()
   {
   }
   /**
    * Gets a suitable instance of AddressFinderConnection to use based on the connection
    * name passed.
    * AddressFinderServiceException is thrown when there is a failure in getting connection.
    *
    * @return connection a suitable instance of AddressFinderConnection.
    * @throws AddressFinderServiceException when connection with matchcode server fails.
    */
   public static AddressFinderConnection getConnection() throws AddressFinderServiceException
   {
      AddressFinderConnection connection = null;
      try
      {
         connection = (AddressFinderConnection) Class.forName(
            ConfReader.getConfEntry("capscan.Connection", null)).newInstance();
      }
      catch (ClassNotFoundException cnfe)
      {
         String message = PropertyResource.getProperty("capscan.addressfinderconnection.notfound",
            MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(message + cnfe.getMessage(), cnfe);
         throw new AddressFinderServiceException(message, cnfe);
      }
      catch (InstantiationException ie)
      {
         String message = PropertyResource.getProperty("capscan.addressfinderinstantiation.failed",
            MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(message + ie.getMessage(), ie);
         throw new AddressFinderServiceException(message, ie);
      }
      catch (IllegalAccessException iae)
      {
         String message = PropertyResource.getProperty("capscan.addressfinderaccess.failed",
            MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(message + iae.getMessage(), iae);
         throw new AddressFinderServiceException(message, iae);
      }
      return connection;
   }
}
