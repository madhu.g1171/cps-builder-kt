/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Address.java,v $
 *
 * $Revision: 1.0$
 *
 * $Date: 2008-06-23 08:08:25$
 *
 * $Author: bibin.j@sonata-software.com$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.capscan.addressfinder;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.io.CharArrayWriter;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.AddressFinderServiceException;
/**
 * This class performs following actions,
 * 1. Populate address from result set.
 * 2. Get address using house name, house number, street, locality, town, county and post code.
 *
 * @author bibin.j@sonata-software.com
 */

public final class Address
{
   /** The address. */
   private String address;

   /**
    * The constructor when invoked ,populates the address from given result set.
    * AddressFinderServiceException is thrown when there is a failure in fetching the results for
    * populating.
    *
    * @param resultSet the output results of the search from capscan.
    * @throws AddressFinderServiceException the Exception.
    */
   public Address(String... resultSet) throws AddressFinderServiceException
   {
     populateAddress(resultSet);
   }

   /**
    * Get the address.
    * @return address the address
    */
   public String getAddress()
   {
      return address;
   }

   /**
    * The method populate the address from given result set.
    * AddressFinderServiceException is thrown when there is a failure in fetching the results for
    * populating.
    *
    * @param resultSet the output results of the search from capscan.
    * @throws AddressFinderServiceException the Exception.
    */
   private void populateAddress(String... resultSet)
      throws AddressFinderServiceException
   {
      int count = 0;
      //Gets the count of ambiguous list.
      if (StringUtils.isNotBlank(resultSet[AddressConstants.AMBLISTCOUNT]))
      {
         count = Integer.parseInt(resultSet[AddressConstants.AMBLISTCOUNT]);
      }
      Element addresses = new Element(AddressConstants.ADDRESSES);
      if (count > 0)
      {
         // When ambiguous list count greater than zero, split the ambiguous list
         // based on '|' separator and store in a string array ambList.
         String[] ambList = StringUtils.split(resultSet[AddressConstants.AMBLIST], '|');

         for (String value : ambList)
         {
            //If ambList contains 'Odds' constant indicating the odd building number or
            //'Evens' constant indicating the even building number.
            if (value.contains(AddressConstants.ODDS) || value.contains(AddressConstants.EVENS))
            {
               //generates the sub elements for creating address from ambiguous list
               generateAddress(value, addresses, resultSet);
            }
            else
            {
               //creates the address from ambiguous list.
               createAddress(addresses, value, resultSet);
            }
         }
      }
      else if (StringUtils.isNotBlank(resultSet[AddressConstants.POSTCODE]))
      {
         createAddress(addresses, resultSet[AddressConstants.HOUSENUM], resultSet);
      }
      else
      {
         return;
      }
      processResults(addresses);
   }

   /**
    * The method gets the address based on house name, house number, street name, locality,
    * town, county and post code.
    *
    * @param hName the house name.
    * @param hNumber the houseNumber.
    * @param sName the street.
    * @param locality the locality.
    * @param townName the town.
    * @param countyName the county.
    * @param pCode the post code.
    * @return addresses the Element.
    */
   private Element getAddress(String hName, String hNumber, String sName, String locality,
      String townName, String countyName, String pCode)
   {
      Element addresses = new Element(AddressConstants.ADDRESS);
      Element houseName = new Element(AddressConstants.HOUSE_NAME).addContent(hName);
      Element houseNumber = new Element(AddressConstants.HOUSE_NUMBER).addContent(hNumber);
      Element street = new Element(AddressConstants.STREET_NAME).addContent(sName);
      Element townDistrict = new Element(AddressConstants.LOCALITY_NAME).addContent(locality);
      Element town = new Element(AddressConstants.TOWN_NAME).addContent(townName);
      Element county = new Element(AddressConstants.COUNTY_NAME).addContent(countyName);
      Element postcode = new Element(AddressConstants.POST_CODE).addContent(pCode);
      addresses.addContent(houseName);
      addresses.addContent(houseNumber);
      addresses.addContent(street);
      addresses.addContent(townDistrict);
      addresses.addContent(town);
      addresses.addContent(county);
      addresses.addContent(postcode);
      return addresses;
   }

   /**
    * This method generates the sub elements for creating address from ambiguous list.
    *
    * @param value the value from ambiguous list
    * @param addresses the Element.
    * @param resultSet the results of query.
    */
   private void generateAddress(String value, Element addresses, String... resultSet)
   {
      String[] subList = StringUtils.substringsBetween(value, "{", "}");
      String[] splitSubList = StringUtils.split(subList[AddressConstants.HOUSENAME], ',');
      int firstElement = Integer.parseInt(splitSubList[AddressConstants.HOUSENAME]);
      int lastElement = Integer.parseInt(splitSubList[splitSubList.length - 1]);
      for (int counter = firstElement; counter <= lastElement; counter = counter + 2)
      {
         createAddress(addresses, String.valueOf(counter), resultSet);
      }
   }

   /**
    * This method creates the address from ambiguous list.
    *
    * @param addresses the Element.
    * @param value the house number or house name.
    * @param resultSet the results of query.
    */
   private void createAddress(Element addresses, String value, String... resultSet)
   {
      String houseName = "";
      String houseNumber = "";
      if (value.contains(AddressConstants.WITHOUT_BUILDING_NAME))
      {
         houseNumber = resultSet[AddressConstants.HOUSENUM];
      }
      else if (value.contains(AddressConstants.WITHOUT_BUILDING_NUMBER))
      {
         houseName = resultSet[AddressConstants.HOUSENAME];
      }
      else
      {
         houseName = resultSet[AddressConstants.HOUSENAME];
         houseNumber = StringUtils.substringBefore(value, ":");
      }
      addresses.addContent(getAddress(houseName, houseNumber,
         resultSet[AddressConstants.STREET], resultSet[AddressConstants.LOCALITY],
            resultSet[AddressConstants.TOWN], resultSet[AddressConstants.COUNTY],
               resultSet[AddressConstants.POSTCODE]));
   }

   /**
    * This method is responsible for processing the results.
    * AddressFinderServiceException is thrown when there is a failure in fetching
    * the results for populating.
    *
    * @param addresses the Element.
    * @throws AddressFinderServiceException the address finder service exception.
    */
   private void processResults(Element addresses) throws AddressFinderServiceException
   {
      Document document = new Document(addresses);
      XMLOutputter outputter = new XMLOutputter();
      CharArrayWriter output = new CharArrayWriter();
      try
      {
          outputter.output(document, output);
      }
      catch (IOException ioe)
      {
         String message = PropertyResource.getProperty("capscan.results.processing.failed",
            MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(message + ioe.getMessage(), ioe);
         throw new AddressFinderServiceException(message, ioe);
      }
      LogWriter.logMethodEnd("processResponse");
      this.address = output.toString();
   }
}