/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: SymmetricDecrypter.java $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-03-31 10:19:55 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 *
 */
package com.tui.uk.service.security;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;

/**
 * This class defines methods for encryption and decryption of input using Triple DES
 * algorithm(Symmetric encryption).
 *
 * @author bibin.j
 *
 */
public final class SymmetricDecrypter implements Decrypter
{
   /** The default symmetric encryption algorithm. */
   private static final String ALGORITHM = "DESede/ECB/NoPadding";

   /** The symmetric encryption algorithm. */
   private String algorithm;

   /** The key  identifier. */
   private String identifier;

   /** This is a cache for keeping the keys in CPS key-store. */
   private static Map<String, Key> keyMap = new HashMap<String, Key>();

   /**
    * The constructor.
    *
    * @param keystoreType - Key store Type
    * @param keystoreFile - Key store filename
    * @param keystorePassword - key store password
    * @param algorithm - Algorithm
    * @param keyIdentifier - the secret key identifier
    */
   public SymmetricDecrypter(String keystoreType, String keystoreFile, String keystorePassword,
                        String algorithm, String keyIdentifier)
   {
      this.identifier = keyIdentifier;
      this.algorithm = algorithm;
      if (null == keyMap.get(keyIdentifier))
      {
         CPSKeyStore cpsKeyStore =
            new CPSKeyStore(keystoreType, keystoreFile, keystorePassword, keyIdentifier, null);
         keyMap.put(keyIdentifier, cpsKeyStore.getKey());
      }
   }

   /**
    * Use the specified TripleDES key to decrypt bytes ready from the input stream and write them
    * to the output stream.
    *
    * @param encryptedString the encrypted string that is to be decrypted.
    * @return decryptedString the decrypted value
    */
   public String decrypt(String encryptedString)
   {
      byte[] decryptedBytes;
      // Create and initialize the decryption engine and then decrypt the bytes
      try
      {
         Key symmetricKey = keyMap.get(identifier);
         if (null == this.algorithm)
         {
            this.algorithm = ALGORITHM;
         }
         Cipher cipher = Cipher.getInstance(algorithm);
         cipher.init(Cipher.DECRYPT_MODE, symmetricKey);
         byte[] encryptedBinary = HexEncoder.decode(encryptedString);
         decryptedBytes = cipher.doFinal(encryptedBinary);
      }
      catch (InvalidKeyException ikex)
      {
         String message = ikex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ikex);
         throw new RuntimeException(message, ikex);
      }
      catch (NoSuchAlgorithmException nsex)
      {
         String message = nsex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), nsex);
         throw new RuntimeException(message, nsex);
      }
      catch (NoSuchPaddingException nspex)
      {
         String message = nspex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), nspex);
         throw new RuntimeException(message, nspex);
      }
      catch (IllegalBlockSizeException ibex)
      {
         String message = ibex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ibex);
         throw new RuntimeException(message, ibex);
      }
      catch (BadPaddingException bpex)
      {
         String message = bpex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), bpex);
         throw new RuntimeException(message, bpex);
      }

      return new String(decryptedBytes);
   }
}
