/*
 * Copyright (c) 2008 sagkhx. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the GNU
 * Public License v2.0 which accompanies this distribution, and is
 * available at http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Contributors: sagkhx - initial API and implementation
 */
package com.tui.uk.service.security;

/**
 * Used to encode the given string.
 *
 * @author Kevin Holloway
 *
 */
public final class HexEncoder
{
   /** The constructor. */
   private HexEncoder()
   {
      // Stops instantiation of this class.
   }

   /** Holds the integer value TEN. */
   private static final int INT_VALUE_TEN = 10;

   /** Holds the integer value four. */
   private static final int INT_VALUE_FOUR = 4;

   /** Holds the HEXADECIMAL value 0xff. */
   private static final int HEXA_VALUE_0XFF = 0xff;

   /** Holds the HEXADECIMAL value 0x0f. */
   private static final int HEXA_VALUE_0X0F = 0x0f;

   /**
    * Method used to decode the HEXADECIMAL values by passing byte
    * parameter.
    *
    * @param input - byte parameter
    * @return in - integer value;
    */
   private static int decodeHex(byte input)
   {
      if (input >= 'a')
      {
         return (input - 'a') + INT_VALUE_TEN;
      }
      else if (input >= 'A')
      {
         return (input - 'A') + INT_VALUE_TEN;
      }
      else
      {
         return input - '0';
      }
   }

   /**
    * Method used to decode HEXADECIMAL values by passing byte array
    * parameter.
    *
    * @param input byte array
    * @return out byte array
    */

   private static byte[] decodeHex(byte[] input)
   {
      byte[] out = new byte[input.length / 2];
      for (int i = 0; i < input.length; i += 2)
      {
         int out0 = decodeHex(input[i]);
         int out1 = decodeHex(input[i + 1]);
         out[i / 2] = (byte)
         (((out0 << INT_VALUE_FOUR) | out1) & HEXA_VALUE_0XFF);
      }
      return out;
   }

   /**
    * Method used to encode the passed integer values.
    *
    * @param input integer value
    * @return in - integer value
    */
   private static int encodeHex(final int input)
   {
      if (input >= INT_VALUE_TEN)
      {
         return input + 'A' - INT_VALUE_TEN;
      }
      else
      {
         return input + '0';
      }
   }

   /**
    * Method is used to encode the passed byte array.
    *
    * @param input - byte array hexa decimal value
    *
    * @return encoded byte array.
    */

   private static byte[] encodeHex(final byte[] input)
   {
      byte[] out = new byte[input.length * 2];
      for (int i = 0; i < input.length; i++)
      {
         int out1 = (input[i] >> INT_VALUE_FOUR) & HEXA_VALUE_0X0F;
         int out2 = input[i] & HEXA_VALUE_0X0F;
         out[i * 2] = (byte) encodeHex(out1);
         out[i * 2 + 1] = (byte) encodeHex(out2);
      }
      return out;
   }

   /**
    * Method used to reverse the order of the passed byte array values.
    *
    * @param input - byte array parameter values
    *
    * @return out byte array
    */
   public static byte[] reverse(final byte[] input)
   {
      byte[] out = new byte[input.length];
      for (int i = 0; i < input.length; i++)
      {
         out[input.length - i - 1] = input[i];
      }
      return out;
   }

   /**
    * Method used to decode by passing String HEXADECIMAL value.
    *
    * @param hex - HEXADECIMAL value
    *
    * @return decoded value
    */
   public static byte[] decode(String hex)
   {
      return decodeHex(hex.getBytes());
   }

   /**
    * Method used to encode the HEXADECIMAL value, by passing HEXADECIMAL
    * byte array.
    *
    * @param hex the HEXADECIMAL to encode
    *
    * @return encoded- encoded string
    */
   public static String encode(byte[] hex)
   {
      byte[] encoded = encodeHex(hex);
      return new String(encoded);
   }
}
