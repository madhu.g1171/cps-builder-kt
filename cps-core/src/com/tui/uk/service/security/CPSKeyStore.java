/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CPSKeyStore.java $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-03-31 10:19:55 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 *
 */
package com.tui.uk.service.security;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;

/**
 * This class defines methods for loading a key store and getting the private key and secret keys.
 *
 * @author bibin.j
 *
 */
public final class CPSKeyStore
{
   /** The key store containing keys. */
   private KeyStore keyStore;

   /** A saved copy of the key store password. */
   private char[] password;

   /** A identifier to identify the secret key in key store. */
   private String identifier;

   /** The serial number. */
   private String serialNumber;

   /**
    * The constructor for populating password identifier and serialNumber.
    *
    * @param keystoreType - Key store type
    * @param keystoreFileName - Key store filename
    * @param keystorePassword - key store password
    * @param keyIdentifier - Key identifier
    * @param serialNum - Serial Number
    */
   public CPSKeyStore(String keystoreType, String keystoreFileName, String keystorePassword,
                      String keyIdentifier, String serialNum)
   {
      password = keystorePassword.toCharArray();
      identifier = keyIdentifier;
      serialNumber = serialNum;
      keyStore = openKeyStore(keystoreType, keystoreFileName);
   }

   /**
    * Opens a key store.
    *
    * @param keystoreType - the key store type. Typically this is "PKCS12".
    * @param keystoreFileName - the key store file name. The key store file must match the key store
    *        type.
    * @return an open key store.
    */
   private KeyStore openKeyStore(String keystoreType, String keystoreFileName)
   {
      FileInputStream keystoreStream = null;
      try
      {
         // Generally the key store type is "PKCS12".
         keyStore = KeyStore.getInstance(keystoreType);
         if (StringUtils.isBlank(keystoreFileName))
         {
            keystoreFileName = System.getProperty("keystorefile");
         }
         // For security, KeyStore wants certificate password as char[]
         File keystoreFile = new File(keystoreFileName);
         keystoreStream = new FileInputStream(keystoreFile);
         keyStore.load(keystoreStream, password);
         keystoreStream.close();
      }
      catch (KeyStoreException ksex)
      {
         String message = ksex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ksex);
         throw new RuntimeException(message, ksex);
      }
      catch (NoSuchAlgorithmException nsae)
      {
         String message = nsae.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), nsae);
         throw new RuntimeException(message, nsae);
      }
      catch (FileNotFoundException fnfe)
      {
         String message = fnfe.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), fnfe);
         throw new RuntimeException(message, fnfe);
      }
      catch (IOException ioe)
      {
         String message = ioe.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ioe);
         throw new RuntimeException(message, ioe);
      }
      catch (CertificateException ce)
      {
         String message = ce.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ce);
         throw new RuntimeException(message, ce);
      }
      finally
      {
         if (keystoreStream != null)
         {
            try
            {
               keystoreStream.close();
            }
            catch (IOException ioe)
            {
               LogWriter.logDebugMessage(ioe.getMessage(), ioe);
            }
         }
      }
      return keyStore;
   }

   /**
    * This method is used for getting the private key or secret key from key store.
    *
    * @return Key the private key or secret key.
    */
   public Key getKey()
   {
      Key key = null;
      KeyStore.PasswordProtection protection = new KeyStore.PasswordProtection(password);
      KeyStore.Entry entry;
      try
      {
         entry = keyStore.getEntry(identifier, protection);
         if (entry instanceof KeyStore.SecretKeyEntry)
         {
            KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) entry;
            key = secretKeyEntry.getSecretKey();
         }
         else if (entry instanceof KeyStore.PrivateKeyEntry)
         {
            Certificate cert = keyStore.getCertificate(identifier);
            // We only expect X.509 certificates, but there could be others.
            if (cert instanceof X509Certificate)
            {
               X509Certificate x509Cert = (X509Certificate) cert;
               // If the serial number matches, we have found the right
               // alias.
               // Using this we can get the key (which will be a private
               // key).
               if (x509Cert.getSerialNumber().equals(
                  BigInteger.valueOf(Long.parseLong(serialNumber))))
               {
                  KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) entry;
                  key = privateKeyEntry.getPrivateKey();
               }
            }
         }
      }
      catch (NoSuchAlgorithmException nsex)
      {
         String message = nsex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), nsex);
         throw new RuntimeException(message, nsex);
      }
      catch (UnrecoverableEntryException urex)
      {
         String message = urex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), urex);
         throw new RuntimeException(message, urex);
      }
      catch (KeyStoreException ksex)
      {
         String message = ksex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ksex);
         throw new RuntimeException(message, ksex);
      }
      return key;
   }
}
