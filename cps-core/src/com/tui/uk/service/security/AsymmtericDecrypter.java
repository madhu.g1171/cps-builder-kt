/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: RSADecrypter.java $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-03-31 10:19:55 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 *
 */
package com.tui.uk.service.security;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;

/**
 * This class defines methods for encryption and decryption of input using RSA algorithm
 * (Asymmetric encryption).
 *
 * @author bibin.j
 */
public final class AsymmtericDecrypter implements Decrypter
{
   /** The default symmetric encryption algorithm. */
   private static final String ALGORITHM = "RSA/ECB/PKCS1PADDING";

   /** The asymmetric encryption algorithm. */
   private String algorithm;

   /** The key identifier. */
   private String identifier;

   /** This is a cache for keeping the keys in CPS key-store. */
   private static Map<String, Key> keyMap = new HashMap<String, Key>();

   /**
    * The Constructor for populating identifier algorithm and cpsKeyStore.
    *
    * @param keystoreType - Key store Type
    * @param keystoreFileName - Key store filename
    * @param keystorePassword - key store password
    * @param algorithm - Algorithm
    * @param keyIdentifier - Alias name
    * @param serialNumber - Serial Number
    */
   public AsymmtericDecrypter(String keystoreType, String keystoreFileName,
                              String keystorePassword, String algorithm, String keyIdentifier,
                              String serialNumber)
   {
      this.identifier = keyIdentifier;
      this.algorithm = algorithm;
      if (null == keyMap.get(keyIdentifier))
      {
         CPSKeyStore cpsKeyStore =
            new CPSKeyStore(keystoreType, keystoreFileName, keystorePassword, keyIdentifier,
                            serialNumber);
         keyMap.put(keyIdentifier, cpsKeyStore.getKey());
      }
   }

   /**
    * Decrypt a small piece of encrypted content.
    *
    * @param encryptedContent - the encrypted content. This is an array of binary data-- it is not
    *        printable.
    *
    * @return decrypted card information
    */
   public String decrypt(String encryptedContent)
   {
      LogWriter.logMethodStart("decrypt");
      LogWriter.logInfoMessage("Decrypting the encrypted string");
      byte[] clearText = null;
      try
      {
         Key privateKey = keyMap.get(identifier);
         if (StringUtils.isBlank(this.algorithm))
         {
            this.algorithm = ALGORITHM;
         }
         Cipher cipher = Cipher.getInstance(algorithm);
         cipher.init(Cipher.DECRYPT_MODE, privateKey);
         // Reverse the order of the bytes. Microsoft reverses the bytes after encryption.
         byte[] encryptedBinary = HexEncoder.reverse(HexEncoder.decode(encryptedContent));
         // Decrypt the encrypted binary resulting in clear text.
         clearText = cipher.doFinal(encryptedBinary);
      }
      catch (InvalidKeyException ikex)
      {
         String message = ikex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ikex);
         throw new RuntimeException(message, ikex);
      }
      catch (IllegalBlockSizeException ibex)
      {
         String message = ibex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), ibex);
         throw new RuntimeException(message, ibex);
      }
      catch (NoSuchPaddingException nspex)
      {
         String message = nspex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), nspex);
         throw new RuntimeException(message, nspex);
      }
      catch (NoSuchAlgorithmException nsaex)
      {
         String message = nsaex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), nsaex);
         throw new RuntimeException(message, nsaex);
      }
      catch (BadPaddingException bpex)
      {
         String message = bpex.getMessage();
         LogWriter.logErrorMessage(PropertyResource.getProperty(message, MESSAGES_PROPERTY), bpex);
         throw new RuntimeException(message, bpex);
      }
      LogWriter.logMethodEnd("decrypt");
      LogWriter.logInfoMessage("Decryption succesfully completed");
      return new String(clearText);
   }

}
