/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DecrypterFactory.java $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-03-31 10:19:55 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 *
 */
package com.tui.uk.service.security;

import org.apache.commons.lang.StringUtils;

/**
 * This class defines methods for loading a key from map.
 *
 * @author bibin.j
 *
 */
public final class DecrypterFactory
{
   /**
    * Private constructor to avoid instantiation.
    */
   private DecrypterFactory()
   {
   }

   /**
    * This method searches for the keys in the map with the given the alias name or key identifier
    * and returns it.If key is not present it creates and stores in the cache.
    *
    * @param keystoreType - Key store Type
    * @param keystoreFileName - Key store filename
    * @param keystorePassword - key store password
    * @param algorithm - Algorithm
    * @param keyIdentifier - the secret key identifier
    * @param serialNumber - the serial number

    * @return the Decrypter.
    */
   public static Decrypter getDecrypter(String keystoreType, String keystoreFileName,
      String keystorePassword, String algorithm, String keyIdentifier, String serialNumber)
   {
      if (StringUtils.isBlank(serialNumber))
      {
        return new SymmetricDecrypter(keystoreType, keystoreFileName, keystorePassword,
             algorithm, keyIdentifier);
      }

        return new AsymmtericDecrypter(keystoreType, keystoreFileName, keystorePassword, algorithm,
           keyIdentifier, serialNumber);
   }
}
