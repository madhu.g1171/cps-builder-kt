/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: Decrypter.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2009-04-10 05:18:36 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 *
 */
package com.tui.uk.service.security;


/**
 * The Decrypter interface.Has method for decryption  of the encrypted string.
 *
 * @author bibin.j
 *
 */
public interface Decrypter
{
   /**
    * Use to decrypt the encrypted string.
    *
    * @param encryptedString the encrypted string that is to be decrypted.
    *
    * @return decryptedString the de-crypted value
    */
   String decrypt(String encryptedString);
}
