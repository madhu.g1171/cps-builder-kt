/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: ValidationException.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2009-06-22 08:25:26 $
 * 
 * $Author: roopesh.s@sonata-software.com $
 * 
 * 
 * $Log: $.
 */
package com.tui.uk.exception;

/**
 * This class is used for data validation exception.
 * 
 * @author roopesh.s
 * 
 */
@SuppressWarnings("serial")
public class ValidationException extends Exception
{
   /** The code associated with this exception. */
   private String code;
   
   /**
    * Default constructor for ValidationException class.
    */
   public ValidationException()
   {
      super();
   }

   /**
    * Constructor to create the exception by specifying a custom message.
    * 
    * @param message The custom message associated with this exception.
    */
   public ValidationException(String message)
   {
      super(message);
   }
   
   /**
    * Constructor to create this exception with a code and a message.
    * 
    * @param code the exception code.
    * @param message the exception message.
    */
   public ValidationException(String code, String message)
   {
      super(message);
      this.code = code;
   }

   /**
    * Defines the constructor for ValidationException with specified cause.
    * 
    * @param throwable - The specified cause.
    */
   public ValidationException(Throwable throwable)
   {
      super(throwable);
   }
   
   /**
    * Constructor to create this exception with a code and a throwable.
    * 
    * @param throwable - The specified cause for the exception.
    * @param code the exception code.
    */
   public ValidationException(Throwable throwable, String code)
   {
      super(throwable);
      this.code = code;
   }

   /**
    * Defines the constructor for ValidationException with detailed message
    * and specified cause.
    * 
    * @param message - The detailed exception message
    * @param throwable - The specified cause for the exception
    */
   public ValidationException(String message, Throwable throwable)
   {
      super(message, throwable);
   }
   
   /**
    * Returns the code of the exception.
    * 
    * @return the code associated with this exception.
    */
   public String getCode()
   {
      return code;
   }
}
