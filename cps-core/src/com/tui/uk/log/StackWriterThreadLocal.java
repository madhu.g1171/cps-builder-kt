/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: StackWriterThreadLocal.java$
 *
 * $Revision: 1.0$
 *
 * $Date: Jun 13, 2008$
 *
 * Author: VijayaLakshmi.d
 *
 *
 * $Log: $
 */
package com.tui.uk.log;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Provides thread local variable for writing stack trace.Improves performance by avoiding
 * synchronization.This is used in LoggerLog4jImpl.
 *
 * @author VijayaLakshmi.d
 *
 */
public final class StackWriterThreadLocal
{

   /** The constant used to hold the StringWriter class. */
   private StringWriter stringWriter;

   /** The constant used to hold the PrintWriter class. */
   private PrintWriter printWriter;

   /**
    * Private constructor to prevent instantiation.
    */
   private StackWriterThreadLocal()
   {
      stringWriter = new StringWriter();
      printWriter = new PrintWriter(stringWriter);
   }

   /** ThreadLoacal instance to get StringWriter & PrintWriter for each thread. */
   @SuppressWarnings("unchecked")
   private static ThreadLocal threadLocal = new ThreadLocal()
   {
      protected synchronized Object initialValue()
      {
         return new StackWriterThreadLocal();
      }

   };

   /**
    * Gets the PrintWriter Object.
    *
    * @return printWriter the PrintWriter Object.
    */
   public PrintWriter getPrintWriter()
   {
      return printWriter;
   }

   /**
    * Gets the StringWriter Object.
    *
    * @return stringWriter the StringWriter Object.
    */
   public StringWriter getStringWriter()
   {
      return stringWriter;
   }

   /**
    * Gets the thread local instance to get Writer Object.
    *
    * @return StackWriterThreadLocal.
    */
   public static StackWriterThreadLocal getWriter()
   {
      return (StackWriterThreadLocal) threadLocal.get();
   }

   /**
    * Release the system resources.
    */
   protected void finalize()
   {
      if (printWriter != null)
      {
         printWriter.close();
      }
   }

}
