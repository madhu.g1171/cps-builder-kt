package com.tui.uk.log;

/**
 * @author anup.g
 *interface for logging request response of various service of DD,Experian and Paypal
 */
public interface LoggerServiceFrameWork {

    public String logRequest(String request ,String dateFormat,String method,String logFolderPath);
    public void logResponse(String request ,String dateFormat,String method,String logFolderPath);

}
