/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: LoggerLog4jImpl.java$
 *
 * $Revision: $
 *
 * $Date: Jun 13, 2008$
 *
 * Author: VijayaLakshmi.d.
 *
 *
 *
 */
package com.tui.uk.log;

import java.util.Properties;

import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.spi.LoggingEvent;

import com.tui.uk.config.PropertyConstants;
import com.tui.uk.config.PropertyResource;

/**
 * Log4j implementation of the Logger.
 *
 * @author Vijayalakhsmi.dsonata-software.com
 */

public class LoggerLog4jImpl implements Logger
{
   /** The global logger Object to be used for logging. */
   private org.apache.log4j.Logger globalLogger;

   /** Fully Qualified Class Name. Will be used as the base name for configuration.*/
   private final String className;

   /**
    * Construct the logger.
    */
   public LoggerLog4jImpl()
   {
      className = LogWriter.class.getName();
      // obtain our main logger
      globalLogger = org.apache.log4j.Logger.getLogger(className);
      getDefaultConfiguration();
   }

   /**
    * Construct the logger.
    *
    * @param fullyQualifiedClassName the fully qualified class name to use as the base.
    */
   public LoggerLog4jImpl(String fullyQualifiedClassName)
   {
      if (fullyQualifiedClassName == null)
      {
         fullyQualifiedClassName = LoggerLog4jImpl.class.getName();
      }
      className = fullyQualifiedClassName;
      // obtain our main logger
      globalLogger = org.apache.log4j.Logger.getLogger(fullyQualifiedClassName);
      getDefaultConfiguration();
   }

   /**
    * Obtains the default configuration from ConfReader. If the behaviour of the log writer has
    * been changed, running this method will return the logger to it's default behaviour
    * ( as per ConfReader ).Entries that do not exist in ConfReader will be unchanged, and not
    * necessarily set to what they were when the LoggerLog4jImpl was first created.
    */
   public final synchronized void getDefaultConfiguration()
   {
      configure();
   }

   /**
    *  Build the category string ie., something like "DEBUG#,roll file".
    *  Here DATE prints date and time in the following format: "01 MAY 2002 09:15,123".
    */
   private void configure()
   {
      Properties props = PropertyResource.getAllProperties(PropertyConstants.LOG4J_PROPERTY);

      globalLogger.removeAllAppenders();
      new PropertyConfigurator().doConfigure(props, globalLogger.getLoggerRepository());
   }

   /**
    * This method logs this message and exception to either the global logger or a user
    * logger or not at all, as appropriate.
    *
    * @param priority hold the priority of logging as DEBUG or INFO level.
    * @param message message to be printed in the log
    * @param throwable the exception to be printed
    */
   public void logMessage(Level priority, String message, Throwable throwable)
   {
      logMessage(priority, message, throwable, null);
   }

   public void logMessage(Level priority, SOAPMessage soapMessage, Throwable throwable)
   {
      logMessage(priority, soapMessage, throwable, null);
   }

   /**
    * This method logs this message and exception to either the global logger or a user logger
    * or not at all, as appropriate.
    *
    * @param priority hold the priority of logging as DEBUG or INFO level.
    * @param message to be printed in the log
    * @param throwable the exception to be printed
    * @param caller if specified the implementation will not throw an exception to find out
    * who was the caller but it will use this value.
    */
   public void logMessage(Level priority, String message, Throwable throwable, String caller)
   {
      // this will either point to the user logger or the global logger or be null
      org.apache.log4j.Logger logger = getLogger();
      if (logger == null)
      {
         // we have no intention of logging anything
         return;
      }

      // check enabling, as would be done by calling logger.log()
      if (!logger.isEnabledFor(priority))
      {
         // escape, avoiding constructing the full message, if we don't plan to log it at all.
         return;
      }

      // prepend call point
      StringBuilder buf = new StringBuilder();
      buf.append("[");
      if (caller == null)
      {
         buf.append(getCaller());
      }
      else
      {
         buf.append(caller);
      }
      buf.append("] [").append(Thread.currentThread().getName()).append("] ").append(message);
      LoggingEvent evt = new LoggingEvent(className, logger, priority, buf.toString(), throwable);
      logger.callAppenders(evt);
   }

   public void logMessage(Level priority, SOAPMessage soapMessage, Throwable throwable, String caller)
   {
      // this will either point to the user logger or the global logger or be null
      org.apache.log4j.Logger logger = getLogger();
      if (logger == null)
      {
         // we have no intention of logging anything
         return;
      }

      // check enabling, as would be done by calling logger.log()
      if (!logger.isEnabledFor(priority))
      {
         // escape, avoiding constructing the full message, if we don't plan to log it at all.
         return;
      }

      // prepend call point
      StringBuilder buf = new StringBuilder();
      buf.append("[");
      if (caller == null)
      {
         buf.append(getCaller());
      }
      else
      {
         buf.append(caller);
      }
      buf.append("] [").append(Thread.currentThread().getName()).append("] ").append(soapMessage);
      LoggingEvent evt = new LoggingEvent(className, logger, priority, buf.toString(), throwable);
      logger.callAppenders(evt);
   }

   /**
    * Returns the logger to use, based on the state of <code>globalLoggingEnabled</code> However,
    * a <code>null</code> return value indicates that the logger is disabled.
    *
    * @return either a logger or null
    */
   private synchronized org.apache.log4j.Logger getLogger()
   {
      return globalLogger;
   }

   /**
    * This method return the class of the caller of this method.
    *
    * @return the class of the caller of this method.
    */
   private String getCaller()
   {
      String callStack;
      // share the string writer for same thread
      StackWriterThreadLocal writer = StackWriterThreadLocal.getWriter();
      new Throwable().printStackTrace(writer.getPrintWriter());
      callStack = writer.getStringWriter().toString();
      // reset to empty
      writer.getStringWriter().getBuffer().setLength(0);

      // the last couple of layers were probably internal to the logging.
      // So we want to skip the first few lines.
      int atPos = callStack.indexOf("at ");
      if (atPos < 0)
      {
         return "Call Site Unknown";
      }

      int atNext = callStack.indexOf("at ", atPos + 1);

      // skip over the top few entries that are from the log system.
      while (true)
      {
         if (atNext < 0)
         {
            break;
         }

         String string = callStack.substring(atPos, atNext);

         if (string.indexOf(".log.") < 0)
         {
            break;
         }

         // skip this call site
         atPos = atNext;
         atNext = callStack.indexOf("at ", atPos + 1);
      }

      // extract the class name, typically from "at foo.Bar.alpha( "
      int bPos = callStack.indexOf(" ( ", atPos);
      if (bPos < 0)
      {
         // otherwise look for a line ending
         bPos = callStack.indexOf('\n', atPos);
         if (bPos < 0)
         {
            // if all else fails, return the end of the stack trace
            return callStack.substring(atPos + 2).trim();
         }
      }

      // the expected case
      return callStack.substring(atPos + 2, bPos).trim();
   }

   /**
    * Method to log the debug message.
    *
    * @param message the message to be logged.
    */
   public void logDebugMessage(String message)
   {
      logMessage(Level.DEBUG, message, null);
   }

   public void logDebugMessage(SOAPMessage message)
   {
      logMessage(Level.DEBUG, message, null);
   }
   /**
    * Method to log the debug message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logDebugMessage(String message, Throwable throwable)
   {
      logMessage(Level.DEBUG, message, throwable);
   }

   /**
    * Method to log the debug message.
    *
    * @param message the message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    */
   public void logDebugMessage(String message, String fullyQualifiedClassName)
   {
      getLogger().log(Level.DEBUG, message);
   }

   /**
    * Method to log the debug message with the exception.
    *
    * @param message message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    * @param throwable the exception to be logged.
    */
   public void logDebugMessage(String message, String fullyQualifiedClassName, Throwable throwable)
   {
      getLogger().log(Level.DEBUG, message, throwable);
   }

   /**
    * Method to log the info message.
    *
    * @param message the message to be logged.
    */
   public void logInfoMessage(String message)
   {
      logMessage(Level.INFO, message, null);
   }

   /**
    * Method to log the info message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logInfoMessage(String message, Throwable throwable)
   {
      logMessage(Level.INFO, message, throwable);
   }

   /**
    * Method to log the info message.
    *
    * @param message the message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    */
   public void logInfoMessage(String message, String fullyQualifiedClassName)
   {
      getLogger().log(Level.INFO, message);
   }

   /**
    * Method to log the info message with the exception.
    *
    * @param message message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    * @param throwable the exception to be logged.
    */
   public void logInfoMessage(String message, String fullyQualifiedClassName, Throwable throwable)
   {
      getLogger().log(Level.INFO, message, throwable);
   }

   /**
    * Method to log the warning message.
    *
    * @param message the message to be logged.
    */
   public void logWarningMessage(String message)
   {
      logMessage(Level.WARN, message, null);
   }

   /**
    * Method to log the warning message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logWarningMessage(String message, Throwable throwable)
   {
      logMessage(Level.WARN, message, throwable);
   }

   /**
    * Method to log the warning message.
    *
    * @param message the message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    */
   public void logWarningMessage(String message, String fullyQualifiedClassName)
   {
      getLogger().log(Level.WARN, message);
   }

   /**
    * Method to log the warning message with the exception.
    *
    * @param message message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    * @param throwable the exception to be logged.
    */
   public void logWarningMessage(String message, String fullyQualifiedClassName,
      Throwable throwable)
   {
      getLogger().log(Level.WARN, message, throwable);
   }

   /**
    * Method to log the error message.
    *
    * @param message the message to be logged.
    */
   public void logErrorMessage(String message)
   {
      logMessage(Level.ERROR, message, null);
   }

   /**
    * Method to log the error message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logErrorMessage(String message, Throwable throwable)
   {
      logMessage(Level.ERROR, message, throwable);
   }

   /**
    * Method to log the error message.
    *
    * @param message the message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    */
   public void logErrorMessage(String message, String fullyQualifiedClassName)
   {
      getLogger().log(Level.ERROR, message);
   }

   /**
    * Method to log the error message with the exception.
    *
    * @param message message to be logged.
    * @param fullyQualifiedClassName the fully qualified class name for which separate logging is
    *    required.
    * @param throwable the exception to be logged.
    */
   public void logErrorMessage(String message, String fullyQualifiedClassName, Throwable throwable)
   {
      getLogger().log(Level.ERROR, message, throwable);
   }

}
