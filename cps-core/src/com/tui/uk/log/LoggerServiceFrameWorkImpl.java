package com.tui.uk.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tui.uk.payment.service.datacash.DataCashServiceConstants;



/**
 * @author anup.g
 * implementation class of LoggerServiceFrameWork interface which
 *         give implementation of the logRequest and logResponse methods
 */
public class LoggerServiceFrameWorkImpl implements LoggerServiceFrameWork {
    /**
     * Logger
     */
    private Logger logger = LogWriter.getLogger(LoggerServiceFrameWorkImpl.class
            .getName());;

    /**
     *
     *implementation of  logRequest  method
     */
    @Override
    public String logRequest(String request, String dateFormat,
            String method ,String logFolderPath) {
        Date date = new Date();
        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        String currentDate = dateFormatter.format(date);
        String prefix = "request";
        String formattedFileName = getFileName(method, currentDate,
                prefix);
        writeInFileSystem(logFolderPath,method,formattedFileName,request);
        return currentDate;
    }


    private String getFileName(String method, String currentDate,
            String prefix) {
        String formattedCurrentDate = currentDate.replace(" ", DataCashServiceConstants.UNDER_SCORE);
        StringBuilder sb = new StringBuilder(method);
        String fileName = sb.append(DataCashServiceConstants.UNDER_SCORE).append(prefix).toString();
        String formattedFileName = fileName.concat(DataCashServiceConstants.UNDER_SCORE+formattedCurrentDate)
                .replace(":", DataCashServiceConstants.UNDER_SCORE);

        return formattedFileName;

    }

    private void writeInFileSystem(String logFolderPath,String method,String formattedFileName,String request){

        File f = new File(logFolderPath + method + "/" + formattedFileName
                + ".txt");

        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (Exception e) {
                logger.logInfoMessage(e.getLocalizedMessage());
            }
        }
        try {
            // dir will change directory and specifies file name for writer
            File dir = new File(f.getParentFile(), f.getName());
            PrintWriter writer = new PrintWriter(dir);
            writer.print(request);
            writer.close();
        } catch (FileNotFoundException e) {
            logger.logInfoMessage(e.getLocalizedMessage());
        }

    }

    /**
     *
     *implementation of  logResponse  method
     */
    @Override
    public void logResponse(String request, String currentDate,
            String method,String logFolderPath) {
        String prefix = "response";
        String formattedFileName = getFileName(method, currentDate,
                prefix);
        writeInFileSystem(logFolderPath,method,formattedFileName,request);

    }

}
