package com.tui.uk.cacheeventlistener;

import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;

/**
 * This is an utility class for logging and updating the TransactionStatus @each stage of the
 * transaction life cycle.
 * 
 * @author saleem shaik
 * 
 */
public class TransactionStatusUtil
{

   /*
    * updateStatus is a method to update the TransactionStatus in each stage of the transaction
    * paymentData and TransactionStatus would be passed as arguments
    */

   /** TransactionStatus. */
   private TransactionStatus transactionStatus;

   /** COMMA. */
   private static final String COMMA = ",";

   /**
    * Update Status.
    * 
    * @param oldStatus oldStatus.
    * @param newStatus newStatus.
    * @param paymentData paymentData
    * @param token token.
    */
   public void updateStatus(PaymentData paymentData, String token, TransactionStatus oldStatus,
      TransactionStatus newStatus)
   {

      paymentData.setTransactionStatus(newStatus);

      logTransactionStatus(token, oldStatus, paymentData.getTransactionStatus());
   }

   /**
    * logTransactionStatus is a method to update the TransactionStatus in
    * 
    * each stage of the transaction token,olsStatus and newStatus would be passed as arguments.
    * 
    * @param token the token.
    * @param oldStatus the oldStatus.
    * @param newStatus the newStatus.
    */
   public void logTransactionStatus(String token, TransactionStatus oldStatus,
      TransactionStatus newStatus)
   {
      StringBuilder transactionStatusLog = new StringBuilder();
      transactionStatusLog.append(token).append(COMMA).append(oldStatus)
      .append(COMMA).append(newStatus);
      LogWriter.logInfoMessage(transactionStatusLog.toString(), 
      TransactionStatusUtil.class.getName());
   }

   /**
    * 
    * Transaction status.
    * 
    * @return transactionStatus transactionStatus.
    * 
    */

   public TransactionStatus getTransactionStatus()
   {
      return transactionStatus;
   }

   /**
    * 
    * Transaction status.
    * 
    * @param transactionStatus transactionStatus.
    * 
    */
   public void setTransactionStatus(TransactionStatus transactionStatus)
   {
      this.transactionStatus = transactionStatus;
   }

}
