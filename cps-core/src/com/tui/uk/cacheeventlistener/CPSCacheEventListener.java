package com.tui.uk.cacheeventlistener;

import java.util.UUID;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;
import org.apache.commons.lang.StringUtils;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;

/**
 * This is a CPSCacheEventListener class for notifyElementExpired if transaction status is
 * PAYMENT_PRE_AUTH_SUCCESS FRAUD_SCREENING_PRE_AUTH_SUCCESS and PAYMENT_FULFILL_FAILURE.
 *
 * @author saleembasha.s
 */
public class CPSCacheEventListener implements CacheEventListener
{
   /** The PAYMENT. */
   private static final String PAYMENT = "payment.";

   /** The DEFAULT_VALUE. */
   private static final String DEFAULT_VALUE = "default";

   /** The DATA_CASH_ERROR. */
   private static final String DATA_CASH_ERROR = ".datacash.error";

   /**
    *
    * notifyElementExpired.
    *
    * @param cache the cache component.
    * @param element element.
    *
    */
   public void notifyElementExpired(Ehcache cache, Element element)
   {

      PaymentData paymentData = (PaymentData) element.getObjectValue();
      /*
       * Check if we got valid payment objects.
       */
      if (paymentData == null || paymentData.getPayment() == null)
      {
      LogWriter.logInfoMessage("Got null payment data elements " 
      + "in notifyElementExpired method for token : "
      + element.getObjectKey().toString());
       return;
      }
      BookingInfo bookingInfo = paymentData.getBookingInfo();
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();

      if (paymentData.getTransactionStatus().toString()
         .equalsIgnoreCase("PAYMENT_PRE_AUTH_SUCCESS")
         || paymentData.getTransactionStatus().toString()
            .equalsIgnoreCase("FRAUD_SCREENING_PRE_AUTH_SUCCESS")
         || paymentData.getTransactionStatus().toString()
            .equalsIgnoreCase("PAYMENT_FULFILL_FAILURE"))
      {
         try
         {
            DataCashPaymentTransaction dataCashPaymentTransaction = null;
            for (PaymentTransaction paymentTransaction : paymentData.getPayment()
               .getPaymentTransactions(PaymentMethod.DATACASH))
            {
               LogWriter.logInfoMessage("Auto Cancel method started for the token : "
                  + element.getObjectKey().toString());
               dataCashPaymentTransaction = (DataCashPaymentTransaction) paymentTransaction;
               dataCashPaymentTransaction.setTrackingData(bookingInfo.getTrackingData().toString());
               String vTid = bookingComponent.getPaymentGatewayVirtualTerminalId();
               if (vTid != null)
               {
                  dataCashPaymentTransaction.cancel(vTid);
                  new TransactionStatusUtil().updateStatus(paymentData, element.getObjectKey()
                     .toString(), paymentData.getTransactionStatus(), TransactionStatus
                     .findByCodeTS("AUTO_CANCEL_SUCCESS"));
                  LogWriter.logInfoMessage("Auto Cancel completed for the client "
                     + bookingComponent.getClientApplication().getClientApplicationName()
                     + " and the token : " + element.getObjectKey().toString());
               }
               else
               {
                  new TransactionStatusUtil().updateStatus(paymentData, element.getObjectKey()
                     .toString(), paymentData.getTransactionStatus(), TransactionStatus
                     .findByCodeTS("AUTO_CANCEL_FAILURE"));
                  LogWriter.logInfoMessage("Auto Cancel Failure due to vTid null for the client "
                     + bookingComponent.getClientApplication().getClientApplicationName()
                     + " and the token : " + element.getObjectKey().toString());
               }
            }
         }
         catch (DataCashServiceException dcse)
         {
            LogWriter.logErrorMessage(dcse.getMessage(), dcse);
            String key = PAYMENT + DEFAULT_VALUE + DATA_CASH_ERROR;
            if (StringUtils.isNotBlank(bookingComponent.getClientApplication()
               .getClientApplicationName()))
            {
               key =
                  PAYMENT + bookingComponent.getClientApplication().getClientApplicationName()
                     + DATA_CASH_ERROR;
            }
            new TransactionStatusUtil().updateStatus(paymentData,
               element.getObjectKey().toString(), paymentData.getTransactionStatus(),
               TransactionStatus.findByCodeTS("AUTO_CANCEL_FAILURE"));
            // throw new PaymentValidationException(dcse.getCode(), key,
            // dcse);
         }
      }
      new TransactionStatusUtil().updateStatus(paymentData, element.getObjectKey().toString(),
         paymentData.getTransactionStatus(), TransactionStatus.findByCodeTS("TOKEN_REMOVED"));

   }

   /**
    *
    * notifyElementEvicted.
    *
    * @param cache the cache component.
    * @param element element.
    *
    */
   public void notifyElementEvicted(Ehcache cache, Element element)
   {
      LogWriter.logInfoMessage("notifyElementEvicted");
   }

   /**
    *
    * dispose.
    *
    * @throws CacheException CacheException.
    *
    */
   public void dispose() throws CacheException
   {
      // TODO Auto-generated method stub

   }

   /**
    *
    * notifyElementPut.
    *
    * @param arg0 the arg0 component.
    * @param arg1 the arg1 component.
    * @throws CacheException CacheException.
    *
    */
   public void notifyElementPut(Ehcache arg0, Element arg1) throws CacheException
   {
      // TODO Auto-generated method stub

   }

   /**
    *
    * notifyElementPut.
    *
    * @param arg0 the arg0 component.
    * @param arg1 the arg1 component.
    * @throws CacheException CacheException.
    *
    */
   public void notifyElementRemoved(Ehcache arg0, Element arg1) throws CacheException
   {
      // TODO Auto-generated method stub

   }

   /**
    *
    * notifyElementUpdated.
    *
    * @param arg0 the arg0 component.
    * @param arg1 the arg1 component.
    * @throws CacheException CacheException.
    *
    */
   public void notifyElementUpdated(Ehcache arg0, Element arg1) throws CacheException
   {
      // TODO Auto-generated method stub

   }

   /**
    *
    * notifyRemoveAll.
    *
    * @param arg0 the arg0 component.
    *
    */
   public void notifyRemoveAll(Ehcache arg0)
   {
      // TODO Auto-generated method stub

   }

   /**
    *
    * notifyElementPut.
    *
    * @return null null.
    *
    */
   public Object clone()
   {
      return null;

   }

}
