package com.tui.uk.cacheeventlistener;

/**
 * This is a enum class for TransactionStatus.
 * 
 * @author saleembasha.s
 * 
 */
public enum TransactionStatus
{

   /** GenerateToken. */
   TOKEN_GENERATED("TOKEN_GENERATED"),

   /** GenerateToken2. */
   TOKEN_GENERATED2("TOKEN_GENERATED2"),

   /** generateTokenForRefund. */
   TOKEN_GENERATED_REFUND("TOKEN_GENERATED_REFUND"),

   /** BINValidation. */
   BIN_VALIDATION_SUCCESS("BIN_VALIDATION_SUCCESS"),

   /** BINValidation. */
   BIN_VALIDATION_FAILURE("BIN_VALIDATION_FAILURE"),

   /** authPayment. */
   PAYMENT_PRE_AUTH_SUCCESS("PAYMENT_PRE_AUTH_SUCCESS"),

   /** authPayment. */
   PAYMENT_PRE_AUTH_FAILURE("PAYMENT_PRE_AUTH_FAILURE"),

   /** cardEnrolmentVerfication. */
   CARD_ENROLLMENT_VERIFICATION("CARD_ENROLLMENT_VERIFICATION"),

   /** authorize3DPayment. */
   THREED_PAYMENT_SUCCESS("THREED_PAYMENT_SUCCESS"),

   /** authorize3DPayment. */
   THREED_PAYMENT_FAILURE("THREED_PAYMENT_FAILURE"),

   /** cancel. */
   TRANSACTION_CANCEL("TRANSACTION_CANCEL"),

   /** Redirect to PaymentURL. */
   REDIRECT_TO_PAYMENT_URL("REDIRECT_TO_PAYMENT_URL"),

   /** Redirect to Post PaymentURL. */
   REDIRECT_TO_POST_PAYMENT_URL("REDIRECT_TO_POST_PAYMENT_URL"),

   /** preRegisteredRefund. */
   PRE_REFUND_SUCCESS("PRE_REFUND_SUCCESS"),

   /** preRegisteredRefund. */
   PRE_REFUND_FAILURE("PRE_REFUND_FAILURE"),

   /** processFraudScreening. */
   FRAUD_SCREENING_PRE_AUTH_SUCCESS("FRAUD_SCREENING_PRE_AUTH_SUCCESS"),

   /** processFraudScreening. */
   FRAUD_SCREENING_PRE_AUTH_FAILURE("FRAUD_SCREENING_PRE_AUTH_FAILURE"),

   /** processFraudScreening. */
   FRAUD_SCREENING_FULFILL_SUCCESS("FRAUD_SCREENING_FULFILL_SUCCESS"),

   /** processFraudScreening. */
   FRAUD_SCREENING_FULFILL_FAILURE("FRAUD_SCREENING_FULFILL_FAILURE"),

   /** fulfill. */
   PAYMENT_FULFILL_SUCCESS("PAYMENT_FULFILL_SUCCESS"),

   /** fulfill. */
   PAYMENT_FULFILL_FAILURE("PAYMENT_FULFILL_FAILURE"),

   /** getEssentialTransactionData. */
   ESSENTIAL_TRANSACTION_DATA_RETURNED("ESSENTIAL_TRANSACTION_DATA_RETURNED"),

   /** get PaymentData. */
   PAYMENTDATA_RETURNED("PAYMENTDATA_RETURNED"),

   /** get Non PaymentData. */
   NON_PAYMENTDATA_RETURNED("NON_PAYMENTDATA_RETURNED"),

   /** purgeSensitiveData. */
   PAYMENTDATA_PURGED("PAYMENTDATA_PURGED"),

   /** Auto Cancel success. */
   AUTO_CANCEL_SUCCESS("AUTO_CANCEL_SUCCESS"),

   /** Auto Cancel Failure. */
   AUTO_CANCEL_FAILURE("AUTO_CANCEL_FAILURE"),

   /** Auto Cancel Failure. */
   TOKEN_REMOVED("TOKEN_REMOVED"),

   /** notifyBookingCompletion. */
   BOOKING_COMPLETED("BOOKING_COMPLETED"),
   
   /** notifyBookingCompletion. */
   PRE_REGISTERED_REFUND_FAILURE("PRE_REGISTERED_REFUND_FAILURE"),
   
   /** PRE_REGISTERED_REFUND_SUCCESS. */
   PRE_REGISTERED_REFUND_SUCCESS("PRE_REGISTERED_REFUND_SUCCESS"),
   
   EMV_INITIATE_AUTH_IN_PROGRESS("EMV_INITIATE_AUTH_IN_PROGRESS"),
   
   EMV_INITIATE_AUTH_FAILED("EMV_INITIATE_AUTH_FAILED"),
   
   EMV_INITIATE_AUTH_COMPLETED("EMV_INITIATE_AUTH_COMPLETED"),
   
   EMV_INITIATE_AUTH_FALLBACK("EMV_INITIATE_AUTH_FALLBACK"),

   EMV_AUTH_INPROGRESS("EMV_AUTH_INPROGRESS"),

   EMV_CHALLENGE_FLOW_INPROGRESS("EMV_CHALLENGE_FLOW_INPROGRESS"),

   EMV_CHALLENGE_FLOW_COMPLETED("EMV_CHALLENGE_FLOW_COMPLETED"),

   EMV_FRICTIONLESS_FLOW_INPROGRESS("EMV_FRICTIONLESS_FLOW_INPROGRESS"),
   
   EMV_FRICTIONLESS_FLOW_COMPLETED("EMV_FRICTIONLESS_FLOW_COMPLETED"),
   
   PAYMENT_PRE_AUTH_INPROGRESS("PAYMENT_PRE_AUTH_INPROGRESS"),
   
   EMV_INITIATE_AUTH_RETRY_IN_PROGRESS("EMV_INITIATE_AUTH_RETRY_IN_PROGRESS");




   /** transactionStatus. */
   private String transactionStatus;

   /**
    * TransactionStatus.
    * 
    * @param transactionStatus the transactionStatus.
    */
   private TransactionStatus(String transactionStatus)
   {
      this.transactionStatus = transactionStatus;

   }

   /**
    * Retrieve the transactionStatus.
    * 
    * @return transactionStatus transactionStatus
    */
   public String getTransactionStatus()
   {
      return transactionStatus;
   }

   /**
    * Determine the TransactionStatus.
    * 
    * @param transactionStatusRequired transactionStatusRequired.
    * 
    * @return transactionStatus transactionStatus.
    * 
    */
   public static TransactionStatus findByCodeTS(String transactionStatusRequired)
   {
      for (TransactionStatus transactionStatus : values())
      {
         if (transactionStatus.getTransactionStatus().equals(transactionStatusRequired))
         {
            return transactionStatus;
         }
      }
      throw new IllegalArgumentException("Unknown Transaction Status :" 
      + transactionStatusRequired);
   }

}
