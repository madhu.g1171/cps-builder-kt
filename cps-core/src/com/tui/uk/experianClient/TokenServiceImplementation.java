package com.tui.uk.experianClient;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.startup.ExperianTokenJobRun;
import com.tui.uk.startup.ExperianTokenManagement;

public class TokenServiceImplementation {

    public Logger logger;

    public TokenServiceImplementation() {
        logger = LogWriter
                .getLogger(TokenServiceImplementation.class.getName());

    }

    /**
     * This method gets the Experian token required to access BankWizard API of
     * Experian
     *
     * @return String token
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public String getExperianToken() throws IOException, ClassNotFoundException {

        String tokenValue = null;

        final Object experianTokenData = PaymentStore.getInstance()
                .getExperianTokenData("experianToken");

        if (null != experianTokenData) {
            final ExperianTokenManagement tokenData = (ExperianTokenManagement) experianTokenData;
            logger.logInfoMessage(tokenData.getExperianToken()
                    + "===================================Experian token retrieved from Cache =========================================");
            tokenValue = tokenData.getExperianToken();
        }

        if(StringUtils.isBlank(tokenValue)){

        	final ExperianTokenManagement experianTokenManageObj = getExperianTokenFromFile();
	        final String token = experianTokenManageObj.getExperianToken();
	        logger.logInfoMessage("===================================Experian token retrieved from tokenFile ========================================="+token);

	        if (StringUtils.isEmpty(token)) {
	            logger.logInfoMessage(" Get new token from Token service because either file doesn't exists or file don't have token ");

	            tokenValue = updateSystemWithNewToken();
	            logger.logInfoMessage("===================================Experian token retrieved from new tokenService request ========================================="+token);
	        } else {
	                if (isTokenExpired()) {
	                	tokenValue = updateSystemWithNewToken();
	                	logger.logInfoMessage("===================================Experian token retrieved from new tokenService request when token in tokenFile was expired ========================================="+token);
	                }
	                // Assign the token to tokenValue if token retrieved from file is
	                // not expired
	                tokenValue = token;
	        }
        }
        return tokenValue;

    }

    /**
     * This method stores the token in PaymentCache
     * @param experianTokenManagementObj
     */
    private void storeTokenInCache(
            ExperianTokenManagement experianTokenManagementObj) {

        PaymentStore.getInstance().experianTokenData("experianToken",
                experianTokenManagementObj);

    }

    /**
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private ExperianTokenManagement getExperianTokenFromFile()
            throws IOException, ClassNotFoundException {

        File file = new File(ConfReader.getConfEntry("experian.tokenFile", "")
                + "/" + "tokenFile.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);

        logger.logInfoMessage("reading from file if cache is null");
        ExperianTokenManagement tokenFromFile = null;

        try {
            tokenFromFile = (ExperianTokenManagement) ois.readObject();

        } catch (EOFException ex) {
          logger.logInfoMessage("EOFException occured"+ex);
        }
         catch (IOException ex) {
             logger.logInfoMessage("IOException occured"+ex);
        }

        finally {
            ois.close();
            fis.close();
        }

        return tokenFromFile;
    }

    /**
     *  This method writes the provided token data to file
     *
     * @param experianTokenManagementObj
     */
    private void writeTokenToFile(
            final ExperianTokenManagement experianTokenManagementObj) {

        File file = new File(ConfReader.getConfEntry("experian.tokenFile", "")
                + "/" + "tokenFile.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.logInfoMessage(
                        "Exception while creating the  tokenFile.txt file ", e);
            }
        }

        FileOutputStream fout = null;
        ObjectOutputStream oos = null;

        try {
            fout = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            logger.logInfoMessage("tokenFile.txt not found", e);
        }
        try {
            oos = new ObjectOutputStream(fout);
            oos.writeObject(experianTokenManagementObj);

        } catch (IOException e) {
            logger.logInfoMessage(
                    "Exception occured while writing the data to file ", e);
        } finally {
            try {
                oos.flush();
                oos.close();
            } catch (IOException e) {
                logger.logInfoMessage(
                        "Exception occured while closing the stream ", e);
            }
        }

        logger.logInfoMessage("writing to file and cache if cache,file is null");

    }

    /**
     * Checks if the token present is file is expired or not
     * @return boolean
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private boolean isTokenExpired() throws ClassNotFoundException, IOException {

        logger.logInfoMessage("Reading token from file and check if token is still valid ");

        final ExperianTokenManagement tokenFromFile = getExperianTokenFromFile();

        final Date lastDate = tokenFromFile.getLastDate();

        final Date currentDate = new Date();

        final long tokenGenerationIntervalInMillis = ConfReader.getLongEntry(
                "experian.generate.token.interval", 27000000);
        final long different = currentDate.getTime() - lastDate.getTime();

        return different > tokenGenerationIntervalInMillis;

    }

    /**
     *
     * @return String token
     */
    private String updateSystemWithNewToken() {

        logger.logInfoMessage("Generate new token and update the new token in cache as well as in file ");

        ExperianTokenJobRun tokenJob = new ExperianTokenJobRun();
        tokenJob.run();
        String experianToken = null;
        final Object experianTokenData = PaymentStore.getInstance()
                .getExperianTokenData("experianToken");

        if (null != experianTokenData) {
            final ExperianTokenManagement tokenData = (ExperianTokenManagement) experianTokenData;
            logger.logInfoMessage(tokenData.getExperianToken()
                    + "===================================Experian token retrieved from Cache =========================================");
            experianToken = tokenData.getExperianToken();
        }

       /* final TokenServiceRequestProcessor tokenServiceRequestProcessor = new TokenServiceRequestProcessor();
        String experianToken = tokenServiceRequestProcessor.getToken();

        if(StringUtils.isNotBlank(experianToken)){
        	logger.logInfoMessage("Got the new token.. store it in cache as well as as in tokenFile...");
        	   final ExperianTokenManagement experianTokenManagementObj = new ExperianTokenManagement(
                       experianToken, new Date());

               writeTokenToFile(experianTokenManagementObj);

               storeTokenInCache(experianTokenManagementObj);
        }*/

        return experianToken;
    }

}
