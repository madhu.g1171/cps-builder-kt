package com.tui.uk.experianClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Base64;
import org.apache.commons.lang.StringUtils;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;
import com.tui.uk.log.LoggerServiceFrameWork;
import com.tui.uk.log.LoggerServiceFrameWorkImpl;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;

/**
 * process the token request.
 *
 * @author Anup.g
 *
 * @since 1.0 API
 */
public class TokenServiceRequestProcessor {

	/**
	 * Logger
	 */
    private Logger logger = LogWriter.getLogger(TokenServiceRequestProcessor.class
            .getName());;

	/**
	 * reads the tokenService end-point URL from conf file
	 */
	private String tokenServiceEndPointURl = ConfReader.getConfEntry(
			"experian.tokenServiceEndPointURl", null);
	/**
     * the dateformat to be appended in the logfiles
     */
	private String currentDate = null;

	/**
	 * Token Service Soap Action
	 */
	private String soapAction = ConfReader.getConfEntry("experian.tokenService.soapAction", "http://www.uk.experian.com/WASP/STS");
	/**
     * Token Service log file path
     */
	private String logFolderPath = ConfReader.getConfEntry("experian.log.folderPath", "./logs/directDebit/experian/");
	/**
     * Token Service log enabled/disabled
     */
	private String loggerSwitch = ConfReader.getConfEntry("tokenService.log.switch", "OFF");

	private TokenServiceResponsePopulator tokenServiceResponsePopulator;
	private LoggerServiceFrameWork loggerServiceFrameWork = new LoggerServiceFrameWorkImpl();


	public TokenServiceRequestProcessor() {

        this.tokenServiceResponsePopulator = new TokenServiceResponsePopulator();
    }


	/**
	 * returns the base64 encoded token
	 */
	public String getToken() {
		String base64encodedToken = null;
		 String WSAPtoken = null;
		try {
    			final SOAPMessage responseSoapMessage = callSoapWebService(
    					tokenServiceEndPointURl, soapAction);
    			if(null != responseSoapMessage){
        			 WSAPtoken = tokenServiceResponsePopulator
        					.getTokenServiceResponsePopulator(responseSoapMessage);
        			}
    			if(StringUtils.isNotBlank(WSAPtoken)){
        			base64encodedToken = Base64.getEncoder().encodeToString(
        					WSAPtoken.getBytes("utf-8"));
    			}

		} catch (UnsupportedEncodingException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);

		} catch (SOAPException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			e.printStackTrace();

		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return base64encodedToken;

	}

	/**
	 * calls the Experian tokenService.
	 */
	private SOAPMessage callSoapWebService(String soapEndpointUrl,
			String soapAction) {



		SOAPMessage soapResponse = null;
		SOAPMessage message=null;
		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			URL urlEndpoint = null;
			try {
				urlEndpoint = getURL();
			} catch (MalformedURLException e) {
				logger.logInfoMessage("Error in endpoint URL "+e);
			}

			// Send SOAP Message to SOAP Server
			message=createSOAPRequest(soapAction);

			/* Print the request message, just for debugging purposes */
	        logger.logInfoMessage("Request SOAP Message for Token is: =========================================================================\n");

	        String requestMessage = soapMessageToString(message);
	        logger.logInfoMessage(requestMessage);
	        if(loggerSwitch.equalsIgnoreCase("ON")){
	            currentDate =  loggerServiceFrameWork.logRequest(requestMessage, DataCashServiceConstants.LOGFILE_DATE_FORMAT, "tokenService",logFolderPath);
	        }


			try {
				soapResponse = soapConnection.call(message, urlEndpoint);
			} catch (SOAPException ex) {
			    logger.logInfoMessage("Error Occured while making soap SoapCall for tokenService "+ex);
			    ex.printStackTrace();

			}

			soapConnection.close();

		} catch (Exception e) {
			LogWriter.logErrorMessage(soapMessageToString(message) + "\n");
			LogWriter.logErrorMessage(e.getMessage(), e);
			e.printStackTrace();
		}
		logger.logInfoMessage("Token Service Response Soap Message is : \n ");
		logger.logInfoMessage(soapMessageToString(soapResponse) + "\n");
		if(loggerSwitch.equalsIgnoreCase("ON")){
		    loggerServiceFrameWork.logResponse(soapMessageToString(soapResponse), currentDate, "tokenService",logFolderPath);
		}

		return soapResponse;
	}

	/**
	 * create the tokenService request.
	 */
	private SOAPMessage createSOAPRequest(String soapAction) throws Exception {

		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		TokenServiceRequestPopulator.createSoapEnvelope(soapMessage);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);
		soapMessage.saveChanges();


		return soapMessage;
	}

	private String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	}

	private URL getURL() throws MalformedURLException {
		final String connectionTimeout = ConfReader.getConfEntry(
				"experian.tokenservice.connectionTimeout", null);

		final String readTimeout = ConfReader.getConfEntry(
				"experian.tokenservice.readTimeout", null);

		String verifyUrl = ConfReader.getConfEntry(
				"experian.tokenServiceEndPointURl", null);
		;
		String[] urlArray = verifyUrl.split(".com");

		URL endpoint = new URL(new URL(urlArray[0] + ".com"), urlArray[1],
				new URLStreamHandler() {
					@Override
					protected URLConnection openConnection(URL url)
							throws IOException {
						URL target = new URL(url.toString());
						URLConnection connection = target.openConnection();
						// Connection settings
						connection.setConnectTimeout(Integer.parseInt(connectionTimeout)); // 10
																					// sec
						connection.setReadTimeout(Integer.parseInt(readTimeout)); // 1
																				// min
						return (connection);
					}
				});

		return endpoint;
	}

}
