package com.tui.uk.experianClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.log.Logger;
import com.tui.uk.log.LoggerServiceFrameWork;
import com.tui.uk.log.LoggerServiceFrameWorkImpl;
import com.experianpayments.bankwizard.xsd._2009._07.ValidateIBANRequest;
import com.sun.org.apache.xpath.internal.axes.HasPositionalPredChecker;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.startup.ExperianTokenJobRun;
import com.tui.uk.xmlrpc.handlers.DataCashServiceHandler;

public class ValidateServiceRequestProcessor {

	private Map<String, String> validateDetails;
	private String experianServiceEndPointURl = ConfReader.getConfEntry(
			"experian.bankWizardServiceEndPointURl", null);
	private String soapAction = ConfReader.getConfEntry("experian.validateService.soapAction", "Validate");
	private ValidateServiceRequestPopulator validateServiceRequestpopulator;
	private ValidateServiceResponsePopulator validateServiceResponsePopulator;
	private SoapEnvelopeCommonHeader soapEnvelopeCommonHeader;
	private Logger logger;

	   /**
     * Validate Service log file path
     */
    private String logFolderPath = ConfReader.getConfEntry("experian.log.folderPath", "./logs/directDebit/experian/");
    /**
     * Validate Service log enabled/disabled
     */
    private String loggerSwitch = ConfReader.getConfEntry("validateService.log.switch", "OFF");

    /**
     * the dateformat to be appended in the logfiles
     */
    private String currentDate = null;


    private LoggerServiceFrameWork loggerServiceFrameWork = new LoggerServiceFrameWorkImpl();

	public ValidateServiceRequestProcessor(Map<String, String> validateDetails) {

		this.validateDetails = validateDetails;
		validateServiceRequestpopulator = new ValidateServiceRequestPopulator();
		validateServiceResponsePopulator = new ValidateServiceResponsePopulator();
		soapEnvelopeCommonHeader = new SoapEnvelopeCommonHeader();
		logger = LogWriter.getLogger(ValidateServiceRequestProcessor.class
				.getName());

	}

	public Map<String, String> getValidationResult() {

		if(!isExperianServiceEnabled()){
			 LogWriter.logInfoMessage("Experian has been switched off , please enable experian services and try:");
			 throw new UnsupportedOperationException("Experian service is not enabled");
		 }

		Map<String, String> response = new HashMap<String, String>();
		SOAPMessage validationSopaResponseMessage = null;
		try {
			validationSopaResponseMessage = callSoapWebService(
					experianServiceEndPointURl, soapAction, validateDetails);
		} catch (ExperianServiceException e) {
			logger.logErrorMessage(e.getMessage(), e);

		}

		response = validateServiceResponsePopulator
				.getValidationResponse(validationSopaResponseMessage);
		logger.logDebugMessage(response.toString());
		return response;

	}

	/**
	 * calls the Experian validateService.
	 */
	private SOAPMessage callSoapWebService(String soapEndpointUrl,
			String soapAction, Map<String, String> validateDetails)
			throws ExperianServiceException {

		SOAPMessage soapResponse = null;
		SOAPMessage message=null;

		try {

			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			URL urlEndpoint = null;
			try {
				urlEndpoint = getURL();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Send SOAP Message to SOAP Server
			message=createSOAPRequest(soapAction, validateDetails);
			logger.logInfoMessage("Experian Validation Request is =============================================================================================================");
            logger.logInfoMessage(soapMessageToString(message));
            if(loggerSwitch.equalsIgnoreCase("ON")){
                currentDate =  loggerServiceFrameWork.logRequest(soapMessageToString(message), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "validateService",logFolderPath);
            }
			try {
				/*soapResponse = soapConnection.call(
						createSOAPRequest(soapAction, validateDetails),
						urlEndpoint);*/
				soapResponse = soapConnection.call(message,urlEndpoint);

				logger.logInfoMessage("Experian Validation Response is =============================================================================================================");
				logger.logInfoMessage(soapMessageToString(soapResponse));
				if(loggerSwitch.equalsIgnoreCase("ON")){
		            loggerServiceFrameWork.logResponse(soapMessageToString(soapResponse), currentDate, "validateService",logFolderPath);
		        }

			} catch (SOAPException ex) {
				String msg = ex.getMessage();
				if (msg.contains("401Unauthorized")) {

					ExperianTokenJobRun tokenJobRun = new ExperianTokenJobRun();
					tokenJobRun.run();
					message=createSOAPRequest(soapAction, validateDetails);
					soapResponse = soapConnection.call(message,urlEndpoint);
					logger.logInfoMessage("Experian Validation Response is =============================================================================================================");
					logger.logInfoMessage(soapMessageToString(soapResponse));

				} else {
					throw ex;
				}

				// throw new RuntimeException(e.getMessage(), e);
			}
			soapConnection.close();

		} catch (SOAPException e) {
			LogWriter.logErrorMessage(soapMessageToString(message));
			LogWriter.logErrorMessage(e.getMessage(), e);

		}
		return soapResponse;
	}

	/**
	 * create the tokenService request.
	 */
	private SOAPMessage createSOAPRequest(String soapAction,
			Map<String, String> validateDetails) {

		SOAPMessage soapMessage = null;

		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			soapMessage = messageFactory.createMessage();
			SOAPMessage validateServiceMessageHeader = soapEnvelopeCommonHeader
					.getCommonHeader(soapMessage);
			validateServiceRequestpopulator.createSoapEnvelope(soapMessage,
					validateDetails);

			MimeHeaders headers = soapMessage.getMimeHeaders();
			headers.addHeader("SOAPAction", soapAction);
			soapMessage.saveChanges();

		}

		catch (SOAPException e) {
			logger.logErrorMessage(e.getMessage(), e);

		}

		return soapMessage;
	}

	public String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	}

	public URL getURL() throws MalformedURLException {


		final String connectionTimeout = ConfReader.getConfEntry(
				"experian.validation.connectionTimeout", null);

		final String readTimeout = ConfReader.getConfEntry(
				"experian.validation.readTimeout", null);

		String verifyUrl = ConfReader.getConfEntry(
				"experian.bankWizardServiceEndPointURl", null);
		;
		String[] urlArray = verifyUrl.split(".com");
		URL endpoint = new URL(new URL(urlArray[0] + ".com"), urlArray[1],
				new URLStreamHandler() {
					@Override
					protected URLConnection openConnection(URL url)
							throws IOException {
						URL target = new URL(url.toString());
						URLConnection connection = target.openConnection();
						// Connection settings
						connection.setConnectTimeout(Integer.parseInt(connectionTimeout)); // 10
																					// sec
						connection.setReadTimeout(Integer.parseInt(readTimeout)); // 1
																				// min
						return (connection);
					}
				});

		return endpoint;
	}

	/**
	 * This method tells  whether Experian service is active or not
	 * @return boolean flag
	 */
	private boolean isExperianServiceEnabled(){

	    boolean isExperianServiceEnabled = ConfReader.getBooleanEntry("experian.services.enabled",false);

	    LogWriter.logInfoMessage("Experian enabled flag is :"+isExperianServiceEnabled);

		return isExperianServiceEnabled;
	}
}
