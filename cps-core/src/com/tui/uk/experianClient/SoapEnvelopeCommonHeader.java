package com.tui.uk.experianClient;

import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.soap.*;

import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

/**
 * this class is used for constructing the common Soap Header which has to be
 * used by all the experian services while making Soap request.
 */
public class SoapEnvelopeCommonHeader {

	TokenServiceRequestProcessor tokenServiceRequestProcessor;
	TokenServiceImplementation tokenServiceImplementation;

	public Logger logger;

	public SoapEnvelopeCommonHeader() {
		logger = LogWriter.getLogger(SoapEnvelopeCommonHeader.class.getName());
	}

	/**
	 * this methods returns the Soapmessage with common header.
	 *
	 * @param SOAPMessage
	 *            .
	 */
	public SOAPMessage getCommonHeader(SOAPMessage soapMessage) {
		tokenServiceRequestProcessor = new TokenServiceRequestProcessor();
		tokenServiceImplementation = new TokenServiceImplementation();
		String WaspToken = null;
		try {
			WaspToken = tokenServiceImplementation.getExperianToken();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			SOAPPart soapPart = soapMessage.getSOAPPart();
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			soapEnvelope
					.addNamespaceDeclaration(
							"wsse",
							"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
			soapEnvelope
					.addNamespaceDeclaration(
							"wsu",
							"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
			soapEnvelope
					.addNamespaceDeclaration("ban",
							"http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0");
			soapEnvelope.addNamespaceDeclaration("ns",
					"http://experianpayments.com/bankwizard/xsd/2009/07");
			SOAPHeader commonSoapHeader = soapMessage.getSOAPHeader();
			Name valueType = soapEnvelope.createName("ValueType");
			Name id = soapEnvelope.createName("wsu:Id");
			String nameSpaceURI = "ExperianWASP";
			Name encodingType = soapEnvelope.createName("EncodingType");
			String encodingTypeValue = "wsse:Base64Binary";
			QName qname = new QName(
					"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
					"Security", "wsse");
			SOAPHeaderElement soapHeaderElement = commonSoapHeader
					.addHeaderElement(qname);
			soapHeaderElement
					.addNamespaceDeclaration(
							"wsse",
							"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
			soapHeaderElement
					.addNamespaceDeclaration(
							"wsu",
							"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
			QName qname2 = new QName(
					"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
					"BinarySecurityToken", "wsse");
			SOAPElement headerElement = soapHeaderElement
					.addChildElement(qname2);

			headerElement.addAttribute(encodingType, encodingTypeValue);
			headerElement.addAttribute(valueType, nameSpaceURI);
			headerElement.addAttribute(id, "SecurityToken");
			headerElement.setValue(WaspToken);

			soapMessage.saveChanges();

		} catch (SOAPException e) {

			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}

		return soapMessage;

	}

}
