package com.tui.uk.experianClient;

import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

public class GetBranchDataRequestPopulator {

	public GetBranchDataRequestPopulator() {

	}

	public void createSoapEnvelope(SOAPMessage soapMessage1,
			String dataAccessKey) {
		Logger logger = LogWriter.getLogger(GetBranchDataRequestPopulator.class
				.getName());
		try {
			SOAPEnvelope soapEnvelope = soapMessage1.getSOAPPart()
					.getEnvelope();

			SOAPBody soapbody = soapEnvelope.getBody();

			Name banChildElement = soapEnvelope
					.createName("ban:GetBranchDataRequest");

			QName dataAccessKeyName = new QName(
					"http://experianpayments.com/bankwizard/xsd/2009/07",
					"dataAccessKey", "ns");

			Name language = soapEnvelope.createName("language");
			Name returnSubBranches = soapEnvelope
					.createName("returnSubBranches");

			SOAPBodyElement soapElement1 = soapbody
					.addBodyElement(banChildElement);
			soapElement1.addAttribute(language, "en");
			soapElement1.addAttribute(returnSubBranches, "false");

			SOAPElement childElement1 = soapElement1
					.addChildElement(dataAccessKeyName);

			childElement1.setValue(dataAccessKey);

		} catch (SOAPException e) {
			logger.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		}

	}

}
