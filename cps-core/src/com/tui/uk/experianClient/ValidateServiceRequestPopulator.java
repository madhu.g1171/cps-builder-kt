package com.tui.uk.experianClient;

import java.io.IOException;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

public class ValidateServiceRequestPopulator {

	public Logger logger;

	public ValidateServiceRequestPopulator() {
		logger = LogWriter.getLogger(ValidateServiceRequestPopulator.class
				.getName());
	}

	public String sortCodefromRequest = "sortCode";
	public String accounNumberfromRequest = "accountNumber";

	public void createSoapEnvelope(SOAPMessage soapMessage,
			Map<String, String> validateDetails) {

		try {
			String sortCode = validateDetails.get(sortCodefromRequest);
			String accountnumber = validateDetails.get(accounNumberfromRequest);
			SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
			SOAPBody soapbody = soapEnvelope.getBody();
			Name banChildElement = soapEnvelope
					.createName("ban:ValidateRequest");
			QName bban = new QName(
					"http://experianpayments.com/bankwizard/xsd/2009/07",
					"BBAN", "ns");
			Name index = soapEnvelope.createName("index");
			Name isoCountry = soapEnvelope.createName("ISOCountry");
			Name checkingLevel = soapEnvelope.createName("checkingLevel");
			Name language = soapEnvelope.createName("language");
			Name reportString = soapEnvelope.createName("reportString");
			Name itemisationID = soapEnvelope.createName("itemisationID");

			SOAPBodyElement soapElement1 = soapbody
					.addBodyElement(banChildElement);
			soapElement1.addAttribute(isoCountry, "GB");
			soapElement1.addAttribute(checkingLevel, "Account");
			soapElement1.addAttribute(language, "en");
			soapElement1.addAttribute(reportString, "111");
			soapElement1.addAttribute(itemisationID, "123");
			SOAPElement childElement1 = soapElement1.addChildElement(bban);
			childElement1.setAttribute("index", "1");
			childElement1.setValue(sortCode);
			SOAPElement childElement2 = soapElement1.addChildElement(bban);
			childElement2.setAttribute("index", "2");
			childElement2.setValue(accountnumber);

		}

		catch (SOAPException e) {
			LogWriter.logErrorMessage("error occured while forming SOAPEnvelope for Validate Service"+ e);

		}

	}

}
