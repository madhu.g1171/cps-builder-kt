package com.tui.uk.experianClient;

public class ExperianServiceException extends Exception {

	public String message = null;

	public ExperianServiceException(String msg) {
		super(msg);
		this.message = msg;
	}

	public ExperianServiceException() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String toString() {
		return super.toString();

	}

}
