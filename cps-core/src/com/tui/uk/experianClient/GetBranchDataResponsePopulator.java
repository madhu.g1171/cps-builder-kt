package com.tui.uk.experianClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.experianpayments.bankwizard.xsd._2009._07.AddressLineType;
import com.experianpayments.bankwizard.xsd._2009._07.BranchAddressType;
import com.experianpayments.bankwizard.xsd._2009._07.BranchDataType;
import com.experianpayments.bankwizard.xsd._2009._07.GetBranchDataResponse;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

public class GetBranchDataResponsePopulator {

	public Logger logger;

	public GetBranchDataResponsePopulator() {

		logger = LogWriter.getLogger(GetBranchDataResponsePopulator.class
				.getName());

	}

	public Map<String, String> getBranchDataServiceResopnse(
			SOAPMessage respMessage) {
		List<BranchDataType> resplist;
		GetBranchDataResponse getBranchDataResponse = null;
		Map<String, String> respMap = new HashMap<String, String>();
		try {
			final Unmarshaller unmarshaller = JAXBContext.newInstance(
					GetBranchDataResponse.class).createUnmarshaller();
			getBranchDataResponse = (GetBranchDataResponse) unmarshaller
					.unmarshal(respMessage.getSOAPBody().getFirstChild());
			respMessage.saveChanges();

			resplist = getBranchDataResponse.getBranchData();

			Iterator<BranchDataType> iterator = resplist.iterator();

			while (iterator.hasNext()) {
				BranchDataType branchData = iterator.next();
				String institutionName = branchData.getInstitutionName();
				String branchName = branchData.getBranchName();
				String telephoneNumber = branchData.getTelephoneNumber();
				BranchAddressType branchaddress = branchData.getAddress();
				List<AddressLineType> addressline = branchaddress
						.getAddressLine();
				respMap.put("institutionName", institutionName);
				respMap.put("branchName", branchName);
				respMap.put("telephoneNumber", telephoneNumber);
				String postCode = branchaddress.getPostOrZipCode();
				respMap.put("postOrZipCode", postCode);

				Iterator<AddressLineType> addressItr = addressline.iterator();

				while (addressItr.hasNext()) {
					AddressLineType itr = addressItr.next();

					String addressKey = new Integer(itr.getLine()).toString();
					String addressline1 = itr.getValue();
					respMap.put(addressKey, addressline1);
				}

			}

		}

		catch (JAXBException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		} catch (SOAPException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		}
		return respMap;

	}

}
