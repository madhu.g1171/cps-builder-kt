package com.tui.uk.experianClient;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.*;
import com.experian.uk.wasp.STSResponse;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

/**
 * Process the tokenService Response from Experian and returns the token as
 * String
 */

public class TokenServiceResponsePopulator {

	public Logger logger;

	public TokenServiceResponsePopulator() {

		logger = LogWriter.getLogger(TokenServiceResponsePopulator.class
				.getName());

	}

	public String getTokenServiceResponsePopulator(SOAPMessage soapResponse)
			throws SOAPException {

		STSResponse response = null;
		String token = null;
		try {
			final Unmarshaller unmarshaller = JAXBContext.newInstance(
					STSResponse.class).createUnmarshaller();
			response = (STSResponse) unmarshaller.unmarshal(soapResponse
					.getSOAPBody().getFirstChild());
			soapResponse.saveChanges();

			token = response.getSTSResult();

		} catch (JAXBException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			

		}

		return token;
	}

}
