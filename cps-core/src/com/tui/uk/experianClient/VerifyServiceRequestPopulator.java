package com.tui.uk.experianClient;


import javax.xml.namespace.QName;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.experian.data.VerifyRequestData;
import com.tui.uk.log.LogWriter;

public class VerifyServiceRequestPopulator {

	private String sortCode = null;
	private String accountNumber = null;
	private String firstName = null;
	private String lastName = null;
	private String dob = null;
	private String houseNumber = null;
	private String street = null;
	private String postCode = null;
	private String houseName = null;
	private String flat = null;

	public VerifyServiceRequestPopulator(VerifyRequestData requestData) {
		sortCode = requestData.getSortCode();
        accountNumber = requestData.getAccountNumber();
        firstName = requestData.getFirstName();
        lastName = requestData.getLastName();
        dob =  requestData.getDob();
        houseNumber = requestData.getAddress().getHouseNumber();
        street =  requestData.getAddress().getStreet();
        postCode = requestData.getAddress().getPostCode();
        houseName = requestData.getAddress().getHouseName();
        flat = requestData.getAddress().getFlatNumber();

        LogWriter.logInfoMessage("-----housenumber,Flat,housename returned from AddressRefinementService ----"+"houseNumber="+houseNumber+","+"flat="+flat+","+"houseName="+houseName+"street="+street);
	}

	public void createSoapEnvelope(SOAPMessage soapMessage) {

		try {

			SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
			SOAPBody soapbody = soapEnvelope.getBody();
			Name verifyRequestElementd = soapEnvelope
					.createName("ban:VerifyRequest");

			QName accountInformation = new QName(
					"http://experianpayments.com/bankwizard/xsd/2009/07",
					"accountInformation", "ns");

			SOAPBodyElement VerifyRequestElement = soapbody
					.addBodyElement(verifyRequestElementd);
			VerifyRequestElement.addAttribute(
					soapEnvelope.createName("language"), "en");

			SOAPElement accountElement = VerifyRequestElement
					.addChildElement(accountInformation);

			QName qsortCode = new QName(
					"http://experianpayments.com/bankwizard/xsd/2009/07",
					"sortCode", "ns");
			QName qaccountnumber = new QName(
					"http://experianpayments.com/bankwizard/xsd/2009/07",
					"accountNumber", "ns");
			QName qcheckContext = new QName(
					"http://experianpayments.com/bankwizard/xsd/2009/07",
					"checkContext", "ns");

			SOAPElement sortCodeElement = accountElement
					.addChildElement(qsortCode);
			sortCodeElement.addTextNode(sortCode);
			SOAPElement accountNumberElement = accountElement
					.addChildElement(qaccountnumber);
			accountNumberElement.addTextNode(accountNumber);
			SOAPElement checkContextelement = accountElement
					.addChildElement(qcheckContext);
			checkContextelement.addTextNode("Direct Debit");

			if (!StringUtils.isEmpty(houseNumber) || !StringUtils.isEmpty(flat)
					|| !StringUtils.isEmpty(houseName)
					|| !StringUtils.isEmpty(postCode)
					|| !StringUtils.isEmpty(street)
					|| !StringUtils.isEmpty(firstName)
					|| !StringUtils.isEmpty(lastName)
					|| !StringUtils.isEmpty(dob)) {

				QName personalInformation = new QName(
						"http://experianpayments.com/bankwizard/xsd/2009/07",
						"personalInformation", "ns");
				SOAPElement personalElement = VerifyRequestElement
						.addChildElement(personalInformation);

				if (!StringUtils.isEmpty(firstName)
						|| !StringUtils.isEmpty(lastName)
						|| !StringUtils.isEmpty(dob)) {

					if (StringUtils.isEmpty(firstName)
							|| StringUtils.isEmpty(lastName)) {
						throw new ExperianServiceException(
								"Please check the Required mandatory parameters like firstName , surName might be missing");

					}

					QName personal = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"personal", "ns");
					SOAPElement personalInnerElement = personalElement
							.addChildElement(personal);
					QName qfirstname = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"firstName", "ns");
					QName qsurName = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"surname", "ns");
					QName qdob = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"dob", "ns");

					if (!StringUtils.isEmpty(firstName)) {
						SOAPElement firstNameElement = personalInnerElement
								.addChildElement(qfirstname);
						firstNameElement.addTextNode(firstName);
					}
					if (!StringUtils.isEmpty(lastName)) {
						SOAPElement sirNameElement = personalInnerElement
								.addChildElement(qsurName);
						sirNameElement.addTextNode(lastName);
					}
					if (!StringUtils.isEmpty(dob)) {
						SOAPElement dobElement = personalInnerElement
								.addChildElement(qdob);
						dobElement.addTextNode(dob);
					}

				}

				if (!StringUtils.isEmpty(houseNumber)
						|| !StringUtils.isEmpty(flat)
						|| !StringUtils.isEmpty(houseName)
						|| !StringUtils.isEmpty(postCode)
						|| !StringUtils.isEmpty(street)) {

					if (StringUtils.isEmpty(postCode)
							&& StringUtils.isEmpty(street)) {
						throw new ExperianServiceException(
								"Please check the Required mandatory parameters like postCode or street might be missing");

					} else if ((StringUtils.isEmpty(houseNumber)
							&& StringUtils.isEmpty(flat) && StringUtils
								.isEmpty(houseName))) {
						throw new ExperianServiceException(
								"Please check the Required mandatory parameters like houseNumber,flat,houseName might be missing");
					}

					QName address = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"address", "ns");
					SOAPElement addressInnerElement = personalElement
							.addChildElement(address);
					QName qdeliveryPoint = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"deliveryPoint", "ns");
					QName qdeliveryPoint2 = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"deliveryPoint", "ns");
					QName qdeliveryPoint3 = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"deliveryPoint", "ns");
					QName qpostalPoint1 = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"postalPoint", "ns");
					QName qpostalPoint2 = new QName(
							"http://experianpayments.com/bankwizard/xsd/2009/07",
							"postalPoint", "ns");

					if (!(StringUtils.isEmpty(houseNumber))) {
						SOAPElement deliveryPointElement1 = addressInnerElement
								.addChildElement(qdeliveryPoint);
						deliveryPointElement1.setAttribute("deliveryType",
								"houseNumber");
						deliveryPointElement1.addTextNode(houseNumber);
					}
					if (!(StringUtils.isEmpty(flat))) {
						SOAPElement deliveryPointElement2 = addressInnerElement
								.addChildElement(qdeliveryPoint2);
						deliveryPointElement2.setAttribute("deliveryType",
								"flat");
						deliveryPointElement2.addTextNode(flat);
					}
					if (!(StringUtils.isEmpty(houseName))) {
						SOAPElement deliveryPointElement3 = addressInnerElement
								.addChildElement(qdeliveryPoint3);
						deliveryPointElement3.setAttribute("deliveryType",
								"houseName");
						deliveryPointElement3.addTextNode(houseName);
					}
					if (!(StringUtils.isEmpty(postCode))) {
						SOAPElement postalPointElement1 = addressInnerElement
								.addChildElement(qpostalPoint1);
						postalPointElement1.setAttribute("postalType",
								"postcode");
						postalPointElement1.addTextNode(postCode);
					}
					if (!(StringUtils.isEmpty(street))) {
						SOAPElement postalPointElement2 = addressInnerElement
								.addChildElement(qpostalPoint2);
						postalPointElement2
								.setAttribute("postalType", "street");
						postalPointElement2.addTextNode(street);
					}

				}

			}
		} catch (ExperianServiceException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		} catch (SOAPException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}

	}

}
