package com.tui.uk.experianClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.tui.uk.config.ConfReader;
import com.tui.uk.experian.data.VerifyRequestData;
import com.tui.uk.experian.populator.VerifyRequestDataPopulator;
import com.tui.uk.experian.service.AddressRefinementService;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;
import com.tui.uk.log.LoggerServiceFrameWork;
import com.tui.uk.log.LoggerServiceFrameWorkImpl;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.startup.ExperianTokenJobRun;
import org.apache.commons.lang.StringUtils;
public class VerifyServiceProcessor {

	private String experianServiceEndPointURl = ConfReader.getConfEntry(
			"experian.bankWizardServiceEndPointURl", null);
	private String soapAction = ConfReader.getConfEntry("experian.verifyService.soapAction", "Verify");
	private VerifyServiceRequestPopulator verifyServiceRequestPopulator;
	private VerifyServiceResponsePopulator verifyServiceResponsePopulator = new VerifyServiceResponsePopulator();
	private SoapEnvelopeCommonHeader soapEnvelopeCommonHeader;
	public Logger logger;
    /**
     * verify Service log file path
     */
    private String logFolderPath = ConfReader.getConfEntry(
            "experian.log.folderPath", "./logs/directDebit/experian/");
    /**
     * verify Service log enabled/disabled
     */
    private String loggerSwitch = ConfReader.getConfEntry(
            "verifyService.log.switch", "OFF");

    /**
     * the dateformat to be appended in the logfiles
     */
    private String currentDate = null;

    private LoggerServiceFrameWork loggerServiceFrameWork = new LoggerServiceFrameWorkImpl();

	public VerifyServiceProcessor(final Map<String, String> verifyDetails) {


		final VerifyRequestData requestData = new VerifyRequestData();

		final VerifyRequestDataPopulator populator = new VerifyRequestDataPopulator();
		populator.populate(verifyDetails, requestData);

		//service for address refinement to extract houseNumber /Flat or HouseName
        final AddressRefinementService refinementService = new AddressRefinementService();

        if(StringUtils.isBlank(verifyDetails.get("address_refinement_required")) || StringUtils.equalsIgnoreCase("true", verifyDetails.get("address_refinement_required"))){
    		refinementService.processAddressData(requestData.getAddress());
    }


		verifyServiceRequestPopulator = new VerifyServiceRequestPopulator(requestData);

		verifyServiceResponsePopulator = new VerifyServiceResponsePopulator();
		soapEnvelopeCommonHeader = new SoapEnvelopeCommonHeader();
		this.logger = LogWriter.getLogger(VerifyServiceProcessor.class
				.getName());

	}

	public Map<String, String> getVerifyResult() throws SOAPException,
			IOException, ExperianServiceException {
		Map<String, String> response = new HashMap<String, String>();

		SOAPMessage verifiySoapResponseMessage = callSoapWebService(
				experianServiceEndPointURl, soapAction);
		logger.logInfoMessage(soapMessageToString(verifiySoapResponseMessage));
		response = verifyServiceResponsePopulator
				.getVerificationResponse(verifiySoapResponseMessage);

		return response;

	}

	/**
	 * calls the Experian validateService.
	 *
	 * @throws IOException
	 * @throws SOAPException
	 */
	private SOAPMessage callSoapWebService(String soapEndpointUrl,
			String soapAction)
			throws SOAPException, IOException {

		SOAPMessage soapResponse = null;
		SOAPMessage message = null;
		try {
			TokenServiceResponsePopulator tokenServiceResponsePopulator = new TokenServiceResponsePopulator();

		    // Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			URL urlEndpoint = null;
			try {

				urlEndpoint = getURL();
				// Send SOAP Message to SOAP Server

			} catch (MalformedURLException e) {

				e.printStackTrace();
			}

			message = createSOAPRequest(soapAction);
			/* Print the request message, just for debugging purposes */
            logger.logInfoMessage("Request SOAP Message=============================================================:");
            logger.logInfoMessage(soapMessageToString(message));
             if(loggerSwitch.equalsIgnoreCase("ON")){
                    currentDate =  loggerServiceFrameWork.logRequest(soapMessageToString(message), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "verifyService",logFolderPath);
                }
			try {
				/*soapResponse = soapConnection.call(
						createSOAPRequest(soapAction, verifyDetails),
						urlEndpoint);*/
				soapResponse = soapConnection.call(message,urlEndpoint);
			} catch (SOAPException ex) {

				String msg = ex.getMessage();
				if (msg.contains("401Unauthorized")) {

					ExperianTokenJobRun tokenJobRun = new ExperianTokenJobRun();
					tokenJobRun.run();
					message = createSOAPRequest(soapAction);
					soapResponse = soapConnection.call(message,urlEndpoint);
					logger.logInfoMessage("Experian Verification Response is =============================================================================================================");
					logger.logInfoMessage(soapMessageToString(soapResponse));

				} else {
					throw ex;
				}

			}

			soapConnection.close();

		} catch (Exception e) {
			LogWriter.logErrorMessage("Experian Verification Request is : - \n"+soapMessageToString(message));
			LogWriter.logErrorMessage(e.getMessage(), e);
			//throw new RuntimeException(e.getMessage(), e);

		}

		logger.logInfoMessage("Response SOAP Message=============================================================:");
		logger.logInfoMessage(soapMessageToString(soapResponse));
		if(loggerSwitch.equalsIgnoreCase("ON")){
            loggerServiceFrameWork.logResponse(soapMessageToString(soapResponse), currentDate, "verifyService",logFolderPath);
        }

		return soapResponse;
	}

	/**
	 * create the tokenService request.
	 *
	 * @throws IOException
	 */
	private SOAPMessage createSOAPRequest(String soapAction) throws IOException {

		SOAPMessage soapMessage;
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			soapMessage = messageFactory.createMessage();
			SOAPMessage validateServiceMessageHeader = soapEnvelopeCommonHeader
					.getCommonHeader(soapMessage);
			verifyServiceRequestPopulator.createSoapEnvelope(soapMessage);

			MimeHeaders headers = soapMessage.getMimeHeaders();
			headers.addHeader("SOAPAction", soapAction);

			soapMessage.saveChanges();

		}

		catch (SOAPException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		}
		return soapMessage;
	}

	public String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	}

	public URL getURL() throws MalformedURLException {


		final String connectionTimeout = ConfReader.getConfEntry(
				"experian.verification.connectionTimeout", null);

		final String readTimeout = ConfReader.getConfEntry(
				"experian.verification.readTimeout", null);

		String verifyUrl = ConfReader.getConfEntry(
				"experian.bankWizardServiceEndPointURl", null);
		;
		String[] urlArray = verifyUrl.split(".com");
		URL endpoint = new URL(new URL(urlArray[0] + ".com"), urlArray[1],
				new URLStreamHandler() {
					@Override
					protected URLConnection openConnection(URL url)
							throws IOException {
						URL target = new URL(url.toString());
						URLConnection connection = target.openConnection();
						// Connection settings
						connection.setConnectTimeout(Integer.parseInt(connectionTimeout)); // 10
																					// sec
						connection.setReadTimeout(Integer.parseInt(readTimeout)); // 1
																				// min
						return (connection);
					}
				});

		return endpoint;
	}

}
