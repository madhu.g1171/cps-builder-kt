package com.tui.uk.experianClient;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.experianpayments.bankwizard.common.xsd._2009._09.Condition;
import com.experianpayments.bankwizard.common.xsd._2009._09.ConditionSeverity;
import com.experianpayments.bankwizard.common.xsd._2009._09.Conditions;
import com.experianpayments.bankwizard.xsd._2009._07.ValidateResponse;
import com.experianpayments.bankwizard.xsd._2009._07.VerifyResponse;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.BACSCodeType;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

public class VerifyServiceResponsePopulator {

	public VerifyResponse verifyResponse = null;
	public Logger logger;

	public Map<String, String> getVerificationResponse(SOAPMessage soapMessage) {
		Map<String, String> results = null;

		logger = LogWriter.getLogger(VerifyServiceResponsePopulator.class
				.getName());

		try {
			final Unmarshaller unmarshaller = JAXBContext.newInstance(
					VerifyResponse.class).createUnmarshaller();
			verifyResponse = (VerifyResponse) unmarshaller
					.unmarshal(soapMessage.getSOAPBody().getFirstChild());
			soapMessage.saveChanges();

			results = getResponse(verifyResponse);
			if (results != null)
				logger.logInfoMessage(results.toString());

		} catch (JAXBException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		} catch (SOAPException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		}
		return results;
	}

	public Map<String, String> getResponse(VerifyResponse verifyResponse) {

		Map<String, String> verifyResults = new HashMap<String, String>();
		Map<String, String> errors = null;
		Map<String, String> warnings = null;
		String errorMessage = "";
		String warningMessage = "";

		String accountVerificationStatus = null;

		BACSCodeType bacsCodeType = verifyResponse.getAccountInformation()
				.getBacsCode();
		if (bacsCodeType != null) {
			String bacsCode = bacsCodeType.getCode();
			String bacsValue = bacsCodeType.getValue().value();
			verifyResults.put("bacsCode", bacsCode);
		}

		Conditions conditions = verifyResponse.getConditions();
		List<Condition> conditionList = null;
		if (conditions != null) {
			conditionList = conditions.getCondition();
		}
		if (conditionList != null) {

			for (Condition c : conditionList) {
				ConditionSeverity cs = c.getSeverity();

				if ("error".equals(cs.value())) {
					errorMessage += c.getCode().toString() + " ";
				}
				if ("warning".equals(cs.value())) {
					warningMessage += c.getCode().toString() + " ";

				}

			}
			errorMessage.trim();
			warningMessage.trim();

		}

		if (errors != null) {
			verifyResults.put("error", errorMessage);

		}

		if (verifyResponse.getAccountInformation() != null)
			verifyResults.put("dataAccessKey", verifyResponse
					.getAccountInformation().getDataAccessKey());
		if (verifyResponse.getAccountInformation()
				.getAccountVerificationStatus() != null)
			accountVerificationStatus = verifyResponse.getAccountInformation()
					.getAccountVerificationStatus().value();
		verifyResults.put("accountVerificationStatus",
				accountVerificationStatus);

		if (warnings != null) {
			verifyResults.put("warning", warningMessage);
		}

		String addressDetailsScore = null;
		String personalDetailsScore = null;

		if (verifyResponse.getPersonalInformation() != null) {
			if (verifyResponse.getPersonalInformation()
					.getPersonalDetailsScore() != null)
				personalDetailsScore = ""
						+ verifyResponse.getPersonalInformation()
								.getPersonalDetailsScore().intValue();
			if (verifyResponse.getPersonalInformation().getAddressScore() != null)
				addressDetailsScore = ""
						+ verifyResponse.getPersonalInformation()
								.getAddressScore().intValue();
			if (personalDetailsScore != null)
				verifyResults.put("personalDetailsScore", personalDetailsScore);
			if (addressDetailsScore != null)
				verifyResults.put("addressDetailsScore", addressDetailsScore);
		}

		return verifyResults;
	}

}
