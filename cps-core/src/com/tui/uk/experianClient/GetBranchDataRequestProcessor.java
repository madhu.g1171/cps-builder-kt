package com.tui.uk.experianClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.*;

import com.experianpayments.bankwizard.xsd._2009._07.AddressLineType;
import com.experianpayments.bankwizard.xsd._2009._07.BranchAddressType;
import com.experianpayments.bankwizard.xsd._2009._07.BranchDataType;
import com.experianpayments.bankwizard.xsd._2009._07.GetBranchDataResponse;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;
import com.tui.uk.log.LoggerServiceFrameWork;
import com.tui.uk.log.LoggerServiceFrameWorkImpl;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.startup.ExperianTokenJobRun;

public class GetBranchDataRequestProcessor {

	public Logger logger;

	public GetBranchDataRequestProcessor() {

		logger = LogWriter.getLogger(GetBranchDataRequestProcessor.class
				.getName());
	}

	private String experianServiceEndPointURl = ConfReader.getConfEntry(
			"experian.bankWizardServiceEndPointURl", null);
	private String soapAction = ConfReader.getConfEntry("experian.getBranchDataService.soapAction", "GetBranchData");


    /**
     * getBranchData Service log file path
     */
    private String logFolderPath = ConfReader.getConfEntry(
            "experian.log.folderPath", "./logs/directDebit/experian/");
    /**
     * getBranchData Service log enabled/disabled
     */
    private String loggerSwitch = ConfReader.getConfEntry(
            "getBranchDataService.log.switch", "OFF");

    /**
     * the dateformat to be appended in the logfiles
     */
    private String currentDate = null;

    private LoggerServiceFrameWork loggerServiceFrameWork = new LoggerServiceFrameWorkImpl();

	SoapEnvelopeCommonHeader soapEnvelopeCommonHeader;
	GetBranchDataRequestPopulator getBranchDataRequestPopulator;
	GetBranchDataResponsePopulator getBranchDataResponsePopulator;

	public Map<String, String> getBranchData(String dataAccessKey) {
		getBranchDataResponsePopulator = new GetBranchDataResponsePopulator();
		Map<String, String> addressdetails = new HashMap<String, String>();
		SOAPMessage branchDataResponseMessage = null;
		try {
			branchDataResponseMessage = callSoapWebService(
					experianServiceEndPointURl, soapAction, dataAccessKey);

			addressdetails = getBranchDataResponsePopulator
					.getBranchDataServiceResopnse(branchDataResponseMessage);

		} catch (ExperianServiceException e) {
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);

		}

		return addressdetails;

	}

	private SOAPMessage callSoapWebService(String soapEndpointUrl,
			String soapAction, String dataAccessKey)
			throws ExperianServiceException {
		SOAPMessage soapResponse = null;
		SOAPMessage message=null;
		try {

			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			URL urlEndpoint = null;
			try {
				urlEndpoint = getURL();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Send SOAP Message to SOAP Server
			message=createSOAPRequest(soapAction,dataAccessKey);
			logger.logInfoMessage("GetBranchDataSoap Requst is :========================================== \n");
	        logger.logInfoMessage(soapMessageToString(message));
	        if(loggerSwitch.equalsIgnoreCase("ON")){
	            currentDate =  loggerServiceFrameWork.logRequest(soapMessageToString(message), DataCashServiceConstants.LOGFILE_DATE_FORMAT, "getBranchDataService",logFolderPath);
	        }
			try {
				/*soapResponse = soapConnection.call(
						createSOAPRequest(soapAction, dataAccessKey),
						urlEndpoint);*/
				soapResponse = soapConnection.call(message,urlEndpoint);
				soapResponse.saveChanges();
				// Print the SOAP Response
				logger.logInfoMessage("GetBranchDataRequest soap response is: ==================================\n");
				logger.logInfoMessage(soapMessageToString(soapResponse));
				if(loggerSwitch.equalsIgnoreCase("ON")){
		            loggerServiceFrameWork.logResponse(soapMessageToString(soapResponse), currentDate, "getBranchDataService",logFolderPath);
		        }

			} catch (SOAPException ex) {
				String msg = ex.getMessage();
				if (msg.contains("401Unauthorized")) {

					ExperianTokenJobRun tokenJobRun = new ExperianTokenJobRun();
					tokenJobRun.run();
					message=createSOAPRequest(soapAction,dataAccessKey);
					soapResponse = soapConnection.call(message,urlEndpoint);
					logger.logInfoMessage("Experian Verification Response is =============================================================================================================");
					logger.logInfoMessage(soapMessageToString(soapResponse));

				} else {
					throw ex;
				}

			}

			SOAPFault soapFault = soapResponse.getSOAPBody().getFault();
			String faulterror = null;
			if (soapFault != null) {
				faulterror = soapFault.getFaultCode();
			}
			if (faulterror != null) {
				throw new ExperianServiceException(
						"Bad Request is made with Expired dataAccessKey");
			}

			soapConnection.close();
		} catch (SOAPException e) {
			LogWriter.logInfoMessage(soapMessageToString(message));
			LogWriter.logErrorMessage(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}
		return soapResponse;
	}

	private SOAPMessage createSOAPRequest(String soapAction,
			String dataAccessKey) throws SOAPException {
		soapEnvelopeCommonHeader = new SoapEnvelopeCommonHeader();
		getBranchDataRequestPopulator = new GetBranchDataRequestPopulator();
		SOAPMessage soapMessage;

		MessageFactory messageFactory = MessageFactory.newInstance();
		soapMessage = messageFactory.createMessage();
		SOAPMessage soapMessage2 = soapEnvelopeCommonHeader
				.getCommonHeader(soapMessage);
		getBranchDataRequestPopulator.createSoapEnvelope(soapMessage2,
				dataAccessKey);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);
		soapMessage.saveChanges();

		return soapMessage;
	}

	public String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	}

	public URL getURL() throws MalformedURLException {

		final String connectionTimeout = ConfReader.getConfEntry(
				"experian.branchData.connectionTimeout", null);

		final String readTimeout = ConfReader.getConfEntry(
				"experian.branchData.readTimeout", null);

		String verifyUrl = ConfReader.getConfEntry(
				"experian.bankWizardServiceEndPointURl", null);
		;
		String[] urlArray = verifyUrl.split(".com");

		URL endpoint = new URL(new URL(urlArray[0] + ".com"), urlArray[1],
				new URLStreamHandler() {
					@Override
					protected URLConnection openConnection(URL url)
							throws IOException {
						URL target = new URL(url.toString());
						URLConnection connection = target.openConnection();
						connection.setConnectTimeout(Integer.parseInt(connectionTimeout));
						connection.setReadTimeout(Integer.parseInt(readTimeout));
						return (connection);
					}
				});

		return endpoint;
	}

}
