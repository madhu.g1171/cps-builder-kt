/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: Jul 7, 2009
 *
 * $Author: sindhushree.g
 *
 * $Log: $
*/
package com.tui.uk.config;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import com.tui.uk.log.LogWriter;

/**
 * This class is responsible for reading messages through the ResourceBundle.
 *
 * @author sindhushree.g
 *
 */
public final class PropertyResource
{

   /** Map that holds the locale language as key and the value as key-value pair
    * for that locale.
    */
   private static final Map<String, Map<String, String>> PROPERTY_MAP =
      new HashMap<String, Map<String, String>>();

   /**
    * The private constructor.
    */
   private PropertyResource()
   {
      // Nothing to be done.
   }

   /**
    * Gets the property from the specified properties file. This uses the server's default locale.
    *
    * @param key the key.
    * @param baseName the name of the property file.
    *
    * @return String the message.
    */
   public static String getProperty(String key, String baseName)
   {
      return getProperty(key, baseName, Locale.getDefault());
   }

   /**
    * Gets all the properties of the specified property file.
    *
    * @param baseName the property file name.
    *
    * @return properties.
    */
   public static Properties getAllProperties(String baseName)
   {
      Properties properties = new Properties();
      ResourceBundle resourceBundle = ResourceBundle.getBundle(baseName);
      Enumeration<String> enumeration = resourceBundle.getKeys();
      while (enumeration.hasMoreElements())
      {
         String key = enumeration.nextElement();
         properties.put(key, resourceBundle.getObject(key));
      }
      return properties;
   }
   /**
    * Gets the property from the specified properties file for a specified locale.
    *
    * @param key the key.
    * @param baseName the name of the property file.
    * @param locale the locale from which the message is required.
    *
    * @return String the message.
    */
   public static String getProperty(String key, String baseName, Locale locale)
   {
      String baseKey = locale.getLanguage() + locale.getCountry() + "_" + baseName;
      if (PROPERTY_MAP.get(baseKey) == null || PROPERTY_MAP.get(baseKey).isEmpty())
      {
         loadMessageProperties(baseName, locale);
      }
      return PROPERTY_MAP.get(baseKey).get(key);
   }

   /**
    * This method is responsible for loading the resource bundle and populating the map.
    *
    * @param baseName the name of the property file
    * @param locale The locale for which message is required.
    */
   private static void loadMessageProperties(String baseName, Locale locale)
   {
      ResourceBundle resourceBundle = ResourceBundle.getBundle(baseName, locale);

      LogWriter.logInfoMessage("Loaded the resource bundle for: " + locale.getLanguage()
         + locale.getCountry());

      // The key value pairs in the resource bundle properties file.
      Map<String, String> properties = new HashMap<String, String>();

      for (String propertyKey : Collections.list(resourceBundle.getKeys()))
      {
         properties.put(propertyKey, resourceBundle.getString(propertyKey));
      }

      if (PROPERTY_MAP.isEmpty())
      {
         PROPERTY_MAP.clear();
      }
      PROPERTY_MAP.put(locale.getLanguage() + locale.getCountry() + "_" + baseName, properties);
   }

}