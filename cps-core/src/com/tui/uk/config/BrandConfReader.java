/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BrandConfReader.java,v $
 *
 * $Revision: $
 *
 * $Date: 2009-09-16 $
 *
 * $Author: kumarhe $
 *
 * $Log: $
 *
 */
package com.tui.uk.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.tui.uk.domain.BrandMapping;
import com.tui.uk.exception.BrandException;


/**
 * The class to read the Config file and create the brand URL mapping.
 *
 * @author kumarhe
 */
public final class BrandConfReader
{
   /** This stores configuration entries as key/value pair. */
   private static List<BrandMapping> brandMappings = new ArrayList<BrandMapping>();

   /** Name of the configuration properties file used. */
   private static String configFileName;

   /**
    * Private constructor to stop instantiation by third parties.
    */
   private BrandConfReader()
   {
      // Nothing to be done.
   }


   /**
    * Set the config file name.
    * @param configFileName the configFileName to set
    */
   public static void setConfigFileName(String configFileName)
   {
      BrandConfReader.configFileName = configFileName;
   }

   /**
    * Gets the list of brand mapping object.
    * @return the brandMappings
    * @throws BrandException the error if any
    */
   public static List<BrandMapping> getBrandMappings() throws BrandException
   {

      if (!checkConfiguration())
      {
            loadConfigEntries();
      }
      return brandMappings;

   }

   /**
    * Loads configuration entries.
    *
    * @throws BrandException in case of an error.
    */
   public static void loadConfiguration() throws BrandException
   {
      loadConfigEntries();
   }

   /**
    * Returns true if a configuration has been loaded, false if no configuration is present.
    *
    * @return boolean true.
    */
   private static boolean checkConfiguration()
   {
      if (brandMappings == null)
      {
         return false;
      }
      synchronized (brandMappings)
      {
         return !brandMappings.isEmpty();
      }
   }

   /**
    * Read configuration entries and stores it as key/value pair in CONFIG.
    *
    * @throws BrandException in case of an error.
    */
   private static void loadConfigEntries() throws BrandException
   {
      loadEntries("brandconfigfile", configFileName);
   }

   /**
    * The method which loads the configuration/message entries.
    *
    * @param name the file name.
    * @param defaultName the default name.
    * @throws BrandException the Exception.
    */
   private static void loadEntries(String name, String defaultName)
      throws BrandException
   {
      String propertyFileName = defaultName;
      brandMappings.clear();
      String filename = System.getProperty(name);
      if (filename != null)
      {
         propertyFileName = filename;
      }
      List<BrandMapping> localFileProperties = readPropertyFile(propertyFileName);
      // avoid NPE
      if (localFileProperties != null && !localFileProperties.isEmpty())
      {
         brandMappings.addAll(localFileProperties);
      }
   }

   /**
    * This takes a filename and loads it from the file system.
    *
    * @param filename the filename containing configuration
    * @return the properties loaded from file
    * @throws BrandException if the configuration cannot be loaded
    */
   public static List<BrandMapping> readPropertyFile(String filename) throws BrandException
   {
      File file = new File(filename);
      BufferedReader bufferedreader = null;
      List<BrandMapping> brandMap = new ArrayList<BrandMapping>();
      try
      {
         bufferedreader = new BufferedReader(new FileReader(file));
         load(bufferedreader, brandMap);
      }
      catch (FileNotFoundException ex)
      {
         throw new BrandException("Configuration file (" + filename
            + ") not found, Ensure that the file exists and is readable", ex);
      }
      catch (IOException ex)
      {
         throw new BrandException("Configuration file (" + filename + ") cannot be read.", ex);
      }
      finally
      {
         if (bufferedreader != null)
         {
            try
            {
               bufferedreader.close();
            }
            catch (IOException e)
            {
               throw new BrandException(
                  "Configuration file (" + filename + ") cannot be closed", e);
            }
         }
      }

      return brandMap;
   }

   /**
    * Loads the config from the brand.conf file.
    *  "=" is the used as the key value separater
    *  and a line comment start with "#" in the conf file.
    *
    * @param reader the buffered reader.
    * @param brandMap the list of  BrandMapping object.
    * @exception IOException if an error occurred when reading from the input stream.
    * @throws IllegalArgumentException if the input stream contains a
    *         malformed Unicode escape sequence.
    */
   private static synchronized void load(BufferedReader reader, List<BrandMapping> brandMap)
      throws IOException
   {
      String line = null;
      String[] keyValPair = null;
      while ((line = reader.readLine()) != null)
      {
         line = line.trim();
         if (line.length() == 0 || line.charAt(0) == '#')
         {
            continue;
         }
         else
         {
            keyValPair = line.split("\\s*=\\s*");
            brandMap.add(new BrandMapping(keyValPair[0], keyValPair[1]));

         }
      }
   }
}
