/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ConfReader.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $ Revision 1.2 2008/05/07 11:18:36
 * sindhushree.g Add file revision history comment.
 *
 */

package com.tui.uk.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;

import com.tui.uk.exception.ConfReaderException;
import com.tui.uk.log.LogWriter;
import com.tui.uk.startup.ContextStartup;

/**
 * Loads configuration entries/messages for Common Payment Service.
 *
 * @author sindhushree.g
 */
public final class ConfReader
{
   /** This stores configuration entries as key/value pair. */
   private static final Properties CONFIG = new Properties();

   /** Name of the configuration properties file used. */
   private static final List<String>  CONFIG_FILE_NAMES = new ArrayList<String>();

   /** Application Name, this is used for software configuration entries. */
   private static String appName;
   
   /** This is real path for ACSS. */
   private static String realPathForACSS = "";

   /**
    * Private constructor to stop instantiation by third parties.
    */
   private ConfReader()
   {
      // Nothing to be done.
   }

   /**
    * Sets the application name.
    *
    * @param newAppName the application name to be used.
    */
   public static void setAppName(String newAppName)
   {
      appName = newAppName;
   }

   /**
    * Loads configuration  entries.
    *
    * @param confFileNames the list of conf files to be loaded.
    *
    * @throws ConfReaderException in case of an error.
    */
   public static void loadConfiguration(List<String> confFileNames) throws ConfReaderException
   {
      CONFIG_FILE_NAMES.clear();
      for (String confFileName : confFileNames)
      {
         CONFIG_FILE_NAMES.add(confFileName);
      }
      loadEntries();
   }

   /**
    * Gets entry passing key and defaultStr.
    *
    * @param key the key.
    * @param defaultStr if no value is found.
    *
    * @return the value retrieved from configuration
    */
   public static String getConfEntry(String key, String defaultStr)
   {
      return getEntry(key, defaultStr);
   }

   /**
    * Gets entry passing key and defaultStr.
    *
    * @param key the key.
    * @param defaultStr if no value is found.
    *
    * @return the value retrieved from configuration.
    */
   private static String getEntry(String key, String defaultStr)
   {
      String result = get(key, null);
      if (result != null)
      {
         return result;
      }
      return defaultStr;
   }

   /**
    * Gets entry(int) passing key and defaultStr.
    *
    * @param key the key.
    * @param defaultInt if no value is found.
    *
    * @return the value retrieved from configuration
    */
   public static int getIntEntry(String key, int defaultInt)
   {
      try
      {
         String value = getEntry(key, null);
         if (value == null)
         {
            return defaultInt;
         }
         else
         {
            return Integer.parseInt(value);
         }
      }
      catch (NumberFormatException nfe)
      {
         LogWriter.logDebugMessage("Failed to retrieve entry for the given key");
         LogWriter.logErrorMessage(nfe.getMessage());
         return defaultInt;
      }
   }

   /**
    * Gets entry(long) passing key and defaultStr.
    *
    * @param key the key.
    * @param defaultLong if no value is found.
    *
    * @return the value retrieved from configuration
    */
   public static long getLongEntry(String key, long defaultLong)
   {
      try
      {
         String value = getEntry(key, null);
         if (value == null)
         {
            return defaultLong;
         }
         else
         {
            return Long.parseLong(value);
         }
      }
      catch (NumberFormatException nfe)
      {
         LogWriter.logDebugMessage("Failed to retrieve entry for the given key");
         LogWriter.logErrorMessage(nfe.getMessage());
         return defaultLong;
      }
   }

   /**
    * Gets entry(Boolean) passing key and defaultStr.
    *
    * @param key the key.
    * @param defaultBoolean if no value is found.
    *
    * @return the boolean value retrieved from configuration
    */
   public static boolean getBooleanEntry(String key, boolean defaultBoolean)
   {
      String value = getEntry(key, null);
      if (value == null)
      {
         return defaultBoolean;
      }
      return Boolean.parseBoolean(value);
   }

   /**
    * The method to retrieve an array of values that are stored in the configuration file
    * concatenated using a delimiter.
    *
    * @param key the key.
    * @param defaultValue if no value is found.
    * @param delimiter The character or String used as delimitter e.g. "," ,"|" etc
    *    OR can be Regex.
    *
    * @return the delimited values as an array of Strings
    */
   public static String[] getStringValues(String key, String[] defaultValue, String delimiter)
   {
      String concatenatedEntry = getEntry(key, null);
      if (concatenatedEntry == null)
      {
         return defaultValue;
      }
      return concatenatedEntry.trim().split(delimiter);
   }

   /**
    * The method which loads the configuration entries.
    *
    * @throws ConfReaderException the Exception.
    */
   private static void loadEntries() throws ConfReaderException
   {
      CONFIG.clear();
      populateConfFileNames();
      populateProperties();
   }

   /**
    * This method populates the list of conf file names used in this application.
    */
   private static void populateConfFileNames()
   {
      if (CONFIG_FILE_NAMES.isEmpty())
      {
         for (Entry<Object, Object> propertyEntry : System.getProperties().entrySet())
         {
            if (((String) propertyEntry.getValue()).endsWith(".conf"))
            {
               CONFIG_FILE_NAMES.add((String) propertyEntry.getValue());
            }
         }
      }
   }

   /**
    * This method reads the property file and populates the map with the properties read.
    *
    * @throws ConfReaderException if properties can not be read.
    */
   private static void populateProperties() throws ConfReaderException
   {
      for (String propertyFileName : CONFIG_FILE_NAMES)
      {
         Properties localFileProperties = readPropertyFile(propertyFileName);
         // avoid NPE
         if (localFileProperties != null && !localFileProperties.isEmpty())
         {
            for (Entry<Object, Object> prop : localFileProperties.entrySet())
            {
               String key = (String) prop.getKey();
               String value = (String) prop.getValue();

               if (appName == null)
               {
                  appName = System.getProperty("appname");
               }

               if (key.startsWith(appName + "."))
               {
                  key = key.substring(appName.length() + 1);
               }
               CONFIG.setProperty(key, value);
            }
         }
      }
   }

   /**
    * This takes a filename and loads it from the file system.
    *
    * @param filename the filename containing configuration
    * @return the properties loaded from file
    * @throws ConfReaderException if the configuration cannot be loaded
    */
   public static Properties readPropertyFile(String filename)
      throws ConfReaderException
   {
      File file = new File(filename);
      FileInputStream fis = null;
      Properties prop = null;

      try
      {
         fis = new FileInputStream(file);
         prop = new Properties();
         prop.load(fis);
      }
      catch (FileNotFoundException ex)
      {
         LogWriter.logDebugMessage(ex.getMessage(), ex);
         throw new ConfReaderException("Configuration file (" + file.getName()
            + ") not found, Ensure that the file exists and is readable.", ex);
      }
      catch (IOException ex)
      {
         LogWriter.logDebugMessage(ex.getMessage(), ex);
         throw new ConfReaderException("Configuration file (" + file.getName()
            + ") cannot be read.", ex);
      }
      finally
      {
         if (fis != null)
         {
            try
            {
               fis.close();
            }
            catch (IOException e)
            {
               throw new ConfReaderException("Configuration file (" + filename
                  + ") cannot be closed" + e.getMessage(), e);
            }
         }
      }
      return prop;
   }

   /**
    * Returns true if a configuration has been loaded, false if no configuration is present.
    *
    * @return boolean true.
    */
   private static boolean checkConfiguration()
   {
      if (CONFIG == null)
      {
         return false;
      }
      synchronized (CONFIG)
      {
         return !CONFIG.isEmpty();
      }
   }

   /**
    * Obtains the value for a given Configuration Entry, returns defaultStr if the configuration
    * entry does not exist.
    *
    * @param configEntry Configuration Entry, note this is case sensitive.
    * @param defaultStr if no value is found.
    * @return The Value held for that configuration entry.
    */
   private static String get(String configEntry, String defaultStr)
   {
      if (!checkConfiguration())
      {
         try
         {
            loadEntries();
         }
         catch (ConfReaderException cfe)
         {
            LogWriter.logDebugMessage("Failed to load the configuration...........");
            LogWriter.logErrorMessage(cfe.getMessage());
            return defaultStr;
         }
      }
      String value = CONFIG.getProperty(configEntry);
      if (value == null)
      {
         return defaultStr;
      }
      else
      {
         return value;
      }
   }
   
public static String getRealPathForACSS() {
return realPathForACSS;
}

public static void setRealPathForACSS(String realPathForACSS) {
ConfReader.realPathForACSS = realPathForACSS;
}

}
