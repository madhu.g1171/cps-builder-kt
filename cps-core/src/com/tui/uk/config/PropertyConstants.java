/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: Jul 17, 2009
 *
 * $Author: sindhushree.g
 *
 * $Log: $
*/
package com.tui.uk.config;

/**
 * This class contains constants for the properties file names.
 *
 * @author sindhushree.g
 *
 */
public final class PropertyConstants
{

   /**
    * The private constructor.
    */
   private PropertyConstants()
   {
      // Do nothing.
   }

   /** The messages properties file name. */
   public static final String MESSAGES_PROPERTY = "messages";

   /** The log4j properties file name. */
   public static final String LOG4J_PROPERTY = "log4j";
   
   /** The Autocancel config key. */
   public static final String AUTOCANCELFLAG = "autocancel.enable";

   /** The down time switch config key. */
   public static final String DOWNTIME_SWITCH = "downtime.switch";
   
   /** The down time timerange config key. */
   public static final String DOWNTIME_TIMERANGE = "downtime.timerange";
   
   /** The departure date config key. */
   public static final String DEPARTUREDATEVALIDATON_ARRAY = "departuredatevalidation.array";
   
   /** The home page url config key. */
   public static final String DOWNTIME_HOMEPAGE_URL = "downtime.homepage.url";
   
   /** The down time departure date config key. */
   public static final String DOWNTIME_DEPARTUREDATE = "downtime.departuredate";
   
}
