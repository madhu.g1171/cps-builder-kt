/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Transaction.java$
 *
 * $Revision: $
 *
 * $Date: Oct 16, 2008$
 *
 * Author: bibin.j
 *
 * $Log: $
 */
package com.tui.uk.domain;

import java.util.Date;

import com.tui.uk.client.domain.Money;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.service.datacash.CardResponse;
import com.tui.uk.payment.service.datacash.DataCashCard;
import com.tui.uk.payment.service.datacash.DataCashServiceFactory;
import com.tui.uk.payment.service.datacash.Response;

/**
 * Represents a particular transaction.
 *
 * @author bibin.j@sonata-software.com
 */
public class Transaction
{

   /** The transaction id. */
   private final String transactionId;

   /** The transaction date. */
   private final Date transactionDate;

   /** The transaction source. */
   private final String transactionSource;

   /** The transaction type. */
   private final String transactionType;

   /** The transaction amount. */
   private final Money transactionAmount;

   /** The data cash reference number. */
   private String dcTransactionId;

   /** The data cash success message or failure reason. */
   private String dcMessage;

   /** The data cash return code. */
   private int dcStatusCode;

   /** The data cash transaction date. */
   private Date dcTransactionDate;

   /** The card information. */
   private DataCashCard card;
   
   /** Method name to be logged with dcMessage after doPayment().  */
   private static final String PAYMENT = "Payment";
   
   /** Method name to be logged with dcMessage after doRefund(). */
   private static final String REFUND = "Refund";
   
   /** Method name to be logged with dcMessage after doFulfilment(). */
   private static final String FULFILMENT = "Fulfilment";
   
   /**
    * The constructor.
    *
    * @param dcTransactionId the dcTransactionId.
    * @param transactionAmount the transaction amount.
    */
   public Transaction(String dcTransactionId, Money transactionAmount)
   {
      this.dcTransactionId = dcTransactionId;
      this.transactionAmount = transactionAmount;
      this.transactionId = null;
      this.transactionDate = null;
      this.transactionSource = null;
      this.transactionType = null;
   }

   /**
    * The overloaded constructor.
    *
    * @param transactionId the transaction id.
    * @param transactionDate the transaction date.
    * @param transactionSource the transaction source.
    * @param transactionType the transaction type.
    * @param transactionAmount the transaction amount.
    * @param card the card information.
    */
   public Transaction(String transactionId, Date transactionDate, String transactionSource,
                      String transactionType, Money transactionAmount, DataCashCard card)
   {
      this.transactionId = transactionId;
      this.transactionDate = new Date(transactionDate.getTime());
      this.transactionSource = transactionSource;
      this.transactionType = transactionType;
      this.transactionAmount = transactionAmount;
      this.card = card;
   }

   /**
    * Gets the transaction Id.
    *
    * @return the transactionId
    */
   public String getTransactionId()
   {
      return transactionId;
   }

   /**
    * Gets the transaction date.
    *
    * @return the transactionDate
    */
   public Date getTransactionDate()
   {
      return new Date(transactionDate.getTime());
   }

   /**
    * Gets the transaction source.
    *
    * @return the transactionSource
    */
   public String getTransactionSource()
   {
      return transactionSource;
   }

   /**
    * Gets the transaction type.
    *
    * @return the transactionType
    */
   public String getTransactionType()
   {
      return transactionType;
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transactionAmount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the data cash reference number.
    *
    * @return the dcTransactionId
    */
   public String getDcTransactionId()
   {
      return dcTransactionId;
   }

   /**
    * Gets the data cash status code.
    *
    * @return the dcStatusCode
    */
   public int getDcStatusCode()
   {
      return dcStatusCode;
   }

   /**
    * Gets the data cash success message or failure reason.
    *
    * @return the dcMessage
    */
   public String getDcMessage()
   {
      return dcMessage;
   }

   /**
    * Gets the data cash updated date.
    *
    * @return the dcTransactionDate
    */
   public Date getDcTransactionDate()
   {
      return new Date(dcTransactionDate.getTime());
   }

   /**
    * Gets the Card.
    *
    * @return the card
    */
   public final DataCashCard getCard()
   {
      return card;
   }

   /**
    * This method is responsible for performing the "auth" transaction type.The transaction amount
    * must be positive in order to do the payment.
    *
    * @param clientAccount the data-cash client account(same as Vtid).
    * @param merchantReference the merchant reference number.
    */
   public final void doPayment(String clientAccount, String merchantReference)
   {
      LogWriter.logInfoMessage("Sending request to Datacash for payment");
      CardResponse cardResponse =
         DataCashServiceFactory.getDataCashService(clientAccount).doPayment(card,
            transactionAmount, null, merchantReference);
      this.dcStatusCode = cardResponse.getCode();
      updateDCMessage(cardResponse.getDescription(), PAYMENT);
      this.dcTransactionId = cardResponse.getDatacashReference();
      this.dcTransactionDate = cardResponse.getTime();
   }

   /**
    * This method is responsible for performing the "refund" transaction type. This method accepts a
    * negative transaction amount which indicates it is a refund.
    *
    * @param clientAccount the data-cash client account(same as Vtid).
    * @param merchantReference the merchant reference number.
    */
   public final void doRefund(String clientAccount, String merchantReference)
   {
      LogWriter.logInfoMessage("Sending request to Datacash for refund");
      CardResponse cardResponse =
         DataCashServiceFactory.getDataCashService(clientAccount).doRefund(card,
            new Money(transactionAmount.getAmount().abs(), transactionAmount.getCurrency()), null,
            merchantReference);
      this.dcStatusCode = cardResponse.getCode();
      updateDCMessage(cardResponse.getDescription(), REFUND);
      this.dcTransactionId = cardResponse.getDatacashReference();
      this.dcTransactionDate = cardResponse.getTime();
   }

   /**
    * This method is responsible for performing the fulfillment of an authorized DataCash payment.
    *
    * @param clientAccount the data-cash client account(same as Vtid).
    */
   public final void doFulfilTransaction(String clientAccount)
   {
      LogWriter.logInfoMessage("Sending request to Datacash for fulfilment");
      Response cardResponse =
         (Response) DataCashServiceFactory.getDataCashService(clientAccount).fulfill(
            dcTransactionId, "1", null);
      this.dcStatusCode = cardResponse.getCode();
      updateDCMessage(cardResponse.getDescription(), FULFILMENT);
      this.dcTransactionId = cardResponse.getDatacashReference();
      this.dcTransactionDate = cardResponse.getTime();
   }

   /**
    * Purge the card details.
    *
    */
   public void purgeCardDetails()
   {
      card.purgeCardDetails();
      this.card = null;
   }

   /**
    * This method updates the transaction message with success/failure message from data-cash.
    *
    * @param description the message returned from data-cash.
    * @param methodName the method from which this is called.
    */
   private void updateDCMessage(String description, String methodName)
   {
      if (dcStatusCode != 1)
      {
         dcMessage = description;
         LogWriter.logErrorMessage("Status Code - " + dcStatusCode);
         LogWriter.logErrorMessage("Status - Failure : Failure Reason: " + dcMessage);
         LogWriter.logInfoMessage(methodName + " failed");
      }
      else
      {
         dcMessage = description;
         LogWriter.logInfoMessage(methodName + " successful");
      }
   }
}
