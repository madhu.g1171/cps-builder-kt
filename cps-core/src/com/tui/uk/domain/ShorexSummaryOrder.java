package com.tui.uk.domain;

import java.text.SimpleDateFormat;
import java.util.Comparator;

import com.tui.uk.client.domain.DiningReservation;
import com.tui.uk.client.domain.ShorexExcursions;

public class ShorexSummaryOrder implements Comparator
{
   


   @Override
   public int compare(Object object1, Object object2)
   {      
      int result=1;
      ShorexExcursions excursion1=null;
      ShorexExcursions excursion2=null;
      DiningReservation reservation1=null;
      DiningReservation reservation2=null;
      
      if(object2==null || object1==null){
         return 1;
      }
      if(object1 instanceof ShorexExcursions && object2 instanceof ShorexExcursions){
         excursion1=(ShorexExcursions) object1;
         excursion2=(ShorexExcursions) object2;
         if(excursion1.getDate()!=null){
            result=excursion1.getDate().compareTo(excursion2.getDate());
         }         
      }
      else if(object1 instanceof DiningReservation && object2 instanceof DiningReservation){
         reservation1=(DiningReservation) object1;
         reservation2=(DiningReservation) object2;
         if(reservation1.getDate()!=null){
            result=reservation1.getDate().compareTo(reservation2.getDate());
         }
         
      }else if(object1 instanceof ShorexExcursions && object2 instanceof DiningReservation){
         excursion1=(ShorexExcursions) object1;
         reservation1=(DiningReservation) object2;
         if(excursion1.getDate()!=null){
            result=excursion1.getDate().compareTo(reservation1.getDate());
         }
         if(result==0){
            result=-1;
         }
         
      }
      else if(object1 instanceof DiningReservation && object2 instanceof ShorexExcursions){
         reservation1=(DiningReservation) object1;
         excursion1=(ShorexExcursions) object2;;
         if(reservation1.getDate()!=null){
            result=reservation1.getDate().compareTo(excursion1.getDate());
         }
         if(result==0){
            result=1;
         }
         
      }
      
     /* if(excursion1!=null && excursion2!=null){
         result=excursion1.getDate().compareTo(excursion2.getDate());
      }else if(reservation1!=null && reservation2!=null){
            result=reservation1.getDate().compareTo(reservation2.getDate());
         
      }else if(excursion1!=null && reservation1!=null){
         result=excursion1.getDate().compareTo(reservation1.getDate());
         
      }*/
      
         
      
      
      return result;
   }

}

   
