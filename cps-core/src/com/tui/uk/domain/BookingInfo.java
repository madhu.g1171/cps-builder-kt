/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BookingInfo.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-15 08:25:26 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
 */
package com.tui.uk.domain;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.Objects;

import com.tui.uk.client.domain.*;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import com.tui.uk.config.ConfReader;
import com.tui.uk.exception.ConfReaderException;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.validation.validator.SafeHTMLValidator;

/**
 * This class contains all the booking information. It holds the <code>BookingComponent</code>
 * object, which is sent by the client application. Additionally, it contains data that are
 * manipulated in the CPS during transactions.
 *
 * @author sindhushree.g
 *
 */
public final class BookingInfo
{

   /** The <code>BookingComponent</code> object. */
   private BookingComponent bookingComponent;

   /** The calculated Payable amount. */
   private Money calculatedPayableAmount;

   /** The calculated Total amount. */
   private Money calculatedTotalAmount;

   /** The calculated discount amount. */
   private Money calculatedDiscount;

   /** The calculated card charge. */
   private Money calculatedCardCharge;

   /** For newHoliday. */
   private Boolean newHoliday = Boolean.TRUE;

   /** For new HPS set req. */
   private Boolean newSetupReq = Boolean.TRUE;

   public Boolean getNewSetupReq()
   {
      return newSetupReq;
   }

   public void setNewSetupReq(Boolean newSetupReq)
   {
      this.newSetupReq = newSetupReq;
   }

   /** For 3D authentication. */
   private Boolean threeDAuth = Boolean.FALSE;

   /** For DatacashEnabled. */
   private Boolean datacashEnabled;

   /**
    * The applied card charge percent.This value is taken from the conf file. As of now the card
    * charge percentage is 2.5% if the card charge is applicable.
    */
   private String cardChargeDetails;

   /** An array of allowed payment types. */
   private String[] paymentTypes;

   /** A key-value pair of allowed card type and card charges. */
   // private Map<String, Object[]> allowedCards;

   /** An array of post payment guarantee card types. */
   private String[] guaranteeCardTypes;

   /** The error fields name. */
   private String errorFields;

   /** The datacash vTid. */
   private String datacashVTid;

   /** The transaction tracking data. */
   private TransactionTrackingData trackingData;

   /** The eachLogoList holds categories of Card Type. */
   private Map<String, List<String>> eachLogoList;

   /** The paymentDataMap holds payment data. */
   private Map<String, String> paymentDataMap = new HashMap<String, String>();

   /** The card charge map. */
   private Map<String, String> cardChargeMap = new HashMap<String, String>();

   /** A key-value pair of allowed card type and card charges. */
   private Map<String, Object[]> allowedCards;

   /** The fraud screening response. */
   private String fraudScreeningResponse;

   /** Constant for year pattern. */
   private static final String YEAR_PATTERN = "yy";

   /** The comma separator. */
   private static final String COMMA_SEPARATOR = ",";

   /** Index for Three. */
   private static final int INDEX_THREE = 3;

   /** Constant for client domain url. */
   private static final String CLIENT_DOMAIN_URL = "client_domain_url";

   /** The DOT seperator. */
   private static final String DOT = ".";

   /** Constant for datacash reference index. */
   private static final int DATCASHREF = 2;

   /** Constant for maskedCardNum index. */
   private static final int MASKEDCARDNUM = 4;

   /** Constant for paymentType index. */
   private static final int PAYTYPE = 6;

   /** Constant for payment method index. */
   private static final int PAYMETHOD = 7;

   /** Constant for payment name index. */
   private static final int PAYNAME = 3;

   /** Constant for cardExp index. */
   private static final int CARDEXP = 5;

   /** Constant for refEnabled index. */
   private static final int REFENABLED = 8;

   /** Constant for integratedRef index. */
   private static final int INTEGRATEDREF = 9;

   /** The balance amount for the deposit. */
   public Money ddOutstandingBalanceForHy;

   /** The due date for the deposit. */
   public Date depositDueDateforDD;

   public Money getDdOutstandingBalanceForHy()
   {
      return ddOutstandingBalanceForHy;
   }

   public void setDdOutstandingBalanceForHy(Money ddOutstandingBalanceForHy)
   {
      this.ddOutstandingBalanceForHy = ddOutstandingBalanceForHy;
   }

   /*
    * public Money getDdOutstandingBalance() { return ddOutstandingBalance; }
    * 
    * public void setDdOutstandingBalance(Money ddOutstandingBalance) { this.ddOutstandingBalance =
    * ddOutstandingBalance; }
    */

   public Date getDepositDueDateforDD()
   {
      return depositDueDateforDD;
   }

   public void setDepositDueDateforDD(Date depositDueDateforDD)
   {
      this.depositDueDateforDD = depositDueDateforDD;
   }

   static
   {
      // Initialize the card data.
      CardData.populateCardData();
   }

   /**
    * Constructor for BookingInfo.
    *
    * @param bookingComponent The <code>BookingComponent</code> object.
    */
   public BookingInfo(BookingComponent bookingComponent)
   {
      this.bookingComponent = bookingComponent;
      // set the discount amount to zero, by default.
      calculatedDiscount =
         new Money(BigDecimal.ZERO, bookingComponent.getTotalAmount().getCurrency());
      calculatedCardCharge =
         new Money(BigDecimal.ZERO, bookingComponent.getTotalAmount().getCurrency());
      setCardChargeDetails(cardChargeDefault());
      populateCardChargeForPaymentOptions();

      String operatingBrand = null;
      if (bookingComponent.getOperatingBrand() != null)
      {
         operatingBrand = bookingComponent.getOperatingBrand().getClientApplicationName();
      }
      updatePaymentInfo(operatingBrand);

      // validates the holiday benefits. If an unsafe html is passed,
      // the benefits are cleared from accomodationSummary (ie the
      // content is reset to "" in the map corresponding to the erring benefit key)
      if (null != bookingComponent.getAccommodationSummary())
      {
         if (null != bookingComponent.getAccommodationSummary().getAdditionalDetails())
         {
            if (!bookingComponent.getAccommodationSummary().getAdditionalDetails().isEmpty())
            {
               SafeHTMLValidator.validateBenefitsContent(bookingComponent);
            }
         }
      }
      // validates the cruise additional details. If an unsafe html is passed,
      // the details are cleared from cruiseSummary (i.e. the
      // content is reset to "" in the map corresponding to the erring details key)
      if (null != bookingComponent.getCruiseSummary())
      {
         if (null != bookingComponent.getCruiseSummary().getAdditionalDetails())
         {
            if (!bookingComponent.getCruiseSummary().getAdditionalDetails().isEmpty())
            {
               SafeHTMLValidator.validateCruiseAdditionalDetails(bookingComponent);
            }
         }
      }
     boolean summaryorderflag=ConfReader.getBooleanEntry("tuithshorex.summaryorder.enable", false);
      if("TUITHSHOREX".equalsIgnoreCase(bookingComponent.getClientApplication().getClientApplicationName()) && summaryorderflag)
      {
         setFormatedDateInExcursAndReserComp(bookingComponent);
         setDaysInExcursionAndReservationComponents(bookingComponent);
         bookingComponent.getCruiseSummary().getExcursionSummary().sortAllSummmaryElements();
      }
   }

   /**
    * Constructor for .NET clients.
    *
    * @param currency the currency.
    * @param countryLocale the country name for the locale.
    * @param clientApplication the client application name.
    * @param datacashVTid the datacash vTid.
    * @param allowedAmounts the allowed amounts.
    * @param paymentTypes the payment types.
    * @param applicationData the application data.
    */
   public BookingInfo(String currency, String countryLocale, String clientApplication,
                      String datacashVTid, Object[] allowedAmounts, Object[] paymentTypes,
                      Map<String, String> applicationData)
   {
      Money amount = new Money(BigDecimal.valueOf((Double) allowedAmounts[0]),
         Currency.getInstance(currency), new Locale(Locale.ENGLISH.getLanguage(), countryLocale));

      bookingComponent = new BookingComponent(ClientApplication.findByCode(clientApplication),
         applicationData.get(CLIENT_DOMAIN_URL), amount);
      bookingComponent.setPayableAmount(amount);
      bookingComponent.setNonPaymentData(applicationData);

      this.datacashVTid = datacashVTid;

      bookingComponent.setPaymentGatewayVirtualTerminalId(datacashVTid);

      this.paymentTypes = new String[paymentTypes.length];
      for (int i = 0; i < paymentTypes.length; i++)
      {
         this.paymentTypes[i] = (String) paymentTypes[i];
      }

      BookingApplicationData.updateBookingComponent(bookingComponent, applicationData);

      // set the discount amount to zero, by default.
      calculatedDiscount =
         new Money(BigDecimal.ZERO, bookingComponent.getTotalAmount().getCurrency());
      calculatedCardCharge =
         new Money(BigDecimal.ZERO, bookingComponent.getTotalAmount().getCurrency());
      calculatedPayableAmount = bookingComponent.getPayableAmount();
      calculatedTotalAmount = bookingComponent.getTotalAmount();

      updatePaymentInfo(null);

      if (bookingComponent.getClientApplication().getClientApplicationName()
         .equalsIgnoreCase("NEWSKIES")
         || bookingComponent.getClientApplication().getClientApplicationName()
            .equalsIgnoreCase("NEWSKIESM"))
      {
         String confFile = "countrylist";
         Properties countryListPropsACSS = new Properties();
         HashMap<String, String> countryListMapACSS = null;
         try
         {
            countryListPropsACSS = ConfReader.readPropertyFile(ConfReader.getRealPathForACSS()
               + System.getProperty(confFile + ".file.path", "../../conf/" + confFile + ".conf"));
            countryListMapACSS = new HashMap<String, String>((Map) countryListPropsACSS);
            List list = new LinkedList(countryListMapACSS.entrySet());
            // sort list based on comparator
            Collections.sort(list, new Comparator()
            {
               public int compare(Object o1, Object o2)
               {
                  return ((Comparable) ((Map.Entry) (o1)).getValue())
                     .compareTo(((Map.Entry) (o2)).getValue());
               }
            });
            // put sorted list into map again
            Map sortedMap = new LinkedHashMap();
            for (Iterator it = list.iterator(); it.hasNext();)
            {
               Map.Entry entry = (Map.Entry) it.next();
               sortedMap.put(entry.getKey(), entry.getValue());

            }

            bookingComponent.setCountryMap(sortedMap);

         }
         catch (ConfReaderException cre)
         {
            LogWriter.logErrorMessage("ConfReaderException", cre);
         }

      }
   }

   /**
    * Constructor to perform refund.
    *
    * @param currency the currency.
    * @param countryLocale the country name for the locale.
    * @param clientApplication the client application name.
    * @param datacashVTid the datacash vTid.
    * @param refundAmount the allowed refundAmount.
    * @param refundDetails the refundDetails.
    * @param paymentTypes the paymentTypes .
    * @param applicationData the application data.
    */
   public BookingInfo(String currency, String countryLocale, String clientApplication,
                      String datacashVTid, String refundAmount, Object[] refundDetails,
                      Object[] paymentTypes, Map<String, String> applicationData)
   {
      Money amount = new Money(BigDecimal.valueOf(Double.parseDouble(refundAmount)),
         Currency.getInstance(currency), new Locale(Locale.ENGLISH.getLanguage(), countryLocale));
      bookingComponent = new BookingComponent(ClientApplication.findByCode(clientApplication),
         applicationData.get(CLIENT_DOMAIN_URL), amount);
      bookingComponent.setPayableAmount(amount);
      bookingComponent.setNonPaymentData(applicationData);
      this.datacashVTid = datacashVTid;
      bookingComponent.setPaymentGatewayVirtualTerminalId(datacashVTid);

      BookingData bookingData =
         new BookingData(applicationData.get("reservation_id"), "CONFIRMED", new Date());
      BookingFlowDetails bookingFlowDetails =
         new BookingFlowDetails(BookingFlow.OLBP, PaymentFlow.REFUND);
      BookingApplicationData.updateBookingComponent(bookingComponent, applicationData);
      bookingComponent.setBookingData(bookingData);
      bookingComponent.getBookingData().setBookingFlowDetails(bookingFlowDetails);
      calculatedTotalAmount = bookingComponent.getTotalAmount();

      List<PaymentType> paymentTypesList = new ArrayList<PaymentType>();

      for (int i = 0; i < paymentTypes.length; i++)
      {
         paymentTypesList.add(new PaymentType("", "", paymentTypes[i].toString()));
      }
      if (bookingComponent.getPaymentType() == null)
      {
         bookingComponent.setPaymentType(new HashMap<String, List<PaymentType>>());
      }
      bookingComponent.getPaymentType().put(PaymentMode.CUSTOMERNOTPRESENT.toString(),
         paymentTypesList);

      if (bookingComponent.getRefundDetails() == null)
      {
         bookingComponent.setRefundDetails(new ArrayList<RefundDetails>());
      }

      for (Object refundDet : refundDetails)
      {
         Object[] refundDetail = (Object[]) refundDet;
         Money amountRefund =
            new Money(BigDecimal.valueOf(Double.parseDouble(refundDetail[0].toString())),
               Currency.getInstance(currency),
               new Locale(Locale.ENGLISH.getLanguage(), countryLocale));

         String date = (String) refundDetail[1];
         String datacashReference = (String) refundDetail[DATCASHREF];
         String maskedCardNumber = (String) refundDetail[MASKEDCARDNUM];
         String paymentType = (String) refundDetail[PAYTYPE];
         String paymentMethod = (String) refundDetail[PAYMETHOD];
         String originalPaymentMode = null;
         // following code has been added by sreenivasulu k for paypal refund service.
         if (isOriginalPaymentModeSet(refundDetail))
         {
            originalPaymentMode = String.valueOf(refundDetail[10]);
         }

         Date paymentDate = new Date();
         Date cardExpDate = new Date();

         try
         {
            SimpleDateFormat transactionDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat expiryDateFormat = new SimpleDateFormat("MM/yyyy");
            paymentDate = transactionDateFormat.parse(refundDetail[1].toString());
            cardExpDate = expiryDateFormat.parse(refundDetail[CARDEXP].toString());
         }
         catch (Exception ex)
         {
            LogWriter.logInfoMessage(ex.toString());
         }

         RefundDetails refundDetailsObj =
            new RefundDetails(datacashReference, amountRefund, paymentDate, maskedCardNumber);
         refundDetailsObj.setPayeeName((String) refundDetail[PAYNAME]);
         refundDetailsObj.setCardExpiry(cardExpDate);
         refundDetailsObj.setPaymentMethod(paymentMethod);
         refundDetailsObj.setPaymentType(paymentType);
         refundDetailsObj.setRefundEnabled(Boolean.valueOf(refundDetail[REFENABLED].toString()));
         refundDetailsObj
            .setIntegratedRefund((Boolean.valueOf(refundDetail[INTEGRATEDREF].toString())));
         refundDetailsObj.setOriginalPaymentMode(originalPaymentMode);
         bookingComponent.getRefundDetails().add(refundDetailsObj);

      }

   }

   /**
    * Check if originalPaymentMode attribute has been set in the Refund request in refundDetails
    * array
    * 
    * @param refundDetail
    * @return
    */
   private boolean isOriginalPaymentModeSet(Object[] refundDetail)
   {

      return refundDetail.length > 10;
   }

   /**
    * Gets the <code>BookingComponent</code> object.
    *
    * @return the bookingComponent.
    */
   public BookingComponent getBookingComponent()
   {
      return bookingComponent;
   }

   /**
    * Gets the Calculated Payable Amount.
    *
    * @return the CalculatedPayableAmount
    */
   public Money getCalculatedPayableAmount()
   {
      return calculatedPayableAmount;
   }

   /**
    * Gets the Calculated Total Amount.
    *
    * @return the CalculatedTotalAmount
    */
   public Money getCalculatedTotalAmount()
   {
      return calculatedTotalAmount;
   }

   /**
    * Gets the calculated discount.
    *
    * @return the calculatedDiscountAmount.
    */
   public Money getCalculatedDiscount()
   {
      return calculatedDiscount;
   }

   /**
    * Gets the calculated card charge.
    *
    * @return the calculatedCardCharge.
    */
   public Money getCalculatedCardCharge()
   {
      return calculatedCardCharge;
   }

   /**
    * Gets if the holiday is new or not.
    *
    * @return the newHoliday.
    */
   public Boolean getNewHoliday()
   {
      return newHoliday;
   }

   /**
    * Gets if the authentication is done or not.
    *
    * @return the threeDAuth.
    */
   public Boolean getThreeDAuth()
   {
      return threeDAuth;
   }

   /**
    * Gets the DatacashEnabled status.
    *
    * @return datacashEnabled status.
    */
   public boolean getDatacashEnabled()
   {
      return datacashEnabled;
   }

   /**
    * Gets the card charge percent.
    *
    * @return the cardChargeDetails.
    */
   public String getCardChargeDetails()
   {
      return cardChargeDetails;
   }

   /**
    * Gets the payment types.
    *
    * @return the paymentTypes.
    */
   public String[] getPaymentTypes()
   {
      return paymentTypes.clone();
   }

   /**
    * Gets the allowed cards.
    *
    * @return the allowedCards.
    */
   /*
    * public Map<String, Object[]> getAllowedCards() { return allowedCards; }
    */

   /**
    * Gets the guarantee card types.
    *
    * @return the guaranteeCardTypes.
    */
   public String[] getGuaranteeCardTypes()
   {
      return guaranteeCardTypes.clone();
   }

   /**
    * Gets the error fields.
    *
    * @return the errorFields.
    */
   public String getErrorFields()
   {
      return errorFields;
   }

   /**
    * Gets the datacash vTid.
    *
    * @return the datacashVTid.
    */
   public String getDatacashVTid()
   {
      return datacashVTid;
   }

   /**
    * Gets the tracking data.
    *
    * @return the tracking data.
    */
   public TransactionTrackingData getTrackingData()
   {
      return trackingData;
   }

   /**
    * Gets the payment data.
    *
    * @return the payment data.
    */
   public Map<String, String> getPaymentDataMap()
   {
      return paymentDataMap;
   }

   /**
    * Gets the card charge map.
    *
    * @return the card charge map.
    */
   public Map<String, String> getCardChargeMap()
   {
      return cardChargeMap;
   }

   /**
    * Gets the fraud screening response.
    *
    * @return the fraud screening response.
    */
   public String getFraudScreeningResponse()
   {
      return fraudScreeningResponse;
   }

   /**
    * Set the calculatedTotalAmount.
    *
    * @param calculatedTotalAmount the calculatedTotalAmount to set
    */
   public void setCalculatedTotalAmount(Money calculatedTotalAmount)
   {
      this.calculatedTotalAmount = calculatedTotalAmount;
   }

   /**
    * Set the calculatedPayableAmount.
    *
    * @param calculatedPayableAmount the calculatedPayableAmount to set
    */
   public void setCalculatedPayableAmount(Money calculatedPayableAmount)
   {
      this.calculatedPayableAmount = calculatedPayableAmount;
   }

   /**
    * Sets the calculated card charge.
    *
    * @param calculatedCardCharge to set.
    */
   public void setCalculatedCardCharge(Money calculatedCardCharge)
   {
      this.calculatedCardCharge = calculatedCardCharge;
   }

   /**
    * Sets id the holiday is new or not.
    *
    * @param newHoliday the newHoliday to set.
    */
   public void setNewHoliday(Boolean newHoliday)
   {
      this.newHoliday = newHoliday;
   }

   /**
    * Sets the 3D authentication flag.
    *
    * @param threeDAuth the three authorization to set.
    */
   public void setThreeDAuth(Boolean threeDAuth)
   {
      this.threeDAuth = threeDAuth;
   }

   /**
    * Sets the DatacashEnabled status.
    *
    * @param datacashEnabled the datacashEnabled status.
    */
   public void setDatacashEnabled(boolean datacashEnabled)
   {
      this.datacashEnabled = datacashEnabled;
   }

   /**
    * Sets the card charge percent.
    *
    * @param cardChargeDetails the cardChargePercent to set.
    */
   public void setCardChargeDetails(String cardChargeDetails)
   {
      this.cardChargeDetails = cardChargeDetails;
   }

   /**
    * Sets the tracking data.
    *
    * @param trackingData the trackingData to set.
    */
   public void setTrackingData(TransactionTrackingData trackingData)
   {
      this.trackingData = trackingData;
   }

   /**
    * Sets the allowed cards.
    *
    * @param allowedCards the allowedCards to set.
    */
   public void setAllowedCards(Map<String, Object[]> allowedCards)
   {
      this.allowedCards = new TreeMap<String, Object[]>();
      this.allowedCards.putAll(allowedCards);
      if (bookingComponent.getPaymentType() == null)
      {
         getPaymentTypeList();
      }
      if (cardChargeMap == null || cardChargeMap.isEmpty())
      {
         populateCardChargeMapFromAllowedCards();
      }
   }

   /**
    * Sets the guarantee card types.
    *
    * @param guaranteeCardTypes the guaranteeCardTypes to set
    */
   public void setGuaranteeCardTypes(Object[] guaranteeCardTypes)
   {
      this.guaranteeCardTypes = (String[]) guaranteeCardTypes.clone();
   }

   /**
    * Sets the error field names.
    *
    * @param errorFields the errorFields to set.
    */
   public void setErrorFields(String errorFields)
   {
      this.errorFields = errorFields;
   }

   /**
    * Sets the payment data.
    *
    * @param paymentDataMap the payment data map to set.
    */
   public void setPaymentDataMap(Map<String, String> paymentDataMap)
   {
      this.paymentDataMap = paymentDataMap;
   }

   /**
    * Sets the fraud screening response.
    *
    * @param fraudScreeningResponse the fraud screening response.
    */
   public void setFraudScreeningResponse(String fraudScreeningResponse)
   {
      this.fraudScreeningResponse = fraudScreeningResponse;
   }

   /**
    * The method to update discount.
    *
    * @param discount the discount amount.
    * @param discountType the PriceBeat string.
    * @param balanceType the balanceType like low deposit,deposit and full balance.
    */
   public void updateDiscount(Money discount, String discountType, String balanceType)
   {
      updateCalculatedDiscount();
      updateDiscountComponent(discount, discountType);
      updateAmount(discount, balanceType);
   }

   /**
    * Calculates the percentage of discount given for this package, based upon total amount to be
    * paid and discount offered and adds calculated "Discount %" to <tt>PriceComponent</tt>, if the
    * PriceComponent for "Discount %" does not exist.
    *
    * @param totalDiscountAmount the total discount amount.
    * @return percentDiscount the discount percentage.
    */
   public Money calculateDiscountPercentage(Money totalDiscountAmount)
   {
      double totalDiscount =
         (totalDiscountAmount.getAmount().doubleValue() * DispatcherConstants.PERCENTAGE_FACTOR)
            / this.calculatedTotalAmount.getAmount().doubleValue();
      return new Money(BigDecimal.valueOf(totalDiscount), calculatedTotalAmount.getCurrency());
   }

   /**
    * Adds priceBeatAmount entered by user to a new PriceComponent if the PriceComponent for
    * priceBeat does not exist.
    *
    * @param discount the discount amount.
    * @param discountType the PriceBeat string.
    */
   public void updateDiscountComponent(Money discount, String discountType)
   {
      // Using Iterator to avoid ConcurrentModificationExceptions.
      Iterator<PriceComponent> priceComponents = bookingComponent.getPriceComponents().iterator();
      while (priceComponents.hasNext())
      {
         PriceComponent priceComponent = priceComponents.next();
         if (priceComponent.getItemDescription().equalsIgnoreCase(discountType))
         {
            bookingComponent.getPriceComponents().remove(priceComponent);
            break;
         }
      }

      if (discount.getAmount().doubleValue() != 0.0)
      {
         PriceComponent priceComponent = new PriceComponent(discountType, discount);
         priceComponent.setDiscount(Boolean.TRUE);
         bookingComponent.getPriceComponents().add(priceComponent);
      }
   }

   /**
    * Returns updated totalAmount after subtracting it from discount offered.
    *
    * @param discount discount amount.
    * @param balanceType the balanceType.
    */
   private void updateAmount(Money discount, String balanceType)
   {
      // Reset the total amount by adding the original discount.
      calculatedTotalAmount = calculatedTotalAmount.add(calculatedDiscount);

      // Update the calculated discount.
      updateCalculatedDiscount();

      // update the total amount.
      if (calculatedTotalAmount.getAmount().doubleValue() > discount.getAmount().doubleValue())
      {
         calculatedTotalAmount = calculatedTotalAmount.subtract(calculatedDiscount);
      }
      if (bookingComponent.getPayableAmount().getAmount().doubleValue() > discount.getAmount()
         .doubleValue()
         && StringUtils.equalsIgnoreCase(balanceType, DispatcherConstants.FULL_BALANCE))
      {
         calculatedPayableAmount = calculatedTotalAmount;
      }

   }

   /**
    * Updates the calculated discount amount with the default value.
    */
   private void updateCalculatedDiscount()
   {
      calculatedDiscount =
         new Money(BigDecimal.ZERO, bookingComponent.getTotalAmount().getCurrency());
      for (PriceComponent priceComponent : bookingComponent.getPriceComponents())
      {
         // TODO:Remove the hard coded.
         if (priceComponent.getDiscount()
            && !priceComponent.getItemDescription().contains("Discount %"))
         {
            calculatedDiscount = priceComponent.getAmount().add(calculatedDiscount);
         }
      }
   }

   /**
    * Gets the updated card charge.
    *
    * @param selectedCard the selected card.
    * @param totAmtWithoutCardCharge amount for which card charges should be calculated.
    *
    * @return the calculated card charge.
    */
   public BigDecimal getUpdatedCardCharge(String selectedCard, Money totAmtWithoutCardCharge)
   {
      BigDecimal updatedCardCharge = BigDecimal.ZERO;
      String cardChargeInfo = cardChargeMap.get(selectedCard);
      if (cardChargeInfo != null)
      {
         updatedCardCharge = getCardCharge(totAmtWithoutCardCharge, cardChargeInfo);
         calculatedCardCharge = calculatedCardCharge
            .add(new Money(updatedCardCharge.abs(), calculatedTotalAmount.getCurrency()));
      }
      calculatedPayableAmount = calculatedPayableAmount
         .add(new Money(updatedCardCharge.abs(), calculatedTotalAmount.getCurrency()));
      calculatedTotalAmount = calculatedTotalAmount
         .add(new Money(updatedCardCharge.abs(), calculatedTotalAmount.getCurrency()));
      return updatedCardCharge;
   }

   /**
    * Resets the card charges to zero.
    */
   public void resetCardCharge()
   {
      calculatedPayableAmount = calculatedPayableAmount.subtract(calculatedCardCharge);
      calculatedTotalAmount = calculatedTotalAmount.subtract(calculatedCardCharge);
      calculatedCardCharge = new Money(BigDecimal.ZERO, calculatedTotalAmount.getCurrency());
   }

   /**
    * Resets the calculatedPayableAmount to zero.
    */
   public void resetPayableAmount()
   {
      calculatedPayableAmount = new Money(BigDecimal.ZERO, calculatedTotalAmount.getCurrency());
   }

   /**
    * Gets a list of expiry years that should be populated in the expiry year drop down.
    *
    * @return list of years for expiry year.
    */
   public List<String> getExpiryYearList()
   {
      List<String> expiryYearList = new ArrayList<String>();

      Calendar cal = Calendar.getInstance();

      expiryYearList.add(getFormattedYear(cal.getTime()));

      for (int i = 0; i < ConfReader.getIntEntry("expiry.year.count", 0) - 1; i++)
      {
         cal.add(Calendar.YEAR, 1);
         expiryYearList.add(getFormattedYear(cal.getTime()));
      }

      return expiryYearList;
   }

   /**
    * Gets a list of start years that should be populated in the start year drop down.
    *
    * @return list of years for start year.
    */
   public List<String> getStartYearList()
   {
      List<String> startYearList = new ArrayList<String>();

      int startYearCount = ConfReader.getIntEntry("start.year.count", 0) - 1;

      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.YEAR, -startYearCount);

      startYearList.add(getFormattedYear(cal.getTime()));

      for (int i = 0; i < startYearCount; i++)
      {
         cal.add(Calendar.YEAR, 1);
         startYearList.add(getFormattedYear(cal.getTime()));
      }

      return startYearList;
   }

   /**
    * Gets the year in 2 digit format.
    *
    * @param yearDate the year date to be formatted.
    *
    * @return year in proper format.
    */
   private String getFormattedYear(Date yearDate)
   {
      return DateFormatUtils.format(yearDate, YEAR_PATTERN);
   }

   /**
    * Resets all the amounts.
    */
   public void reset()
   {
      if (calculatedPayableAmount == null)
      {
         calculatedPayableAmount = bookingComponent.getPayableAmount();
      }
      if (calculatedTotalAmount == null)
      {
         calculatedTotalAmount = bookingComponent.getTotalAmount().subtract(calculatedDiscount);
      }
      if (calculatedCardCharge != null)
      {
         resetCardCharge();
      }
   }

   /**
    * Updates the card charge.
    *
    * @param selectedCard the payment type code of the selected card.
    */
   /*
    * public void updateAllowedCardCharges(String selectedCard) { BigDecimal cardCharge =
    * BigDecimal.ZERO;
    * 
    * Object[] applicableCharges = allowedCards.get(selectedCard);
    * 
    * for (Object[] allowedCardValues : allowedCards.values()) { if (allowedCardValues.length >
    * INDEX_THREE && selectedCard .equalsIgnoreCase((String) allowedCardValues[INDEX_THREE])) {
    * applicableCharges = allowedCardValues; break; } }
    * 
    * if ((Double) applicableCharges[0] > 0.0) { cardCharge =
    * (calculatedPayableAmount.getAmount().multiply(BigDecimal .valueOf((Double)
    * applicableCharges[0]))).divide(BigDecimal .valueOf(DispatcherConstants.PERCENTAGE_FACTOR)); }
    * if ((Double) applicableCharges[1] > 0.0 && cardCharge.doubleValue() < (Double)
    * applicableCharges[1]) { cardCharge = BigDecimal.valueOf((Double) applicableCharges[1]); } if
    * ((Double) applicableCharges[2] > 0.0 && cardCharge.doubleValue() > (Double)
    * applicableCharges[2]) { cardCharge = BigDecimal.valueOf((Double) applicableCharges[2]); }
    * 
    * cardCharge = cardCharge.setScale(2, BigDecimal.ROUND_HALF_UP);
    * 
    * calculatedCardCharge = new Money(cardCharge, calculatedTotalAmount.getCurrency());
    * calculatedPayableAmount = calculatedPayableAmount.add(calculatedCardCharge);
    * calculatedTotalAmount = calculatedTotalAmount.add(calculatedCardCharge); }
    */

   /**
    * This method is responsible for getting Logo name .
    *
    * @param cardName the card Name.
    * @return Name of Logo.
    */
   private String findLogo(String cardName)
   {
      for (Map.Entry<String, List<String>> entry : eachLogoList.entrySet())
      {
         for (String eachCard : entry.getValue())
         {
            if (eachCard.equalsIgnoreCase(cardName))
            {
               return entry.getKey();
            }
         }
      }
      return null;
   }

   /**
    * This method is responsible for getting 3d card categories.
    *
    */

   private void getThreeDCardCategorise()
   {
      if (eachLogoList == null)
      {
         eachLogoList = new HashMap<String, List<String>>();
         List<String> secureLogoApplicableCardGroup = new ArrayList<String>();
         // Load secure logo applicable card group like MasterCardGroup,VisaGroup.
         secureLogoApplicableCardGroup = getThreeDSchemes("securelogo.applicablecardgroup");
         // Load each associated list of logos
         if (secureLogoApplicableCardGroup != null)
         {
            // load eachlist cards like MasterCardLogoList :
            // mastercard,switch,maestro
            for (String eachLogoApplicableCardGroup : secureLogoApplicableCardGroup)
            {
               eachLogoList.put(eachLogoApplicableCardGroup,
                  getThreeDSchemes("securelogo." + eachLogoApplicableCardGroup));
            }
         }

      }
   }

   /**
    * This method is responsible for getting list of scheme from conf file.
    *
    * @param logoListName for the logoListName.
    * @return list of scheme.
    */
   private List<String> getThreeDSchemes(String logoListName)
   {
      List<String> schemeList = new ArrayList<String>();
      String[] schemes = ConfReader.getStringValues(logoListName, null, COMMA_SEPARATOR);
      if (schemes != null)
      {
         for (String scheme : schemes)
         {
            schemeList.add(StringUtils.lowerCase(scheme));
         }
      }
      return schemeList;
   }

   /**
    * This method is responsible for getting list of ThreeDEnabledLogos.
    *
    * @return list of scheme.
    */
   public Set<String> getThreeDEnabledLogos()
   {
      Set<String> threeDLogoList = new HashSet<String>();
      if (ConfReader.getBooleanEntry(
         bookingComponent.getClientApplication().getClientApplicationName() + ".3DSecure", false))
      {
         List<String> applicationThreeDEnabledList = new ArrayList<String>();
         String cardNameCode = null;

         // Load all card catageories
         getThreeDCardCategorise();
         // Load all cards enabled for 3d by the application.
         applicationThreeDEnabledList =
            getThreeDSchemes(getBookingComponent().getClientApplication().getClientApplicationName()
               + ".3DCardScheme");

         // Iterate to all cards set by client application and load Logos.
         for (PaymentType paymentType : bookingComponent.getPaymentType()
            .get(PaymentMode.CUSTOMERNOTPRESENT.getCode()))
         {
            cardNameCode = paymentType.getPaymentCode();
            for (String eachapplicationThreeDEnabledListCard : applicationThreeDEnabledList)
            {
               if (eachapplicationThreeDEnabledListCard
                  .equalsIgnoreCase(cardNameCode.replace('_', ' ')))
               {
                  threeDLogoList.add(findLogo(cardNameCode.replace('_', ' ')));
               }
            }
         }

      }
      return threeDLogoList;
   }

   /**
    * This method is responsible for getting payButton description.
    *
    * @return Map of description.
    */
   public Map<String, String> getPayDescription()
   {
      Map<String, String> payDescription = new HashMap<String, String>();
      if (ConfReader.getBooleanEntry(
         bookingComponent.getClientApplication().getClientApplicationName() + ".3DSecure", false))
      {
         List<String> applicationThreeDEnabledList = new ArrayList<String>();

         // Load all card catageories
         getThreeDCardCategorise();

         // Load all cards enabled for 3d by the application.

         String cardNameCode = null;
         applicationThreeDEnabledList =
            getThreeDSchemes(getBookingComponent().getClientApplication().getClientApplicationName()
               + ".3DCardScheme");
         for (PaymentType paymentType : bookingComponent.getPaymentType()
            .get(PaymentMode.CUSTOMERNOTPRESENT.getCode()))
         {
            cardNameCode = paymentType.getPaymentCode();
            for (String eachapplicationThreeDEnabledListCard : applicationThreeDEnabledList)
            {
               if (eachapplicationThreeDEnabledListCard
                  .equalsIgnoreCase(cardNameCode.replace('_', ' ')))
               {
                  payDescription.put(cardNameCode, findLogo(cardNameCode.replace('_', ' ')));
                  break;
               }
               else
               {
                  payDescription.put(cardNameCode, "");
               }
            }

         }
      }
      return payDescription;
   }

   /**
    * This method sets the applicable card charge for the payment options.
    *
    */
   private void populateCardChargeForPaymentOptions()
   {
      Currency currency = bookingComponent.getTotalAmount().getCurrency();
      List<DepositComponent> depositList = bookingComponent.getDepositComponents();

      /* following code added by sreenivasulu k for direct debit */

      Map<String, DepositComponent> depositMap = new HashMap<String, DepositComponent>();
      if (depositList != null)
      {

         for (DepositComponent depositComponent : depositList)
         {
            depositMap.put(depositComponent.getDepositType(), depositComponent);
         }

      }

      boolean directDebitVarian = false;
      if (bookingComponent.getDdVariant() != null && (StringUtils
         .equalsIgnoreCase(bookingComponent.getDdVariant().getCode(), DDVariant.V1.getCode())
         || StringUtils.equalsIgnoreCase(bookingComponent.getDdVariant().getCode(),
            DDVariant.V2.getCode())))
      {
         directDebitVarian = true;
      }

      DepositComponent lowDepositPlusDD = (DepositComponent) depositMap.get("lowDepositPlusDD");

      DepositComponent lowDeposit = (DepositComponent) depositMap.get("lowDeposit");

      DepositComponent deposit = (DepositComponent) depositMap.get("deposit");
      if (Objects.nonNull(lowDepositPlusDD) && !checkDDApplicableBrand(
         bookingComponent.getClientApplication().getClientApplicationName()))
      {
         depositList.remove(lowDepositPlusDD);
         lowDepositPlusDD = null;
      }
      if (Objects.nonNull(deposit) && Objects.nonNull(lowDeposit)
         && Objects.nonNull(lowDepositPlusDD))
      {
         depositDueDateforDD = deposit.getDepositDueDate();
         ddOutstandingBalanceForHy = deposit.getOutstandingBalance();

         depositList.remove(deposit);

      }

      /* above code is added by sreenivasulu K */

      if (depositList != null)
      {
         for (DepositComponent depositComponent : depositList)
         {
            BigDecimal applicableCardCharge =
               getCardCharge(depositComponent.getDepositAmount(), this.cardChargeDetails);
            depositComponent.setApplicableCardCharge(new Money(applicableCardCharge, currency));
            BigDecimal outstandingBalanceCardCharge =
               getCardCharge(depositComponent.getOutstandingBalance(), this.cardChargeDetails);
            depositComponent
               .setOutstandingBalanceCardCharge(new Money(outstandingBalanceCardCharge, currency));
            BigDecimal applicableDebitCardCharge =
               getCardCharge(depositComponent.getDepositAmount(),
                  ConfReader.getConfEntry("debitCardChargeDetails", "0.00,5.00,5.00"));
            depositComponent
               .setApplicableDebitCardCharge(new Money(applicableDebitCardCharge, currency));
         }
      }
   }

   /**
    * This method calculates the card charge for the amount and card charge percentage passed.
    *
    * @param amount the amount for which card charge needs to be calculated.
    * @param cardChargeInfo the card charge info - percent|min|max|desc.
    *
    * @return card charge the card charge amount.
    */
   private BigDecimal getCardCharge(Money amount, String cardChargeInfo)
   {
      BigDecimal cardCharge = BigDecimal.ZERO;
      if (amount != null)
      {
         String[] cardChargeData = cardChargeInfo.split(COMMA_SEPARATOR);
         cardCharge = new BigDecimal(cardChargeData[0]);
         cardCharge = cardCharge.multiply(amount.getAmount())
            .divide(BigDecimal.valueOf(DispatcherConstants.PERCENTAGE_FACTOR));

         BigDecimal minCardCharge = BigDecimal.valueOf(Double.valueOf(cardChargeData[1]));
         BigDecimal maxCardCharge = BigDecimal.valueOf(Double.valueOf(cardChargeData[2]));

         if (minCardCharge.doubleValue() > cardCharge.doubleValue())
         {
            cardCharge = minCardCharge;
         }

         if (maxCardCharge.doubleValue() < cardCharge.doubleValue()
            && maxCardCharge.doubleValue() > 0)
         {
            cardCharge = maxCardCharge;
         }
         cardCharge = cardCharge.setScale(2, BigDecimal.ROUND_HALF_UP);
      }
      return cardCharge;
   }

   // TODO : This method should be made as private, once both BRAC & WSS send operating brand info.
   /**
    * This method is responsible for updating the payment types, card charges from the conf file.
    *
    * @param operatingBrand the sub brand.
    */
   public void updatePaymentInfo(String operatingBrand)
   {
      Map<String, List<PaymentType>> paymentTypeMap = bookingComponent.getPaymentType();
      if (paymentTypeMap == null)
      {
         paymentTypeMap = new HashMap<String, List<PaymentType>>();
      }

      String clientApplicationName =
         bookingComponent.getClientApplication().getClientApplicationName();

      if (StringUtils.isNotEmpty(operatingBrand))
      {
         clientApplicationName = operatingBrand;
      }

      String currency = bookingComponent.getTotalAmount().getCurrency().getCurrencyCode();

      List<PaymentType> paymentTypeList = CardData.getPaymentTypes(clientApplicationName, currency);

      cardChargeMap = CardData.getCardChargeMap(clientApplicationName, currency);

      if (paymentTypeList.isEmpty())
      {
         if (cardChargeMap.isEmpty())
         {
            if (bookingComponent.getCardChargesMap() != null)
            {
               populateCardChargeFromBookingComponent(bookingComponent.getCardChargesMap());
            }
         }
      }
      else
      {
         List<PaymentType> cnpList = new ArrayList<PaymentType>();
         cnpList.addAll(paymentTypeList);

         // Get the list of payment types, other than Datacash types for CNP.
         // This would include GiftCard for Wish applications.
         List<PaymentType> nonDatacashPaymentTypes =
            getNonDataCashPaymentTypes(PaymentMode.CUSTOMERNOTPRESENT.getCode());

         // If there are any other payment types, sent by the client, then
         // add it to the CNP Payment types.
         if (nonDatacashPaymentTypes.size() > 0)
         {
            cnpList.addAll(nonDatacashPaymentTypes);
         }
         paymentTypeMap.put(PaymentMode.CUSTOMERNOTPRESENT.getCode(), cnpList);

         List<PaymentType> cpnaList = new ArrayList<PaymentType>();
         cpnaList.addAll(paymentTypeList);

         // If there is a CPNA entry sent by the client then add the cards given in the conf file
         // and the other payment types sent by the client application
         // to form the CPNA payment type list.
         if (bookingComponent.getPaymentType() != null && bookingComponent.getPaymentType()
            .get(PaymentMode.CHIPANDPINNOTAVAIL.getCode()) != null)
         {
            List<PaymentType> nonDatacashCPNAPayments =
               getNonDataCashPaymentTypes(PaymentMode.CHIPANDPINNOTAVAIL.getCode());
            if (nonDatacashCPNAPayments.size() > 0)
            {
               cpnaList.addAll(nonDatacashCPNAPayments);
            }
            paymentTypeMap.put(PaymentMode.CHIPANDPINNOTAVAIL.getCode(), cpnaList);
         }

         bookingComponent.setPaymentType(paymentTypeMap);
      }
   }

   /**
    * Gets the non data cash payment methods for the give payment mode.
    *
    * @param paymentMode the payment mode.
    *
    * @return the list of non datacash payment types.
    */
   private List<PaymentType> getNonDataCashPaymentTypes(String paymentMode)
   {
      List<PaymentType> paymentTypeList = new ArrayList<PaymentType>();
      if (bookingComponent.getPaymentType() != null
         && bookingComponent.getPaymentType().get(paymentMode) != null)
      {
         for (PaymentType paymentType : bookingComponent.getPaymentType().get(paymentMode))
         {
            if (!paymentType.getPaymentMethod().equalsIgnoreCase(PaymentMethod.DATACASH.getCode()))
            {
               paymentTypeList.add(paymentType);
            }
         }
      }
      return paymentTypeList;
   }

   /**
    * This method forms a map containing payment code as key and a comma seperated strings as value
    * which contains card charge percent, min card charge and max card charge.
    *
    * @param cardChargeValues is a Map with payment code and respective card charge percent.
    */
   private void populateCardChargeFromBookingComponent(Map<String, BigDecimal> cardChargeValues)
   {
      /* Forms the string of the form 2.50,0.00,95.00 */
      for (Map.Entry<String, BigDecimal> cardChargeEntry : cardChargeValues.entrySet())
      {
         BigDecimal maxCardCharge;
         StringBuilder cardChargeString = new StringBuilder();
         BigDecimal cardChargePercent =
            cardChargeEntry.getValue().setScale(2, BigDecimal.ROUND_HALF_UP);
         BigDecimal minCardCharge = BigDecimal.ZERO;
         if (bookingComponent.getMaxCardCharge() == null)
         {
            maxCardCharge = BigDecimal.ZERO;
         }
         else
         {
            maxCardCharge =
               bookingComponent.getMaxCardCharge().setScale(2, BigDecimal.ROUND_HALF_UP);

         }
         cardChargeString.append(cardChargePercent);
         cardChargeString.append(COMMA_SEPARATOR);
         cardChargeString.append(minCardCharge);
         cardChargeString.append(COMMA_SEPARATOR);
         cardChargeString.append(maxCardCharge);
         cardChargeMap.put(cardChargeEntry.getKey(), cardChargeString.toString());

      }
   }

   /**
    * This method forms a map containing payment code as key and a comma seperated strings as value
    * which contains card charge percent, min card charge and max card charge.
    *
    */
   private void populateCardChargeMapFromAllowedCards()
   {
      // Forms the string of the form 2.50,0.00,95.00
      for (Entry<String, Object[]> cardChargeEntry : this.allowedCards.entrySet())
      {
         StringBuilder cardChargeString = new StringBuilder();
         for (Object cardChargeData : cardChargeEntry.getValue())
         {
            if (cardChargeData instanceof Double)
            {
               Double cardCharge = (Double) cardChargeData;
               cardChargeString.append(cardCharge);
               cardChargeString.append(COMMA_SEPARATOR);
            }
            else if (cardChargeData instanceof Integer)
            {
               Integer cardCharge = (Integer) cardChargeData;
               cardChargeString.append(cardCharge);
               cardChargeString.append(COMMA_SEPARATOR);
            }
            else if (cardChargeData instanceof String)
            {
               String cardCharge = (String) cardChargeData;
               cardChargeString.append(cardCharge);
               cardChargeString.append(COMMA_SEPARATOR);
            }
         }
         cardChargeMap.put(cardChargeEntry.getKey(), cardChargeString.toString());
      }
   }

   /**
    * This method verifies if the clientApplication has got brand specific default card charge
    * entry.If there is one , then fetch the brand specific default card charge entry else fetch the
    * global default entry.
    *
    * @return cardChargeDefaultEntry which returns brand specific default entry or global default
    *         entry for all brands.
    */
   private String cardChargeDefault()
   {
      String cardDetailsForClient =
         ConfReader.getConfEntry(bookingComponent.getClientApplication().getClientApplicationName()
            + DOT + bookingComponent.getTotalAmount().getCurrency().getCurrencyCode() + DOT
            + "cardChargeDetails", null);

      if (cardDetailsForClient == null)
      {
         cardDetailsForClient = ConfReader.getConfEntry("cardChargeDetails", "2.00,0.00,95.00");

      }
      return cardDetailsForClient;
   }

   /**
    * Gets the card details - payment type code and payment description.
    *
    */
   private void getPaymentTypeList()
   {
      List<PaymentType> paymentTypeList = new ArrayList<PaymentType>();
      for (Entry<String, Object[]> allowedCardEntry : allowedCards.entrySet())
      {
         Object[] cardCharges = allowedCardEntry.getValue();
         String cardTypeCode = allowedCardEntry.getKey();
         StringBuilder handlingFee = new StringBuilder();

         if (cardCharges.length > INDEX_THREE)
         {
            cardTypeCode = (String) cardCharges[INDEX_THREE];
         }
         if ((Double) cardCharges[0] > 0.0)
         {
            handlingFee.append("  (+ ").append(cardCharges[0]).append("%").append(" Handling Fee)");
         }
         else
         {
            BigDecimal flatCharge = BigDecimal.valueOf((Double) cardCharges[1]);
            handlingFee.append(" (+ ").append(bookingComponent.getTotalAmount().getSymbol())
               .append(flatCharge.setScale(2)).append(" Handling Fee)");
         }

         // finding each card Min, Max CVV length.
         CardDetails cardDetail = CardDetails.findByCode(cardTypeCode);
         CardType cardType =
            new CardType(cardDetail.getMaxSecLength(), cardDetail.getMinSecLength());
         cardType.setIsIssueNumberRequired(cardDetail.getIssueNumberRequired());
         PaymentType paymentType = new PaymentType(cardTypeCode,
            allowedCardEntry.getKey() + handlingFee, PaymentMethod.DATACASH.getCode());
         paymentType.setCardType(cardType);
         paymentTypeList.add(paymentType);
      }

      Map<String, List<PaymentType>> paymentTypeMap = new HashMap<String, List<PaymentType>>();
      paymentTypeMap.put(PaymentMode.CUSTOMERNOTPRESENT.getCode(), paymentTypeList);
      bookingComponent.setPaymentType(paymentTypeMap);

   }

   private boolean checkDDApplicableBrand(String clientApp)
   {

      if (null != bookingComponent.getLockYourPriceSummary()
         && bookingComponent.getLockYourPriceSummary().isSelected()
         && bookingComponent.getLockYourPriceSummary().isMmbLockYourPriceFlow())
      {
         clientApp = bookingComponent.getLockYourPriceSummary().getClientApplication();
      }
      List<String> ddApplicableBrands =
         Arrays.asList(StringUtils.split(ConfReader.getConfEntry("dd.applicableBrands",
            "TUITH,TUIFC,TUITHCRUISE,THOMSONMOBILE,TUITHRIVERCRUISE"), COMMA_SEPARATOR));
      return ddApplicableBrands.contains(clientApp);
   }
   public void setDaysInExcursionAndReservationComponents(BookingComponent bookingComponent){
      
     Date date1=bookingComponent.getCruiseSummary().getExcursionSummary().getCruiseDepartureDate();
     List<ShorexExcursions> excursions=bookingComponent.getCruiseSummary().getExcursionSummary().getShorexExcursions();
      for(ShorexExcursions excursion:excursions){         
         excursion.setDays(getDays(date1,excursion.getDate()));
      }
     List<DiningReservation> reservations=bookingComponent.getCruiseSummary().getExcursionSummary().getDiningReservations();
     for(DiningReservation reservation:reservations){
        reservation.setDays(getDays(date1,reservation.getDate()));
     }
     
     
   }
   
   
   /**
    * @param bookingComponent2
    */
   private void setFormatedDateInExcursAndReserComp(BookingComponent bookingComponent2)
   {     
      List<ShorexExcursions> excursions=bookingComponent.getCruiseSummary().getExcursionSummary().getShorexExcursions();
       for(ShorexExcursions excursion:excursions){         
         excursion.setFormatedDateValue(this.getFormattedDate(excursion.getDate()));
       }
      List<DiningReservation> reservations=bookingComponent.getCruiseSummary().getExcursionSummary().getDiningReservations();
      for(DiningReservation reservation:reservations){
        reservation.setFormatedDateValue(this.getFormattedDate(reservation.getDate()));
      }
      
   }

   /**
    * @param date1
    * @param date
    * @return
    */
   private Integer getDays(Date date1, Date date2)
   {
        Integer result=null;
      if(date1!=null && date2!=null){
      long difference=date2.getTime()-date1.getTime();
      long days=TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
      result=Integer.valueOf(days+"");
      }
      return result;
   }
   
   public String getFormattedDate(Date date){
     int dayValue=date.getDay();
      Calendar cal=Calendar.getInstance();
      cal.setTime(date);
      SimpleDateFormat sdf=null;
      //2nd of march 2015
      int day=cal.get(Calendar.DATE);      
      switch (day % 10) {
      case 1:sdf=new SimpleDateFormat(" E dd'st' MMM yyyy");break;
      case 2:sdf=new SimpleDateFormat("E dd'nd' MMM yyyy");break;         
      case 3:sdf=new SimpleDateFormat("E dd'rd' MMM yyyy" );break;          
      default:sdf=new SimpleDateFormat("E dd'th' MMM yyyy");break;         
  }
  return sdf.format(date);
}

}
