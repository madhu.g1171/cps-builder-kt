/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingInfo.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-15 08:25:26 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
 */
package com.tui.uk.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.tui.uk.client.domain.CardType;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;

/**
 *
 * This class is responsible for populating the card payment types for each of the brands.
 * The data is being read from the carddetails.conf file.
 *
 * @author sindhushree.g
 *
 */
public final class CardData
{
   /** Index for Three. */
   private static final int INDEX_THREE = 3;

   /** Index for Four. */
   private static final int INDEX_FOUR = 4;

   /** The comma separator. */
   private static final String COMMA_SEPARATOR = ",";

   /** The dot separator. */
   private static final String DOT_SEPARATOR = ".";

   /** The under score separator. */
   private static final String UNDER_SCORE_SEPARATOR = "_";

   /** The Map containing client application_currency as key and the card charge map as value. */
   private static Map<String, Map<String, String>> cardChargeData =
       new HashMap<String, Map<String, String>>();

   /** The Map containing client application_currency as key and the list of payment types. */
   private static Map<String, List<PaymentType>> cardPaymentData =
       new HashMap<String, List<PaymentType>>();

   /**
    * Private Constructor to stop instantiation.
    */
   private CardData()
   {
      // Do nothing
   }

   /**
    * This method is responsible for populating the card data required for all the brands.
    */
   public static void populateCardData()
   {
      ClientApplication[] clientApplications = ClientApplication.values();
      String[] defaultCurrency = {"GBP"};

      for (ClientApplication clientApplication : clientApplications)
      {
         String clientApplicationName = clientApplication.getClientApplicationName();

         // Get the list of currency for each of the client applications.
         String[] currencyList = ConfReader.getStringValues(clientApplicationName + DOT_SEPARATOR
                 + "currency", defaultCurrency, COMMA_SEPARATOR);

         for (String currency : currencyList)
         {
            // Get the payment types from the conf file based on client name and currency as key.
            String[] paymentTypeArray = ConfReader.getStringValues(clientApplicationName
                    + DOT_SEPARATOR + currency + ".cnpcards", null, COMMA_SEPARATOR);

            List<PaymentType> paymentTypeList = new ArrayList<PaymentType>();
            Map<String, String> cardChargeMap = new HashMap<String, String>();

            if (paymentTypeArray != null)
            {
               // For each payment type, get the details from conf file and update the
               // card charge map.
               for (String paymentTypeCode : paymentTypeArray)
               {
                   String tempPaymentTypeCode = paymentTypeCode.replace(" " , "_");
                    String[] cardData = ConfReader.getStringValues(clientApplicationName
                          + DOT_SEPARATOR + currency + DOT_SEPARATOR
                           + tempPaymentTypeCode + ".details",
                          null , COMMA_SEPARATOR);



                  PaymentType paymentType = new PaymentType(paymentTypeCode, cardData[INDEX_THREE],
                          PaymentMethod.DATACASH.getCode());
                  CardDetails cardDetails = CardDetails.findByCode(paymentTypeCode);
                  CardType cardType = new CardType(cardDetails.getMaxSecLength(), cardDetails
                          .getMinSecLength());
                  cardType.setIsIssueNumberRequired(cardDetails.getIssueNumberRequired());
                  paymentType.setCardType(cardType);
                  paymentTypeList.add(paymentType);

                  if (cardData.length > INDEX_FOUR)
                  {
                      paymentType.setCardCategory(cardData[INDEX_FOUR]);
                  }

                  StringBuilder cardChargeValue = new StringBuilder();
                  for (String cardInfo : cardData)
                  {
                     cardChargeValue.append(cardInfo);
                     cardChargeValue.append(COMMA_SEPARATOR);
                  }
                  cardChargeMap.put(paymentTypeCode, cardChargeValue.toString());
               }
            }

            String[] cpPaymentTypeArray = ConfReader.getStringValues(clientApplicationName
                    + DOT_SEPARATOR + currency + ".cpcards", null, COMMA_SEPARATOR);

            if (cpPaymentTypeArray != null)
            {
                for (String paymentTypeCode : cpPaymentTypeArray)
                {
                    String tempPaymentTypeCode = paymentTypeCode.replace(" " , "_");
                     String[] cardData = ConfReader.getStringValues(clientApplicationName
                           + DOT_SEPARATOR + currency + DOT_SEPARATOR
                            + tempPaymentTypeCode + ".details",
                           null , COMMA_SEPARATOR);

                     StringBuilder cardChargeValue = new StringBuilder();
                     for (String cardInfo : cardData)
                     {
                        cardChargeValue.append(cardInfo);
                        cardChargeValue.append(COMMA_SEPARATOR);
                     }
                     cardChargeMap.put(paymentTypeCode, cardChargeValue.toString());
                }
            }

            LogWriter.logInfoMessage("Updating card data for " + clientApplicationName
                    + UNDER_SCORE_SEPARATOR + currency);
            cardPaymentData.put(clientApplicationName + UNDER_SCORE_SEPARATOR + currency,
                    paymentTypeList);
            cardChargeData.put(clientApplicationName + UNDER_SCORE_SEPARATOR + currency,
                    cardChargeMap);
         }
      }
   }

   /**
    * Gets the payment type list for given client application and currency.
    *
    * @param clientApplicationName the client application.
    * @param currency the currency.
    *
    * @return the list of payment types.
    */
   public static List<PaymentType> getPaymentTypes(String clientApplicationName, String currency)
   {
      return cardPaymentData.get(clientApplicationName + UNDER_SCORE_SEPARATOR + currency);
   }

   /**
    * Gets the payment type list for given client application and currency.
    *
    * @param clientApplicationName the client application.
    * @param currency the currency.
    *
    * @return the list of payment types.
    */
   public static Map<String, String> getCardChargeMap(String clientApplicationName, String currency)
   {
      return cardChargeData.get(clientApplicationName + UNDER_SCORE_SEPARATOR + currency);
   }

}
