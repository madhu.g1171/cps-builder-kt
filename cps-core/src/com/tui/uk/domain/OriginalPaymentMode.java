/**
 *
 */
package com.tui.uk.domain;

/**
 * @author sreenivasulu.k
 *
 */
public enum OriginalPaymentMode {

	 /** The DATACASH. */
	   PAYPAL("PayPal"),

	   /** The PDQ. */
	   CARD("Card");

	   /** The Payment code. */
	   private final String code;

	   /**
	    * The Constructor.
	    *
	    * @param code the Payment code
	    */
	   OriginalPaymentMode(String code)
	   {
	      this.code = code;
	   }

	   /**
	    * Gets the code.
	    *
	    * @return the code
	    */
		public String getCode() {
			return code;
		}


		/**
		    * This method will return the payment type based on the code passed. It will throw
		    * IllegalArgumentException if the code is not valid.
		    *
		    * @param code the Payment code
		    *
		    * @return the payment type
		    */
		   public static OriginalPaymentMode valueFromCode(String code)
		   {
		      for (OriginalPaymentMode type : OriginalPaymentMode.values())
		      {
		         if (type.code.equalsIgnoreCase(code.trim()))
		         {
		            return type;
		         }
		      }
		      throw new IllegalArgumentException("Unknown Payment Gate:" + code);
		   }

}
