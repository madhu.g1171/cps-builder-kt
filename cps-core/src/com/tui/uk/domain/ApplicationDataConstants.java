/**
 *
 */
package com.tui.uk.domain;

/**
 * Holds all application Map data constants of BookingInfo.
 * @author jaleelakbar.m
 *
 */
public final class ApplicationDataConstants
{
    /**
     * Private constructor to stop instantiation of this class.
     */
    private ApplicationDataConstants()
    {
        // Do nothing.
    }


   /** constant for accommodationSelected. */
   public static final String ACCOMMODATION_SELECTED = "accommodationSelected";

   /** constant for tAndCUrl. */
   public static final String TANDC_URL = "tAndCUrl";

   /** added for request element. */
   public static final String REQUEST="Request";

   /** added for Authentication element. */
   public static final String AUTHENTICATION="Authentication";

   /** added for client element. */
   public static final String CLIENT="client";

   /** added for password element. */
   public static final String PASSWORD="password";

   /** added for Transaction element. */
   public static final String TRANSACTION="Transaction";

   /** added for BatchInputTxn element. */
   public static final String BATCHINPUTTXN="BatchInputTxn";

   /** added for batchfile element. */
   public static final String BATCHFILE="batchfile";

   /** added for BatchInputRequest element. */
   public static final String BATCHINPUTREQUEST="BatchInputRequest";

   /** added for Transactions element. */
   public static final String TRANSACTIONS="Transactions";

   /** added for TxnDetails element. */
   public static final String TXNDETAILS="TxnDetails";

   /** added for merchantreference element. */
   public static final String MERCHANTREFERENCE="merchantreference";

   /** added for amount element. */
   public static final String AMOUNT="amount";

   /** added for HistoricTxn element. */
   public static final String HISTORICTXN="HistoricTxn";

   /** added for method element. */
   public static final String METHOD="method";

   /** added for reference element. */
   public static final String REFERENCE="reference";

   /** added for duedate element. */
   public static final String DUEDATE="duedate";

   /** added for txn_count element. */
   public static final String TXN_COUNT="txn_count";

   /** added for total_amount element. */
   public static final String TOTAL_AMOUNT="total_amount";

   /** added for tran_code element. */
   public static final String TRAN_CODE="tran_code";

   /** added for format element. */
   public static final String FORMAT="format";

   /** added for header element. */
   public static final String HEADER="Header";





}
