/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CardDetails.java,v $
 *
 * $Revision: 1.5 $
 *
 * $Date: 2009-07-22 08:25:26 $
 *
 * $Author: Jaleel $
 *
 *
 */
package com.tui.uk.domain;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.io.Serializable;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;

/**
 * Represents all the possible known card details.
 *
 * @author Jaleel
 *
 */
public enum CardDetails implements Serializable
{
   /** Max, Min ,issueNumber for ATM. */
   ATM("ATM", 3, 3, false),

   /** Max, Min ,issueNumber for DEBIT MASTERCARD. */
   DEBITMASTERCARD("Debit Mastercard", 3, 3, false),

   /** Max, Min ,issueNumber for DINERS CLUB. */
   DINERS_CLUB("Diners Club", 3, 3, false),

   /** Max, Min ,issueNumber for DISCOVER. */
   DISCOVER("Discover", 3, 3, false),

   /** Max, Min ,issueNumber for ENROUTE. */
   ENROUTE("EnRoute", 3, 3, false),

   /** Max, Min ,issueNumber for GE CAPITAL. */
   GE_CAPITAL("GE Capital", 3, 3, false),

   /** Max, Min ,issueNumber for JCB. */
   JCB("JCB", 3, 3, false),

   /** Max, Min ,issueNumber for PLATIMA. */
   PLATIMA("Platima", 3, 3, false),

   /** Max, Min ,issueNumber for Mastercard. */
   MASTERCARD("Mastercard", 3, 3, false),

   /** Max, Min,issueNumber for AMERICAN EXPRESS. */
   AMERICANEXPRESS("American Express", 4, 4, false),

   /** Max, Min,issueNumber for VISA DELTA. */
   VISA_DELTA("VISA Delta", 3, 3, false),

   /** Max, Min,issueNumber for VISA ELECTRON. */
   VISA_ELECTRON("VISA Electron", 3, 3, false),

   /** Max, Min,issueNumber for VISA PURCHASING. */
   VISA_PURCHASING("VISA Purchasing", 3, 3, false),

   /** Max, Min,issueNumber for SOLO. */
   SOLO("Solo", 3, 3, false),

   /** Max, Min,issueNumber for MAESTRO. */
   MAESTRO("Maestro", 3, 3, false),

   /** Max, Min,issueNumber for SWITCH. */
   SWITCH("Switch", 3, 3, false),

   /** Max, Min,issueNumber for LASER. */
   LASER("Laser", 3, 3, false),

   /** Max, Min,issueNumber for VISA. */
   VISA("VISA", 3, 3, false),
   
   /** Max, Min,issueNumber for TUI_MASTERCARD. */
   TUI_MASTERCARD("TUI MASTERCARD", 3, 3, false),
   
   /** Max, Min,issueNumber for TUI_MASTERCARD. */
   MASTERCARD_GIFT("MASTERCARD GIFT", 3, 3, false);
   
   /** The maximum security code length. */
   private int maxSecLength;

   /** The minimum security code length. */
   private int minSecLength;

   /** The boolean for whether issue number is required or not. */
   private boolean issueNumberRequired;

   /** The Card name. */
   private String cardType;


   /**
    * Construct the enumeration item.
    *
    * @param cardType the cardType.
    * @param maxSecLength the maxSecLength.
    * @param minSecLength the minSecLength.
    * @param issueNumberRequired the issueNumberRequired.
    */
   private CardDetails(String cardType, int maxSecLength,
      int minSecLength, boolean issueNumberRequired)
   {
      this.cardType = cardType;
      this.maxSecLength = maxSecLength;
      this.minSecLength = minSecLength;
      this.issueNumberRequired = issueNumberRequired;
   }

   /**
    * Gets the maximum security length.
    *
    * @return the maxSecLength.
    */
   public int getMaxSecLength()
   {
      return maxSecLength;
   }

   /**
    * Gets the minimum security length.
    *
    * @return the minSecLength
    */
   public int getMinSecLength()
   {
      return minSecLength;
   }

   /**
    * Gets the issue number required.
    *
    * @return the issueNumberRequired
    */
   public boolean getIssueNumberRequired()
   {
      return issueNumberRequired;
   }

   /**
    * Retrieve the Card name.
    *
    * @return the Card name
    */
   public String getCardType()
   {
      return cardType;
   }

   /**
    * Determine the Card Type.
    *
    * @param paymentCode the PaymentCode .
    *
    * @return the CardExtraDetails.
    *
    * @throws IllegalArgumentException if the code does not relate to a
    *            known package type.
    */
   public static CardDetails findByCode(String paymentCode)
   {
      for (CardDetails cardType : values())
      {
         if (cardType.getCardType().equalsIgnoreCase(paymentCode.replace('_', ' ')))
         {
            return cardType;
         }
      }
      String errorMessage = PropertyResource.getProperty("booking.paymentCode.unknown",
         MESSAGES_PROPERTY)
         + paymentCode;
      LogWriter.logErrorMessage(errorMessage);
      throw new IllegalArgumentException(errorMessage);
   }


}
