/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: TransactionTrackingData.java$
 *
 * $Revision: $
 *
 * $Date: Sep 1, 2009$
 *
 * Author: vinothkumar.k
 *
 *
 * $Log: $
 */
package com.tui.uk.domain;


/**
 * This class is responsible for holding the attributes like token and
 * sessionId required for tracking the transaction.
 *
 * @author vinothkumar.k
 *
 */
public class TransactionTrackingData
{
   /** The CPS token. */
   private String token;

   /** The sessionId. */
   private String sessionId;

   /** The inventory booking reference number. */
   private String invBookingRef;

   /** The ip address. */
   private String ipAddress;

   /**
    * The constructor.
    *
    * @param token the token.
    * @param sessionId the sessionId.
    */
   public TransactionTrackingData(String token, String sessionId)
   {
      this.token = token;
      this.sessionId = sessionId;
      this.invBookingRef = "";
   }

   /**
    * Gets the token.
    *
    * @return the token
    */
   public String getToken()
   {
      return token;
   }

   /**
    * Gets the sessionId.
    *
    * @return the sessionId
    */
   public String getSessionId()
   {
      return sessionId;
   }

   /**
    * Gets the inventory booking reference number.
    *
    * @return the invBookingRef
    */
   public String getInvBookingRef()
   {
      return invBookingRef;
   }

   /**
    * Gets the ip address.
    *
    * @return the ipAddress.
    */
   public String getIpAddress()
   {
      return ipAddress;
   }

   /**
    * Sets the inventory booking reference number.
    *
    * @param invBookingRef the invBookingRef.
    *
    */
   public void setInvBookingRef(String invBookingRef)
   {
      this.invBookingRef = invBookingRef;
   }

   /**
    * Sets the ip address.
    *
    * @param ipAddress the ip address.
    *
    */
   public void setIpAddress(String ipAddress)
   {
      this.ipAddress = ipAddress;
   }

   /**
    * Meaningful toString implementation. Prints all the attribute values of
    * this object.
    *
    * @return string representation of transactionTrackingData object.
    */
   public String toString()
   {
       return token + "," + sessionId + "," + invBookingRef;
   }
}
