/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingInfo.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-15 08:25:26 $
 *
 * $Author: Jaleel $
 *
 * $Log: $
 */
package com.tui.uk.domain;

import static com.tui.uk.domain.ApplicationDataConstants.ACCOMMODATION_SELECTED;
import static com.tui.uk.domain.ApplicationDataConstants.TANDC_URL;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.Accommodation;
import com.tui.uk.client.domain.AccommodationSummary;
import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.client.domain.CruiseSummary;
import com.tui.uk.client.domain.FlightDetails;
import com.tui.uk.client.domain.FlightSummary;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PackageType;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.client.domain.PriceComponent;
import com.tui.uk.client.domain.RefundDetails;
import com.tui.uk.client.domain.ShopDetails;
import com.tui.uk.client.domain.TermsAndCondition;
import com.tui.uk.log.LogWriter;

/**
 * This class contains all the NonPayment Data information. It holds the
 * <code>BookingComponent</code> object, which is sent by the client application.
 * 
 * @author Jaleel
 * 
 */
public final class BookingApplicationData
{

   /** Constant for postpayment url. */
   private static final String POST_PAYMENT_URL = "post_payment_url";

   /** Constant for prepayment url. */
   private static final String PRE_PAYMENT_URL = "pre_payment_url";

   /** Constant for failurepayment url. */
   private static final String FAILURE_PAYMENT_URL = "failure_payment_url";

   /** Constant for noRedirect. */
   private static final String NO_REDIRECT = "noRedirect";

   /** Constant for pricing. */
   private static final String PRICING = "pricing_";

   /** Constant for intellitrackerSnippet description. */
   private static final String INTELLITRACKERSNIPPET = "intellitrackerSnippet";

   /** Constant for country count. */
   private static final String COUNTRY_COUNT = "country_count";

   /** Constant for country code. */
   private static final String COUNTRY_CODE = "country_code_";

   /** Constant for prevPage. */
   private static final String PREVPAGE = "prevPage";

   /** Constant for country description. */
   private static final String COUNTRY_DSC = "country_dsc_";

   /** Constant for addressLine1. */
   private static final String ADDRESSLINE1 = "addressLine1";

   /** Constant for addressLine2. */
   private static final String ADDRESSLINE2 = "addressLine2";

   /** Constant for city. */
   private static final String CITY = "city";

   /** Constant for country description. */
   private static final String COUNTY = "county";

   /** Constant for postCode. */
   private static final String POSTCODE = "postCode";

   /** Constant for country. */
   private static final String COUNTRY = "country";

   /** Constant for phoneNumber. */
   private static final String PHONENUMBER = "phoneNumber";

   /** Constant for mobilePhone. */
   private static final String MOBILEPHONE = "mobilePhone";

   /** Constant for emailAddress. */
   private static final String EMAILADDRESS = "emailAddress";

   /** Constant for bookingSessionId. */
   private static final String BOOKINGSESSIONID = "bookingSessionId";

   /** Constant for passenger_. */
   private static final String PASSENGER_COUNT = "passenger_";

   /** Constant for space. */
   private static final String BLANK_SPACE = " ";

   /** Constant for refund key prefix. */
   private static final String REFUND_KEY_PREFIX = "refund_";

   /**
    * Private constructor to stop instantiation.
    */
   private BookingApplicationData()
   {
      // Do nothing.
   }

   /**
    * Updates the booking component.
    * 
    * @param bookingComponent The <code>bookingComponent</code> object.
    * @param applicationData The <code>applicationData</code> object.
    */
   public static void updateBookingComponent(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      bookingComponent.setNonPaymentData(applicationData);
      populateBookingApplicationData(bookingComponent, applicationData);
      if (StringUtils.isNotBlank(applicationData.get(INTELLITRACKERSNIPPET))
         && StringUtils.isNotBlank(applicationData.get(PREVPAGE)))
      {
         bookingComponent.setIntelliTrackSnippet(applicationData.get(INTELLITRACKERSNIPPET),
            applicationData.get(PREVPAGE));
      }

      if (StringUtils.isNotBlank(applicationData.get(POST_PAYMENT_URL)))
      {
         bookingComponent.setPaymentSuccessUrl(applicationData.get(POST_PAYMENT_URL));
      }

      if (StringUtils.isNotBlank(applicationData.get(PRE_PAYMENT_URL)))
      {
         bookingComponent.setPrePaymentUrl(applicationData.get(PRE_PAYMENT_URL));
      }

      if (StringUtils.isNotBlank(applicationData.get(FAILURE_PAYMENT_URL)))
      {
         bookingComponent.setPaymentFailureURL(applicationData.get(FAILURE_PAYMENT_URL));
      }

      if (StringUtils.isNotBlank(applicationData.get(NO_REDIRECT)))
      {
         bookingComponent.setNoRedirect(Boolean.valueOf(applicationData.get(NO_REDIRECT)));
      }

   }

   /**
    * This method is responsible for populating all details related to country, passenger, flight,
    * accomodation.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData The <code>applicationData</code> object.
    */
   private static void populateBookingApplicationData(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      populateCountryMap(bookingComponent, applicationData);
      populatePriceComponent(bookingComponent, applicationData);
      populateTAndCUrl(bookingComponent, applicationData);
      populateContactInformation(bookingComponent, applicationData);
      populateBookingSessionId(bookingComponent, applicationData);
      populateFlightSummary(bookingComponent, applicationData);
      populateAccomodationSummary(bookingComponent, applicationData);
      populateCruiseSummary(bookingComponent, applicationData);
      populatePassengerSummary(bookingComponent, applicationData);
      populatePackageType(bookingComponent, applicationData);
      populateShopDetails(bookingComponent, applicationData);
      if (bookingComponent.getRefundDetails() == null)
      {
         populateRefundDetails(bookingComponent, applicationData);
      }

   }

   /**
    * This method is to populate shop details.
    * 
    * @param bookingComponent BookingComponent object
    * @param applicationData applicationData map
    */
   private static void populateShopDetails(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      String shopId = "";

      if (applicationData.get("shop_number") != null)
      {
         shopId = applicationData.get("shop_number");
      }
      bookingComponent.setShopDetails(new ShopDetails("", shopId));
   }

   /**
    * This method is to populate Refund details.
    * 
    * @param bookingComponent BookingComponent object
    * @param applicationData applicationData map
    */
   private static void populateRefundDetails(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {

      List<RefundDetails> refundDetailList = new ArrayList<RefundDetails>();
      String dataCashReference = null;
      Money transactionAmount = null;
      Date transactionDate = new Date();
      String maskedCardNumber = null;
      DateFormat dateFormatRefund = new SimpleDateFormat("dd MM yyyy");
      if (applicationData.get("refund_count") != null)
      {
         int count = Integer.parseInt(applicationData.get("refund_count"));

         for (int i = 0; i < count; i++)
         {

            if (applicationData.get(REFUND_KEY_PREFIX + i + "_datacashreference") != null)
            {
               dataCashReference =
                  applicationData.get(REFUND_KEY_PREFIX + i + "_datacashreference");
            }
            if (applicationData.get(REFUND_KEY_PREFIX + i + "_amountPaid") != null)
            {
               transactionAmount =
                  new Money(new BigDecimal(applicationData.get(REFUND_KEY_PREFIX + i
                     + "_amountPaid")), Currency.getInstance("GBP"));
            }
            if (applicationData.get(REFUND_KEY_PREFIX + i + "_transactionDate") != null)
            {
               try
               {

                  transactionDate =
                     dateFormatRefund.parse(applicationData.get(REFUND_KEY_PREFIX + i
                        + "_transactionDate"));
               }
               catch (ParseException pe)
               {
                  LogWriter.logErrorMessage(pe.getMessage());
               }
            }
            if (applicationData.get(REFUND_KEY_PREFIX + i + "_maskedCardNumber") != null)
            {
               maskedCardNumber = applicationData.get("refund_" + i + "_maskedCardNumber");
            }
            RefundDetails refundDetails =
               new RefundDetails(dataCashReference, transactionAmount, transactionDate,
                  maskedCardNumber);

            refundDetails.setRefundEnabled(true);
            refundDetails.setRefundReferenceId(applicationData.get("refund_" + i + "_refundreferenceid"));
            refundDetailList.add(refundDetails);
         }
      }

      bookingComponent.setRefundDetails(refundDetailList);
   }

   /**
    * This method populates the country map of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populateCountryMap(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      Map<String, String> countryMap = new LinkedHashMap<String, String>();
      int countryListLength = 0;

      if (applicationData.get(COUNTRY_COUNT) != null)
      {
         countryListLength = Integer.parseInt(applicationData.get(COUNTRY_COUNT));
      }

      for (int i = 0; i < countryListLength; i++)
      {
         countryMap
            .put(applicationData.get(COUNTRY_CODE + i), applicationData.get(COUNTRY_DSC + i));
      }
      bookingComponent.setCountryMap(countryMap);
   }

   /**
    * This method populates the PriceComponent of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populatePriceComponent(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      List<PriceComponent> priceComponentList = new ArrayList<PriceComponent>();
      int priceCaptionCount = 0;

      if (applicationData.get(PRICING + "caption_count") != null)
      {
         priceCaptionCount = Integer.parseInt(applicationData.get(PRICING + "caption_count"));
      }

      for (int captionIndex = 0; captionIndex < priceCaptionCount; captionIndex++)
      {
         PriceComponent priceComponent =
            new PriceComponent(applicationData.get(PRICING + "caption_" + captionIndex), null);
         priceComponentList.add(priceComponent);
         int lineItemCount =
            Integer.parseInt(applicationData.get(PRICING + captionIndex + "_count"));
         for (int index = 0; index < lineItemCount; index++)
         {
            Money lineItemAmount =
               new Money(BigDecimal.valueOf(Double.parseDouble(applicationData.get(PRICING
                  + captionIndex + "_amt_" + index))), bookingComponent.getTotalAmount()
                  .getCurrency());
            PriceComponent lineItem =
               new PriceComponent(applicationData.get(PRICING + captionIndex + "_dsc_" + index),
                  lineItemAmount);

            if (applicationData.get(PRICING + captionIndex + "_qty_" + index) != null)
            {
               lineItem.setQuantity(Integer.valueOf(applicationData.get(PRICING + captionIndex
                  + "_qty_" + index)));
            }
            priceComponentList.add(lineItem);
         }
      }
      bookingComponent.setPriceComponents(priceComponentList);
   }

   /**
    * This method populates the TAndCUrlUrl of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populateTAndCUrl(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      if (StringUtils.isNotBlank(applicationData.get(ACCOMMODATION_SELECTED))
         && Boolean.parseBoolean(applicationData.get(ACCOMMODATION_SELECTED)))
      {
         AccommodationSummary accommodationSummary =
            new AccommodationSummary(null, 0, Boolean.TRUE, "");

         TermsAndCondition termsAndCondition =
            new TermsAndCondition(bookingComponent.getClientApplication(),
               applicationData.get(TANDC_URL), "");
         bookingComponent.setAccommodationSummary(accommodationSummary);
         bookingComponent.setTermsAndCondition(termsAndCondition);
      }
   }

   /**
    * This method populates the ContactInformation in <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populateContactInformation(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      ContactInfo contactInfo = new ContactInfo();
      contactInfo.setAltPhoneNumber(applicationData.get(MOBILEPHONE));
      contactInfo.setCity(applicationData.get(CITY));
      contactInfo.setCountry(applicationData.get(COUNTRY));
      contactInfo.setCounty(applicationData.get(COUNTY));
      contactInfo.setEmailAddress(applicationData.get(EMAILADDRESS));
      contactInfo.setPhoneNumber(applicationData.get(PHONENUMBER));
      contactInfo.setPostCode(applicationData.get(POSTCODE));
      contactInfo.setStreetAddress1(applicationData.get(ADDRESSLINE1));
      contactInfo.setStreetAddress2(applicationData.get(ADDRESSLINE2));

      bookingComponent.setContactInfo(contactInfo);
   }

   /**
    * This method populates the bookingSessionId of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populateBookingSessionId(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      bookingComponent.setBookingSessionIdentifier(applicationData.get(BOOKINGSESSIONID));
   }

   /**
    * This method populates the Flight Summary of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populateFlightSummary(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      DateFormat dateFormat = new SimpleDateFormat("E dd MMM yy HH:mm");
      if (applicationData.containsKey("outBoundFlight_departureDate"))
      {
         List<FlightDetails> outboundFlight = new ArrayList<FlightDetails>();
         List<FlightDetails> inboundFlight = new ArrayList<FlightDetails>();

         String outBoundDepartureDate = applicationData.get("outBoundFlight_departureDate");
         String outBoundDepartureTime = applicationData.get("outBoundFlight_departureTime");

         Date outBoundDepartureDateTime = null;
         try
         {
            outBoundDepartureDateTime =
               dateFormat.parse(outBoundDepartureDate + BLANK_SPACE + outBoundDepartureTime);
         }
         catch (ParseException pe)
         {
            LogWriter.logErrorMessage(pe.getMessage());
         }

         String outBoundArrivalTime = applicationData.get("outBoundFlight_arrivalTime");

         Date outBoundArrivalDateTime = null;
         try
         {
            outBoundArrivalDateTime =
               dateFormat.parse(outBoundDepartureDate + BLANK_SPACE + outBoundArrivalTime);
         }
         catch (ParseException pe)
         {
            LogWriter.logErrorMessage(pe.getMessage());
         }

         FlightDetails outboundFlightDetails =
            new FlightDetails(applicationData.get("outBoundFlight_departureAirportName"),
               outBoundDepartureDateTime, applicationData.get("outBoundFlight_arrivalAirportName"),
               outBoundArrivalDateTime, applicationData.get("operatingCarrier "),
               applicationData.get("outBoundFlight_operatingAirlineCode"),
               applicationData.get("outBoundFlight_operatingAirlineCode"));

         outboundFlightDetails.setDepartureAirportCode(applicationData
            .get("outBoundFlight_departureAirportCode"));
         outboundFlightDetails.setArrivalAirportCode(applicationData
            .get("outBoundFlight_arrivalAirportCode"));

         outboundFlight.add(outboundFlightDetails);

         if (applicationData.containsKey("inBoundFlight_departureDate"))
         {
            String inBoundDepartureDate = applicationData.get("inBoundFlight_departureDate");
            String inBoundDepartureTime = applicationData.get("inBoundFlight_departureTime");

            Date inBoundDepartureDateTime = null;
            try
            {
               inBoundDepartureDateTime =
                  dateFormat.parse(inBoundDepartureDate + BLANK_SPACE + inBoundDepartureTime);
            }
            catch (ParseException pe)
            {
               LogWriter.logErrorMessage(pe.getMessage());
            }

            String inBoundArrivalTime = applicationData.get("inBoundFlight_arrivalTime");

            Date inBoundArrivalDateTime = null;
            try
            {
               inBoundArrivalDateTime =
                  dateFormat.parse(inBoundDepartureDate + BLANK_SPACE + inBoundArrivalTime);
            }
            catch (ParseException pe)
            {
               LogWriter.logErrorMessage(pe.getMessage());
            }

            FlightDetails inboundFlightDetails =
               new FlightDetails(applicationData.get("inBoundFlight_departureAirportName"),
                  inBoundDepartureDateTime,
                  applicationData.get("inBoundFlight_arrivalAirportName"), inBoundArrivalDateTime,
                  applicationData.get("operatingCarrier "),
                  applicationData.get("inBoundFlight_operatingAirlineCode"),
                  applicationData.get("inBoundFlight_operatingAirlineCode"));

            inboundFlightDetails.setDepartureAirportCode(applicationData
               .get("inBoundFlight_departureAirportCode"));
            inboundFlightDetails.setArrivalAirportCode(applicationData
               .get("inBoundFlight_arrivalAirportCode"));

            inboundFlight.add(inboundFlightDetails);
         }

         FlightSummary flightSummary = new FlightSummary(outboundFlight, inboundFlight, "", null);
         flightSummary.setTicketType(applicationData.get("flight_ticketType"));
         bookingComponent.setFlightSummary(flightSummary);
      }
   }

   /**
    * This method populates the bookingSessionId of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populateAccomodationSummary(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      if (applicationData.containsKey("accommodation_duration"))
      {
         Accommodation accommodation =
            new Accommodation(applicationData.get("accommodation_country"),
               applicationData.get("accommodation_destination"),
               applicationData.get("accommodation_resort"),
               applicationData.get("accommodation_name"), "");

         AccommodationSummary accommodationSummary =
            new AccommodationSummary(accommodation, Integer.parseInt(applicationData
               .get("accommodation_duration")), null,
               applicationData.get("accommodation_inventorySystem"));

         DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

         Date checkInDate = null;
         Date checkOutDate = null;

         try
         {
            if (applicationData.containsKey("accommodation_checkInDate"))
            {
               checkInDate = dateFormat.parse(applicationData.get("accommodation_checkInDate"));
            }
            if (applicationData.containsKey("accommodation_checkOutDate"))
            {
               checkOutDate = dateFormat.parse(applicationData.get("accommodation_checkOutDate"));
            }
         }
         catch (ParseException pe)
         {
            LogWriter.logErrorMessage(pe.getMessage());
         }

         if (checkInDate != null)
         {
            accommodationSummary.setStartDate(checkInDate);
         }

         if (checkOutDate != null)
         {
            accommodationSummary.setEndDate(checkOutDate);
         }

         bookingComponent.setAccommodationSummary(accommodationSummary);
      }
   }

   /**
    * This method populates the Cruise Details of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populateCruiseSummary(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      if (applicationData.containsKey("cruise_duration"))
      {
         Accommodation ship = new Accommodation("", "", "", applicationData.get("cruise_name"), "");
         ship.setSellingCode(applicationData.get("cruise_sellingCode"));

         CruiseSummary cruiseSummary =
            new CruiseSummary(ship, Integer.parseInt(applicationData.get("cruise_duration")));
         cruiseSummary.setCruiseArea(applicationData.get("cruise_cruiseArea"));
         cruiseSummary.setStartPort(applicationData.get("cruise_startPort"));

         bookingComponent.setCruiseSummary(cruiseSummary);
      }
   }

   /**
    * This method populates the Passenger Details of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populatePassengerSummary(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      if (applicationData.containsKey("passenger_total_count"))
      {
         int passengerTotalCount = Integer.parseInt(applicationData.get("passenger_total_count"));
         List<PassengerSummary> passengerRoomSummaryList = new ArrayList<PassengerSummary>();

         int adultCount = 0;
         if (applicationData.containsKey("passenger_ADT_count"))
         {
            adultCount =
               Integer.valueOf(bookingComponent.getNonPaymentData().get("passenger_ADT_count"));
         }

         int childCount = 0;
         if (applicationData.containsKey("passenger_CHD_count"))
         {
            childCount =
               Integer.valueOf(bookingComponent.getNonPaymentData().get("passenger_CHD_count"));
         }

         int infantCount = 0;
         if (applicationData.containsKey("passenger_INF_count"))
         {
            infantCount =
               Integer.valueOf(bookingComponent.getNonPaymentData().get("passenger_INF_count"));
         }
         int childTempCount = 1;
         int infantTempCount = 1;
         for (int passengerCount = 1; passengerCount <= passengerTotalCount; passengerCount++)
         {
            PassengerSummary passengerSummary;

            if (applicationData.containsKey(PASSENGER_COUNT + passengerCount + "_paxAgeGroup"))
            {
               passengerSummary =
                  new PassengerSummary(AgeClassification.findByCode(applicationData
                     .get(PASSENGER_COUNT + passengerCount + "_paxAgeGroup")),
                     Boolean.parseBoolean(applicationData.get(PASSENGER_COUNT + passengerCount
                        + "_leadPassenger")), passengerCount);
            }
            else
            {
               AgeClassification ageClassification = null;

               if (passengerCount <= adultCount)
               {
                  ageClassification = AgeClassification.ADULT;
               }
               else if (passengerCount <= childCount + (passengerCount - childTempCount))
               {
                  childTempCount++;
                  ageClassification = AgeClassification.CHILD;
               }
               else if (passengerCount <= infantCount + passengerCount - infantTempCount)
               {
                  ageClassification = AgeClassification.INFANT;
                  infantTempCount++;
               }
               boolean isLead = false;
               if (passengerCount == 1)
               {
                  isLead = true;
               }
               passengerSummary = new PassengerSummary(ageClassification, isLead, passengerCount);
            }
            String passengerIndex = PASSENGER_COUNT + passengerCount;
            passengerSummary.setTitle(applicationData.get(passengerIndex + "_title"));
            passengerSummary.setForeName(applicationData.get(passengerIndex + "_foreName"));
            passengerSummary.setLastName(applicationData.get(passengerIndex + "_lastName"));

            passengerRoomSummaryList.add(passengerSummary);
         }

         Map<Integer, List<PassengerSummary>> passengerRoomSummary =
            new HashMap<Integer, List<PassengerSummary>>();
         passengerRoomSummary.put(1, passengerRoomSummaryList);

         bookingComponent.setPassengerRoomSummary(passengerRoomSummary);
      }
   }

   /**
    * This method populates the bookingSessionId of <code>BookingComponent</code> object.
    * 
    * @param bookingComponent the booking component.
    * @param applicationData the application data sent by the client.
    */
   private static void populatePackageType(BookingComponent bookingComponent,
      Map<String, String> applicationData)
   {
      if (applicationData.containsKey("package_type"))
      {
         bookingComponent
            .setPackageType(PackageType.findByCode(applicationData.get("package_type")));
      }
   }

}
