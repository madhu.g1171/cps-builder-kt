/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BrandMapping.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2009-09-16 12:11:00$
 *
 * $author: kumarhe$
 *
 * $Log : $
 *
 */
package com.tui.uk.domain;

/**
 *
 * Determines the Brand for a given "host/context" string.
 * @author kumarhe
 *
 */
public final class BrandMapping
{
  /** The pattern representing "host/context". */
   private final String hostContextPattern;

   /** The brand to determine.*/
   private final String brand;

   /**
    * The constructor for BrandMapping .
    * @param pattern the  "host/context" pattern.
    * @param brand the brand.
    */
   public BrandMapping(String pattern, String brand)
   {
      this.hostContextPattern = pattern;
      this.brand = brand;
   }

   /**
    * Find the brand using host and context name using the pattern.
    * @param pattern the  "host/context" pattern.
    * @return brand if the pattern matches.
    */
   public String find(String pattern)
   {
      if (hostContextPattern.contains(pattern))
      {
         return brand;
      }
      return null;
   }

}
