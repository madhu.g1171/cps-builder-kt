package com.tui.uk.email.service.impl;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.email.service.EmailService;
import com.tui.uk.log.LogWriter;

/**
 * This class implements sendEmail method which is responsible for sending
 * emails when refund cheque
 * option is selected in the payment page.
 *
 * @author Suman.P
 *
 */
public class EmailServiceImpl implements EmailService
{

 /**
    * Sends the email to the requested recipient when refund cheque
    * option is selected in the
    * payment page.
    *
    * @param message the message to be sent.
    *
    * @throws EmailProcessorException the email processor exception.
    */
public void sendEmail(Message message) throws EmailProcessorException
{
      try
      {

          Transport.send(message);
     }
      catch (MessagingException mex)
      {
         String msg = mex.getMessage();
         LogWriter.logErrorMessage(msg, mex);
         throw new EmailProcessorException(msg, mex);
      }
   }

}
