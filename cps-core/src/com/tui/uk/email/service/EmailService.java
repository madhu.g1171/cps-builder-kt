package com.tui.uk.email.service;

import javax.mail.Message;

import com.tui.uk.email.exception.EmailProcessorException;

/**
 * The interface for sending mails.
 *
 * @author Suman.P
 *
 */
public interface EmailService
{
/**
 * Sends the email.
 *
 * @param message the email message.
 *
 * @throws EmailProcessorException the email processor exception.
 */
void sendEmail(Message message) throws EmailProcessorException;
}
