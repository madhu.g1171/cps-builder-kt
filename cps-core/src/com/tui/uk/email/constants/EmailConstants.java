/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd,
 * Columbus House,
 * Westwood Way,
 * Westwood Business Park,
 * Coventry,
 * United Kingdom
 * CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence
 * any actual or intended publication of this source code.
 *
 * $RCSfile:   EmailConstants.java$
 *
 * $Revision:   $
 *
 * $Date:   Dec 22, 2009$
 *
 * Author: veena.bn
 *
 *
 * $Log:   $
 */
package com.tui.uk.email.constants;

/**
 * This class is used to hold all constants used by the sendMail method of the EmailProcessor.
 * 
 * @author veena.bn
 * 
 */

public final class EmailConstants
{
   /**
    * Private constructor to stop instantiation of this class.
    */
   private EmailConstants()
   {
      // Do nothing.
   }

   /** The Constant FROM_ADDRESS. */
   public static final String FROM_ADDRESS = "mailService.fromAddress";

   /** The Constant HOST. */
   public static final String HOST = "mailService.host";

   /** The Constant SMTP Connection String. */
   public static final String SMTP_CONNECTION_STRING = "mailService.smtpConnectionString";

   /** The Constant RECIPIENTS. */
   public static final String RECIPIENTS = "mailService.recipients";

   /** The Constant REFUND_CHEQUE_BUISNESSINFO. */
   public static final String REFUND_CHEQUE_BUSINESSINFO = "mailService.refundChequeBusinessInfo";

   /** The Constant REFUND_CHEQUE_SUBJECT. */
   public static final String REFUND_CHEQUE_SUBJECT = "mailService.refundChequeSubject";

   /** The Constant DEFAULT HOST. */
   public static final String DEFAULT_HOST = "mail.smtp.host";

   /** The Constant BOOKING_REF_NO. */
   public static final String BOOKING_REF_NO = "Booking Reference Number: ";

   /** The Constant SHOREX_BOOKING_REF_NO. */
   public static final String SHOREX_BOOKING_REF_NO = "Shorex booking reference: ";

   /** The Constant REASON. */
   public static final String REASON = "Reason: ";

   /** The Constant CRUISE_HOLIDAY_BOOKING_REF_NO. */
   public static final String CRUISE_HOLIDAY_BOOKING_REF_NO = "Cruise holiday booking reference: ";

   /** The Constant CONTENT. */
   public static final String CONTENT = "text/html";

   /** The Constant AMT_REFUND. */
   public static final String AMT_REFUND = "Amount of Refund: ";

   /** The Constant LEAD_PASSENGER_DETAILS. */
   public static final String LEAD_PASSENGER_DETAILS = "<p><b>Lead Passenger Details:</b>";

   /** The Constant DEPARTURE_DATE. */
   public static final String DEPARTURE_DATE = "<p>Departure Date: ";

   /** The Constant HOUSENO. */
   public static final String HOUSENO = "houseNo";

   /** The Constant COUNTRY. */
   public static final String COUNTRY = "country";

   /** The Constant DAYTIME_PHONE. */
   public static final String DAYTIME_PHONE = "Day time phone: ";

   /** The Constant MOBILEPHONE. */
   public static final String MOBILEPHONE = "Mobile phone: ";

   /** The Constant Holiday start date. */
   public static final String HOLIDAY_START_DATE = "Holiday start date: ";

   /** The Constant Sailing date. */
   public static final String SAILING_DATE = "Sailing date: ";

   /** The Constant Itinerary. */
   public static final String ITINERARY = "Itinerary: ";

   /** The Constant Ship. */
   public static final String SHIP = "Ship: ";

   /** The Constant EMAIL. */
   public static final String EMAIL = "Email: ";

   /** The Constant BREAK. */
   public static final String BREAK = "<br/>";

   /** The constant for holding the refund by cheque date format. */
   public static final String REFUND_CHEQUE_DATE_FORMAT = "dd/MM/yyyy";

   /** The constant for holding the DEFAULT_SUBJECT. */
   public static final String DEFAULT_SUBJECT = "Refund Cheque -";

   /** The Constant SPACE. */
   public static final String SPACE = "&nbsp;";

}
