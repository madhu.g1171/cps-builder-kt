/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd,
 * Columbus House,
 * Westwood Way,
 * Westwood Business Park,
 * Coventry,
 * United Kingdom
 * CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence
 * any actual or intended publication of this source code.
 *
 * $RCSfile:   EmailProcessorException.java$
 *
 * $Revision:   $
 *
 * $Date:   Dec 22, 2009$
 *
 * Author: veena.bn
 *
 *
 * $Log:   $
 */
package com.tui.uk.email.exception;

/**
 * This is the exception thrown from the email service layer.
 *
 * @author veena.bn
 *
 */
@SuppressWarnings("serial")
public class EmailProcessorException extends Exception
{

   /**
    * Default constructor for EmailProcessorException class.
    */
   public EmailProcessorException()
   {
      super();
   }

   /**
    * Defines the constructor for EmailProcessorException with
    * detailed message and specified cause.
    *
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public EmailProcessorException(String message, Throwable cause)
   {
      super(message, cause);
   }
}
