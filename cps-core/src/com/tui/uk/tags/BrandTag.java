/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BrandTag.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2009-09-16 12:11:00$
 *
 * $author: Vinayak$
 *
 * $Log : $
 *
 */
package com.tui.uk.tags;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;


import com.tui.uk.config.BrandConfReader;
import com.tui.uk.domain.BrandMapping;
import com.tui.uk.exception.BrandException;
import com.tui.uk.log.LogWriter;

/**
 * Brand tag to identify the brand based on the conf entry and uri.
 *
 * @author vinayak
 *
 */
public class BrandTag extends SimpleTagSupport
{
   /** The default configuration file location. */
   private static final String DEFAULT_BRANDCONFIG_FILEPATH = "../../conf/";

   /** The system property that can be used to alter the location of the configuration file.*/
   private static final String BRANDCONFIG_SYSTEM_PROPERTY = "brandconfig.file.path";

   /** Brand. */
   private static final String TUI_BRAND = "tui_brand";

   /** the list of brand mappings. */
   private List<BrandMapping> brandMappings;

  /**
   * Renders the tag.
   *
   * @throws JspException if error occurs
   */
   public void doTag() throws JspException
   {
      PageContext context = (PageContext) getJspContext();

      // If the brand is already a request parameter, use that
      String brand = (String) context.getAttribute(TUI_BRAND, PageContext.SESSION_SCOPE);
      if (brand == null)
      {
         getBrandMappings(context);
         // Find the session based on the mapping data
         HttpServletRequest request = (HttpServletRequest) context.getRequest();

         String url = request.getServerName().concat(request.getContextPath());
         for (BrandMapping mapping : brandMappings)
         {
            brand = mapping.find(url);
            if (brand != null)
            {
               request.getSession().setAttribute(TUI_BRAND, brand);
               writeToTag(context, brand);
               break;
            }
         }
      }
      else
      {
        writeToTag(context, brand);
      }

   }

   /**
    * Writes brand name to tag.
    *
    * @param context is the PageContext
    * @param brand is the name of brand that will be written to tag
    */
   private void writeToTag(PageContext context, String brand)
   {
      try
      {
         context.getOut().write(brand);
      }
      catch (IOException e)
      {
         //do not do anything if brand not getting rendered don't break the page
         LogWriter.logDebugMessage(e.getMessage(), e);
      }
   }

  /**
   * Gets brand mappings from BrandConfig.
   * @param context the pageContext
   *
   * @throws JspException the jsp exception
   */
   private void getBrandMappings(PageContext context) throws JspException
   {
      String brandConfigFileName = context.getServletContext().getInitParameter("BRAND_CONFIG");
      if (brandConfigFileName != null)
      {
         String filepathSuffix =
            System.getProperty(BRANDCONFIG_SYSTEM_PROPERTY, DEFAULT_BRANDCONFIG_FILEPATH
               + brandConfigFileName + ".conf");
         BrandConfReader.setConfigFileName(context.getServletContext().getRealPath(File.separator)
            + filepathSuffix);
         try
         {
            LogWriter
               .logInfoMessage("Reading configuration from: " + brandConfigFileName + ".conf");
            brandMappings = BrandConfReader.getBrandMappings();
            LogWriter.logInfoMessage(" Initialization complete.............");
         }
         catch (BrandException cfe)
         {
            LogWriter.logDebugMessage("Initialization failed ...........");
            LogWriter.logErrorMessage("Unable to read the brand configuration file.");

            throw new JspException(cfe.getMessage(), cfe);
         }
      }
      else
      {
         throw  new JspException("Brand File name not specified in web.xml");
      }
   }
}
