/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/

package com.tui.uk.tags;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.tui.uk.log.LogWriter;

/**
 * A simple utility Tag to display the application version in on pages,
 * Along side application version this displays other useful information session id
 * Server ip etc.
 * Format of Version info is like following
 * <!-- Version ID: xx.xxx.xxxx built on [build date]
 * Session ID: [JsesssionId]
 * -->
 * <p/>
 * For getting version id it looks for the version.id file in META-INF dir inside war structure
 * <p/>
 *
 * @author karmuni
 */
public class VersionTag extends SimpleTagSupport
{

   /** The META-INF folder name. */
   private static final String META_INF = "META-INF";

   /** The name of the version info file.*/
   private static final String VERSION_FILE_NAME = "version.id";

   /**
    * static instance of version info.
    * Having static instance avoids reading version file for  every single page rendering.
    */
   private static Properties versionInfo = new Properties();

   /**
    * Renders the tag.
    *
    * @throws JspException if error occurs
    */
   public void doTag() throws JspException
   {
      PageContext context = (PageContext) getJspContext();
      if (versionInfo == null || versionInfo.isEmpty())
      {
         readVersionInfo(context.getServletContext());
      }
      JspWriter writer = getJspContext().getOut();
      try
      {
         writer.print("<!--    ");
         versionInfo.list(new PrintWriter(writer));
         writer.println("Session Id=" + context.getSession().getId() + " -->");
      }
      catch (IOException e)
      {
         //do not do anything if version not getting rendered don't break the page
         LogWriter.logDebugMessage(e.getMessage(), e);
      }

   }

   /**
    * Reads the version info from META-INF/version.id file.
    * If it does not exist of fails to read it populates this#versionInfo with empty info.
    *
    * @param context the servlet context.
    */
   private void readVersionInfo(ServletContext context)
   {
      versionInfo = new Properties();

      String versionfilePath = context.getRealPath(File.separator)
                  + META_INF + File.separator + VERSION_FILE_NAME;
      File propertyFile = new File(versionfilePath);
      if (propertyFile.exists())
      {
         FileInputStream fileInputStream = null;
         try
         {
            fileInputStream = new FileInputStream(propertyFile);
            versionInfo.load(fileInputStream);
         }
         catch (IOException ex)
         {
            versionInfo.setProperty("Version Id", "Unkown");
         }
         finally
         {
            // cleanup
            if (fileInputStream != null)
            {
               try
               {
                  fileInputStream.close();
               }
               catch (IOException e)
               {
                  // do nothing
                  LogWriter.logDebugMessage(e.getMessage(), e);
               }
            }
         }
      }
      else
      {
         versionInfo.setProperty("Version Id", "Unkown");
      }
   }

}
