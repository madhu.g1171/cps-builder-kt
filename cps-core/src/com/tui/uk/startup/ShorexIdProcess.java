/**
 * 
 */
package com.tui.uk.startup;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;

/**
 * @author sreenivasulu.k
 *
 */
public class ShorexIdProcess implements Runnable {

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		File file;
		long refundPeriod=ConfReader.getLongEntry("shorex.refundid.period",86400000);
		long refundDateInMillis;
		ObjectInputStream ois;
		ArrayList<EssentialTransactionData> refundsListFromFile;
		ArrayList<EssentialTransactionData> refundsList=new ArrayList<EssentialTransactionData>();		
		try{
			LogWriter.logInfoMessage("Shorex ID process has been started ........................");
			file=new File(ConfReader.getConfEntry("shorex.file", "")+"/shorexid.ser");
			if(!file.exists()){
				file.createNewFile();
			}Object object=null;
			try{
			ois=new ObjectInputStream(new FileInputStream(file));
			object=ois.readObject();
			ois.close();
			}catch(EOFException ex){}
			if(object==null){
				LogWriter.logInfoMessage("Shorex id file information not available");
				return;
			}
			refundsListFromFile=(ArrayList<EssentialTransactionData>) object;				
			for(EssentialTransactionData etd:refundsListFromFile){
				refundDateInMillis=System.currentTimeMillis()-etd.getRefundProcessedDate().getTime();
				if(!(refundDateInMillis>refundPeriod)){
					refundsList.add(etd);					
				}
			}
//			file=new File(ConfReader.getConfEntry("cps.shorex.file", "")+"/shorexid.ser");
			ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(file));
			oos.writeObject(refundsList);
			oos.close();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

}
