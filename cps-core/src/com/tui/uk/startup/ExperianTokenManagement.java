package com.tui.uk.startup;

import java.io.Serializable;
import java.util.Date;

public class ExperianTokenManagement implements Serializable, Cloneable {
	private static final long serialVersionUID = 5L;

	public String experianToken;
	public Date lastDate;
	public String getExperianToken() {
		return experianToken;
	}

	public void setExperianToken(String experianToken) {
		this.experianToken = experianToken;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public ExperianTokenManagement(String token,Date lastDate){

		this.lastDate=lastDate;
		this.experianToken=token;
	}

	public String toString(){
		return this.experianToken+"\t\t\t"+this.lastDate;
	}

}
