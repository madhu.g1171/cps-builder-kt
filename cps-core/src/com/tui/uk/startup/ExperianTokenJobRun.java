package com.tui.uk.startup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;

import com.tui.uk.config.ConfReader;
import com.tui.uk.experianClient.TokenServiceRequestProcessor;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;
import com.tui.uk.payment.domain.PaymentStore;

import org.apache.commons.lang.StringUtils;

public class ExperianTokenJobRun implements Runnable {

    Logger logger = LogWriter.getLogger("com.tui.uk.experianClient");
    public void run() {


            final TokenServiceRequestProcessor tokenServiceRequestProcessor = new TokenServiceRequestProcessor();
            int totalNoOfRetries =  ConfReader.getIntEntry("experian.maximum.tryFortoken", 3);
            String experianToken = null;
            int noOfAttempts = 0;
            try{

                do {
                    experianToken = tokenServiceRequestProcessor.getToken();
                    noOfAttempts++;
                }
                while(StringUtils.isBlank(experianToken) && noOfAttempts <= totalNoOfRetries);

                if(StringUtils.isBlank(experianToken)){
                    logger.logInfoMessage("=============== Maximum retries to generate token has been reached without getting the valid token from Experian. Please check with "
                            + " Experian for further analysis "
                            + new Date() + "\n experianToken = " + experianToken +" =======================");
                }


                if(StringUtils.isNotBlank(experianToken)){

                    logger.logInfoMessage("Token received from experian is at "
                            + new Date() + "\n " + experianToken);

                    final ExperianTokenManagement experianTokenManagementObj = new ExperianTokenManagement(
                            experianToken, new Date());

                    writeTokenToFile(experianTokenManagementObj);

                    PaymentStore.getInstance().experianTokenData("experianToken",
                            experianTokenManagementObj);

                    logger.logInfoMessage("Token has been updated in the cache.......... ");
                }

            }
            catch (Exception e){
                    logger.logInfoMessage("Exception Occured while generation Experian token ...... "+e);
                    e.printStackTrace();
                }

        }



    /**
     * This method writes the provided token data to file
     *
     * @param experianTokenManagementObj
     */
    private void writeTokenToFile(
            final ExperianTokenManagement experianTokenManagementObj) {

        File file = new File(ConfReader.getConfEntry("experian.tokenFile", "")
                + "/" + "tokenFile.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                LogWriter.logInfoMessage(
                        "Exception while creating the  tokenFile.txt file ", e);
            }
        }

        FileOutputStream fout = null;
        ObjectOutputStream oos = null;

        try {
                fout = new FileOutputStream(file);
                oos = new ObjectOutputStream(fout);
                synchronized(this){
                	oos.writeObject(experianTokenManagementObj);
                }
            }
            catch (FileNotFoundException e) {
                LogWriter.logInfoMessage("tokenFile.txt not found", e);
            }
            catch (IOException e) {
                LogWriter.logInfoMessage(
                        "Exception occured while writing the data to file ", e);
            }

            catch (Exception e){
                LogWriter.logInfoMessage(
                        "Exception occured ", e);
                e.printStackTrace();

            }

            finally {
                try {
                    if(null != oos){
                        oos.flush();
                        fout.close();
                        oos.close();
                    }

                } catch (IOException e) {
                    LogWriter.logInfoMessage(
                            "Exception occured while closing the stream ", e);
                }
        }

     //   LogWriter.logInfoMessage("Successfully written token to file");

    }

}
