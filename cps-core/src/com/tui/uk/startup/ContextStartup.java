/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ContextStartup.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: bibin.j@sonata-software.com $
 *
 * $Log: not supported by cvs2svn $ Revision 1.2 2008/05/07 11:18:36
 * sindhushree.g Add file revision history comment.
 *
 */
package com.tui.uk.startup;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadFactory;

import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.startup.ExperianTokenManagement;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.ConfReader;
import com.tui.uk.exception.ConfReaderException;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;
import com.tui.uk.validation.validator.SafeHTMLValidator;

import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

/**
 * Listener for Common Payment Application.Loads configuration file/message
 * resources. Include listener entry in the web.xml to ensure that this task is
 * executed during application startup.
 *
 * @author bibin.j@sonata-software.com
 */
/**
 * @author anup.g
 *
 */

public final class ContextStartup implements ServletContextListener {
    /** The default configuration file location. */
    private static final String DEFAULT_CONFIG_FILEPATH = "../../conf/";
    /** Added below line for loading ACSS country list conf. */

    private Logger logger = LogWriter.getLogger("com.tui.uk.experianClient");
    /** the scheduler to run the token Service */
    private ScheduledExecutorService scheduler;
    private ScheduledExecutorService scheduledExecutorService;
    private  ScheduledFuture scheduledFuture ;

    /**
     * Loads the application configuration/message resources and datacash
     * accounts file. a)If the system property "config.file.path" is set,this is
     * used as the file path suffix, otherwise the default value
     * "../../conf/[appname].conf" is used.This suffix value is appended to the
     * servlet real path to obtain the path for the configuration file. b)For
     * loading message resources it is assumed that message resources file is
     * placed in the classpath. c)If the system property "datacash.file.path" is
     * set,this is used as the file path suffix, otherwise the default value
     * "../../conf/datacash.conf" is used.This suffix value is appended
     *
     *
     * @param contextEvent
     *            the event triggering this listener
     */
    public void contextInitialized(ServletContextEvent contextEvent) {
        /** Added below line for loading ACSS country list conf. */
        ConfReader.setRealPathForACSS(contextEvent.getServletContext()
                .getRealPath(File.separator));
        String appname = contextEvent.getServletContext().getInitParameter(
                "APP-NAME");
        String datacashfileName = contextEvent.getServletContext()
                .getInitParameter("DATACASH-ACCOUNTS");

        StringBuilder confFiles = new StringBuilder(appname);
        confFiles
                .append(",")
                .append(datacashfileName)
                .append(",")
                .append(contextEvent.getServletContext().getInitParameter(
                        "CONFIG_FILES"));

        String[] files = StringUtils.split(confFiles.toString(), ",");

        LogWriter.logInfoMessage("Loading the configuration for " + appname  + ".............");



        ConfReader.setAppName(appname);

        List<String> confFileNames = new ArrayList<String>();

        for (String confFile : files) {
            confFileNames.add(contextEvent.getServletContext().getRealPath(
                    File.separator)
                    + System.getProperty(confFile + ".file.path",
                            DEFAULT_CONFIG_FILEPATH + confFile + ".conf"));
        }

        // Set the real path in SafeHTMLValidator.To be used to construct the
        // absolute path to the antisamy-esapi.xml
        SafeHTMLValidator.setContextPath(contextEvent.getServletContext()
                .getRealPath(File.separator));

        try {
            ConfReader.loadConfiguration(confFileNames);
            LogWriter.logInfoMessage(appname
                    + " loaded sucessfully.............");
        } catch (ConfReaderException cfe) {
            LogWriter.logErrorMessage(cfe.getMessage(), cfe);
            throw new RuntimeException(cfe);
        }

        final boolean isExperianServiceEnabled = ConfReader.getBooleanEntry("experian.services.enabled",false);

            try {
            	  LogWriter.logInfoMessage("*****************************Experian Service switch ****************"+isExperianServiceEnabled);
            	 if(isExperianServiceEnabled){

            		 final String keyStore = ConfReader.getConfEntry("experian.certificate.path", "/tui/iscape/program/experian_files/TUISTORE.jks");
            		 final String keyStorePassword = ConfReader.getConfEntry("experian.keystore.password", "BAb4u623");
            		 File file = new File(keyStore);

            	            if (!file.exists()) {
            	            	 LogWriter.logInfoMessage("The keyStore file ***  "+keyStore+" *** containing Experian certifications doesn't exist "
            	            	 		+ "in the specified location ...........");
            	            }
            	            else{
            	            	System.setProperty("java.net.useSystemProxies", "true");
                                System.setProperty("javax.net.ssl.keyStoreType", "JKS");
                                System.setProperty("javax.net.ssl.keyStore",keyStore);
                                System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);

                                experianTokenJob(scheduler);

            	            }

            	 }
            	 else{
                     LogWriter.logInfoMessage("++++++++++++++++++++++++++++Experian Service flag++++++++++++++++++++++"+isExperianServiceEnabled);;
                     File file = new File(ConfReader.getConfEntry("experian.tokenFile", "")
                             + "/" + "tokenFile.txt");

                   if(file.delete())  {
                       LogWriter.logInfoMessage("*****************************Experian Token file has been deleted****************");;
                   }
            	 }

            } catch (Exception ex) {
                LogWriter.logInfoMessage("Exception occured in loading the experianTokenJob " + ex
                        + ".............");;

        }

            boolean isCurrentInstaceSupportUID=ConfReader.getBooleanEntry("shorexid.validation.enabled",false);
            boolean shorexUIDEnabled=ConfReader.getBooleanEntry("shorexid.enable",false);

            if(shorexUIDEnabled && isCurrentInstaceSupportUID){
                String cpsConfPath=contextEvent.getServletContext().getRealPath(File.separator)+ System.getProperty("cps.file.path",DEFAULT_CONFIG_FILEPATH + "cps.conf");

                String cpsTempFilePath=contextEvent.getServletContext().getRealPath(File.separator)+ System.getProperty("cps.file.path",DEFAULT_CONFIG_FILEPATH + "cpsfile.conf");

            startShorexIdProcess(cpsConfPath,cpsTempFilePath);
            }


    }

    /**
     * Unloads the application when the context is destroyed.
     *
     * @param contextEvent
     *            the event instance for context destruction
     */
    public void contextDestroyed(ServletContextEvent contextEvent) {
        String appname = contextEvent.getServletContext().getInitParameter(
                "APP-NAME");
        LogWriter.logInfoMessage("UnLoaded the application " + appname
                + ".............");
        LogWriter.logInfoMessage("Shutting down the ThreadScheduledExecutor ");

        if(scheduler!=null){
        	scheduler.shutdownNow();
        }

        if(scheduler!=null){
	        while(!scheduler.isTerminated()){
	            LogWriter.logInfoMessage("Waiting for pending tasks to be completed... before shutting down the scheduler");

	        }
        }
        LogWriter.logInfoMessage("ThreadScheduledExecutor has been shutdown succefully");

        if(scheduledFuture!=null){
        	scheduledFuture.cancel(true);
        }
        if(scheduledExecutorService!=null){
     //   	scheduledExecutorService.shutdown();
        	scheduledExecutorService.shutdownNow();
        	   while(!scheduledExecutorService.isTerminated()){
   	            LogWriter.logInfoMessage("Shorex Id generation thread is stopped ................");

   	        }
        }
        if(ConfReader.getBooleanEntry("shorexid.enable",false)){
        String cpsConfPath=contextEvent.getServletContext().getRealPath(File.separator)+ System.getProperty("cps.file.path",DEFAULT_CONFIG_FILEPATH + "cps.conf");

        String cpsTempFilePath=contextEvent.getServletContext().getRealPath(File.separator)+ System.getProperty("cps.file.path",DEFAULT_CONFIG_FILEPATH + "cpsfile.conf");
        updateServerStoppedTime(cpsConfPath,cpsTempFilePath);
        }

    }

    /**
     * This method creates new Timer task and assign the job of creating or
     * fetching the experianToken from a file
     *
     * @throws Exception
     */

    public void experianTokenJob(ScheduledExecutorService scheduler) throws Exception {
        logger.logInfoMessage("experian token scheduler execution started....................");

        long tokenGenerationIntervalInMillis = ConfReader.getLongEntry(
                "experian.generate.token.interval", 27000000);

        // Instantiate the timer as Daemon thread so that tomcat shutdown will
        // not have any issues with running thread.
       // scheduler = Executors.newSingleThreadScheduledExecutor();


        final ThreadFactory threadFactory =  new ThreadFactory() {
            public Thread newThread(Runnable r) {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                return t;
            }
        };

        scheduler = Executors.newSingleThreadScheduledExecutor(threadFactory);
        this.scheduler=scheduler;

        ExperianTokenJobRun tokenJob = new ExperianTokenJobRun();
        ExperianTokenManagement tokenData = null;
        Date lastDate = null;
        Date currentDate = null;
        File file = new File(ConfReader.getConfEntry("experian.tokenFile", "")
                + "/" + "tokenFile.txt");
        try {
            if (!file.exists()) {
                file.createNewFile();
                scheduler.scheduleAtFixedRate(tokenJob, 0,
                        tokenGenerationIntervalInMillis,TimeUnit.MILLISECONDS);
            }
            else{
                tokenData = getTokenFromFile(file);
            }
         }
             catch (FileNotFoundException ex) {
                logger.logInfoMessage("FileNotFound Exception......................."
                        + ex);
            } catch (IOException ex) {
                logger.logInfoMessage("IOException ................................."
                        + ex);
            }


        if (null == tokenData) {
            logger.logInfoMessage("Server start up token data is NULL , Get the new token immediately and update in the system ");
            scheduler.scheduleAtFixedRate(tokenJob, 0,
                    tokenGenerationIntervalInMillis,TimeUnit.MILLISECONDS);
        } else {
            logger.logInfoMessage("Server start up token data is not null ");
            lastDate = tokenData.getLastDate();
            currentDate = new Date();
            long differenceInTime= currentDate.getTime() - lastDate.getTime();
            long seconds = differenceInTime / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            logger.logInfoMessage("differenceInTime from current time to last tokenfile modified time is  "+days+":"+hours+":"+minutes+":"+seconds);

            if (differenceInTime > tokenGenerationIntervalInMillis) {
                logger.logInfoMessage("At Server start up token has expired. Get the new token immediately and update in the system ");
                scheduler.scheduleAtFixedRate(tokenJob, 0,
                        tokenGenerationIntervalInMillis,TimeUnit.MILLISECONDS);
            } else {
                logger.logInfoMessage("At Server start up token is valid, but set the timer to get the new token in future.. ");
                PaymentStore.getInstance().experianTokenData("experianToken",
                		tokenData);
                scheduler.scheduleAtFixedRate(tokenJob,
                        (tokenGenerationIntervalInMillis - differenceInTime),
                        tokenGenerationIntervalInMillis,TimeUnit.MILLISECONDS);
                long differenceInInterval = tokenGenerationIntervalInMillis - differenceInTime;
                logNextTokenJobRunTime(differenceInInterval);

            }
        }
    }

    /**
     * This method to log  the next token Job run time
     * @param differenceInInterval
     *
     */
   private void logNextTokenJobRunTime(long differenceInInterval){

       long currenttime1 = System.currentTimeMillis();
       long addeddifferenceInInterval = currenttime1+differenceInInterval;
       Date date = new Date(addeddifferenceInInterval);
       DateFormat format = new SimpleDateFormat("yyyy-MM-dd'Time'-HH:mm:ss");
       format.setTimeZone(TimeZone.getDefault());
       logger.logInfoMessage("At Server start up token is valid, but set the timer to get the new token in future..@ "+format.format(date));

   }


    /**
     * This method gets the token from file
     * @param file
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private ExperianTokenManagement getTokenFromFile(final File file)
            throws FileNotFoundException, IOException, ClassNotFoundException {
        final FileInputStream fis = new FileInputStream(file);
        final ObjectInputStream ois = new ObjectInputStream(fis);
        ExperianTokenManagement TokenData = null;
        try {
            TokenData = (ExperianTokenManagement) ois.readObject();
        } catch (EOFException ex) {
            logger.logInfoMessage("EOFException occured while trying to read token from file .............................."
                    + ex);
        } finally {
            try {
                ois.close();
                fis.close();
            } catch (IOException ex) {
                logger.logInfoMessage("IOException occured while trying to close file streams ......................"
                        + ex);
            }
        }
        return TokenData;
    }


	public void startShorexIdProcess(String cpsConfPath, String cpsTempFilePath) {
		scheduledExecutorService  = Executors.newScheduledThreadPool(1);
		Runnable idJob=new ShorexIdProcess();
		long refundJobDuration=ConfReader.getLongEntry("shorex.refundProcess.duration", 86400000);
		long refundScheduleTime=0;
		try{
		File file=new File(ConfReader.getConfEntry("shorex.file", "")+"/shorexid.ser");
		if(!file.exists()){
			file.createNewFile();
			refundScheduleTime=refundJobDuration;
	//		updateServerStartedTime(cpsConfPath,cpsTempFilePath);
		}else if(file.length()==0){
			refundScheduleTime=refundJobDuration;
//			updateServerStartedTime(cpsConfPath,cpsTempFilePath);
		}else{
			File cpsFile=new File(cpsConfPath);
			FileInputStream fis=new FileInputStream(file);
			Properties prop=new Properties();
			prop.load(fis);
			String serverTime=prop.getProperty("cps.serverStoppedTime");
			long serverStoppedTime=0;
			long difference=0;
			Date d=null;
			if(StringUtils.isNotEmpty(serverTime)){
			serverStoppedTime=Long.parseLong(serverTime);
			}
			if(serverStoppedTime!=0){
				d=new Date();
				d.setTime(serverStoppedTime);
				difference=System.currentTimeMillis()-d.getTime();
			}
			if(difference<refundJobDuration){
				refundScheduleTime=refundJobDuration-difference;
			}else{
				idJob.run();
				long value=difference%86400000;
				refundScheduleTime=value;
			}
			fis.close();
		}
		scheduledFuture=scheduledExecutorService.scheduleAtFixedRate(idJob, refundScheduleTime,refundJobDuration, TimeUnit.MILLISECONDS);

		}catch(Exception ex){
			  logger.logInfoMessage("Error occured while resheduling job for shorex UID......"
                      + ex);
			  ex.printStackTrace();
		}

	}

    public void updateServerStoppedTime(String filePath,String cpsTempFilePath){


    	/*File cpsFile=new File(filePath);
		FileInputStream fis=new FileInputStream(cpsFile);
		Properties prop=new Properties();
		prop.load(fis);
		fis.close();
		Date d=new Date();
		FileOutputStream fos=new FileOutputStream(new File(filePath),true);

		prop.setProperty("cps.serverStartedTime",Long.toString(d.getTime()));
		prop.store(fos,"#Server time has been updated successfully2..........");
		fos.close();
		fis.close();
    		Date d=new Date();
		FileWriter fw=new FileWriter(filePath+"/cps.conf",true);
		fw.write("cps.serverStartedTime="+d.getTime());
		fw.close();

		PropertiesConfiguration config = new PropertiesConfiguration(filePath);
		config.setProperty("cps.serverStartedTime", Long.toString(d.getTime()));
		config.save();*/

    		try{
    			BufferedReader file = new BufferedReader(new FileReader(filePath));
    			File tempFile=new File(cpsTempFilePath);
    			if(!tempFile.exists()){
    				tempFile.createNewFile();
    			}
    			PrintWriter pw=new PrintWriter(cpsTempFilePath);
    			BufferedWriter writer=new BufferedWriter(pw);
  	        String line;

    	        while ((line = file.readLine()) != null) {

    	            if(line.contains("cps.serverStoppedTime")){
    	                String str="cps.serverStoppedTime="+System.currentTimeMillis();
    	                writer.write(str);
    	                writer.flush();
    	               writer.newLine();

    	            }
    	            else{
    	            	 writer.write(line);
    	            	 writer.flush();
    	            	 writer.newLine();
    	            }

    	        }
    	        file.close();
    	        writer.close();
    	    	File f=new File(filePath);
    	        f.delete();
    	        File renameToFile=new File(filePath);
    	       File ff=new File(cpsTempFilePath);
    	        ff.renameTo(renameToFile);

    /*
    	        FileOutputStream fileOut = new FileOutputStream("cps.conf");
    	        fileOut.write(inputBuffer.toString().getBytes());
    	        fileOut.close();*/

    	    } catch (Exception ex) {
    	        LogWriter.logInfoMessage("Problem reading file.");
    	        ex.printStackTrace();
    	    }

    	}


}


