/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: NonPaymentDataValidator.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2010-10-12 08:25:26 $
 *
 * $Author: arpan.k@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.validation.validator;

import java.util.Map;
import java.util.Map.Entry;

import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.owasp.validator.html.ScanException;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.log.LogWriter;


/**
 * This class validates the benefits overlay html content (coming from
 * outside the cps and needs to be hosted on the payment page) and
 * resets the content to empty in case the content is found to be unsafe.
 */
public final class SafeHTMLValidator
{
    /**
     * The no args constructor for the utility class.Its private so that
     * no instantiation is requiored to use it.
     */
    private  SafeHTMLValidator()
    {
      //The no args constructor for the utility class.
    }

   /**
     * The absolute path is required since antisamy xml cannot be loaded
     * form the classpath(If you discover how to do so then please remove this variable).
     * This variable needs to be set at context startup.
     */
   private static String contextPath = "";



   /**
    * Sets the context path of the application.
    * @return the contextPath
    */
    public static String getContextPath()
    {
       return contextPath;
    }

   /**
    * gets the context path of the application.
    * @param contextPath the contextPath to set
    */
    public static void setContextPath(String contextPath)
    {
       SafeHTMLValidator.contextPath = contextPath;
    }

   /**
     *  This method shall make use of antisamy-esapi.xml to
     *  determine if the content passed is safe for hosting within cps.
     *  any change in the validation schemes need to be done in the
     *  antisamy-esapi.xml.
     *
     *  @param contentToBeValidated the external content to be hosted by cps.
     *
     *  @return isSafe the flag to denote if the content is safe or not.
     */
   private static boolean validateSafeHTML(String contentToBeValidated)
   {
     Policy policy = null;
     boolean  isSafe = false;
     try
     {
        policy = Policy.getInstance(contextPath + "/WEB-INF/classes/antisamy-esapi.xml");
     }
     catch (PolicyException pe)
     {
       pe.printStackTrace();
       LogWriter.logInfoMessage("There was a problem in the "
                + "esapi antisamy policy, hence resetting the benefit "
                + "content to empty");
     }
     AntiSamy antiSamy = new AntiSamy();
     try
     {
        if (null != policy)
        {
           CleanResults cleanResults = antiSamy.scan(contentToBeValidated , policy);
           isSafe = (cleanResults.getNumberOfErrors() == 0);
           if (!isSafe)
           {
               LogWriter.logInfoMessage("The benefits content sent is deemed unsafe as it"
                       + " is suspected to contain malicious/unsafe code, hence clearing the"
                       + "benefit content."
                       );
           }
        }
     }
     catch (ScanException e)
     {
         LogWriter.logInfoMessage("There was a problem while "
                 + "scanning the content using esapi antisamy policy, hence resetting the benefit "
                 + "content to empty");
     }
     catch (PolicyException e)
     {
         LogWriter.logInfoMessage("There was a problem in the "
                 + "esapi antisamy policy, hence resetting the benefit "
                 + "content to empty");
     }
     return isSafe;
   }

   /**
    * This method extracts the benefits content from bookinginfo.bookingComponent.
    * accomodationSummary.additionalDetails and resets it to an empty string
    * if its content is not safe for hosting.
    *
    * @param bookingComponent the bookingComponent object that carries the content
    * nested within it.
    */
   public static void validateBenefitsContent(BookingComponent bookingComponent)
   {
      Map < String , String > additionalDetails = bookingComponent
      .getAccommodationSummary().getAdditionalDetails();
      for (Entry < String , String > entry
             : additionalDetails.entrySet())
      {
         if (!validateSafeHTML(entry.getValue()))
         {
           additionalDetails.put(entry.getKey(), "");
         }
      }
   }
   
   /**
    * This method extracts the additional details from bookinginfo.bookingComponent.
    * cruiseSummary.additionalDetails and resets it to an empty string if its content is not safe
    * for hosting.
    *
    * @param bookingComponent the bookingComponent object that carries the content
    * nested within it.
    */
   public static void validateCruiseAdditionalDetails(BookingComponent bookingComponent)
   {
      if ((bookingComponent != null) && (bookingComponent.getCruiseSummary() != null)
         && (bookingComponent.getCruiseSummary().getAdditionalDetails() != null))
      {
         Map<String, String> cruiseAdditionalDetails =
            bookingComponent.getCruiseSummary().getAdditionalDetails();
         for (Entry<String, String> entry : cruiseAdditionalDetails.entrySet())
         {
            if (!validateSafeHTML(entry.getValue()))
            {
               cruiseAdditionalDetails.put(entry.getKey(), "");
            }
         }
      }
   }
}
