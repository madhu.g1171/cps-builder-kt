/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: NonPaymentDataValidator.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-06-22 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.validation.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.ImportantInformationDataPanel;
import com.tui.uk.client.domain.LeadPassengerDataPanel;
import com.tui.uk.client.domain.PassengersDataPanel;
import com.tui.uk.client.domain.TermsandConditionsDataPanel;
import com.tui.uk.config.ConfReader;
import com.tui.uk.exception.ValidationException;
import com.tui.uk.validation.constants.ValidationConstants;
import com.tui.uk.validation.exception.NonPaymentValidationException;
import org.apache.commons.lang.StringUtils;

/**
 * This class is responsible for validating non payment data. For validation its using all data data
 * panels(<code>PassengersDataPanel</code>, <code>LeadPassengerDataPanel</code>,
 * <code>ImportantInformationDataPanel</code> and <code>TermsandConditionsDataPanel</code>).
 *
 * @author roopesh.s
 *
 */
public final class NonPaymentDataValidator implements Validator
{
   /** The MAX_CHILD_AGE constant. */
   private static final int MAX_CHILD_AGE = 12;

   /** The panel data map. */
   private Map<String, String> panelDataMap = new HashMap<String, String>();

   /** The clientApplication name. */
   private ClientApplication clientApplication;

   /**
    * Constructor for populating panelDataMap.
    * 
    * @param clientApplication clientApplication.
    */
   public NonPaymentDataValidator(ClientApplication clientApplication)
   {
      this.clientApplication = clientApplication;
      // Its loading all the key pattern information to panelDataMap.
      buildPanelDataMap();
   }

   /**
    * This method is responsible for non payment data validation. This method is used to validate
    * single field. It will throw <code>ValidationException</code>, if validation fail.
    * 
    * @param fieldName the field name in JSP page.
    * @param fieldValue the value entered for fieldName.
    * 
    * @throws ValidationException if validation fails.
    */
   public void validate(String fieldName, String fieldValue) throws ValidationException
   {
      // extracting key from field name
      String key = extractKey(fieldName);

      if (validationRequired(key) && !patternCheck(key, fieldValue))
      {
         // if data is not valid throwing exception.
         throw new NonPaymentValidationException(key, getErrorMessage(key));
      }

   }

   /**
    * This method is responsible for populating panelDataMap with the details of the specified
    * fields in the validation list.
    */
   private void buildPanelDataMap()
   {
      String[] validationArray =
         ConfReader.getStringValues(clientApplication.getClientApplicationName()
            + ".validationList", null, ",");

      if (validationArray != null && validationArray.length > 0)
      {
         List<String> validationList = Arrays.asList(validationArray);
         panelDataMap.putAll(ImportantInformationDataPanel.getAllPatterns(validationList));
         panelDataMap.putAll(PassengersDataPanel.getAllPatterns(validationList));
         panelDataMap.putAll(LeadPassengerDataPanel.getAllPatterns(validationList));
         panelDataMap.putAll(TermsandConditionsDataPanel.getAllPatterns(validationList));
      }
   }

   /**
    * This method is responsible for data validation of multiple fields. If exceptionRequired
    * parameter is true, an exception will be thrown once any validation fails. If exceptionRequired
    * parameter is false, all the errors are accumulated and then sent back through map.
    * 
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param exceptionRequired the check for exception.
    * 
    * @return map with key as field name and value as error message for each validation failure.
    * 
    * @throws ValidationException if validation fails.
    */

   public Map<String, String> validate(Map<String, String> fieldNameValueMap,
      boolean exceptionRequired) throws ValidationException
   {
      Map<String, String> errorMessageMap = new HashMap<String, String>();
      Map<String, String> seniorDataMap = new HashMap<String, String>();

      for (Entry<String, String> entry : fieldNameValueMap.entrySet())
      {
         String fieldName = entry.getKey();
         String fieldValue = entry.getValue();
         if (fieldName.contains(ValidationConstants.AGE_FOR_VALIDATION))
         {
            validateChild(fieldName, fieldNameValueMap, errorMessageMap, exceptionRequired);
         }
         else if (fieldName.contains(ValidationConstants.AGEBETWEEN_65_TO_75)
            || fieldName.contains(ValidationConstants.AGEBETWEEN_76_TO_84))
         {
            if (fieldValue.equals("true"))
            {
               seniorDataMap.put(fieldName, fieldValue);
            }
         }
         else if (fieldName.contains(ValidationConstants.POST_CODE))
         {
            validatePostCode(fieldName, fieldValue, fieldNameValueMap, errorMessageMap,
               exceptionRequired);
         }
         else
         {
            validateOtherDetails(fieldName, fieldValue, errorMessageMap, exceptionRequired);
         }
      }

      validateSenior(fieldNameValueMap, seniorDataMap, errorMessageMap, exceptionRequired);
      validateAdultAccompanyingInfant(fieldNameValueMap, errorMessageMap, exceptionRequired);
      validateChildMeals(fieldNameValueMap, errorMessageMap, exceptionRequired);

      return errorMessageMap;

   }

   /**
    * This method is responsible for validating the postCode value.
    * 
    * @param field the field name.
    * @param fieldValue the value for the field name.
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param exceptionRequired the check for exception.
    * 
    * @throws ValidationException if validation fails.
    */
   private void validatePostCode(String field, String fieldValue,
      Map<String, String> fieldNameValueMap, Map<String, String> errorMessageMap,
      boolean exceptionRequired) throws ValidationException
   {
      if (StringUtils.isNotBlank(fieldNameValueMap.get(ValidationConstants.COUNTRY))
         && validationRequired(field)
         && StringUtils.equals(fieldNameValueMap.get(ValidationConstants.COUNTRY), "GB")
         && !patternCheck(field, fieldValue))
      {
         handleErrorMessage(exceptionRequired, field, errorMessageMap, null);
      }
   }

   /**
    * This method is responsible for validating the details other than child and senior data.
    * 
    * @param fieldName the field name.
    * @param fieldValue the value for the field name.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param exceptionRequired the check for exception.
    * 
    * @throws ValidationException if validation fails.
    */
   private void validateOtherDetails(String fieldName, String fieldValue,
      Map<String, String> errorMessageMap, boolean exceptionRequired) throws ValidationException
   {

      String field = extractKey(fieldName);
      if (validationRequired(field))
      {
         validatePassengerDetails(field, fieldValue, errorMessageMap, exceptionRequired);
      }
   }

   /**
    * This method is responsible for child validation.
    * 
    * @param fieldName the field name.
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param exceptionRequired the check for exception.
    * 
    * @throws ValidationException if validation fails.
    */
   private void validateChild(String fieldName, Map<String, String> fieldNameValueMap,
      Map<String, String> errorMessageMap, boolean exceptionRequired) throws ValidationException
   {
      String message = DOBValidator.validateDOB(fieldNameValueMap, fieldName, clientApplication);
      if (!message.equals(ValidationConstants.NOERROR))
      {
         handleErrorMessage(exceptionRequired, fieldName, errorMessageMap, message);
      }
   }

   /**
    * This method is responsible for senior validation.
    * 
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param seniorDataMap the senior data map, contain senior related data.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param exceptionRequired the check for exception.
    * 
    * @throws ValidationException if validation fails.
    */
   private void validateSenior(Map<String, String> fieldNameValueMap,
      Map<String, String> seniorDataMap, Map<String, String> errorMessageMap,
      boolean exceptionRequired) throws ValidationException
   {
      int numberfrom65to75Selected = 0;
      int numberfrom76to84Selected = 0;

      for (Entry<String, String> entry : seniorDataMap.entrySet())
      {
         String fieldName = entry.getKey();
         String fieldValue = entry.getValue();

         /*
          * if (fieldName.contains(ValidationConstants.AGEBETWEEN_65_TO_75)) { if
          * (fieldValue.equals(trueValue)) { numberfrom65to75Selected++; } } else if
          * (fieldName.contains(ValidationConstants.AGEBETWEEN_76_TO_84)) { if
          * (fieldValue.equals(trueValue)) { numberfrom76to84Selected++; } }
          */
         if (fieldValue.equals("true"))
         {
            if (fieldName.contains(ValidationConstants.AGEBETWEEN_65_TO_75))
            {
               numberfrom65to75Selected++;
            }
            else if (fieldName.contains(ValidationConstants.AGEBETWEEN_76_TO_84))
            {
               numberfrom76to84Selected++;
            }
         }
      }

      // for senior citizen validation
      String message =
         seniorPassengerValidation(fieldNameValueMap, numberfrom65to75Selected,
            numberfrom76to84Selected);

      if (!message.equals(ValidationConstants.NOERROR))
      {
         handleErrorMessage(exceptionRequired, "Over65Validation", errorMessageMap, message);
      }
   }

   /**
    * This method is responsible for passenger details validation.
    * 
    * @param fieldName the field name.
    * @param fieldValue the field value.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param exceptionRequired the check for exception.
    * 
    * @throws ValidationException if validation fails.
    */
   private void validatePassengerDetails(String fieldName, String fieldValue,
      Map<String, String> errorMessageMap, boolean exceptionRequired) throws ValidationException
   {
      String field = extractKey(fieldName);

      if (validationRequired(field) && !patternCheck(field, fieldValue))
      {
         handleErrorMessage(exceptionRequired, field, errorMessageMap, null);
      }
   }

   /**
    * Performs validation for Over65 indicators specified in payment page.
    * 
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param numberfrom65to75Selected the count of checked 65 to 75 box in payment page.
    * @param numberfrom76to84Selected the count of checked 76 to 84 box in payment page.
    * 
    * @return error message.
    * @throws ValidationException if validation fails.
    */
   private String seniorPassengerValidation(Map<String, String> fieldNameValueMap,
      int numberfrom65to75Selected, int numberfrom76to84Selected) throws ValidationException
   {
      if (StringUtils.isNotBlank(fieldNameValueMap.get(ValidationConstants.SENIOR_COUNT)))
      {
         int seniorPassengerCount =
            Integer.parseInt(fieldNameValueMap.get(ValidationConstants.SENIOR_COUNT));

         if (seniorPassengerCount != 0 && seniorPassengerCount != numberfrom65to75Selected)
         {
            return getErrorMessage("over65donotmatch");
         }
      }
      if (StringUtils.isNotBlank(fieldNameValueMap.get(ValidationConstants.SENIOR2_COUNT)))
      {
         int senior2PassengerCount =
            Integer.parseInt(fieldNameValueMap.get(ValidationConstants.SENIOR2_COUNT));

         if (senior2PassengerCount != 0 && senior2PassengerCount != numberfrom76to84Selected)
         {
            return getErrorMessage("over76donotmatch");
         }

      }
      return ValidationConstants.NOERROR;
   }

   /**
    * This method returns key(surName or foreName or title) from fieldName(personaldetails_
    * [index]_foreName or personaldetails_[index]_surName or personaldetails_[index]_title).
    * 
    * @param fieldName the field name in jsp page.
    * 
    * @return fieldName.
    */
   private String extractKey(String fieldName)
   {
      // if block for field
      // name(personaldetails_[index]_foreName,personaldetails_[index]_surName
      // and personaldetails_[index]_title field name).
      String field = fieldName;
      if (StringUtils.isNotBlank(field)
         && field.contains(String.valueOf(ValidationConstants.UNDERSCORE)))
      {
         // extracting surName or title or foreName.
         field = field.substring(field.lastIndexOf(ValidationConstants.UNDERSCORE) + 1);
      }
      return field;
   }

   /**
    * This method is responsible for pattern check for the fields like houseName, postCode,
    * dayTimePhone, city, county, mobilePhone that are used in the validation.
    * 
    * @param fieldName the field name.
    * @param fieldValue the field value.
    * 
    * @return boolean the true if pattern match.
    */
   private boolean patternCheck(String fieldName, String fieldValue)
   {
      // panelDataMap having all key and patterns.
      // checking for pattern.
      boolean isPatternMatch = false;
      String fieldPattern = panelDataMap.get(fieldName);
      if (StringUtils.isNotBlank(fieldValue) && StringUtils.isNotBlank(fieldPattern))
      {
         Pattern pattern = Pattern.compile(fieldPattern);
         Matcher matcher = pattern.matcher(fieldValue);
         isPatternMatch = matcher.matches();
      }
      return isPatternMatch;
   }

   /**
    * This method is responsible for check validation is required. if panelData having key, then
    * only do validation. panelDataMap having all key and patterns.
    * 
    * @param key the field name.
    * 
    * @return if validation is required true.
    */
   private boolean validationRequired(String key)
   {
      boolean validationRequired = false;
      if (panelDataMap.containsKey(key))
      {
         validationRequired = true;
      }
      return validationRequired;
   }

   /**
    * This method create error message base on field name.
    * 
    * @param fieldName the field name in cps payment page.
    * 
    * @return error message.
    */
   private String getErrorMessage(String fieldName)
   {
      return ValidationConstants.NON_PAYMENT_DATA_VALIDATION
         + clientApplication.getClientApplicationName() + "." + fieldName;
   }

   /**
    * This method for handling error message. if exceptionRequired is true, it will throw
    * NonPaymentValidationException with error message. if false, it will add error message to
    * errorMessageMap.
    * 
    * @param exceptionRequired the check for exception.
    * @param fieldName the field name in cps payment page.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param errorMessage the error message.
    * 
    * @throws ValidationException if validation fails.
    */
   private void handleErrorMessage(boolean exceptionRequired, String fieldName,
      Map<String, String> errorMessageMap, String errorMessage) throws ValidationException
   {
      String message = errorMessage;
      if (StringUtils.isBlank(message))
      {
         message = getErrorMessage(fieldName);
      }
      if (exceptionRequired)
      {
         // if data is not valid and exceptionRequired is true
         // throwing exception.
         throw new NonPaymentValidationException(fieldName, message);
      }
      else
      {
         // if data is not valid and exceptionRequired is false
         // adding to map.
         errorMessageMap.put(fieldName, message);
      }
   }

   /**
    * This method is responsible for validating more than one Infant for same accompanying adult.
    * 
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param exceptionRequired the check for exception.
    * 
    * @throws ValidationException if validation fails.
    */
   private void validateAdultAccompanyingInfant(Map<String, String> fieldNameValueMap,
      Map<String, String> errorMessageMap, boolean exceptionRequired) throws ValidationException
   {
   String nullValue="null";
      if (fieldNameValueMap.containsKey(ValidationConstants.PASSENGER_INF_COUNT))
      {
         List<String> adultList = new ArrayList<String>();

         int count =
            Integer.parseInt(fieldNameValueMap.get(ValidationConstants.PASSENGER_INF_COUNT));
         for (int i = 1; i <= count; i++)
         {
            String adultTravelWith =
               fieldNameValueMap.get(ValidationConstants.PASSENGER + i
                  + ValidationConstants.TRAVEL_WITH);
            if (adultList.contains(adultTravelWith))
            {
               handleErrorMessage(exceptionRequired, ValidationConstants.PASSENGER + i
                  + ValidationConstants.TRAVEL_WITH, errorMessageMap, 
                  getErrorMessage("travelWith"));
               break;
            }
            else
            {
               if (!nullValue.equals(adultTravelWith) && adultTravelWith != null
                  && adultTravelWith != "null")
               {

                  adultList.add(adultTravelWith);
               }
            }
         }
      }
   }

   /**
    * This method is responsible for validating child meals. if child age is more then 12, then
    * adult meals should be selected.
    * 
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param errorMessageMap map with key as field name and value as error message for each
    *           validation failure.
    * @param exceptionRequired the check for exception.
    * 
    * @throws ValidationException if validation fails.
    */
   private void validateChildMeals(Map<String, String> fieldNameValueMap,
      Map<String, String> errorMessageMap, boolean exceptionRequired) throws ValidationException
   {
      if (fieldNameValueMap.containsKey(ValidationConstants.PASSENGER_CHD_COUNT))
      {
         int count =
            Integer.parseInt(fieldNameValueMap.get(ValidationConstants.PASSENGER_CHD_COUNT));
         for (int i = 1; i <= count; i++)
         {
            Boolean childMealsSelected =
               Boolean.valueOf(fieldNameValueMap.get(ValidationConstants.PASSENGER + i
                  + ValidationConstants.CHILD_MEALS_SELECTED));
            if (!StringUtils.isBlank(fieldNameValueMap.get(ValidationConstants.PASSENGER + i
               + ValidationConstants.CHILD_AGE)))
            {
               int childAge =
                  Integer.parseInt(fieldNameValueMap.get(ValidationConstants.PASSENGER + i
                     + ValidationConstants.CHILD_AGE));

               if (childAge > MAX_CHILD_AGE && childMealsSelected)
               {
                  String foreName =
                     fieldNameValueMap.get(ValidationConstants.PASSENGER + i
                        + ValidationConstants.CHILD_FORE_NAME);
                  String lastName =
                     fieldNameValueMap.get(ValidationConstants.PASSENGER + i
                        + ValidationConstants.CHILD_LAST_NAME);
                  String message = getErrorMessage("childMeals") + "|" + foreName + "|" + lastName;
                  handleErrorMessage(exceptionRequired, ValidationConstants.PASSENGER + i
                     + ValidationConstants.CHILD_MEALS_SELECTED, errorMessageMap, message);
               }

            }
         }
      }
   }
}
