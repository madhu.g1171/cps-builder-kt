/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Validator.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-05-18 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.validation.validator;

import java.util.Map;

import com.tui.uk.exception.ValidationException;

/**
 * This is an interface for validating data.
 *
 * The <code>Validator</code> validates the data available in payment page. The payment page is
 * divided into 4 parts <code>PassengersDataPanel</code>, <code>LeadPassengerDataPanel</code>,
 * <code>ImportantInformationDataPanel</code> and <code>TermsandConditionsDataPanel</code>.
 *
 * @author roopesh.s
 *
 */
public interface Validator
{
   /**
    * This method is responsible for data validation. This method is used to validate single field.
    * It will throw <code>ValidationException</code>, if validation fail.
    *
    * @param fieldName the field name in JSP page.
    * @param fieldValue the value entered for fieldName.
    *
    * @throws ValidationException if validation fails.
    */
   void validate(String fieldName, String fieldValue) throws ValidationException;

   /**
    * This method is responsible for data validation of multiple fields. If exceptionRequired
    * parameter is true, an exception will be thrown once any validation fails. If exceptionRequired
    * parameter is false, all the errors are accumulated and then sent back through map.
    *
    * @param fieldNameValueMap map with key as field name, value as value entered for the field.
    * @param exceptionRequired the check for exception.
    *
    * @return map with key as field name and value as error message for each validation failure.
    * @throws ValidationException if validation fails.
    */
   Map<String, String> validate(Map<String, String> fieldNameValueMap, boolean exceptionRequired)
      throws ValidationException;
}
