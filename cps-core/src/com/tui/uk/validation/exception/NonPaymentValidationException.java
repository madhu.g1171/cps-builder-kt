/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: NonPaymentValidationException.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2009-05-18 08:25:26 $
 * 
 * $Author: roopesh.s@sonata-software.com $
 * 
 * 
 * $Log: $.
 */
package com.tui.uk.validation.exception;

import com.tui.uk.exception.ValidationException;

/**
 * This is the exception thrown when the non payment data validation fails.
 * 
 * @author roopesh.s
 * 
 */
@SuppressWarnings("serial")
public class NonPaymentValidationException extends ValidationException
{
   /** The code associated with this exception. */
   private String code;
   
   /**
    * Default constructor for NonPaymentValidationException class.
    */
   public NonPaymentValidationException()
   {
      super();
   }

   /**
    * Constructor to create the exception by specifying a custom message.
    * 
    * @param message The custom message associated with this exception.
    */
   public NonPaymentValidationException(String message)
   {
      super(message);
   }
   
   /**
    * Constructor to create this exception with a code and a message.
    * 
    * @param code the exception code.
    * @param message the exception message.
    */
   public NonPaymentValidationException(String code, String message)
   {
      super(message);
      this.code = code;
   }

   /**
    * Defines the constructor for NonPaymentValidationException with
    * specified cause.
    * 
    * @param throwable - The specified cause.
    */
   public NonPaymentValidationException(Throwable throwable)
   {
      super(throwable);
   }
   
   /**
    * Constructor to create this exception with a code and a throwable.
    * 
    * @param throwable - The specified cause for the exception.
    * @param code the exception code.
    */
   public NonPaymentValidationException(Throwable throwable, String code)
   {
      super(throwable);
      this.code = code;
   }

   /**
    * Defines the constructor for NonPaymentValidationException with
    * detailed message and specified cause.
    * 
    * @param message - The detailed exception message
    * @param throwable - The specified cause for the exception
    */
   public NonPaymentValidationException(String message, Throwable throwable)
   {
      super(message, throwable);
   }
   
   /**
    * Returns the code of the exception.
    * 
    * @return the code associated with this exception.
    */
   public String getCode()
   {
      return code;
   }
}
