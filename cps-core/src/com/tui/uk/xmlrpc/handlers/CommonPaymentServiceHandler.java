/*
. * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: CommonPaymentServiceHandler.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : veena.bn $
 *
 * $Log : $
 */
package com.tui.uk.xmlrpc.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Date;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import org.apache.xmlrpc.XmlRpcException;

import com.tui.uk.cacheeventlistener.TransactionStatusUtil;
import com.tui.uk.client.domain.AuthorizePayPalTransaction;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.EncryptedCard;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PayPalAuthorizeResponse;
import com.tui.uk.client.domain.PayPalComponent;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutResponse;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.client.domain.DDPaymentSheduleComponent;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PostPaymentGuaranteeTransaction;
import com.tui.uk.cacheeventlistener.TransactionStatus;
import  com.tui.uk.client.domain.PaymentScheduleData;

/**
 * This class is an interface between client application and payment server for Xml RPC calls
 * supported by CPC. This provides methods for generating the token, getting essential transaction
 * data, getting encrypted card data, getting non Payment related data, purging payment data and for
 * registering errors.
 *
 * @author veena.bn
 */
public final class CommonPaymentServiceHandler
{

   /** This variable has been added to remove checkstyle errors. */
   private static final String AND_BRAND = "and brand : ";

   /**
    * This method stores booking information sent from client application against a unique token and
    * sends the unique token back to client.
    *
    * @param bookingComponent the BookingComponent.
    * @return String the token.
    */
   public String generateToken(BookingComponent bookingComponent)
   {
      LogWriter.logMethodStart("generateToken", bookingComponent);
      LogWriter.logInfoMessage("Generating token started");
      UUID uuid = PaymentStore.getInstance().newPaymentData(new PaymentData(bookingComponent));
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      LogWriter.logInfoMessage("Successfully generated token " + uuid + " for the brand "
         + bookingComponent.getClientApplication().getClientApplicationName());
      if(bookingComponent. getPaypalComponent()!=null){

    	  LogWriter.logInfoMessage("PaypalComponent Received for token :" + uuid +"And VTid : "+bookingComponent. getPaypalComponent().getvTid());
    	  LogWriter.logInfoMessage("And isPayPalEnabled :"+bookingComponent. getPaypalComponent().getEnablePaypal());
    	  LogWriter.logInfoMessage("And isPaypalOptionActive :"+bookingComponent. getPaypalComponent().isPaypalOptionActive());

      }else{

    	  LogWriter.logInfoMessage("No PayPalComponent Received for token :" + uuid +"And VTid : "+bookingComponent.getPaymentGatewayVirtualTerminalId());
      }
      LogWriter.logInfoMessage("Direct Debit flag : "+(String)PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getPaymentDataMap().get("dddeposit"));
      if(bookingComponent.getDdVariant() != null){
         LogWriter.logInfoMessage("Direct Debit variant for this request : "+bookingComponent.getDdVariant().getDdVariantName()+","+bookingComponent.getDdVariant().getCode());
         LogWriter.logInfoMessage("Direct Debit enabled for this request : "+PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent().getNonPaymentData().get("directDebitVariant"));
      }
      LogWriter.logMethodEnd("generateToken");

      new TransactionStatusUtil().updateStatus(paymentData, uuid.toString(),
         paymentData.getTransactionStatus(), TransactionStatus.findByCodeTS("TOKEN_GENERATED"));

      return uuid.toString();
   }

   /**
    * This method stores booking information sent by .NET client application against a unique token
    * and sends the unique token back to client.
    *
    * This is a trial API for .NET, which takes allowed amounts as Object array and payment types
    * also as Object array.
    *
    * @param currency A three character ISO currency indicator. For most applications this will be
    *           "GBP" or "EUR".
    * @param countryLocale the country name for the locale.
    * @param clientApplication the client application.
    * @param datacashVTid the datacash vTid.
    * @param allowedAmounts An array of allowed payment amounts, typically the full payment amount,
    *           deposit and low deposit. If all three are allowed, the array will consist of three
    *           values. The client should send this as a double[] data.
    * @param paymentTypes An array of allowed payment types. Eg: Cash, Cheque, etc. For B2C
    *           applications, this will always contain only one entry as "DCard". The client should
    *           send this data as String[]data.
    * @param applicationData Information in the form of key-value pair that the client application
    *           would like to be included on the payment page.
    *
    * @return String the token.
    */
   public String generateToken2(String currency, String countryLocale, String clientApplication,
      String datacashVTid, Object[] allowedAmounts, Object[] paymentTypes,
      Map<String, String> applicationData)
   {
      LogWriter.logMethodStart("generateToken2", currency, allowedAmounts, paymentTypes,
         applicationData);
      LogWriter.logInfoMessage("Generating token2 started");

      BookingInfo bookingInfo =
         new BookingInfo(currency, countryLocale, clientApplication, datacashVTid, allowedAmounts,
            paymentTypes, applicationData);

      UUID uuid = PaymentStore.getInstance().newPaymentData(new PaymentData(bookingInfo));
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      LogWriter.logInfoMessage("Successfully generated token2 " + uuid + " for the client"
         + clientApplication);
      LogWriter.logMethodEnd("generateToken2");

      new TransactionStatusUtil().updateStatus(paymentData, uuid.toString(),
         paymentData.getTransactionStatus(), TransactionStatus.findByCodeTS("TOKEN_GENERATED"));

      return uuid.toString();
   }

   /**
    * This method implementation will reconstruct the bookingcomponent object to reuse the existing
    * refund flow supported for Java clients.
    *
    * @param currency A three character ISO currency indicator. For most applications this will be
    *           "GBP" or "EUR".
    * @param countryLocale the country name for the locale.
    * @param clientApplication the client application.
    * @param datacashVTid the datacash vTid.
    * @param refundAmount the refund Amount.
    * @param refundDetails the refund Details.
    * @param paymentTypes An array of allowed payment types. Eg: Cash, Cheque, etc. For B2C
    *           applications, this will always contain only one entry as "DCard". The client should
    *           send this data as String[]data.
    * @param applicationData Information in the form of key-value pair that the client application
    *           would like to be included on the payment page.
    * @return String the token.
    */
   public String generateTokenForRefund(String currency, String countryLocale,
      String clientApplication, String datacashVTid, String refundAmount, Object[] refundDetails,
      Object[] paymentTypes, Map<String, String> applicationData)
   {
      LogWriter.logMethodStart("generateTokenForRefund", currency, refundDetails, paymentTypes,
         applicationData);
      LogWriter.logInfoMessage("Generating tokenForRefund started");

      BookingInfo bookingInfo =
         new BookingInfo(currency, countryLocale, clientApplication, datacashVTid, refundAmount,
            refundDetails, paymentTypes, applicationData);

      UUID uuid = PaymentStore.getInstance().newPaymentData(new PaymentData(bookingInfo));
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      LogWriter.logInfoMessage("Successfully generated tokenForRefund " + uuid
         + " for the client" + clientApplication);
      LogWriter.logMethodEnd("generateTokenForRefund");

      new TransactionStatusUtil().updateStatus(paymentData, uuid.toString(),
         paymentData.getTransactionStatus(), TransactionStatus.findByCodeTS("TOKEN_GENERATED"));

      return uuid.toString();

   }

   /**
    * This method takes the token as the input parameter and gets the non Payment data such as
    * passenger first name, surname,address etc entered by the user in the Payment Page rendered by
    * CPC. This Map contains the field names in the Payment Page as the key and the corresponding
    * data entered by the user as the value.
    *
    * @param token the token
    * @return the map< string, string>
    */
   public Map<String, String> getNonPaymentData(String token)
   {
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("getNonPaymentData", token);
      LogWriter.logInfoMessage("Getting the non Payment data for the token:" + token + AND_BRAND
         + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logInfoMessage("Retrieved non Payment data for the token " + uuid + AND_BRAND
         + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logMethodEnd("getNonPaymentData");
      return bookingComponent.getNonPaymentData();
   }

   /**
    * This method takes the token as the input parameter and gets essential transaction data like
    * amount,payment method, transaction charges, payment type code and transaction reference.These
    * transaction info are stored in Vision and is essential for reference in future.
    *
    * @param token the token
    * @return the essential transaction data
    */
   public List<EssentialTransactionData> getEssentialTransactionData(String token)
   {
      LogWriter.logInfoMessage("Processing GetEssentialTransactionData call for Token: " + token);
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("getEssentialTransactionData", token);
      LogWriter.logInfoMessage("Getting the essential transaction for the token" + token
         + AND_BRAND + bookingComponent.getClientApplication().getClientApplicationName());

      /* following code added by sreenivasulu.k for PayPal implementation     */

      PayPalComponent  paypalComponent=PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent().getPaypalComponent();
      if(null!=paypalComponent)
      {
	      if(paypalComponent.getEnablePaypal()==true && PaymentStore.getInstance().getPaymentData(uuid).getPayment()==null && paypalComponent.getPayPalExpressCheckoutResponse()!=null){
	    	  Money money=new Money(new BigDecimal(paypalComponent.getPayPalExpressCheckout().getAmount()),bookingComponent.getTotalAmount().getCurrency());
	    	  EssentialTransactionData paypalEssentialTransactionData=new EssentialTransactionData(null,money,null,null);
	    	  paypalEssentialTransactionData.setTransactionReference(paypalComponent.getPayPalExpressCheckoutResponse().getDatacashReference());
	    	  paypalEssentialTransactionData.setDdSelectedByUser(Boolean.parseBoolean(PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getPaymentDataMap().get("ddAndPayPalSelected")));
	    	  paypalEssentialTransactionData.setPaypalExpressCheckoutResponse(paypalComponent.getPayPalExpressCheckoutResponse());
	    	  List<EssentialTransactionData> data=new ArrayList<EssentialTransactionData>();
	    	  data.add(paypalEssentialTransactionData);
	    	  return data;

	      }

      }


      /*
       * following code has been added by sreenivasulu.k for direct debit implementation.
       *  */
     List<EssentialTransactionData> allEssentialTransactionsData= PaymentStore.getInstance().getPaymentData(uuid).getPayment()
      .getAllEssentialTransactionsData();


     String dddeposit=(String)PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getPaymentDataMap().get("dddeposit");


     if(BookingConstants.DD_DEPOSIT_TYPE.equals(dddeposit))
     {
    	  LogWriter.logInfoMessage("Processing low Deposit Plus Direct Debit for Token: " + token);

     EssentialTransactionData essentialTransactionData=allEssentialTransactionsData.get(0);

    	 List<DepositComponent> depositComponents=bookingComponent.getDepositComponents();
    	 List<PaymentScheduleData> psdata=null;
    	 DDPaymentSheduleComponent ddPaymentSheduleComponent=null;
    	 for(DepositComponent depositComponent:depositComponents){
    		 if(depositComponent.getDepositType().equals(BookingConstants.DD_DEPOSIT_TYPE)){

    			 List<DDPaymentSheduleComponent> components=depositComponent.getDdPaymentComponents();


    			 for(DDPaymentSheduleComponent component:components){
    				 if(component.getDefaultSchedule()!=null && component.getDefaultSchedule())
    					 ddPaymentSheduleComponent=component;
    			 }

    			 if(ddPaymentSheduleComponent==null)
    				 ddPaymentSheduleComponent=components.get(0);



    			PaymentScheduleData data=new PaymentScheduleData();


    			data.setAmount(ddPaymentSheduleComponent.getAmount());
    			data.setPaymentScheduleId(ddPaymentSheduleComponent.getPaymentScheduleId());
    			data.setPayCount(ddPaymentSheduleComponent.getPayCount());


    			try{


    				if(depositComponent.getDayOfMonth()!=null && ddPaymentSheduleComponent.getFirstInstallmentMonth()!=null)
    				{
    			    String dateValue=depositComponent.getDayOfMonth()+"-"+ddPaymentSheduleComponent.getFirstInstallmentMonth();

    			    Date firstInstallmentDate =new SimpleDateFormat("dd-MMM-yyyy").parse(dateValue);

    		 	      data.setFirstInstallmentDate(firstInstallmentDate);

    				}
    			  LogWriter.logDebugMessage("low DepositPlus Direct Debit returned data:   Amount = " + data.getAmount().getAmount()+"\t PaymentSheduleId = "+data.getPaymentScheduleId()+"\t PayCount = "+data.getPayCount()+"\t date = "+data.getFirstInstallmentDate() );

    			psdata=new ArrayList<PaymentScheduleData>();
    			psdata.add(data);

    			essentialTransactionData.setPaymentScheduleData(psdata);
    			essentialTransactionData.setDdSelectedByUser(true);

    			}
    			catch(Exception ex){
    				LogWriter.logErrorMessage(ex.getMessage());
    				}




    		 }

    	 }

   }

     LogWriter.logInfoMessage("Retrieved essential transaction for the token " + uuid + AND_BRAND
             + bookingComponent.getClientApplication().getClientApplicationName());
          LogWriter.logMethodEnd("getEssentialTransactionData");

     /* return PaymentStore.getInstance().getPaymentData(uuid).getPayment()
         .getAllEssentialTransactionsData();*/



       /*  if(bookingComponent.getPaypalComponent().getEnablePaypal()){

        	 bookingComponent.getPaypalComponent().setPayPalExpressCheckoutResponse(bookingComponent.getPaypalComponent().getPayPalExpressCheckoutResponse());
         }*/



    	 return allEssentialTransactionsData;
   }

   /**
    * This method takes the token as the input parameter and gets essential transaction data like
    * amount,payment method, transaction charges, payment type code and transaction reference for
    * the .NET clients.
    *
    * @param token the token
    * @return the essential transaction data
    */
   public Map<String, String> getPaymentData(String token)
   {
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("getPaymentData", token);
      LogWriter.logInfoMessage("Getting the payment data for the token" + token + "and brand: "
         + bookingComponent.getClientApplication().getClientApplicationName());
      Map<String, String> paymentDataMap = new HashMap<String, String>();
      for (EssentialTransactionData essentialTransactionData : PaymentStore.getInstance()
         .getPaymentData(uuid).getPayment().getAllEssentialTransactionsData())
      {
         paymentDataMap.put("paymentType", essentialTransactionData.getPaymentTypeCode());
         paymentDataMap.put("paymentAmount",
            String.valueOf(essentialTransactionData.getTransactionAmount().getAmount()));
         paymentDataMap.put("paymentCharge",
            String.valueOf(essentialTransactionData.getTransactionCharge().getAmount()));
         paymentDataMap.put("paymentReference", essentialTransactionData.getTransactionReference());
         paymentDataMap.put("maskedCardNumber", essentialTransactionData.getMaskedCardNumber());
         paymentDataMap.put("authCode", essentialTransactionData.getAuthCode());
         paymentDataMap.put("expiryDate", essentialTransactionData.getExpiryDate());
         paymentDataMap.put("nameOnCard", essentialTransactionData.getNameOnCard());
         paymentDataMap.put("PaymentGatewayResponseCode",
            essentialTransactionData.getPaymentGatewayResponse());

         //added by sreenivasulu for direct debit
        String dddeposit=(String)PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getPaymentDataMap().get("dddeposit");
        if("lowDepositPlusDD".equals(dddeposit)){

        	 LogWriter.logInfoMessage("User has selected lowDepositPlusDD option.....................");

        	paymentDataMap.put("ddSelectedByUser","true");

        }





      }
      LogWriter.logInfoMessage("Retrieved payment data for the token " + uuid + "and brand: "
         + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logMethodEnd("getPaymentData");
      return paymentDataMap;
   }

   /**
    * This method takes the token as the input parameter and gets the encrypted card details.
    *
    * @param token the token.
    * @return the encrypted card.
    */
   public EncryptedCard getEncryptedCardData(String token)
   {
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("getEncryptedCardData", token);
      LogWriter.logInfoMessage("Getting the encrypted card data for the token " + uuid
         + "and brand: " + bookingComponent.getClientApplication().getClientApplicationName());
      List<PaymentTransaction> paymentTransactions =
         PaymentStore.getInstance().getPaymentData(uuid).getPayment()
            .getPaymentTransactions(PaymentMethod.POST_PAYMENT);
      LogWriter
         .logInfoMessage("Retrieved paymentTransactions to get encrypted card for the token" + uuid
            + "and brand:" + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logMethodEnd("getEncryptedCardData");
      return ((PostPaymentGuaranteeTransaction) paymentTransactions.get(0)).getEncryptedCard();
   }

   /**
    * This method is used for removing sensitive data and booking data from payment store. This
    * method returns either True OR exception.
    *
    * @param token unique identifier.
    * @return Boolean returns always true.
    * @throws XmlRpcException the Exception.
    */
   public Boolean purgeClientPaymentPageData(String token) throws XmlRpcException
   {
      return true;
   }

   /**
    * This method is used for purging sensitive data after payment. This method returns either True
    * OR exception as client can proceed without purging payment data and if any issues while
    * purging.
    *
    * @param token unique identifier.
    * @return Boolean returns always true.
    * @throws XmlRpcException the Exception.
    */
   public Boolean purgeSensitiveData(String token) throws XmlRpcException
   {
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      BookingComponent bookingComponent = paymentData.getBookingInfo().getBookingComponent();

      LogWriter.logMethodStart("purgeSensitiveData", token);
      LogWriter.logInfoMessage("Purging the payment data for the token" + uuid + AND_BRAND
         + bookingComponent.getClientApplication().getClientApplicationName());
      PaymentStore.getInstance().destroySensitiveData(uuid);
      LogWriter.logInfoMessage("Removed PaymentData from paymentstore for the given token " + uuid
         + AND_BRAND + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logMethodEnd("purgeSensitiveData");
      new TransactionStatusUtil().updateStatus(paymentData, uuid.toString(),
         paymentData.getTransactionStatus(), TransactionStatus.findByCodeTS("PAYMENTDATA_PURGED"));
      return true;
   }

   /**
    * This method takes the token and error message as
    * the input parameters and gets the payment
    * data for the token and sets the error message in the Booking Component.
    *
    * @param token the token
    * @param errorMsg holds the message to be displayed.
    * @return Boolean returns always true.
    */
   public Boolean registerError(String token, String errorMsg)
   {
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("registerError", token, errorMsg);
      LogWriter.logInfoMessage("Populating the error message in booking component for the token"
         + uuid + AND_BRAND + bookingComponent.getClientApplication().getClientApplicationName());
      PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent()
         .setErrorMessage(errorMsg);
      LogWriter.logInfoMessage("Error message populated in booking component " + uuid + AND_BRAND
         + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logInfoMessage("Error message :" + errorMsg + ":for token :" + uuid);
      LogWriter.logMethodEnd("registerError");
      // As XMlRPC calls should return some value/object, this method will return true
      // without any condition.
      return true;
   }

   /**
    * Sets the allowed cards to the object associated with the token.
    *
    * @param token the token.
    * @param allowedCards the allowed cards.
    *
    * @return Boolean returns always true.
    *
    * @deprecated not required as this made configurable.
    */
   public Boolean setAllowedCards(String token, Map<String, Object[]> allowedCards)
   {
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("setAllowedCards", token, allowedCards);
      LogWriter.logInfoMessage("Populating the allowed cards in booking details for the token"
         + uuid + AND_BRAND + bookingComponent.getClientApplication().getClientApplicationName());
      PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo()
         .setAllowedCards(allowedCards);
      LogWriter.logInfoMessage("Allowed cards populated in booking details for token" + uuid
         + AND_BRAND + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logMethodEnd("setAllowedCards");
      // As XMlRPC calls should return some value/object, this method will return true
      // without any condition.
      return true;
   }

   /**
    * Sets the post payment guarantee cards to the object associated with the token.
    *
    * @param token the token.
    * @param guaranteeCardTypes the guarantee card types.
    *
    * @return Boolean returns always true.
    */
   public Boolean setGuaranteeCardTypes(String token, Object[] guaranteeCardTypes)
   {
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("setGuaranteeCardTypes", token, guaranteeCardTypes);
      LogWriter
         .logInfoMessage("Populating the guarantee card types in booking details for the token"
            + token + AND_BRAND
            + bookingComponent.getClientApplication().getClientApplicationName());
      PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo()
         .setGuaranteeCardTypes(guaranteeCardTypes);

      LogWriter.logInfoMessage("Guarantee card types populated in booking details " + uuid
         + " and brand: " + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logMethodEnd("setGuaranteeCardTypes");

      // As XMlRPC calls should return some value/object, this method will return true
      // without any condition.
      return true;
   }

   /**
    * Notifies to CPS that booking has been completed.
    *
    * @param token the token.
    *
    * @return Boolean always true.
    */
   public Boolean notifyBookingCompletion(String token)
   {
      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      BookingComponent bookingComponent = paymentData.getBookingInfo().getBookingComponent();
      LogWriter.logMethodStart("notifyBookingCompletion", token);
      LogWriter.logInfoMessage("Notifying the booking completion for the token:" + token
         + AND_BRAND + bookingComponent.getClientApplication().getClientApplicationName());

      paymentData.getBookingInfo().setNewHoliday(Boolean.FALSE);
      LogWriter.logInfoMessage("Successfully notified the booking completion fo token:" + token
         + AND_BRAND + bookingComponent.getClientApplication().getClientApplicationName());
      LogWriter.logMethodEnd("notifyBookingCompletion");
      // As XMlRPC calls should return some value/object, this method will return true
      // without any condition.
      new TransactionStatusUtil().updateStatus(paymentData, uuid.toString(),
         paymentData.getTransactionStatus(), TransactionStatus.findByCodeTS("BOOKING_COMPLETED"));
      return true;
   }

   /**
    * This method will notify the cps that price has been changed.
    *
    * @param token token form client application.
    * @param priceStatus value to indicate price changed or not
    */
   public void priceChanged(String token, Boolean priceStatus)
   {
      UUID uuid = UUID.fromString(token);
      BookingComponent bookingComponent =
         PaymentStore.getInstance().getPaymentData(uuid).getBookingInfo().getBookingComponent();
      bookingComponent.setPriceStatus(priceStatus);
   }

   /**
    * This method is used for purging payment data including token. This method returns either True
    * OR exception
    *
    * @param token unique identifier.
    * @return Boolean returns always true.
    * @throws XmlRpcException the Exception.
    */
   public Boolean purgePaymentToken(String token) throws XmlRpcException
   {
      UUID uuid = UUID.fromString(token);
      LogWriter.logInfoMessage("Purging the token :" + uuid);
      PaymentStore.getInstance().destroyPayment(uuid);
      LogWriter.logInfoMessage("Removed token :" + uuid);
      return true;
   }
}
