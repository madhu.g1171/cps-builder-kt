/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BackEndDataCashService.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2009-04-10 05:18:36 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 *
 */

package com.tui.uk.xmlrpc.handlers;

import org.apache.commons.lang.StringUtils;
import org.apache.xmlrpc.XmlRpcException;

import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.BackEndDataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransaction;

/**
 * This class is an interface between client application and payment server for Xml RPC calls
 * supported by CPS. This class is responsible to call different datacash services like
 * backEndFulfillPayment,backEndCancel requested by client applications.
 *
 * @author bibin.j
 */
public final class BackEndDataCashService
{
   /**
    * This method is responsible for fulfilling a payment or refund. It returns response in the
    * following format: 3 digit code|Error Message or data-cash reference no|auth code. Otherwise
    * XmlRpcException is thrown.
    *
    * @param vTid the dataCashAccount.
    * @param datacashReference the datacashReference obtained in the AUTH call.
    * @param authCode the authCode.
    * @param inventoryBookingReference the inventoryBookingReference as the TRACS booking reference.
    *        In case of DP packages, the format is Flight Inventory Reference/Hotel Inventory
    *        Reference.
    *
    * @return String the response from CPS.
    * @throws XmlRpcException the exception during transaction.
    */
   public String backEndFulfillPayment(String vTid, String datacashReference, String authCode,
      String inventoryBookingReference) throws XmlRpcException
   {
      BackEndDataCashPaymentTransaction datacashPaymentTransaction =
         new BackEndDataCashPaymentTransaction(vTid, datacashReference, authCode, "");
      setTransactionTrackingData(inventoryBookingReference, datacashPaymentTransaction);
      LogWriter.logInfoMessage("Sending fulfill request to datacash: "
         + " with datacash account : " + vTid + " and transaction id: " + datacashReference);
      String response = datacashPaymentTransaction.backEndFulfilPayment();
      LogWriter.logInfoMessage("Fulfill request successful");
      return response;
   }

   /**
    * This method is responsible for canceling an authorized payment/refund without fulfilling it.
    * It returns response in the following format: 3 digit code|Error Message or data-cash reference
    * no|auth code. Otherwise XmlRpcException is thrown.
    *
    * @param vTid the dataCashAccount.
    * @param datacashReference the datacashReference obtained in the AUTH call.
    * @param inventoryBookingReference the inventoryBookingReference as the TRACS booking reference.
    *        In case of DP packages, the format is Flight Inventory Reference/Hotel Inventory
    *        Reference.
    *
    * @return String the response from CPS.
    * @throws XmlRpcException the exception during transaction.
    */
   public String backEndCancel(String vTid, String datacashReference,
      String inventoryBookingReference) throws XmlRpcException
   {
      BackEndDataCashPaymentTransaction datacashPaymentTransaction =
         new BackEndDataCashPaymentTransaction(vTid, datacashReference, null, "");
      setTransactionTrackingData(inventoryBookingReference, datacashPaymentTransaction);
      LogWriter.logInfoMessage("Sending cancel request to datacash: " + " with datacash account : "
         + vTid + " and transaction id: " + datacashReference);
      String response = datacashPaymentTransaction.backEndCancel();
      LogWriter.logInfoMessage("Cancel request successful");
      return response;
   }

   /**
    * This method reads the inventory booking reference number from bookingDetails map and sets it
    * into the paymentTransaction object.
    *
    * @param inventoryBookingReference the inventoryBookingReference as the TRACS booking reference.
    *        In case of DP packages, the format is Flight Inventory Reference/Hotel Inventory
    *        Reference.
    * @param paymentTransaction the paymentTransaction object.
    *
    */
   private void setTransactionTrackingData(String inventoryBookingReference,
      PaymentTransaction paymentTransaction)
   {
      TransactionTrackingData trackingData =
         new TransactionTrackingData(StringUtils.EMPTY, StringUtils.EMPTY);
      if (StringUtils.isNotBlank(inventoryBookingReference))
      {
         trackingData.setInvBookingRef(inventoryBookingReference);
      }
      paymentTransaction.setTrackingData(trackingData.toString());
   }
}
