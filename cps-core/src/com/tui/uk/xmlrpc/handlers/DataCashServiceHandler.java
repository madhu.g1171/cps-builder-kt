/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: DataCashServiceHandler.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2008-05-15 05:18:36 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $ Revision 1.2 2008/05/14 09:21:32 sindhushree.g Getting the
 * PaymentStore instance to call the getPaymentData().
 *
 * Revision 1.1 2008/05/13 08:34:30 sindhushree.g Added/Modified to add XMLRPC server support.
 *
 */

package com.tui.uk.xmlrpc.handlers;

import static com.tui.uk.client.domain.BookingConstants.INVENTORY_BOOKING_REFERENCE;
import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.domain.PaymentConstants.HYPHEN;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_SETUP;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_SORT_CODE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_ACCOUNT_NUMBER;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_ACCOUNT_NAME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_METHOD_RS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_STAGE_RS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DD_START_DATE_RS;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.Date;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.Long;

import javax.management.RuntimeErrorException;
import javax.xml.soap.SOAPException;

import com.datacash.errors.FailureReport;
import com.datacash.util.XMLDocument;
import com.tui.uk.client.domain.*;

import org.apache.commons.lang.StringUtils;
import org.apache.xmlrpc.XmlRpcException;
import org.jdom.Document;
import org.jdom.Element;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.tui.uk.payment.service.datacash.PayPalConstants;

import java.util.Properties;

import com.tui.uk.cache.Cache;
import com.tui.uk.cache.NonDiskCacheImpl;
import com.tui.uk.cache.NonDiskCacheManager;
import com.tui.uk.cacheeventlistener.TransactionStatusUtil;
import com.tui.uk.domain.OriginalPaymentMode;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.experianClient.ExperianServiceException;
import com.tui.uk.experianClient.ExperianServiceImplementation;
import com.tui.uk.experianClient.VerifyServiceResponsePopulator;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.DataCashHistoricTransaction;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.GiftCardPaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.processor.FraudScreeningProcessor;
import com.tui.uk.payment.processor.FraudScreeningProcessorImpl;
import com.tui.uk.payment.service.datacash.DataCashService;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;
import com.tui.uk.payment.service.datacash.DataCashServiceConstantsForPayPalTransaction;
import com.tui.uk.payment.service.datacash.exception.DataCashServiceException;
import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.client.domain.PayPalComponent;
import com.tui.uk.client.domain.PayPalCustomerVerificationDetails;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutData;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutResponse;





import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransport;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class is an interface between client application and payment server for
 * Xml RPC calls supported by CPC. This class is responsible to call different
 * datacash services like authPayment,fulfill,authRefund,cancel,reverse,etc.,
 * requested by the client applications.
 *
 * @author sindhushree.g
 */
public final class DataCashServiceHandler {

    /** The auth failed code. */
    private static final int AUTH_FAILED_CODE = 7;

    /** Auth failure error key. */
    private static final String DATACASHE_ERROR_KEY = "datacash.response."
            + AUTH_FAILED_CODE;

    /** Datacash response. */
    private DataCashService dataCashService = null;

    /** PayPal Customer. */
    private PayPalCustomer payPalCustomer = null;
    
    /** The xmlRpcClient. */
    private XmlRpcClient xmlRpcClient;

    /** The HttpClient object. */
    private HttpClient httpClient;


    /**
     * This method is responsible for authentication of the card through
     * datacash.It returns true as XML RPC client requires value/object to be
     * returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param authCode
     *            The auth code. This is optional.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             the exception during transaction.
     */
    public Boolean authPayment(String token, String authCode, String vTid)
            throws XmlRpcException {
        LogWriter.logMethodStart("authPayment", token, authCode, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        BookingComponent bookingComponent = bookingInfo.getBookingComponent();

        String clientAppName = bookingComponent.getClientApplication()
                .getClientApplicationName();

        if (StringUtils.isBlank(bookingComponent
                .getPaymentGatewayVirtualTerminalId())
                && !ConfReader.getBooleanEntry(clientAppName + ".3DSecure",
                        false)) {
            try {
                TransactionTrackingData trackingData = bookingInfo
                        .getTrackingData();
                for (PaymentTransaction paymentTransaction : paymentData
                        .getPayment().getPaymentTransactions(
                                PaymentMethod.DATACASH)) {
                    LogWriter
                            .logInfoMessage("Sending authorization request to Datacash by client: "
                                    + clientAppName
                                    + " : for  token : "
                                    + token
                                    + " : with data-cash account : "
                                    + vTid);
                    setTransactionTrackingData(null, paymentTransaction,
                            trackingData);
                    ((DataCashPaymentTransaction) paymentTransaction)
                            .authPayment(vTid, authCode);
                    LogWriter
                            .logInfoMessage("Authorizing Payment is successful");
                }
                // For Gift Cards, Redeem request should be sent.
                redeemGiftCardPayment(paymentData.getPayment()
                        .getPaymentTransactions(PaymentMethod.GIFT_CARD),
                        clientAppName, token, vTid, trackingData);

                FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
                        token);
                boolean fraudScreeningStaus = fraudScreeningProcessor
                        .processFraudScreening();
                if (!fraudScreeningStaus) {
                    throw new DataCashServiceException(AUTH_FAILED_CODE,
                            DATACASHE_ERROR_KEY);
                }
            } catch (DataCashServiceException dcse) {
                // paymentData.setFailureCount();
                cancel(token, vTid);

                // Since for S2 application, we need to send transaction details
                // even
                // in case of auth failure, following lines are commented.
                // paymentData.getPayment().clearSensitiveData();
                // paymentData.getPayment().clearPaymentTransactions();

                FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
                        token);
                fraudScreeningProcessor.updateFailureCardData();

                handleDataCashServiceException(dcse);
            }
            LogWriter.logMethodEnd("authPayment");
        }
        return true;
    }

    /**
     * This method is responsible for redeeming the gift card payment.
     *
     * @param giftCardTransactionList
     *            the list of gift card payment transactions.
     * @param clientAppName
     *            the client application name.
     * @param token
     *            the token.
     * @param vTid
     *            the datacash vTid.
     * @param trackingData
     *            the tracking data.
     *
     * @throws DataCashServiceException
     *             if transaction fails.
     */
    private void redeemGiftCardPayment(
            List<PaymentTransaction> giftCardTransactionList,
            String clientAppName, String token, String vTid,
            TransactionTrackingData trackingData)
            throws DataCashServiceException {
        for (PaymentTransaction paymentTransaction : giftCardTransactionList) {
            LogWriter
                    .logInfoMessage("Sending redemption request to Datacash by client: "
                            + clientAppName
                            + " : for token: "
                            + token
                            + " : with data-cash account :" + vTid);
            setTransactionTrackingData(null, paymentTransaction, trackingData);
            ((GiftCardPaymentTransaction) paymentTransaction)
                    .redeemPayment(vTid);
            LogWriter.logInfoMessage("Redemption is successful");
        }
    }

    /**
     * This method is responsible for authentication of the card through
     * datacash.It returns true as XML RPC client requires value/object to be
     * returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param authCode
     *            The auth code. This is optional.
     *
     * @return the data-cash reference.
     *
     * @throws XmlRpcException
     *             the exception during transaction.
     */
    public String authSinglePayment(String token, String authCode, String vTid)
            throws XmlRpcException {
        LogWriter.logMethodStart("authSinglePayment", token, authCode, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        BookingComponent bookingComponent = bookingInfo.getBookingComponent();
        String clientAppName = bookingComponent.getClientApplication()
                .getClientApplicationName();

        String datacashReference = null;
        try {
            LogWriter
                    .logInfoMessage("Sending authorization request to Datacash by client: "
                            + clientAppName
                            + " : for token: "
                            + token
                            + " :with datacash account : " + vTid);
            DataCashPaymentTransaction datacashPaymentTransaction = (DataCashPaymentTransaction) paymentData
                    .getPayment()
                    .getPaymentTransactions(PaymentMethod.DATACASH).get(0);

            if (StringUtils.isBlank(bookingComponent
                    .getPaymentGatewayVirtualTerminalId())
                    && !ConfReader.getBooleanEntry(clientAppName + ".3DSecure",
                            false)) {
                setTransactionTrackingData(null, datacashPaymentTransaction,
                        bookingInfo.getTrackingData());
                datacashPaymentTransaction.authPayment(vTid, authCode);
            }
            datacashReference = datacashPaymentTransaction
                    .getDatacashReference();
            LogWriter.logInfoMessage("Authorizing Payment is successful");
        } catch (DataCashServiceException dcse) {
            paymentData.setFailureCount();
            paymentData.getPayment().clearSensitiveData();
            paymentData.getPayment().clearPaymentTransactions();
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("authSinglePayment");
        return datacashReference;
    }

    /**
     * This method is responsible for fulfill of a datacash transaction.It
     * returns true as XML RPC client requires value/object to be returned.
     * Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param bookingDetails
     *            the bookingDetails map which has the "inventory_booking_ref"
     *            as the key and value will be in String format which is exactly
     *            the Inventory Reference number.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean fulfil(String token, String vTid,
            Map<String, String> bookingDetails) throws XmlRpcException {
        LogWriter.logMethodStart("fulfil", token, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        boolean isDataCashTx = false;
        try {
            if (paymentData.getPayment()
                    .getPaymentTransactions(PaymentMethod.DATACASH).size() > 0) {
                isDataCashTx = true;
            }
            BigDecimal totalAmount = BigDecimal.ZERO;
            Currency currency = null;
            for (PaymentTransaction dataCashPaymentTransaction : paymentData
                    .getPayment()
                    .getPaymentTransactions(PaymentMethod.DATACASH)) {
                LogWriter
                        .logInfoMessage("Sending fulfill request to Datacash by client: "
                                + bookingInfo.getBookingComponent()
                                        .getClientApplication()
                                        .getClientApplicationName()
                                + " :for token : "
                                + token
                                + ": with datacash account : " + vTid);
                setTransactionTrackingData(bookingDetails,
                        dataCashPaymentTransaction,
                        bookingInfo.getTrackingData());
                ((DataCashPaymentTransaction) dataCashPaymentTransaction)
                        .fulfill(vTid);
                LogWriter.logInfoMessage("Successful fulfillment");
                totalAmount = totalAmount.add(dataCashPaymentTransaction
                        .getTransactionAmount().getAmount());
                if (currency == null) {
                    currency = dataCashPaymentTransaction
                            .getTransactionAmount().getCurrency();
                }
            }

            // Send data for fraud screening.
            String bookingReference = StringUtils.EMPTY;
            if (bookingDetails != null) {
                bookingReference = bookingDetails
                        .get(INVENTORY_BOOKING_REFERENCE);
            }
            if (isDataCashTx) {
                FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
                        token);
                fraudScreeningProcessor.updateBookingDetails(bookingReference,
                        new Money(totalAmount, currency));
            }

        } catch (DataCashServiceException dcse) {
            new TransactionStatusUtil().updateStatus(paymentData,
                    uuid.toString(), paymentData.getTransactionStatus(),
                    TransactionStatus.findByCodeTS("PAYMENT_FULFILL_FAILURE"));
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("fulfil");
        new TransactionStatusUtil().updateStatus(paymentData, uuid.toString(),
                paymentData.getTransactionStatus(),
                TransactionStatus.findByCodeTS("PAYMENT_FULFILL_SUCCESS"));
        return true;
    }

    /**
     * This method is responsible for fulfill of a datacash transaction.It
     * returns true as XML RPC client requires value/object to be returned.
     * Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean fulfill(String token, String vTid) throws XmlRpcException {
        return fulfil(token, vTid, null);
    }

    public Boolean updateBookingDetailsForDD(String token, String bookingReference)  {
        FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(
                 token);
        fraudScreeningProcessor.updateBookingDetails(bookingReference,
                 null);
        return true;
    }

    /**
     * This method is responsible for refund of a data-cash transaction. 1.It
     * checks for the negative amount in the data-cash transaction and then
     * refunds the amount. It returns true as XML RPC client requires
     * value/object to be returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param dataCashReference
     *            the dataCashReference.
     * @param bookingDetails
     *            the bookingDetails map which has the "inventory_booking_ref"
     *            as the key and value will be in String format which is exactly
     *            the Inventory Reference number.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean refund(String token, String vTid, String dataCashReference,
            Map<String, String> bookingDetails) throws XmlRpcException {
        LogWriter.logMethodStart("refund", token, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();

        try {
            for (PaymentTransaction dataCashHistoricTransaction : paymentData
                    .getPayment().getPaymentTransactions(
                            PaymentMethod.REFUND_CARD)) {
                if (dataCashReference.contentEquals(dataCashHistoricTransaction
                        .getEssentialTransactionData()
                        .getTransactionReference())) {
                    LogWriter
                            .logInfoMessage("Sending refund request to Datacash by client: "
                                    + bookingInfo.getBookingComponent()
                                            .getClientApplication()
                                            .getClientApplicationName()
                                    + ": for token : "
                                    + token
                                    + ": with datacash account : " + vTid);
                    setTransactionTrackingData(bookingDetails,
                            dataCashHistoricTransaction,
                            bookingInfo.getTrackingData());
                    ((DataCashHistoricTransaction) dataCashHistoricTransaction)
                            .historicRefund(vTid, dataCashReference);
                    LogWriter.logInfoMessage("Successful refund");
                }
            }
        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("refund");
        return true;
    }

    /**
     * This method is responsible for refund of a data-cash transaction. 1.It
     * checks for the negative amount in the data-cash transaction and then
     * refunds the amount. It returns true as XML RPC client requires
     * value/object to be returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param dataCashReference
     *            the dataCashReference
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean refund(String token, String vTid, String dataCashReference)
            throws XmlRpcException {
        return refund(token, vTid, dataCashReference, null);
    }

    /**
     * This method is responsible for refund of a data-cash transaction. 1.It
     * checks for the negative amount and also for the data-cash reference in
     * the data-cash transactions and then refunds the amount.It returns true as
     * XML RPC client requires value/object to be returned. Otherwise
     * XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param bookingDetails
     *            the bookingDetails map which has the "inventory_booking_ref"
     *            as the key and value will be in String format which is exactly
     *            the Inventory Reference number.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean refund(String token, String vTid,
            Map<String, String> bookingDetails) throws XmlRpcException {
        LogWriter.logMethodStart("refund ", token, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();

        try {
            for (PaymentTransaction dataCashHistoricTransaction : paymentData
                    .getPayment().getPaymentTransactions(
                            PaymentMethod.REFUND_CARD)) {
                LogWriter
                        .logInfoMessage("Sending refund request to Datacash by client: "
                                + bookingInfo.getBookingComponent()
                                        .getClientApplication()
                                        .getClientApplicationName()
                                + " :for token : "
                                + token
                                + " :with datacash account : " + vTid);
                setTransactionTrackingData(bookingDetails,
                        dataCashHistoricTransaction,
                        bookingInfo.getTrackingData());
                ((DataCashHistoricTransaction) dataCashHistoricTransaction)
                        .historicRefund(vTid);
                LogWriter.logInfoMessage("Successful refund");
            }
        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("refund ");
        return true;
    }

    /**
     * This method is responsible for refund of a data-cash transaction. 1.It
     * checks for the negative amount and also for the data-cash reference in
     * the data-cash transactions and then refunds the amount.It returns true as
     * XML RPC client requires value/object to be returned. Otherwise
     * XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean refund(String token, String vTid) throws XmlRpcException {
        Map<String, String> bookingDetails = null;
        return refund(token, vTid, bookingDetails);
    }

    /**
     * This method is responsible for auth refund of the card through
     * datacash.It returns true as XML RPC client requires value/object to be
     * returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param authCode
     *            The auth code.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean authRefund(String token, String authCode, String vTid)
            throws XmlRpcException {
        LogWriter.logMethodStart("authRefund", token, authCode, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        /*
         * String clientAppName =
         * bookingInfo.getBookingComponent().getClientApplication()
         * .getClientApplicationName();
         */
        try {
            for (PaymentTransaction dataCashPaymentTransaction : paymentData
                    .getPayment()
                    .getPaymentTransactions(PaymentMethod.DATACASH)) {
                if (((DataCashPaymentTransaction) dataCashPaymentTransaction)
                        .getTransactionAmount().getAmount().doubleValue() < 0.0
                        && ((DataCashPaymentTransaction) dataCashPaymentTransaction)
                                .getDatacashReference() == null) {
                    LogWriter
                            .logInfoMessage("Sending authorizing refund request to Datacash by client: "
                                    + bookingInfo.getBookingComponent()
                                            .getClientApplication()
                                            .getClientApplicationName()
                                    + " :for token : "
                                    + token
                                    + ": with datacash account : " + vTid);
                    setTransactionTrackingData(null,
                            dataCashPaymentTransaction,
                            bookingInfo.getTrackingData());
                    ((DataCashPaymentTransaction) dataCashPaymentTransaction)
                            .authRefund(vTid, authCode);
                    LogWriter.logInfoMessage("Authorizing Refund successful");
                }
            }
        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("authRefund");
        return true;
    }

    /**
     * This method is responsible for cancel of a datacash transaction.It
     * returns true as XML RPC client requires value/object to be returned.
     * Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param bookingDetails
     *            the bookingDetails map which has the "inventory_booking_ref"
     *            as the key and value will be in String format which is exactly
     *            the Inventory Reference number.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean cancel(String token, String vTid,
            Map<String, String> bookingDetails) throws XmlRpcException {
        LogWriter.logMethodStart("cancel", token, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        String clientAppName = bookingInfo.getBookingComponent()
                .getClientApplication().getClientApplicationName();
        TransactionTrackingData trackingData = bookingInfo.getTrackingData();
        try {
            for (PaymentTransaction dataCashPaymentTransaction : paymentData
                    .getPayment()
                    .getPaymentTransactions(PaymentMethod.DATACASH)) {
                LogWriter
                        .logInfoMessage("Sending cancellation request to Datacash by client: "
                                + clientAppName
                                + " : for token :  "
                                + token
                                + " :  with datacash account : " + vTid);
                setTransactionTrackingData(bookingDetails,
                        dataCashPaymentTransaction, trackingData);
                ((DataCashPaymentTransaction) dataCashPaymentTransaction)
                        .cancel(vTid);
                LogWriter.logInfoMessage("Cancellation successful");
            }
        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        try {
            for (PaymentTransaction giftCardPaymentTransaction : paymentData
                    .getPayment().getPaymentTransactions(
                            PaymentMethod.GIFT_CARD)) {
                LogWriter
                        .logInfoMessage("Sending ppt reversal request to Datacash by client: "
                                + clientAppName
                                + " : for token : "
                                + token
                                + " : with datacash account : " + vTid);
                setTransactionTrackingData(bookingDetails,
                        giftCardPaymentTransaction, trackingData);
                ((GiftCardPaymentTransaction) giftCardPaymentTransaction)
                        .reversePayment(vTid);
                LogWriter.logInfoMessage("Cancellation successful");
            }
        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("cancel");
        new TransactionStatusUtil().updateStatus(paymentData, uuid.toString(),
                paymentData.getTransactionStatus(),
                TransactionStatus.findByCodeTS("TRANSACTION_CANCEL"));
        return true;
    }

    /**
     * This method is responsible for cancel of a datacash transaction.It
     * returns true as XML RPC client requires value/object to be returned.
     * Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean cancel(String token, String vTid) throws XmlRpcException {
        return cancel(token, vTid, null);
    }

    /**
     * This method is responsible for doPayment operation on the card through
     * datacash.It returns true as XML RPC client requires value/object to be
     * returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param authCode
     *            The auth code.
     * @param bookingDetails
     *            the bookingDetails map which has the "inventory_booking_ref"
     *            as the key and value will be in String format which is exactly
     *            the Inventory Reference number.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean doPayment(String token, String authCode, String vTid,
            Map<String, String> bookingDetails) throws XmlRpcException {
        LogWriter.logMethodStart("doPayment", token, authCode, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();

        try {
            for (PaymentTransaction dataCashPaymentTransaction : paymentData
                    .getPayment()
                    .getPaymentTransactions(PaymentMethod.DATACASH)) {
                LogWriter
                        .logInfoMessage("Sending payment request to Datacash by client: "
                                + bookingInfo.getBookingComponent()
                                        .getClientApplication()
                                        .getClientApplicationName()
                                + " : for token : "
                                + token
                                + " : with datacash account : " + vTid);
                setTransactionTrackingData(bookingDetails,
                        dataCashPaymentTransaction,
                        bookingInfo.getTrackingData());
                ((DataCashPaymentTransaction) dataCashPaymentTransaction)
                        .doPayment(vTid, authCode);
                LogWriter.logInfoMessage("Successful Payment");
            }
        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("doPayment");
        return true;
    }

    /**
     * This method is responsible for doPayment operation on the card through
     * datacash.It returns true as XML RPC client requires value/object to be
     * returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param authCode
     *            The auth code.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean doPayment(String token, String authCode, String vTid)
            throws XmlRpcException {
        return doPayment(token, authCode, vTid, null);
    }

    /**
     * This method is responsible for doRefund operation on the card through
     * datacash.It returns true as XML RPC client requires value/object to be
     * returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param authCode
     *            The auth code.
     * @param bookingDetails
     *            the bookingDetails map which has the "inventory_booking_ref"
     *            as the key and value will be in String format which is exactly
     *            the Inventory Reference number.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean doRefund(String token, String authCode, String vTid,
            Map<String, String> bookingDetails) throws XmlRpcException {
        LogWriter.logMethodStart("doRefund", token, authCode, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        String clientAppName = bookingInfo.getBookingComponent()
                .getClientApplication().getClientApplicationName();
        try {
            for (PaymentTransaction dataCashPaymentTransaction : paymentData
                    .getPayment()
                    .getPaymentTransactions(PaymentMethod.DATACASH)) {
                // if transaction amount is negative and also the
                // datacashreference is null then only it
                // will refund

                if (((DataCashPaymentTransaction) dataCashPaymentTransaction)
                        .getTransactionAmount().getAmount().doubleValue() < 0.0) {
                    LogWriter
                            .logInfoMessage("Sending refund request to Datacash by client: "
                                    + clientAppName
                                    + " : for token : "
                                    + token
                                    + " : with datacash account : " + vTid);
                    setTransactionTrackingData(bookingDetails,
                            dataCashPaymentTransaction,
                            bookingInfo.getTrackingData());
                    ((DataCashPaymentTransaction) dataCashPaymentTransaction)
                            .fulfill(vTid);
                    LogWriter.logInfoMessage("Successful Refund fullfillment");
                }
            }
        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("doRefund");
        return true;
    }

    /**
     * This method is responsible for doRefund operation on the card through
     * datacash.It returns true as XML RPC client requires value/object to be
     * returned. Otherwise XmlRpcException is thrown.
     *
     * @param token
     *            The token of the transaction.
     * @param vTid
     *            the data-cash client account.
     * @param authCode
     *            The auth code.
     * @return Boolean returns always true.
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean doRefund(String token, String authCode, String vTid)
            throws XmlRpcException {
        return doRefund(token, authCode, vTid, null);
    }

    /**
     * This method gets the error message from the properties file for the given
     * exception and throws XmlRpcException.
     *
     * @param dcse
     *            the DataCashServiceException.
     *
     * @throws XmlRpcException
     *             with the error message retrieved.
     */
    private void handleDataCashServiceException(DataCashServiceException dcse)
            throws XmlRpcException {
        String message = PropertyResource.getProperty(dcse.getMessage(),
                MESSAGES_PROPERTY);
        LogWriter.logErrorMessage(message, dcse);
        throw new XmlRpcException(dcse.getCode(), message);
    }

    /**
     * This method reads the inventory booking reference number from
     * bookingDetails map and sets it into the paymentTransaction object.
     *
     * @param bookingDetails
     *            the bookingDetails map.
     * @param paymentTransaction
     *            the paymentTransaction object.
     * @param trackingData
     *            the transaction tracking data.
     *
     */
    private void setTransactionTrackingData(Map<String, String> bookingDetails,
            PaymentTransaction paymentTransaction,
            TransactionTrackingData trackingData) {
        if (!(bookingDetails == null || bookingDetails.isEmpty())) {
            if (bookingDetails.containsKey(INVENTORY_BOOKING_REFERENCE)) {
                trackingData.setInvBookingRef(bookingDetails
                        .get(INVENTORY_BOOKING_REFERENCE));
            }
        }
        paymentTransaction.setTrackingData(trackingData.toString());
    }

    /**
     * This method is responsible for preRegisteredRefund of dataCash
     * transaction. It checks for the negative amount in the dataCash
     * transaction and then refunds the amount. It returns true as XML RPC
     * client requires value/object to be returned,otherwise XmlRpcException is
     * thrown.
     *
     * @param token
     *            the transaction token.
     * @param vTid
     *            the data-cash client account.
     * @param dataCashReference
     *            the dataCashReference.
     * @param bookingDetails
     *            the bookingDetails map which has the "inventory_booking_ref"
     *            as the key and value will be in String format which is exactly
     *            the Inventory Reference number.
     *
     * @return Boolean returns always true.
     *
     * @throws XmlRpcException
     *             if there is an exception during transaction.
     */
    public Boolean preRegisteredRefund(String token, String vTid,
            String dataCashReference, Map<String, String> bookingDetails)
            throws XmlRpcException {
    	

    	
        LogWriter.logMethodStart("preRegisteredRefund", token, vTid);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        BookingComponent bookingComponent = bookingInfo.getBookingComponent();
        String clientApp = bookingComponent.getClientApplication().getClientApplicationName();
        if(clientApp.equalsIgnoreCase("TUITHSHOREX")){
            processB2CRefund(bookingComponent, uuid, vTid, paymentData, bookingInfo,dataCashReference );
        }

        else{
        try {
            for (PaymentTransaction dataCashHistoricTransaction : paymentData
                    .getPayment().getPaymentTransactions(
                            PaymentMethod.REFUND_CARD)) {
                if (dataCashReference.contentEquals(dataCashHistoricTransaction
                        .getEssentialTransactionData()
                        .getTransactionReference())) {
                    LogWriter
                            .logInfoMessage("Sending preRegisteredRefund request to Datacash by client:"
                                    + bookingInfo.getBookingComponent()
                                            .getClientApplication()
                                            .getClientApplicationName()
                                    + ": for token : "
                                    + token
                                    + ": with datacash account: " + vTid);
                    setTransactionTrackingData(bookingDetails,
                            dataCashHistoricTransaction,
                            bookingInfo.getTrackingData());
                    RefundDetails refundDetails = (RefundDetails) bookingComponent
                            .getRefundDetails().get(0);

                    if (isPaymentModeSet(refundDetails.getOriginalPaymentMode())) {
                        ((DataCashHistoricTransaction) dataCashHistoricTransaction)
                                .preRegisteredPayPalRefund(vTid,
                                        dataCashReference);

                    } else {

                        ((DataCashHistoricTransaction) dataCashHistoricTransaction)
                                .preRegisteredRefund(vTid, dataCashReference);

                    }
                    LogWriter.logInfoMessage("Successful preRegisteredRefund");
                }
            }
            new TransactionStatusUtil().updateStatus(paymentData, uuid
                    .toString(), paymentData.getTransactionStatus(),
                    TransactionStatus
                            .findByCodeTS("PRE_REGISTERED_REFUND_SUCCESS"));
        } catch (DataCashServiceException dcse) {
            updateRefundDetails(bookingInfo, dataCashReference);
            new TransactionStatusUtil().updateStatus(paymentData, uuid
                    .toString(), paymentData.getTransactionStatus(),
                    TransactionStatus
                            .findByCodeTS("PRE_REGISTERED_REFUND_FAILURE"));
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("preRegisteredRefund");
        }
        return true;
    }

      private static String getMerchantReference(BookingComponent bookingComponent)
       {
          String clientApp = bookingComponent.getClientApplication().getClientApplicationName();
          
          
          
          
          if(clientApp.equalsIgnoreCase("TUITHSHOREX") && ConfReader.getBooleanEntry("merchantid.enabled", false) ){
  			
  			if(!Objects.isNull(bookingComponent.getCruiseSummary()) && !Objects.isNull(bookingComponent.getCruiseSummary().getExcursionSummary())){
  				
  				String merchantId=bookingComponent.getCruiseSummary().getExcursionSummary().getMerchantID();
  				if(StringUtils.isEmpty(merchantId)){
  					merchantId=clientApp;
  				}
  				return merchantId
  						+ StringUtils.left(String.valueOf(UUID.randomUUID())
  								.replaceAll(HYPHEN, ""), MAX_REFERENCE_LENGTH
  								- clientApp.length());
  				
  				
  				
  			}else{
  				return clientApp
  				+ StringUtils.left(String.valueOf(UUID.randomUUID())
  						.replaceAll(HYPHEN, ""), MAX_REFERENCE_LENGTH
  						- clientApp.length());
  			}
  			
  			
  		}
          return clientApp
             + StringUtils.left(String.valueOf(UUID.randomUUID()).
     replaceAll(HYPHEN, ""),
     MAX_REFERENCE_LENGTH - clientApp.length());
          }



    private boolean isPaymentModeSet(String originalPaymentMode) {
        boolean status = false;
        if (StringUtils.isNotEmpty(originalPaymentMode)) {
            status = OriginalPaymentMode.valueFromCode(originalPaymentMode)
                    .getCode().equalsIgnoreCase(PayPalConstants.PAYPAL);
        }
        return status;
    }

    private void updateRefundDetails(BookingInfo bookingInfo,
            String dataCashReference) {
        for (RefundDetails refundDetail : bookingInfo.getBookingComponent()
                .getRefundDetails()) {
            if (StringUtils.equals(dataCashReference,
                    refundDetail.getDatacashReference())) {
                refundDetail.setRefundEnabled(false);
            }
        }
    }

  private synchronized void processB2CRefund(BookingComponent bookingComponent,UUID uuid ,String vTid ,PaymentData paymentData ,BookingInfo bookingInfo ,String dataCashReference)throws XmlRpcException{
      List<RefundDetails> refundDetailsshorex = new ArrayList<RefundDetails>();
      String datacashReferenceNumber = null;
      Money refundAmount = null;
      String captureMethod=null;
      String merchantReference = getMerchantReference(bookingComponent);
              refundDetailsshorex = bookingComponent.getRefundDetails();
              String refundReferenceId=null;
      for (RefundDetails refundItem :refundDetailsshorex){
          datacashReferenceNumber = refundItem.getDatacashReference();
          refundAmount = refundItem.getTransactionAmount();
          refundReferenceId=refundItem.getRefundReferenceId();
      }
     
      LogWriter.logInfoMessage("Refund details against token"+"-"+uuid +"are:"+"refundAmount="+refundAmount.getAmount().toString()+","+"datacashReferenceNumber"+datacashReferenceNumber+",vTid = "+vTid +", RefundReferenceId = "+refundReferenceId);
      DataCashHistoricTransaction dCashHistoricTransaction = new DataCashHistoricTransaction("", refundAmount, datacashReferenceNumber, merchantReference);
      dCashHistoricTransaction.setRefundReferenceId(refundReferenceId);
      if(!StringUtils.isEmpty(bookingComponent.getClientApplication().getCaptureMethod())){
    	  captureMethod=bookingComponent.getClientApplication().getCaptureMethod();
    	  dCashHistoricTransaction.setCaptureMethod(captureMethod);
      }
    
      try {
    	  boolean shorexIdEnabled=ConfReader.getBooleanEntry("shorexid.enable",false);
    	  String idfile=ConfReader.getConfEntry("shorex.file", "shorexid.ser");
    	  
    	 if(shorexIdEnabled && !StringUtils.isEmpty(refundReferenceId)){    		 
    		 
    		  boolean validationCurrentInstance=ConfReader.getBooleanEntry("shorexid.validation.enabled",false);
    		  if(validationCurrentInstance){
    		  File file=new File(idfile+"/shorexid.ser");   		   
    		  if(dCashHistoricTransaction.validateRequestWithTextFile(refundReferenceId, file)){
    	      dCashHistoricTransaction.preRegisteredRefund(vTid, datacashReferenceNumber);
    	      dCashHistoricTransaction.updateUniqueIdWithTextFile();
              Payment payment=new Payment();
              payment.addPaymentTransaction(dCashHistoricTransaction);
              paymentData.setPayment(payment);
              LogWriter.logInfoMessage("Successful preRegisteredRefund");
              new TransactionStatusUtil().updateStatus(paymentData, uuid
                      .toString(), paymentData.getTransactionStatus(),
                      TransactionStatus
                              .findByCodeTS("PRE_REGISTERED_REFUND_SUCCESS"));
    		  }
    		  
    		  
    		  
    		  
    		  }else{
    			  
    			  boolean result;    	
    			  result=verifyReferenceAtRemote(dCashHistoricTransaction);
    				if (result) {
						dCashHistoricTransaction.preRegisteredRefund(vTid,
								datacashReferenceNumber);
						boolean saveStatus = saveRefundToFile(dCashHistoricTransaction);
					}else{
						   LogWriter.logInfoMessage("Refund Unique ID validation Failed , error code is 03" );
						    throw new DataCashServiceException(03, "datacash.uid.error.03");
					}
						Payment payment = new Payment();
						payment.addPaymentTransaction(dCashHistoricTransaction);
						paymentData.setPayment(payment);
						LogWriter
								.logInfoMessage("Successful preRegisteredRefund");
						new TransactionStatusUtil()
								.updateStatus(
										paymentData,
										uuid.toString(),
										paymentData.getTransactionStatus(),
										TransactionStatus
												.findByCodeTS("PRE_REGISTERED_REFUND_SUCCESS"));
					
				}
    		  
    	  }else{
    		  dCashHistoricTransaction.preRegisteredRefund(vTid, datacashReferenceNumber);
              Payment payment=new Payment();
              payment.addPaymentTransaction(dCashHistoricTransaction);
              paymentData.setPayment(payment);
              LogWriter.logInfoMessage("Successful preRegisteredRefund");
              new TransactionStatusUtil().updateStatus(paymentData, uuid
                      .toString(), paymentData.getTransactionStatus(),
                      TransactionStatus
                              .findByCodeTS("PRE_REGISTERED_REFUND_SUCCESS"));
    		  
    	  }
     
      } catch (DataCashServiceException dcse) {
    	  Payment payment=new Payment();
          payment.addPaymentTransaction(dCashHistoricTransaction);
          paymentData.setPayment(payment);
          updateRefundDetails(bookingInfo, dataCashReference);
          new TransactionStatusUtil().updateStatus(paymentData, uuid
                  .toString(), paymentData.getTransactionStatus(),
                  TransactionStatus
                          .findByCodeTS("PRE_REGISTERED_REFUND_FAILURE"));
         handleDataCashServiceException(dcse);
      }
  }

  
  
  
  private boolean verifyReferenceAtRemote(DataCashHistoricTransaction dCashHistoricTransaction)throws XmlRpcException
  {
	  boolean result=false;
	  try{
		  this.xmlRpcClient=getXmlRpcClient();
	      Object[] params = new Object[] {dCashHistoricTransaction};
			result =  (boolean) xmlRpcClient.execute("datacash.verifyRefereneId", params);
			LogWriter.logInfoMessage("successfully executed verifyReferenceId method : "+result);
		
			
		  }catch(XmlRpcException ex){
   				LogWriter.logInfoMessage("verifyReferenceId method execution failed..: "+ex.getMessage());

			  throw new XmlRpcException("verifyReferenceId method execution failed..",ex);
		  }
	  
	  return result;
  }
  
  
    /**
 * @param dCashHistoricTransaction
     * @return 
 */
private boolean saveRefundToFile(DataCashHistoricTransaction dCashHistoricTransaction)throws XmlRpcException{

	try{
	 this.xmlRpcClient=getXmlRpcClient();
     Object[] params = new Object[] {dCashHistoricTransaction};
		boolean result =  (boolean) xmlRpcClient.execute("datacash.saveRefundStatusToFile", params);
		LogWriter.logInfoMessage(" saveRefundToFile successfully executed verifyReferenceId method : "+result);
	}catch(XmlRpcException ex){
		LogWriter.logInfoMessage("saveRefundStatusToFile method execution failed..: "+ex.getMessage());
		throw new XmlRpcException("saveRefundStatusToFile method execution failed",ex);
	}
	return true;
	
}

	/* The following method is for Hybris... */
    public Map<String, String> ddSetUpMandate(String token,String currency,
            String countryLocale, String countryCode, String clientApplication,
            String datacashVTid, Map directDebitDetails)
            throws XmlRpcException, DataCashServiceException, SOAPException,
            IOException, ExperianServiceException {

       /* LogWriter
                .logInfoMessage("Parameters for ddSetupMandate are:  corrency =  "
                        + currency
                        + ", countryLocale = "
                        + countryLocale
                        + ", countryCode = "
                        + countryCode
                        + ", clientApplication = "
                        + clientApplication
                        + ", datacashVTid = "
                        + datacashVTid
                        + " ,"
                        + directDebitDetails);
        */
        /***Implementation for maskig personal details in directDebitDetails objecgts ***/
        
	    int startInd = 0;	    
	    String accountNumber = "";
	    String method = (String)directDebitDetails.get("method");
	    String address_refinement_required = (String)directDebitDetails.get("address_refinement_required");
	    String merchantreference = (String)directDebitDetails.get("merchantreference");	    
	    String sortCode = (String)directDebitDetails.get("sortCode");
	    String accnValue = (String)directDebitDetails.get("accountNumber");
	    try {
	    	accountNumber = maskString(accnValue, startInd, accnValue.length() - 4, '*');
	    }catch (Exception e) {
			// TODO: handle exception
			LogWriter.logErrorMessage("Error in Masking ", e);
		}
	    
	     /*************************************************/
        LogWriter
                .logInfoMessage("Parameters for ddSetupMandate are:  corrency =  "
                        + currency
                        + ", countryLocale = "
                        + countryLocale
                        + ", countryCode = "
                        + countryCode
                        + ", clientApplication = "
                        + clientApplication
                        + ", datacashVTid = "
                        + datacashVTid
                        + " , method = "
                        + method
                        + " , address_refinement_required = "
                        + address_refinement_required
                        + " , merchantreference = "
                        + merchantreference
                        + " , accountNumber = "
                        + accountNumber
                        + " , sortCode = "
                        + sortCode
                        );
        Map<String, String> ddsetupResponse = null;
        Map<String, String> response = null;
        Map<String, String> verifyDetails = null;
        if (token != null)
        {
            UUID uuid = UUID.fromString(token);
            PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                     uuid);
            BookingInfo bookingInfo = paymentData.getBookingInfo();
            BookingComponent bookingComponent = bookingInfo.getBookingComponent();

            if (bookingComponent.getDdVariant() != null && StringUtils
                     .equalsIgnoreCase(bookingComponent.getDdVariant().getCode(), DDVariant.V2.getCode()))
            {
                if (!frudscreeningRequestToAccertifyForDD(token, directDebitDetails))
                {
                    verifyDetails.put("Status", "10");
                    verifyDetails.put("frudCheckStatus", "false");
                    response = verifyDetails;
                    return response;
                }
            }
        }
        method = (String) directDebitDetails.get("method");
        String verificationStatus = null;

        if ("verifyAndSetup".equals(method)) {
            directDebitDetails.put("method", "setup");
            verifyDetails = verifyAccountDetails(countryCode,
                    directDebitDetails);
            verificationStatus = (String) verifyDetails
                    .get("verificationStatus");
            if (verificationStatus.equals("true")) {

                ddsetupResponse = hybrisDDSetup(clientApplication,
                        datacashVTid, directDebitDetails);
                verifyDetails.put("datacashReferenceNumber",
                        ddsetupResponse.get("datacashReferenceNumber"));
                verifyDetails.put("method", ddsetupResponse.get("method"));
                verifyDetails.put("stage", ddsetupResponse.get("stage"));
                verifyDetails.put("merchantReferenceNumber",
                        ddsetupResponse.get("merchantReferenceNumber"));
                verifyDetails.put("mode", ddsetupResponse.get("mode"));
                verifyDetails
                        .put("startDate", ddsetupResponse.get("startDate"));
                verifyDetails.put("reason", ddsetupResponse.get("reason"));
                verifyDetails.put("status", ddsetupResponse.get("status"));

            }

            response = verifyDetails;

        } else {
            response = hybrisDDSetup(clientApplication, datacashVTid,
                    directDebitDetails);

        }

        LogWriter.logInfoMessage("response from cps to hybris/atcore is : \n"
                + response.toString());

        return response;

    }
    /**
     * This method is responsible for masking
     */
    private String maskString(String strText, int start, int end, char maskChar) 
	        throws Exception{
	        
	        if(strText == null || strText.equals(""))
	            return "";
	        
	        if(start < 0)
	            start = 0;
	        
	        if( end > strText.length() )
	            end = strText.length();
	            
	        if(start > end)
	            throw new Exception("End index cannot be greater than start index");
	        
	        int maskLength = end - start;
	        
	        if(maskLength == 0)
	            return strText;
	        
	        StringBuilder sbMaskString = new StringBuilder(maskLength);
	        
	        for(int i = 0; i < maskLength; i++){
	            sbMaskString.append(maskChar);
	        }
	        
	        return strText.substring(0, start) 
	                + sbMaskString.toString() 
	                + strText.substring(start + maskLength);
	    }

    /**
     * This method is responsible for contacting Accertify
     */
    public boolean frudscreeningRequestToAccertifyForDD(String token, Map directDebitDetails) {

        LogWriter.logInfoMessage("Sending fraudscreening request...: " + token);

        FraudScreeningProcessorImpl fraudScreeningProcessor = new FraudScreeningProcessorImpl(
                 token);

        return fraudScreeningProcessor.processFraudScreeningForZeroDepositeDD(directDebitDetails);
    }

    public Map<String, String> hybrisDDSetup(String clientApplication,
            String datacashVTid, Map directDebitDetails) throws XmlRpcException {
        Map<String, String> setupResponse = new HashMap<String, String>();

        try {

            LogWriter
                    .logInfoMessage("Sending direct debit setup request to Datacash by client: "
                            + clientApplication
                            + "  "
                            + ": with datacash account : " + datacashVTid);

            DataCashService dataCashService = new DataCashService(datacashVTid);
            XMLDocument response = dataCashService.getDDSetupXmlResponse(
                    datacashVTid, directDebitDetails);

            setupResponse.put("method",
                    response.get(DataCashServiceConstants.DD_METHOD_RS));
            setupResponse.put("stage",
                    response.get(DataCashServiceConstants.DD_STAGE_RS));
            setupResponse.put("startDate",
                    response.get(DataCashServiceConstants.DD_START_DATE_RS));
            // setupResponse.put("information",response.get(DataCashServiceConstants.RESPONSE_INFORMATION));
            setupResponse.put("merchantReferenceNumber", response
                    .get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
            setupResponse.put("mode",
                    response.get(DataCashServiceConstants.MODE_RS));
            setupResponse.put("status",
                    response.get(DataCashServiceConstants.STATUS_RS));

            String status = response.get(DataCashServiceConstants.STATUS_RS);

            String info = response
                    .get(DataCashServiceConstants.RESPONSE_INFORMATION);

            if (StringUtils.equalsIgnoreCase(status, "70")
                    && !StringUtils.containsIgnoreCase(info, "reference")) {
                setupResponse.put("reason",
                        "Invalid Bank Account Number or Sort Code");

            } else {
                setupResponse.put("reason",
                        response.get(DataCashServiceConstants.REASON_RS));
            }
            setupResponse.put("datacashReferenceNumber", response
                    .get(DataCashServiceConstants.DATACASH_REFERENCE_RS));

            setupResponse.put("accountVerificationStatus", "NA");
            setupResponse.put("dataAccessKey", "NA");
            setupResponse.put("personalDetailsScore", "NA");
            setupResponse.put("addressScore", "NA");
            setupResponse.put("accountSetupDateMatch", "NA");
            setupResponse.put("accountSetupDateScore", "NA");
            setupResponse.put("bankAddress", "NA");

            LogWriter.logInfoMessage("Successful setup");

        } catch (DataCashServiceException dcse) {

            handleDataCashServiceException(dcse);
        }
        LogWriter.logInfoMessage("sending response back to hybris"
                + setupResponse);
        LogWriter.logMethodEnd("ddSetUpMandate");
        return setupResponse;

    }

    // This method is atcore implementation...
    public Map<String, String> ddSetUpMandate(String currency,
            String countryLocale, String countryCode, String clientApplication,
            String datacashVTid, Map<String, String> applicationData,
            Object[] directDebitDetails) throws XmlRpcException,
            DataCashServiceException, SOAPException, IOException,
            ExperianServiceException {

        LogWriter.logInfoMessage("parameter from hybris is : currency="
                + currency + ", countryLocale = " + countryLocale
                + ", countryCode = " + countryCode + ", clientApplication = "
                + clientApplication + ", datacashVTid = " + datacashVTid
                + ", directDebitDetails = " + directDebitDetails);

        Map<String, String> ddsetupResponse = null;
        Map<String, String> response = null;

        String method = (String) directDebitDetails[4];
        Map<String, String> verifyAccountDetails = new HashMap<String, String>();
        Map<String, String> verifyDetails = null;
        String verificationStatus = null;

        if ("verifyAndSetup".equals(method)) {

            verifyAccountDetails
                    .put("sortCode", (String) directDebitDetails[0]);
            verifyAccountDetails.put("accountNumber",
                    (String) directDebitDetails[1]);
            verifyAccountDetails.put("firstName",
                    (String) directDebitDetails[2]);
            verifyAccountDetails
                    .put("lastName", (String) directDebitDetails[3]);
            verifyAccountDetails.put("houseNumber",
                    (String) directDebitDetails[6]);
            verifyAccountDetails.put("street", (String) directDebitDetails[7]);
            verifyAccountDetails
                    .put("postCode", (String) directDebitDetails[8]);
            verifyAccountDetails.put("dob", (String) directDebitDetails[9]);
            verifyAccountDetails.put("flat", (String) directDebitDetails[10]);
            verifyAccountDetails.put("houseName",
                    (String) directDebitDetails[11]);
            Object token = directDebitDetails[12];
            if (token != null)
                verifyAccountDetails.put("token",
                        (String) directDebitDetails[12]);
            Object bookingReference = directDebitDetails[13];
            if (bookingReference != null)
                verifyAccountDetails.put("bookingReference",
                        (String) directDebitDetails[13]);
            Object departureDate = directDebitDetails[14];
            if (departureDate != null)
                verifyAccountDetails.put("bookingDepartureDate",
                        (String) directDebitDetails[14]);

            directDebitDetails[4] = "setup";
            verifyDetails = verifyAccountDetails(countryCode,
                    verifyAccountDetails);
            verificationStatus = (String) verifyDetails
                    .get("verificationStatus");
            if (verificationStatus.equals("true")) {

                ddsetupResponse = ddsetup(datacashVTid, clientApplication,
                        applicationData, directDebitDetails);
                verifyDetails.put("datacashReferenceNumber",
                        ddsetupResponse.get("datacashReferenceNumber"));
                verifyDetails.put("method", ddsetupResponse.get("method"));
                verifyDetails.put("stage", ddsetupResponse.get("stage"));
                verifyDetails.put("merchantReferenceNumber",
                        ddsetupResponse.get("merchantReferenceNumber"));
                verifyDetails.put("mode", ddsetupResponse.get("mode"));
                verifyDetails
                        .put("startDate", ddsetupResponse.get("startDate"));
                verifyDetails.put("reason", ddsetupResponse.get("reason"));
                verifyDetails.put("status", ddsetupResponse.get("status"));

            }
            response = verifyDetails;

        } else {
            response = ddsetup(datacashVTid, clientApplication,
                    applicationData, directDebitDetails);

        }

        return response;

    }

    public Map<String, String> ddsetup(String datacashVTid,
            String clientApplication, Map<String, String> applicationData,
            Object[] directDebitDetails) throws XmlRpcException {
        LogWriter.logMethodStart("ddSetUpMandate", clientApplication,
                datacashVTid);

        Map<String, String> setupResponse = new HashMap<String, String>();

        try {

            LogWriter
                    .logInfoMessage("Sending direct debit setup request to Datacash by client: "
                            + clientApplication
                            + "  "
                            + ": with datacash account : " + datacashVTid);

            DataCashService dataCashService = new DataCashService(datacashVTid);
            XMLDocument response = dataCashService.getDDSetupXmlResponse(
                    datacashVTid, applicationData, directDebitDetails);

            setupResponse.put("method",
                    response.get(DataCashServiceConstants.DD_METHOD_RS));
            setupResponse.put("stage",
                    response.get(DataCashServiceConstants.DD_STAGE_RS));
            setupResponse.put("startDate",
                    response.get(DataCashServiceConstants.DD_START_DATE_RS));
            // setupResponse.put("information",response.get(DataCashServiceConstants.RESPONSE_INFORMATION));
            setupResponse.put("merchantReferenceNumber", response
                    .get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
            setupResponse.put("mode",
                    response.get(DataCashServiceConstants.MODE_RS));
            setupResponse.put("status",
                    response.get(DataCashServiceConstants.STATUS_RS));

            String status = response.get(DataCashServiceConstants.STATUS_RS);
            String info = response
                    .get(DataCashServiceConstants.RESPONSE_INFORMATION);

            if (StringUtils.equalsIgnoreCase(status, "70")
                    && !StringUtils.containsIgnoreCase(info, "reference")) {
                setupResponse.put("reason",
                        "Invalid Bank Account Number or Sort Code");

            } else {
                setupResponse.put("reason",
                        response.get(DataCashServiceConstants.REASON_RS));
            }

            setupResponse.put("datacashReferenceNumber", response
                    .get(DataCashServiceConstants.DATACASH_REFERENCE_RS));

            setupResponse.put("accountVerificationStatus", "NA");
            setupResponse.put("dataAccessKey", "NA");
            setupResponse.put("personalDetailsScore", "NA");
            setupResponse.put("addressScore", "NA");
            setupResponse.put("accountSetupDateMatch", "NA");
            setupResponse.put("accountSetupDateScore", "NA");
            setupResponse.put("bankAddress", "NA");

            LogWriter.logInfoMessage("Successful setup");

        } catch (DataCashServiceException dcse) {

            handleDataCashServiceException(dcse);
        }
        LogWriter.logInfoMessage("sending response back to Atcore"
                + setupResponse);
        LogWriter.logMethodEnd("ddSetUpMandate");
        return setupResponse;

    }

    public Map<String, String> submitddDrawdownBatch(String currency,
            String countryLocale, String clientApplication,
            String datacashVTid, Map<String, String> applicationData,
            Object[] drawDownDetails) throws XmlRpcException {

        LogWriter.logMethodStart("submitddDrawdownBatch", clientApplication,
                datacashVTid);
        Map<String, String> drawDownBatchStatus = new HashMap<String, String>();
        try {
            LogWriter
                    .logInfoMessage("Sending direct debit submitddDrawdownBatch request to Datacash by client: "
                            + clientApplication
                            + "  "
                            + ": with datacash account : " + datacashVTid);
            DataCashService dataCashService = new DataCashService(datacashVTid);
            XMLDocument response = dataCashService.getDrawDownXmlResponse(
                    datacashVTid, applicationData, drawDownDetails);
            drawDownBatchStatus.put("datacashReferenceNumber", response
                    .get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
            // drawDownBatchStatus.put("information",response.get(DataCashServiceConstants.DESCRIPTION));
            drawDownBatchStatus.put("merchantReferenceNumber", response
                    .get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
            drawDownBatchStatus.put("mode",
                    response.get(DataCashServiceConstants.MODE_RS));
            drawDownBatchStatus.put("reason",
                    response.get(DataCashServiceConstants.REASON_RS));
            drawDownBatchStatus.put("status",
                    response.get(DataCashServiceConstants.STATUS_RS));
            drawDownBatchStatus.put("time",
                    response.get(DataCashServiceConstants.TIME_RS));
            LogWriter.logInfoMessage("sending respone back to atcore"
                    + drawDownBatchStatus);
            LogWriter
                    .logInfoMessage("Successful submitddDrawdownBatch execute");

        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }
        LogWriter.logMethodEnd("submitddDrawdownBatch");
        return drawDownBatchStatus;

    }

    public Object[] getddDrawdownBatchStatus(String currency,
            String countryLocale, String clientApplication,
            String datacashVTid, Map<String, String> applicationData,
            Map<String, String> getDrawDownStatus) throws XmlRpcException {

        LogWriter.logMethodStart("getddDrawdownBatchStatus", clientApplication,
                datacashVTid);
        Object[] drawdownStatus = null;

        try {

            LogWriter
                    .logInfoMessage("Sending direct debit getddDrawdownBatchStatus request to Datacash by client: "
                            + clientApplication
                            + "  "
                            + ": with datacash account : " + datacashVTid);

            DataCashService dataCashService = new DataCashService(datacashVTid);

            drawdownStatus = dataCashService.getddDrawdownBatchStatus(
                    applicationData, getDrawDownStatus);

            LogWriter
                    .logInfoMessage("Successful getddDrawdownBatchStatus execute");
        } catch (DataCashServiceException dcse) {

            handleDataCashServiceException(dcse);
        }

        LogWriter.logDebugMessage(drawdownStatus + "has been sent to atcom");
        LogWriter.logMethodEnd("getddDrawdownBatchStatus");

        return drawdownStatus;

    }

    public Map<String, String> submitCancelMandateBatch(String currency,
            String countryLocale, String clientApplication,
            String datacashVTid, Map<String, String> applicationData,
            Object[] cancelMandateDetails) throws XmlRpcException {

        LogWriter.logMethodStart("submitCancelMandateBatch", clientApplication,
                datacashVTid);

        Map<String, String> cancelBatchStatus = new HashMap<String, String>();

        try {
            LogWriter
                    .logInfoMessage("Sending direct debit submitCancelMandateBatch request to Datacash by client: "
                            + clientApplication
                            + "  "
                            + ": with datacash account : " + datacashVTid);

            DataCashService dataCashService = new DataCashService(datacashVTid);
            XMLDocument response = dataCashService.getCancelBatchXmlResponse(
                    applicationData, cancelMandateDetails);

            cancelBatchStatus.put("datacashReferenceNumber", response
                    .get(DataCashServiceConstants.DATACASH_REFERENCE_RS));
            cancelBatchStatus.put("information",
                    response.get(DataCashServiceConstants.DESCRIPTION));
            cancelBatchStatus.put("merchantReferenceNumber", response
                    .get(DataCashServiceConstants.MERCHANT_REFERENCE_RS));
            cancelBatchStatus.put("mode",
                    response.get(DataCashServiceConstants.MODE_RS));
            cancelBatchStatus.put("reason",
                    response.get(DataCashServiceConstants.REASON_RS));
            cancelBatchStatus.put("status",
                    response.get(DataCashServiceConstants.STATUS_RS));
            cancelBatchStatus.put("time",
                    response.get(DataCashServiceConstants.TIME_RS));

            LogWriter.logInfoMessage("Sending response back to atcore"
                    + cancelBatchStatus);
            LogWriter
                    .logInfoMessage("Successful submitCancelMandateBatch execute");

        } catch (DataCashServiceException dcse) {
            handleDataCashServiceException(dcse);
        }

        LogWriter.logMethodEnd("submitCancelMandateBatch");

        return cancelBatchStatus;
    }

    public Object[] getCancelMandateBatchStatus(String currency,
            String countryLocale, String clientApplication,
            String datacashVTid, Map<String, String> applicationData,
            Map<String, String> getBatchDetails) throws XmlRpcException {

        LogWriter.logMethodStart("getCancelMandateBatchStatus",
                clientApplication, datacashVTid);

        Object[] responseObject = null;

        try {
            LogWriter
                    .logInfoMessage("Sending direct debit getCancelMandateBatchStatus request to Datacash by client: "
                            + clientApplication
                            + "  "
                            + ": with datacash account : " + datacashVTid);

            DataCashService dataCashService = new DataCashService(datacashVTid);
            responseObject = dataCashService.getCancelMandateBatchStatus(
                    applicationData, getBatchDetails);
            LogWriter
                    .logInfoMessage("Returning Response Object Arrays back to client ");

        } catch (DataCashServiceException dcse) {

            handleDataCashServiceException(dcse);
        }

        LogWriter.logMethodEnd("getCancelMandateBatchStatus");
        return responseObject;

    }

    public Map<String, String> validateAccountDetails(String countryCode,
            Map<String, String> validateDetails) throws ClassNotFoundException,
            IOException {

        if (!isExperianFlagEnabled()) {
            LogWriter
                    .logInfoMessage("Experian has been switched off , please enable experian services and try:");
            throw new RuntimeException("Experian is not enabled");
        }

        LogWriter.logMethodStart("validateAccountDetails", countryCode,
                validateDetails.get("sortCode"),
                validateDetails.get("accountNumber"));
        LogWriter.logInfoMessage("Parameters for validating the account are:  "
                + countryCode + validateDetails);

        com.tui.uk.log.Logger logger = LogWriter
                .getLogger("com.tui.uk.experianClient");
        logger.logInfoMessage("Parameters for validating the account are:  "
                + countryCode + validateDetails);

        /*
         * ExperianLogger experianLogger=new
         * ExperianLogger("com.tui.uk.experianClient");
         * ExperianLogger.logInfoMessage("validateAccountDetails");
         * ExperianLogger.logInfoMessage(
         * "Parameters for validateAccountDetails service is  :   countryCode = "
         * +countryCode+" , "+validateDetails);
         * ExperianLogger.logErrorMessage("This is error message");
         */

        ExperianServiceImplementation experianServiceImplementation = new ExperianServiceImplementation();
        Map<String, String> response = null;
        try {

            response = experianServiceImplementation.validateAccountDetails(
                    countryCode, validateDetails);

        }
        /*
         * catch(ExperianServiceException ex){ throw new
         * ExperianServiceException("Exception Occured at experian site");
         *
         * }
         */
        catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);

        }
        logger.logInfoMessage("Response from cps to hybris/atocm :  "
                + response);
        logger.logInfoMessage("validateAccountDetails");

        LogWriter.logInfoMessage("Response from cps to hybris/atocm :  "
                + response);
        LogWriter.logInfoMessage("validateAccountDetails");
        return response;
    }

    public Map<String, String> verifyAccountDetails(String countryCode,
            Map<String, String> verifyAccountDetails) throws SOAPException,
			IOException, ExperianServiceException {

		if (!isExperianFlagEnabled()) {
			LogWriter.logInfoMessage("Experian has been switched off , please enable experian services and try:");
			throw new RuntimeException("Experian is not enabled");
		}

		com.tui.uk.log.Logger logger = LogWriter.getLogger("com.tui.uk.experianClient");
		LogWriter.logMethodStart("verifyAccountDetails", verifyAccountDetails);

		// implementation for hiding customer personal details in log

		int startInd = 0;
		String accountNumber = "";
		String method = (String) verifyAccountDetails.get("method");
		String address_refinement_required = (String) verifyAccountDetails.get("address_refinement_required");
		String merchantreference = (String) verifyAccountDetails.get("merchantreference");
		String sortCode = (String) verifyAccountDetails.get("sortCode");
		String accnValue = (String) verifyAccountDetails.get("accountNumber");
		try {
			accountNumber = maskString(accnValue, startInd, accnValue.length() - 4, '*');
		} catch (Exception e) {
			// TODO: handle exception
			LogWriter.logErrorMessage("Error in Masking ", e);
		}

		logger.logInfoMessage(
				"Parameters for verifying the account are:  countryCode = " + countryCode + " , method = " + method
						+ " , address_refinement_required = " + address_refinement_required + " , merchantreference = "
						+ merchantreference + " , accountNumber = " + accountNumber + " , sortCode = " + sortCode);

		logger.logInfoMessage("verifyAccountDetails" + ", countryCode = " + countryCode + " , method = " + method
				+ " , address_refinement_required = " + address_refinement_required + " , merchantreference = "
				+ merchantreference + " , accountNumber = " + accountNumber + " , sortCode = " + sortCode);

		/* ================================================ */

		// TODO: need refactoring as part of not displaying personal details in log
		/*
		 * String address = "", customerName = ""; customerName +=
		 * verifyAccountDetails.get("firstName"); customerName += " " +
		 * verifyAccountDetails.get("lastName");
		 */
		// TODO: need refactoring till here

		ExperianServiceImplementation experianServiceImplementation = new ExperianServiceImplementation();
		Map<String, String> results = experianServiceImplementation.verifyAccountDetails(countryCode,
				verifyAccountDetails);

		// String sortCode = verifyAccountDetails.get("sortCode");
		// String accountNumber = verifyAccountDetails.get("accountNumber");
		String bookingReference = verifyAccountDetails.get("bookingReference");
		String bookingDepartureDate = verifyAccountDetails.get("bookingDepartureDate");

		// TODO: need refactoring as part of not displaying personal details in log

		/*
		 * String houseNumber = verifyAccountDetails.get("houseNumber"); String
		 * houseName = verifyAccountDetails.get("houseName"); String street =
		 * verifyAccountDetails.get("street"); if (!StringUtils.isEmpty(houseNumber))
		 * address += houseNumber + " , "; if (!StringUtils.isEmpty(houseName)) address
		 * += houseName + " , "; if (!StringUtils.isEmpty(street)) address += street;
		 */

		// TODO: need refactoring till here

		// results.put("cutomerAddress",address);

		// TODO: need refactoring as part of not displaying personal details in log
		/*
		 * logger.logInfoMessage("customerName = " + customerName +
		 * " , account number =   " + accountNumber + " , sortCode = " + sortCode + "" +
		 * " , bookingReference = " + bookingReference + " , bookingDepartureDate = " +
		 * bookingDepartureDate + "customerAddress =" + address);
		 */

		// TODO: need refactoring till here

		// implementation for hiding customer personal details in log
		logger.logInfoMessage("account number =   " + accountNumber + " , sortCode = " + sortCode + ""
				+ " , bookingReference = " + bookingReference + " , bookingDepartureDate = " + bookingDepartureDate);

		logger.logInfoMessage("verifyAccountDetails response cps to hybris/atcom is :  " + results.toString());
		LogWriter.logMethodEnd("verifyAccountDetails");
		return results;

	}

    /**
     * This method is responsible for setting up the paypal transaction that
     * returns PayPalSetExpressCheckoutResponse on successful transactions.
     * Otherwise XmlRpcException is thrown.
     *
     * @param token
     * @param PayPalSetExpressCheckoutResponse
     * @return
     */
    public PayPalSetExpressCheckoutResponse setUpPayPal(
            PayPalComponent paypalComponent, UUID uuid,
            BookingComponent bookingComponent) {

        LogWriter.logDebugMessage("vTid for paypal is :"
                + paypalComponent.getvTid());
        DataCashService dataCashService = new DataCashService(
                paypalComponent.getvTid());
        dataCashService.setCpsToken(uuid);
        return dataCashService.getPaypalSECResponse(
                paypalComponent.getPayPalExpressCheckout(), bookingComponent);

    }

    /**
     * This method is responsible for setting up the paypal transaction that
     * returns PayPalSetExpressCheckoutResponse on successful transactions.
     * Otherwise XmlRpcException is thrown.
     *
     * @param token
     * @param PayPalSetExpressCheckoutResponse
     * @return
     */
    public PayPalSetExpressCheckoutResponse setUpPayPal(String token) {

        LogWriter.logInfoMessage("set Up PayPal for Token.......: " + token);
        PayPalSetExpressCheckoutResponse payPalSetExpressCheckoutResponse = null;
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        BookingComponent bookingComponent = bookingInfo.getBookingComponent();

        payPalSetExpressCheckoutResponse = setUpPayPal(
                bookingComponent.getPaypalComponent(), uuid, bookingComponent);
        bookingComponent.getPaypalComponent().setPayPalExpressCheckoutResponse(
                payPalSetExpressCheckoutResponse);

        LogWriter
                .logInfoMessage("returning  payPalSetExpressCheckoutResponse object is :   -"
                        + payPalSetExpressCheckoutResponse);
        LogWriter.logMethodStart("set Up PayPal Method end.......: ");
        return payPalSetExpressCheckoutResponse;
    }

    /**
     * This method is responsible for verifing the customer details that returns
     * PayPalCustomerVerificationDetails on successful transactions. Otherwise
     * XmlRpcException is thrown.
     *
     * @param token
     * @param PayPalCustomerVerificationDetails
     * @return
     */
    public PayPalCustomerVerificationDetails verifyPaypalCustomerDetails(
            String token)throws XmlRpcException {

        LogWriter.logMethodStart("verifyPaypalCustomerDetails method start: "
                + token);
        LogWriter.logInfoMessage("verifyPaypalCustomerDetails method start: "
                + token);

        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        if(paymentData==null){
        	LogWriter.logInfoMessage("Payment data is not available with respect to the token "+token);
        	throw new XmlRpcException("payment data not available for the token "+token);
        }
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        BookingComponent bookingComponent = bookingInfo.getBookingComponent();
        PayPalComponent paypalComponent = bookingComponent.getPaypalComponent();
        PayPalSetExpressCheckoutResponse paypalExpressCheckoutResponse = paypalComponent
                .getPayPalExpressCheckoutResponse();
        DataCashService dataCashService = new DataCashService(
                paypalComponent.getvTid());
        dataCashService.setCpsToken(uuid);
        PayPalCustomerVerificationDetails paypalCustomerVerificationDetails = dataCashService
                .getPaypalVerificationDetails(token);

        LogWriter
                .logInfoMessage("returning  paypalCustomerVerificationDetails object is :   -"
                        + paypalCustomerVerificationDetails);

        LogWriter.logMethodEnd("verifyPaypalCustomerDetails  method end:   -");

        return paypalCustomerVerificationDetails;
    }

    /**
     * This method is responsible for contacting Accertify
     */
    public boolean frudscreeningRequestToAccertify(String token,
            AuthorizePayPalTransaction authorizePayPalTransaction,
            PayPalCaptureRequest payPalCaptureRequest) {

        LogWriter.logInfoMessage("Sending fraudscreening request...: " + token);

        FraudScreeningProcessorImpl fraudScreeningProcessor = new FraudScreeningProcessorImpl(
                token);

        return fraudScreeningProcessor
                .processFraudScreeningForPayPalTransaction(
                        authorizePayPalTransaction, payPalCaptureRequest);
    }

    /**
     * This method is responsible for authorizing a PayPal transaction that
     * returns PayPalAuthorizeResponse on successful transactions. Otherwise
     * XmlRpcException is thrown.
     *
     * @param authorizePayPalTransaction
     * @param payPalExpressCheckoutResponse
     * @return
     */

    private PayPalAuthorizeResponse doAuthorizePayPalTransaction(
            AuthorizePayPalTransaction authorizePayPalTransaction,
            String merchantRef) {
        PayPalAuthorizeResponse payPalAuthorizeResponse = null;

        try {

            payPalAuthorizeResponse = (PayPalAuthorizeResponse) dataCashService
                    .doAuthorizePayPalTransaction(authorizePayPalTransaction,
                            merchantRef);
        } catch (Exception ie) {

        }
        return payPalAuthorizeResponse;
    }

    /**
     * This method is responsible for capturing a PayPal transaction that
     * returns PayPalCaptureResponse on successful transactions. Otherwise
     * XmlRpcException is thrown.
     *
     * @param PayPalCaptureRequest
     * @param PayPalCaptureResponse
     * @return
     */
    public PayPalCaptureResponse doCapturePaypalTransaction(String token,
            PayPalCaptureRequest paypalCaptureRequest) {

        LogWriter
                .logMethodStart("doCapturePaypalTransaction method started.......: "
                        + token);
        LogWriter.logInfoMessage("doCapturePaypalTransaction method started...: " + token);
        UUID uuid = UUID.fromString(token);
        PaymentData paymentData = PaymentStore.getInstance().getPaymentData(
                uuid);
        BookingInfo bookingInfo = paymentData.getBookingInfo();
        BookingComponent bookingComponent = bookingInfo.getBookingComponent();
        PayPalCaptureResponse paypalCaptureResponse = new PayPalCaptureResponse();
        DataCashService dataCashService = new DataCashService(bookingComponent
                .getPaypalComponent().getvTid());
        if (frudscreeningRequestToAccertify(token, null, paypalCaptureRequest)) {
            dataCashService.setCpsToken(uuid);
            paypalCaptureResponse = dataCashService.doCapturePaypalTransaction(
                    token, paypalCaptureRequest);

        } else {
            paypalCaptureResponse.setAcknowledgement(PayPalConstants.FRAUD_ACK);
            paypalCaptureResponse.setStatus(PayPalConstants.FRAUD_STATUS);
            paypalCaptureResponse.setReason(PayPalConstants.FRAUD_MSG);
        }

        LogWriter
                .logInfoMessage("returning  doCapturePaypalTransaction object is :   -"
                        + paypalCaptureResponse);
        LogWriter.logMethodEnd("doCapturePaypalTransaction method end :   -");
        return paypalCaptureResponse;
    }

    /**
     * This is a wrapper service to authorize a paypal transaction
     *
     * @param token
     * @param authorizePayPalTransaction
     * @return
     * @throws XmlRpcException
     */
    public PayPalAuthorizeResponse doAuthorizePayPalTransaction(String token,
            AuthorizePayPalTransaction authorizePayPalTransaction)
            throws XmlRpcException {

        LogWriter
                .logMethodStart("doAuthorizePayPalTransaction method started  - : "
                        + token);
        LogWriter.logInfoMessage("doAuthorizePayPalTransaction method started  - : "
                        + token);
        PayPalAuthorizeResponse payPalAuthorizeResponse = null;

        UUID uuid = UUID.fromString(token);

        BookingComponent bookingComponent = PaymentStore.getInstance()
                .getPaymentData(uuid).getBookingInfo().getBookingComponent();

        LogWriter.logMethodStart(
                "do_Authorize_PayPal_Transaction for token : ", token
                        + "AND BRAND :"
                        + bookingComponent.getClientApplication()
                                .getClientApplicationName());

        PayPalSetExpressCheckoutResponse payPalSetExpressCheckoutResponse = (PayPalSetExpressCheckoutResponse) bookingComponent
                .getPaypalComponent().getPayPalExpressCheckoutResponse();

        dataCashService = new DataCashService(bookingComponent
                .getPaypalComponent().getvTid());


        if (authorizePayPalTransaction.getDatacashReferance().equals(
                payPalSetExpressCheckoutResponse.getDatacashReference())) {
            dataCashService.setCpsToken(uuid);
            payPalAuthorizeResponse = doAuthorizePayPalTransaction(
                    authorizePayPalTransaction,
                    payPalSetExpressCheckoutResponse.getMerchantReference());

            // one receiving successful response call fraudscreening
            if (payPalAuthorizeResponse != null
                    && payPalAuthorizeResponse.getStatus().equals(
                            PayPalConstants.DATACASH_SUCCESS)) {

                if (frudscreeningRequestToAccertify(token,
                        authorizePayPalTransaction, null)) {

                    return payPalAuthorizeResponse;
                } else {

                    payPalAuthorizeResponse = new PayPalAuthorizeResponse();
                    payPalAuthorizeResponse
                            .setStatus(PayPalConstants.FRAUD_STATUS);
                    payPalAuthorizeResponse
                            .setReason(PayPalConstants.FRAUD_MSG);
                    payPalAuthorizeResponse
                            .setAcknowledgement(PayPalConstants.FRAUD_ACK);
                    return payPalAuthorizeResponse;
                }
            }
        }
        LogWriter
                .logInfoMessage("returning  payPalAuthorizeResponse object is :   -"
                        + payPalAuthorizeResponse);
        LogWriter.logMethodEnd("doAuthorizePayPalTransaction  :   -");
        return payPalAuthorizeResponse;
    }

    /**
     * This method is responsible for reversing an order or authorization of
     * PayPal Transaction. Otherwise XmlRpcException is thrown.
     *
     * @param authorizePayPalTransaction
     * @param payPalExpressCheckoutResponse
     * @return
     */
    public PayPalFundReleaseResponse cancelPayPalTransaction(String token,
            PayPalCustomer payPalCustomer) {

        LogWriter.logInfoMessage("Verifying the token: " + token);

        PayPalFundReleaseResponse payPalFundReleaseResponse = null;

        UUID uuid = UUID.fromString(token);

        BookingComponent bookingComponent = PaymentStore.getInstance()
                .getPaymentData(uuid).getBookingInfo().getBookingComponent();

        LogWriter.logMethodStart("cancel PayPal Transaction for token : ",
                token
                        + "AND BRAND :"
                        + bookingComponent.getClientApplication()
                                .getClientApplicationName());

        LogWriter
                .logInfoMessage("Contacting datacash service to cancel PayPal Transaction...: "
                        + token);

        DataCashService dataCashService = new DataCashService(bookingComponent
                .getPaypalComponent().getvTid());
        dataCashService.setCpsToken(uuid);
        payPalFundReleaseResponse = dataCashService
                .cancelPayPalTransaction(payPalCustomer);

        if (!StringUtils.equals(payPalFundReleaseResponse.getStatus(),PayPalConstants.DATACASH_SUCCESS)) {

            LogWriter.logInfoMessage("failed to release PayPal funds...: "
                    + token);

        } else {

            try {
                LogWriter.logInfoMessage("Purging the token :" + uuid);
                PaymentStore.getInstance().destroyPayment(uuid);

            } catch (Exception ie) {

            }

        }
        LogWriter
                .logInfoMessage("returning  payPalFundReleaseResponse object is :   -"
                        + payPalFundReleaseResponse);
        LogWriter.logMethodEnd("cancelPayPalTransaction method end  :   -");
        return payPalFundReleaseResponse;
    }

    /**
     * This method is responsible for Querying the status of PayPal Transaction.
     * Otherwise XmlRpcException is thrown.
     *
     * @param authorizePayPalTransaction
     * @param payPalExpressCheckoutResponse
     * @return
     */
    public PayPalQueryTransactionResponse queryPayPalTransaction(String token,
            PayPalCustomer payPalCustomer) {

        LogWriter.logInfoMessage("Verifying the token: " + token);

        PayPalQueryTransactionResponse payPalQueryTransactionResponse = null;

        UUID uuid = UUID.fromString(token);

        BookingComponent bookingComponent = PaymentStore.getInstance()
                .getPaymentData(uuid).getBookingInfo().getBookingComponent();


        DataCashService dataCashService = new DataCashService(bookingComponent
                .getPaypalComponent().getvTid());
        dataCashService.setCpsToken(uuid);
        payPalQueryTransactionResponse = dataCashService
                .queryPayPalTransaction(payPalCustomer);

        LogWriter
                .logInfoMessage("returning  payPalQueryTransactionResponse object is :   -"
                        + payPalQueryTransactionResponse);
        LogWriter.logMethodEnd("queryPayPalTransaction method end  :   -");

        return payPalQueryTransactionResponse;
    }

    /**
     * This method checks if Experian services has been switched on or not
     *
     * @return boolean
     */

    private boolean isExperianFlagEnabled() {

        boolean experianFlag = ConfReader.getBooleanEntry(
                "experian.services.enabled", true);

        return experianFlag;

    }

    public Map<String,String> revokeDDSetup(String datacashVTid, Map<String,String> revokeDDSetupDetails){
        DataCashService dataCashService = new DataCashService(datacashVTid);
        Map<String,String> responseMap = new HashMap<String, String>();
        int maxnoOfAttempts = ConfReader.getIntEntry("directdebit.revoke.max.retry", 3);
        int noOfAttempts = 1;
        try{
            do{
                responseMap= dataCashService.revokeDDSetup(datacashVTid,revokeDDSetupDetails);
                noOfAttempts++;
            }
            while(responseMap.isEmpty() && noOfAttempts<=maxnoOfAttempts );
            if(responseMap.isEmpty()){
                throw new DataCashServiceException("Maxium nnumber of retries reached for datacash revoke request");
            }

        }
        catch(DataCashServiceException e){
            LogWriter.logErrorMessage(e.getMessage());

        }


        return responseMap;

    }
    
    
    
    
    private XmlRpcClient getXmlRpcClient()
    {
       final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
       config.setEnabledForExtensions(true);
       try
       {
          config.setServerURL(new URL(ConfReader.getConfEntry("shorex.instaceUrl",null)));
       }
       catch (MalformedURLException mue)
       {
          LogWriter.logErrorMessage(mue.getMessage(), mue);
          throw new RuntimeException(mue.getMessage());
       }

       xmlRpcClient = new XmlRpcClient();


/*       final XmlRpcTransportFactory transportFactory = new XmlRpcTransportFactory()
       {
           public XmlRpcTransport getTransport()
           {
               return new XmlRpcLogging(xmlRpcClient);
           }
       };





    //   xmlRpcClient.setTransportFactory(new XmlRpcCommonsTransportFactory(xmlRpcClient));
       xmlRpcClient.setTransportFactory(transportFactory);*/



       xmlRpcClient.setConfig(config);

       httpClient = new HttpClient();

       final XmlRpcCommonsTransportFactory factory = new XmlRpcCommonsTransportFactory(xmlRpcClient);
       factory.setHttpClient(httpClient);

  //     xmlRpcClient.setTransportFactory(factory);
       xmlRpcClient.setTransportFactory(factory);


       return xmlRpcClient;
    }

    
    
    
    public boolean verifyRefereneId(DataCashHistoricTransaction dhTransaction)throws DataCashServiceException{
    	LogWriter.logInfoMessage("verifyReferenfceId method execution started successfully...............");
    	String idfile=ConfReader.getConfEntry("shorex.file", "shorexid.ser");
    	File file=new File(idfile+"/shorexid.ser");
    	return dhTransaction.validateRequestWithTextFile(dhTransaction.getRefundReferenceId(), file);

    }

    public boolean saveRefundStatusToFile(DataCashHistoricTransaction dhTransaction)throws DataCashServiceException{
    	LogWriter.logInfoMessage("verifyReferenfceId method execution started successfully...............");

    	dhTransaction.updateUniqueIdWithTextFile();
    	return true;

    }
    
    
    }
