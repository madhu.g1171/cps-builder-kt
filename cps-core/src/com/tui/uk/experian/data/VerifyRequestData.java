package com.tui.uk.experian.data;
/**
 *
 * This the address data which holds refined address used for
 * Experian verify service address for Direct Debit
 * @author anup.g
 *
 */
public class VerifyRequestData {

    private AddressData address;

    private String sortCode;

    private String accountNumber;

    private String firstName;

    private String lastName;

    private String dob;



    public AddressData getAddress() {

        if(this.address == null)
        {
            address = new AddressData();
        }
        return address;
    }
    public void setAddress(AddressData address) {
        this.address = address;
    }
    public String getSortCode() {
        return sortCode;
    }
    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getDob() {
        return dob;
    }
    public void setDob(String dob) {
        this.dob = dob;
    }





}
