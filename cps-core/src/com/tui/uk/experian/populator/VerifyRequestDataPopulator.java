package com.tui.uk.experian.populator;

import java.util.Map;

import com.tui.uk.experian.data.VerifyRequestData;

public class VerifyRequestDataPopulator {


    public void populate(Map<String, String> verifyDetails, VerifyRequestData target)

    {
        target.getAddress().setHouseNumber(verifyDetails.get("houseNumber"));
        target.getAddress().setHouseName(verifyDetails.get("houseName"));
        target.getAddress().setFlatNumber(verifyDetails.get("flat"));
        target.getAddress().setPostCode(verifyDetails.get("postCode"));
        target.getAddress().setStreet(verifyDetails.get("street"));
        target.setFirstName(verifyDetails.get("firstName"));
        target.setLastName(verifyDetails.get("lastName"));
        target.setSortCode(verifyDetails.get("sortCode"));
        target.setAccountNumber(verifyDetails.get("accountNumber"));
        target.setDob(verifyDetails.get("dob"));

    }
}
