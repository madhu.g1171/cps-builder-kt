package com.tui.uk.experian.service;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tui.uk.experian.data.AddressData;
import com.tui.uk.log.LogWriter;

import org.apache.commons.lang.StringUtils;
/**
 * This class is used for String operations of Address data
 * @author anup.g
 *  AddressRefinementService
 */
public final class AddressRefinementService {
    /**
     * This method process Flat number / House number /House Name from address line 1
     * @param item
     */
        public  final void processAddressData(final AddressData address){
              LogWriter.logInfoMessage("----------------- Refine the houseNumber, Flat or houseName from AddressData -------------------------");
              final String houseNumberFromRequest = address.getHouseNumber();

              String houseName = "";
              String street = "";
              LogWriter.logInfoMessage("Input ----------> "+houseNumberFromRequest);
              String flatNumber = processFlatNumber(houseNumberFromRequest);
              LogWriter.logInfoMessage("Flat number ----> "+flatNumber);
              String houseNumber = "";
              //if word 'flat' is not mentioned by the user in the address line 1
              if(StringUtils.isEmpty(flatNumber)){
                  houseNumber = processHouseNumber(houseNumberFromRequest);
                  LogWriter.logInfoMessage("House number ---> "+ houseNumber);
                  if(StringUtils.isEmpty(houseNumber)){
                      houseName = processHouseName(houseNumberFromRequest, houseNumber);
                      LogWriter.logInfoMessage("houseName -----> "+houseName);

                  }
                  else{
                      street = processHouseName(houseNumberFromRequest, houseNumber);
                      LogWriter.logInfoMessage("street -----> "+street);
                  }


              }
              else{
                  LogWriter.logInfoMessage("House number ---> "+ houseNumber);
                  houseName = processHouseName(houseNumberFromRequest, flatNumber);
                  LogWriter.logInfoMessage("houseName -----> "+houseName);

              }

              //set the derived data back to AddressData only if the respective fields were not populated by the calling system
              if(StringUtils.isBlank(address.getFlatNumber())){
                  LogWriter.logInfoMessage("Originally flat number not supplied , so update with derrived flat number if applicable ");
                  address.setFlatNumber(flatNumber);
              }
              address.setHouseNumber(houseNumber);
             if((StringUtils.isNotBlank(houseNumber)) && (StringUtils.isNotBlank(street))){
                 address.setStreet(street);
             }


              if(StringUtils.isBlank(address.getHouseName())){
                  LogWriter.logInfoMessage("Originally houseName not supplied , so update with derrived houseName if applicable ");
                  address.setHouseName(houseName);
              }


}

    /**
     * This method process the flat number for the given address line 1
     * @param input
     * @return
     */
        @SuppressWarnings("resource")
        private  String processFlatNumber(String input){
            String flatNumber = "";

            String flatKey = "";

            if(input.contains("Flat")  || input.contains("flat") || input.contains("FLAT"))
            {
                flatKey = "flat";
            }


            if(StringUtils.isNotBlank(flatKey)){
                flatNumber = processHouseNumber(input);
            }
            return flatNumber;
        }

        /**
         * This method process the house number for the given address line 1
         * @param input
         * @return
         */
        @SuppressWarnings("resource")
        private  String processHouseNumber(String input){
            String houseNumber = "";
            String houseNumberWithSpecialChar = digitAndCharacterValidation(input);
            if(StringUtils.isNotBlank(houseNumberWithSpecialChar)){
                houseNumber = houseNumberWithSpecialChar;
            }
            else if(digitValidation(input)){
                houseNumber =  String.valueOf(new Scanner(input).useDelimiter("[^\\d]+").nextInt());
                 }

            return houseNumber;
        }


    /**
     * Process house name from the given address 1 , with the help of derrived house number
     * @param input
     * @param houseNumber
     * @return
     */
        private  String processHouseName(String input, String houseNumber){
            String houseName ="";
            houseName = input.replace(houseNumber, "");
            houseName = houseName.replace("Flat", "");
            houseName = houseName.replace("flat", "");
            houseName = houseName.replace("FLAT", "");
            //replace all special characters with empty chars
            //TODO these regex patterns should come from configuration or constant
            houseName = houseName.replaceAll("[\\,\\-,\\/]", "");
            return houseName.trim();
        }

    /**
     *  This method checks if the given input has digit
     * @param input
     * @return
     */
        private  boolean digitValidation(String input){
            Pattern digit = Pattern.compile("[0-9]");
            Matcher hasDigit = digit.matcher(input);
            return hasDigit.find();
        }

        /**
         * This method checks for house number with pattern 55/B,55
         * @param input
         * @return
         */
        private  String digitAndCharacterValidation(String input){
            //TODO these regex patterns should come from configuration or constant
            String houseNumber = houseNumberWithAlpha(input);
            if(StringUtils.isBlank(houseNumber)){
                   houseNumberWithSlash(input);
            }
              return houseNumber;
        }

        /**
         * This method checks for house number with "/" pattern
         * @param input
         * @return
         */
        private  String houseNumberWithSlash(String input){
            String houseNumber = "";
            Pattern digitAndSlash = Pattern.compile("[0-9]+/[a-zA-Z]");
            Matcher hasdigitAndSlash = digitAndSlash.matcher(input);

            if(hasdigitAndSlash.find()){
                houseNumber = hasdigitAndSlash.group();
            }
            return houseNumber;
        }

        /**
         * This method checks for prefixed with alpha or postfix with alpha pattern
         * @param input
         * @return
         */

        private  String houseNumberWithAlpha(String input){

             String houseNumber = "";

             Pattern houseNumberPrefixedWithAlpha = Pattern.compile("[a-zA-Z]+[0-9]+");
             Matcher hashouseNumberPrefixedWithAlpha = houseNumberPrefixedWithAlpha.matcher(input);

             Pattern houseNumberPostfixedWithAlpha = Pattern.compile("[0-9]+[a-zA-Z]+");
             Matcher hashouseNumberPostfixedWithAlpha = houseNumberPostfixedWithAlpha.matcher(input);

             Pattern houseNumberWithSlash = Pattern.compile("[0-9]+/[a-zA-Z]+");
             Matcher hashouseNumberWithSlash = houseNumberWithSlash.matcher(input);

             if(hashouseNumberPrefixedWithAlpha.find()){
                 houseNumber = hashouseNumberPrefixedWithAlpha.group();
             }

             if(hashouseNumberPostfixedWithAlpha.find()){
                 houseNumber = hashouseNumberPostfixedWithAlpha.group();
             }
             if(hashouseNumberWithSlash.find()){
                 houseNumber = hashouseNumberWithSlash.group();
             }
             return houseNumber;
         }


}
