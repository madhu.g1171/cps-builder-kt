/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: NonDiskCacheImpl.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-16 09:04:52 $
 *
 * $Author : bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.cache;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import com.tui.uk.cacheeventlistener.CPSCacheEventListener;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyConstants;

/**
 * NonDiskCache implementation of the Cache interface. This cache is configured so that it never
 * stores or caches information on disk. This is an in-memory cache only.
 *
 * @author : bibin.j@sonata-software.com
 */
public final class NonDiskCacheImpl implements Cache
{
   /** An instance of ehCache. */
   private net.sf.ehcache.Cache ehCache;

   /** Whether elements can overflow to disk when the in-memory cache has reached the set limit. */
   private boolean overflowToDisk;

   /** The max elements in cache. */
   private static final int MAX_ELEMENTS = 5000;

   /**
    * Cache that never expires.This is an in-memory cache only.
    *
    * @param name representing the name of the cache.
    * @param eternal representing whether the elements in the cache are eternal or not.
    */
   protected NonDiskCacheImpl(String name, boolean eternal)
   {
      init(name, eternal, 0, 0);
   }

   /**
    * Cache with expire time.This is an in-memory cache only.
    *
    * @param name representing the name of the cache.
    * @param timeToLiveSeconds expire time in seconds.
    */
   protected NonDiskCacheImpl(String name, long timeToLiveSeconds)
   {
      init(name, false, timeToLiveSeconds, timeToLiveSeconds);
   }

   /**
    * Gets the cache name.
    *
    * @return String the name of the cache
    */
   public String getCacheName()
   {
      return ehCache.getName();
   }

   /**
    * Gets an element from the cache, based on the key passed.
    *
    * @param key object representing the key
    * @return Element from the cache.
    */
   public Object get(Object key)
   {
      Element element = ehCache.get(key);
      if (element != null)
      {
         return element.getObjectValue();
      }
      return null;
   }

   /**
    * Put an element in the cache.
    *
    * @param key object representing the key
    * @param value element to be added to the cache.
    * @see com.tui.uk.cache.Cache#put(java.lang.Object, java.lang.Object)
    */
   public void put(Object key, Object value)
   {
      ehCache.put(new Element(key, value));
   }

   /**
    * Remove the element corresponding to the passed in key from the cache.
    *
    * @param key representing the key.
    */
   public void remove(Object key)
   {
      ehCache.remove(key);
   }

   /**
    * Remove all elements from the cache.
    */
   public void dump()
   {
      ehCache.removeAll();
   }

   /**
    * Gets the count of elements in the cache.
    *
    * @return the element count.
    */
   public int getElementCount()
   {
      return ehCache.getSize();
   }

   /**
    * Initialize the instance with default values.
    *
    * @param name of the cache.
    * @param eternal boolean representing whether the cache to be eternal or not.
    * @param timeToLiveSeconds time the objects are stored in the cache.
    * @param timeToIdleSeconds the time to idle for an element before it expires.
    */
   private void init(String name, boolean eternal, long timeToLiveSeconds,
           long timeToIdleSeconds)
   {
      int maxElementsInMemory =  ConfReader.getIntEntry(name + ".cache.maxElementsInMemory",
                MAX_ELEMENTS);
      ehCache = new net.sf.ehcache.Cache(name, maxElementsInMemory, overflowToDisk, eternal,
                timeToLiveSeconds, timeToIdleSeconds);
      registerWithCacheManager();
   }

   /**
    * Registers the cache with the cache manager.
    *
    */
   private void registerWithCacheManager()
   {
      CacheManager singletoneMgr = CacheManager.getInstance();
      singletoneMgr.addCache(ehCache);       
      Boolean autoCancelEnabled = ConfReader.getBooleanEntry(
      PropertyConstants.AUTOCANCELFLAG, false);
      if (autoCancelEnabled)
      {
       ehCache.getCacheEventNotificationService().registerListener(
       new CPSCacheEventListener());
      }
     
     
   }
}
