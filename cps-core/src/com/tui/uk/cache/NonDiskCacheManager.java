/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: NonDiskCacheManager.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-16 09:04:52 $
 *
 * $Author : bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.cache;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Implementation of CacheManger for NonDiskCache. This registers the cache which never stores
 * or caches information on disk.
 *
 * @author : bibin.j@sonata-software.com
 */
public final class NonDiskCacheManager implements CacheManager
{
   /** The single instance of this class. */
   private static NonDiskCacheManager cacheMgr = new NonDiskCacheManager();

   /**
    * A list of all caches that have been created by the Cache Manager.Caches will be added to this
    * list when they are registered. The <code>CacheFactory</code> currently registers all caches at
    * creation. In theory, client code will deregister caches when they are no longer used.
    * In practice, we guard against this by using a WeakHashMap. Any registered cache that is not
    * strongly reachable from client code will be cleared by the garbage collection system.
    */
   private Map<String, Cache> cacheMap =
                    Collections.synchronizedMap(new WeakHashMap<String, Cache>());

   /** private constructor to prevent instantiation. */
   private NonDiskCacheManager()
   {
   }

   /**
    * Gets the singleton.
    *
    * @return the singleton instance of the EhCacheManager.
    */
   public static CacheManager getInstance()
   {
      return cacheMgr;
   }

   /**
    * Register the cache with the CacheManager.
    *
    * @param cache the instance to be registered with the CacheManager.
    */
   public void register(Cache cache)
   {
      cacheMap.put(cache.getCacheName(), cache);
   }

   /**
    * Remove the cache that is registered with the CacheManager.
    *
    * @param cacheName the name of the cache.
    */
   public void removeCache(String cacheName)
   {
      net.sf.ehcache.CacheManager singletoneMgr =   net.sf.ehcache.CacheManager.getInstance();
      singletoneMgr.removeCache(cacheName);
      cacheMap.remove(cacheName);
   }

   /**
    * Remove all the caches registered with the CacheManager.
    */
   public void removeAll()
   {
      net.sf.ehcache.CacheManager singletoneMgr = net.sf.ehcache.CacheManager.getInstance();
      singletoneMgr.removalAll();
      cacheMap.clear();
   }

   /**
    * Get all the caches registered with the CacheManager.
    *
    * @return the collection of cache.
    */
   public Collection<Cache> getAll()
   {
      return cacheMap.values();
   }
}
