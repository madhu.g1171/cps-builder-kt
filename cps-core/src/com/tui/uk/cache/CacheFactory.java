/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CacheFactory.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-16 09:04:52 $
 *
 * $Author : bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.cache;

import com.tui.uk.config.ConfReader;

/**
 * Factory class for creating caches.
 *
 * @author : bibin.j@sonata-software.com
 */
public final class CacheFactory
{
   /** Default expire time is 30 minutes or 1800 seconds. */
   private static final long DEFAULT_EXPIRE_TIME = 1800;

   /** private constructor to prevent instantiation. */
   private CacheFactory()
   {
      // empty constructor
   }

   /**
    * Returns the instance of CacheManager.
    *
    * @return CacheManager the NonDiskCacheManager
    */
   public static CacheManager getCacheManager()
   {
      return NonDiskCacheManager.getInstance();
   }

   /**
    * Creates a named cache with the quick expire. The cache is registered with the CacheManager.
    *
    * @param cacheName the String to identify this cache.
    * @return Cache the cache.
    */
   public static Cache createCache(String cacheName)
   {
      return createCache(cacheName, ConfReader.getLongEntry(
                   cacheName + ".cache.ExpireTime", DEFAULT_EXPIRE_TIME));
   }

   /**
    * Create a named cache with expire time.
    *
    * @param cacheName a string to identify this cache
    * @param expiryTime expire time in seconds.
    * @return Cache the cache.
    */
   public static Cache createCache(String cacheName, long expiryTime)
   {
      Cache cache = new NonDiskCacheImpl(cacheName, expiryTime);
      NonDiskCacheManager.getInstance().register(cache);
      return cache;
   }

   /**
    * Create a named eternal cache.(cache that never expires).
    *
    * @param cacheName a string to identify this cache
    * @return Cache the cache.
    */
   public static Cache createEternalCache(String cacheName)
   {
      Cache cache = new NonDiskCacheImpl(cacheName, true);
      NonDiskCacheManager.getInstance().register(cache);
      return cache;
   }
}
