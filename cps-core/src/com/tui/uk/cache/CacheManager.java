/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CacheManager.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-16 09:04:52 $
 *
 * $Author : bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.cache;

import java.util.Collection;

/**
 * Maintain a list of caches, for central management.
 *
 * @author : bibin.j@sonata-software.com
 */

public interface CacheManager
{
   /**
    * Register cache with cache manager.
    *
    * @param cache the Cache.
    */
   void register(Cache cache);

   /**
    * Removes the cache from the list. This will enable the cache to be
    * finally garbage collected unless there are references to it.
    *
    * @param cacheName the name of the cache.
    */
   void removeCache(String cacheName);

   /**
    * Removes all caches.
    */
   void removeAll();

   /**
    * Get all the caches registered with the CacheManager.
    *
    * @return the collection of cache.
    */
   Collection<Cache> getAll();

}
