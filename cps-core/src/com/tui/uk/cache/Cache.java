/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Cache.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-16 09:04:52 $
 *
 * $Author : bibin.j@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.cache;

/**
 * General Interface to Caches.
 *
 * Obtain instances of Cache from  <code>CacheFactory</code>.Cached Items can be set to expire after
 * a certain period of time.Cache provides a mechanism for storing objects in memory with automatic
 * expiry.
 * The <code>CacheManager</code> provides methods to view all caches running on the VM.
 *
 * @author : bibin.j@sonata-software.com
 */
public interface Cache
{
   /**
    * The name of the cache.
    *
    * @return the name of the cache.
    */
   String getCacheName();

   /**
    * Return the object at this key.
    * Return null if:
    *  a)no such object exists
    *  b)the object has expired
    *  c)the key is null
    *
    * @param key the key to retrieve the element from the cache.
    * @return Object the object stored against the key.
    */
   Object get(Object key);

   /**
    * Put this object at this key. Does nothing if the key is null.
    *
    * @param key the key against which the element is stored in cache.
    * @param value the object that is in the cache against the key.
    *
    */
   void put(Object key, Object value);

   /**
    * Remove the object at this key. Does nothing if the key is null.
    *
    * @param key the key to retrieve the element from the cache.
    */
   void remove(Object key);

}
