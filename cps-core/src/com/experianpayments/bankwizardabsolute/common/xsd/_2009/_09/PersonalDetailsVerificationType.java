
package com.experianpayments.bankwizardabsolute.common.xsd._2009._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonalDetailsVerificationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PersonalDetailsVerificationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Match"/>
 *     &lt;enumeration value="No Match"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PersonalDetailsVerificationType")
@XmlEnum
public enum PersonalDetailsVerificationType {

    @XmlEnumValue("Match")
    MATCH("Match"),
    @XmlEnumValue("No Match")
    NO_MATCH("No Match"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    PersonalDetailsVerificationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PersonalDetailsVerificationType fromValue(String v) {
        for (PersonalDetailsVerificationType c: PersonalDetailsVerificationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
