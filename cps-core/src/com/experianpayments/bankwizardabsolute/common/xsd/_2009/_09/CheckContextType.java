
package com.experianpayments.bankwizardabsolute.common.xsd._2009._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CheckContextType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CheckContextType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Direct Debit"/>
 *     &lt;enumeration value="Direct Credit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CheckContextType")
@XmlEnum
public enum CheckContextType {

    @XmlEnumValue("Direct Debit")
    DIRECT_DEBIT("Direct Debit"),
    @XmlEnumValue("Direct Credit")
    DIRECT_CREDIT("Direct Credit");
    private final String value;

    CheckContextType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckContextType fromValue(String v) {
        for (CheckContextType c: CheckContextType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
