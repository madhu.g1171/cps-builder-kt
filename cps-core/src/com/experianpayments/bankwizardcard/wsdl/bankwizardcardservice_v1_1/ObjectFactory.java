
package com.experianpayments.bankwizardcard.wsdl.bankwizardcardservice_v1_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.experianpayments.bankwizardcard.xsd._2012._05.MatchCardHolderRequest;
import com.experianpayments.bankwizardcard.xsd._2012._05.MatchCardHolderResponse;
import com.experianpayments.bankwizardcard.xsd._2012._05.VerifyAvsCvvVelocityRequest;
import com.experianpayments.bankwizardcard.xsd._2012._05.VerifyAvsCvvVelocityResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizardcard.wsdl.bankwizardcardservice_v1_1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MatchCardHolderRequest_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", "MatchCardHolderRequest");
    private final static QName _VerifyAvsCvvVelocityResponse_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", "VerifyAvsCvvVelocityResponse");
    private final static QName _VerifyAvsCvvVelocityRequest_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", "VerifyAvsCvvVelocityRequest");
    private final static QName _MatchCardHolderResponse_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", "MatchCardHolderResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizardcard.wsdl.bankwizardcardservice_v1_1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MatchCardHolderRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", name = "MatchCardHolderRequest")
    public JAXBElement<MatchCardHolderRequest> createMatchCardHolderRequest(MatchCardHolderRequest value) {
        return new JAXBElement<MatchCardHolderRequest>(_MatchCardHolderRequest_QNAME, MatchCardHolderRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyAvsCvvVelocityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", name = "VerifyAvsCvvVelocityResponse")
    public JAXBElement<VerifyAvsCvvVelocityResponse> createVerifyAvsCvvVelocityResponse(VerifyAvsCvvVelocityResponse value) {
        return new JAXBElement<VerifyAvsCvvVelocityResponse>(_VerifyAvsCvvVelocityResponse_QNAME, VerifyAvsCvvVelocityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyAvsCvvVelocityRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", name = "VerifyAvsCvvVelocityRequest")
    public JAXBElement<VerifyAvsCvvVelocityRequest> createVerifyAvsCvvVelocityRequest(VerifyAvsCvvVelocityRequest value) {
        return new JAXBElement<VerifyAvsCvvVelocityRequest>(_VerifyAvsCvvVelocityRequest_QNAME, VerifyAvsCvvVelocityRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MatchCardHolderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-1", name = "MatchCardHolderResponse")
    public JAXBElement<MatchCardHolderResponse> createMatchCardHolderResponse(MatchCardHolderResponse value) {
        return new JAXBElement<MatchCardHolderResponse>(_MatchCardHolderResponse_QNAME, MatchCardHolderResponse.class, null, value);
    }

}
