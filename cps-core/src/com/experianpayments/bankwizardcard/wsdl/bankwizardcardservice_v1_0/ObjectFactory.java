
package com.experianpayments.bankwizardcard.wsdl.bankwizardcardservice_v1_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.experianpayments.bankwizardcard.xsd._2010._09.MatchDebitCardDetailsRequest;
import com.experianpayments.bankwizardcard.xsd._2010._09.MatchDebitCardDetailsResponse;
import com.experianpayments.bankwizardcard.xsd._2010._09.ValidateCardDetailsRequest;
import com.experianpayments.bankwizardcard.xsd._2010._09.ValidateCardDetailsResponse;
import com.experianpayments.bankwizardcard.xsd._2010._09.VerifyAvsCvvRequest;
import com.experianpayments.bankwizardcard.xsd._2010._09.VerifyAvsCvvResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizardcard.wsdl.bankwizardcardservice_v1_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerifyAvsCvvResponse_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", "VerifyAvsCvvResponse");
    private final static QName _VerifyAvsCvvRequest_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", "VerifyAvsCvvRequest");
    private final static QName _MatchDebitCardDetailsRequest_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", "MatchDebitCardDetailsRequest");
    private final static QName _MatchDebitCardDetailsResponse_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", "MatchDebitCardDetailsResponse");
    private final static QName _ValidateCardDetailsRequest_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", "ValidateCardDetailsRequest");
    private final static QName _ValidateCardDetailsResponse_QNAME = new QName("http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", "ValidateCardDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizardcard.wsdl.bankwizardcardservice_v1_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyAvsCvvResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", name = "VerifyAvsCvvResponse")
    public JAXBElement<VerifyAvsCvvResponse> createVerifyAvsCvvResponse(VerifyAvsCvvResponse value) {
        return new JAXBElement<VerifyAvsCvvResponse>(_VerifyAvsCvvResponse_QNAME, VerifyAvsCvvResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyAvsCvvRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", name = "VerifyAvsCvvRequest")
    public JAXBElement<VerifyAvsCvvRequest> createVerifyAvsCvvRequest(VerifyAvsCvvRequest value) {
        return new JAXBElement<VerifyAvsCvvRequest>(_VerifyAvsCvvRequest_QNAME, VerifyAvsCvvRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MatchDebitCardDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", name = "MatchDebitCardDetailsRequest")
    public JAXBElement<MatchDebitCardDetailsRequest> createMatchDebitCardDetailsRequest(MatchDebitCardDetailsRequest value) {
        return new JAXBElement<MatchDebitCardDetailsRequest>(_MatchDebitCardDetailsRequest_QNAME, MatchDebitCardDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MatchDebitCardDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", name = "MatchDebitCardDetailsResponse")
    public JAXBElement<MatchDebitCardDetailsResponse> createMatchDebitCardDetailsResponse(MatchDebitCardDetailsResponse value) {
        return new JAXBElement<MatchDebitCardDetailsResponse>(_MatchDebitCardDetailsResponse_QNAME, MatchDebitCardDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCardDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", name = "ValidateCardDetailsRequest")
    public JAXBElement<ValidateCardDetailsRequest> createValidateCardDetailsRequest(ValidateCardDetailsRequest value) {
        return new JAXBElement<ValidateCardDetailsRequest>(_ValidateCardDetailsRequest_QNAME, ValidateCardDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCardDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/wsdl/BankWizardCardService-v1-0", name = "ValidateCardDetailsResponse")
    public JAXBElement<ValidateCardDetailsResponse> createValidateCardDetailsResponse(ValidateCardDetailsResponse value) {
        return new JAXBElement<ValidateCardDetailsResponse>(_ValidateCardDetailsResponse_QNAME, ValidateCardDetailsResponse.class, null, value);
    }

}
