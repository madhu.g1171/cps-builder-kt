
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardcard.xsd._2010._09.GenericResponse;
import com.experianpayments.bankwizardcard.xsd._2010._09.VerifyAvsCvvResponse;


/**
 * 
 *         Overall response to a Card Details velocity and verify check.
 *       
 * 
 * <p>Java class for VerifyAvsCvvVelocityResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyAvsCvvVelocityResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericResponse">
 *       &lt;sequence>
 *         &lt;element name="VerifyAvsCvvResponse" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}VerifyAvsCvvResponse"/>
 *         &lt;element name="VelocityResponse" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}VelocityResponse"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyAvsCvvVelocityResponse", propOrder = {
    "verifyAvsCvvResponse",
    "velocityResponse"
})
public class VerifyAvsCvvVelocityResponse
    extends GenericResponse
{

    @XmlElement(name = "VerifyAvsCvvResponse", required = true)
    protected VerifyAvsCvvResponse verifyAvsCvvResponse;
    @XmlElement(name = "VelocityResponse", required = true)
    protected VelocityResponse velocityResponse;

    /**
     * Gets the value of the verifyAvsCvvResponse property.
     * 
     * @return
     *     possible object is
     *     {@link VerifyAvsCvvResponse }
     *     
     */
    public VerifyAvsCvvResponse getVerifyAvsCvvResponse() {
        return verifyAvsCvvResponse;
    }

    /**
     * Sets the value of the verifyAvsCvvResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerifyAvsCvvResponse }
     *     
     */
    public void setVerifyAvsCvvResponse(VerifyAvsCvvResponse value) {
        this.verifyAvsCvvResponse = value;
    }

    /**
     * Gets the value of the velocityResponse property.
     * 
     * @return
     *     possible object is
     *     {@link VelocityResponse }
     *     
     */
    public VelocityResponse getVelocityResponse() {
        return velocityResponse;
    }

    /**
     * Sets the value of the velocityResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link VelocityResponse }
     *     
     */
    public void setVelocityResponse(VelocityResponse value) {
        this.velocityResponse = value;
    }

}
