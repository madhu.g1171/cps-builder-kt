
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Datablocks complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Datablocks">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IPAddress" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}IPAddressDataBlockType" minOccurs="0"/>
 *         &lt;element name="BIN" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}BINDataBlockType" minOccurs="0"/>
 *         &lt;element name="Emails" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}EmailDataBlockType" minOccurs="0"/>
 *         &lt;element name="LinkedData" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}LinkedDataResultType" minOccurs="0"/>
 *         &lt;element name="DerivedData" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DerivedDataResultType" minOccurs="0"/>
 *         &lt;element name="ReportSummaryType" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}ReportSummaryType" minOccurs="0"/>
 *         &lt;element name="FraudResultType" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}FraudResultType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Datablocks", propOrder = {
    "ipAddress",
    "bin",
    "emails",
    "linkedData",
    "derivedData",
    "reportSummaryType",
    "fraudResultType"
})
public class Datablocks {

    @XmlElement(name = "IPAddress")
    protected IPAddressDataBlockType ipAddress;
    @XmlElement(name = "BIN")
    protected BINDataBlockType bin;
    @XmlElement(name = "Emails")
    protected EmailDataBlockType emails;
    @XmlElement(name = "LinkedData")
    protected LinkedDataResultType linkedData;
    @XmlElement(name = "DerivedData")
    protected DerivedDataResultType derivedData;
    @XmlElement(name = "ReportSummaryType")
    protected ReportSummaryType reportSummaryType;
    @XmlElement(name = "FraudResultType")
    protected FraudResultType fraudResultType;

    /**
     * Gets the value of the ipAddress property.
     * 
     * @return
     *     possible object is
     *     {@link IPAddressDataBlockType }
     *     
     */
    public IPAddressDataBlockType getIPAddress() {
        return ipAddress;
    }

    /**
     * Sets the value of the ipAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link IPAddressDataBlockType }
     *     
     */
    public void setIPAddress(IPAddressDataBlockType value) {
        this.ipAddress = value;
    }

    /**
     * Gets the value of the bin property.
     * 
     * @return
     *     possible object is
     *     {@link BINDataBlockType }
     *     
     */
    public BINDataBlockType getBIN() {
        return bin;
    }

    /**
     * Sets the value of the bin property.
     * 
     * @param value
     *     allowed object is
     *     {@link BINDataBlockType }
     *     
     */
    public void setBIN(BINDataBlockType value) {
        this.bin = value;
    }

    /**
     * Gets the value of the emails property.
     * 
     * @return
     *     possible object is
     *     {@link EmailDataBlockType }
     *     
     */
    public EmailDataBlockType getEmails() {
        return emails;
    }

    /**
     * Sets the value of the emails property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailDataBlockType }
     *     
     */
    public void setEmails(EmailDataBlockType value) {
        this.emails = value;
    }

    /**
     * Gets the value of the linkedData property.
     * 
     * @return
     *     possible object is
     *     {@link LinkedDataResultType }
     *     
     */
    public LinkedDataResultType getLinkedData() {
        return linkedData;
    }

    /**
     * Sets the value of the linkedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkedDataResultType }
     *     
     */
    public void setLinkedData(LinkedDataResultType value) {
        this.linkedData = value;
    }

    /**
     * Gets the value of the derivedData property.
     * 
     * @return
     *     possible object is
     *     {@link DerivedDataResultType }
     *     
     */
    public DerivedDataResultType getDerivedData() {
        return derivedData;
    }

    /**
     * Sets the value of the derivedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link DerivedDataResultType }
     *     
     */
    public void setDerivedData(DerivedDataResultType value) {
        this.derivedData = value;
    }

    /**
     * Gets the value of the reportSummaryType property.
     * 
     * @return
     *     possible object is
     *     {@link ReportSummaryType }
     *     
     */
    public ReportSummaryType getReportSummaryType() {
        return reportSummaryType;
    }

    /**
     * Sets the value of the reportSummaryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportSummaryType }
     *     
     */
    public void setReportSummaryType(ReportSummaryType value) {
        this.reportSummaryType = value;
    }

    /**
     * Gets the value of the fraudResultType property.
     * 
     * @return
     *     possible object is
     *     {@link FraudResultType }
     *     
     */
    public FraudResultType getFraudResultType() {
        return fraudResultType;
    }

    /**
     * Sets the value of the fraudResultType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudResultType }
     *     
     */
    public void setFraudResultType(FraudResultType value) {
        this.fraudResultType = value;
    }

}
