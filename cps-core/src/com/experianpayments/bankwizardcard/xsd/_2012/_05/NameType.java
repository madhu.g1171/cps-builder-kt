
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for NameType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NameType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Title" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="Forename" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://experianpayments.com/bankwizardcard/xsd/2012/05>strMin1">
 *                 &lt;attribute name="Match" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}MatchType" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OtherNames" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="Surname">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://experianpayments.com/bankwizardcard/xsd/2012/05>strMin2">
 *                 &lt;attribute name="Match" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}MatchType" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameType", propOrder = {

})
public class NameType {

    @XmlElement(name = "Title")
    protected StringWithMatch title;
    @XmlElement(name = "Forename")
    protected NameType.Forename forename;
    @XmlElement(name = "OtherNames")
    protected StringWithMatch otherNames;
    @XmlElement(name = "Surname", required = true)
    protected NameType.Surname surname;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setTitle(StringWithMatch value) {
        this.title = value;
    }

    /**
     * Gets the value of the forename property.
     * 
     * @return
     *     possible object is
     *     {@link NameType.Forename }
     *     
     */
    public NameType.Forename getForename() {
        return forename;
    }

    /**
     * Sets the value of the forename property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameType.Forename }
     *     
     */
    public void setForename(NameType.Forename value) {
        this.forename = value;
    }

    /**
     * Gets the value of the otherNames property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getOtherNames() {
        return otherNames;
    }

    /**
     * Sets the value of the otherNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setOtherNames(StringWithMatch value) {
        this.otherNames = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return
     *     possible object is
     *     {@link NameType.Surname }
     *     
     */
    public NameType.Surname getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameType.Surname }
     *     
     */
    public void setSurname(NameType.Surname value) {
        this.surname = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://experianpayments.com/bankwizardcard/xsd/2012/05>strMin1">
     *       &lt;attribute name="Match" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}MatchType" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Forename {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Match")
        protected Integer match;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the match property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMatch() {
            return match;
        }

        /**
         * Sets the value of the match property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMatch(Integer value) {
            this.match = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://experianpayments.com/bankwizardcard/xsd/2012/05>strMin2">
     *       &lt;attribute name="Match" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}MatchType" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Surname {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Match")
        protected Integer match;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the match property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMatch() {
            return match;
        }

        /**
         * Sets the value of the match property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMatch(Integer value) {
            this.match = value;
        }

    }

}
