
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.PersonalDetailsVerificationType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizardcard.xsd._2012._05 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MatchCardHolderResponseAddressMatchResult_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2012/05", "addressMatchResult");
    private final static QName _MatchCardHolderResponsePersonalDetailsScore_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2012/05", "personalDetailsScore");
    private final static QName _VelocityResponseDatablocks_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2012/05", "Datablocks");
    private final static QName _VelocityResponseDecisionMatrix_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2012/05", "DecisionMatrix");
    private final static QName _VerifyAvsCvvVelocityRequestEmailAddress_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2012/05", "EmailAddress");
    private final static QName _VerifyAvsCvvVelocityRequestIpAddress_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2012/05", "IpAddress");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizardcard.xsd._2012._05
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PersonResponse }
     * 
     */
    public PersonResponse createPersonResponse() {
        return new PersonResponse();
    }

    /**
     * Create an instance of {@link NameType }
     * 
     */
    public NameType createNameType() {
        return new NameType();
    }

    /**
     * Create an instance of {@link RuleList }
     * 
     */
    public RuleList createRuleList() {
        return new RuleList();
    }

    /**
     * Create an instance of {@link VerifyAvsCvvVelocityResponse }
     * 
     */
    public VerifyAvsCvvVelocityResponse createVerifyAvsCvvVelocityResponse() {
        return new VerifyAvsCvvVelocityResponse();
    }

    /**
     * Create an instance of {@link VerifyAvsCvvVelocityRequest }
     * 
     */
    public VerifyAvsCvvVelocityRequest createVerifyAvsCvvVelocityRequest() {
        return new VerifyAvsCvvVelocityRequest();
    }

    /**
     * Create an instance of {@link MatchCardHolderResponse }
     * 
     */
    public MatchCardHolderResponse createMatchCardHolderResponse() {
        return new MatchCardHolderResponse();
    }

    /**
     * Create an instance of {@link MatchCardHolderRequest }
     * 
     */
    public MatchCardHolderRequest createMatchCardHolderRequest() {
        return new MatchCardHolderRequest();
    }

    /**
     * Create an instance of {@link DecisionReasonType }
     * 
     */
    public DecisionReasonType createDecisionReasonType() {
        return new DecisionReasonType();
    }

    /**
     * Create an instance of {@link FraudRecordType }
     * 
     */
    public FraudRecordType createFraudRecordType() {
        return new FraudRecordType();
    }

    /**
     * Create an instance of {@link IPHistoryType }
     * 
     */
    public IPHistoryType createIPHistoryType() {
        return new IPHistoryType();
    }

    /**
     * Create an instance of {@link EmailType }
     * 
     */
    public EmailType createEmailType() {
        return new EmailType();
    }

    /**
     * Create an instance of {@link LinkedDataResultType }
     * 
     */
    public LinkedDataResultType createLinkedDataResultType() {
        return new LinkedDataResultType();
    }

    /**
     * Create an instance of {@link TelephoneType }
     * 
     */
    public TelephoneType createTelephoneType() {
        return new TelephoneType();
    }

    /**
     * Create an instance of {@link LivesWithType }
     * 
     */
    public LivesWithType createLivesWithType() {
        return new LivesWithType();
    }

    /**
     * Create an instance of {@link TelephoneList }
     * 
     */
    public TelephoneList createTelephoneList() {
        return new TelephoneList();
    }

    /**
     * Create an instance of {@link StringWithMatch }
     * 
     */
    public StringWithMatch createStringWithMatch() {
        return new StringWithMatch();
    }

    /**
     * Create an instance of {@link OrderType }
     * 
     */
    public OrderType createOrderType() {
        return new OrderType();
    }

    /**
     * Create an instance of {@link AddressResponse }
     * 
     */
    public AddressResponse createAddressResponse() {
        return new AddressResponse();
    }

    /**
     * Create an instance of {@link AddressVerificationType }
     * 
     */
    public AddressVerificationType createAddressVerificationType() {
        return new AddressVerificationType();
    }

    /**
     * Create an instance of {@link BINType }
     * 
     */
    public BINType createBINType() {
        return new BINType();
    }

    /**
     * Create an instance of {@link AliasList }
     * 
     */
    public AliasList createAliasList() {
        return new AliasList();
    }

    /**
     * Create an instance of {@link NationalityList }
     * 
     */
    public NationalityList createNationalityList() {
        return new NationalityList();
    }

    /**
     * Create an instance of {@link DecisionReasonList }
     * 
     */
    public DecisionReasonList createDecisionReasonList() {
        return new DecisionReasonList();
    }

    /**
     * Create an instance of {@link ReportSummaryItemList }
     * 
     */
    public ReportSummaryItemList createReportSummaryItemList() {
        return new ReportSummaryItemList();
    }

    /**
     * Create an instance of {@link DatablockSummaryType }
     * 
     */
    public DatablockSummaryType createDatablockSummaryType() {
        return new DatablockSummaryType();
    }

    /**
     * Create an instance of {@link DerivedDataResultType }
     * 
     */
    public DerivedDataResultType createDerivedDataResultType() {
        return new DerivedDataResultType();
    }

    /**
     * Create an instance of {@link ReportSummaryItemType }
     * 
     */
    public ReportSummaryItemType createReportSummaryItemType() {
        return new ReportSummaryItemType();
    }

    /**
     * Create an instance of {@link CardResponse }
     * 
     */
    public CardResponse createCardResponse() {
        return new CardResponse();
    }

    /**
     * Create an instance of {@link CountryType }
     * 
     */
    public CountryType createCountryType() {
        return new CountryType();
    }

    /**
     * Create an instance of {@link DerivedDataType }
     * 
     */
    public DerivedDataType createDerivedDataType() {
        return new DerivedDataType();
    }

    /**
     * Create an instance of {@link EmailDataBlockType }
     * 
     */
    public EmailDataBlockType createEmailDataBlockType() {
        return new EmailDataBlockType();
    }

    /**
     * Create an instance of {@link TransactionType }
     * 
     */
    public TransactionType createTransactionType() {
        return new TransactionType();
    }

    /**
     * Create an instance of {@link RegionType }
     * 
     */
    public RegionType createRegionType() {
        return new RegionType();
    }

    /**
     * Create an instance of {@link Telephone }
     * 
     */
    public Telephone createTelephone() {
        return new Telephone();
    }

    /**
     * Create an instance of {@link Datablocks }
     * 
     */
    public Datablocks createDatablocks() {
        return new Datablocks();
    }

    /**
     * Create an instance of {@link CityType }
     * 
     */
    public CityType createCityType() {
        return new CityType();
    }

    /**
     * Create an instance of {@link MoneyAmount }
     * 
     */
    public MoneyAmount createMoneyAmount() {
        return new MoneyAmount();
    }

    /**
     * Create an instance of {@link ReportSummaryType }
     * 
     */
    public ReportSummaryType createReportSummaryType() {
        return new ReportSummaryType();
    }

    /**
     * Create an instance of {@link TransactionAmountType }
     * 
     */
    public TransactionAmountType createTransactionAmountType() {
        return new TransactionAmountType();
    }

    /**
     * Create an instance of {@link DecisionType }
     * 
     */
    public DecisionType createDecisionType() {
        return new DecisionType();
    }

    /**
     * Create an instance of {@link DecisionOutcomeType }
     * 
     */
    public DecisionOutcomeType createDecisionOutcomeType() {
        return new DecisionOutcomeType();
    }

    /**
     * Create an instance of {@link AddressList }
     * 
     */
    public AddressList createAddressList() {
        return new AddressList();
    }

    /**
     * Create an instance of {@link CardsList }
     * 
     */
    public CardsList createCardsList() {
        return new CardsList();
    }

    /**
     * Create an instance of {@link TransactionDetailsType }
     * 
     */
    public TransactionDetailsType createTransactionDetailsType() {
        return new TransactionDetailsType();
    }

    /**
     * Create an instance of {@link TelephoneResponse }
     * 
     */
    public TelephoneResponse createTelephoneResponse() {
        return new TelephoneResponse();
    }

    /**
     * Create an instance of {@link IPAddressDetailsType }
     * 
     */
    public IPAddressDetailsType createIPAddressDetailsType() {
        return new IPAddressDetailsType();
    }

    /**
     * Create an instance of {@link IPAddressDataBlockType }
     * 
     */
    public IPAddressDataBlockType createIPAddressDataBlockType() {
        return new IPAddressDataBlockType();
    }

    /**
     * Create an instance of {@link EmailList }
     * 
     */
    public EmailList createEmailList() {
        return new EmailList();
    }

    /**
     * Create an instance of {@link DecisionMatrixType }
     * 
     */
    public DecisionMatrixType createDecisionMatrixType() {
        return new DecisionMatrixType();
    }

    /**
     * Create an instance of {@link LinkedDataType }
     * 
     */
    public LinkedDataType createLinkedDataType() {
        return new LinkedDataType();
    }

    /**
     * Create an instance of {@link FraudResultType }
     * 
     */
    public FraudResultType createFraudResultType() {
        return new FraudResultType();
    }

    /**
     * Create an instance of {@link BINDataBlockType }
     * 
     */
    public BINDataBlockType createBINDataBlockType() {
        return new BINDataBlockType();
    }

    /**
     * Create an instance of {@link CV2AVSResponseType }
     * 
     */
    public CV2AVSResponseType createCV2AVSResponseType() {
        return new CV2AVSResponseType();
    }

    /**
     * Create an instance of {@link OrderAmountType }
     * 
     */
    public OrderAmountType createOrderAmountType() {
        return new OrderAmountType();
    }

    /**
     * Create an instance of {@link DecisionRuleType }
     * 
     */
    public DecisionRuleType createDecisionRuleType() {
        return new DecisionRuleType();
    }

    /**
     * Create an instance of {@link VelocityResponse }
     * 
     */
    public VelocityResponse createVelocityResponse() {
        return new VelocityResponse();
    }

    /**
     * Create an instance of {@link PersonResponse.Gender }
     * 
     */
    public PersonResponse.Gender createPersonResponseGender() {
        return new PersonResponse.Gender();
    }

    /**
     * Create an instance of {@link PersonResponse.DateOfBirth }
     * 
     */
    public PersonResponse.DateOfBirth createPersonResponseDateOfBirth() {
        return new PersonResponse.DateOfBirth();
    }

    /**
     * Create an instance of {@link NameType.Forename }
     * 
     */
    public NameType.Forename createNameTypeForename() {
        return new NameType.Forename();
    }

    /**
     * Create an instance of {@link NameType.Surname }
     * 
     */
    public NameType.Surname createNameTypeSurname() {
        return new NameType.Surname();
    }

    /**
     * Create an instance of {@link RuleList.Rule }
     * 
     */
    public RuleList.Rule createRuleListRule() {
        return new RuleList.Rule();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonalDetailsVerificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", name = "addressMatchResult", scope = MatchCardHolderResponse.class)
    public JAXBElement<PersonalDetailsVerificationType> createMatchCardHolderResponseAddressMatchResult(PersonalDetailsVerificationType value) {
        return new JAXBElement<PersonalDetailsVerificationType>(_MatchCardHolderResponseAddressMatchResult_QNAME, PersonalDetailsVerificationType.class, MatchCardHolderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", name = "personalDetailsScore", scope = MatchCardHolderResponse.class)
    public JAXBElement<Integer> createMatchCardHolderResponsePersonalDetailsScore(Integer value) {
        return new JAXBElement<Integer>(_MatchCardHolderResponsePersonalDetailsScore_QNAME, Integer.class, MatchCardHolderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Datablocks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", name = "Datablocks", scope = VelocityResponse.class)
    public JAXBElement<Datablocks> createVelocityResponseDatablocks(Datablocks value) {
        return new JAXBElement<Datablocks>(_VelocityResponseDatablocks_QNAME, Datablocks.class, VelocityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DecisionMatrixType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", name = "DecisionMatrix", scope = VelocityResponse.class)
    public JAXBElement<DecisionMatrixType> createVelocityResponseDecisionMatrix(DecisionMatrixType value) {
        return new JAXBElement<DecisionMatrixType>(_VelocityResponseDecisionMatrix_QNAME, DecisionMatrixType.class, VelocityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", name = "EmailAddress", scope = VerifyAvsCvvVelocityRequest.class)
    public JAXBElement<String> createVerifyAvsCvvVelocityRequestEmailAddress(String value) {
        return new JAXBElement<String>(_VerifyAvsCvvVelocityRequestEmailAddress_QNAME, String.class, VerifyAvsCvvVelocityRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", name = "IpAddress", scope = VerifyAvsCvvVelocityRequest.class)
    public JAXBElement<String> createVerifyAvsCvvVelocityRequestIpAddress(String value) {
        return new JAXBElement<String>(_VerifyAvsCvvVelocityRequestIpAddress_QNAME, String.class, VerifyAvsCvvVelocityRequest.class, value);
    }

}
