
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LivesWithType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LivesWithType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoOfResidents" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Resident" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}NameType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LivesWithType", propOrder = {
    "noOfResidents",
    "resident"
})
public class LivesWithType {

    @XmlElement(name = "NoOfResidents")
    protected int noOfResidents;
    @XmlElement(name = "Resident")
    protected List<NameType> resident;

    /**
     * Gets the value of the noOfResidents property.
     * 
     */
    public int getNoOfResidents() {
        return noOfResidents;
    }

    /**
     * Sets the value of the noOfResidents property.
     * 
     */
    public void setNoOfResidents(int value) {
        this.noOfResidents = value;
    }

    /**
     * Gets the value of the resident property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resident property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResident().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameType }
     * 
     * 
     */
    public List<NameType> getResident() {
        if (resident == null) {
            resident = new ArrayList<NameType>();
        }
        return this.resident;
    }

}
