
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TransactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuthorisationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankResponseMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThreeDSecureECIIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThreeDSecureCavvAvv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionDetailsType", propOrder = {

})
public class TransactionDetailsType {

    @XmlElement(name = "TransactionType")
    protected String transactionType;
    @XmlElement(name = "AuthorisationCode")
    protected String authorisationCode;
    @XmlElement(name = "BankResponseCode")
    protected String bankResponseCode;
    @XmlElement(name = "BankResponseMessage")
    protected String bankResponseMessage;
    @XmlElement(name = "ThreeDSecureECIIndicator")
    protected String threeDSecureECIIndicator;
    @XmlElement(name = "ThreeDSecureCavvAvv")
    protected String threeDSecureCavvAvv;

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the authorisationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorisationCode() {
        return authorisationCode;
    }

    /**
     * Sets the value of the authorisationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorisationCode(String value) {
        this.authorisationCode = value;
    }

    /**
     * Gets the value of the bankResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankResponseCode() {
        return bankResponseCode;
    }

    /**
     * Sets the value of the bankResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankResponseCode(String value) {
        this.bankResponseCode = value;
    }

    /**
     * Gets the value of the bankResponseMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankResponseMessage() {
        return bankResponseMessage;
    }

    /**
     * Sets the value of the bankResponseMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankResponseMessage(String value) {
        this.bankResponseMessage = value;
    }

    /**
     * Gets the value of the threeDSecureECIIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThreeDSecureECIIndicator() {
        return threeDSecureECIIndicator;
    }

    /**
     * Sets the value of the threeDSecureECIIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThreeDSecureECIIndicator(String value) {
        this.threeDSecureECIIndicator = value;
    }

    /**
     * Gets the value of the threeDSecureCavvAvv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThreeDSecureCavvAvv() {
        return threeDSecureCavvAvv;
    }

    /**
     * Sets the value of the threeDSecureCavvAvv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThreeDSecureCavvAvv(String value) {
        this.threeDSecureCavvAvv = value;
    }

}
