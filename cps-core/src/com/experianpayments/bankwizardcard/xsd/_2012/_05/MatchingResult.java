
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MatchingResult.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MatchingResult">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Full"/>
 *     &lt;enumeration value="Partial"/>
 *     &lt;enumeration value="Issuer"/>
 *     &lt;enumeration value="No Match"/>
 *     &lt;enumeration value="Unable To Match"/>
 *     &lt;enumeration value="Account Closed"/>
 *     &lt;enumeration value="Deceased"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MatchingResult")
@XmlEnum
public enum MatchingResult {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Partial")
    PARTIAL("Partial"),
    @XmlEnumValue("Issuer")
    ISSUER("Issuer"),
    @XmlEnumValue("No Match")
    NO_MATCH("No Match"),
    @XmlEnumValue("Unable To Match")
    UNABLE_TO_MATCH("Unable To Match"),
    @XmlEnumValue("Account Closed")
    ACCOUNT_CLOSED("Account Closed"),
    @XmlEnumValue("Deceased")
    DECEASED("Deceased");
    private final String value;

    MatchingResult(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MatchingResult fromValue(String v) {
        for (MatchingResult c: MatchingResult.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
