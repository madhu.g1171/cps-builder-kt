
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizard.xsd._2009._07.PersonalDetails;
import com.experianpayments.bankwizardcard.xsd._2010._09.VerifyAvsCvvRequest;


/**
 * 
 *         Request for a Velocity and verify check. (Validation is automatically performed as part of the verify check)
 *       
 * 
 * <p>Java class for VerifyAvsCvvVelocityRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyAvsCvvVelocityRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}VerifyAvsCvvRequest">
 *       &lt;sequence>
 *         &lt;element name="Country" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}ThreeCharCode"/>
 *         &lt;element name="Personal" type="{http://experianpayments.com/bankwizard/xsd/2009/07}PersonalDetails"/>
 *         &lt;element name="EmailAddress" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}EmailAddress" minOccurs="0"/>
 *         &lt;element name="IpAddress" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}IPAddress" minOccurs="0"/>
 *         &lt;element name="Telephones" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}TelephoneList"/>
 *         &lt;element name="Order" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}OrderType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyAvsCvvVelocityRequest", propOrder = {
    "country",
    "personal",
    "emailAddress",
    "ipAddress",
    "telephones",
    "order"
})
public class VerifyAvsCvvVelocityRequest
    extends VerifyAvsCvvRequest
{

    @XmlElement(name = "Country", required = true)
    protected String country;
    @XmlElement(name = "Personal", required = true)
    protected PersonalDetails personal;
    @XmlElementRef(name = "EmailAddress", namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailAddress;
    @XmlElementRef(name = "IpAddress", namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ipAddress;
    @XmlElement(name = "Telephones", required = true, nillable = true)
    protected TelephoneList telephones;
    @XmlElement(name = "Order", required = true, nillable = true)
    protected OrderType order;

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the personal property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetails }
     *     
     */
    public PersonalDetails getPersonal() {
        return personal;
    }

    /**
     * Sets the value of the personal property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetails }
     *     
     */
    public void setPersonal(PersonalDetails value) {
        this.personal = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailAddress(JAXBElement<String> value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the ipAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIpAddress() {
        return ipAddress;
    }

    /**
     * Sets the value of the ipAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIpAddress(JAXBElement<String> value) {
        this.ipAddress = value;
    }

    /**
     * Gets the value of the telephones property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneList }
     *     
     */
    public TelephoneList getTelephones() {
        return telephones;
    }

    /**
     * Sets the value of the telephones property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneList }
     *     
     */
    public void setTelephones(TelephoneList value) {
        this.telephones = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link OrderType }
     *     
     */
    public OrderType getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderType }
     *     
     */
    public void setOrder(OrderType value) {
        this.order = value;
    }

}
