
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TransactionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TransactionAmount" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}TransactionAmountType" minOccurs="0"/>
 *         &lt;element name="TransactionDetails" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}TransactionDetailsType" minOccurs="0"/>
 *         &lt;element name="CV2AVSResponse" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}CV2AVSResponseType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionType", propOrder = {

})
public class TransactionType {

    @XmlElement(name = "TransactionDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transactionDate;
    @XmlElement(name = "TransactionAmount")
    protected TransactionAmountType transactionAmount;
    @XmlElement(name = "TransactionDetails")
    protected TransactionDetailsType transactionDetails;
    @XmlElement(name = "CV2AVSResponse")
    protected CV2AVSResponseType cv2AVSResponse;

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransactionDate(XMLGregorianCalendar value) {
        this.transactionDate = value;
    }

    /**
     * Gets the value of the transactionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionAmountType }
     *     
     */
    public TransactionAmountType getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * Sets the value of the transactionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionAmountType }
     *     
     */
    public void setTransactionAmount(TransactionAmountType value) {
        this.transactionAmount = value;
    }

    /**
     * Gets the value of the transactionDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDetailsType }
     *     
     */
    public TransactionDetailsType getTransactionDetails() {
        return transactionDetails;
    }

    /**
     * Sets the value of the transactionDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDetailsType }
     *     
     */
    public void setTransactionDetails(TransactionDetailsType value) {
        this.transactionDetails = value;
    }

    /**
     * Gets the value of the cv2AVSResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CV2AVSResponseType }
     *     
     */
    public CV2AVSResponseType getCV2AVSResponse() {
        return cv2AVSResponse;
    }

    /**
     * Sets the value of the cv2AVSResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CV2AVSResponseType }
     *     
     */
    public void setCV2AVSResponse(CV2AVSResponseType value) {
        this.cv2AVSResponse = value;
    }

}
