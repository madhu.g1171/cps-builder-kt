
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for LinkedDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkedDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="LinkedTransactions" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedFraud" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedCards" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedEmails" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedPhoneNumbers" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedBillingAddresses" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedDeliveryAddresses" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedNameAndAddresses" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedIPAddresses" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LinkedMerchants" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="LastTransactionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DaysSinceLastTransaction" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="AverageOrderValue" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}MoneyAmount" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkedDataType", propOrder = {

})
public class LinkedDataType {

    @XmlElement(name = "LinkedTransactions")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedTransactions;
    @XmlElement(name = "LinkedFraud")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedFraud;
    @XmlElement(name = "LinkedCards")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedCards;
    @XmlElement(name = "LinkedEmails")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedEmails;
    @XmlElement(name = "LinkedPhoneNumbers")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedPhoneNumbers;
    @XmlElement(name = "LinkedBillingAddresses")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedBillingAddresses;
    @XmlElement(name = "LinkedDeliveryAddresses")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedDeliveryAddresses;
    @XmlElement(name = "LinkedNameAndAddresses")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedNameAndAddresses;
    @XmlElement(name = "LinkedIPAddresses")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedIPAddresses;
    @XmlElement(name = "LinkedMerchants")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger linkedMerchants;
    @XmlElement(name = "LastTransactionDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastTransactionDate;
    @XmlElement(name = "DaysSinceLastTransaction")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger daysSinceLastTransaction;
    @XmlElement(name = "AverageOrderValue")
    protected MoneyAmount averageOrderValue;

    /**
     * Gets the value of the linkedTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedTransactions() {
        return linkedTransactions;
    }

    /**
     * Sets the value of the linkedTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedTransactions(BigInteger value) {
        this.linkedTransactions = value;
    }

    /**
     * Gets the value of the linkedFraud property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedFraud() {
        return linkedFraud;
    }

    /**
     * Sets the value of the linkedFraud property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedFraud(BigInteger value) {
        this.linkedFraud = value;
    }

    /**
     * Gets the value of the linkedCards property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedCards() {
        return linkedCards;
    }

    /**
     * Sets the value of the linkedCards property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedCards(BigInteger value) {
        this.linkedCards = value;
    }

    /**
     * Gets the value of the linkedEmails property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedEmails() {
        return linkedEmails;
    }

    /**
     * Sets the value of the linkedEmails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedEmails(BigInteger value) {
        this.linkedEmails = value;
    }

    /**
     * Gets the value of the linkedPhoneNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedPhoneNumbers() {
        return linkedPhoneNumbers;
    }

    /**
     * Sets the value of the linkedPhoneNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedPhoneNumbers(BigInteger value) {
        this.linkedPhoneNumbers = value;
    }

    /**
     * Gets the value of the linkedBillingAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedBillingAddresses() {
        return linkedBillingAddresses;
    }

    /**
     * Sets the value of the linkedBillingAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedBillingAddresses(BigInteger value) {
        this.linkedBillingAddresses = value;
    }

    /**
     * Gets the value of the linkedDeliveryAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedDeliveryAddresses() {
        return linkedDeliveryAddresses;
    }

    /**
     * Sets the value of the linkedDeliveryAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedDeliveryAddresses(BigInteger value) {
        this.linkedDeliveryAddresses = value;
    }

    /**
     * Gets the value of the linkedNameAndAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedNameAndAddresses() {
        return linkedNameAndAddresses;
    }

    /**
     * Sets the value of the linkedNameAndAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedNameAndAddresses(BigInteger value) {
        this.linkedNameAndAddresses = value;
    }

    /**
     * Gets the value of the linkedIPAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedIPAddresses() {
        return linkedIPAddresses;
    }

    /**
     * Sets the value of the linkedIPAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedIPAddresses(BigInteger value) {
        this.linkedIPAddresses = value;
    }

    /**
     * Gets the value of the linkedMerchants property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLinkedMerchants() {
        return linkedMerchants;
    }

    /**
     * Sets the value of the linkedMerchants property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLinkedMerchants(BigInteger value) {
        this.linkedMerchants = value;
    }

    /**
     * Gets the value of the lastTransactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastTransactionDate() {
        return lastTransactionDate;
    }

    /**
     * Sets the value of the lastTransactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastTransactionDate(XMLGregorianCalendar value) {
        this.lastTransactionDate = value;
    }

    /**
     * Gets the value of the daysSinceLastTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDaysSinceLastTransaction() {
        return daysSinceLastTransaction;
    }

    /**
     * Sets the value of the daysSinceLastTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDaysSinceLastTransaction(BigInteger value) {
        this.daysSinceLastTransaction = value;
    }

    /**
     * Gets the value of the averageOrderValue property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyAmount }
     *     
     */
    public MoneyAmount getAverageOrderValue() {
        return averageOrderValue;
    }

    /**
     * Sets the value of the averageOrderValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyAmount }
     *     
     */
    public void setAverageOrderValue(MoneyAmount value) {
        this.averageOrderValue = value;
    }

}
