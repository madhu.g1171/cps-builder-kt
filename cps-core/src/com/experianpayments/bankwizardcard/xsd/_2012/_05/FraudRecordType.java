
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FraudRecordType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FraudRecordType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Source" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Internal"/>
 *               &lt;enumeration value="CIFAS"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FraudType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FraudSector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FraudDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Person" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}PersonResponse" minOccurs="0"/>
 *         &lt;element name="Organisation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isCommercialEntity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AccessLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Author" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cards" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}CardsList" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}AddressResponse" minOccurs="0"/>
 *         &lt;element name="ShipToAddress" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}AddressResponse" minOccurs="0"/>
 *         &lt;element name="Telephones" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}TelephoneResponse" minOccurs="0"/>
 *         &lt;element name="Emails" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}EmailList" minOccurs="0"/>
 *         &lt;element name="IPAddress" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}IPAddress" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FraudRecordType", propOrder = {

})
public class FraudRecordType {

    @XmlElement(name = "Source")
    protected String source;
    @XmlElement(name = "ReferenceNumber")
    protected String referenceNumber;
    @XmlElement(name = "FraudType")
    protected String fraudType;
    @XmlElement(name = "FraudSector")
    protected String fraudSector;
    @XmlElement(name = "FraudDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fraudDate;
    @XmlElement(name = "Person")
    protected PersonResponse person;
    @XmlElement(name = "Organisation")
    protected String organisation;
    protected Boolean isCommercialEntity;
    @XmlElement(name = "AccessLevel")
    protected String accessLevel;
    @XmlElement(name = "Author")
    protected String author;
    @XmlElement(name = "Cards")
    protected CardsList cards;
    @XmlElement(name = "Address")
    protected AddressResponse address;
    @XmlElement(name = "ShipToAddress")
    protected AddressResponse shipToAddress;
    @XmlElement(name = "Telephones")
    protected TelephoneResponse telephones;
    @XmlElement(name = "Emails")
    protected EmailList emails;
    @XmlElement(name = "IPAddress")
    protected String ipAddress;
    @XmlElement(name = "Comments")
    protected String comments;

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the fraudType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFraudType() {
        return fraudType;
    }

    /**
     * Sets the value of the fraudType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFraudType(String value) {
        this.fraudType = value;
    }

    /**
     * Gets the value of the fraudSector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFraudSector() {
        return fraudSector;
    }

    /**
     * Sets the value of the fraudSector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFraudSector(String value) {
        this.fraudSector = value;
    }

    /**
     * Gets the value of the fraudDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFraudDate() {
        return fraudDate;
    }

    /**
     * Sets the value of the fraudDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFraudDate(XMLGregorianCalendar value) {
        this.fraudDate = value;
    }

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link PersonResponse }
     *     
     */
    public PersonResponse getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonResponse }
     *     
     */
    public void setPerson(PersonResponse value) {
        this.person = value;
    }

    /**
     * Gets the value of the organisation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganisation() {
        return organisation;
    }

    /**
     * Sets the value of the organisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganisation(String value) {
        this.organisation = value;
    }

    /**
     * Gets the value of the isCommercialEntity property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCommercialEntity() {
        return isCommercialEntity;
    }

    /**
     * Sets the value of the isCommercialEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCommercialEntity(Boolean value) {
        this.isCommercialEntity = value;
    }

    /**
     * Gets the value of the accessLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * Sets the value of the accessLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessLevel(String value) {
        this.accessLevel = value;
    }

    /**
     * Gets the value of the author property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets the value of the author property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthor(String value) {
        this.author = value;
    }

    /**
     * Gets the value of the cards property.
     * 
     * @return
     *     possible object is
     *     {@link CardsList }
     *     
     */
    public CardsList getCards() {
        return cards;
    }

    /**
     * Sets the value of the cards property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardsList }
     *     
     */
    public void setCards(CardsList value) {
        this.cards = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link AddressResponse }
     *     
     */
    public AddressResponse getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressResponse }
     *     
     */
    public void setAddress(AddressResponse value) {
        this.address = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressResponse }
     *     
     */
    public AddressResponse getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressResponse }
     *     
     */
    public void setShipToAddress(AddressResponse value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the telephones property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneResponse }
     *     
     */
    public TelephoneResponse getTelephones() {
        return telephones;
    }

    /**
     * Sets the value of the telephones property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneResponse }
     *     
     */
    public void setTelephones(TelephoneResponse value) {
        this.telephones = value;
    }

    /**
     * Gets the value of the emails property.
     * 
     * @return
     *     possible object is
     *     {@link EmailList }
     *     
     */
    public EmailList getEmails() {
        return emails;
    }

    /**
     * Sets the value of the emails property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailList }
     *     
     */
    public void setEmails(EmailList value) {
        this.emails = value;
    }

    /**
     * Gets the value of the ipAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPAddress() {
        return ipAddress;
    }

    /**
     * Sets the value of the ipAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPAddress(String value) {
        this.ipAddress = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

}
