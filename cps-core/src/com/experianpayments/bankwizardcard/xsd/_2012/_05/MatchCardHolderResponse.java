
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.PersonalDetailsVerificationType;
import com.experianpayments.bankwizardcard.xsd._2010._09.CardValidationResponse;
import com.experianpayments.bankwizardcard.xsd._2010._09.GenericCodeMessage;
import com.experianpayments.bankwizardcard.xsd._2010._09.GenericResponse;


/**
 * 
 *         Response for Match Card Holder Request
 *       
 * 
 * <p>Java class for MatchCardHolderResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MatchCardHolderResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericResponse">
 *       &lt;sequence>
 *         &lt;element name="overallResult" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}MatchingResult"/>
 *         &lt;element name="cardCheckResult" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}MatchingResult"/>
 *         &lt;element name="accountClosedFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deceasedFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="personalDetailsScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *         &lt;element name="addressMatchResult" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}PersonalDetailsVerificationType" minOccurs="0"/>
 *         &lt;element name="CardValidationResponse" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardValidationResponse"/>
 *         &lt;element name="ExceptionMessage" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericCodeMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MatchCardHolderResponse", propOrder = {
    "overallResult",
    "cardCheckResult",
    "accountClosedFlag",
    "deceasedFlag",
    "personalDetailsScore",
    "addressMatchResult",
    "cardValidationResponse",
    "exceptionMessage"
})
public class MatchCardHolderResponse
    extends GenericResponse
{

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "token")
    protected MatchingResult overallResult;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "token")
    protected MatchingResult cardCheckResult;
    @XmlElement(required = true, type = Boolean.class, nillable = true)
    protected Boolean accountClosedFlag;
    @XmlElement(required = true, type = Boolean.class, nillable = true)
    protected Boolean deceasedFlag;
    @XmlElementRef(name = "personalDetailsScore", namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> personalDetailsScore;
    @XmlElementRef(name = "addressMatchResult", namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", type = JAXBElement.class, required = false)
    protected JAXBElement<PersonalDetailsVerificationType> addressMatchResult;
    @XmlElement(name = "CardValidationResponse", required = true)
    protected CardValidationResponse cardValidationResponse;
    @XmlElement(name = "ExceptionMessage")
    protected List<GenericCodeMessage> exceptionMessage;

    /**
     * Gets the value of the overallResult property.
     * 
     * @return
     *     possible object is
     *     {@link MatchingResult }
     *     
     */
    public MatchingResult getOverallResult() {
        return overallResult;
    }

    /**
     * Sets the value of the overallResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchingResult }
     *     
     */
    public void setOverallResult(MatchingResult value) {
        this.overallResult = value;
    }

    /**
     * Gets the value of the cardCheckResult property.
     * 
     * @return
     *     possible object is
     *     {@link MatchingResult }
     *     
     */
    public MatchingResult getCardCheckResult() {
        return cardCheckResult;
    }

    /**
     * Sets the value of the cardCheckResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchingResult }
     *     
     */
    public void setCardCheckResult(MatchingResult value) {
        this.cardCheckResult = value;
    }

    /**
     * Gets the value of the accountClosedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAccountClosedFlag() {
        return accountClosedFlag;
    }

    /**
     * Sets the value of the accountClosedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccountClosedFlag(Boolean value) {
        this.accountClosedFlag = value;
    }

    /**
     * Gets the value of the deceasedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeceasedFlag() {
        return deceasedFlag;
    }

    /**
     * Sets the value of the deceasedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeceasedFlag(Boolean value) {
        this.deceasedFlag = value;
    }

    /**
     * Gets the value of the personalDetailsScore property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPersonalDetailsScore() {
        return personalDetailsScore;
    }

    /**
     * Sets the value of the personalDetailsScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPersonalDetailsScore(JAXBElement<Integer> value) {
        this.personalDetailsScore = value;
    }

    /**
     * Gets the value of the addressMatchResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PersonalDetailsVerificationType }{@code >}
     *     
     */
    public JAXBElement<PersonalDetailsVerificationType> getAddressMatchResult() {
        return addressMatchResult;
    }

    /**
     * Sets the value of the addressMatchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PersonalDetailsVerificationType }{@code >}
     *     
     */
    public void setAddressMatchResult(JAXBElement<PersonalDetailsVerificationType> value) {
        this.addressMatchResult = value;
    }

    /**
     * Gets the value of the cardValidationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CardValidationResponse }
     *     
     */
    public CardValidationResponse getCardValidationResponse() {
        return cardValidationResponse;
    }

    /**
     * Sets the value of the cardValidationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardValidationResponse }
     *     
     */
    public void setCardValidationResponse(CardValidationResponse value) {
        this.cardValidationResponse = value;
    }

    /**
     * Gets the value of the exceptionMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exceptionMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExceptionMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericCodeMessage }
     * 
     * 
     */
    public List<GenericCodeMessage> getExceptionMessage() {
        if (exceptionMessage == null) {
            exceptionMessage = new ArrayList<GenericCodeMessage>();
        }
        return this.exceptionMessage;
    }

}
