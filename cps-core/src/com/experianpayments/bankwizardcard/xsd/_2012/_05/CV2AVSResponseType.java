
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CV2AVSResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CV2AVSResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CV2Response" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AVSAddressResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AVSPostCodeResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CV2AVSResponseType", propOrder = {

})
public class CV2AVSResponseType {

    @XmlElement(name = "CV2Response")
    protected String cv2Response;
    @XmlElement(name = "AVSAddressResponse")
    protected String avsAddressResponse;
    @XmlElement(name = "AVSPostCodeResponse")
    protected String avsPostCodeResponse;

    /**
     * Gets the value of the cv2Response property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCV2Response() {
        return cv2Response;
    }

    /**
     * Sets the value of the cv2Response property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCV2Response(String value) {
        this.cv2Response = value;
    }

    /**
     * Gets the value of the avsAddressResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSAddressResponse() {
        return avsAddressResponse;
    }

    /**
     * Sets the value of the avsAddressResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSAddressResponse(String value) {
        this.avsAddressResponse = value;
    }

    /**
     * Gets the value of the avsPostCodeResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSPostCodeResponse() {
        return avsPostCodeResponse;
    }

    /**
     * Sets the value of the avsPostCodeResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSPostCodeResponse(String value) {
        this.avsPostCodeResponse = value;
    }

}
