
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressVerificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressVerificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="DeliveryAddressDistanceToIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrderAddressDistanceToIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressVerificationType", propOrder = {

})
public class AddressVerificationType {

    @XmlElement(name = "DeliveryAddressDistanceToIP", required = true)
    protected String deliveryAddressDistanceToIP;
    @XmlElement(name = "OrderAddressDistanceToIP", required = true)
    protected String orderAddressDistanceToIP;

    /**
     * Gets the value of the deliveryAddressDistanceToIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryAddressDistanceToIP() {
        return deliveryAddressDistanceToIP;
    }

    /**
     * Sets the value of the deliveryAddressDistanceToIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryAddressDistanceToIP(String value) {
        this.deliveryAddressDistanceToIP = value;
    }

    /**
     * Gets the value of the orderAddressDistanceToIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderAddressDistanceToIP() {
        return orderAddressDistanceToIP;
    }

    /**
     * Sets the value of the orderAddressDistanceToIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderAddressDistanceToIP(String value) {
        this.orderAddressDistanceToIP = value;
    }

}
