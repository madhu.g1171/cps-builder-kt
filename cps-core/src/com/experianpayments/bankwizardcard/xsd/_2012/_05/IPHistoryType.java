
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for IPHistoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPHistoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DaysCount7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DaysCount30" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MonthsCount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MonthsCount6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Transaction" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPHistoryType", propOrder = {
    "daysCount7",
    "daysCount30",
    "monthsCount3",
    "monthsCount6",
    "transaction"
})
public class IPHistoryType {

    @XmlElement(name = "DaysCount7", required = true)
    protected String daysCount7;
    @XmlElement(name = "DaysCount30", required = true)
    protected String daysCount30;
    @XmlElement(name = "MonthsCount3", required = true)
    protected String monthsCount3;
    @XmlElement(name = "MonthsCount6", required = true)
    protected String monthsCount6;
    @XmlElement(name = "Transaction", required = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> transaction;

    /**
     * Gets the value of the daysCount7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDaysCount7() {
        return daysCount7;
    }

    /**
     * Sets the value of the daysCount7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDaysCount7(String value) {
        this.daysCount7 = value;
    }

    /**
     * Gets the value of the daysCount30 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDaysCount30() {
        return daysCount30;
    }

    /**
     * Sets the value of the daysCount30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDaysCount30(String value) {
        this.daysCount30 = value;
    }

    /**
     * Gets the value of the monthsCount3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthsCount3() {
        return monthsCount3;
    }

    /**
     * Sets the value of the monthsCount3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthsCount3(String value) {
        this.monthsCount3 = value;
    }

    /**
     * Gets the value of the monthsCount6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthsCount6() {
        return monthsCount6;
    }

    /**
     * Sets the value of the monthsCount6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthsCount6(String value) {
        this.monthsCount6 = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getTransaction() {
        if (transaction == null) {
            transaction = new ArrayList<XMLGregorianCalendar>();
        }
        return this.transaction;
    }

}
