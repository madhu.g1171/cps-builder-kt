
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutcomeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OutcomeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Primary"/>
 *     &lt;enumeration value="Secondary"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OutcomeType")
@XmlEnum
public enum OutcomeType {


    /**
     * Primary outcome
     * 
     */
    @XmlEnumValue("Primary")
    PRIMARY("Primary"),

    /**
     * Secondary outcome
     * 
     */
    @XmlEnumValue("Secondary")
    SECONDARY("Secondary");
    private final String value;

    OutcomeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OutcomeType fromValue(String v) {
        for (OutcomeType c: OutcomeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
