
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReportSummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReportSummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="DatablocksSummary" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}ReportSummaryItemList"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportSummaryType", propOrder = {

})
public class ReportSummaryType {

    @XmlElement(name = "DatablocksSummary", required = true)
    protected ReportSummaryItemList datablocksSummary;

    /**
     * Gets the value of the datablocksSummary property.
     * 
     * @return
     *     possible object is
     *     {@link ReportSummaryItemList }
     *     
     */
    public ReportSummaryItemList getDatablocksSummary() {
        return datablocksSummary;
    }

    /**
     * Sets the value of the datablocksSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportSummaryItemList }
     *     
     */
    public void setDatablocksSummary(ReportSummaryItemList value) {
        this.datablocksSummary = value;
    }

}
