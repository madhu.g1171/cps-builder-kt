
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubPremise" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="Premise" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="SubStreet" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="Street" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="SubLocality" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="Locality" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="PostTown" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="Region" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="Postcode" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}StringWithMatch" minOccurs="0"/>
 *         &lt;element name="CountryCode" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}ThreeCharCode" minOccurs="0"/>
 *         &lt;element name="Latitude" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Longitude" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attribute name="Current">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="0"/>
 *             &lt;enumeration value="1"/>
 *             &lt;enumeration value="2"/>
 *             &lt;enumeration value="3"/>
 *             &lt;enumeration value=""/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="StartDay" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="StartMonth" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="StartYear" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="EndDay" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="EndMonth" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="EndYear" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="TimeAtAddress" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}YYMM" />
 *       &lt;attribute name="AddressType" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}ResidentialCommercial" />
 *       &lt;attribute name="Selected" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="ResidentialStatus" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}ResidentialStatus" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressResponse", propOrder = {

})
public class AddressResponse {

    @XmlElement(name = "AddressID")
    protected String addressID;
    @XmlElement(name = "SubPremise")
    protected StringWithMatch subPremise;
    @XmlElement(name = "Premise")
    protected StringWithMatch premise;
    @XmlElement(name = "SubStreet")
    protected StringWithMatch subStreet;
    @XmlElement(name = "Street")
    protected StringWithMatch street;
    @XmlElement(name = "SubLocality")
    protected StringWithMatch subLocality;
    @XmlElement(name = "Locality")
    protected StringWithMatch locality;
    @XmlElement(name = "PostTown")
    protected StringWithMatch postTown;
    @XmlElement(name = "Region")
    protected StringWithMatch region;
    @XmlElement(name = "Postcode")
    protected StringWithMatch postcode;
    @XmlElement(name = "CountryCode")
    protected String countryCode;
    @XmlElement(name = "Latitude")
    protected BigDecimal latitude;
    @XmlElement(name = "Longitude")
    protected BigDecimal longitude;
    @XmlAttribute(name = "Current")
    protected String current;
    @XmlAttribute(name = "StartDay")
    protected BigInteger startDay;
    @XmlAttribute(name = "StartMonth")
    protected BigInteger startMonth;
    @XmlAttribute(name = "StartYear")
    protected BigInteger startYear;
    @XmlAttribute(name = "EndDay")
    protected BigInteger endDay;
    @XmlAttribute(name = "EndMonth")
    protected BigInteger endMonth;
    @XmlAttribute(name = "EndYear")
    protected BigInteger endYear;
    @XmlAttribute(name = "TimeAtAddress")
    protected String timeAtAddress;
    @XmlAttribute(name = "AddressType")
    protected ResidentialCommercial addressType;
    @XmlAttribute(name = "Selected")
    protected Boolean selected;
    @XmlAttribute(name = "ResidentialStatus")
    protected ResidentialStatus residentialStatus;

    /**
     * Gets the value of the addressID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressID() {
        return addressID;
    }

    /**
     * Sets the value of the addressID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressID(String value) {
        this.addressID = value;
    }

    /**
     * Gets the value of the subPremise property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getSubPremise() {
        return subPremise;
    }

    /**
     * Sets the value of the subPremise property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setSubPremise(StringWithMatch value) {
        this.subPremise = value;
    }

    /**
     * Gets the value of the premise property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getPremise() {
        return premise;
    }

    /**
     * Sets the value of the premise property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setPremise(StringWithMatch value) {
        this.premise = value;
    }

    /**
     * Gets the value of the subStreet property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getSubStreet() {
        return subStreet;
    }

    /**
     * Sets the value of the subStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setSubStreet(StringWithMatch value) {
        this.subStreet = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setStreet(StringWithMatch value) {
        this.street = value;
    }

    /**
     * Gets the value of the subLocality property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getSubLocality() {
        return subLocality;
    }

    /**
     * Sets the value of the subLocality property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setSubLocality(StringWithMatch value) {
        this.subLocality = value;
    }

    /**
     * Gets the value of the locality property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getLocality() {
        return locality;
    }

    /**
     * Sets the value of the locality property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setLocality(StringWithMatch value) {
        this.locality = value;
    }

    /**
     * Gets the value of the postTown property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getPostTown() {
        return postTown;
    }

    /**
     * Sets the value of the postTown property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setPostTown(StringWithMatch value) {
        this.postTown = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setRegion(StringWithMatch value) {
        this.region = value;
    }

    /**
     * Gets the value of the postcode property.
     * 
     * @return
     *     possible object is
     *     {@link StringWithMatch }
     *     
     */
    public StringWithMatch getPostcode() {
        return postcode;
    }

    /**
     * Sets the value of the postcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringWithMatch }
     *     
     */
    public void setPostcode(StringWithMatch value) {
        this.postcode = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLatitude(BigDecimal value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLongitude(BigDecimal value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the current property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrent() {
        return current;
    }

    /**
     * Sets the value of the current property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrent(String value) {
        this.current = value;
    }

    /**
     * Gets the value of the startDay property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartDay() {
        return startDay;
    }

    /**
     * Sets the value of the startDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartDay(BigInteger value) {
        this.startDay = value;
    }

    /**
     * Gets the value of the startMonth property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartMonth() {
        return startMonth;
    }

    /**
     * Sets the value of the startMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartMonth(BigInteger value) {
        this.startMonth = value;
    }

    /**
     * Gets the value of the startYear property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartYear() {
        return startYear;
    }

    /**
     * Sets the value of the startYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartYear(BigInteger value) {
        this.startYear = value;
    }

    /**
     * Gets the value of the endDay property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEndDay() {
        return endDay;
    }

    /**
     * Sets the value of the endDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEndDay(BigInteger value) {
        this.endDay = value;
    }

    /**
     * Gets the value of the endMonth property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEndMonth() {
        return endMonth;
    }

    /**
     * Sets the value of the endMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEndMonth(BigInteger value) {
        this.endMonth = value;
    }

    /**
     * Gets the value of the endYear property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEndYear() {
        return endYear;
    }

    /**
     * Sets the value of the endYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEndYear(BigInteger value) {
        this.endYear = value;
    }

    /**
     * Gets the value of the timeAtAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeAtAddress() {
        return timeAtAddress;
    }

    /**
     * Sets the value of the timeAtAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeAtAddress(String value) {
        this.timeAtAddress = value;
    }

    /**
     * Gets the value of the addressType property.
     * 
     * @return
     *     possible object is
     *     {@link ResidentialCommercial }
     *     
     */
    public ResidentialCommercial getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResidentialCommercial }
     *     
     */
    public void setAddressType(ResidentialCommercial value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the selected property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSelected() {
        return selected;
    }

    /**
     * Sets the value of the selected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelected(Boolean value) {
        this.selected = value;
    }

    /**
     * Gets the value of the residentialStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ResidentialStatus }
     *     
     */
    public ResidentialStatus getResidentialStatus() {
        return residentialStatus;
    }

    /**
     * Sets the value of the residentialStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResidentialStatus }
     *     
     */
    public void setResidentialStatus(ResidentialStatus value) {
        this.residentialStatus = value;
    }

}
