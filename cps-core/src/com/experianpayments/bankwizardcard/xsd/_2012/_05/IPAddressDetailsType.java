
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IPAddressDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPAddressDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AreaCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeoConfidence" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="0"/>
 *               &lt;enumeration value="1"/>
 *               &lt;enumeration value="2"/>
 *               &lt;enumeration value="3"/>
 *               &lt;enumeration value="4"/>
 *               &lt;enumeration value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IPType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Latitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="Longitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="Postcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProxyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}CityType" minOccurs="0"/>
 *         &lt;element name="ConnectionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}CountryType" minOccurs="0"/>
 *         &lt;element name="Region" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}RegionType" minOccurs="0"/>
 *         &lt;element name="AddressVerification" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}AddressVerificationType" minOccurs="0"/>
 *         &lt;element name="TransactionHistory" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}IPHistoryType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPAddressDetailsType", propOrder = {

})
public class IPAddressDetailsType {

    @XmlElement(name = "AreaCode")
    protected String areaCode;
    @XmlElement(name = "GeoConfidence")
    protected String geoConfidence;
    @XmlElement(name = "IPType")
    protected String ipType;
    @XmlElement(name = "Latitude")
    protected Float latitude;
    @XmlElement(name = "Longitude")
    protected Float longitude;
    @XmlElement(name = "Postcode")
    protected String postcode;
    @XmlElement(name = "ProxyType")
    protected String proxyType;
    @XmlElement(name = "City")
    protected CityType city;
    @XmlElement(name = "ConnectionType")
    protected String connectionType;
    @XmlElement(name = "Country")
    protected CountryType country;
    @XmlElement(name = "Region")
    protected RegionType region;
    @XmlElement(name = "AddressVerification")
    protected AddressVerificationType addressVerification;
    @XmlElement(name = "TransactionHistory")
    protected IPHistoryType transactionHistory;

    /**
     * Gets the value of the areaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * Sets the value of the areaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaCode(String value) {
        this.areaCode = value;
    }

    /**
     * Gets the value of the geoConfidence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeoConfidence() {
        return geoConfidence;
    }

    /**
     * Sets the value of the geoConfidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeoConfidence(String value) {
        this.geoConfidence = value;
    }

    /**
     * Gets the value of the ipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPType() {
        return ipType;
    }

    /**
     * Sets the value of the ipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPType(String value) {
        this.ipType = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setLatitude(Float value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setLongitude(Float value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the postcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Sets the value of the postcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Gets the value of the proxyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxyType() {
        return proxyType;
    }

    /**
     * Sets the value of the proxyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxyType(String value) {
        this.proxyType = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link CityType }
     *     
     */
    public CityType getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link CityType }
     *     
     */
    public void setCity(CityType value) {
        this.city = value;
    }

    /**
     * Gets the value of the connectionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConnectionType() {
        return connectionType;
    }

    /**
     * Sets the value of the connectionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConnectionType(String value) {
        this.connectionType = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link CountryType }
     *     
     */
    public CountryType getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryType }
     *     
     */
    public void setCountry(CountryType value) {
        this.country = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link RegionType }
     *     
     */
    public RegionType getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionType }
     *     
     */
    public void setRegion(RegionType value) {
        this.region = value;
    }

    /**
     * Gets the value of the addressVerification property.
     * 
     * @return
     *     possible object is
     *     {@link AddressVerificationType }
     *     
     */
    public AddressVerificationType getAddressVerification() {
        return addressVerification;
    }

    /**
     * Sets the value of the addressVerification property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressVerificationType }
     *     
     */
    public void setAddressVerification(AddressVerificationType value) {
        this.addressVerification = value;
    }

    /**
     * Gets the value of the transactionHistory property.
     * 
     * @return
     *     possible object is
     *     {@link IPHistoryType }
     *     
     */
    public IPHistoryType getTransactionHistory() {
        return transactionHistory;
    }

    /**
     * Sets the value of the transactionHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link IPHistoryType }
     *     
     */
    public void setTransactionHistory(IPHistoryType value) {
        this.transactionHistory = value;
    }

}
