
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DatablockSummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DatablockSummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Decision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DecisionReasons" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DecisionReasonList" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="tmp" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatablockSummaryType", propOrder = {
    "decision",
    "decisionReasons"
})
public class DatablockSummaryType {

    @XmlElement(name = "Decision")
    protected String decision;
    @XmlElement(name = "DecisionReasons")
    protected List<DecisionReasonList> decisionReasons;
    @XmlAttribute(name = "tmp")
    protected String tmp;

    /**
     * Gets the value of the decision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets the value of the decision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecision(String value) {
        this.decision = value;
    }

    /**
     * Gets the value of the decisionReasons property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the decisionReasons property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDecisionReasons().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DecisionReasonList }
     * 
     * 
     */
    public List<DecisionReasonList> getDecisionReasons() {
        if (decisionReasons == null) {
            decisionReasons = new ArrayList<DecisionReasonList>();
        }
        return this.decisionReasons;
    }

    /**
     * Gets the value of the tmp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTmp() {
        return tmp;
    }

    /**
     * Sets the value of the tmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTmp(String value) {
        this.tmp = value;
    }

}
