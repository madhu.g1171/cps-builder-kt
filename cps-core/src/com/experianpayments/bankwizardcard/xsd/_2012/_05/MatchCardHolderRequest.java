
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizard.xsd._2009._07.Address;
import com.experianpayments.bankwizard.xsd._2009._07.PersonalDetails;
import com.experianpayments.bankwizardcard.xsd._2010._09.CardDetails;
import com.experianpayments.bankwizardcard.xsd._2010._09.GenericRequest;


/**
 * 
 *         Request for Match Card Holder Request
 *       
 * 
 * <p>Java class for MatchCardHolderRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MatchCardHolderRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericRequest">
 *       &lt;sequence>
 *         &lt;element name="CardDetails" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardDetails"/>
 *         &lt;element name="Personal" type="{http://experianpayments.com/bankwizard/xsd/2009/07}PersonalDetails"/>
 *         &lt;element name="Address" type="{http://experianpayments.com/bankwizard/xsd/2009/07}Address"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MatchCardHolderRequest", propOrder = {
    "cardDetails",
    "personal",
    "address"
})
public class MatchCardHolderRequest
    extends GenericRequest
{

    @XmlElement(name = "CardDetails", required = true)
    protected CardDetails cardDetails;
    @XmlElement(name = "Personal", required = true)
    protected PersonalDetails personal;
    @XmlElement(name = "Address", required = true)
    protected Address address;

    /**
     * Gets the value of the cardDetails property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetails }
     *     
     */
    public CardDetails getCardDetails() {
        return cardDetails;
    }

    /**
     * Sets the value of the cardDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetails }
     *     
     */
    public void setCardDetails(CardDetails value) {
        this.cardDetails = value;
    }

    /**
     * Gets the value of the personal property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetails }
     *     
     */
    public PersonalDetails getPersonal() {
        return personal;
    }

    /**
     * Sets the value of the personal property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetails }
     *     
     */
    public void setPersonal(PersonalDetails value) {
        this.personal = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

}
