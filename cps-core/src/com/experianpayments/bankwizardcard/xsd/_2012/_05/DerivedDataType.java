
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DerivedDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DerivedDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="IDConfirmationsAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="IDConfirmationsAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="DOBConfirmations" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="Age" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsBillPayerAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsBillPayerAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HouseholdOccupantsDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="HouseholdOccupantsDeliveryAddressLast2Years" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="HouseholdOccupantsBillingAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="HouseholdOccupantsBillingAddressLast2Years" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="IsRegisteredVoterAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsRegisteredVoterAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsBusinessPhoneNoProvided" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsCommercialActivityAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsCommercialActivityAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BillingAddressType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryAddressType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FullNameAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="FullNameAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="SHAatBillingAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SHAatDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BAIAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BAIAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MostRecentERYearAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MostRecentERYearAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ValidAddressBillingAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ValidAddressDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NoOfBAIAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="NoOfBAIAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="NoOfSHAAtBillingAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="NoOfSHAAtDeliveryAddress" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DerivedDataType", propOrder = {

})
public class DerivedDataType {

    @XmlElement(name = "IDConfirmationsAtBillingAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger idConfirmationsAtBillingAddress;
    @XmlElement(name = "IDConfirmationsAtDeliveryAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger idConfirmationsAtDeliveryAddress;
    @XmlElement(name = "DOBConfirmations")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger dobConfirmations;
    @XmlElement(name = "Age")
    protected String age;
    @XmlElement(name = "IsBillPayerAtBillingAddress")
    protected Boolean isBillPayerAtBillingAddress;
    @XmlElement(name = "IsBillPayerAtDeliveryAddress")
    protected Boolean isBillPayerAtDeliveryAddress;
    @XmlElement(name = "HouseholdOccupantsDeliveryAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger householdOccupantsDeliveryAddress;
    @XmlElement(name = "HouseholdOccupantsDeliveryAddressLast2Years")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger householdOccupantsDeliveryAddressLast2Years;
    @XmlElement(name = "HouseholdOccupantsBillingAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger householdOccupantsBillingAddress;
    @XmlElement(name = "HouseholdOccupantsBillingAddressLast2Years")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger householdOccupantsBillingAddressLast2Years;
    @XmlElement(name = "IsRegisteredVoterAtBillingAddress")
    protected Boolean isRegisteredVoterAtBillingAddress;
    @XmlElement(name = "IsRegisteredVoterAtDeliveryAddress")
    protected Boolean isRegisteredVoterAtDeliveryAddress;
    @XmlElement(name = "IsBusinessPhoneNoProvided")
    protected Boolean isBusinessPhoneNoProvided;
    @XmlElement(name = "IsCommercialActivityAtBillingAddress")
    protected Boolean isCommercialActivityAtBillingAddress;
    @XmlElement(name = "IsCommercialActivityAtDeliveryAddress")
    protected Boolean isCommercialActivityAtDeliveryAddress;
    @XmlElement(name = "BillingAddressType")
    protected String billingAddressType;
    @XmlElement(name = "DeliveryAddressType")
    protected String deliveryAddressType;
    @XmlElement(name = "FullNameAtBillingAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger fullNameAtBillingAddress;
    @XmlElement(name = "FullNameAtDeliveryAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger fullNameAtDeliveryAddress;
    @XmlElement(name = "SHAatBillingAddress")
    protected Boolean shAatBillingAddress;
    @XmlElement(name = "SHAatDeliveryAddress")
    protected Boolean shAatDeliveryAddress;
    @XmlElement(name = "BAIAtBillingAddress")
    protected Boolean baiAtBillingAddress;
    @XmlElement(name = "BAIAtDeliveryAddress")
    protected Boolean baiAtDeliveryAddress;
    @XmlElement(name = "MostRecentERYearAtBillingAddress")
    protected String mostRecentERYearAtBillingAddress;
    @XmlElement(name = "MostRecentERYearAtDeliveryAddress")
    protected String mostRecentERYearAtDeliveryAddress;
    @XmlElement(name = "Gender")
    protected String gender;
    @XmlElement(name = "ValidAddressBillingAddress")
    protected Boolean validAddressBillingAddress;
    @XmlElement(name = "ValidAddressDeliveryAddress")
    protected Boolean validAddressDeliveryAddress;
    @XmlElement(name = "NoOfBAIAtBillingAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger noOfBAIAtBillingAddress;
    @XmlElement(name = "NoOfBAIAtDeliveryAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger noOfBAIAtDeliveryAddress;
    @XmlElement(name = "NoOfSHAAtBillingAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger noOfSHAAtBillingAddress;
    @XmlElement(name = "NoOfSHAAtDeliveryAddress")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger noOfSHAAtDeliveryAddress;

    /**
     * Gets the value of the idConfirmationsAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIDConfirmationsAtBillingAddress() {
        return idConfirmationsAtBillingAddress;
    }

    /**
     * Sets the value of the idConfirmationsAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIDConfirmationsAtBillingAddress(BigInteger value) {
        this.idConfirmationsAtBillingAddress = value;
    }

    /**
     * Gets the value of the idConfirmationsAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIDConfirmationsAtDeliveryAddress() {
        return idConfirmationsAtDeliveryAddress;
    }

    /**
     * Sets the value of the idConfirmationsAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIDConfirmationsAtDeliveryAddress(BigInteger value) {
        this.idConfirmationsAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the dobConfirmations property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDOBConfirmations() {
        return dobConfirmations;
    }

    /**
     * Sets the value of the dobConfirmations property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDOBConfirmations(BigInteger value) {
        this.dobConfirmations = value;
    }

    /**
     * Gets the value of the age property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAge(String value) {
        this.age = value;
    }

    /**
     * Gets the value of the isBillPayerAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBillPayerAtBillingAddress() {
        return isBillPayerAtBillingAddress;
    }

    /**
     * Sets the value of the isBillPayerAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBillPayerAtBillingAddress(Boolean value) {
        this.isBillPayerAtBillingAddress = value;
    }

    /**
     * Gets the value of the isBillPayerAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBillPayerAtDeliveryAddress() {
        return isBillPayerAtDeliveryAddress;
    }

    /**
     * Sets the value of the isBillPayerAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBillPayerAtDeliveryAddress(Boolean value) {
        this.isBillPayerAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the householdOccupantsDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHouseholdOccupantsDeliveryAddress() {
        return householdOccupantsDeliveryAddress;
    }

    /**
     * Sets the value of the householdOccupantsDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHouseholdOccupantsDeliveryAddress(BigInteger value) {
        this.householdOccupantsDeliveryAddress = value;
    }

    /**
     * Gets the value of the householdOccupantsDeliveryAddressLast2Years property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHouseholdOccupantsDeliveryAddressLast2Years() {
        return householdOccupantsDeliveryAddressLast2Years;
    }

    /**
     * Sets the value of the householdOccupantsDeliveryAddressLast2Years property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHouseholdOccupantsDeliveryAddressLast2Years(BigInteger value) {
        this.householdOccupantsDeliveryAddressLast2Years = value;
    }

    /**
     * Gets the value of the householdOccupantsBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHouseholdOccupantsBillingAddress() {
        return householdOccupantsBillingAddress;
    }

    /**
     * Sets the value of the householdOccupantsBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHouseholdOccupantsBillingAddress(BigInteger value) {
        this.householdOccupantsBillingAddress = value;
    }

    /**
     * Gets the value of the householdOccupantsBillingAddressLast2Years property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHouseholdOccupantsBillingAddressLast2Years() {
        return householdOccupantsBillingAddressLast2Years;
    }

    /**
     * Sets the value of the householdOccupantsBillingAddressLast2Years property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHouseholdOccupantsBillingAddressLast2Years(BigInteger value) {
        this.householdOccupantsBillingAddressLast2Years = value;
    }

    /**
     * Gets the value of the isRegisteredVoterAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRegisteredVoterAtBillingAddress() {
        return isRegisteredVoterAtBillingAddress;
    }

    /**
     * Sets the value of the isRegisteredVoterAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRegisteredVoterAtBillingAddress(Boolean value) {
        this.isRegisteredVoterAtBillingAddress = value;
    }

    /**
     * Gets the value of the isRegisteredVoterAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRegisteredVoterAtDeliveryAddress() {
        return isRegisteredVoterAtDeliveryAddress;
    }

    /**
     * Sets the value of the isRegisteredVoterAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRegisteredVoterAtDeliveryAddress(Boolean value) {
        this.isRegisteredVoterAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the isBusinessPhoneNoProvided property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBusinessPhoneNoProvided() {
        return isBusinessPhoneNoProvided;
    }

    /**
     * Sets the value of the isBusinessPhoneNoProvided property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBusinessPhoneNoProvided(Boolean value) {
        this.isBusinessPhoneNoProvided = value;
    }

    /**
     * Gets the value of the isCommercialActivityAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCommercialActivityAtBillingAddress() {
        return isCommercialActivityAtBillingAddress;
    }

    /**
     * Sets the value of the isCommercialActivityAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCommercialActivityAtBillingAddress(Boolean value) {
        this.isCommercialActivityAtBillingAddress = value;
    }

    /**
     * Gets the value of the isCommercialActivityAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCommercialActivityAtDeliveryAddress() {
        return isCommercialActivityAtDeliveryAddress;
    }

    /**
     * Sets the value of the isCommercialActivityAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCommercialActivityAtDeliveryAddress(Boolean value) {
        this.isCommercialActivityAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the billingAddressType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAddressType() {
        return billingAddressType;
    }

    /**
     * Sets the value of the billingAddressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAddressType(String value) {
        this.billingAddressType = value;
    }

    /**
     * Gets the value of the deliveryAddressType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryAddressType() {
        return deliveryAddressType;
    }

    /**
     * Sets the value of the deliveryAddressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryAddressType(String value) {
        this.deliveryAddressType = value;
    }

    /**
     * Gets the value of the fullNameAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFullNameAtBillingAddress() {
        return fullNameAtBillingAddress;
    }

    /**
     * Sets the value of the fullNameAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFullNameAtBillingAddress(BigInteger value) {
        this.fullNameAtBillingAddress = value;
    }

    /**
     * Gets the value of the fullNameAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFullNameAtDeliveryAddress() {
        return fullNameAtDeliveryAddress;
    }

    /**
     * Sets the value of the fullNameAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFullNameAtDeliveryAddress(BigInteger value) {
        this.fullNameAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the shAatBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSHAatBillingAddress() {
        return shAatBillingAddress;
    }

    /**
     * Sets the value of the shAatBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSHAatBillingAddress(Boolean value) {
        this.shAatBillingAddress = value;
    }

    /**
     * Gets the value of the shAatDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSHAatDeliveryAddress() {
        return shAatDeliveryAddress;
    }

    /**
     * Sets the value of the shAatDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSHAatDeliveryAddress(Boolean value) {
        this.shAatDeliveryAddress = value;
    }

    /**
     * Gets the value of the baiAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBAIAtBillingAddress() {
        return baiAtBillingAddress;
    }

    /**
     * Sets the value of the baiAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBAIAtBillingAddress(Boolean value) {
        this.baiAtBillingAddress = value;
    }

    /**
     * Gets the value of the baiAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBAIAtDeliveryAddress() {
        return baiAtDeliveryAddress;
    }

    /**
     * Sets the value of the baiAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBAIAtDeliveryAddress(Boolean value) {
        this.baiAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the mostRecentERYearAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMostRecentERYearAtBillingAddress() {
        return mostRecentERYearAtBillingAddress;
    }

    /**
     * Sets the value of the mostRecentERYearAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMostRecentERYearAtBillingAddress(String value) {
        this.mostRecentERYearAtBillingAddress = value;
    }

    /**
     * Gets the value of the mostRecentERYearAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMostRecentERYearAtDeliveryAddress() {
        return mostRecentERYearAtDeliveryAddress;
    }

    /**
     * Sets the value of the mostRecentERYearAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMostRecentERYearAtDeliveryAddress(String value) {
        this.mostRecentERYearAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the validAddressBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isValidAddressBillingAddress() {
        return validAddressBillingAddress;
    }

    /**
     * Sets the value of the validAddressBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidAddressBillingAddress(Boolean value) {
        this.validAddressBillingAddress = value;
    }

    /**
     * Gets the value of the validAddressDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isValidAddressDeliveryAddress() {
        return validAddressDeliveryAddress;
    }

    /**
     * Sets the value of the validAddressDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidAddressDeliveryAddress(Boolean value) {
        this.validAddressDeliveryAddress = value;
    }

    /**
     * Gets the value of the noOfBAIAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoOfBAIAtBillingAddress() {
        return noOfBAIAtBillingAddress;
    }

    /**
     * Sets the value of the noOfBAIAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoOfBAIAtBillingAddress(BigInteger value) {
        this.noOfBAIAtBillingAddress = value;
    }

    /**
     * Gets the value of the noOfBAIAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoOfBAIAtDeliveryAddress() {
        return noOfBAIAtDeliveryAddress;
    }

    /**
     * Sets the value of the noOfBAIAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoOfBAIAtDeliveryAddress(BigInteger value) {
        this.noOfBAIAtDeliveryAddress = value;
    }

    /**
     * Gets the value of the noOfSHAAtBillingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoOfSHAAtBillingAddress() {
        return noOfSHAAtBillingAddress;
    }

    /**
     * Sets the value of the noOfSHAAtBillingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoOfSHAAtBillingAddress(BigInteger value) {
        this.noOfSHAAtBillingAddress = value;
    }

    /**
     * Gets the value of the noOfSHAAtDeliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoOfSHAAtDeliveryAddress() {
        return noOfSHAAtDeliveryAddress;
    }

    /**
     * Sets the value of the noOfSHAAtDeliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoOfSHAAtDeliveryAddress(BigInteger value) {
        this.noOfSHAAtDeliveryAddress = value;
    }

}
