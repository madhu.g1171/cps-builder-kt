
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BINDataBlockType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BINDataBlockType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Summary" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DatablockSummaryType" minOccurs="0"/>
 *         &lt;element name="BINDetails" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}BINType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DataBlockResultTypeGroup"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BINDataBlockType", propOrder = {
    "summary",
    "binDetails"
})
public class BINDataBlockType {

    @XmlElement(name = "Summary")
    protected DatablockSummaryType summary;
    @XmlElement(name = "BINDetails")
    protected BINType binDetails;
    @XmlAttribute(name = "Type")
    protected String type;

    /**
     * Gets the value of the summary property.
     * 
     * @return
     *     possible object is
     *     {@link DatablockSummaryType }
     *     
     */
    public DatablockSummaryType getSummary() {
        return summary;
    }

    /**
     * Sets the value of the summary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DatablockSummaryType }
     *     
     */
    public void setSummary(DatablockSummaryType value) {
        this.summary = value;
    }

    /**
     * Gets the value of the binDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BINType }
     *     
     */
    public BINType getBINDetails() {
        return binDetails;
    }

    /**
     * Sets the value of the binDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BINType }
     *     
     */
    public void setBINDetails(BINType value) {
        this.binDetails = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
