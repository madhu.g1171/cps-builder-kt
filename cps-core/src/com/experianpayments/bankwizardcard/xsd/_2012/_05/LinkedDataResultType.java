
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LinkedDataResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkedDataResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Summary" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DatablockSummaryType" minOccurs="0"/>
 *         &lt;element name="LinkedDataDetails" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}LinkedDataType" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DataBlockResultTypeGroup"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkedDataResultType", propOrder = {

})
public class LinkedDataResultType {

    @XmlElement(name = "Summary")
    protected DatablockSummaryType summary;
    @XmlElement(name = "LinkedDataDetails")
    protected LinkedDataType linkedDataDetails;
    @XmlAttribute(name = "Type")
    protected String type;

    /**
     * Gets the value of the summary property.
     * 
     * @return
     *     possible object is
     *     {@link DatablockSummaryType }
     *     
     */
    public DatablockSummaryType getSummary() {
        return summary;
    }

    /**
     * Sets the value of the summary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DatablockSummaryType }
     *     
     */
    public void setSummary(DatablockSummaryType value) {
        this.summary = value;
    }

    /**
     * Gets the value of the linkedDataDetails property.
     * 
     * @return
     *     possible object is
     *     {@link LinkedDataType }
     *     
     */
    public LinkedDataType getLinkedDataDetails() {
        return linkedDataDetails;
    }

    /**
     * Sets the value of the linkedDataDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkedDataType }
     *     
     */
    public void setLinkedDataDetails(LinkedDataType value) {
        this.linkedDataDetails = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
