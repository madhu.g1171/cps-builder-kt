
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CardDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CardDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PAN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CVV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssueNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValidFrom" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardDate" minOccurs="0"/>
 *         &lt;element name="ValidUntil" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardDate"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardDetails", propOrder = {
    "pan",
    "cvv",
    "issueNumber",
    "validFrom",
    "validUntil"
})
public class CardDetails {

    @XmlElement(name = "PAN", required = true)
    protected String pan;
    @XmlElementRef(name = "CVV", namespace = "http://experianpayments.com/bankwizardcard/xsd/2010/09", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cvv;
    @XmlElement(name = "IssueNumber", required = true, nillable = true)
    protected String issueNumber;
    @XmlElement(name = "ValidFrom")
    protected CardDate validFrom;
    @XmlElement(name = "ValidUntil", required = true)
    protected CardDate validUntil;

    /**
     * Gets the value of the pan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAN() {
        return pan;
    }

    /**
     * Sets the value of the pan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAN(String value) {
        this.pan = value;
    }

    /**
     * Gets the value of the cvv property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCVV() {
        return cvv;
    }

    /**
     * Sets the value of the cvv property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCVV(JAXBElement<String> value) {
        this.cvv = value;
    }

    /**
     * Gets the value of the issueNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueNumber() {
        return issueNumber;
    }

    /**
     * Sets the value of the issueNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueNumber(String value) {
        this.issueNumber = value;
    }

    /**
     * Gets the value of the validFrom property.
     * 
     * @return
     *     possible object is
     *     {@link CardDate }
     *     
     */
    public CardDate getValidFrom() {
        return validFrom;
    }

    /**
     * Sets the value of the validFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDate }
     *     
     */
    public void setValidFrom(CardDate value) {
        this.validFrom = value;
    }

    /**
     * Gets the value of the validUntil property.
     * 
     * @return
     *     possible object is
     *     {@link CardDate }
     *     
     */
    public CardDate getValidUntil() {
        return validUntil;
    }

    /**
     * Sets the value of the validUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDate }
     *     
     */
    public void setValidUntil(CardDate value) {
        this.validUntil = value;
    }

}
