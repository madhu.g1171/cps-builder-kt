
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MatchResult.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MatchResult">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BankMatch"/>
 *     &lt;enumeration value="BankGroupMatch"/>
 *     &lt;enumeration value="NoMatch"/>
 *     &lt;enumeration value="UnableToMatch"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MatchResult")
@XmlEnum
public enum MatchResult {

    @XmlEnumValue("BankMatch")
    BANK_MATCH("BankMatch"),
    @XmlEnumValue("BankGroupMatch")
    BANK_GROUP_MATCH("BankGroupMatch"),
    @XmlEnumValue("NoMatch")
    NO_MATCH("NoMatch"),
    @XmlEnumValue("UnableToMatch")
    UNABLE_TO_MATCH("UnableToMatch");
    private final String value;

    MatchResult(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MatchResult fromValue(String v) {
        for (MatchResult c: MatchResult.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
