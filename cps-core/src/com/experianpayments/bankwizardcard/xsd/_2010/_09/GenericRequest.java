
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardcard.xsd._2012._05.MatchCardHolderRequest;


/**
 * <p>Java class for GenericRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="ReportString" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ReportString" />
 *       &lt;attribute name="ItemisationID" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ItemisationID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericRequest")
@XmlSeeAlso({
    ValidateCardDetailsRequest.class,
    MatchDebitCardDetailsRequest.class,
    VerifyAvsCvvRequest.class,
    MatchCardHolderRequest.class
})
public abstract class GenericRequest {

    @XmlAttribute(name = "ReportString")
    protected String reportString;
    @XmlAttribute(name = "ItemisationID")
    protected String itemisationID;

    /**
     * Gets the value of the reportString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportString() {
        return reportString;
    }

    /**
     * Sets the value of the reportString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportString(String value) {
        this.reportString = value;
    }

    /**
     * Gets the value of the itemisationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemisationID() {
        return itemisationID;
    }

    /**
     * Sets the value of the itemisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemisationID(String value) {
        this.itemisationID = value;
    }

}
