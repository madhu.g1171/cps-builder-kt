
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizard.xsd._2009._07.Address;
import com.experianpayments.bankwizardcard.xsd._2012._05.VerifyAvsCvvVelocityRequest;


/**
 * 
 *         Request for an AVS CVV/CV2 check on the supplied card details.
 *         This is for future development.
 *       
 * 
 * <p>Java class for VerifyAvsCvvRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyAvsCvvRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericRequest">
 *       &lt;sequence>
 *         &lt;element name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CardDetails" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardDetails"/>
 *         &lt;element name="CardScheme" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}ExperianCardType" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://experianpayments.com/bankwizard/xsd/2009/07}Address"/>
 *         &lt;element name="CustomerPresent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyAvsCvvRequest", propOrder = {
    "nameOnCard",
    "cardDetails",
    "cardScheme",
    "address",
    "customerPresent"
})
@XmlSeeAlso({
    VerifyAvsCvvVelocityRequest.class
})
public class VerifyAvsCvvRequest
    extends GenericRequest
{

    @XmlElement(name = "NameOnCard", required = true)
    protected String nameOnCard;
    @XmlElement(name = "CardDetails", required = true)
    protected CardDetails cardDetails;
    @XmlElementRef(name = "CardScheme", namespace = "http://experianpayments.com/bankwizardcard/xsd/2010/09", type = JAXBElement.class, required = false)
    protected JAXBElement<ExperianCardType> cardScheme;
    @XmlElement(name = "Address", required = true)
    protected Address address;
    @XmlElement(name = "CustomerPresent", required = true, type = Boolean.class, nillable = true)
    protected Boolean customerPresent;

    /**
     * Gets the value of the nameOnCard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameOnCard() {
        return nameOnCard;
    }

    /**
     * Sets the value of the nameOnCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameOnCard(String value) {
        this.nameOnCard = value;
    }

    /**
     * Gets the value of the cardDetails property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetails }
     *     
     */
    public CardDetails getCardDetails() {
        return cardDetails;
    }

    /**
     * Sets the value of the cardDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetails }
     *     
     */
    public void setCardDetails(CardDetails value) {
        this.cardDetails = value;
    }

    /**
     * Gets the value of the cardScheme property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ExperianCardType }{@code >}
     *     
     */
    public JAXBElement<ExperianCardType> getCardScheme() {
        return cardScheme;
    }

    /**
     * Sets the value of the cardScheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ExperianCardType }{@code >}
     *     
     */
    public void setCardScheme(JAXBElement<ExperianCardType> value) {
        this.cardScheme = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the customerPresent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCustomerPresent() {
        return customerPresent;
    }

    /**
     * Sets the value of the customerPresent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustomerPresent(Boolean value) {
        this.customerPresent = value;
    }

}
