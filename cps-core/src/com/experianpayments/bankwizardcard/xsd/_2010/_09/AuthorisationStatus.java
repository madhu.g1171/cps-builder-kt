
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuthorisationStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AuthorisationStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Authorised"/>
 *     &lt;enumeration value="NotAuthorised"/>
 *     &lt;enumeration value="UnableToAuthorise"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AuthorisationStatus")
@XmlEnum
public enum AuthorisationStatus {

    @XmlEnumValue("Authorised")
    AUTHORISED("Authorised"),
    @XmlEnumValue("NotAuthorised")
    NOT_AUTHORISED("NotAuthorised"),
    @XmlEnumValue("UnableToAuthorise")
    UNABLE_TO_AUTHORISE("UnableToAuthorise");
    private final String value;

    AuthorisationStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuthorisationStatus fromValue(String v) {
        for (AuthorisationStatus c: AuthorisationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
