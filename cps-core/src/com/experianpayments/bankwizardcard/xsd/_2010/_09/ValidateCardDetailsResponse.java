
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to a request to validate card details.
 *         The 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bankwizardcard="http://experianpayments.com/bankwizardcard/xsd/2010/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ID&lt;/code&gt;
 * </pre>
 *  field is for auditing and tracking only.
 *       
 * 
 * <p>Java class for ValidateCardDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateCardDetailsResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericResponse">
 *       &lt;sequence>
 *         &lt;element name="CardValidationResponse" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardValidationResponse"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateCardDetailsResponse", propOrder = {
    "cardValidationResponse"
})
public class ValidateCardDetailsResponse
    extends GenericResponse
{

    @XmlElement(name = "CardValidationResponse", required = true)
    protected CardValidationResponse cardValidationResponse;

    /**
     * Gets the value of the cardValidationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CardValidationResponse }
     *     
     */
    public CardValidationResponse getCardValidationResponse() {
        return cardValidationResponse;
    }

    /**
     * Sets the value of the cardValidationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardValidationResponse }
     *     
     */
    public void setCardValidationResponse(CardValidationResponse value) {
        this.cardValidationResponse = value;
    }

}
