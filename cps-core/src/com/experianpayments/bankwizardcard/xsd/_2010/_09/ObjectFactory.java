
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizardcard.xsd._2010._09 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerifyAvsCvvRequestCardScheme_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2010/09", "CardScheme");
    private final static QName _CardDetailsCVV_QNAME = new QName("http://experianpayments.com/bankwizardcard/xsd/2010/09", "CVV");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizardcard.xsd._2010._09
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidateCardDetailsRequest }
     * 
     */
    public ValidateCardDetailsRequest createValidateCardDetailsRequest() {
        return new ValidateCardDetailsRequest();
    }

    /**
     * Create an instance of {@link MatchDebitCardDetailsResponse }
     * 
     */
    public MatchDebitCardDetailsResponse createMatchDebitCardDetailsResponse() {
        return new MatchDebitCardDetailsResponse();
    }

    /**
     * Create an instance of {@link MatchDebitCardDetailsRequest }
     * 
     */
    public MatchDebitCardDetailsRequest createMatchDebitCardDetailsRequest() {
        return new MatchDebitCardDetailsRequest();
    }

    /**
     * Create an instance of {@link ValidateCardDetailsResponse }
     * 
     */
    public ValidateCardDetailsResponse createValidateCardDetailsResponse() {
        return new ValidateCardDetailsResponse();
    }

    /**
     * Create an instance of {@link VerifyAvsCvvRequest }
     * 
     */
    public VerifyAvsCvvRequest createVerifyAvsCvvRequest() {
        return new VerifyAvsCvvRequest();
    }

    /**
     * Create an instance of {@link VerifyAvsCvvResponse }
     * 
     */
    public VerifyAvsCvvResponse createVerifyAvsCvvResponse() {
        return new VerifyAvsCvvResponse();
    }

    /**
     * Create an instance of {@link CardDate }
     * 
     */
    public CardDate createCardDate() {
        return new CardDate();
    }

    /**
     * Create an instance of {@link CardValidationResponse }
     * 
     */
    public CardValidationResponse createCardValidationResponse() {
        return new CardValidationResponse();
    }

    /**
     * Create an instance of {@link GenericCodeMessage }
     * 
     */
    public GenericCodeMessage createGenericCodeMessage() {
        return new GenericCodeMessage();
    }

    /**
     * Create an instance of {@link CardCondition }
     * 
     */
    public CardCondition createCardCondition() {
        return new CardCondition();
    }

    /**
     * Create an instance of {@link CardDetails }
     * 
     */
    public CardDetails createCardDetails() {
        return new CardDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExperianCardType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2010/09", name = "CardScheme", scope = VerifyAvsCvvRequest.class)
    public JAXBElement<ExperianCardType> createVerifyAvsCvvRequestCardScheme(ExperianCardType value) {
        return new JAXBElement<ExperianCardType>(_VerifyAvsCvvRequestCardScheme_QNAME, ExperianCardType.class, VerifyAvsCvvRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizardcard/xsd/2010/09", name = "CVV", scope = CardDetails.class)
    public JAXBElement<String> createCardDetailsCVV(String value) {
        return new JAXBElement<String>(_CardDetailsCVV_QNAME, String.class, CardDetails.class, value);
    }

}
