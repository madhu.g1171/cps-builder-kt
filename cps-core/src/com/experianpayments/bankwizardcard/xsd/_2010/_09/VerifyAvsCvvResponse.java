
package com.experianpayments.bankwizardcard.xsd._2010._09;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to an AVS CVV/CV2 check request.
 *         
 *         If the check could not be performed 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bankwizardcard="http://experianpayments.com/bankwizardcard/xsd/2010/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ValidationPerformed&lt;/code&gt;
 * </pre>
 * 
 *         is set to false. Try repeating the call after a short delay.
 *         The 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bankwizardcard="http://experianpayments.com/bankwizardcard/xsd/2010/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ID&lt;/code&gt;
 * </pre>
 *  field is for auditing and tracking only.
 *       
 * 
 * <p>Java class for VerifyAvsCvvResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyAvsCvvResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericResponse">
 *       &lt;sequence>
 *         &lt;element name="PreAuthorised" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}AuthorisationStatus"/>
 *         &lt;element name="AvsMatched" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CvvMatched" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PostcodeMatched" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CardValidationResponse" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardValidationResponse"/>
 *         &lt;element name="ExceptionMessage" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericCodeMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyAvsCvvResponse", propOrder = {
    "preAuthorised",
    "avsMatched",
    "cvvMatched",
    "postcodeMatched",
    "cardValidationResponse",
    "exceptionMessage"
})
public class VerifyAvsCvvResponse
    extends GenericResponse
{

    @XmlElement(name = "PreAuthorised", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected AuthorisationStatus preAuthorised;
    @XmlElement(name = "AvsMatched", required = true, type = Boolean.class, nillable = true)
    protected Boolean avsMatched;
    @XmlElement(name = "CvvMatched", required = true, type = Boolean.class, nillable = true)
    protected Boolean cvvMatched;
    @XmlElement(name = "PostcodeMatched", required = true, type = Boolean.class, nillable = true)
    protected Boolean postcodeMatched;
    @XmlElement(name = "CardValidationResponse", required = true)
    protected CardValidationResponse cardValidationResponse;
    @XmlElement(name = "ExceptionMessage")
    protected List<GenericCodeMessage> exceptionMessage;

    /**
     * Gets the value of the preAuthorised property.
     * 
     * @return
     *     possible object is
     *     {@link AuthorisationStatus }
     *     
     */
    public AuthorisationStatus getPreAuthorised() {
        return preAuthorised;
    }

    /**
     * Sets the value of the preAuthorised property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorisationStatus }
     *     
     */
    public void setPreAuthorised(AuthorisationStatus value) {
        this.preAuthorised = value;
    }

    /**
     * Gets the value of the avsMatched property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAvsMatched() {
        return avsMatched;
    }

    /**
     * Sets the value of the avsMatched property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAvsMatched(Boolean value) {
        this.avsMatched = value;
    }

    /**
     * Gets the value of the cvvMatched property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCvvMatched() {
        return cvvMatched;
    }

    /**
     * Sets the value of the cvvMatched property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCvvMatched(Boolean value) {
        this.cvvMatched = value;
    }

    /**
     * Gets the value of the postcodeMatched property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPostcodeMatched() {
        return postcodeMatched;
    }

    /**
     * Sets the value of the postcodeMatched property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPostcodeMatched(Boolean value) {
        this.postcodeMatched = value;
    }

    /**
     * Gets the value of the cardValidationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CardValidationResponse }
     *     
     */
    public CardValidationResponse getCardValidationResponse() {
        return cardValidationResponse;
    }

    /**
     * Sets the value of the cardValidationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardValidationResponse }
     *     
     */
    public void setCardValidationResponse(CardValidationResponse value) {
        this.cardValidationResponse = value;
    }

    /**
     * Gets the value of the exceptionMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exceptionMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExceptionMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericCodeMessage }
     * 
     * 
     */
    public List<GenericCodeMessage> getExceptionMessage() {
        if (exceptionMessage == null) {
            exceptionMessage = new ArrayList<GenericCodeMessage>();
        }
        return this.exceptionMessage;
    }

}
