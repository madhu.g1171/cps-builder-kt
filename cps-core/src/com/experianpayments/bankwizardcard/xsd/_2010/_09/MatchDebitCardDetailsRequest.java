
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to match a debit card to a bank account.
 *         The supplied sort code is not validated before it is matched. To validate the sort code, call the 
 *         standard Bank Wizaard Validate service.
 *       
 * 
 * <p>Java class for MatchDebitCardDetailsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MatchDebitCardDetailsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericRequest">
 *       &lt;sequence>
 *         &lt;element name="CardDetails" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardDetails"/>
 *         &lt;element name="SortCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MatchDebitCardDetailsRequest", propOrder = {
    "cardDetails",
    "sortCode"
})
public class MatchDebitCardDetailsRequest
    extends GenericRequest
{

    @XmlElement(name = "CardDetails", required = true)
    protected CardDetails cardDetails;
    @XmlElement(name = "SortCode", required = true)
    protected String sortCode;

    /**
     * Gets the value of the cardDetails property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetails }
     *     
     */
    public CardDetails getCardDetails() {
        return cardDetails;
    }

    /**
     * Sets the value of the cardDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetails }
     *     
     */
    public void setCardDetails(CardDetails value) {
        this.cardDetails = value;
    }

    /**
     * Gets the value of the sortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the value of the sortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortCode(String value) {
        this.sortCode = value;
    }

}
