
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubTypeOfCard.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SubTypeOfCard">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="Business"/>
 *     &lt;enumeration value="Corporate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SubTypeOfCard")
@XmlEnum
public enum SubTypeOfCard {

    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("Corporate")
    CORPORATE("Corporate");
    private final String value;

    SubTypeOfCard(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SubTypeOfCard fromValue(String v) {
        for (SubTypeOfCard c: SubTypeOfCard.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
