
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests validation of the supplied card details.
 *       
 * 
 * <p>Java class for ValidateCardDetailsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateCardDetailsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericRequest">
 *       &lt;sequence>
 *         &lt;element name="CardDetails" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardDetails"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateCardDetailsRequest", propOrder = {
    "cardDetails"
})
public class ValidateCardDetailsRequest
    extends GenericRequest
{

    @XmlElement(name = "CardDetails", required = true)
    protected CardDetails cardDetails;

    /**
     * Gets the value of the cardDetails property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetails }
     *     
     */
    public CardDetails getCardDetails() {
        return cardDetails;
    }

    /**
     * Sets the value of the cardDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetails }
     *     
     */
    public void setCardDetails(CardDetails value) {
        this.cardDetails = value;
    }

}
