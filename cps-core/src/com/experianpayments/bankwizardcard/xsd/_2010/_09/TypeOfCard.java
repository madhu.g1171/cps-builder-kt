
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeOfCard.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeOfCard">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CreditCharge"/>
 *     &lt;enumeration value="CreditCard"/>
 *     &lt;enumeration value="ChargeCard"/>
 *     &lt;enumeration value="DebitCard"/>
 *     &lt;enumeration value="PrepaidCard"/>
 *     &lt;enumeration value="AcquirerOnly"/>
 *     &lt;enumeration value="Clearing"/>
 *     &lt;enumeration value="StoreCard"/>
 *     &lt;enumeration value="AcquiringOnly"/>
 *     &lt;enumeration value="Rewards"/>
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="CashCard"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeOfCard")
@XmlEnum
public enum TypeOfCard {

    @XmlEnumValue("CreditCharge")
    CREDIT_CHARGE("CreditCharge"),
    @XmlEnumValue("CreditCard")
    CREDIT_CARD("CreditCard"),
    @XmlEnumValue("ChargeCard")
    CHARGE_CARD("ChargeCard"),
    @XmlEnumValue("DebitCard")
    DEBIT_CARD("DebitCard"),
    @XmlEnumValue("PrepaidCard")
    PREPAID_CARD("PrepaidCard"),
    @XmlEnumValue("AcquirerOnly")
    ACQUIRER_ONLY("AcquirerOnly"),
    @XmlEnumValue("Clearing")
    CLEARING("Clearing"),
    @XmlEnumValue("StoreCard")
    STORE_CARD("StoreCard"),
    @XmlEnumValue("AcquiringOnly")
    ACQUIRING_ONLY("AcquiringOnly"),
    @XmlEnumValue("Rewards")
    REWARDS("Rewards"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("CashCard")
    CASH_CARD("CashCard");
    private final String value;

    TypeOfCard(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeOfCard fromValue(String v) {
        for (TypeOfCard c: TypeOfCard.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
