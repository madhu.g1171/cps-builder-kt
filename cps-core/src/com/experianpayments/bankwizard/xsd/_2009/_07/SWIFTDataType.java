
package com.experianpayments.bankwizard.xsd._2009._07;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         SWIFT Data.
 *         For the main branch the 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;subBranchNumber&lt;/code&gt;
 * </pre>
 *  will be zero.
 *       
 * 
 * <p>Java class for SWIFTDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SWIFTDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="swiftBranchBIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chipsUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="swiftCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="swiftService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentRoutingBIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="institutionPOBoxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="institutionPOBoxPostcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="institutionPOBoxLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="institutionPOBoxCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="swiftExtraInformation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="subBranchNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SWIFTDataType", propOrder = {
    "swiftBranchBIC",
    "chipsUID",
    "swiftCategory",
    "swiftService",
    "paymentRoutingBIC",
    "institutionPOBoxNumber",
    "institutionPOBoxPostcode",
    "institutionPOBoxLocation",
    "institutionPOBoxCountry",
    "swiftExtraInformation"
})
public class SWIFTDataType {

    protected String swiftBranchBIC;
    protected String chipsUID;
    protected String swiftCategory;
    protected String swiftService;
    protected String paymentRoutingBIC;
    protected String institutionPOBoxNumber;
    protected String institutionPOBoxPostcode;
    protected String institutionPOBoxLocation;
    protected String institutionPOBoxCountry;
    protected String swiftExtraInformation;
    @XmlAttribute(name = "subBranchNumber")
    protected BigInteger subBranchNumber;

    /**
     * Gets the value of the swiftBranchBIC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwiftBranchBIC() {
        return swiftBranchBIC;
    }

    /**
     * Sets the value of the swiftBranchBIC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwiftBranchBIC(String value) {
        this.swiftBranchBIC = value;
    }

    /**
     * Gets the value of the chipsUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChipsUID() {
        return chipsUID;
    }

    /**
     * Sets the value of the chipsUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChipsUID(String value) {
        this.chipsUID = value;
    }

    /**
     * Gets the value of the swiftCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwiftCategory() {
        return swiftCategory;
    }

    /**
     * Sets the value of the swiftCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwiftCategory(String value) {
        this.swiftCategory = value;
    }

    /**
     * Gets the value of the swiftService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwiftService() {
        return swiftService;
    }

    /**
     * Sets the value of the swiftService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwiftService(String value) {
        this.swiftService = value;
    }

    /**
     * Gets the value of the paymentRoutingBIC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentRoutingBIC() {
        return paymentRoutingBIC;
    }

    /**
     * Sets the value of the paymentRoutingBIC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentRoutingBIC(String value) {
        this.paymentRoutingBIC = value;
    }

    /**
     * Gets the value of the institutionPOBoxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionPOBoxNumber() {
        return institutionPOBoxNumber;
    }

    /**
     * Sets the value of the institutionPOBoxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionPOBoxNumber(String value) {
        this.institutionPOBoxNumber = value;
    }

    /**
     * Gets the value of the institutionPOBoxPostcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionPOBoxPostcode() {
        return institutionPOBoxPostcode;
    }

    /**
     * Sets the value of the institutionPOBoxPostcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionPOBoxPostcode(String value) {
        this.institutionPOBoxPostcode = value;
    }

    /**
     * Gets the value of the institutionPOBoxLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionPOBoxLocation() {
        return institutionPOBoxLocation;
    }

    /**
     * Sets the value of the institutionPOBoxLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionPOBoxLocation(String value) {
        this.institutionPOBoxLocation = value;
    }

    /**
     * Gets the value of the institutionPOBoxCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionPOBoxCountry() {
        return institutionPOBoxCountry;
    }

    /**
     * Sets the value of the institutionPOBoxCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionPOBoxCountry(String value) {
        this.institutionPOBoxCountry = value;
    }

    /**
     * Gets the value of the swiftExtraInformation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwiftExtraInformation() {
        return swiftExtraInformation;
    }

    /**
     * Sets the value of the swiftExtraInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwiftExtraInformation(String value) {
        this.swiftExtraInformation = value;
    }

    /**
     * Gets the value of the subBranchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubBranchNumber() {
        return subBranchNumber;
    }

    /**
     * Sets the value of the subBranchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubBranchNumber(BigInteger value) {
        this.subBranchNumber = value;
    }

}
