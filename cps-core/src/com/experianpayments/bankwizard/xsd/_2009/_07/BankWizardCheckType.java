
package com.experianpayments.bankwizard.xsd._2009._07;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         The BBAN required to be passed in for a specific validation checking level.
 *         The element is made up of the following:
 *           * BBAN  : input BBAN details
 *           * checkingLevel : the validation checking level
 *           * description : the country modules description of the checking level
 *       
 * 
 * <p>Java class for BankWizardCheckType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankWizardCheckType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBAN" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BBANResponseType" maxOccurs="5"/>
 *       &lt;/sequence>
 *       &lt;attribute name="checkingLevel" type="{http://experianpayments.com/bankwizard/xsd/2009/07}CheckingLevel" />
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankWizardCheckType", propOrder = {
    "bban"
})
public class BankWizardCheckType {

    @XmlElement(name = "BBAN", required = true)
    protected List<BBANResponseType> bban;
    @XmlAttribute(name = "checkingLevel")
    protected CheckingLevel checkingLevel;
    @XmlAttribute(name = "description")
    protected String description;

    /**
     * Gets the value of the bban property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bban property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBBAN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BBANResponseType }
     * 
     * 
     */
    public List<BBANResponseType> getBBAN() {
        if (bban == null) {
            bban = new ArrayList<BBANResponseType>();
        }
        return this.bban;
    }

    /**
     * Gets the value of the checkingLevel property.
     * 
     * @return
     *     possible object is
     *     {@link CheckingLevel }
     *     
     */
    public CheckingLevel getCheckingLevel() {
        return checkingLevel;
    }

    /**
     * Sets the value of the checkingLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckingLevel }
     *     
     */
    public void setCheckingLevel(CheckingLevel value) {
        this.checkingLevel = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
