
package com.experianpayments.bankwizard.xsd._2009._07;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.experianpayments.bankwizard.common.xsd._2009._09.Conditions;


/**
 *
 *         Validation of a bank account response.
 *
 *         For a successful validation the
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;BBAN&lt;/code&gt;
 * </pre>
 * : transposed BBANs
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;IBAN&lt;/code&gt;
 * </pre>
 *
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;dataAccessKey&lt;/code&gt;
 * </pre>
 * : for accessing lookup functions and
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;conditions&lt;/code&gt;
 * </pre>
 * : BankWizard core conditions
 *          are returned.
 *
 * <p>Java class for ValidateResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ValidateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBAN" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BBANBaseType" maxOccurs="5" minOccurs="0"/>
 *         &lt;element name="IBAN" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}IBAN" minOccurs="0"/>
 *         &lt;element name="dataAccessKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conditions" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}Conditions" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateResponse", propOrder = {
    "bban",
    "iban",
    "dataAccessKey",
    "conditions"
})
@XmlRootElement(name="ValidateResponse", namespace="http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0")
public class ValidateResponse {

    @XmlElement(name = "BBAN")
    protected List<BBANBaseType> bban;
    @XmlElement(name = "IBAN")
    protected String iban;
    protected String dataAccessKey;
    protected Conditions conditions;

    /**
     * Gets the value of the bban property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bban property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBBAN().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BBANBaseType }
     *
     *
     */
    public List<BBANBaseType> getBBAN() {
        if (bban == null) {
            bban = new ArrayList<BBANBaseType>();
        }
        return this.bban;
    }

    /**
     * Gets the value of the iban property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Sets the value of the iban property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

    /**
     * Gets the value of the dataAccessKey property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDataAccessKey() {
        return dataAccessKey;
    }

    /**
     * Sets the value of the dataAccessKey property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDataAccessKey(String value) {
        this.dataAccessKey = value;
    }

    /**
     * Gets the value of the conditions property.
     *
     * @return
     *     possible object is
     *     {@link Conditions }
     *
     */
    public Conditions getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     *
     * @param value
     *     allowed object is
     *     {@link Conditions }
     *
     */
    public void setConditions(Conditions value) {
        this.conditions = value;
    }

}
