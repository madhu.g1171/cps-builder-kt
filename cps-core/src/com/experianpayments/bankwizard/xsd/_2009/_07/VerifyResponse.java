
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.experianpayments.bankwizard.common.xsd._2009._09.Conditions;


/**
 *
 *         Verification of a bank account with personal details response.
 *
 *         Fatal conditions will be returned as SOAP Exceptions.
 *
 *
 * <p>Java class for VerifyResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="VerifyResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountInformation" type="{http://experianpayments.com/bankwizard/xsd/2009/07}VerifiedAccountType"/>
 *         &lt;element name="personalInformation" type="{http://experianpayments.com/bankwizard/xsd/2009/07}VerifyPersonalResponseType" minOccurs="0"/>
 *         &lt;element name="companyInformation" type="{http://experianpayments.com/bankwizard/xsd/2009/07}VerifyCompanyResponseType" minOccurs="0"/>
 *         &lt;element name="conditions" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}Conditions" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyResponse", propOrder = {
    "accountInformation",
    "personalInformation",
    "companyInformation",
    "conditions"
})
@XmlRootElement(name="VerifyResponse", namespace="http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0")
public class VerifyResponse {

    @XmlElement(required = true)
    protected VerifiedAccountType accountInformation;
    protected VerifyPersonalResponseType personalInformation;
    protected VerifyCompanyResponseType companyInformation;
    protected Conditions conditions;

    /**
     * Gets the value of the accountInformation property.
     *
     * @return
     *     possible object is
     *     {@link VerifiedAccountType }
     *
     */
    public VerifiedAccountType getAccountInformation() {
        return accountInformation;
    }

    /**
     * Sets the value of the accountInformation property.
     *
     * @param value
     *     allowed object is
     *     {@link VerifiedAccountType }
     *
     */
    public void setAccountInformation(VerifiedAccountType value) {
        this.accountInformation = value;
    }

    /**
     * Gets the value of the personalInformation property.
     *
     * @return
     *     possible object is
     *     {@link VerifyPersonalResponseType }
     *
     */
    public VerifyPersonalResponseType getPersonalInformation() {
        return personalInformation;
    }

    /**
     * Sets the value of the personalInformation property.
     *
     * @param value
     *     allowed object is
     *     {@link VerifyPersonalResponseType }
     *
     */
    public void setPersonalInformation(VerifyPersonalResponseType value) {
        this.personalInformation = value;
    }

    /**
     * Gets the value of the companyInformation property.
     *
     * @return
     *     possible object is
     *     {@link VerifyCompanyResponseType }
     *
     */
    public VerifyCompanyResponseType getCompanyInformation() {
        return companyInformation;
    }

    /**
     * Sets the value of the companyInformation property.
     *
     * @param value
     *     allowed object is
     *     {@link VerifyCompanyResponseType }
     *
     */
    public void setCompanyInformation(VerifyCompanyResponseType value) {
        this.companyInformation = value;
    }

    /**
     * Gets the value of the conditions property.
     *
     * @return
     *     possible object is
     *     {@link Conditions }
     *
     */
    public Conditions getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     *
     * @param value
     *     allowed object is
     *     {@link Conditions }
     *
     */
    public void setConditions(Conditions value) {
        this.conditions = value;
    }

}
