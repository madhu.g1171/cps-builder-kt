
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * 
 *         Country element contains the country name  and the following attributes
 *           * ISO3166  : the 2-alpha 3166-1 ISO code for the country.
 *           * Licensed : boolean indication to identify the country module is licensed.
 *       
 * 
 * <p>Java class for CountryInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CountryInformation">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="ISOCountry" use="required" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ISO3166-1" />
 *       &lt;attribute name="licensed" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryInformation", propOrder = {
    "value"
})
public class CountryInformation {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "ISOCountry", required = true)
    protected String isoCountry;
    @XmlAttribute(name = "licensed", required = true)
    protected boolean licensed;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the isoCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOCountry() {
        return isoCountry;
    }

    /**
     * Sets the value of the isoCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOCountry(String value) {
        this.isoCountry = value;
    }

    /**
     * Gets the value of the licensed property.
     * 
     */
    public boolean isLicensed() {
        return licensed;
    }

    /**
     * Sets the value of the licensed property.
     * 
     */
    public void setLicensed(boolean value) {
        this.licensed = value;
    }

}
