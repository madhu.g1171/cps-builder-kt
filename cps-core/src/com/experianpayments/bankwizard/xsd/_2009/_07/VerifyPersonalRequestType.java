
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.OwnerType;


/**
 * 
 *         Verification personal details request element.
 *       
 * 
 * <p>Java class for VerifyPersonalRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyPersonalRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personal" type="{http://experianpayments.com/bankwizard/xsd/2009/07}PersonalDetails" minOccurs="0"/>
 *         &lt;element name="address" type="{http://experianpayments.com/bankwizard/xsd/2009/07}Address" minOccurs="0"/>
 *         &lt;element name="ownerType" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}OwnerType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyPersonalRequestType", propOrder = {
    "personal",
    "address",
    "ownerType"
})
public class VerifyPersonalRequestType {

    protected PersonalDetails personal;
    protected Address address;
    @XmlElementRef(name = "ownerType", namespace = "http://experianpayments.com/bankwizard/xsd/2009/07", type = JAXBElement.class, required = false)
    protected JAXBElement<OwnerType> ownerType;

    /**
     * Gets the value of the personal property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetails }
     *     
     */
    public PersonalDetails getPersonal() {
        return personal;
    }

    /**
     * Sets the value of the personal property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetails }
     *     
     */
    public void setPersonal(PersonalDetails value) {
        this.personal = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the ownerType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OwnerType }{@code >}
     *     
     */
    public JAXBElement<OwnerType> getOwnerType() {
        return ownerType;
    }

    /**
     * Sets the value of the ownerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OwnerType }{@code >}
     *     
     */
    public void setOwnerType(JAXBElement<OwnerType> value) {
        this.ownerType = value;
    }

}
