
package com.experianpayments.bankwizard.xsd._2009._07;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 *         Branch details response.
 *
 *         If the
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;returnSubBranches&lt;/code&gt;
 * </pre>
 *  was false or missing, only the main branch is returned and
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;subBranchesAvailable&lt;/code&gt;
 * </pre>
 *
 *         is set to indicate if sub-branches exist.
 *
 *         If the
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;returnSubBranches&lt;/code&gt;
 * </pre>
 *  was true, then sub-branches are also returned in the response as additional
 *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;branchData&lt;/code&gt;
 * </pre>
 *  elements.
 *
 *
 * <p>Java class for GetBranchDataResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GetBranchDataResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="branchData" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BranchDataType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="subBranchesAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetBranchDataResponse", propOrder = {
    "branchData"
})
@XmlRootElement(name="GetBranchDataResponse", namespace="http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0")
public class GetBranchDataResponse {

    protected List<BranchDataType> branchData;
    @XmlAttribute(name = "subBranchesAvailable")
    protected Boolean subBranchesAvailable;

    /**
     * Gets the value of the branchData property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the branchData property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBranchData().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BranchDataType }
     *
     *
     */
    public List<BranchDataType> getBranchData() {
        if (branchData == null) {
            branchData = new ArrayList<BranchDataType>();
        }
        return this.branchData;
    }

    /**
     * Gets the value of the subBranchesAvailable property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isSubBranchesAvailable() {
        return subBranchesAvailable;
    }

    /**
     * Sets the value of the subBranchesAvailable property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setSubBranchesAvailable(Boolean value) {
        this.subBranchesAvailable = value;
    }

}
