
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.AccountType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.CustomerAccountType;


/**
 * 
 *         The account information.
 *       
 * 
 * <p>Java class for AccountTypeInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountTypeInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountType" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}AccountType"/>
 *         &lt;element name="customerAccountType" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}CustomerAccountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountTypeInformation", propOrder = {
    "accountType",
    "customerAccountType"
})
public class AccountTypeInformation {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AccountType accountType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CustomerAccountType customerAccountType;

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link AccountType }
     *     
     */
    public AccountType getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountType }
     *     
     */
    public void setAccountType(AccountType value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the customerAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerAccountType }
     *     
     */
    public CustomerAccountType getCustomerAccountType() {
        return customerAccountType;
    }

    /**
     * Sets the value of the customerAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerAccountType }
     *     
     */
    public void setCustomerAccountType(CustomerAccountType value) {
        this.customerAccountType = value;
    }

}
