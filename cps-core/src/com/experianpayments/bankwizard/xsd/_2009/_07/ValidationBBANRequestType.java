
package com.experianpayments.bankwizard.xsd._2009._07;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ValidationRequestType&lt;/code&gt;
 * </pre>
 *  element, containing a 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;BBAN&lt;/code&gt;
 * </pre>
 *  element
 *         with 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ISOCountry&lt;/code&gt;
 * </pre>
 *  and 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;checkingLevel&lt;/code&gt;
 * </pre>
 *  attributes.
 *       
 * 
 * <p>Java class for ValidationBBANRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidationBBANRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBAN" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BBANBaseType" maxOccurs="5"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ISOCountry" use="required" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ISO3166-1" />
 *       &lt;attribute name="checkingLevel" use="required" type="{http://experianpayments.com/bankwizard/xsd/2009/07}CheckingLevel" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidationBBANRequestType", propOrder = {
    "bban"
})
@XmlSeeAlso({
    ValidateRequest.class
})
public class ValidationBBANRequestType {

    @XmlElement(name = "BBAN", required = true)
    protected List<BBANBaseType> bban;
    @XmlAttribute(name = "ISOCountry", required = true)
    protected String isoCountry;
    @XmlAttribute(name = "checkingLevel", required = true)
    protected CheckingLevel checkingLevel;

    /**
     * Gets the value of the bban property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bban property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBBAN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BBANBaseType }
     * 
     * 
     */
    public List<BBANBaseType> getBBAN() {
        if (bban == null) {
            bban = new ArrayList<BBANBaseType>();
        }
        return this.bban;
    }

    /**
     * Gets the value of the isoCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOCountry() {
        return isoCountry;
    }

    /**
     * Sets the value of the isoCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOCountry(String value) {
        this.isoCountry = value;
    }

    /**
     * Gets the value of the checkingLevel property.
     * 
     * @return
     *     possible object is
     *     {@link CheckingLevel }
     *     
     */
    public CheckingLevel getCheckingLevel() {
        return checkingLevel;
    }

    /**
     * Sets the value of the checkingLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckingLevel }
     *     
     */
    public void setCheckingLevel(CheckingLevel value) {
        this.checkingLevel = value;
    }

}
