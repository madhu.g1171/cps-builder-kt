
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GlobalSearchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GlobalSearchResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizard/xsd/2011/04}SearchResult">
 *       &lt;sequence>
 *         &lt;element name="SearchTerms" type="{http://experianpayments.com/bankwizard/xsd/2011/04}SearchTermList"/>
 *         &lt;element name="ResultsList" type="{http://experianpayments.com/bankwizard/xsd/2011/04}SearchResultList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GlobalSearchResponse", propOrder = {
    "searchTerms",
    "resultsList"
})
public class GlobalSearchResponse
    extends SearchResult
{

    @XmlElement(name = "SearchTerms", required = true)
    protected SearchTermList searchTerms;
    @XmlElement(name = "ResultsList")
    protected SearchResultList resultsList;

    /**
     * Gets the value of the searchTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SearchTermList }
     *     
     */
    public SearchTermList getSearchTerms() {
        return searchTerms;
    }

    /**
     * Sets the value of the searchTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchTermList }
     *     
     */
    public void setSearchTerms(SearchTermList value) {
        this.searchTerms = value;
    }

    /**
     * Gets the value of the resultsList property.
     * 
     * @return
     *     possible object is
     *     {@link SearchResultList }
     *     
     */
    public SearchResultList getResultsList() {
        return resultsList;
    }

    /**
     * Sets the value of the resultsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchResultList }
     *     
     */
    public void setResultsList(SearchResultList value) {
        this.resultsList = value;
    }

}
