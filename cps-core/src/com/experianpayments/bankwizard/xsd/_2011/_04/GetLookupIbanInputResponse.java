
package com.experianpayments.bankwizard.xsd._2011._04;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizard.xsd._2009._07.BBANResponseType;


/**
 * 
 *         The BBAN inputs for various Bank Wizard checking levels associated the requested ISO 3166-1 two-alpha country code.
 *       
 * 
 * <p>Java class for GetLookupIbanInputResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetLookupIbanInputResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LookupInputBBAN" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BBANResponseType" maxOccurs="5"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ISOCountry" use="required" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ISO3166-1" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetLookupIbanInputResponse", propOrder = {
    "lookupInputBBAN"
})
public class GetLookupIbanInputResponse {

    @XmlElement(name = "LookupInputBBAN", required = true)
    protected List<BBANResponseType> lookupInputBBAN;
    @XmlAttribute(name = "ISOCountry", required = true)
    protected String isoCountry;

    /**
     * Gets the value of the lookupInputBBAN property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lookupInputBBAN property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLookupInputBBAN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BBANResponseType }
     * 
     * 
     */
    public List<BBANResponseType> getLookupInputBBAN() {
        if (lookupInputBBAN == null) {
            lookupInputBBAN = new ArrayList<BBANResponseType>();
        }
        return this.lookupInputBBAN;
    }

    /**
     * Gets the value of the isoCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOCountry() {
        return isoCountry;
    }

    /**
     * Sets the value of the isoCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOCountry(String value) {
        this.isoCountry = value;
    }

}
