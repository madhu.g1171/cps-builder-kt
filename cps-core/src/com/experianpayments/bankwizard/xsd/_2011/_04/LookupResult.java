
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LookupResult.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LookupResult">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Match"/>
 *     &lt;enumeration value="No Match"/>
 *     &lt;enumeration value="Not Supported"/>
 *     &lt;enumeration value="Check Match"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LookupResult")
@XmlEnum
public enum LookupResult {

    @XmlEnumValue("Match")
    MATCH("Match"),
    @XmlEnumValue("No Match")
    NO_MATCH("No Match"),
    @XmlEnumValue("Not Supported")
    NOT_SUPPORTED("Not Supported"),
    @XmlEnumValue("Check Match")
    CHECK_MATCH("Check Match");
    private final String value;

    LookupResult(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LookupResult fromValue(String v) {
        for (LookupResult c: LookupResult.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
