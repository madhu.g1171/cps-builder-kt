
package com.experianpayments.bankwizard.xsd._2011._04;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizard.xsd._2009._07.BBANBaseType;


/**
 * 
 *         Results of an IBAN and BIC lookup.
 * 
 *         The following are returned:
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bw="http://experianpayments.com/bankwizard/xsd/2011/04" xmlns:bwc="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:bws="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;BBAN&lt;/code&gt;
 * </pre>
 * : transposed BBANs
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bw="http://experianpayments.com/bankwizard/xsd/2011/04" xmlns:bwc="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:bws="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;IBAN&lt;/code&gt;
 * </pre>
 * 
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bw="http://experianpayments.com/bankwizard/xsd/2011/04" xmlns:bwc="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:bws="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Routing BIC&lt;/code&gt;
 * </pre>
 * 
 *           *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bw="http://experianpayments.com/bankwizard/xsd/2011/04" xmlns:bwc="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:bws="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Result&lt;/code&gt;
 * </pre>
 * :
 *       
 * 
 * <p>Java class for LookupIbanResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LookupIbanResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBAN" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BBANBaseType" maxOccurs="4"/>
 *         &lt;element name="IBAN" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}IBAN" minOccurs="0"/>
 *         &lt;element name="RoutingBic" type="{http://experianpayments.com/bankwizard/xsd/2011/04}BIC" minOccurs="0"/>
 *         &lt;element name="Result" type="{http://experianpayments.com/bankwizard/xsd/2011/04}LookupResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LookupIbanResponse", propOrder = {
    "bban",
    "iban",
    "routingBic",
    "result"
})
public class LookupIbanResponse {

    @XmlElement(name = "BBAN", required = true)
    protected List<BBANBaseType> bban;
    @XmlElement(name = "IBAN")
    protected String iban;
    @XmlElement(name = "RoutingBic")
    protected String routingBic;
    @XmlElement(name = "Result", required = true)
    @XmlSchemaType(name = "token")
    protected LookupResult result;

    /**
     * Gets the value of the bban property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bban property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBBAN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BBANBaseType }
     * 
     * 
     */
    public List<BBANBaseType> getBBAN() {
        if (bban == null) {
            bban = new ArrayList<BBANBaseType>();
        }
        return this.bban;
    }

    /**
     * Gets the value of the iban property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Sets the value of the iban property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

    /**
     * Gets the value of the routingBic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoutingBic() {
        return routingBic;
    }

    /**
     * Sets the value of the routingBic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoutingBic(String value) {
        this.routingBic = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link LookupResult }
     *     
     */
    public LookupResult getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link LookupResult }
     *     
     */
    public void setResult(LookupResult value) {
        this.result = value;
    }

}
