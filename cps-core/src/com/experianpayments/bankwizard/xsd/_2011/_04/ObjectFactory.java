
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizard.xsd._2011._04 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizard.xsd._2011._04
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GlobalSearchRequest }
     * 
     */
    public GlobalSearchRequest createGlobalSearchRequest() {
        return new GlobalSearchRequest();
    }

    /**
     * Create an instance of {@link BranchSearchResults }
     * 
     */
    public BranchSearchResults createBranchSearchResults() {
        return new BranchSearchResults();
    }

    /**
     * Create an instance of {@link GlobalSearchResponse }
     * 
     */
    public GlobalSearchResponse createGlobalSearchResponse() {
        return new GlobalSearchResponse();
    }

    /**
     * Create an instance of {@link LookupIbanRequest }
     * 
     */
    public LookupIbanRequest createLookupIbanRequest() {
        return new LookupIbanRequest();
    }

    /**
     * Create an instance of {@link BranchSearch }
     * 
     */
    public BranchSearch createBranchSearch() {
        return new BranchSearch();
    }

    /**
     * Create an instance of {@link AccountInformation }
     * 
     */
    public AccountInformation createAccountInformation() {
        return new AccountInformation();
    }

    /**
     * Create an instance of {@link GetLookupIbanInputRequest }
     * 
     */
    public GetLookupIbanInputRequest createGetLookupIbanInputRequest() {
        return new GetLookupIbanInputRequest();
    }

    /**
     * Create an instance of {@link GetInputWithSearchRequest }
     * 
     */
    public GetInputWithSearchRequest createGetInputWithSearchRequest() {
        return new GetInputWithSearchRequest();
    }

    /**
     * Create an instance of {@link SwiftSearchResults }
     * 
     */
    public SwiftSearchResults createSwiftSearchResults() {
        return new SwiftSearchResults();
    }

    /**
     * Create an instance of {@link GetLookupIbanInputResponse }
     * 
     */
    public GetLookupIbanInputResponse createGetLookupIbanInputResponse() {
        return new GetLookupIbanInputResponse();
    }

    /**
     * Create an instance of {@link SwiftSearch }
     * 
     */
    public SwiftSearch createSwiftSearch() {
        return new SwiftSearch();
    }

    /**
     * Create an instance of {@link LookupIbanResponse }
     * 
     */
    public LookupIbanResponse createLookupIbanResponse() {
        return new LookupIbanResponse();
    }

    /**
     * Create an instance of {@link GetInputWithSearchResponse }
     * 
     */
    public GetInputWithSearchResponse createGetInputWithSearchResponse() {
        return new GetInputWithSearchResponse();
    }

    /**
     * Create an instance of {@link FieldElement }
     * 
     */
    public FieldElement createFieldElement() {
        return new FieldElement();
    }

    /**
     * Create an instance of {@link SearchTermList }
     * 
     */
    public SearchTermList createSearchTermList() {
        return new SearchTermList();
    }

    /**
     * Create an instance of {@link SearchResultList }
     * 
     */
    public SearchResultList createSearchResultList() {
        return new SearchResultList();
    }

    /**
     * Create an instance of {@link ResultFieldList }
     * 
     */
    public ResultFieldList createResultFieldList() {
        return new ResultFieldList();
    }

    /**
     * Create an instance of {@link SwiftData }
     * 
     */
    public SwiftData createSwiftData() {
        return new SwiftData();
    }

    /**
     * Create an instance of {@link CountryInputMetaData }
     * 
     */
    public CountryInputMetaData createCountryInputMetaData() {
        return new CountryInputMetaData();
    }

    /**
     * Create an instance of {@link CategoryElement }
     * 
     */
    public CategoryElement createCategoryElement() {
        return new CategoryElement();
    }

    /**
     * Create an instance of {@link SearchCategories }
     * 
     */
    public SearchCategories createSearchCategories() {
        return new SearchCategories();
    }

    /**
     * Create an instance of {@link BranchData }
     * 
     */
    public BranchData createBranchData() {
        return new BranchData();
    }

}
