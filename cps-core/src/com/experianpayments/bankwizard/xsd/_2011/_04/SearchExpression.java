
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for SearchExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchExpression">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="page" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="pageSize" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="reportString" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ReportString" />
 *       &lt;attribute name="itemisationID" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ItemisationID" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchExpression", propOrder = {
    "value"
})
@XmlSeeAlso({
    BranchSearch.class,
    SwiftSearch.class
})
public abstract class SearchExpression {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "page", required = true)
    protected int page;
    @XmlAttribute(name = "pageSize", required = true)
    protected int pageSize;
    @XmlAttribute(name = "reportString")
    protected String reportString;
    @XmlAttribute(name = "itemisationID")
    protected String itemisationID;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the page property.
     * 
     */
    public int getPage() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     */
    public void setPage(int value) {
        this.page = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     */
    public void setPageSize(int value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the reportString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportString() {
        return reportString;
    }

    /**
     * Sets the value of the reportString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportString(String value) {
        this.reportString = value;
    }

    /**
     * Gets the value of the itemisationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemisationID() {
        return itemisationID;
    }

    /**
     * Sets the value of the itemisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemisationID(String value) {
        this.itemisationID = value;
    }

}
