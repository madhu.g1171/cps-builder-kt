
package com.experianpayments.bankwizard.xsd._2011._04;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BranchSearchResults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BranchSearchResults">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizard/xsd/2011/04}SearchResult">
 *       &lt;sequence>
 *         &lt;element name="BranchData" type="{http://experianpayments.com/bankwizard/xsd/2011/04}BranchData" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BranchSearchResults", propOrder = {
    "branchData"
})
public class BranchSearchResults
    extends SearchResult
{

    @XmlElement(name = "BranchData")
    protected List<BranchData> branchData;

    /**
     * Gets the value of the branchData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the branchData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBranchData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BranchData }
     * 
     * 
     */
    public List<BranchData> getBranchData() {
        if (branchData == null) {
            branchData = new ArrayList<BranchData>();
        }
        return this.branchData;
    }

}
