
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BranchSearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BranchSearch">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://experianpayments.com/bankwizard/xsd/2011/04>SearchExpression">
 *       &lt;attribute name="country" use="required" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ISO3166-1" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BranchSearch")
public class BranchSearch
    extends SearchExpression
{

    @XmlAttribute(name = "country", required = true)
    protected String country;

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

}
