<%@include file="commonTagLibs.jspf"%>
<%@ page import="com.tui.uk.config.ConfReader"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="errorMessage" scope="request"  value='<%=request.getParameter("errorMsg") %>' />
<%--Referer need not be checked if the brand code parameter(b) is being set properly. --%>
<%
  String referer = request.getHeader("REFERER");
  if (referer != null)
  {
    pageContext.setAttribute("ref", referer);

    if(referer.indexOf("shop/byo") !=-1)
    {
      pageContext.setAttribute("byo", "true");
    }
    else if(referer.indexOf("shop/ao") !=-1)
    {
      pageContext.setAttribute("ao", "true");
    }
    else if(referer.indexOf("shop/cruise") !=-1)
    {
      pageContext.setAttribute("cruise", "true");
    }
    else if(referer.indexOf("brac") !=-1)
    {
      pageContext.setAttribute("brac", "true");
    }
    else if(referer.indexOf("/accom/page/ao/") !=-1)
    {
      pageContext.setAttribute("accom", "true");
    }
   else if(referer.indexOf("th/beach") !=-1)
    {
      pageContext.setAttribute("beach", "true");
    }
    else if(referer.indexOf("st/sun") !=-1)
    {
      pageContext.setAttribute("simply", "true");
    }
    else if(referer.indexOf("th/cruise") !=-1)
    {
      pageContext.setAttribute("cruise", "true");
    }
    else if(referer.indexOf("accom/page") !=-1)
    {
      pageContext.setAttribute("fcao", "true");
    }
    else if(referer.indexOf("flightextras") != -1)
    {
       pageContext.setAttribute("NEWSKIES", "true");
    }
  }

  if (request.getParameter("brand") != null)
  {
    pageContext.setAttribute("applicationName", request.getParameter("brand"));
  }
  else
  {
  String code = (String)request.getParameter("b");
    String bCode = (String) ConfReader.getConfEntry("brandcode." + code, "");
  pageContext.setAttribute("applicationName", bCode);
  }
%>

<c:set var="clientApp" value="${bookingComponent.clientApplication.clientApplicationName}"/>
<c:choose>
   <c:when test="${applicationName=='WiSHBYO' || clientApp=='WiSHBYO' || byo == 'true'}">
        <jsp:forward page="/shop/common/error.jsp?clientApplication=WiSHBYO"/>
   </c:when>
   <c:when test="${applicationName=='NEWSKIES' || clientApp=='NEWSKIES' || NEWSKIES == 'true'}">
        <jsp:forward page="/newskies/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='NEWSKIESM' || clientApp=='NEWSKIESM' || NEWSKIESM == 'true'}">
        <jsp:forward page="/newskiesm/error.jsp"/>
   </c:when>
    <c:when test="${applicationName=='TFly' || clientApp=='TFly' || tfly == 'true'}">
        <jsp:forward page="/tflyredesign/error.jsp?clientApplication=TFly"/>
   </c:when>
    <c:when test="${applicationName=='FCFO' || clientApp=='FCFO' || fcfo == 'true'}">
        <jsp:forward page="/fcfo/error.jsp?clientApplication=FCFO"/>
   </c:when>
    <c:when test="${applicationName=='Portland' || clientApp=='Portland' || portland == 'true'}">
        <jsp:forward page="/portland/error.jsp?clientApplication=Portland"/>
   </c:when>
   <c:when
        test="${applicationName=='MANAGEFIRSTCHOICE' || clientApp=='MANAGEFIRSTCHOICE' || MANAGEFIRSTCHOICE == 'true'}">
        <jsp:forward page="/managefc/error.jsp" />
    </c:when>
    <c:when
        test="${applicationName=='MANAGETHOMSON' || clientApp=='MANAGETHOMSON' || MANAGETHOMSON == 'true'}">
        <jsp:forward page="/manageth/error.jsp" />
    </c:when>
   <c:when test="${applicationName=='WiSHAO' || clientApp=='WiSHAO' || ao == 'true'}">
        <jsp:forward page="/shop/common/error.jsp?clientApplication=WiSHAO"/>
   </c:when>
   <c:when test="${applicationName=='Cruise' || clientApp=='Cruise' || cruise == 'true'}">
       <jsp:forward page="/shop/common/error.jsp?clientApplication=Cruise"/>
   </c:when>
   <c:when test="${applicationName=='BRACApplication' || clientApp=='BRACApplication' || brac=='true'}">
       <jsp:forward page="/brac/bracError.jsp?clientApplication=BRACApplication"/>
   </c:when>
  <c:when test="${applicationName=='HUGOBYO' || clientApp=='HUGOBYO'}">
       <jsp:forward page="/hugo/error.jsp?clientApplication=HUGOBYO"/>
   </c:when>
   <c:when test="${applicationName=='FALCONBYO' || clientApp=='FALCONBYO'}">
       <jsp:forward page="/falcon/error.jsp?clientApplication=FALCONBYO"/>
   </c:when>
   <c:when test="${applicationName=='ThomsonFHPI' || clientApp=='ThomsonFHPI'}">
       <jsp:forward page="/thomson/byoredesign/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='ThomsonBYO' || clientApp=='ThomsonBYO'}">
       <jsp:forward page="/thomson/byo/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='B2CShorex' || clientApp=='B2CShorex'}">
       <jsp:forward page="/shorex/b2c/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='B2BShorex' || clientApp=='B2BShorex'}">
       <jsp:forward page="/shorex/b2b/error.jsp"/>
   </c:when>

   <c:when test="${applicationName=='ThomsonAO' || clientApp=='ThomsonAO' || accom=='true'}">
       <%--TODO: following scriptlet will be removed once Retrofit goes live --%>

          <jsp:forward page="/thomson/thao/thaoError.jsp"/>
       </c:when>
   <c:when test="${applicationName=='ThomsonCruise' || clientApp=='ThomsonCruise'}">
       <jsp:forward page="/thomson/cruise/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='GREENFIELDCruise' || clientApp=='GREENFIELDCruise' || cruise=='true'}">
       <jsp:forward page="/greenfield/cruise/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='GREENFIELDBeach' || clientApp=='GREENFIELDBeach' || beach=='true'}">
       <jsp:forward page="/greenfield/beach/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='GREENFIELDSimply' || clientApp=='GREENFIELDSimply' || simply=='true'}">
       <jsp:forward page="/greenfield/simply/jsp/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='FCAO' || clientApp=='FCAO' || fcao=='true'}">
       <jsp:forward page="/fcao/fcaoError.jsp"/>
   </c:when>
    <c:when test="${applicationName=='WSS' || clientApp=='WSS' }">
       <jsp:forward page="/wss/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='Tracs' || clientApp=='Tracs' }">
       <jsp:forward page="/tracs/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='FCX' || clientApp=='FCX' }">
       <jsp:forward page="/fcx/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='THX' || clientApp=='THX' }">
       <jsp:forward page="/thx/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='FIRSTCHOICE' || clientApp=='FIRSTCHOICE'}">
       <jsp:forward page="/firstchoice/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='THOMSON' || clientApp=='THOMSON'}">
       <jsp:forward page="/thomson/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='HYBRISFALCON' || clientApp=='HYBRISFALCON'}">
       <jsp:forward page="/hybrisfalcon/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='HYBRISTHCRUISE' || clientApp=='HYBRISTHCRUISE'}">
       <jsp:forward page="/hybristhcruise/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='HYBRISTHFO' || clientApp=='HYBRISTHFO'}">
       <jsp:forward page="/hybristhfo/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='HYBRISFALCONFO' || clientApp=='HYBRISFALCONFO'}">
       <jsp:forward page="/falconfo/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='FALCONFOMOBILE' || clientApp=='FALCONFOMOBILE'}">
       <jsp:forward page="/mobilefalconfo/error.jsp"/>
   </c:when>
    <c:when test="${applicationName=='MFLIGHTONLY' || clientApp=='MFLIGHTONLY'}">
       <jsp:forward page="/mflightonly/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='THOMSONMOBILE' || clientApp=='THOMSONMOBILE'}">
       <jsp:forward page="/mthomson/error.jsp"/>
   </c:when>
    <c:when test="${applicationName=='FIRSTCHOICEMOBILE' || clientApp=='FIRSTCHOICEMOBILE'}">
       <jsp:forward page="/mfirstchoice/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='FALCONMOBILE' || clientApp=='FALCONMOBILE'}">
       <jsp:forward page="/mfalcon/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='ANCTHOMSON' || clientApp=='ANCTHOMSON'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>
    </c:when>
    <c:when test="${applicationName=='ANCFIRSTCHOICE' || clientApp=='ANCFIRSTCHOICE'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>>
    </c:when>
    <c:when test="${applicationName=='ANCTHFO' || clientApp=='ANCTHFO'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>
    </c:when>
    <c:when test="${applicationName=='ANCTHCRUISE' || clientApp=='ANCTHCRUISE'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>
    </c:when>
    <c:when test="${applicationName=='ANCFALCON' || clientApp=='ANCFALCON'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>
    </c:when>
     <c:when test="${applicationName=='ANCFJFO' || clientApp=='ANCFJFO'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>
    </c:when>
    <c:when test="${applicationName=='ANCFALCON' || clientApp=='ANCSKICS'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>
    </c:when>
    <c:when test="${applicationName=='ANCFALCON' || clientApp=='ANCSKIES'}">
        <jsp:forward page="/amendandcancel/error.jsp"/>
    </c:when>
    <c:when test="${applicationName=='CRUISEMOBILE' || clientApp=='CRUISEMOBILE'}">
       <jsp:forward page="/mcruise/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='TUITH' || clientApp=='TUITH'}">
       <jsp:forward page="/tuith/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='TUICS' || clientApp=='TUICS'}">
       <jsp:forward page="/tuicommon/error.jsp">
       	<jsp:param name="clientapp" value="TUICS"></jsp:param>
       	</jsp:forward>
   </c:when>
    <c:when test="${applicationName=='TUIES' || clientApp=='TUIES'}">
       <jsp:forward page="/tuicommon/error.jsp">
    	   <jsp:param name="clientapp" value="TUIES"></jsp:param>
    	</jsp:forward>
   </c:when>
   <c:when test="${applicationName=='TUIFC' || clientApp=='TUIFC'}">
       <jsp:forward page="/tuifc/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='TUIFALCON' || clientApp=='TUIFALCON'}">
       <jsp:forward page="/tuifalcon/error.jsp"/>
   </c:when>
    <c:when test="${applicationName=='TUITHCRUISE' || clientApp=='TUITHCRUISE'}">
       <jsp:forward page="/tuithcruise/error.jsp"/>
   </c:when>
    <c:when test="${applicationName=='TUICRUISEDEALS' || clientApp=='TUICRUISEDEALS'}">
       <jsp:forward page="/tuicruisedeals/error.jsp"/>
   </c:when>
    <c:when test="${applicationName=='TUIHMCRUISE' || clientApp=='TUIHMCRUISE'}">
       <jsp:forward page="/tuihmcruise/error.jsp"/>
   </c:when>
    <c:when test="${applicationName=='TUITHRIVERCRUISE' || clientApp=='TUITHRIVERCRUISE'}">
       <jsp:forward page="/tuithcruise/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='TUITHFO' || clientApp=='TUITHFO'}">
       <jsp:forward page="/tuithfo/error.jsp"/>
   </c:when>
   <c:when test="${applicationName=='TUIFALCONFO' || clientApp=='TUIFALCONFO'}">
       <jsp:forward page="/tuifalconfo/error.jsp"/>
   </c:when>
   <c:otherwise>
        <jsp:forward page="/common/cpsError.jsp"/>
   </c:otherwise>
</c:choose>
