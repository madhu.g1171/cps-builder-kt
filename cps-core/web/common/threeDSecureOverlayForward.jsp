<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="paymentUrl" value="${paymentUrl}" scope="session" />
<c:set var="MD" value="${MD}" scope="session" />
<c:set var="TermUrl" value="${TermUrl}" scope="session" />
<c:set var="PaReq" value="${PaReq}" scope="session" />
<c:set var="ACSURL" value="${ACSURL}" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <script type="text/javascript">

         function showErrorPage()
		 {
           var purl="<c:out value='${paymentUrl}'/>";
		   var mdUrl="<c:out value='${MD}'/>";
		    var termUrl="<c:out value='${TermUrl}'/>";
            var ACSURL="<c:out value='${ACSURL}'/>";
           if(purl==''||mdUrl==''||termUrl==''||ACSURL=='')
			 {
         
		  window.location.href="error.jsp?b=<c:out value='${param.b}'/>"
                return false;
			 }
			 else{
				 return true;
			 }
		 }
         function bankRedirect()
         {			 
			 if(showErrorPage())
			 {
           
	        document.bankform.target = "ACSframe";
	        document.bankform.submit();
			 }
         }		 
      </script>
   </head>

   <body onLoad="javascript:bankRedirect();">
		<div class="3ds-content">
		<form action="<c:out value='${ACSURL}'/>" method="post" name="bankform">
	     <input type="hidden" name="MD" value="<c:out value='${MD}'/>" />
	     <input type="hidden" name="TermUrl" value="<c:out value='${TermUrl}'/>" />
         <textarea name="PaReq" style="display:none"><c:out value='${PaReq}'/></textarea>
		</form>
		<%@ include file="/common/threeDSecureOverlayPage.jspf"%>
		</div>
   </body>
</html>