<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName}' />
<html>
	<head>
	<%-- <%@include file="javascript.jspf"%> --%>
		<style>
			.hccprocess {
				height: 300px;
				width: 300px;
				background-color: powderblue;
			}
			.hcc{
				color:red;
				font-size:50px;
			}
		</style>
		<script  type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
		<script type="text/javascript" src="/cms-cps/tuith/js/prototype.js"></script>
		<script>


	   function makePayment() {

		   var value = "${clientapp}";
		   var url = "";
		   if(value == "AtcomRes"){
		   url = "/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />"; 
		   } else if(value == "NEWSKIES" || value == "NEWSKIESM") {
		   url = "/cps/processPostPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />";
		   } 
		   else{
			   url = "/cps/processMobilePayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />";
		   }
			elementstring =  "&token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
			var url = "/cps/processMobilePayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />";
			elementstring = elementstring.replace("&title=&", "&title=abcd&");
			elementstring = elementstring.replace("payment_0_type=&", "");
			elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
			elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
			var request = new Ajax.Request(url, {
					method : "POST",
					parameters : elementstring,
					onSuccess : showThreeDOverlay
				});





			}

			function showThreeDOverlay(http_request) {    		
			if (http_request.readyState == 4) {
				if (http_request.status == 200) {
					result = http_request.responseText;
					if (result.indexOf("3dspage") == -1) {
						if (result.indexOf("ERROR_MESSAGE") == -1) {
							window.top.location.href= result;
							/*
							document.postPaymentForm.action = result;
							//document.CheckoutPaymentDetailsForm.reset();
							var csrfTokenVal = "<c:out value="${bookingComponent.nonPaymentData['csrfToken']}"/>";
							if(csrfTokenVal != '')
							{
							 var csrfParameter = document.createElement("input");
							 csrfParameter.type = "hidden";
							 csrfParameter.name = "CSRFToken";
							 csrfParameter.value = csrfTokenVal;
							 document.postPaymentForm.appendChild(csrfParameter);
							}

							var loadingScreens = jQuery("#loader-screens")[0];
								var loadingScreensSticky = jQuery("html")[0];
							loadingScreens.className += " show in";
					loadingScreensSticky.className += " modal-open";

					setTimeout(function(){ document.postPaymentForm.submit(); }, 500);
					
					*/

						} else if(result.indexOf("ERROR_MESSAGE") != -1){
							document.getElementById("hccError").innerHTML=result;
							jQuery('.lds-css').css("display", "none");
							jQuery('.text-c').css("display", "none");
						}else {
							window.location.href= result;

						}
					} else {
						//window.location.href = 	result;
						 window.top.location.href= result;

					}
				} else {
					document.getElementById('errorMsg').innerHTML = "<h2>Payment failed. Please try again.</h2>";
					jQuery(".ctnbutton").removeClass("disable");
				}
			}
			window.scrollTo(0, 0);
		}
		</script>
		<style type="text/css">
		
			.text-c{
				 text-align: center;
				 color: #4c4c4c;
				 font-family: 'tui-type-light',Arial,sans-serif;
			}
			@font-face {
			font-family: 'tui-type-light';
			font-style: normal;
			font-weight: 400;
			src: url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.eot'); /* IE9 Compat Modes */
			src: local('Open Sans Regular'), local('OpenSans-Regular'),
			url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
			url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.woff2') format('woff2'), /* Super Modern Browsers */
			url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.woff') format('woff'), /* Modern Browsers */
			url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.ttf') format('truetype'), /* Safari, Android, iOS */
			url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.svg#OpenSans') format('svg'); /* Legacy iOS */
		}
		
		</style>

	</head>

	<body onload="makePayment()">
		<%@include file="/tuicommon/tealium.jspf" %>
		<div class="lds-css ng-scope">
<div class="lds-spin" ><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
<style type="text/css">@keyframes lds-spin {
  0% {
    opacity: 1;
    -webkit-transform: scale(1.1, 1.1);
    transform: scale(1.1, 1.1);
  }
  100% {
    opacity: 0;
    -webkit-transform: scale(1, 1);
    transform: scale(1, 1);
  }
}
@-webkit-keyframes lds-spin {
  0% {
    opacity: 1;
    -webkit-transform: scale(1.1, 1.1);
    transform: scale(1.1, 1.1);
  }
  100% {
    opacity: 0;
    -webkit-transform: scale(1, 1);
    transform: scale(1, 1);
  }
}
.lds-spin {
  position: relative;
}
.lds-spin div > div {
  position: absolute;
  width: 13px;
  height: 13px;
  border-radius: 50%;
  background: #092a5e;
  -webkit-animation: lds-spin 1s linear infinite;
  animation: lds-spin 1s linear infinite;
}
.lds-spin div:nth-child(1) > div {
  left: 122px;
  top: 90px;
  -webkit-animation-delay: -0.875s;
  animation-delay: -0.875s;
}
.lds-spin > div:nth-child(1) {
  -webkit-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transform-origin: 132px 100px;
  transform-origin: 132px 100px;
}
.lds-spin div:nth-child(2) > div {
  left: 112.62741699200001px;
  top: 112.62741699200001px;
  -webkit-animation-delay: -0.75s;
  animation-delay: -0.75s;
}
.lds-spin > div:nth-child(2) {
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transform-origin: 122.62741699200001px 122.62741699200001px;
  transform-origin: 122.62741699200001px 122.62741699200001px;
}
.lds-spin div:nth-child(3) > div {
  left: 90px;
  top: 122px;
  -webkit-animation-delay: -0.625s;
  animation-delay: -0.625s;
}
.lds-spin > div:nth-child(3) {
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
  -webkit-transform-origin: 100px 132px;
  transform-origin: 100px 132px;
}
.lds-spin div:nth-child(4) > div {
  left: 67.37258300799999px;
  top: 112.62741699200001px;
  -webkit-animation-delay: -0.5s;
  animation-delay: -0.5s;
}
.lds-spin > div:nth-child(4) {
  -webkit-transform: rotate(135deg);
  transform: rotate(135deg);
  -webkit-transform-origin: 77.37258300799999px 122.62741699200001px;
  transform-origin: 77.37258300799999px 122.62741699200001px;
}
.lds-spin div:nth-child(5) > div {
  left: 58px;
  top: 90px;
  -webkit-animation-delay: -0.375s;
  animation-delay: -0.375s;
}
.lds-spin > div:nth-child(5) {
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
  -webkit-transform-origin: 68px 100px;
  transform-origin: 68px 100px;
}
.lds-spin div:nth-child(6) > div {
  left: 67.37258300799999px;
  top: 67.37258300799999px;
  -webkit-animation-delay: -0.25s;
  animation-delay: -0.25s;
}
.lds-spin > div:nth-child(6) {
  -webkit-transform: rotate(225deg);
  transform: rotate(225deg);
  -webkit-transform-origin: 77.37258300799999px 77.37258300799999px;
  transform-origin: 77.37258300799999px 77.37258300799999px;
}
.lds-spin div:nth-child(7) > div {
  left: 90px;
  top: 58px;
  -webkit-animation-delay: -0.125s;
  animation-delay: -0.125s;
}
.lds-spin > div:nth-child(7) {
  -webkit-transform: rotate(270deg);
  transform: rotate(270deg);
  -webkit-transform-origin: 100px 68px;
  transform-origin: 100px 68px;
}
.lds-spin div:nth-child(8) > div {
  left: 112.62741699200001px;
  top: 67.37258300799999px;
  -webkit-animation-delay: 0s;
  animation-delay: 0s;
}
.lds-spin > div:nth-child(8) {
  -webkit-transform: rotate(315deg);
  transform: rotate(315deg);
  -webkit-transform-origin: 122.62741699200001px 77.37258300799999px;
  transform-origin: 122.62741699200001px 77.37258300799999px;
}
.lds-spin div:nth-child(9) > div {
  left: 122px;
  top: 90px;
  -webkit-animation-delay: 0.125s;
  animation-delay: 0.125s;
}
.lds-spin > div:nth-child(9) {
  -webkit-transform: rotate(360deg);
  transform: rotate(360deg);
  -webkit-transform-origin: 132px 100px;
  transform-origin: 132px 100px;
}
.lds-spin {
  width: 200px;
  height: 200px;
  left: 50%;
  -webkit-transform: translate(-50%, 0) scale(1);
  transform: translate(-50%, 0) scale(1);
}
.hcc{
		font-size: 40px;
		color:red;
}
.title {
  text-align: center;
}
</style></div>
		<h3 class="text-c title" >Your payment is being processed. <br/>Please wait.</h3>
		<h2 id="hccError" class="hcc"></h2>
		<!--h2 class="text-c">Card details captured successfully... </h2-->
		<!-- <button style="background:rgb(130, 218, 15);" onclick="makePayment()"> BOOK THIS HOLIDAY </button>  -->



	</body>
</html>