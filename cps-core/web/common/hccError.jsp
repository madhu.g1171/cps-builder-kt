<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<c:set var="captureStatus" value="${request.captureStatus}" scope="request" />    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Hcc Error</title>
</head>
<body>
<c:choose>
	<c:when
		test="${request.captureStatus == 'pending'}">
		<h3>The cardholder has not yet completed the capture process</h3>
	</c:when>
	<c:when
		test="${request.captureStatus == 'purged_unused'}">
		<h3>The data captured during this session was not used to a payment before it expired, and has since been deleted</h3>
	</c:when>
	<c:otherwise>
		<title>The data captured during this session has already been used in a payment</title>
	</c:otherwise>
  </c:choose>

</body>
</html>