<%@include file="/common/commonTagLibs.jspf"%>
<html>
		<script type="text/javascript">
function bankRedirectToclient()
{
	document.beforeconfirm.target = "_parent";
	var csrfTokenVal = "<c:out value="${csrfTokenVal}"/>";
	if(csrfTokenVal != '')
	{
	   var csrfParameter = document.createElement("input");
       csrfParameter.type = "hidden";
       csrfParameter.name = "CSRFToken";
       csrfParameter.value = csrfTokenVal;
       document.beforeconfirm.appendChild(csrfParameter);
	}
	//  document.beforeconfirm.submit();

	setTimeout(function() {document.beforeconfirm.submit();},500);
}
</script>

<body onLoad="javascript:bankRedirectToclient();">

<c:choose>
   <c:when test="${fn:containsIgnoreCase(postPaymentUrl,'pageRender')}">
      <form name="beforeconfirm" method='get' action='<c:out value="${postPaymentUrl}"  escapeXml="false"/>'  target = "_parent">
   </c:when>
   <c:otherwise>
      <form name="beforeconfirm" method='post' action='<c:out value="${postPaymentUrl}"  escapeXml="false"/>' target = "_parent">
   </c:otherwise>
</c:choose>
        <div style="height:50px;font-weight:bold">
		  <br/><br/>

        <br/><br/>
		We are contacting your bank
        <br/><br/>
        Please Wait...	   </div>

          <noscript>
            <center>
               </br>
	           </br>
	           </br>
	           </br>
	           <input type="submit" value="Continue with payment" />
            </center>
         </noscript>
		<input type="hidden" name="token" value="<c:out value='${token}'/>">
		<input type="hidden" name="b" value="<c:out value='${param.b}'/>">
		<input type="hidden" name="tomcat" value="<c:out value='${param.tomcat}'/>">
		<span id="test" style="display:none;" ><c:out value='${bookingInfo.threeDAuth}'/></span>
		<input type="hidden" id="errorMsg" name="errorMsg" value="${errorMessage}">
</form>
</body>
</html>