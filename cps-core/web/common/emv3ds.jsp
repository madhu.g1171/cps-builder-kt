<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<%@ include file="/common/head.jspf"%>
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

<c:choose>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
		<%@include file="/amendandcancel/javascript.jspf" %>
		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/mobile.css" />
		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfalcon/css/ac.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfalcon/css/m.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfalcon/css/acss.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfalcon/css/base.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfalcon/css/base-new-th.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfalcon/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO'}">
		<%@include file="/amendandcancel/javascript.jspf" %>
		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/mobile.css" />
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/m.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/acss.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base-new-th.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/bf.css"/>
        <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/ac.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'FALCONFOMOBILE'}">
		<%@include file="/mobilefalconfo/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/mobilefalconfo/css/mobile.css" />
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/mobilefalconfo/css/m.css"/>
         <link rel="stylesheet" type="text/css" href="/cms-cps/mobilefalconfo/css/acss.css"/>
         <link rel="stylesheet" type="text/css" href="/cms-cps/mobilefalconfo/css/base.css"/>
         <link rel="stylesheet" type="text/css" href="/cms-cps/mobilefalconfo/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'FALCONMOBILE'}">
		<%@include file="/mfalcon/javascript.jspf" %>
		<link rel="stylesheet" href="/cms-cps/mfalcon/css/mobile.css" />
 		<link rel="stylesheet" type="text/css" href="/cms-cps/mfalcon/css/m.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/mfalcon/css/acss.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/mfalcon/css/base.css"/>
 		<link rel="stylesheet" type="text/css" href="/cms-cps/mfalcon/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUIFALCON'}">
		<%@include file="/tuifalcon/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuifalcon/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalcon/css/m.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalcon/css/acss.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalcon/css/base.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalcon/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUIFALCONFO'}">
		<%@include file="/tuifalconfo/javascript.jspf" %>
		  <link rel="stylesheet" href="/cms-cps/tuifalconfo/css/mobile.css" />
 		  <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/m.css"/>
 		  <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/acss.css"/>
 		  <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/base.css"/>
 		  <link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUITH'}">
		<%@include file="/tuith/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuith/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuith/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuith/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuith/css/bf.css"/>  
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUIFC'}">
		<%@include file="/tuifc/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuifc/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuifc/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuifc/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuifc/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'THOMSONMOBILE'}">
		<%@include file="/mthomson/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/mthomson/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mthomson/css/m.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mthomson/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mthomson/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mthomson/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'FIRSTCHOICEMOBILE'}">
		<%@include file="/mfirstchoice/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/mfirstchoice/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mfirstchoice/css/m.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mfirstchoice/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mfirstchoice/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mfirstchoice/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUITHFO'}">
		<%@include file="/tuithfo/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuithfo/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithfo/css/m.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithfo/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithfo/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithfo/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'MFLIGHTONLY'}">
		<%@include file="/mflightonly/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/mflightonly/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mflightonly/css/m.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mflightonly/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mflightonly/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/mflightonly/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUICRUISEDEALS'}">
		<%@include file="/tuicruisedeals/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuicruisedeals/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuicruisedeals/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuicruisedeals/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuicruisedeals/css/bf.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuicruisedeals/css/footer.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUITHCRUISE'}">
		<%@include file="/tuithcruise/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuithcruise/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithcruise/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithcruise/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithcruise/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUITHRIVERCRUISE'}">
		<%@include file="/tuithcruise/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuithcruise/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithcruise/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithcruise/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuithcruise/css/bf.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'TUIHMCRUISE'}">
		<%@include file="/tuihmcruise/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/tuihmcruise/css/mobile.css" />
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuihmcruise/css/acss.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuihmcruise/css/base.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuihmcruise/css/bf.css"/>
		 <link rel="stylesheet" type="text/css" href="/cms-cps/tuihmcruise/css/footer.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHOMSON'}">
		<%@include file="/amendandcancel/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/amendandcancel/css/mobile.css" />
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/m.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/acss.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base-new-th.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/bf.css"/>
         <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/ac.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
		<%@include file="/amendandcancel/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/mobile.css" />
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfirstchoice/css/m.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfirstchoice/css/acss.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfirstchoice/css/base.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfirstchoice/css/base-new-fc.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfirstchoice/css/bf.css"/>
         <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/ancfirstchoice/css/ac.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHFO'}">
		<%@include file="/amendandcancel/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/amendandcancel/css/mobile.css" />
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/m.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/acss.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base-new-th.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/bf.css"/>
         <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/ac.css"/>
	</c:when>
	<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHCRUISE'}">
		<%@include file="/amendandcancel/javascript.jspf" %>
		 <link rel="stylesheet" href="/cms-cps/amendandcancel/css/mobile.css" />
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/m.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/acss.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/base-new-th.css"/>
 		 <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/bf.css"/>
         <link rel="stylesheet" type="text/css" href="/cms-cps/amendandcancel/css/ac.css"/>
	</c:when>
	<c:otherwise>
		<%@include file="/tuifalconfo/javascript.jspf" %>
		<link rel="stylesheet" href="/cms-cps/tuifalconfo/css/mobile.css" />
		<link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/m.css" />
		<link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/acss.css" />
		<link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/base.css" />
		<link rel="stylesheet" type="text/css" href="/cms-cps/tuifalconfo/css/bf.css" />
	</c:otherwise>
</c:choose>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript"
	src="/cms-cps/common/js/commonAjaxCalls.js"></script>
<script type="text/javascript" src="/cms-cps/tuith/js/prototype.js"></script>
<style>
.hccprocess {
	height: 300px;
	width: 300px;
	background-color: powderblue;
}

.hcc {
	color: red;
	font-size: 50px;
}

.postEmv3dsHtml {
	max-width: 610px;
	margin: 0 auto;
	margin-bottom: 3rem;
}

.text-c {
	text-align: center;
	color: #4c4c4c;
	font-family: 'tui-type-light', Arial, sans-serif;
}

@font-face {
	font-family: 'tui-type-light';
	font-style: normal;
	font-weight: 400;
	src: url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.eot');
	/* IE9 Compat Modes */
	src: local('Open Sans Regular'), local('OpenSans-Regular'),
		url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.eot?#iefix')
		format('embedded-opentype'), /* IE6-IE8 */ 
			 url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.woff2')
		format('woff2'), /* Super Modern Browsers */ 
			 url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.woff')
		format('woff'), /* Modern Browsers */ 
			 url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.ttf')
		format('truetype'), /* Safari, Android, iOS */    
			
		
		
		
		url('/cms-cps/tuicommon/fonts/open-sans-v15-latin-regular.svg#OpenSans')
		format('svg'); /* Legacy iOS */
}

@media ( max-width :768px) {
	#postEmv3dsHtml iframe {
		width: 100vw !important;
	}
}
</style>

<script type="text/javascript">
	function renderEMV3DSHTML() {
		var waitTime = ${waitTime};
		setTimeout(
				function() {
					url = "/cps/processauthentication?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
					elementstring = "&colour_depth=" + screen.colorDepth
							+ "&java_enabled=" + navigator.javaEnabled()
							+ "&screen_height=" + screen.height
							+ "&screen_width=" + screen.width + "&time_zone="
							+ new Date().getTimezoneOffset() + "";
					var request = new Ajax.Request(url, {
						method : "POST",
						parameters : elementstring,
						onSuccess : displayEmv3dsPage
					});
				}, waitTime);
	}

	function insertAndExecute(id, text) {
		document.getElementById(id).innerHTML = text;
		var scripts = Array.prototype.slice.call(document.getElementById(id).getElementsByTagName("script"));
		for (var i = 0; i < scripts.length; i++) {
			if (scripts[i].src != "") {
			var tag = document.createElement("script");
			tag.src = scripts[i].src;
			document.getElementsByTagName("head")[0].appendChild(tag);
			}
			else {
				eval(scripts[i].innerHTML);
			}
		}
	}
	
	function displayEmv3dsPage(http_request) {
		if (http_request.readyState == 4) {
			if (http_request.status == 200) {
				result = http_request.responseText;
				 if(result.indexOf("paymentfailure") != -1){
					window.top.location.href = result;		
				}else if (result.indexOf("ERROR_MESSAGE") == -1) {
					if(typeof window.Range != 'undefined' && typeof Range.prototype.createContextualFragment == 'function'){
						var slotHtml = document.createRange().createContextualFragment(
							result);
						document.getElementById("postEmv3dsHtml").innerHTML = '';
					    document.getElementById("postEmv3dsHtml").appendChild(
							slotHtml);
						}else{
							//IE 9 & below
						  insertAndExecute("postEmv3dsHtml", result);
						}

					jQuery('.lds-css').css("display", "none");
					jQuery('.text-c').css("display", "none");
					jQuery('#postEmv3dsHtml').css("display", "block");
					jQuery('#hccError').css("display", "none");
				} else if (result.indexOf("ERROR_MESSAGE") != -1) {
					document.getElementById("hccError").innerHTML = result;
					jQuery('#hccError').css("display", "block");
					jQuery('#hccError').css("margin-bottom", "40px");
					jQuery('.lds-css').css("display", "none");
					jQuery('.text-c').css("display", "none");
					jQuery('#postEmv3dsHtml').css("display", "none");
				}
			}
		}
		window.scrollTo(0, 0);
	}
</script>
<style type="text/css">@keyframes lds-spin {
  0% {
    opacity: 1;
    -webkit-transform: scale(1.1, 1.1);
    transform: scale(1.1, 1.1);
  }
  100% {
    opacity: 0;
    -webkit-transform: scale(1, 1);
    transform: scale(1, 1);
  }
}
@-webkit-keyframes lds-spin {
  0% {
    opacity: 1;
    -webkit-transform: scale(1.1, 1.1);
    transform: scale(1.1, 1.1);
  }
  100% {
    opacity: 0;
    -webkit-transform: scale(1, 1);
    transform: scale(1, 1);
  }
}
.lds-spin {
  position: relative;
}
.lds-spin div > div {
  position: absolute;
  width: 13px;
  height: 13px;
  border-radius: 50%;
  background: #092a5e;
  -webkit-animation: lds-spin 1s linear infinite;
  animation: lds-spin 1s linear infinite;
}
.lds-spin div:nth-child(1) > div {
  left: 122px;
  top: 90px;
  -webkit-animation-delay: -0.875s;
  animation-delay: -0.875s;
}
.lds-spin > div:nth-child(1) {
  -webkit-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transform-origin: 132px 100px;
  transform-origin: 132px 100px;
}
.lds-spin div:nth-child(2) > div {
  left: 112.62741699200001px;
  top: 112.62741699200001px;
  -webkit-animation-delay: -0.75s;
  animation-delay: -0.75s;
}
.lds-spin > div:nth-child(2) {
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transform-origin: 122.62741699200001px 122.62741699200001px;
  transform-origin: 122.62741699200001px 122.62741699200001px;
}
.lds-spin div:nth-child(3) > div {
  left: 90px;
  top: 122px;
  -webkit-animation-delay: -0.625s;
  animation-delay: -0.625s;
}
.lds-spin > div:nth-child(3) {
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
  -webkit-transform-origin: 100px 132px;
  transform-origin: 100px 132px;
}
.lds-spin div:nth-child(4) > div {
  left: 67.37258300799999px;
  top: 112.62741699200001px;
  -webkit-animation-delay: -0.5s;
  animation-delay: -0.5s;
}
.lds-spin > div:nth-child(4) {
  -webkit-transform: rotate(135deg);
  transform: rotate(135deg);
  -webkit-transform-origin: 77.37258300799999px 122.62741699200001px;
  transform-origin: 77.37258300799999px 122.62741699200001px;
}
.lds-spin div:nth-child(5) > div {
  left: 58px;
  top: 90px;
  -webkit-animation-delay: -0.375s;
  animation-delay: -0.375s;
}
.lds-spin > div:nth-child(5) {
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
  -webkit-transform-origin: 68px 100px;
  transform-origin: 68px 100px;
}
.lds-spin div:nth-child(6) > div {
  left: 67.37258300799999px;
  top: 67.37258300799999px;
  -webkit-animation-delay: -0.25s;
  animation-delay: -0.25s;
}
.lds-spin > div:nth-child(6) {
  -webkit-transform: rotate(225deg);
  transform: rotate(225deg);
  -webkit-transform-origin: 77.37258300799999px 77.37258300799999px;
  transform-origin: 77.37258300799999px 77.37258300799999px;
}
.lds-spin div:nth-child(7) > div {
  left: 90px;
  top: 58px;
  -webkit-animation-delay: -0.125s;
  animation-delay: -0.125s;
}
.lds-spin > div:nth-child(7) {
  -webkit-transform: rotate(270deg);
  transform: rotate(270deg);
  -webkit-transform-origin: 100px 68px;
  transform-origin: 100px 68px;
}
.lds-spin div:nth-child(8) > div {
  left: 112.62741699200001px;
  top: 67.37258300799999px;
  -webkit-animation-delay: 0s;
  animation-delay: 0s;
}
.lds-spin > div:nth-child(8) {
  -webkit-transform: rotate(315deg);
  transform: rotate(315deg);
  -webkit-transform-origin: 122.62741699200001px 77.37258300799999px;
  transform-origin: 122.62741699200001px 77.37258300799999px;
}
.lds-spin div:nth-child(9) > div {
  left: 122px;
  top: 90px;
  -webkit-animation-delay: 0.125s;
  animation-delay: 0.125s;
}
.lds-spin > div:nth-child(9) {
  -webkit-transform: rotate(360deg);
  transform: rotate(360deg);
  -webkit-transform-origin: 132px 100px;
  transform-origin: 132px 100px;
}
.lds-spin {
  width: 200px;
  height: 200px;
  left: 50%;
  -webkit-transform: translate(-50%, 0) scale(1);
  transform: translate(-50%, 0) scale(1);
}
.hcc{
		font-size: 40px;
		color:red;
}
.title {
  text-align: center;
}
</style>
<!-- COPAYS 15 - CODE ADDED FOR BREADCRUMB ISSUE IN MOBILE VIEW  -->
	<script src="/cms-cps/tuicommon/js/iscroll-lite.js"
		type="text/javascript"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			intiateIscroll(jQuery(".scroll"));
		});
		window.addEventListener("resize", function() {
			var scrollDiv = jQuery('#breadcrumb');
			setTimeout(function() {
				scrollintoView(scrollDiv)
			}, 200);
		}, false);

		(function() {
			var userAgent = window.navigator.userAgent;
			var msie = userAgent.indexOf("MSIE ");

			if (msie > 0) {
				jQuery("html").addClass("ie");
			}
		})();
	</script>
</head>

<body onload="renderEMV3DSHTML()">
	<%@include file="/tuicommon/tealium.jspf" %>
	<div id="page" class="c" style="overflow:scroll;height: auto;">
		<%@ include file="/common/emv3dsHeaderPage.jspf"%>
		<br /><br />
		<h3 class="text-c title">
			Your payment is being processed. <br />Please wait.
		</h3>
		<h2 id="hccError" class="hcc" style="display: none;"></h2>
		<div class="lds-css ng-scope">
			<div class="lds-spin">
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
			</div>
		</div>	

		<div id="postEmv3dsHtml" class="postEmv3dsHtml" style="display: none;"></div>
		
		<%@ include file="/common/emv3dsFooterPage.jspf"%>
	</div>
	<div id="emv3dsHTML">
		<c:out value="${iaHtmlContent}" escapeXml="false" />
	</div>
</body>
</html>