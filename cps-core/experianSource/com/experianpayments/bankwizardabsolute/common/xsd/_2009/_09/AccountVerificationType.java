
package com.experianpayments.bankwizardabsolute.common.xsd._2009._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountVerificationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountVerificationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Match"/>
 *     &lt;enumeration value="No Match"/>
 *     &lt;enumeration value="Unable to check"/>
 *     &lt;enumeration value="Insufficient details to check"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AccountVerificationType")
@XmlEnum
public enum AccountVerificationType {

    @XmlEnumValue("Match")
    MATCH("Match"),
    @XmlEnumValue("No Match")
    NO_MATCH("No Match"),
    @XmlEnumValue("Unable to check")
    UNABLE_TO_CHECK("Unable to check"),
    @XmlEnumValue("Insufficient details to check")
    INSUFFICIENT_DETAILS_TO_CHECK("Insufficient details to check");
    private final String value;

    AccountVerificationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountVerificationType fromValue(String v) {
        for (AccountVerificationType c: AccountVerificationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
