
package com.experianpayments.bankwizardabsolute.common.xsd._2009._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OwnerType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OwnerType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Joint"/>
 *     &lt;enumeration value="Single"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OwnerType")
@XmlEnum
public enum OwnerType {

    @XmlEnumValue("Joint")
    JOINT("Joint"),
    @XmlEnumValue("Single")
    SINGLE("Single");
    private final String value;

    OwnerType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OwnerType fromValue(String v) {
        for (OwnerType c: OwnerType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
