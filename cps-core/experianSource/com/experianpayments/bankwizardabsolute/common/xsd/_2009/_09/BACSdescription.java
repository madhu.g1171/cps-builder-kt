
package com.experianpayments.bankwizardabsolute.common.xsd._2009._09;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BACSdescription.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BACSdescription">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Account closed"/>
 *     &lt;enumeration value="Account holder deceased"/>
 *     &lt;enumeration value="Account does not exist"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BACSdescription")
@XmlEnum
public enum BACSdescription {

    @XmlEnumValue("Account closed")
    ACCOUNT_CLOSED("Account closed"),
    @XmlEnumValue("Account holder deceased")
    ACCOUNT_HOLDER_DECEASED("Account holder deceased"),
    @XmlEnumValue("Account does not exist")
    ACCOUNT_DOES_NOT_EXIST("Account does not exist");
    private final String value;

    BACSdescription(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BACSdescription fromValue(String v) {
        for (BACSdescription c: BACSdescription.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
