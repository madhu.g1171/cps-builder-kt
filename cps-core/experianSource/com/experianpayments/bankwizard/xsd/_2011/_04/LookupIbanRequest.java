
package com.experianpayments.bankwizard.xsd._2011._04;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.experianpayments.bankwizard.xsd._2009._07.BBANBaseType;


/**
 * 
 *         Looukup IBAN and BIC for given BBAN.
 *       
 * 
 * <p>Java class for LookupIbanRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LookupIbanRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBAN" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BBANBaseType" maxOccurs="4"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ISOCountry" use="required" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ISO3166-1" />
 *       &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}language" default="en" />
 *       &lt;attribute name="reportString" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ReportString" />
 *       &lt;attribute name="itemisationID" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ItemisationID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LookupIbanRequest", propOrder = {
    "bban"
})
public class LookupIbanRequest {

    @XmlElement(name = "BBAN", required = true)
    protected List<BBANBaseType> bban;
    @XmlAttribute(name = "ISOCountry", required = true)
    protected String isoCountry;
    @XmlAttribute(name = "language")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;
    @XmlAttribute(name = "reportString")
    protected String reportString;
    @XmlAttribute(name = "itemisationID")
    protected String itemisationID;

    /**
     * Gets the value of the bban property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bban property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBBAN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BBANBaseType }
     * 
     * 
     */
    public List<BBANBaseType> getBBAN() {
        if (bban == null) {
            bban = new ArrayList<BBANBaseType>();
        }
        return this.bban;
    }

    /**
     * Gets the value of the isoCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOCountry() {
        return isoCountry;
    }

    /**
     * Sets the value of the isoCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOCountry(String value) {
        this.isoCountry = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        if (language == null) {
            return "en";
        } else {
            return language;
        }
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the reportString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportString() {
        return reportString;
    }

    /**
     * Sets the value of the reportString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportString(String value) {
        this.reportString = value;
    }

    /**
     * Gets the value of the itemisationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemisationID() {
        return itemisationID;
    }

    /**
     * Sets the value of the itemisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemisationID(String value) {
        this.itemisationID = value;
    }

}
