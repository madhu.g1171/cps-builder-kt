
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * 
 *         Request the Bank Wizard checking levels available for an ISO 3166-1 two-alpha country code the input BBAN details
 *         associated with the checking levels.
 * 
 *         An optional language attribute can be passed in. English (en) is the
 *         default if the requested type is not supported.
 *       
 * 
 * <p>Java class for GetInputWithSearchRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetInputWithSearchRequest">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://experianpayments.com/bankwizard/common/xsd/2009/09>ISO3166-1">
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetInputWithSearchRequest", propOrder = {
    "value"
})
public class GetInputWithSearchRequest {

    @XmlValue
    protected String value;

    /**
     * 
     *         ISO 3166-1 2-alpha code.
     *       
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

}
