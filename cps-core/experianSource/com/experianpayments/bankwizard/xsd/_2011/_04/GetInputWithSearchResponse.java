
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         The BBAN inputs for various Bank Wizard checking levels associated the requested ISO 3166-1 two-alpha country code,
 *         also returns the relevance Search categories which could be used to filter search for the given country.
 *       
 * 
 * <p>Java class for GetInputWithSearchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetInputWithSearchResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="countryInputMetaData" type="{http://experianpayments.com/bankwizard/xsd/2011/04}CountryInputMetaData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetInputWithSearchResponse", propOrder = {
    "countryInputMetaData"
})
public class GetInputWithSearchResponse {

    @XmlElement(required = true)
    protected CountryInputMetaData countryInputMetaData;

    /**
     * Gets the value of the countryInputMetaData property.
     * 
     * @return
     *     possible object is
     *     {@link CountryInputMetaData }
     *     
     */
    public CountryInputMetaData getCountryInputMetaData() {
        return countryInputMetaData;
    }

    /**
     * Sets the value of the countryInputMetaData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryInputMetaData }
     *     
     */
    public void setCountryInputMetaData(CountryInputMetaData value) {
        this.countryInputMetaData = value;
    }

}
