
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizard.xsd._2009._07.GetCountryInputResponse;


/**
 * <p>Java class for CountryInputMetaData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CountryInputMetaData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="countryInput" type="{http://experianpayments.com/bankwizard/xsd/2009/07}GetCountryInputResponse"/>
 *         &lt;element name="searchCategories" type="{http://experianpayments.com/bankwizard/xsd/2011/04}SearchCategories"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryInputMetaData", propOrder = {
    "countryInput",
    "searchCategories"
})
public class CountryInputMetaData {

    @XmlElement(required = true)
    protected GetCountryInputResponse countryInput;
    @XmlElement(required = true)
    protected SearchCategories searchCategories;

    /**
     * Gets the value of the countryInput property.
     * 
     * @return
     *     possible object is
     *     {@link GetCountryInputResponse }
     *     
     */
    public GetCountryInputResponse getCountryInput() {
        return countryInput;
    }

    /**
     * Sets the value of the countryInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCountryInputResponse }
     *     
     */
    public void setCountryInput(GetCountryInputResponse value) {
        this.countryInput = value;
    }

    /**
     * Gets the value of the searchCategories property.
     * 
     * @return
     *     possible object is
     *     {@link SearchCategories }
     *     
     */
    public SearchCategories getSearchCategories() {
        return searchCategories;
    }

    /**
     * Sets the value of the searchCategories property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCategories }
     *     
     */
    public void setSearchCategories(SearchCategories value) {
        this.searchCategories = value;
    }

}
