
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CategoryType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CategoryType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="address"/>
 *     &lt;enumeration value="bank"/>
 *     &lt;enumeration value="bic"/>
 *     &lt;enumeration value="country"/>
 *     &lt;enumeration value="token"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CategoryType")
@XmlEnum
public enum CategoryType {

    @XmlEnumValue("address")
    ADDRESS("address"),
    @XmlEnumValue("bank")
    BANK("bank"),
    @XmlEnumValue("bic")
    BIC("bic"),
    @XmlEnumValue("country")
    COUNTRY("country"),
    @XmlEnumValue("token")
    TOKEN("token");
    private final String value;

    CategoryType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CategoryType fromValue(String v) {
        for (CategoryType c: CategoryType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
