
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Categorization.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Categorization">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="CreditUnion"/>
 *     &lt;enumeration value="BuildingSociety"/>
 *     &lt;enumeration value="Bank"/>
 *     &lt;enumeration value="Business"/>
 *     &lt;enumeration value="Charity"/>
 *     &lt;enumeration value="Personal"/>
 *     &lt;enumeration value="CentralGovernment"/>
 *     &lt;enumeration value="LocalGovernment"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Categorization")
@XmlEnum
public enum Categorization {

    @XmlEnumValue("CreditUnion")
    CREDIT_UNION("CreditUnion"),
    @XmlEnumValue("BuildingSociety")
    BUILDING_SOCIETY("BuildingSociety"),
    @XmlEnumValue("Bank")
    BANK("Bank"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("Charity")
    CHARITY("Charity"),
    @XmlEnumValue("Personal")
    PERSONAL("Personal"),
    @XmlEnumValue("CentralGovernment")
    CENTRAL_GOVERNMENT("CentralGovernment"),
    @XmlEnumValue("LocalGovernment")
    LOCAL_GOVERNMENT("LocalGovernment"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    Categorization(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Categorization fromValue(String v) {
        for (Categorization c: Categorization.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
