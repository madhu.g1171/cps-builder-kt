
package com.experianpayments.bankwizard.xsd._2011._04;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SwiftSearchResults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SwiftSearchResults">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizard/xsd/2011/04}SearchResult">
 *       &lt;sequence>
 *         &lt;element name="SwiftData" type="{http://experianpayments.com/bankwizard/xsd/2011/04}SwiftData" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SwiftSearchResults", propOrder = {
    "swiftData"
})
public class SwiftSearchResults
    extends SearchResult
{

    @XmlElement(name = "SwiftData")
    protected List<SwiftData> swiftData;

    /**
     * Gets the value of the swiftData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the swiftData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSwiftData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SwiftData }
     * 
     * 
     */
    public List<SwiftData> getSwiftData() {
        if (swiftData == null) {
            swiftData = new ArrayList<SwiftData>();
        }
        return this.swiftData;
    }

}
