
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.OwnerType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizard.xsd._2009._07 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerifyPersonalRequestTypeOwnerType_QNAME = new QName("http://experianpayments.com/bankwizard/xsd/2009/07", "ownerType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizard.xsd._2009._07
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAccessKeyType }
     * 
     */
    public DataAccessKeyType createDataAccessKeyType() {
        return new DataAccessKeyType();
    }

    /**
     * Create an instance of {@link CountryInformation }
     * 
     */
    public CountryInformation createCountryInformation() {
        return new CountryInformation();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link GetSWIFTDataResponse }
     * 
     */
    public GetSWIFTDataResponse createGetSWIFTDataResponse() {
        return new GetSWIFTDataResponse();
    }

    /**
     * Create an instance of {@link GetAdditionalDataRequest }
     * 
     */
    public GetAdditionalDataRequest createGetAdditionalDataRequest() {
        return new GetAdditionalDataRequest();
    }

    /**
     * Create an instance of {@link GetFasterPaymentsDataResponse }
     * 
     */
    public GetFasterPaymentsDataResponse createGetFasterPaymentsDataResponse() {
        return new GetFasterPaymentsDataResponse();
    }

    /**
     * Create an instance of {@link SWIFTDataType }
     * 
     */
    public SWIFTDataType createSWIFTDataType() {
        return new SWIFTDataType();
    }

    /**
     * Create an instance of {@link AddressLineType }
     * 
     */
    public AddressLineType createAddressLineType() {
        return new AddressLineType();
    }

    /**
     * Create an instance of {@link ValidateRequest }
     * 
     */
    public ValidateRequest createValidateRequest() {
        return new ValidateRequest();
    }

    /**
     * Create an instance of {@link BankWizardCheckType }
     * 
     */
    public BankWizardCheckType createBankWizardCheckType() {
        return new BankWizardCheckType();
    }

    /**
     * Create an instance of {@link AccountTypeInformation }
     * 
     */
    public AccountTypeInformation createAccountTypeInformation() {
        return new AccountTypeInformation();
    }

    /**
     * Create an instance of {@link VerifyPersonalRequestType }
     * 
     */
    public VerifyPersonalRequestType createVerifyPersonalRequestType() {
        return new VerifyPersonalRequestType();
    }

    /**
     * Create an instance of {@link VerifyPersonalResponseType }
     * 
     */
    public VerifyPersonalResponseType createVerifyPersonalResponseType() {
        return new VerifyPersonalResponseType();
    }

    /**
     * Create an instance of {@link ValidationBBANRequestType }
     * 
     */
    public ValidationBBANRequestType createValidationBBANRequestType() {
        return new ValidationBBANRequestType();
    }

    /**
     * Create an instance of {@link VerifyBankAccountRequestType }
     * 
     */
    public VerifyBankAccountRequestType createVerifyBankAccountRequestType() {
        return new VerifyBankAccountRequestType();
    }

    /**
     * Create an instance of {@link DeliveryPoint }
     * 
     */
    public DeliveryPoint createDeliveryPoint() {
        return new DeliveryPoint();
    }

    /**
     * Create an instance of {@link GetSWIFTDataRequest }
     * 
     */
    public GetSWIFTDataRequest createGetSWIFTDataRequest() {
        return new GetSWIFTDataRequest();
    }

    /**
     * Create an instance of {@link BranchDataType }
     * 
     */
    public BranchDataType createBranchDataType() {
        return new BranchDataType();
    }

    /**
     * Create an instance of {@link PersonalDetails }
     * 
     */
    public PersonalDetails createPersonalDetails() {
        return new PersonalDetails();
    }

    /**
     * Create an instance of {@link GetAdditionalDataResponse }
     * 
     */
    public GetAdditionalDataResponse createGetAdditionalDataResponse() {
        return new GetAdditionalDataResponse();
    }

    /**
     * Create an instance of {@link VerifyResponse }
     * 
     */
    public VerifyResponse createVerifyResponse() {
        return new VerifyResponse();
    }

    /**
     * Create an instance of {@link GetCountryInputResponse }
     * 
     */
    public GetCountryInputResponse createGetCountryInputResponse() {
        return new GetCountryInputResponse();
    }

    /**
     * Create an instance of {@link GetSEPADataRequest }
     * 
     */
    public GetSEPADataRequest createGetSEPADataRequest() {
        return new GetSEPADataRequest();
    }

    /**
     * Create an instance of {@link VerifiedAccountType }
     * 
     */
    public VerifiedAccountType createVerifiedAccountType() {
        return new VerifiedAccountType();
    }

    /**
     * Create an instance of {@link BBANBaseType }
     * 
     */
    public BBANBaseType createBBANBaseType() {
        return new BBANBaseType();
    }

    /**
     * Create an instance of {@link AdditionalBranchDataType }
     * 
     */
    public AdditionalBranchDataType createAdditionalBranchDataType() {
        return new AdditionalBranchDataType();
    }

    /**
     * Create an instance of {@link VerifyCompanyRequestType }
     * 
     */
    public VerifyCompanyRequestType createVerifyCompanyRequestType() {
        return new VerifyCompanyRequestType();
    }

    /**
     * Create an instance of {@link VerifyAccountRequestType }
     * 
     */
    public VerifyAccountRequestType createVerifyAccountRequestType() {
        return new VerifyAccountRequestType();
    }

    /**
     * Create an instance of {@link ValidateIBANResponse }
     * 
     */
    public ValidateIBANResponse createValidateIBANResponse() {
        return new ValidateIBANResponse();
    }

    /**
     * Create an instance of {@link ValidateResponse }
     * 
     */
    public ValidateResponse createValidateResponse() {
        return new ValidateResponse();
    }

    /**
     * Create an instance of {@link GetBranchDataRequest }
     * 
     */
    public GetBranchDataRequest createGetBranchDataRequest() {
        return new GetBranchDataRequest();
    }

    /**
     * Create an instance of {@link GetBranchDataResponse }
     * 
     */
    public GetBranchDataResponse createGetBranchDataResponse() {
        return new GetBranchDataResponse();
    }

    /**
     * Create an instance of {@link GetSEPADataResponse }
     * 
     */
    public GetSEPADataResponse createGetSEPADataResponse() {
        return new GetSEPADataResponse();
    }

    /**
     * Create an instance of {@link AccountDateType }
     * 
     */
    public AccountDateType createAccountDateType() {
        return new AccountDateType();
    }

    /**
     * Create an instance of {@link GetFasterPaymentsDataRequest }
     * 
     */
    public GetFasterPaymentsDataRequest createGetFasterPaymentsDataRequest() {
        return new GetFasterPaymentsDataRequest();
    }

    /**
     * Create an instance of {@link BBANResponseType }
     * 
     */
    public BBANResponseType createBBANResponseType() {
        return new BBANResponseType();
    }

    /**
     * Create an instance of {@link AdditionalBranchElement }
     * 
     */
    public AdditionalBranchElement createAdditionalBranchElement() {
        return new AdditionalBranchElement();
    }

    /**
     * Create an instance of {@link PostalPoint }
     * 
     */
    public PostalPoint createPostalPoint() {
        return new PostalPoint();
    }

    /**
     * Create an instance of {@link SEPADataType }
     * 
     */
    public SEPADataType createSEPADataType() {
        return new SEPADataType();
    }

    /**
     * Create an instance of {@link ValidateIBANRequest }
     * 
     */
    public ValidateIBANRequest createValidateIBANRequest() {
        return new ValidateIBANRequest();
    }

    /**
     * Create an instance of {@link GetCountriesResponse }
     * 
     */
    public GetCountriesResponse createGetCountriesResponse() {
        return new GetCountriesResponse();
    }

    /**
     * Create an instance of {@link BranchAddressType }
     * 
     */
    public BranchAddressType createBranchAddressType() {
        return new BranchAddressType();
    }

    /**
     * Create an instance of {@link GetCountryInputRequest }
     * 
     */
    public GetCountryInputRequest createGetCountryInputRequest() {
        return new GetCountryInputRequest();
    }

    /**
     * Create an instance of {@link GetCountriesRequest }
     * 
     */
    public GetCountriesRequest createGetCountriesRequest() {
        return new GetCountriesRequest();
    }

    /**
     * Create an instance of {@link FasterPaymentDataType }
     * 
     */
    public FasterPaymentDataType createFasterPaymentDataType() {
        return new FasterPaymentDataType();
    }

    /**
     * Create an instance of {@link VerifyCompanyResponseType }
     * 
     */
    public VerifyCompanyResponseType createVerifyCompanyResponseType() {
        return new VerifyCompanyResponseType();
    }

    /**
     * Create an instance of {@link VerifyRequest }
     * 
     */
    public VerifyRequest createVerifyRequest() {
        return new VerifyRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OwnerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/xsd/2009/07", name = "ownerType", scope = VerifyPersonalRequestType.class)
    public JAXBElement<OwnerType> createVerifyPersonalRequestTypeOwnerType(OwnerType value) {
        return new JAXBElement<OwnerType>(_VerifyPersonalRequestTypeOwnerType_QNAME, OwnerType.class, VerifyPersonalRequestType.class, value);
    }

}
