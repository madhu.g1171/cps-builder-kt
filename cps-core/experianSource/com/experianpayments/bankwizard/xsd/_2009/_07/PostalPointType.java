
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PostalPointType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PostalPointType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="street"/>
 *     &lt;enumeration value="postcode"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PostalPointType")
@XmlEnum
public enum PostalPointType {

    @XmlEnumValue("street")
    STREET("street"),
    @XmlEnumValue("postcode")
    POSTCODE("postcode");
    private final String value;

    PostalPointType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PostalPointType fromValue(String v) {
        for (PostalPointType c: PostalPointType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
