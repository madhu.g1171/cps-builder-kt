
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.AccountVerificationType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.BACSCodeType;


/**
 * 
 *         Details of the account details verified, note these may be different to what was requested following transposition.
 *         The 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;accountVerificationStatus&lt;/code&gt;
 * </pre>
 *  attribute is 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;b xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;present&lt;/b&gt;
 * </pre>
 *  details the matching status from the absolute service.
 *       
 * 
 * <p>Java class for VerifiedAccountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifiedAccountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sortCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rollNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bacsCode" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}BACSCodeType" minOccurs="0"/>
 *         &lt;element name="dataAccessKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="accountVerificationStatus" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}AccountVerificationType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifiedAccountType", propOrder = {
    "sortCode",
    "accountNumber",
    "rollNumber",
    "bacsCode",
    "dataAccessKey"
})
public class VerifiedAccountType {

    @XmlElement(required = true)
    protected String sortCode;
    @XmlElement(required = true)
    protected String accountNumber;
    protected String rollNumber;
    protected BACSCodeType bacsCode;
    protected String dataAccessKey;
    @XmlAttribute(name = "accountVerificationStatus")
    protected AccountVerificationType accountVerificationStatus;

    /**
     * Gets the value of the sortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the value of the sortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortCode(String value) {
        this.sortCode = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the rollNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRollNumber() {
        return rollNumber;
    }

    /**
     * Sets the value of the rollNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRollNumber(String value) {
        this.rollNumber = value;
    }

    /**
     * Gets the value of the bacsCode property.
     * 
     * @return
     *     possible object is
     *     {@link BACSCodeType }
     *     
     */
    public BACSCodeType getBacsCode() {
        return bacsCode;
    }

    /**
     * Sets the value of the bacsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BACSCodeType }
     *     
     */
    public void setBacsCode(BACSCodeType value) {
        this.bacsCode = value;
    }

    /**
     * Gets the value of the dataAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAccessKey() {
        return dataAccessKey;
    }

    /**
     * Sets the value of the dataAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAccessKey(String value) {
        this.dataAccessKey = value;
    }

    /**
     * Gets the value of the accountVerificationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link AccountVerificationType }
     *     
     */
    public AccountVerificationType getAccountVerificationStatus() {
        return accountVerificationStatus;
    }

    /**
     * Sets the value of the accountVerificationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountVerificationType }
     *     
     */
    public void setAccountVerificationStatus(AccountVerificationType value) {
        this.accountVerificationStatus = value;
    }

}
