
package com.experianpayments.bankwizard.xsd._2009._07;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizard.common.xsd._2009._09.Conditions;


/**
 * 
 *         Validation of an IBAN response.
 * 
 *        For a successful validation the
 *          *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;BBAN&lt;/code&gt;
 * </pre>
 * : decomposed IBAN
 *          *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;dataAccessKey&lt;/code&gt;
 * </pre>
 * : for accessing lookup functions and
 *          *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;conditions&lt;/code&gt;
 * </pre>
 * : BankWizard core conditions
 *          *
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ISOcountry&lt;/code&gt;
 * </pre>
 * : country code associated with the IBAN
 *         are returned.
 * 
 * <p>Java class for ValidateIBANResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateIBANResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBAN" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BBANResponseType" maxOccurs="5" minOccurs="0"/>
 *         &lt;element name="dataAccessKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conditions" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}Conditions" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ISOcountry" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ISO3166-1" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateIBANResponse", propOrder = {
    "bban",
    "dataAccessKey",
    "conditions"
})
public class ValidateIBANResponse {

    @XmlElement(name = "BBAN")
    protected List<BBANResponseType> bban;
    protected String dataAccessKey;
    protected Conditions conditions;
    @XmlAttribute(name = "ISOcountry")
    protected String isOcountry;

    /**
     * Gets the value of the bban property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bban property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBBAN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BBANResponseType }
     * 
     * 
     */
    public List<BBANResponseType> getBBAN() {
        if (bban == null) {
            bban = new ArrayList<BBANResponseType>();
        }
        return this.bban;
    }

    /**
     * Gets the value of the dataAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAccessKey() {
        return dataAccessKey;
    }

    /**
     * Sets the value of the dataAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAccessKey(String value) {
        this.dataAccessKey = value;
    }

    /**
     * Gets the value of the conditions property.
     * 
     * @return
     *     possible object is
     *     {@link Conditions }
     *     
     */
    public Conditions getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Conditions }
     *     
     */
    public void setConditions(Conditions value) {
        this.conditions = value;
    }

    /**
     * Gets the value of the isOcountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOcountry() {
        return isOcountry;
    }

    /**
     * Sets the value of the isOcountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOcountry(String value) {
        this.isOcountry = value;
    }

}
