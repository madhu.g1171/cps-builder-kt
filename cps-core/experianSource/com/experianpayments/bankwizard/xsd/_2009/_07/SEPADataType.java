
package com.experianpayments.bankwizard.xsd._2009._07;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         SWIFT Data.
 *         For the main branch the 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;subBranchNumber&lt;/code&gt;
 * </pre>
 *  will be zero.
 *       
 * 
 * <p>Java class for SEPADataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SEPADataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ibanBIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSIBIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="creditTransferAdherenceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="creditTransferNonComplianceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="directDebitAdherenceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="directDebitNonComplianceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="priorityPaymentAdherenceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="priorityPaymentNonComplianceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="subBranchNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SEPADataType", propOrder = {
    "ibanBIC",
    "ssibic",
    "creditTransferAdherenceDate",
    "creditTransferNonComplianceDate",
    "directDebitAdherenceDate",
    "directDebitNonComplianceDate",
    "priorityPaymentAdherenceDate",
    "priorityPaymentNonComplianceDate"
})
public class SEPADataType {

    protected String ibanBIC;
    @XmlElement(name = "SSIBIC")
    protected String ssibic;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creditTransferAdherenceDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creditTransferNonComplianceDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar directDebitAdherenceDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar directDebitNonComplianceDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar priorityPaymentAdherenceDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar priorityPaymentNonComplianceDate;
    @XmlAttribute(name = "subBranchNumber")
    protected BigInteger subBranchNumber;

    /**
     * Gets the value of the ibanBIC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIbanBIC() {
        return ibanBIC;
    }

    /**
     * Sets the value of the ibanBIC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbanBIC(String value) {
        this.ibanBIC = value;
    }

    /**
     * Gets the value of the ssibic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSIBIC() {
        return ssibic;
    }

    /**
     * Sets the value of the ssibic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSIBIC(String value) {
        this.ssibic = value;
    }

    /**
     * Gets the value of the creditTransferAdherenceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreditTransferAdherenceDate() {
        return creditTransferAdherenceDate;
    }

    /**
     * Sets the value of the creditTransferAdherenceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreditTransferAdherenceDate(XMLGregorianCalendar value) {
        this.creditTransferAdherenceDate = value;
    }

    /**
     * Gets the value of the creditTransferNonComplianceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreditTransferNonComplianceDate() {
        return creditTransferNonComplianceDate;
    }

    /**
     * Sets the value of the creditTransferNonComplianceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreditTransferNonComplianceDate(XMLGregorianCalendar value) {
        this.creditTransferNonComplianceDate = value;
    }

    /**
     * Gets the value of the directDebitAdherenceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDirectDebitAdherenceDate() {
        return directDebitAdherenceDate;
    }

    /**
     * Sets the value of the directDebitAdherenceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDirectDebitAdherenceDate(XMLGregorianCalendar value) {
        this.directDebitAdherenceDate = value;
    }

    /**
     * Gets the value of the directDebitNonComplianceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDirectDebitNonComplianceDate() {
        return directDebitNonComplianceDate;
    }

    /**
     * Sets the value of the directDebitNonComplianceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDirectDebitNonComplianceDate(XMLGregorianCalendar value) {
        this.directDebitNonComplianceDate = value;
    }

    /**
     * Gets the value of the priorityPaymentAdherenceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPriorityPaymentAdherenceDate() {
        return priorityPaymentAdherenceDate;
    }

    /**
     * Sets the value of the priorityPaymentAdherenceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPriorityPaymentAdherenceDate(XMLGregorianCalendar value) {
        this.priorityPaymentAdherenceDate = value;
    }

    /**
     * Gets the value of the priorityPaymentNonComplianceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPriorityPaymentNonComplianceDate() {
        return priorityPaymentNonComplianceDate;
    }

    /**
     * Sets the value of the priorityPaymentNonComplianceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPriorityPaymentNonComplianceDate(XMLGregorianCalendar value) {
        this.priorityPaymentNonComplianceDate = value;
    }

    /**
     * Gets the value of the subBranchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubBranchNumber() {
        return subBranchNumber;
    }

    /**
     * Sets the value of the subBranchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubBranchNumber(BigInteger value) {
        this.subBranchNumber = value;
    }

}
