
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;key&lt;/code&gt;
 * </pre>
 *  is that returned from the Validate or Verify operation, which can be used to
 *         access branch data
 * 
 * <p>Java class for DataAccessKeyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataAccessKeyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataAccessKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}language" default="en" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAccessKeyType", propOrder = {
    "dataAccessKey"
})
@XmlSeeAlso({
    GetAdditionalDataRequest.class,
    GetSWIFTDataRequest.class,
    GetSEPADataRequest.class,
    GetBranchDataRequest.class,
    GetFasterPaymentsDataRequest.class
})
public class DataAccessKeyType {

    @XmlElement(required = true)
    protected String dataAccessKey;
    @XmlAttribute(name = "language")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;

    /**
     * Gets the value of the dataAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAccessKey() {
        return dataAccessKey;
    }

    /**
     * Sets the value of the dataAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAccessKey(String value) {
        this.dataAccessKey = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        if (language == null) {
            return "en";
        } else {
            return language;
        }
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

}
