
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.CheckContextType;


/**
 * 
 *         Verify a bank account request.
 * 
 *         An optional language attribute can be passed in. English (en) is the
 *         default language returned if the requested type is not supported.
 * 
 *         The 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;userID&lt;/code&gt;
 * </pre>
 *  and 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;branchNumber&lt;/code&gt;
 * </pre>
 *  are optional client defined fields.
 *       
 * 
 * <p>Java class for VerifyAccountRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyAccountRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sortCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rollNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="checkContext" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}CheckContextType"/>
 *         &lt;element name="accountVerification" type="{http://experianpayments.com/bankwizard/xsd/2009/07}VerifyBankAccountRequestType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyAccountRequestType", propOrder = {
    "sortCode",
    "accountNumber",
    "rollNumber",
    "checkContext",
    "accountVerification"
})
public class VerifyAccountRequestType {

    @XmlElement(required = true)
    protected String sortCode;
    @XmlElement(required = true)
    protected String accountNumber;
    protected String rollNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CheckContextType checkContext;
    protected VerifyBankAccountRequestType accountVerification;

    /**
     * Gets the value of the sortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the value of the sortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortCode(String value) {
        this.sortCode = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the rollNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRollNumber() {
        return rollNumber;
    }

    /**
     * Sets the value of the rollNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRollNumber(String value) {
        this.rollNumber = value;
    }

    /**
     * Gets the value of the checkContext property.
     * 
     * @return
     *     possible object is
     *     {@link CheckContextType }
     *     
     */
    public CheckContextType getCheckContext() {
        return checkContext;
    }

    /**
     * Sets the value of the checkContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckContextType }
     *     
     */
    public void setCheckContext(CheckContextType value) {
        this.checkContext = value;
    }

    /**
     * Gets the value of the accountVerification property.
     * 
     * @return
     *     possible object is
     *     {@link VerifyBankAccountRequestType }
     *     
     */
    public VerifyBankAccountRequestType getAccountVerification() {
        return accountVerification;
    }

    /**
     * Sets the value of the accountVerification property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerifyBankAccountRequestType }
     *     
     */
    public void setAccountVerification(VerifyBankAccountRequestType value) {
        this.accountVerification = value;
    }

}
