
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Verification account details request element.
 *       
 * 
 * <p>Java class for VerifyBankAccountRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyBankAccountRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountSetupDate" type="{http://experianpayments.com/bankwizard/xsd/2009/07}AccountDateType" minOccurs="0"/>
 *         &lt;element name="accountTypeInformation" type="{http://experianpayments.com/bankwizard/xsd/2009/07}AccountTypeInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyBankAccountRequestType", propOrder = {
    "accountSetupDate",
    "accountTypeInformation"
})
public class VerifyBankAccountRequestType {

    protected AccountDateType accountSetupDate;
    protected AccountTypeInformation accountTypeInformation;

    /**
     * Gets the value of the accountSetupDate property.
     * 
     * @return
     *     possible object is
     *     {@link AccountDateType }
     *     
     */
    public AccountDateType getAccountSetupDate() {
        return accountSetupDate;
    }

    /**
     * Sets the value of the accountSetupDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountDateType }
     *     
     */
    public void setAccountSetupDate(AccountDateType value) {
        this.accountSetupDate = value;
    }

    /**
     * Gets the value of the accountTypeInformation property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTypeInformation }
     *     
     */
    public AccountTypeInformation getAccountTypeInformation() {
        return accountTypeInformation;
    }

    /**
     * Sets the value of the accountTypeInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTypeInformation }
     *     
     */
    public void setAccountTypeInformation(AccountTypeInformation value) {
        this.accountTypeInformation = value;
    }

}
