
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeliveryPointType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DeliveryPointType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="houseName"/>
 *     &lt;enumeration value="houseNumber"/>
 *     &lt;enumeration value="flat"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DeliveryPointType")
@XmlEnum
public enum DeliveryPointType {

    @XmlEnumValue("houseName")
    HOUSE_NAME("houseName"),
    @XmlEnumValue("houseNumber")
    HOUSE_NUMBER("houseNumber"),
    @XmlEnumValue("flat")
    FLAT("flat");
    private final String value;

    DeliveryPointType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeliveryPointType fromValue(String v) {
        for (DeliveryPointType c: DeliveryPointType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
