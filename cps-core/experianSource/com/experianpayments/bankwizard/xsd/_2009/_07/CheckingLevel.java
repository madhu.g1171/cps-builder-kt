
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CheckingLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CheckingLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Branch"/>
 *     &lt;enumeration value="Account"/>
 *     &lt;enumeration value="Domestic"/>
 *     &lt;enumeration value="BIC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CheckingLevel")
@XmlEnum
public enum CheckingLevel {

    @XmlEnumValue("Branch")
    BRANCH("Branch"),
    @XmlEnumValue("Account")
    ACCOUNT("Account"),
    @XmlEnumValue("Domestic")
    DOMESTIC("Domestic"),
    BIC("BIC");
    private final String value;

    CheckingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckingLevel fromValue(String v) {
        for (CheckingLevel c: CheckingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
