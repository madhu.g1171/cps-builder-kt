
package com.experianpayments.bankwizard.xsd._2009._07;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Additional Branch Data.
 *       
 * 
 * <p>Java class for AdditionalBranchDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdditionalBranchDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="branchInfo" type="{http://experianpayments.com/bankwizard/xsd/2009/07}AdditionalBranchElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="subBranchNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalBranchDataType", propOrder = {
    "branchInfo"
})
public class AdditionalBranchDataType {

    protected List<AdditionalBranchElement> branchInfo;
    @XmlAttribute(name = "subBranchNumber")
    protected BigInteger subBranchNumber;

    /**
     * Gets the value of the branchInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the branchInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBranchInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalBranchElement }
     * 
     * 
     */
    public List<AdditionalBranchElement> getBranchInfo() {
        if (branchInfo == null) {
            branchInfo = new ArrayList<AdditionalBranchElement>();
        }
        return this.branchInfo;
    }

    /**
     * Gets the value of the subBranchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubBranchNumber() {
        return subBranchNumber;
    }

    /**
     * Sets the value of the subBranchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubBranchNumber(BigInteger value) {
        this.subBranchNumber = value;
    }

}
