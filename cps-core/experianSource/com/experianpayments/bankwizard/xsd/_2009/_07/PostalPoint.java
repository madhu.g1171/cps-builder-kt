
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * 
 *         The delivery point.
 *       
 * 
 * <p>Java class for PostalPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PostalPoint">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="postalType" use="required" type="{http://experianpayments.com/bankwizard/xsd/2009/07}PostalPointType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostalPoint", propOrder = {
    "value"
})
public class PostalPoint {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "postalType", required = true)
    protected PostalPointType postalType;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the postalType property.
     * 
     * @return
     *     possible object is
     *     {@link PostalPointType }
     *     
     */
    public PostalPointType getPostalType() {
        return postalType;
    }

    /**
     * Sets the value of the postalType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostalPointType }
     *     
     */
    public void setPostalType(PostalPointType value) {
        this.postalType = value;
    }

}
