
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.PersonalDetailsVerificationType;


/**
 * 
 *         Verification company details response element.
 *       
 * 
 * <p>Java class for VerifyCompanyResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyCompanyResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="companyNameScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *         &lt;element name="companyNameAndAddressScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *         &lt;element name="companyTypeMatch" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}PersonalDetailsVerificationType" minOccurs="0"/>
 *         &lt;element name="registrationNumberMatch" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}PersonalDetailsVerificationType" minOccurs="0"/>
 *         &lt;element name="proprietorDetailsScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *         &lt;element name="companyAccountSetupDateScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyCompanyResponseType", propOrder = {
    "companyNameScore",
    "companyNameAndAddressScore",
    "companyTypeMatch",
    "registrationNumberMatch",
    "proprietorDetailsScore",
    "companyAccountSetupDateScore"
})
public class VerifyCompanyResponseType {

    @XmlSchemaType(name = "integer")
    protected Integer companyNameScore;
    @XmlSchemaType(name = "integer")
    protected Integer companyNameAndAddressScore;
    @XmlSchemaType(name = "token")
    protected PersonalDetailsVerificationType companyTypeMatch;
    @XmlSchemaType(name = "token")
    protected PersonalDetailsVerificationType registrationNumberMatch;
    @XmlSchemaType(name = "integer")
    protected Integer proprietorDetailsScore;
    @XmlSchemaType(name = "integer")
    protected Integer companyAccountSetupDateScore;

    /**
     * Gets the value of the companyNameScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCompanyNameScore() {
        return companyNameScore;
    }

    /**
     * Sets the value of the companyNameScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCompanyNameScore(Integer value) {
        this.companyNameScore = value;
    }

    /**
     * Gets the value of the companyNameAndAddressScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCompanyNameAndAddressScore() {
        return companyNameAndAddressScore;
    }

    /**
     * Sets the value of the companyNameAndAddressScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCompanyNameAndAddressScore(Integer value) {
        this.companyNameAndAddressScore = value;
    }

    /**
     * Gets the value of the companyTypeMatch property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public PersonalDetailsVerificationType getCompanyTypeMatch() {
        return companyTypeMatch;
    }

    /**
     * Sets the value of the companyTypeMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public void setCompanyTypeMatch(PersonalDetailsVerificationType value) {
        this.companyTypeMatch = value;
    }

    /**
     * Gets the value of the registrationNumberMatch property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public PersonalDetailsVerificationType getRegistrationNumberMatch() {
        return registrationNumberMatch;
    }

    /**
     * Sets the value of the registrationNumberMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public void setRegistrationNumberMatch(PersonalDetailsVerificationType value) {
        this.registrationNumberMatch = value;
    }

    /**
     * Gets the value of the proprietorDetailsScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProprietorDetailsScore() {
        return proprietorDetailsScore;
    }

    /**
     * Sets the value of the proprietorDetailsScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProprietorDetailsScore(Integer value) {
        this.proprietorDetailsScore = value;
    }

    /**
     * Gets the value of the companyAccountSetupDateScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCompanyAccountSetupDateScore() {
        return companyAccountSetupDateScore;
    }

    /**
     * Sets the value of the companyAccountSetupDateScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCompanyAccountSetupDateScore(Integer value) {
        this.companyAccountSetupDateScore = value;
    }

}
