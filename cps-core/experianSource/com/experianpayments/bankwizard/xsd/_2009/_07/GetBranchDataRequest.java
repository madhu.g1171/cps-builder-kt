
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Lookup a Branch address based on 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;dataAccessKey&lt;/code&gt;
 * </pre>
 * .
 * 
 *         The 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;returnSubBranches&lt;/code&gt;
 * </pre>
 *  attribute is used to indicate if sub-branches are to be returned.
 *         If false or missing only the main branch is requested.
 * 
 *         An optional language attribute can be passed in. English (en) is the
 *         default if the requested type is not supported.
 * 
 * <p>Java class for GetBranchDataRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetBranchDataRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizard/xsd/2009/07}DataAccessKeyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="returnSubBranches" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetBranchDataRequest")
public class GetBranchDataRequest
    extends DataAccessKeyType
{

    @XmlAttribute(name = "returnSubBranches")
    protected Boolean returnSubBranches;

    /**
     * Gets the value of the returnSubBranches property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnSubBranches() {
        if (returnSubBranches == null) {
            return false;
        } else {
            return returnSubBranches;
        }
    }

    /**
     * Sets the value of the returnSubBranches property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnSubBranches(Boolean value) {
        this.returnSubBranches = value;
    }

}
