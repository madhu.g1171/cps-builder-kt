
package com.experianpayments.bankwizard.xsd._2009._07;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         Faster Payments Data.
 *         For the main branch the 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;code xmlns="http://www.w3.org/2001/XMLSchema" xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;subBranchNumber&lt;/code&gt;
 * </pre>
 *  will be zero.
 *       
 * 
 * <p>Java class for FasterPaymentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FasterPaymentDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agencyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastChangeDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="redirectedToBranch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="redirectingSortcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clearingClosedDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="handlingBankConnType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="handlingBankCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountsNumberedFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="settlementBankConnection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="settlementBankcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="subBranchNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FasterPaymentDataType", propOrder = {
    "status",
    "agencyType",
    "lastChangeDate",
    "redirectedToBranch",
    "redirectingSortcode",
    "clearingClosedDate",
    "handlingBankConnType",
    "handlingBankCode",
    "accountsNumberedFlag",
    "settlementBankConnection",
    "settlementBankcode"
})
public class FasterPaymentDataType {

    protected String status;
    protected String agencyType;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastChangeDate;
    protected Boolean redirectedToBranch;
    protected String redirectingSortcode;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar clearingClosedDate;
    protected String handlingBankConnType;
    protected String handlingBankCode;
    protected String accountsNumberedFlag;
    protected String settlementBankConnection;
    protected String settlementBankcode;
    @XmlAttribute(name = "subBranchNumber")
    protected BigInteger subBranchNumber;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the agencyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyType() {
        return agencyType;
    }

    /**
     * Sets the value of the agencyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyType(String value) {
        this.agencyType = value;
    }

    /**
     * Gets the value of the lastChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastChangeDate() {
        return lastChangeDate;
    }

    /**
     * Sets the value of the lastChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastChangeDate(XMLGregorianCalendar value) {
        this.lastChangeDate = value;
    }

    /**
     * Gets the value of the redirectedToBranch property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRedirectedToBranch() {
        return redirectedToBranch;
    }

    /**
     * Sets the value of the redirectedToBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRedirectedToBranch(Boolean value) {
        this.redirectedToBranch = value;
    }

    /**
     * Gets the value of the redirectingSortcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedirectingSortcode() {
        return redirectingSortcode;
    }

    /**
     * Sets the value of the redirectingSortcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedirectingSortcode(String value) {
        this.redirectingSortcode = value;
    }

    /**
     * Gets the value of the clearingClosedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClearingClosedDate() {
        return clearingClosedDate;
    }

    /**
     * Sets the value of the clearingClosedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClearingClosedDate(XMLGregorianCalendar value) {
        this.clearingClosedDate = value;
    }

    /**
     * Gets the value of the handlingBankConnType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandlingBankConnType() {
        return handlingBankConnType;
    }

    /**
     * Sets the value of the handlingBankConnType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandlingBankConnType(String value) {
        this.handlingBankConnType = value;
    }

    /**
     * Gets the value of the handlingBankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandlingBankCode() {
        return handlingBankCode;
    }

    /**
     * Sets the value of the handlingBankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandlingBankCode(String value) {
        this.handlingBankCode = value;
    }

    /**
     * Gets the value of the accountsNumberedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountsNumberedFlag() {
        return accountsNumberedFlag;
    }

    /**
     * Sets the value of the accountsNumberedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountsNumberedFlag(String value) {
        this.accountsNumberedFlag = value;
    }

    /**
     * Gets the value of the settlementBankConnection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettlementBankConnection() {
        return settlementBankConnection;
    }

    /**
     * Sets the value of the settlementBankConnection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettlementBankConnection(String value) {
        this.settlementBankConnection = value;
    }

    /**
     * Gets the value of the settlementBankcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettlementBankcode() {
        return settlementBankcode;
    }

    /**
     * Sets the value of the settlementBankcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettlementBankcode(String value) {
        this.settlementBankcode = value;
    }

    /**
     * Gets the value of the subBranchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubBranchNumber() {
        return subBranchNumber;
    }

    /**
     * Sets the value of the subBranchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubBranchNumber(BigInteger value) {
        this.subBranchNumber = value;
    }

}
