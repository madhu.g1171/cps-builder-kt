
package com.experianpayments.bankwizard.xsd._2009._07;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         The BBAN inputs for various Bank Wizard checking levels associated the requested ISO 3166-1 two-alpha country code.
 *       
 * 
 * <p>Java class for GetCountryInputResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCountryInputResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validationCheck" type="{http://experianpayments.com/bankwizard/xsd/2009/07}BankWizardCheckType" maxOccurs="4"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCountryInputResponse", propOrder = {
    "validationCheck"
})
public class GetCountryInputResponse {

    @XmlElement(required = true)
    protected List<BankWizardCheckType> validationCheck;

    /**
     * Gets the value of the validationCheck property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validationCheck property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidationCheck().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BankWizardCheckType }
     * 
     * 
     */
    public List<BankWizardCheckType> getValidationCheck() {
        if (validationCheck == null) {
            validationCheck = new ArrayList<BankWizardCheckType>();
        }
        return this.validationCheck;
    }

}
