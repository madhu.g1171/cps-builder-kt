
package com.experianpayments.bankwizard.wsdl.bankwizardservice_v1_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.experianpayments.bankwizard.xsd._2009._07.GetAdditionalDataRequest;
import com.experianpayments.bankwizard.xsd._2009._07.GetAdditionalDataResponse;
import com.experianpayments.bankwizard.xsd._2009._07.GetBranchDataRequest;
import com.experianpayments.bankwizard.xsd._2009._07.GetBranchDataResponse;
import com.experianpayments.bankwizard.xsd._2009._07.GetCountriesRequest;
import com.experianpayments.bankwizard.xsd._2009._07.GetCountriesResponse;
import com.experianpayments.bankwizard.xsd._2009._07.GetCountryInputRequest;
import com.experianpayments.bankwizard.xsd._2009._07.GetCountryInputResponse;
import com.experianpayments.bankwizard.xsd._2009._07.GetFasterPaymentsDataRequest;
import com.experianpayments.bankwizard.xsd._2009._07.GetFasterPaymentsDataResponse;
import com.experianpayments.bankwizard.xsd._2009._07.GetSEPADataRequest;
import com.experianpayments.bankwizard.xsd._2009._07.GetSEPADataResponse;
import com.experianpayments.bankwizard.xsd._2009._07.GetSWIFTDataRequest;
import com.experianpayments.bankwizard.xsd._2009._07.GetSWIFTDataResponse;
import com.experianpayments.bankwizard.xsd._2009._07.ValidateIBANRequest;
import com.experianpayments.bankwizard.xsd._2009._07.ValidateIBANResponse;
import com.experianpayments.bankwizard.xsd._2009._07.ValidateRequest;
import com.experianpayments.bankwizard.xsd._2009._07.ValidateResponse;
import com.experianpayments.bankwizard.xsd._2009._07.VerifyRequest;
import com.experianpayments.bankwizard.xsd._2009._07.VerifyResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizard.wsdl.bankwizardservice_v1_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ValidateRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "ValidateRequest");
    private final static QName _GetFasterPaymentsDataRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetFasterPaymentsDataRequest");
    private final static QName _GetSEPADataResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetSEPADataResponse");
    private final static QName _GetFasterPaymentsDataResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetFasterPaymentsDataResponse");
    private final static QName _GetSWIFTDataResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetSWIFTDataResponse");
    private final static QName _GetAdditionalDataRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetAdditionalDataRequest");
    private final static QName _GetBranchDataRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetBranchDataRequest");
    private final static QName _GetBranchDataResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetBranchDataResponse");
    private final static QName _ValidateResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "ValidateResponse");
    private final static QName _VerifyRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "VerifyRequest");
    private final static QName _ValidateIBANResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "ValidateIBANResponse");
    private final static QName _GetCountryInputRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetCountryInputRequest");
    private final static QName _GetSEPADataRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetSEPADataRequest");
    private final static QName _GetCountryInputResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetCountryInputResponse");
    private final static QName _GetCountriesRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetCountriesRequest");
    private final static QName _GetCountriesResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetCountriesResponse");
    private final static QName _VerifyResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "VerifyResponse");
    private final static QName _ValidateIBANRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "ValidateIBANRequest");
    private final static QName _GetAdditionalDataResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetAdditionalDataResponse");
    private final static QName _GetSWIFTDataRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", "GetSWIFTDataRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizard.wsdl.bankwizardservice_v1_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "ValidateRequest")
    public JAXBElement<ValidateRequest> createValidateRequest(ValidateRequest value) {
        return new JAXBElement<ValidateRequest>(_ValidateRequest_QNAME, ValidateRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFasterPaymentsDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetFasterPaymentsDataRequest")
    public JAXBElement<GetFasterPaymentsDataRequest> createGetFasterPaymentsDataRequest(GetFasterPaymentsDataRequest value) {
        return new JAXBElement<GetFasterPaymentsDataRequest>(_GetFasterPaymentsDataRequest_QNAME, GetFasterPaymentsDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSEPADataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetSEPADataResponse")
    public JAXBElement<GetSEPADataResponse> createGetSEPADataResponse(GetSEPADataResponse value) {
        return new JAXBElement<GetSEPADataResponse>(_GetSEPADataResponse_QNAME, GetSEPADataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFasterPaymentsDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetFasterPaymentsDataResponse")
    public JAXBElement<GetFasterPaymentsDataResponse> createGetFasterPaymentsDataResponse(GetFasterPaymentsDataResponse value) {
        return new JAXBElement<GetFasterPaymentsDataResponse>(_GetFasterPaymentsDataResponse_QNAME, GetFasterPaymentsDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSWIFTDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetSWIFTDataResponse")
    public JAXBElement<GetSWIFTDataResponse> createGetSWIFTDataResponse(GetSWIFTDataResponse value) {
        return new JAXBElement<GetSWIFTDataResponse>(_GetSWIFTDataResponse_QNAME, GetSWIFTDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAdditionalDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetAdditionalDataRequest")
    public JAXBElement<GetAdditionalDataRequest> createGetAdditionalDataRequest(GetAdditionalDataRequest value) {
        return new JAXBElement<GetAdditionalDataRequest>(_GetAdditionalDataRequest_QNAME, GetAdditionalDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranchDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetBranchDataRequest")
    public JAXBElement<GetBranchDataRequest> createGetBranchDataRequest(GetBranchDataRequest value) {
        return new JAXBElement<GetBranchDataRequest>(_GetBranchDataRequest_QNAME, GetBranchDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBranchDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetBranchDataResponse")
    public JAXBElement<GetBranchDataResponse> createGetBranchDataResponse(GetBranchDataResponse value) {
        return new JAXBElement<GetBranchDataResponse>(_GetBranchDataResponse_QNAME, GetBranchDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "ValidateResponse")
    public JAXBElement<ValidateResponse> createValidateResponse(ValidateResponse value) {
        return new JAXBElement<ValidateResponse>(_ValidateResponse_QNAME, ValidateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "VerifyRequest")
    public JAXBElement<VerifyRequest> createVerifyRequest(VerifyRequest value) {
        return new JAXBElement<VerifyRequest>(_VerifyRequest_QNAME, VerifyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateIBANResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "ValidateIBANResponse")
    public JAXBElement<ValidateIBANResponse> createValidateIBANResponse(ValidateIBANResponse value) {
        return new JAXBElement<ValidateIBANResponse>(_ValidateIBANResponse_QNAME, ValidateIBANResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountryInputRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetCountryInputRequest")
    public JAXBElement<GetCountryInputRequest> createGetCountryInputRequest(GetCountryInputRequest value) {
        return new JAXBElement<GetCountryInputRequest>(_GetCountryInputRequest_QNAME, GetCountryInputRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSEPADataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetSEPADataRequest")
    public JAXBElement<GetSEPADataRequest> createGetSEPADataRequest(GetSEPADataRequest value) {
        return new JAXBElement<GetSEPADataRequest>(_GetSEPADataRequest_QNAME, GetSEPADataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountryInputResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetCountryInputResponse")
    public JAXBElement<GetCountryInputResponse> createGetCountryInputResponse(GetCountryInputResponse value) {
        return new JAXBElement<GetCountryInputResponse>(_GetCountryInputResponse_QNAME, GetCountryInputResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountriesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetCountriesRequest")
    public JAXBElement<GetCountriesRequest> createGetCountriesRequest(GetCountriesRequest value) {
        return new JAXBElement<GetCountriesRequest>(_GetCountriesRequest_QNAME, GetCountriesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountriesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetCountriesResponse")
    public JAXBElement<GetCountriesResponse> createGetCountriesResponse(GetCountriesResponse value) {
        return new JAXBElement<GetCountriesResponse>(_GetCountriesResponse_QNAME, GetCountriesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "VerifyResponse")
    public JAXBElement<VerifyResponse> createVerifyResponse(VerifyResponse value) {
        return new JAXBElement<VerifyResponse>(_VerifyResponse_QNAME, VerifyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateIBANRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "ValidateIBANRequest")
    public JAXBElement<ValidateIBANRequest> createValidateIBANRequest(ValidateIBANRequest value) {
        return new JAXBElement<ValidateIBANRequest>(_ValidateIBANRequest_QNAME, ValidateIBANRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAdditionalDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetAdditionalDataResponse")
    public JAXBElement<GetAdditionalDataResponse> createGetAdditionalDataResponse(GetAdditionalDataResponse value) {
        return new JAXBElement<GetAdditionalDataResponse>(_GetAdditionalDataResponse_QNAME, GetAdditionalDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSWIFTDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-0", name = "GetSWIFTDataRequest")
    public JAXBElement<GetSWIFTDataRequest> createGetSWIFTDataRequest(GetSWIFTDataRequest value) {
        return new JAXBElement<GetSWIFTDataRequest>(_GetSWIFTDataRequest_QNAME, GetSWIFTDataRequest.class, null, value);
    }

}
