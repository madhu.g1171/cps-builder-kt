package com.tui.uk.payment.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

/**
 * This class is responsible for validating non payment data. For validation its using all data data
 * panels(<code>PassengersDataPanel</code>, <code>LeadPassengerDataPanel</code>,
 * <code>ImportantInformationDataPanel</code> and <code>TermsandConditionsDataPanel</code>).
 *
 * @author shwetha.rb
 *
 */
public class TuiTHValidationErrors
{

   /** The list of global errors. */
   private List<String> globalErrors;

   /** Field errors map. */
   private Map<String, String> fieldErrors;

   /** The panel data map. */
   private Map<String, String> panelDataMap = new HashMap<String, String>();

   /**
    * Constructor with key.
    *
    */
   public TuiTHValidationErrors()
   {
      globalErrors = new ArrayList<String>();
      fieldErrors = new HashMap<String, String>();
      buildPanelDataMap();
   }

   /**
    * addGlobalError.
    *
    * @param globalErrorMessage the field name, which is used in important information section in
    *           payment page.
    */

   public void addGlobalError(String globalErrorMessage)
   {

      globalErrors.add(globalErrorMessage);
   }

   /**
    * addFieldError.
    *
    * @param field the field name, which is used in important information section in payment page.
    * @param fieldErrorMessage the field name, which is used in important information section in
    *           payment page.
    */

   public void addFieldError(String field, String fieldErrorMessage)
   {

      fieldErrors.put(field, fieldErrorMessage);
   }

   /**
    * getFieldErrors.
    *
    * @return fieldErrors.
    */

   public Map getFieldErrors()
   {
      return fieldErrors;
   }

   /**
    * getGlobalErrors.
    *
    * @return globalErrors.
    *
    */

   public List getGlobalErrors()
   {
      return globalErrors;
   }

   /**
    * This method is responsible for populating panelDataMap with the details of the specified.
    * fields in the validation list.
    */
   public void buildPanelDataMap()
   {
      panelDataMap.putAll(TuiTHPatternMatchDataPanel.getAllPatterns());
   }

   /**
    * This method is responsible for pattern check for the fields like houseName, postCode,
    * dayTimePhone, city, county, mobilePhone that are used in the validation.
    *
    * @param fieldName the field name.
    * @param fieldValue the field value.
    *
    * @return boolean the true if pattern match.
    */
   public boolean patternCheck(String fieldName, String fieldValue)
   {
      // panelDataMap having all key and patterns.
      // checking for pattern.
      boolean isPatternMatch = false;
      String fieldPattern = panelDataMap.get(fieldName);
      if (StringUtils.isNotBlank(fieldValue) && StringUtils.isNotBlank(fieldPattern))
      {
         Pattern pattern = Pattern.compile(fieldPattern);
         Matcher matcher = pattern.matcher(fieldValue);
         isPatternMatch = matcher.matches();
      }
      return isPatternMatch;
   }

}
