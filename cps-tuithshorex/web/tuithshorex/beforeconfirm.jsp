<%@include file="/common/commonTagLibs.jspf"%>
<html>
<script type="text/javascript">
function bankRedirectToclient()
{
	document.beforeconfirm.target = "_parent";

	setTimeout(function() {document.beforeconfirm.submit();},500);
}
</script>

<body onLoad="javascript:bankRedirectToclient();">


	<form name="beforeconfirm" method='get'
		action='<c:out value="${postPaymentUrl}"  escapeXml="false"/>'
		target="_parent">

		<div style="height: 50px; font-weight: bold">
			<br />
			<br /> <br />
			<br /> We are contacting your bank <br />
			<br /> Please Wait...
		</div>

		<noscript>
			<center>
				</br> </br> </br> </br> <input type="submit" value="Continue with payment" />
			</center>
		</noscript>
		<input type="hidden" name="token" value="<c:out value='${token}'/>">
		<span id="test"
			style="display: none;"><c:out
				value='${bookingInfo.threeDAuth}' /></span>

	</form>
</body>
</html>