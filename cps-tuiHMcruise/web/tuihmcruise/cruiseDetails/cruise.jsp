
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<p class="title">Cabin & Board</p>



<div class="breakdown marg-bottom-20 ">
<c:set var="cruiseResort" value="${bookingComponent.cruiseSummary.ship.resort}" />
<c:set var="cruiseResortArray" value="${fn:split(cruiseResort,'|')}" />

<!-- <c:out value="${bookingComponent.cruiseSummary.ship.resort}" />-->

                      <!--     <c:out value="${cruiseResortArray[0]}" />
                           <c:if test="${not empty cruiseResortArray[1]}"><br />
                              <span class="followed">followed by </span><br />
                              <c:out value="${cruiseResortArray[1]}" />
                           </c:if>

						 <c:if test="${not empty bookingComponent.cruiseSummary.ship.destination}">
                               <c:out value="${bookingComponent.cruiseSummary.ship.destination}" />
				            </c:if>
<c:out value="${bookingComponent.cruiseSummary.duration}" />
                        <c:choose>
							<c:when test="${bookingComponent.cruiseSummary.duration == 1}">
								night cruise
							</c:when>
							<c:otherwise>
								 night cruise
							</c:otherwise>
						</c:choose>-->
<ul class=" grey-med">
	<c:if test="${not empty bookingComponent.pricingDetails}">
			<c:set var="cruiseExtras" value="${bookingComponent.pricingDetails['CRUISE_EXTRAS_PRICE_COMPONENT']}" />
		<c:if test="${not empty cruiseExtras}">
	     <c:forEach var="cruiseExtra" items="${cruiseExtras}">
	<li>
		<span class="fl">
			<c:out value="${cruiseExtra.itemDescription}" escapeXml='false'/>
				<c:if test="${cruiseExtra.quantity ne null && cruiseExtra.quantity > 0}"> x <c:out value="${cruiseExtra.quantity}" />
				</c:if>
		 </span>
		<span class="fr">
			<c:choose>
				<c:when test="${cruiseExtra.additionalPriceInfo eq 'included'}">
					<c:out value="Included" />
				</c:when>
				<c:otherwise>
					<c:if test="${cruiseExtra.amount.amount >= 0 && cruiseExtra.quantity ne null && cruiseExtra.quantity > 0}">
										&pound;<fmt:formatNumber value="${cruiseExtra.amount.amount}"
												type="number" maxFractionDigits="2" minFractionDigits="2"
												pattern="#####.##" />
										</c:if>

					<c:if test="${cruiseExtra.amount.amount < 0}">
										-&pound;<fmt:formatNumber value="${cruiseExtra.amount.amount}"
												type="number" maxFractionDigits="2" minFractionDigits="2"
												pattern="#####.##" />
										</c:if>
				</c:otherwise>
			</c:choose>
		 </span>

		<span class="fl">
		 <c:out value="${cruiseExtra.itemInfo}"/>
		 </span>
		<div class="clear"></div>
	</li>

            </c:forEach>
	    </c:if>
	</c:if>
<li>
					<span class="fl">
					<c:out value="${bookingComponent.cruiseSummary.ship.boardBasis}" />
					</span>
					<div class="clear"></div>
				</li>
</div>






