(function(jQuery){

	webForm = {

		validationEventType: null,
		allvalid:null,
		messageMap: null,


		/**
		 * Object: validatorTypes
		 * Defines the rules for all validator input types.
		 */
        validatorTypes: {
			errorType: null,

			alpha: {
                test: function(inputElement){
                    var filter = /^[a-zA-Z -]{2,}$/;
					webForm.validatorTypes.errorType = "";
                    if (inputElement.val().trim() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                                                                           if(inputElement.val().trim() == '' && inputElement[0].id != "addressLine2") {
							webForm.validatorTypes.errorType = inputElement[0].id+"Empty"
						return false;
					}

                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			surname: {
                test: function(inputElement){
                    var filter = /^[a-zA-Z -]{1,}$/;
					webForm.validatorTypes.errorType = "";
                    if (inputElement.val().trim() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                                                                           if(inputElement.val().trim() == '' && inputElement[0].id != "addressLine2") {
							webForm.validatorTypes.errorType = inputElement[0].id+"Empty"
						return false;
					}

                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            numeric: {
                test: function(inputElement){
                    var filter = /^[0-9]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			alphanumeric: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 \\\/.,]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            alphanumericspec: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 \\\/.,#]{1,}$/;
					webForm.validatorTypes.errorType = "";
                    if (inputElement.val().trim() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }

                                                                           if(inputElement.val().trim() == '') {
							webForm.validatorTypes.errorType = "houseNameEmpty"
						return false;
					}
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			streetline1Check: {
                test: function(inputElement){
                    var filter = /^([a-zA-Z0-9_\-\047]{2,}([-a-zA-Z0-9 ,\.\047])*)$/;
					webForm.validatorTypes.errorType = "";
                    if (inputElement.val().trim() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }

					if(inputElement.val().trim() == '' && inputElement[0].id != "addressLine2") {
							webForm.validatorTypes.errorType = inputElement[0].id+"Empty"
						return false;
					}
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			  townorcityCheck: {
                test: function(inputElement){
					if(inputElement.attr('numberAllowed') != 'OFF' && document.getElementById("country").value == 'IE'){
						 var filter = /^([a-zA-Z0-9\s]+(_[a-zA-Z0-9]+)*)$/;
					}else{
						 var filter = /^([a-zA-Z\s]+(_[a-zA-Z]+)*)$/;
					}
					webForm.validatorTypes.errorType = "";
                    if (inputElement.val().trim() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }

					if(inputElement.val().trim() == '') {
							webForm.validatorTypes.errorType = "cityEmpty"
						return false;
					}
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			testCounty: {
                test: function(inputElement){
                    var filter = /^[a-zA-Z., -]*$/;
					webForm.validatorTypes.errorType = "";
                    if (inputElement.val().trim() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }

                      
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
			address: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 ,\.\x27]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            phonenumber: {
                test: function(inputElement){
                    var filter = /(^([+]?[0-9-\(\)]*)([-\(\)0-9]*)$)/;
                    if (inputElement.val() != '' && !filter.test(StringUtils.removeAllSpaces(inputElement.val()))) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			postcode: {
                test: function(inputElement){
				if((document.getElementById("country").value) == 'GB')
				    {
					var filter = /^([A-PR-UWYZ][A-HK-Y0-9][ACDFGJKSEHMNPRTVXYU0-9]?[A-HJKSMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
						if(inputElement.val().trim() == '') {
							webForm.validatorTypes.errorType = "postcodeEmpty"
						return false;
						}
                    }
                else{
					var filter = /^[-a-zA-Z0-9 \\\/.,]*$/;
                   }
					if (inputElement.val().trim() != '' && !filter.test(inputElement.val().trim().toUpperCase())) {
						webForm.validatorTypes.errorType = "postcodeEmpty";
                        return false;
                    }

                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			cardpostcode: {
                test: function(inputElement){
                	 if((document.getElementById("cardcountry").value) == 'United Kingdom')
				   {
				      var filter = /^([A-PR-UWYZ][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
				  }
                  else{
				    var filter = /^[-a-zA-Z0-9 \\\/.,]*$/;
                   }

                    if (inputElement.val() != '' && !filter.test(inputElement.val().toUpperCase())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },


			cardnumber: {
                test: function(inputElement){
				    var filter = /^[0-9]*$/;
				    webForm.validatorTypes.errorType = "";
				    //var cardType =jQuery("#cardType").val();
					var cardType ="";
					//var cardType =jQuery("#card-detail")[0].className;
			        //alert(cardType);
                    if(cardType=="AMERICAN_EXPRESS")
                    {
				        filter = /^[0-9]{15}$/;
                    }
					else if(cardType != '')
					{
					   filter = /^[0-9]{16,20}$/;
					}
					var value=inputElement.val();

					if(inputElement.val() == '') {
							document.getElementById("card-img").className = "fl marg-top-5 marg-left-5";
							document.getElementById("card-desc").innerHTML = "";
							jQuery("#issueNumber").val("");
							document.getElementById('issue').style.display = 'none';
							webForm.validatorTypes.errorType = "cardNoEmpty"
						return false;
					}

					if(value == 0){
					webForm.validatorTypes.errorType = "cardNoZero"
						return false;
					}
					if(value.length >0 && value.length < 15 && !isNaN(parseInt(value))) {
						webForm.validatorTypes.errorType = "cardNoMinLength"
						return false;
					}

                    if (typeof(inputElement.val())=="string"){
						if(!filter.test(StringUtils.removeAllSpaces(inputElement.val()))) {
							webForm.validatorTypes.errorType = "cardNoRegEx"
							return false;
						}

                    }


                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            issuenumber: {
            	test: function(inputElement){
            	var filter = /^[0-9]*$/;
			    webForm.validatorTypes.errorType = "";
				if(inputElement.val() != '' && inputElement.val().length < 2){
					return false;
				}
			    else if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                    return false;
                }
			    return true;
            	},
            	 errorDisplay: function(inputElement, type){
                     this._displayError(inputElement, type);
                 }
            },


			securitycode: {
                test: function(inputElement){
				    var filter = /^[0-9]*$/;
					webForm.validatorTypes.errorType = "";
				    //var cardType =(jQuery("#cardType").val()).split("|")[0];
					//var cardType="";
					var cardType =jQuery("#card-detail")[0].className;

                    if(cardType=="AmericanExpress")
                    {
				        filter = /^[0-9]{4}$/;
                    }
					else if(cardType != '')
					{
					   filter = /^[0-9]{3}$/;
					}
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
					if(inputElement.val() == '') {
							webForm.validatorTypes.errorType = "securityCodeEmpty"
						return false;
					}

					    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			expirydate: {
                test: function(inputElement){
				    var Calendar=new Date();
				    webForm.validatorTypes.errorType = "";
                    var todaysmonth =parseInt(Calendar.getMonth()+1,10);
                    var todaysyear = parseInt(Calendar.getFullYear(),10);
					var cardmonth = jQuery("#expiryDateMM").val();
                    var cardyear = jQuery("#expiryDateYY").val();
					if(cardmonth != "")
					   cardmonth = parseInt(cardmonth,10);
					if(cardyear != "")
                       cardyear = parseInt("20" + cardyear, 10);

                    if (cardmonth != "" && cardyear != "" && ((cardyear == todaysyear) || (cardyear<todaysyear)))
                    {
                       if ((cardmonth<todaysmonth) || (cardyear<todaysyear))
                       {
					      return false;
					   }
					}
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			email: {
                test: function(inputElement){
                    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			title: {
                test: function(inputElement){
                    if (inputElement.val() == '') {
					    webForm.validatorTypes.errorType = "titleEmpty"
                        return false;
                    }
					return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

			nonblank: {
                test: function(inputElement){
                    if (inputElement.val() == '') {
                        return false;
                    }
					return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            }


        },

		/**
		 * Object: validators
		 * Defines the rules for all validator.
		 */
        validators: {
        	gfv_required : {
                test: function(inputElement){
					console.log(inputElement);
                    if (inputElement.attr("type")=="text")
                    {
					   var str = inputElement.val();
					   str = str.replace(/^ /i,"")
					   inputElement.val(str);
                    }
                    if (this._isEmpty(inputElement)) {
					   return false;
                    }
                    return true;

                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            gfv_required_postcode : {
                test: function(inputElement){
                    if (inputElement.attr("type")=="text")
                    {
		       var str = inputElement.val();
		       str = str.replace(/^ /i,"")
		       inputElement.val(str);
                    }
                    if(document.getElementById("country").value == 'GB'){
                    if (this._isEmpty(inputElement)) {
					   return false;
                    }
                    }
                    return true;

                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            gfv_required_cardpostcode : {
                test: function(inputElement){
                    if (inputElement.attr("type")=="text")
                    {
		       var str = inputElement.val();
		       str = str.replace(/^ /i,"")
		       inputElement.val(str);
                    }

                    if(document.getElementById("cardcountry").value == 'United Kingdom'){
                        if (this._isEmpty(inputElement)) {
    					   return false;
                        }
                        }
                    return true;

                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

         gfv_match : {
                       test: function(inputElement){
          var prevVal = "";
          var matched = true;
          jQuery("input[gfv_match='match']").each(function(index){

            //if (jQuery(this).val().trim()==""){return false;}
           if (prevVal != "" && prevVal != jQuery(this).val()){matched = false;}
           prevVal = jQuery(this).val();

          });
          return matched;


                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            }
        },

        options: {
        	evaluateOnSubmit: true,
			evaluateFieldsOnBlur: true,
            showDefaultError: true,
            validateHidden: false,
			evaluateOnClick: true
        },

        _displayError: function(inputElement, type){
			var webForm = this;
			var errorType = inputElement.attr("id") + webForm.validationEventType + "Error"+ type;
			var errorMessage = (webForm.messageMap == null) ? null : webForm.messageMap[errorType];
            this._trigger("on" + webForm.validationEventType +"ErrorDisplay", null, [inputElement, errorType, errorMessage]);
			if(webForm.validationEventType == "Submit")
			this._setBrowserScrollingOffsets(0,0);
        },

        _init: function(){
			var webForm = this;
			var ver = window.navigator.appVersion;
            ver = ver.toLowerCase();

        if ( ver.indexOf("android") >= 0 ){

            var idMaxLengthMap = {};

            //loop through all input-text and textarea element
            jQuery.each(jQuery(':text, textarea, :password'), function () {
                var id = jQuery(this).attr('id'),
                    maxlength = jQuery(this).attr('maxlength');

                //element should have id and maxlength attribute
                if ((typeof id !== 'undefined') && (typeof maxlength !== 'undefined')) {
                    idMaxLengthMap[id] = maxlength;

                    //remove maxlength attribute from element
                    //jQuery(this).removeAttr('maxlength'); setting max length to -1  causing issue in nexus devices

                    //replace maxlength attribute with onkeypress event
                    jQuery(this).attr('onkeypress','if(this.value.length >= maxlength ) return false;');
                }
            });

            //bind onchange & onkeyup events
            //This events prevents user from pasting text with length more then maxlength
            jQuery(':text, textarea, :password').live('change keyup', function () {
                var id = jQuery(this).attr('id'),
                    maxlength = '';
                if (typeof id !== 'undefined' && idMaxLengthMap.hasOwnProperty(id)) {
                    maxlength = idMaxLengthMap[id];
                    if (jQuery(this).val().length > maxlength) {

                        //remove extra text which is more then maxlength
                        jQuery(this).val(jQuery(this).val().slice(0, maxlength));
                    }
                }
            });
        }


			if (webForm.options.evaluateOnSubmit){
            	jQuery(this.element).bind('submit', function(event){
					//event.preventDefault();
					webForm.validationEventType = "Submit";
					var answer = webForm._onValidation(this);
					var inputElements = webForm._getInputFields();
					var allFields = this.elements;

					//console.log(allFields);
					var chkerror=[];
					var invalidInputs=[];
					/* jQuery.each(inputElements, function(index, inputElement){
						chkerror.push(jQuery(inputElement).closest('.row').hasClass('error') ? false : true);
						invalidInputs.push(jQuery(inputElement).closest('.row').hasClass('error') ? inputElement:'');
					}); */
					jQuery.each(allFields, function(index, inputElement){
							chkerror.push(jQuery(inputElement).closest('.row').hasClass('error') ? false : true);
						invalidInputs.push(jQuery(inputElement).closest('.row').hasClass('error') ? inputElement:'');
				});

					var allValid = jQuery.inArray(false, chkerror) > -1 ? false : true;
					if(answer && allValid){
							jQuery(".ctnbutton").addClass("disable");
					}else{
							jQuery(".ctnbutton").removeClass("disable");
						// filter empty elements of an array
						invalidInputs = invalidInputs.filter(function(e){return e});
						console.log("invalids");
						console.log(invalidInputs);
						var pix = 50; //mobile
						var ua=navigator.userAgent;
						if(ua.indexOf("Mobile") == -1){
							var pix = 0; //desktop
						}
						if (invalidInputs[0].id == "tourOperatorTermsAccepted"){
						//Hard Coded the class as hidden input field offset on the page is zero
							var erinputtop = jQuery(".term-conditions-details .select-block").offset().top - pix;
						}else if(invalidInputs[0].tagName == "SELECT"){
							var erinputtop = jQuery(invalidInputs[0].parentElement.parentElement).offset().top - pix;
						}else{

							var erinputtop = jQuery(invalidInputs[0].parentElement).offset().top - pix;
						}


					//	webForm._setBrowserScrollingOffsets(0,erinputtop);
					window.scrollTo(0,erinputtop);
						event.preventDefault();
					}

				})
			}

            if (webForm.options.evaluateFieldsOnBlur) {

                var inputElements = webForm._getInputFields();
                jQuery.each(inputElements, function(index, inputElement){

                    jQuery(inputElement).blur(function(event){
						var answer = null;
						event.preventDefault();
						webForm.validationEventType = "Blur";
						if(this.getAttribute('type')!=="checkbox")
						{
						   webForm._trigger("beforeBlur", null, [this.getAttribute('id')]);
                           answer = webForm._validateInputFieldsType(jQuery(this), this.getAttribute('chk'));
						   if(answer)
						      webForm._trigger("on" + webForm.validationEventType +"SucessfulDisplay", null, [this.getAttribute('id'), this]);
						}
                    })
                });
				var selectElements = webForm._getSelectFields();
                jQuery.each(selectElements, function(index, selectElement){
                    jQuery(selectElement).change(function(event){
						event.preventDefault();
						webForm.validationEventType = "Blur";
				//		if(jQuery(selectElement).val() != ''){
						   webForm._trigger("beforeBlur", null, [this.getAttribute('id')]);
                          var answer = webForm._validateInputFieldsType(jQuery(this), this.getAttribute('chk'));
						  if(answer)
						      webForm._trigger("on" + webForm.validationEventType +"SucessfulDisplay", null, [this.getAttribute('id'), this]);
				//		}
                    })
                });
            }
			 if (webForm.options.evaluateOnClick){
			 jQuery("#promocode").click(function(event){
			 var promoCheck = PromotionalCodeHandler.handle();
			 if(!promoCheck)
			 {
			   jQuery("#promoCodeSection").css('padding-top','6px');
			   webForm._trigger("promoErrorDisplay", null,[PromotionalCodeHandler.errorfieldId, PromotionalCodeHandler.errorMessage]);
			   webForm._setBrowserScrollingOffsets(0,0);
			   event.preventDefault();
	         }
			 else
			 {
			   webForm._trigger("promoSuccessDisplay", null,[PromotionalCodeHandler.errorMessage]);
			 }
             });
			 if(jQuery('.check.confirm').length){
				 jQuery('.check.confirm').click(function(event) {
					var inputElement = jQuery("#tourOperatorTermsAccepted");
					if(jQuery(this).hasClass("active")) {
						inputElement.attr('value', true);
						inputElement.attr('checked', 'checked');
						event.preventDefault();
					}
					else {
						inputElement.attr('value', false);
						inputElement.removeAttr('checked');
					}
					var checkValue = /true/i.test(inputElement.val());
					if(!checkValue){
						var errorType = inputElement.attr("id") +  "SubmitErrorrequired" ;
						var errorMessage = (webForm.messageMap == null) ? null : webForm.messageMap[errorType];
						webForm._trigger("onSubmitErrorDisplay", null, [inputElement, errorType, errorMessage]);
					} else {
						webForm._trigger("onBlurSucessfulDisplay", null, [inputElement.attr("name"), inputElement]);
					}
				 });
			 }
            }
        },

		/**
		 * Function: _getInputFields
		 * Get all input fields in a given form.
		 */
		 _gethiddencheckbox:function(){
			return jQuery(this.element).find("input[chkit='yes']:hidden");
		 },
		_getInputFields: function(){
			var selector = (!this.options.validateHidden) ? 'input[type!=submit]:visible,select[type!=submit]:visible' : ':input[type!=submit]',
				hiddenchkboxes = this._gethiddencheckbox();
            return jQuery.merge(jQuery(this.element).find(selector), hiddenchkboxes);
		},

		/**
		 * Function: _getSelectFields
		 * Get all select fields in a given form.
		 */
		_getSelectFields: function(){
			var selector = (!this.options.validateHidden) ? 'select:visible' : ':select';
            return jQuery(this.element).find(selector);
		},

		/**
		 * Function: _onValidation
		 * Validation form onSubmit using rules specified.
		 *
		 * @param {DOM object} formElement: Given form element in which we want to validate.
		 */
        _onValidation: function(formElement){

  			var webForm = this;
            var passed = new Array();

            var inputElements = webForm._getInputFields();
            inputElements.each(function(i, inputElement){
            	webForm._trigger("beforeSubmit", null, [this.getAttribute('id')]);
                passed.push(webForm._validateInputFields(jQuery(inputElement)));
            })
			var selectElements = webForm._getSelectFields();
            selectElements.each(function(i, selectElement){
            	webForm._trigger("beforeSubmit", null, [this.getAttribute('id')]);
                passed.push(webForm._validateInputFields(jQuery(selectElement)));
            })
            if (jQuery.inArray(false, passed) != -1) {
                return false;
            }

        
			return true;
        },

		/**
		 * Function: _validateInputFields
		 * Validation form input fields using rules specified.
		 *
		 * @param {DOM object} inputElement: Given input element, for validating
		 */
        _validateInputFields: function(inputElement,index){
			var webForm = this;
            var passed = new Array();
            for (property in webForm.validators) {
                var value = inputElement.attr(property);
                if (value === property.replace("gfv_","") || value === true) {
                    if (!webForm.validators[property].test.apply(this, [inputElement])) {

                        webForm.validators[property].errorDisplay.apply(this, [inputElement, property.replace("gfv_","")]);
                        passed.push(false);
                    }
                }
            }

            if (jQuery.inArray(false, passed) != -1) {
                return false;
            }

			return true;
        },

        /**
		 * Function: _validateInputFieldsType
		 * Validation form input fields using type specified.
		 *
		 * @param {DOM object} inputElement: Given input element, for validating
		 */
        _validateInputFieldsType: function(inputElement, type){
            for (property in webForm.validatorTypes) {
                if (property === type) {

               	 	if (!this.validatorTypes[type].test.apply(this, [inputElement])) {
						if(webForm.validatorTypes.errorType) {
							this.validatorTypes[type].errorDisplay.apply(this, [inputElement, webForm.validatorTypes.errorType]);
						}
						else {
							this.validatorTypes[type].errorDisplay.apply(this, [inputElement, type]);
						}
               			return false;
            		}


					return true;
                }
            }
		},

        /**
         * Method: _isEmpty
         * check if a input field is empty.
         */
        _isEmpty: function(inputElement){
            var type = inputElement.attr('type');
            var answer  = (type == 'checkbox') ? (inputElement.val() == 'false') : (inputElement.val() == '');return answer;
        },

        /**
         * Method: addValidatorType
         * adds rules to validatortype.
         */
        addValidatorType: function(type, rules, validatorRulesType){
            var validatorTypes = (validatorRulesType === webForm.TYPE_VALIDATOR) ? this.validatorTypes : this.validators
            validatorType = validatorTypes[type];
            validatorType = (validatorType) ? jQuery.extend(true, validatorType, rules) : rules;
            this.validatorTypes[type] = validatorType;
        },

        /**
         * Method: addValidator
         * adds rules to validator.
         */
        addValidator: function(type, rules){
            var validator = this.validators[type];
            validator = (validator) ? jQuery.extend(true, validator, rules) : rules;
            this.validators[type] = validator;
        },

		addErrorMessage: function(messageMap){
			var webForm = this;
			webForm.messageMap = messageMap;
		},

		_setBrowserScrollingOffsets: function(x, y)
		{
		   if( document.documentElement && document.documentElement.scrollTop ){
              // Explorer 6 Strict
              document.documentElement.scrollLeft = x;
              document.documentElement.scrollTop = y;
           }
           else if( document.body ){
              document.body.scrollLeft = x;
              document.body.scrollTop = y;
           }
		},

		_luhnCheck: function()
		{
           var cardnumber = jQuery("#cardNumber").val();
           var oddoreven = cardnumber.length & 1;
           var sum = 0;
           var addition = "";
           for (var count = 0; count < cardnumber.length; count++)
           {
              var digit = parseInt(cardnumber.substr(count,1));
              if (!((count & 1) ^ oddoreven))
              {
                 digit *= 2;
                 if (digit > 9)
                 {
                    digit -= 9;
                    addition = addition + ' ' + digit;
                 }
                 else
                 {
                    addition = addition + ' ' + digit;
                 }
                 sum += digit;
              }
              else
              {
                 sum += digit;
                 addition = addition + ' ' + digit;
              }
           }
           if (sum % 10 != 0) {return false}
              return true
		},

		_clearCardDetails: function()
		{
			jQuery("#cardType").val("");
			jQuery("#cardNumber").val("");
			jQuery("#cardName").val("");
			jQuery("#expiryDateMM").val("");
			jQuery("#expiryDateYY").val("");
			jQuery("#securityCode").val("");
			jQuery("#issueNumber").val("");
		}
    }

    webForm.TYPE_VALIDATOR = "TYPE_VALIDATOR";
    webForm.VALIDATOR = "VALIDATOR";

    jQuery.widget("ui.webForm", webForm);



})(jQuery);
function checkLengthForMasterCard(ele){
	var masterGiftCard = MasterCardTUIGiftCardNumber.split(',');
	var len;
	masterGiftCard.forEach(function(giftBinaryValue){
		if(len != 19){
			ele.value.includes(giftBinaryValue)? len= 19 : len=16 ;
		}
	})

	var fieldLength = ele.value.length;
	if(fieldLength <= len){
		return true;
	}
	else
	{
		var str = ele.value;
		str = str.substring(0, len);
		ele.value = str;
	}
}
function checkLength(ele){
	var giftCard = TUIGiftCardNumber.split(',');
	var len;
	giftCard.forEach(function(giftBinaryValue){
		if(len != 19){
			ele.value.includes(giftBinaryValue)? len= 19 : len=16 ;
		}
	})

  var fieldLength = ele.value.length;
  if(fieldLength <= len){
    return true;
  }
  else
  {
    var str = ele.value;
    str = str.substring(0, len);
    ele.value = str;
  }
	checkLengthForMasterCard(ele);
}
