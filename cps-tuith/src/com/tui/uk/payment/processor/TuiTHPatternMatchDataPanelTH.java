/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PatternMatchDataPanel.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2014-03-20 08:25:26 $
 *
 * $Author: saleembasha.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.payment.processor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * Represents all the possible known field name used in important
 * information data panel in cps payment page.
 * This enum is used for validating the non payment data in important
 * information data panel.
 * Enum constructor contain two parameters key and pattern.
 * Key represent field name, which is used in important information section in payment page.
 * Pattern represent pattern, which is used for validating field value.
 *
 * @author shwetha.rb
 */
public enum TuiTHPatternMatchDataPanelTH implements Serializable
{
   // CHECKSTYLE:OFF
	irelandTown("irelandTown", "^([a-zA-Z0-9\\s]+[-,:;&()\"\'.!\\/\\s*]*[a-zA-Z0-9 \\s*]*)+$"),
   /** The streetAddress1. */
   streetAddress1("streetAddress1", "([a-zA-Z0-9]+[-,:;&()\"\'.!\\/\\s*]*[a-zA-Z0-9 \\s*]*)+"),

   /** The streetAddress2. */
   streetAddress2("streetAddress2", "([a-zA-Z0-9]+[-,:;&()\"\'.!\\/\\s*]*[a-zA-Z0-9 \\s*]*)+"),

   /** The town. */
   town("town", "^([a-zA-Z\\s]+(_[ a-zA-Z]+)*)$"),

	/** The postcode. */
	postcode("postcode","^([A-Pa-pR-UWYZr-uwyz0-9][A-Ha-hK-Yk-y0-9][AEHMNPRTUVXYaehmnprtuvxy0-9]?[ABEHMNPRVWXYabehmnprvwxy0-9]?[ \\s]{0,1}[0-9][ABD-HJLN-UW-Zabd-hjln-uw-z]{2}|GIR 0AA)$"),

	/** The surName. */
	surName("surName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),

   /** The firstName. */
   firstName("firstName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$");

	//country("country" ,"([[A-Za-z0-9 ]+$"),


   // CHECKSTYLE:ON

   /** The key is field name which is used in cape visa. */
   private String key;

   /** The pattern, used for validate field value. */
   private String pattern;

   /**
    * Constructor with key.
    *
    * @param key the field name, which is used in cape visa.
    * @param pattern the pattern, which is used for validating field value.
    */
   private TuiTHPatternMatchDataPanelTH(String key, String pattern)
   {
      this.key = key;
      this.pattern = pattern;
   }

   /**
    * It will give key.
    *
    * @return key
    */
   public String getKey()
   {
      return key;
   }

   /**
    * This method is responsible for populating map with the details of the
    * specified fields in the validation list.
    * @return the patternMap, the patternMap with all cape visa data panel values.
    */
   public static Map<String, String> getAllPatterns()
   {
      Map<String, String> patternMap = new HashMap<String, String>();
      for (TuiTHPatternMatchDataPanelTH patternMatchDataPanel : values())
      {
            patternMap.put(patternMatchDataPanel.key, patternMatchDataPanel.pattern);
      }
      return patternMap;
   }

}
