<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:formatNumber
	value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
	var="totalCostingLine" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="multiCentre" value="${bookingComponent.multiCentre}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
	<c:set var="searchUrl"
		value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}" />
	<c:set var="passengersUrl"
		value="${bookingComponent.breadCrumbTrail['PASSENGERS']}" />
	<c:if test="${not empty  bookingComponent.depositComponents}">
		<c:set var="depositDetails"
			value="${bookingComponent.depositComponents }"></c:set>
		<c:set var="CONST_LOW_DEPOSIT">lowDeposit</c:set>
		<c:set var="CONST_DEPOSIT">deposit</c:set>
	</c:if>
	<c:choose>
		<c:when test="${multiCentre eq true}">
			<c:set var="optionsUrl"
				value="${bookingComponent.breadCrumbTrail['CENTRESELECTION']}" />
		</c:when>
		<c:otherwise>
			<c:set var="optionsUrl"
				value="${bookingComponent.breadCrumbTrail['HOTEL']}" />
		</c:otherwise>
	</c:choose>
</c:if>


<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="discountFlag" value="false" />
<c:set var="priceBreakDown"
	value="${bookingComponent.pricingDetails['priceBreakDown']}" />

<c:choose>
	<c:when
		test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
		<c:set value="true" var="tuiLogo" />
	</c:when>
	<c:otherwise>
		<c:set value="false" var="tuiLogo" />
	</c:otherwise>
</c:choose>

<c:forEach var="priceComponent" items="${priceBreakDown}"
	varStatus="count">
	<c:if test="${not empty priceComponent.onlineDiscountData}">
		<c:set var="discountFlag" value="true" />
	</c:if>
</c:forEach>

<c:set var="depositComponentval" value="false" />
<c:forEach var="eachDepositComponentAcc" items="${depositDetails}"
	varStatus="count">
	<c:if test="${not empty eachDepositComponentAcc.depositDataPP}">
		<c:set var="depositComponentval" value="true" />
	</c:if>
</c:forEach>

<c:set var="perPersonPrice"
	value="${bookingComponent.pricingDetails['PER_PERSON_PRICE']}" />
<c:if test="${not empty perPersonPrice}">
	<c:forEach var="price" items="${perPersonPrice}">
		<fmt:formatNumber value="${price.amount.amount}"
			var="perPersonPriceCostingLine" type="number" maxFractionDigits="2"
			minFractionDigits="2" pattern="#####" />
		<c:set var="perpersonpricecost"
			value="${fn:split(perPersonPriceCostingLine, '.')}" />
	</c:forEach>
</c:if>

<div id="book-flow-header">
	<div class="content-width">
		<div
			class="logo<c:if test="${tuiLogo}">TUI</c:if> thomson firstchoiceX falconX">
			<a href="${bookingComponent.clientURLLinks.homePageURL}"></a>
		</div>
		<c:if test="${tuiLogo}">
				<div>
				<div>
				<img alt="World Of TUI" class="nomobile  nominitablet "
					id="header-wtui"
					src="/cms-cps/tuicruisedeals/images/WOT_Endorsement_H_V1_3C.jpg">
				</div>
				<div class="call-wrapper">
					<div class="call">
						<a class="phone" href="tel:0800 107 2323">0800 107 2323</a>
					</div>
					<div class="call small">						
						<span class="hide-midsize"><a class="contact-link"
							href="https://www.cruisedeals.co.uk/information/contact-us">Opening
								Times</a></span>
					</div>
				</div>
				
			</div>
			
		</c:if>
	</div>

</div>
<c:choose>
	<c:when
		test="${bookingComponent.nonPaymentData['isResponsiveHubAndSpoke'] == 'Y'}">
		<c:set var="numOfSteps"
			value="${fn:length(bookingComponent.breadCrumbTrail)}" />
		<c:set var="stepWidth" value="${100 / numOfSteps}" />
		<c:set var="stepWidth" value="${fn:substringBefore(stepWidth, '.')}" />

		<div class="span no-bottom-margin header-spacing breadcrumb">
			<div class="content-width">
				<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true"
					data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
					<ul id="indicators-sprite-${numOfSteps}"
						class="breadcrumbs-spacing">
						<c:forEach var="indicator"
							items="${bookingComponent.breadCrumbTrail}" varStatus="counter">
							<c:choose>

								<c:when test="${!counter.last}">
									<c:set var="indicatorHref" value="${indicator.value}" />

								</c:when>
								<c:otherwise>
									<c:set var="indicatorHref" value="#" />

								</c:otherwise>
							</c:choose>

							<li
								class="indicator  uc  sprite-${numOfSteps} ${counter.last ? '' : 'completed'} ${indicator.key=='PAYMENT'? 'active':''}">

								<i class="arrow in ">${counter.count}</i> <c:if
									test="${!counter.last}">
									<a
										<c:if test="${indicator.key ne 'PAYMENT'}">href="<c:out value='${indicatorHref}'/>"</c:if>
										class="ensLinkTrack"
										data-componentId="bookflowStepIndicator_comp"> <span
										class="">${indicator.key}</span>
									</a>
								</c:if> <c:if test="${counter.last}">
									<a class="last"><span class="">${indicator.key}</span></a>
								</c:if> <i class="arrow out "></i>
							</li>

						</c:forEach>
					</ul>
				</div>
			</div>
			<div class="clear-both"></div>
		</div>


		<div class="component summary total-price">
			<div class="book-navigation content-width">
				<div class="price-panel fr">
					<div class="price-block dis-inblock vertal-m">
						<p class="uppercase">Per Person</p>
						<div class="per-person-price">
							<span class="currency">&pound;</span>
							<c:if test="${not empty perPersonPrice}">
								<span> <span class="part1"><fmt:formatNumber
											value="${perpersonpricecost[0]}" /></span> <span class="part2">.<c:out
											value="${perpersonpricecost[1]}" /></span>
								</span>
							</c:if>
						</div>
						<div class="total-price">
							Total price<span> &pound;<fmt:formatNumber
									value="${totalcost[0]}" />.<c:out value="${totalcost[1]}" /></span>
						</div>
					</div>

					<c:if test="${not empty priceBreakDown or discountFlag}">
						<div class="dis-block discount-block">
							<ul>
								<li class="dis-inblock"><c:forEach var="priceComponent"
										items="${priceBreakDown}" varStatus="count">
										<c:choose>
											<c:when
												test="${not empty priceComponent.itemDescription and count.index==0}">
															Inc &pound;<fmt:formatNumber
													value="${priceComponent.amount.amount}" type="number"
													maxFractionDigits="2" minFractionDigits="2"
													pattern="#####.##" /> discount
													</c:when>
										</c:choose>
									</c:forEach></li>
								<c:if test="${not empty depositDetails}">
									<li class="dis-inblock"><c:forEach
											var="eachDepositComponent" items="${depositDetails}"
											varStatus="deposits">

											<c:choose>
												<c:when
													test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
													<c:out value="${eachDepositComponent.depositDataPP}" />
												</c:when>

												<c:otherwise>
													<c:if
														test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
														<c:out value="${eachDepositComponent.depositDataPP}" />
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:forEach></li>
								</c:if>
							</ul>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div id="book-flow-progress">
			<div class="content-width">
				<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true"
					data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
					<!--	<ul class="c">
							<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">1</span> ITINERARY & SHIP</a></li>
							<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">2</span> CRUISE OPTIONS</a></li>
							<li class="back"><a href="<c:out value='${passengersUrl}'/>"><span class="rel b">3</span> FLIGHTS</a></li>
							<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">4</span> EXTRAS</a></li>
							<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">5</span> BOOK</a></li>
							
							<li class="active"><span class="rel b">6</span> Payment</li>
						</ul>-->

					<ul id="indicators-sprite-${numOfSteps}"
						class="c breadcrumbs-spacing">
						<c:forEach var="indicator"
							items="${bookingComponent.breadCrumbTrail}" varStatus="counter">
							<c:choose>

								<c:when test="${!counter.last}">
									<c:set var="indicatorHref" value="${indicator.value}" />

								</c:when>
								<c:otherwise>
									<c:set var="indicatorHref" value="#" />

								</c:otherwise>
							</c:choose>

							<li
								class="indicator back  uc  sprite-${numOfSteps}${counter.last ? ' last active' : counter.first ? ' first completed' : ' completed'}"><c:if
									test="${!counter.last}">
									<a href="<c:out value="${indicatorHref}"/>"
										class="ensLinkTrack"
										data-componentId="bookflowStepIndicator_comp">
								</c:if><span class="rel b">${counter.index+1}</span>${indicator.key}<c:if
									test="${!counter.last}">
									</a>
								</c:if></li>


						</c:forEach>

					</ul>
				</div>
			</div>
		</div>
		<div class="main-price-summary">
			<div class="content-width">
				<div class="inner-wrap">


					<div
						class="left-content <c:if test='${ not discountFlag and not depositComponentval}'> hide </c:if>">
						<div class="text">
							<span class="promotional-discount"> <c:if
									test="${not empty priceBreakDown and discountFlag}">
									<c:forEach var="priceComponent" items="${priceBreakDown}"
										varStatus="count">
										<c:choose>
											<c:when
												test="${not empty priceComponent.itemDescription and not empty priceComponent.onlineDiscountData and count.index==0}">
																	Inc &pound;<fmt:formatNumber
													value="${priceComponent.amount.amount}" type="number"
													maxFractionDigits="2" minFractionDigits="2"
													pattern="#####.##" /> discount<br>
											</c:when>
										</c:choose>
									</c:forEach>
								</c:if>
							</span> <span class="promotional-discount"> <c:if
									test="${not empty depositDetails}">
									<c:forEach var="eachDepositComponent" items="${depositDetails}"
										varStatus="deposits">
										<c:choose>
											<c:when
												test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP }">

												<c:out value="${eachDepositComponent.depositDataPP}"
													escapeXml="false" />
											</c:when>
											<c:when
												test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP}">

												<c:out value="${eachDepositComponent.depositDataPP}"
													escapeXml="false" />
											</c:when>
										</c:choose>
									</c:forEach>
								</c:if>
							</span>
						</div>
					</div>

					<div
						class="right-content <c:if test='${ not discountFlag and not depositComponentval}'>no-discount</c:if>">
						<div class="price">
							<span class="currency">&pound;</span> <span class="num"><span
								class="part1"><fmt:formatNumber value="${totalcost[0]}" />.</span><span
								class="part2"><c:out value="${totalcost[1]}" /></span></span>
						</div>
						<span class="per-person-price"><c:if
								test="${not empty bookingComponent.pricingDetails}">
								<c:set var="perPersonPrice"
									value="${bookingComponent.pricingDetails['PER_PERSON_PRICE']}" />
								<c:if test="${not empty perPersonPrice}">
									<c:forEach var="price" items="${perPersonPrice}">
										<c:if test="${price.amount.amount > 0}">
																		&pound;<fmt:formatNumber
												value="${price.amount.amount}" type="number"
												maxFractionDigits="2" minFractionDigits="2"
												pattern="#####.##" /> per person
																	</c:if>
									</c:forEach>
								</c:if>


							</c:if></span>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>