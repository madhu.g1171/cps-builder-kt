<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.tuifc" , "");
pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);
String firstchoiceBrochure = ConfReader.getConfEntry("firstchoice.brochure.url" , "");
pageContext.setAttribute("firstchoiceBrochure", firstchoiceBrochure, PageContext.REQUEST_SCOPE);

%>

<div id="footer">
				<div id="call-us" class="hide">
					<div class="content-width">
						<i class="caret call white b"></i><h2 class="d-blue">BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203 636 1931</a></span></h2>
					</div>
				</div>
				<div id="search" class="b">
					<div class="content-width">
						<a href="#page" id="backtotop"><span>To top </span><i class="caret back-to-top white"></i></a>
					</div>
				</div>
				<div id="group" class="b thomson">
					<div class="content-width">
						<div class="copy">
							<p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p>
						</div>
						<div class="logos">

							<a target="_blank" href="http://abta.com/go-travel/before-you-travel/find-a-member" id="logo-abta" title="ABTA - The Travel Association"></a>
							<a target="_blank" href="http://www.caa.co.uk/application.aspx?catid=490&pagetype=65&appid=2&mode=detailnosummary&atolnumber=2524" id="logo-atol" title="ATOL Protected"></a>
						</div>
					</div>
				</div>
				<div id="seo" class="nominitablet nomobile">
					<div class="accordion" data-accordion>
						<div class="item">
							<div class="content-width">
								<div class="trigger">
								<ul>

									<li  >Holiday Types</li>

									<li  >Mid/Long haul</li>
									<li  >Short haul</li>

									<li  >All inclusive</li>

								</ul>
								</div>
								<div class="content">
								  <div class="c">
									<div id="holidaytypes" class="c">
										<p>Holiday Types</p>
										<div>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/summer-2017-deal" enslinktrackattached="true">Summer Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/winter-2016-deal" enslinktrackattached="true">Winter Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holidays/adult" enslinktrackattached="true">Adult Only Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holidays/all-inclusive" enslinktrackattached="true">All inclusive holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holidays/family" enslinktrackattached="true">Family Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/summer-2017-deal" enslinktrackattached="true">Sunshine Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/deals" enslinktrackattached="true">Cheap Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holidays/last-minute" enslinktrackattached="true">Last Minute Holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holidays/beach" enslinktrackattached="true">Beach Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/package-holidays" enslinktrackattached="true">Package Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holidays/last-minute" enslinktrackattached="true">Late deals</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holidays/luxury" enslinktrackattached="true">Luxury Holidays</a>
										</div>
									</div>

									<div id="longhaul" class="c">
									  <p>Mid/Long haul</p>
									  <div>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Egypt-EGY" enslinktrackattached="true">Egypt holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Goa-001915" enslinktrackattached="true">Goa holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Sri-Lanka-LKA" enslinktrackattached="true">Sri Lanka Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/The-Caribbean-CCC" enslinktrackattached="true">Caribbean holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Dominican-Republic-DOM" enslinktrackattached="true">Dominican Republic holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cape-Verde-CPV" enslinktrackattached="true">Cape Verde holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Jamaica-JAM" enslinktrackattached="true">Jamaica holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Thailand-THA" enslinktrackattached="true">Thailand Holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Aruba-ABW" enslinktrackattached="true">Aruba holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Turkey-TUR" enslinktrackattached="true">Turkey holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Mexico-MEX" enslinktrackattached="true">Mexico Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Barbados-BRB" enslinktrackattached="true">Barbados holidays</a>


											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/St-Lucia-LCA" enslinktrackattached="true">St Lucia Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Vietnam-VNM" enslinktrackattached="true">Vietnam Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Dubai-and-Emirates-004017" enslinktrackattached="true">Dubai Holidays</a>
									   </div>
									</div>
									<div id="shorthaul" class="c">
									  <p>Short Haul</p>
									  <div>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Portugal-PRT" enslinktrackattached="true">Portugal Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cyprus-CYP" enslinktrackattached="true">Cyprus holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Malta-MLT" enslinktrackattached="true">Malta holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Morocco-MAR" enslinktrackattached="true">Morocco holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Bulgaria-BGR" enslinktrackattached="true">Bulgaria holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Spain-ESP" enslinktrackattached="true">Spain holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Italy-ITA" enslinktrackattached="true">Italy holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Croatia-HRV" enslinktrackattached="true">Croatia holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Greece-GRC" enslinktrackattached="true">Greece holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Crete-000800" enslinktrackattached="true">Crete Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Rhodes-000923" enslinktrackattached="true">Rhodes holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Corfu-000691" enslinktrackattached="true">Corfu holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Dubrovnik-&amp;-Islands-002925" enslinktrackattached="true">Dubrovnik Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Paphos-000799" enslinktrackattached="true">Paphos Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Fuerteventura-000312" enslinktrackattached="true">Fuerteventura Holidays</a>
									  </div>
									</div>
									<div id="popDestinations" class="c">
										<p>All inclusive</p>
									  <div>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Tenerife-000335" enslinktrackattached="true">Tenerife holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Madeira-000157" enslinktrackattached="true">Madeira Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Ibiza-000242" enslinktrackattached="true">Ibiza holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Costa-del-Sol-000365" enslinktrackattached="true">Costa Del Sol holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Zante-000952" enslinktrackattached="true">Zante holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Gran-Canaria-000318" enslinktrackattached="true">Gran Canaria holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Kefalonia-000864" enslinktrackattached="true">Kefalonia holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Benidorm-000349" enslinktrackattached="true">Benidorm holidays</a>

											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Lanzarote-000329" enslinktrackattached="true">Lanzarote holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Majorca-000122" enslinktrackattached="true">Majorca holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cuba-CUB" enslinktrackattached="true">Cuba Holidays</a>
											<a data-componentid="WF_COM_LM_200-3" class="ensLinkTrack" target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Algarve-000154" enslinktrackattached="true">Algarve Holidays</a>

										</div>
									</div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="terms">
					<div class="content-width">
						<p class="title">More from Holidays</p>
						<p>

							<a target="_blank" href="http://www.firstchoice.co.uk/about-us">About First Choice</a>
							<a target="_blank" href="http://www.firstchoice.co.uk/myapp/">MyFirstChoice app</a>
							<a target="_blank" href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies">Cookies policy</a>

							<a target="_blank" href="http://www.firstchoice.co.uk/our-policies/privacy-policy">Privacy Policy</a>
							<a target="_blank" href="http://www.firstchoice.co.uk/our-policies/terms-of-use">Terms &amp; Conditions</a>
							<c:choose>
								<c:when test="${applyCreditCardSurcharge eq 'true'}">
							<a target="_blank" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges">Credit card fees</a>
								</c:when>
								<c:otherwise>
									<a target="_blank" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges">Ways to Pay</a>
								</c:otherwise>
							</c:choose>

							<a target="_blank" href="http://www.firstchoice.co.uk/our-policies/accessibility/">Accessibility</a>
							<a target="_blank" class="nodesktop notablet" href="/holiday?desktop=true">Desktop site</a>
							<!--<a href="http://communicationcentre.firstchoice.co.uk/">Media Centre</a>

							<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>-->
							<a target="_blank" href="http://www.firstchoice.co.uk/affiliates.html">Affiliates</a>
							<a target="_blank" href="http://press.firstchoice.co.uk/">Press</a>
							<a target="_blank" href="http://www.firstchoice.co.uk/blog/">First Choice Blog</a>

								<a target="_blank"  href="http://www.tuitravelplc.com/"><jsp:useBean id='CurrentDate12' class='java.util.Date'/>
         	<fmt:formatDate var='currentYear' value='${CurrentDate12}' pattern='yyyy'/>
			&copy; <c:out value="${currentYear}" />  TUI Group</a>

							<a target="_blank"  href="http://www.firstchoice.co.uk/holiday/info/deals-terms">Promotional Terms &amp; Conditions</a>

                           <a target="_blank"  href="${firstchoiceBrochure}">Holiday Brochures
						</a>
						</p>
					</div>
				</div>
				<%=staySafeAbroad %>
			</div>
			<div id="disclaimer">
				<div class="content-width disclaim two-columns">

					<p>Some of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme.  But ATOL protection does not apply to all holiday and travel services listed on this website. This website will provide you with information on the protection that applies in the case of each holiday and travel service offered before you make your booking.  If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information, or for more information about financial protection and the ATOL Certificate go to:<a href="http://www.caa.co.uk/home/" target="_blank">www.caa.co.uk</a></p>

				</div>
			</div>
