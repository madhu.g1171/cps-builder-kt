
		var errorMap = {
			    //On submit error messages
			   	firstnameSubmitErrorrequired:"Please use between 2 and 15 characters. ",
			    surnameSubmitErrorrequired:"Please use between 1 and 15 characters.",
				houseNameSubmitErrorrequired: "Please use between 1 and 32 characters.",
				addressLine1SubmitErrorrequired: "Please use between 2 and 32 characters.",
				citySubmitErrorrequired: "Please use between 1 and 25 characters.",
				postCodeSubmitErrorrequired: "Please use between 5 and 8 characters. ",
				dayTimePhoneSubmitErrorrequired: "Please enter a Main Phone No",
				emailAddressSubmitErrorrequired: "Please enter an Email Address",
				confirmEmailAddressSubmitErrorrequired: "Please confirm your Email Address",
				emailAddressSubmitErrormatch: "The email addresses do not match",
				confirmEmailAddressSubmitErrormatch: "The email addresses do not match",
				cardTypeSubmitErrorrequired: "Please select a Card Type",
				expiryDateYYSubmitErrorrequired: "Please enter a valid expiry date. ",
				expiryDateMMSubmitErrorrequired: "Please enter a valid expiry date. ",
				cardNumberSubmitErrorrequired: "Please enter a card number.",
				cardNameSubmitErrorrequired: "Please enter the name as it appears on the card.",
				securityCodeSubmitErrorrequired: "Please enter the security code. You can find this number on the back of the card.",
				issueNumberSubmitErrorrequired: "Please enter a issue number",
				cardHouseNameSubmitErrorrequired: "Please enter your House Name/No",
				cardAddress1SubmitErrorrequired: "Please enter a Street Address",
				cardTownCitySubmitErrorrequired: "Please enter a Town/City",
				cardPostCodeSubmitErrorrequired_cardpostcode: "Please enter a Post Code",
				countySubmitErrorrequired: "Please use between 1 and 16 characters.",
				cardcountrySubmitErrorrequired: "Please select a Country",
				tourOperatorTermsAcceptedSubmitErrorrequired: "Please confirm you have read and accept the terms & conditions and privacy policy.",
				passengerTitle0SubmitErrorrequired: "Please select a title.",
				
				passengerTitle0BlurErrortitleEmpty: "Please select a title.",
				
				cardNumberBlurErrorcardNoEmpty: "Please enter a card number.",
				cardNumberBlurErrorcardNoMinLength:"Card number can only include numbers and must be 15 or 16 digits, excluding gift cards.",
				cardNumberBlurErrorcardNoRegEx: "Card number can only include numbers and must be 15 or 16 digits, excluding gift cards.",
				cardNumberBlurErrorcardNoZero: "Please enter a valid card number.",
				
				securityCodeBlurErrorsecurityCodeEmpty: "Please enter the security code. You can find this number on the back of the card.",
				cardNameBlurErrorcardNameEmpty: "Please enter the name as it appears on the card.",
				firstnameBlurErrorfirstnameEmpty: "Please use between 2 and 15 characters.",
				surnameBlurErrorsurnameEmpty: "Please use between 1 and 15 characters.",
				houseNameBlurErrorhouseNameEmpty: "Please use between 1 and 32 characters.",
				cityBlurErrorcityEmpty: "Please use between 1 and 25 characters.",
				countyBlurErrorcountyEmpty: "Please use between 1 and 16 characters.",
				postCodeBlurErrorpostcodeEmpty: "Please use between 5 and 8 characters.",
				addressLine1BlurErroraddressLine1Empty: "Please use between 2 and 32 characters.",
				
				//addressLine2BlurErrorrequired: "Please enter a Street Address2"
				postCodeBlurErrorrequired: "Please use between 5 and 8 characters.",
				dayTimePhoneBlurErrorrequired: "Please enter a Main Phone No",
				emailAddressBlurErrorrequired: "Please enter an Email Address",
				confirmEmailAddressBlurErrorrequired: "Please confirm your Email Address",
				cardTypeBlurErrorrequired: "Please select a Card Type",
				expiryDateYYBlurErrorrequired: "Please enter a valid expiry date. ",
				expiryDateMMBlurErrorrequired: "Please enter a valid expiry date. ",
				cardNumberBlurErrorrequired: "Please enter a card number.",
				cardNameBlurErrorrequired: "Please enter the name as it appears on the card.",
				securityCodeBlurErrorrequired: "Please enter the security code. You can find this number on the back of the card.",
				cardHouseNameBlurErrorrequired: "Please enter your House Name/No",
				cardAddress1BlurErrorrequired: "Please enter a Street Address",
				cardTownCityBlurErrorrequired: "Please enter a Town/City",
				cardPostCodeBlurErrorrequired: "Please use between 5 and 8 characters.",
				countryBlurErrorrequired: "Please select a Country",
				cardcountryBlurErrorrequired: "Please select a Country",
				tourOperatorTermsAcceptedBlurErrorrequired: "Please confirm you have read and accept the terms & conditions and privacy policy.",
				passengerTitle0BlurErrorTitleEmpty: "Please select a title",

				//On blur error messages
				firstnameBlurErroralpha:"Please use between 2 and 15 characters.",
			    surnameBlurErrorsurname:"Please use between 1 and 15 characters.",
				houseNameBlurErroralphanumericspec: "Please use between 1 and 32 characters.",
				addressLine1BlurErrorstreetline1Check: "Please use between 2 and 32 characters.",
				addressLine2BlurErrorstreetline1Check: "Please use between 2 and 25 characters.",
				cityBlurErrortownorcityCheck: "Please use between 1 and 25 characters.",
				countyBlurErrortestCounty: "Please use between 1 and 16 characters.",
				postCodeBlurErrorpostcode: "Please use between 5 and 8 characters.",
				postCodeOptionalBlurErroralphanumeric: "Please use between 5 and 8 characters.",
				dayTimePhoneBlurErrorphonenumber: "Please enter a valid Phone Number",
				mobilePhoneBlurErrorphonenumber: "Please enter a valid Phone Number",
				emailAddressBlurErroremail: "Please enter a valid Email Address",
				confirmEmailAddressBlurErroremail: "Please enter a valid Confirm Email Address",
				cardNumberBlurErrorcardnumber: "Please check your Card Number",
				cardNameBlurErroralpha: "Numbers and some special characters are not valid.",
				expiryDateYYBlurErrorexpirydate: "Expiry Date - Has The Card Expired?",
				expiryDateMMBlurErrorexpirydate: "Expiry Date - Has The Card Expired?",
				securityCodeBlurErrorsecuritycode: "Please enter the security code. You can find this number on the back of the card.",
				issueNumberBlurErrornumeric:"Please enter a valid Issue Number",
				issueNumberBlurErrorissuenumber:"Please enter a valid Issue Number",
				cardHouseNameBlurErroralphanumericspec: "Please use between 1 and 32 characters.",
				cardAddress1BlurErroralpha: "Please use between 2 and 32 characters",
				cardAddress2BlurErroralpha: "Please use between 2 and 20 characters.",
				cardTownCityBlurErroralpha: "Please use between 2 and 25 characters.",
				cardPostCodeBlurErrorcardpostcode: "Please use between 5 and 8 characters.",				
				cardCountyBlurErroralpha:"Please use between 1 and 16 characters.",
				termsAcceptedBlurErrorrequired: "Please confirm you have read and accept the terms & conditions and privacy policy.",

                //top error messages for server side validation errors
				houseNameTopServerError: "House Name/No",
				addressLine1TopServerError: "Street Address",
				cityTopServerError: "Town/City",
				countyTopServerError: "County",
				postCodeTopServerError: "Post Code",
				dayTimePhoneTopServerError: "Telephone Number",
				mobilePhoneTopServerError: "Phone Number",
				emailAddressTopServerError: "Email Address",
				cardHouseNameTopServerError: "CardHolder House Name/No",
				cardAddress1TopServerError: "CardHolder Street Address",
				cardTownCityTopServerError: "CardHolder Town/City",
				cardPostCodeTopServerError: "CardHolder Post Code",
				countryTopServerError: "Country",
				cardcountryTopServerError: "CardHolder Country",
                titleTopServerError:"Passenger Title",
                cardCountyTopServerError:"CardHolder County",

                // Field specific error messages
				houseNameServerError: "Please use between 1 and 32 characters.",
				addressLine1ServerError: "Please use between 2 and 32 characters.",
				cityServerError: "Please use between 1 and 25 characters.",
				countyServerError: "Please use between 1 and 16 characters.",
				postCodeServerError: "Please use between 5 and 8 characters.",
				dayTimePhoneServerError: "Please enter a valid Phone Number",
				mobilePhoneServerError: "Please enter a valid Phone Number",
				emailAddressServerError: "Please enter a valid Email Address",
				confirmEmailAddressServerError: "Please enter a valid Email Address",
				cardHouseNameServerError: "Please use between 1 and 32 characters.",
				cardAddress1ServerError: "Please use between 2 and 32 characters.",
				cardTownCityServerError: "Please use between 1 and 25 characters.",
				cardPostCodeServerError: "Please use between 5 and 8 characters.",
				countryServerError: "Please select a Country",
				cardcountryServerError: "Please select a Country",
				//cardCountyServerError:"CardHolder County",
			    titleServerError:"Passenger Title"
			};
		
		function InitiatizeValidation() {
			var form2 = jQuery('form').webForm({
				onSubmitErrorDisplay: function(event, args){
				   commonSubmitBlurErrorDisplay(args);
				},
				onBlurErrorDisplay: function(event, args){
				   commonSubmitBlurErrorDisplay(args);
     			},
				onBlurSucessfulDisplay: function(event, args){			
					var field = jQuery(args[1]),
						fieldRow = field.closest('.row');
					console.log('args[1]:',args);
					if(args[1].value){
						fieldRow.find('.message').remove();
					  	fieldRow.removeClass("error valid").addClass('valid');
					}else{
						fieldRow.find('.message').remove();
						fieldRow.removeClass("error valid");
					}									   			  
     			},
				beforeSubmit: function(event, args){			    
					if(jQuery("."+ args +"Error #"+ args +"Error").length >1)
					{
			           jQuery("#"+ args +"Error").remove();
					   jQuery("#"+args+"TopError").remove();
					}
					if(args == "tourOperatorTermsAccepted" && jQuery("#"+args).val()== 'true')
					{
					   console.log('its terms & condition',args);
					   //jQuery("#"+ args +"Error").remove();
					   //jQuery("#"+args+"TopError").remove();
					   jQuery("#"+ args).closest('.row').removeClass('error');
					   jQuery("#"+ args).closest('.row').find('.message').text('');
					}
				    if(jQuery("."+ args +"Error p.formErrorMessage").length < 1)
				       jQuery("."+ args +"Error").removeClass("formError");

					if(jQuery("ul#topError li").length < 1)
					   jQuery("#errorSummary").addClass("hide");
					jQuery("#cardDetailsError").addClass("hide");
					forceRedraw();
				},
				beforeBlur: function(event, args){
					jQuery("#"+ args +"Error").remove();
					if(jQuery("li ."+ args +"Error p.formErrorMessage").length < 1)
                       jQuery("."+ args +"Error").removeClass("formError");
					jQuery("."+ args +"Error").removeClass("formSucess");
					jQuery("#"+args+"TopError").remove();
					if(jQuery("ul#topError li").length < 1)
					   jQuery("#errorSummary").addClass("hide");
					forceRedraw();
				},
				onSubmitLuhnErrorDisplay: function(event)
				{
				   jQuery("#cardDetailsError").removeClass("hide");
				   forceRedraw();
				},
				promoErrorDisplay: function(event,args)
				{
				   var id=args[0] + "Error";
				   if (jQuery("#promoCodeSectionformErrorMessage"))
				   {
				     jQuery("#promoCodeSection").removeClass("formError");
                     jQuery("#promoCodeSectionformErrorMessage").addClass("hide");
				     jQuery("#promoCodeSectionError").remove();
			         jQuery("#promoCodeSectionTopError").remove();
				   }
				   if (jQuery("#leadPassengerformErrorMessage"))
				   {
                     jQuery("#leadPassenger").removeClass("formError");
                     jQuery("#leadPassengerformErrorMessage").addClass("hide");
				     jQuery("#leadPassengerError").remove();
			         jQuery("#leadPassengerTopError").remove();
				   }
				   var topContainer = jQuery("#topError");
				   jQuery("#" + args[0]).addClass("formError");
				   var topText = 'Promotional Code';
				   var topContainer = jQuery("#topError");
				   jQuery("#errorSummary").removeClass("hide");
				   jQuery("<p class='formErrorMessage' id='"+id+"'>"+args[1]+"</p>").prependTo(jQuery("#" + args[0]));
                   jQuery("<li id='"+args[0]+"TopError'>"+topText+"</li>").appendTo(topContainer);
				   forceRedraw();

				   if (args[0] == 'leadPassenger')
				   {
				     jQuery("#promoCodeSection").addClass("formError");
                     jQuery("<p class='formErrorMessage' id='promoCodeSectionformErrorMessage'>"+'Please enter Lead Passenger Details'+"</p>").prependTo(jQuery("#promoCodeSection"));
				   }
			       jQuery("#commonError").addClass("hide");

				},

				promoSuccessDisplay: function(event, args)
				{
				  if(jQuery("ul#topError li").length > 1)
				  {
					jQuery("#promoCodeSectionError").remove();
				    jQuery("#promoCodeSectionTopError").remove();
				    jQuery("#leadPassengerError").remove();
			        jQuery("#leadPassengerTopError").remove();
				  }
				  else
				  {
					jQuery("#errorSummary").addClass("hide");
				  }
				  forceRedraw();
				  jQuery("#promoCodeText").addClass('hide');
				  jQuery("#promoCodeSection").addClass('hide');
				  jQuery("#promoCodeSuccessSection").removeClass('hide');
				  jQuery("#discountAmt").html(args[0]);
				}
			});
		}
		
		jQuery(document).ready(function (jQuery) {

			

			InitiatizeValidation();



			for (var i = 0; i < Personaldetails.noOfPassengers; i++)
			{
				errorMap["surName_"+i+"BlurErroralpha"]= "Please use between 1 and 15 characters. ";
                errorMap["foreName_"+i+"BlurErroralpha"]= "Please use between 2 and 15 characters.";
				errorMap["surName_"+i+"SubmitErrorrequired"]= "Please use between 1 and 15 characters. ";
				errorMap["foreName_"+i+"SubmitErrorrequired"]= "Please use between 2 and 15 characters.";
				errorMap["passengerTitle"+i+"SubmitErrorrequired"]="Please select a title.";


            }
			jQuery('form').webForm("addErrorMessage", errorMap);

    });

